/* Tabellen fuer CAO-Faktura MYSQL Stand 18.09.2003 DB-Version 1.08 */

CREATE TABLE ADRESSEN (
  REC_ID int(11) NOT NULL auto_increment,
  MATCHCODE varchar(255) NOT NULL default '',
  KUNDENGRUPPE int(11) NOT NULL default '0',
  SPRACH_ID int(11) NOT NULL default '2',
  KUNNUM1 varchar(20) default NULL,
  KUNNUM2 varchar(20) default NULL,
  NAME1 varchar(40) default NULL,
  PLZ varchar(10) default NULL,
  ORT varchar(40) default NULL,
  LAND varchar(5) default NULL,
  VWAHL varchar(10) default NULL,
  NAME2 varchar(40) default NULL,
  NAME3 varchar(40) default NULL,
  ABTEILUNG varchar(40) default NULL,
  ANREDE varchar(40) default NULL,
  STRASSE varchar(40) default NULL,
  POSTFACH varchar(40) default NULL,
  PF_PLZ varchar(10) default NULL,
  DEFAULT_LIEFANSCHRIFT_ID int(11) NOT NULL default '-1',
  GRUPPE varchar(4) default NULL,
  TELE1 varchar(100) default NULL,
  TELE2 varchar(100) default NULL,
  FAX varchar(100) default NULL,
  FUNK varchar(100) default NULL,
  EMAIL varchar(100) default NULL,
  EMAIL2 varchar(100) default NULL,
  INTERNET varchar(100) default NULL,
  DIVERSES varchar(100) default NULL,
  BRIEFANREDE varchar(100) default NULL,
  BLZ varchar(20) default NULL,
  KTO varchar(20) default NULL,
  BANK varchar(40) default NULL,
  IBAN varchar(100) default NULL,
  SWIFT varchar(100) default NULL,
  KTO_INHABER varchar(40) default NULL,
  DEB_NUM int(11) default NULL,
  KRD_NUM int(11) default NULL,
  STATUS int(11) default NULL,
  NET_SKONTO float(5,2) default NULL,
  NET_TAGE tinyint(4) default NULL,
  BRT_TAGE tinyint(4) default NULL,
  WAERUNG varchar(5) default NULL,
  UST_NUM varchar(25) default NULL,
  VERTRETER_ID int(11) unsigned NOT NULL default '0',
  PROVIS_PROZ float(5,2) NOT NULL default '0.00',
  INFO text,
  GRABATT float(5,2) default NULL,
  KUN_KRDLIMIT float(10,2) default NULL,
  KUN_LIEFART int(11) NOT NULL default '-1',
  KUN_ZAHLART int(11) NOT NULL default '-1',
  KUN_PRLISTE enum('N','Y') NOT NULL default 'N',
  LIEF_LIEFART int(11) NOT NULL default '-1',
  LIEF_ZAHLART int(11) NOT NULL default '-1',
  LIEF_PRLISTE enum('N','Y') NOT NULL default 'N',
  PR_EBENE tinyint(1) default '5',
  BRUTTO_FLAG enum('N','Y') NOT NULL default 'N',
  MWST_FREI_FLAG enum('N','Y') NOT NULL default 'N',
  KUN_SEIT date default NULL,
  KUN_GEBDATUM date default NULL,
  ENTFERNUNG int(10) unsigned default NULL,
  ERSTELLT date default NULL,
  ERST_NAME varchar(20) default NULL,
  GEAEND date default NULL,
  GEAEND_NAME varchar(20) default NULL,
  SHOP_KUNDE enum('N','Y') NOT NULL default 'N',
  SHOP_ID tinyint(3) NOT NULL default '-1',
  SHOP_KUNDE_ID int(11) NOT NULL default '-1',
  USERFELD_01 varchar(255) default NULL,
  USERFELD_02 varchar(255) default NULL,
  USERFELD_03 varchar(255) default NULL,
  USERFELD_04 varchar(255) default NULL,
  USERFELD_05 varchar(255) default NULL,
  USERFELD_06 varchar(255) default NULL,
  USERFELD_07 varchar(255) default NULL,
  USERFELD_08 varchar(255) default NULL,
  USERFELD_09 varchar(255) default NULL,
  USERFELD_10 varchar(255) default NULL,
  PRIMARY KEY  (REC_ID)
);

CREATE TABLE ADRESSEN_LIEF (
  REC_ID int(11) NOT NULL auto_increment,
  ADDR_ID int(11) NOT NULL default '0',
  ANREDE varchar(40) default NULL,
  NAME1 varchar(40) NOT NULL default '',
  NAME2 varchar(40) default NULL,
  NAME3 varchar(40) default NULL,
  ABTEILUNG varchar(40) default NULL,
  STRASSE varchar(40) NOT NULL default '',
  LAND varchar(5) default NULL,
  PLZ varchar(10) NOT NULL default '',
  ORT varchar(40) NOT NULL default '',
  INFO text,
  PRIMARY KEY  (REC_ID,ADDR_ID)
);

CREATE TABLE ADRESSEN_MERK (
  MERKMAL_ID int(11) NOT NULL auto_increment,
  NAME varchar(100) NOT NULL default '',
  PRIMARY KEY  (MERKMAL_ID)
);

CREATE TABLE ADRESSEN_TO_MERK (
  MERKMAL_ID int(11) unsigned NOT NULL default '0',
  ADDR_ID int(11) unsigned NOT NULL default '0',
  PRIMARY KEY  (MERKMAL_ID,ADDR_ID)
);

CREATE TABLE APARTNER (
  ADDR_ID int(11) NOT NULL default '0',
  REC_ID int(11) NOT NULL auto_increment,
  NAME varchar(40) NOT NULL default '',
  VORNAME varchar(40) NOT NULL default '',
  ANREDE varchar(40) default NULL,
  STRASSE varchar(40) default NULL,
  LAND varchar(5) default 'D',
  PLZ varchar(10) default NULL,
  ORT varchar(40) default NULL,
  FUNKTION varchar(40) default NULL,
  TELEFON varchar(100) default NULL,
  TELEFAX varchar(100) default NULL,
  MOBILFUNK varchar(100) default NULL,
  TELEPRIVAT varchar(100) default NULL,
  EMAIL varchar(100) default NULL,
  EMAIL2 varchar(100) NOT NULL default '',
  INFO text,
  GEBDATUM date default NULL,
  USERFELD_01 varchar(255) default NULL,
  USERFELD_02 varchar(255) default NULL,
  USERFELD_03 varchar(255) default NULL,
  USERFELD_04 varchar(255) default NULL,
  USERFELD_05 varchar(255) default NULL,
  USERFELD_06 varchar(255) default NULL,
  USERFELD_07 varchar(255) default NULL,
  USERFELD_08 varchar(255) default NULL,
  USERFELD_09 varchar(255) default NULL,
  USERFELD_10 varchar(255) default NULL,
  PRIMARY KEY  (REC_ID),
  KEY NAME (ADDR_ID,NAME,VORNAME)
);

CREATE TABLE ARTIKEL (
  REC_ID int(11) NOT NULL auto_increment,
  ARTNUM varchar(20) default NULL,
  ERSATZ_ARTNUM varchar(20) default NULL,
  MATCHCODE varchar(255) default NULL,
  WARENGRUPPE int(11) NOT NULL default '0',
  RABGRP_ID varchar(10) NOT NULL default '-',
  BARCODE varchar(20) default NULL,
  BARCODE2 varchar(20) default NULL,
  BARCODE3 varchar(20) default NULL,
  ARTIKELTYP char(1) NOT NULL default 'N',
  KURZNAME varchar(80) default NULL,
  LANGNAME text,
  KAS_NAME varchar(80) default NULL,
  INFO text,
  ME_EINHEIT varchar(10) default 'St�ck',
  VPE int(11) NOT NULL default '1',
  PR_EINHEIT float(10,2) NOT NULL default '1.00',
  LAENGE varchar(20) default NULL,
  GROESSE varchar(20) default NULL,
  DIMENSION varchar(20) default NULL,
  GEWICHT float(10,2) NOT NULL default '0.00',
  INVENTUR_WERT float(10,2) NOT NULL default '100.00',
  EK_PREIS float(10,3) NOT NULL default '0.000',
  VK1 float(10,2) NOT NULL default '0.00',
  VK1B float(10,2) NOT NULL default '0.00',
  VK2 float(10,2) NOT NULL default '0.00',
  VK2B float(10,2) NOT NULL default '0.00',
  VK3 float(10,2) NOT NULL default '0.00',
  VK3B float(10,2) NOT NULL default '0.00',
  VK4 float(10,2) NOT NULL default '0.00',
  VK4B float(10,2) NOT NULL default '0.00',
  VK5 float(10,2) NOT NULL default '0.00',
  VK5B float(10,2) NOT NULL default '0.00',
  PROVIS_PROZ float(5,2) NOT NULL default '0.00',
  STEUER_CODE tinyint(4) NOT NULL default '2',
  ALTTEIL_FLAG enum('N','Y') NOT NULL default 'N',
  NO_RABATT_FLAG enum('N','Y') NOT NULL default 'N',
  NO_PROVISION_FLAG enum('N','Y') NOT NULL default 'N',
  NO_BEZEDIT_FLAG enum('N','Y') NOT NULL default 'N',
  NO_EK_FLAG enum('N','Y') NOT NULL default 'N',
  NO_VK_FLAG enum('N','Y') NOT NULL default 'N',
  SN_FLAG enum('N','Y') NOT NULL default 'N',
  AUTODEL_FLAG enum('N','Y') NOT NULL default 'N',
  MENGE_START float(10,2) NOT NULL default '0.00',
  MENGE_AKT float(10,2) NOT NULL default '0.00',
  MENGE_MIN float(10,2) NOT NULL default '0.00',
  MENGE_BESTELLT float(10,2) NOT NULL default '0.00',
  MENGE_BVOR float(10,2) NOT NULL default '0.00',
  MENGE_EKBEST_EDI float(10,2) NOT NULL default '0.00',
  MENGE_VKRE_EDI float(10,2) NOT NULL default '0.00',
  MENGE_EKRE_EDI float(10,2) NOT NULL default '0.00',
  LAST_EK float(10,3) NOT NULL default '0.000',
  LAST_LIEF int(11) NOT NULL default '-1',
  LAST_LIEFDAT date default NULL,
  LAST_VK float(10,2) NOT NULL default '0.00',
  LAST_KUNDE int(11) NOT NULL default '-1',
  LAST_VKDAT date default NULL,
  DEFAULT_LIEF_ID int(11) NOT NULL default '-1',
  ERLOES_KTO int(11) default NULL,
  AUFW_KTO int(11) default NULL,
  HERKUNFSLAND char(3) default NULL,
  HERSTELLER_ID int(11) NOT NULL default '-1',
  HERST_ARTNUM varchar(100) default NULL,
  LAGERORT varchar(20) default NULL,
  ERSTELLT date default NULL,
  ERST_NAME varchar(20) default NULL,
  GEAEND date default NULL,
  GEAEND_NAME varchar(20) default NULL,
  SHOP_ID tinyint(4) NOT NULL default '-1',
  SHOP_ARTIKEL_ID int(11) NOT NULL default '-1',
  SHOP_KURZTEXT text,
  SHOP_LANGTEXT text,
  SHOP_IMAGE_BIG varchar(100) default NULL,
  SHOP_IMAGE_SMALL varchar(100) default NULL,
  SHOP_DATENBLATT varchar(100) default NULL,
  SHOP_KATALOG varchar(100) default NULL,
  SHOP_ZEICHNUNG varchar(100) default NULL,
  SHOP_HANDBUCH varchar(100) default NULL,
  AUSSCHREIBUNGSTEXT varchar(100) default NULL,
  SHOP_PREIS_LISTE float(12,8) default '0.00000000',
  SHOP_VISIBLE int(1) default '1',
  SHOP_DATE_NEU date default NULL,
  SHOP_FAELLT_WEG_AB date default NULL,
  SHOP_CLICK_COUNT int(11) default '0',
  SHOP_SYNC tinyint(1) unsigned default '0',
  SHOP_ZUB tinyint(1) unsigned default '0',
  SHOP_CHANGE_DATE datetime default NULL,
  SHOP_CHANGE_FLAG tinyint(1) unsigned NOT NULL default '0',
  SHOP_DEL_FLAG enum('N','Y') NOT NULL default 'N',
  USERFELD_01 varchar(255) default NULL,
  USERFELD_02 varchar(255) default NULL,
  USERFELD_03 varchar(255) default NULL,
  USERFELD_04 varchar(255) default NULL,
  USERFELD_05 varchar(255) default NULL,
  USERFELD_06 varchar(255) default NULL,
  USERFELD_07 varchar(255) default NULL,
  USERFELD_08 varchar(255) default NULL,
  USERFELD_09 varchar(255) default NULL,
  USERFELD_10 varchar(255) default NULL,
  PRIMARY KEY  (REC_ID),
  KEY IDX_WGR (WARENGRUPPE),
  KEY IDX_MATCHCODE (MATCHCODE),
  KEY IDX_BARCODE (BARCODE),
  KEY IDX_ARTNUM (ARTNUM),
  KEY IDX_HERST_ARTNUM (HERST_ARTNUM)
);

CREATE TABLE ARTIKEL_INVENTUR (
  INVENTUR_ID int(5) unsigned NOT NULL default '0',
  ARTIKEL_ID int(11) unsigned NOT NULL default '0',
  WARENGRUPPE int(11) unsigned default '0',
  ARTNUM varchar(250) default NULL,
  MATCHCODE varchar(250) default NULL,
  BARCODE varchar(250) default NULL,
  KURZTEXT varchar(250) default NULL,
  MENGE_IST float(10,3) NOT NULL default '0.000',
  MENGE_SOLL float(10,3) NOT NULL default '0.000',
  MENGE_DIFF float(10,3) NOT NULL default '0.000',
  INVENTUR_WERT float(10,2) default '0.00',
  EK_PREIS float(10,3) default '0.000',
  STATUS tinyint(1) unsigned NOT NULL default '0',
  BEARBEITETER varchar(50) default NULL,
  PRIMARY KEY  (INVENTUR_ID,ARTIKEL_ID)
) COMMENT='Artikeldaten f�r Inventur';

CREATE TABLE ARTIKEL_KAT (
  SHOP_ID tinyint(3) NOT NULL default '-1',
  ID int(11) NOT NULL auto_increment,
  TOP_ID int(11) NOT NULL default '-1',
  SORT_NUM int(3) unsigned NOT NULL default '0',
  NAME varchar(250) NOT NULL default '',
  URL varchar(250) default NULL,
  BESCHREIBUNG text,
  IMAGE varchar(250) default NULL,
  VISIBLE_FLAG enum('N','Y') NOT NULL default 'Y',
  LIEF_ID int(13) default '-1',
  ARTIKEL_TEMPLATE varchar(250) default 'normal.tpl',
  GRUPPEN_TEMPLATE varchar(250) default 'grp_normal.tpl',
  CLICK_COUNT int(11) default '0',
  LAST_IP varchar(16) default NULL,
  SYNC_FLAG enum('N','Y') NOT NULL default 'N',
  CHANGE_FLAG enum('N','Y') NOT NULL default 'Y',
  DEL_FLAG enum('N','Y') NOT NULL default 'N',
  PRIMARY KEY  (SHOP_ID,ID)
);

CREATE TABLE ARTIKEL_LTEXT (
  ARTIKEL_ID int(11) NOT NULL default '0',
  SPRACHE_ID int(11) NOT NULL default '0',
  KURZNAME varchar(80) default NULL,
  LANGNAME text,
  KAS_NAME varchar(80) default NULL,
  SHOP_KURZTEXT text,
  SHOP_LANGTEXT text,
  SHOP_DATENBLATT varchar(100) default NULL,
  SHOP_KATALOG varchar(100) default NULL,
  SHOP_ZEICHNUNG varchar(100) default NULL,
  SHOP_HANDBUCH varchar(100) default NULL,
  CHANGE_FLAG tinyint(1) unsigned NOT NULL default '0',
  PRIMARY KEY  (ARTIKEL_ID,SPRACHE_ID),
  KEY IDX_ARTIKEL_ID (ARTIKEL_ID),
  KEY IDX_SPRACHE_ID (SPRACHE_ID),
  KEY IDX_ARTIKEL_SPRACHE (ARTIKEL_ID,SPRACHE_ID)
);


CREATE TABLE ARTIKEL_MERK (
  MERKMAL_ID int(11) NOT NULL auto_increment,
  NAME varchar(100) NOT NULL default '',
  PRIMARY KEY  (MERKMAL_ID)
);

CREATE TABLE ARTIKEL_PREIS (
  ARTIKEL_ID int(11) NOT NULL default '-1',
  ADRESS_ID int(11) NOT NULL default '-1',
  PREIS_TYP tinyint(2) unsigned NOT NULL default '0',
  BESTNUM varchar(50) default NULL,
  PREIS float(10,3) NOT NULL default '0.000',
  MENGE2 int(6) unsigned NOT NULL default '0',
  PREIS2 float(10,3) NOT NULL default '0.000',
  MENGE3 int(6) unsigned NOT NULL default '0',
  PREIS3 float(10,3) NOT NULL default '0.000',
  MENGE4 int(6) unsigned NOT NULL default '0',
  PREIS4 float(10,3) NOT NULL default '0.000',
  MENGE5 int(6) unsigned NOT NULL default '0',
  PREIS5 float(10,3) NOT NULL default '0.000',
  GUELTIG_VON date default NULL,
  GUELTIG_BIS date default NULL,
  INFO text,
  GEAEND timestamp(14) NOT NULL,
  GEAEND_NAME varchar(20) default NULL,
  PRIMARY KEY  (ARTIKEL_ID,ADRESS_ID,PREIS_TYP)
);


CREATE TABLE ARTIKEL_SERNUM (
  ARTIKEL_ID int(11) NOT NULL default '0',
  SERNUMMER varchar(255) NOT NULL default '',
  EINK_NUM int(11) NOT NULL default '-1',
  LIEF_NUM int(11) NOT NULL default '-1',
  VERK_NUM int(11) NOT NULL default '-1',
  SNUM_ID int(11) NOT NULL auto_increment,
  EK_JOURNAL_ID int(10) NOT NULL default '-1',
  VK_JOURNAL_ID int(10) NOT NULL default '-1',
  LS_JOURNAL_ID int(10) NOT NULL default '-1',
  EK_JOURNALPOS_ID int(10) NOT NULL default '-1',
  VK_JOURNALPOS_ID int(10) NOT NULL default '-1',
  LS_JOURNALPOS_ID int(10) NOT NULL default '-1',
  PRIMARY KEY  (SNUM_ID),
  UNIQUE KEY SERNUMMER (SERNUMMER,ARTIKEL_ID)
);


CREATE TABLE ARTIKEL_SHOP (
  ID int(11) NOT NULL auto_increment,
  TOP_ID int(11) NOT NULL default '99999',
  ART_NR varchar(50) default NULL,
  ART_NAME varchar(50) default NULL,
  KURZTEXT text,
  LANGTEXT text,
  BESCHREIBUNG text,
  SPEZIFIKATION text,
  ZUBEHOER text,
  ZUBEHOER_OPT text,
  IMAGE_BIG varchar(100) default NULL,
  IMAGE_SMALL varchar(100) default NULL,
  DATASHEET_D varchar(100) default NULL,
  DATASHEET_E varchar(100) default NULL,
  KATALOG_D varchar(100) default NULL,
  KATALOG_E varchar(100) default NULL,
  DRAWING varchar(100) default NULL,
  HANDBUCH_D varchar(100) default NULL,
  HANDBUCH_E varchar(100) default NULL,
  AUSSCHREIBUNGSTEXT varchar(100) default NULL,
  PREIS_EK float(12,2) default NULL,
  PREIS_LISTE float(12,2) default NULL,
  PREIS_SPEC float(12,2) default NULL,
  EK_RABATT float(5,2) default NULL,
  VISIBLE int(1) default '1',
  DATE_NEU date default NULL,
  FAELLT_WEG int(1) default '0',
  FAELLT_WERG_AB date default NULL,
  CLICK_COUNT int(11) default '0',
  SYNC tinyint(1) unsigned default '0',
  LAST_IP varchar(16) default NULL,
  ZUB tinyint(1) unsigned default '0',
  CHANGE_DATE datetime default NULL,
  CHANGE_FLAG tinyint(1) unsigned NOT NULL default '0',
  LIEF_ID int(13) default '-1',
  RABGRP_ID varchar(10) default NULL,
  PRIMARY KEY  (ID)
);

CREATE TABLE ARTIKEL_SHOP_IDX (
  ID int(11) NOT NULL auto_increment,
  TOP_ID int(11) NOT NULL default '99999',
  NAME varchar(250) default NULL,
  URL varchar(250) default NULL,
  ARTIKEL_TEMPLATE varchar(250) default 'normal.tpl',
  CLICK_COUNT int(11) default '0',
  VISIBLE tinyint(1) unsigned default '1',
  GRUPPEN_TEMPLATE varchar(250) default 'grp_normal.tpl',
  BESCHREIBUNG text,
  IMAGE varchar(250) default NULL,
  LAST_IP varchar(16) default NULL,
  SYNC tinyint(1) unsigned default '0',
  LIEF_ID int(13) default '-1',
  PRIMARY KEY  (ID)
);

CREATE TABLE ARTIKEL_STUECKLIST (
  REC_ID int(11) NOT NULL default '0',
  ART_ID int(11) NOT NULL default '0',
  MENGE float(5,2) default NULL,
  ERSTELLT date default NULL,
  ERST_NAME varchar(20) default NULL,
  GEAEND date default NULL,
  GEAEND_NAME varchar(20) default NULL,
  PRIMARY KEY  (REC_ID,ART_ID)
);

CREATE TABLE ARTIKEL_TO_KAT (
  SHOP_ID tinyint(3) NOT NULL default '-1',
  ARTIKEL_ID int(13) NOT NULL default '-1',
  KAT_ID int(13) NOT NULL default '-1',
  CHANGE_FLAG enum('N','Y') NOT NULL default 'Y',
  DEL_FLAG enum('N','Y') NOT NULL default 'N',
  PRIMARY KEY  (SHOP_ID,ARTIKEL_ID,KAT_ID),
  KEY KAT_TO_ARTIKEL (SHOP_ID,KAT_ID,ARTIKEL_ID)
);

CREATE TABLE ARTIKEL_TO_MERK (
  MERKMAL_ID int(11) unsigned NOT NULL default '0',
  ARTIKEL_ID int(11) unsigned NOT NULL default '0',
  PRIMARY KEY  (MERKMAL_ID,ARTIKEL_ID)
);

CREATE TABLE BLZ (
  BLZ int(10) NOT NULL default '0',
  BANK_NAME varchar(255) NOT NULL default '',
  PRZ char(2) NOT NULL default '',
  PRIMARY KEY  (BLZ),
  KEY IDX_NAME (BANK_NAME,BLZ)
);

CREATE TABLE EXPORT (
  ID int(11) NOT NULL auto_increment,
  KURZBEZ varchar(255) NOT NULL default '',
  INFO text NOT NULL,
  QUERY text NOT NULL,
  FELDER text,
  FORMULAR blob,
  FORMAT char(3) default NULL,
  FILENAME varchar(255) default NULL,
  LAST_CHANGE timestamp(14) NOT NULL,
  CHANGE_NAME varchar(100) default NULL,
  PRIMARY KEY  (ID)
);

CREATE TABLE FIBU_BUCHUNGEN (
  LFD_NR int(11) NOT NULL auto_increment,
  JRN_NUM int(11) NOT NULL default '0',
  KTO_NR int(11) NOT NULL default '0',
  GEG_KTO int(11) NOT NULL default '0',
  DATUM date NOT NULL default '0000-00-00',
  BETRAG double NOT NULL default '0',
  MTGL_NR int(11) NOT NULL default '0',
  BELNUM varchar(20) NOT NULL default '',
  WAEHRUNG tinyint(4) default NULL,
  BUCHTEXT varchar(100) default NULL,
  USER varchar(25) NOT NULL default '',
  PRIMARY KEY  (LFD_NR,JRN_NUM,KTO_NR,GEG_KTO,DATUM,BETRAG,MTGL_NR,BELNUM),
  KEY JRN_NUM (JRN_NUM),
  KEY DATUM (DATUM),
  KEY USER (USER),
  KEY KONTO (KTO_NR),
  KEY BELNUM (BELNUM)
);

CREATE TABLE FIBU_JOURNAL (
  LFD_NR int(11) NOT NULL auto_increment,
  DATUM date NOT NULL default '0000-00-00',
  BUDATUM date NOT NULL default '0000-00-00',
  BELNUM varchar(20) NOT NULL default '',
  SKTO int(11) NOT NULL default '0',
  HKTO int(11) NOT NULL default '0',
  BETRAG float(10,2) default NULL,
  WAEHRUNG tinyint(4) default NULL,
  TXT varchar(200) default NULL,
  USER varchar(20) NOT NULL default '',
  PRIMARY KEY  (LFD_NR,DATUM,BUDATUM,BELNUM,SKTO,HKTO),
  KEY USER (USER),
  KEY BUDATUM (BUDATUM),
  KEY DATUM (DATUM)
);

CREATE TABLE FIBU_KASSE (
  REC_ID int(11) NOT NULL auto_increment,
  JAHR int(11) NOT NULL default '0',
  BDATUM date NOT NULL default '0000-00-00',
  BELEGNUM varchar(10) default NULL,
  QUELLE int(11) NOT NULL default '0',
  JOURNAL_ID int(11) default NULL,
  GKONTO int(11) default '-1',
  SKONTO float(5,3) default '0.000',
  ZU_ABGANG float(12,2) NOT NULL default '0.00',
  BTXT text,
  PRIMARY KEY  (JAHR,BDATUM,REC_ID)
);

CREATE TABLE FIBU_KONTEN (
  KTO_NR int(11) NOT NULL default '0',
  KTO_NAME varchar(100) NOT NULL default '',
  MTGL_NR int(11) NOT NULL default '0',
  KTO_KLASSE int(11) default NULL,
  ZE_VIEW tinyint(4) default NULL,
  ZA_VIEW tinyint(4) default NULL,
  EK_VIEW tinyint(4) default NULL,
  VK_VIEW tinyint(4) default NULL,
  FI_VIEW tinyint(4) default NULL,
  SALDO float(10,2) default NULL,
  PRIMARY KEY  (KTO_NR,KTO_NAME,MTGL_NR),
  KEY KTO (KTO_NR),
  KEY NAME (KTO_NAME)
);

CREATE TABLE FIBU_OPOS (
  LFD_NR int(11) NOT NULL auto_increment,
  MTGL_NR int(11) NOT NULL default '0',
  SDATUM date NOT NULL default '0000-00-00',
  HDATUM date NOT NULL default '0000-00-00',
  ZAHLART tinyint(4) NOT NULL default '0',
  SBETRAG float(10,2) NOT NULL default '0.00',
  HBETRAG float(10,2) NOT NULL default '0.00',
  ERLED tinyint(4) default NULL,
  WAEHRUNG tinyint(4) default NULL,
  TXT varchar(100) default NULL,
  USERNAME varchar(20) default NULL,
  PRIMARY KEY  (LFD_NR,MTGL_NR,SDATUM,HDATUM,ZAHLART,SBETRAG,HBETRAG),
  KEY MTGL_NR (MTGL_NR)
);

CREATE TABLE FIRMA (
  ANREDE varchar(250) default NULL,
  NAME1 varchar(250) default NULL,
  NAME2 varchar(250) default NULL,
  NAME3 varchar(250) default NULL,
  STRASSE varchar(250) default NULL,
  LAND varchar(10) default NULL,
  PLZ varchar(10) default NULL,
  ORT varchar(250) default NULL,
  VORWAHL varchar(250) default NULL,
  TELEFON1 varchar(250) default NULL,
  TELEFON2 varchar(250) default NULL,
  MOBILFUNK varchar(250) default NULL,
  FAX varchar(250) default NULL,
  EMAIL varchar(250) default NULL,
  WEBSEITE varchar(250) default NULL,
  BANK1_BLZ varchar(8) default NULL,
  BANK1_KONTONR varchar(12) default NULL,
  BANK1_NAME varchar(250) default NULL,
  BANK1_IBAN varchar(100) default NULL,
  BANK1_SWIFT varchar(100) default NULL,
  BANK2_BLZ varchar(8) default NULL,
  BANK2_KONTONR varchar(12) default NULL,
  BANK2_NAME varchar(250) default NULL,
  BANK2_IBAN varchar(100) default NULL,
  BANK2_SWIFT varchar(100) default NULL,
  KOPFTEXT text,
  FUSSTEXT text,
  ABSENDER varchar(250) default NULL,
  STEUERNUMMER varchar(25) default NULL,
  UST_ID varchar(25) default NULL,
  IMAGE1 blob,
  IMAGE2 blob,
  IMAGE3 blob
) COMMENT='Allgemeine Firmendaten f�r Formulare';

CREATE TABLE HERSTELLER (
  SHOP_ID tinyint(3) NOT NULL default '-1',
  HERSTELLER_ID int(11) NOT NULL auto_increment,
  ADDR_ID int(11) NOT NULL default '-1',
  HERSTELLER_NAME varchar(32) NOT NULL default '',
  HERSTELLER_IMAGE varchar(64) default NULL,
  LAST_CHANGE datetime default NULL,
  SHOP_DATE_ADDED datetime default NULL,
  SHOP_DATE_CHANGE datetime default NULL,
  SYNC_FLAG enum('N','Y') NOT NULL default 'N',
  CHANGE_FLAG enum('N','Y') NOT NULL default 'N',
  DEL_FLAG enum('N','Y') NOT NULL default 'N',
  PRIMARY KEY  (SHOP_ID,HERSTELLER_ID),
  KEY IDX_HERSTELLER_NAME (HERSTELLER_NAME)
);

CREATE TABLE HERSTELLER_INFO (
  SHOP_ID tinyint(3) NOT NULL default '-1',
  HERSTELLER_ID int(11) NOT NULL default '0',
  SPRACHE_ID int(11) NOT NULL default '0',
  HERSTELLER_URL varchar(255) NOT NULL default '',
  URL_CLICKED int(5) NOT NULL default '0',
  DATE_LAST_CLICK datetime default NULL,
  PRIMARY KEY  (SHOP_ID,HERSTELLER_ID,SPRACHE_ID)
);

CREATE TABLE INFO (
  LFD_NR int(11) NOT NULL auto_increment,
  QUELLE tinyint(4) NOT NULL default '0',
  QUELL_ID int(11) NOT NULL default '-1',
  DATUM date NOT NULL default '0000-00-00',
  WIEDERVORLAGE tinyint(4) default NULL,
  WV_DATUM date default NULL,
  ERLED tinyint(4) default NULL,
  ERST_VON varchar(20) default NULL,
  KURZTEXT varchar(100) default NULL,
  MEMO text,
  PRIMARY KEY  (LFD_NR,QUELLE,DATUM)
);

CREATE TABLE INVENTUR (
  ID int(5) NOT NULL auto_increment,
  DATUM date NOT NULL default '0000-00-00',
  BESCHREIBUNG varchar(250) default NULL,
  INFO text,
  STATUS tinyint(3) unsigned NOT NULL default '0',
  PRIMARY KEY  (ID)
) COMMENT='Kopfdaten f�r Inventuren';

CREATE TABLE JOURNAL (
  TERM_ID int(11) unsigned NOT NULL default '1',
  QUELLE tinyint(4) NOT NULL default '0',
  JAHR int(11) NOT NULL default '0',
  REC_ID int(11) NOT NULL auto_increment,
  QUELLE_SUB tinyint(4) default NULL,
  ADDR_ID int(11) NOT NULL default '-1',
  LIEF_ADDR_ID int(11) NOT NULL default '-1',
  ATRNUM int(11) NOT NULL default '-1',
  VRENUM int(11) NOT NULL default '-1',
  VLSNUM int(11) NOT NULL default '-1',
  FOLGENR int(11) NOT NULL default '-1',
  KM_STAND int(11) default NULL,
  KFZ_ID int(11) NOT NULL default '-1',
  VERTRETER_ID int(11) NOT NULL default '-1',
  GLOBRABATT float(10,2) NOT NULL default '0.00',
  ADATUM date default NULL,
  RDATUM date NOT NULL default '0000-00-00',
  LDATUM date default NULL,
  WVORLAGE tinyint(4) default NULL,
  WV_DATUM date default NULL,
  PR_EBENE tinyint(4) default NULL,
  LIEFART tinyint(2) NOT NULL default '-1',
  ZAHLART tinyint(2) NOT NULL default '-1',
  KOST_NETTO float(10,2) NOT NULL default '0.00',
  WERT_NETTO float(10,2) NOT NULL default '0.00',
  LOHN float(10,2) NOT NULL default '0.00',
  WARE float(10,2) NOT NULL default '0.00',
  TKOST float(10,2) NOT NULL default '0.00',
  MWST_0 float(10,2) NOT NULL default '0.00',
  MWST_1 float(10,2) NOT NULL default '0.00',
  MWST_2 float(10,2) NOT NULL default '0.00',
  MWST_3 float(10,2) NOT NULL default '0.00',
  NSUMME float(10,2) NOT NULL default '0.00',
  MSUMME_0 float(10,2) NOT NULL default '0.00',
  MSUMME_1 float(10,2) NOT NULL default '0.00',
  MSUMME_2 float(10,2) NOT NULL default '0.00',
  MSUMME_3 float(10,2) NOT NULL default '0.00',
  MSUMME float(10,2) NOT NULL default '0.00',
  BSUMME float(10,2) NOT NULL default '0.00',
  ATSUMME float(10,2) NOT NULL default '0.00',
  ATMSUMME float(10,2) NOT NULL default '0.00',
  PROVIS_WERT float(10,2) NOT NULL default '0.00',
  WAEHRUNG varchar(5) NOT NULL default '',
  GEGENKONTO int(11) NOT NULL default '-1',
  SOLL_STAGE tinyint(4) NOT NULL default '0',
  SOLL_SKONTO float(5,2) NOT NULL default '0.00',
  SOLL_NTAGE tinyint(4) NOT NULL default '0',
  SOLL_RATEN tinyint(4) NOT NULL default '1',
  SOLL_RATBETR float(10,2) NOT NULL default '0.00',
  SOLL_RATINTERVALL int(11) NOT NULL default '0',
  IST_ANZAHLUNG float(10,2) default NULL,
  IST_SKONTO float(10,2) default NULL,
  IST_ZAHLDAT date default NULL,
  IST_BETRAG float(10,2) NOT NULL default '0.00',
  MAHNKOSTEN float(10,2) NOT NULL default '0.00',
  KONTOAUSZUG int(11) default NULL,
  BANK_ID int(11) NOT NULL default '-1',
  STADIUM tinyint(4) NOT NULL default '0',
  ERSTELLT date default NULL,
  ERST_NAME varchar(20) default NULL,
  KUN_NUM varchar(20) default NULL,
  KUN_ANREDE varchar(40) default NULL,
  KUN_NAME1 varchar(40) default NULL,
  KUN_NAME2 varchar(40) default NULL,
  KUN_NAME3 varchar(40) default NULL,
  KUN_ABTEILUNG varchar(40) default NULL,
  KUN_STRASSE varchar(40) default NULL,
  KUN_LAND varchar(5) default NULL,
  KUN_PLZ varchar(10) default NULL,
  KUN_ORT varchar(40) default NULL,
  USR1 varchar(80) default NULL,
  USR2 varchar(80) default NULL,
  PROJEKT varchar(80) default NULL,
  ORGNUM varchar(20) default NULL,
  BEST_NAME varchar(40) default NULL,
  BEST_CODE tinyint(4) default NULL,
  BEST_DATUM date default NULL,
  INFO text,
  UW_NUM int(11) NOT NULL default '-1',
  MAHNSTUFE tinyint(1) NOT NULL default '0',
  MAHNDATUM date default NULL,
  MAHNPRINT tinyint(1) NOT NULL default '0',
  FREIGABE1_FLAG enum('N','Y') NOT NULL default 'N',
  FREIGABE2_FLAG enum('N','Y') NOT NULL default 'N',
  BRUTTO_FLAG enum('N','Y') NOT NULL default 'N',
  MWST_FREI_FLAG enum('N','Y') NOT NULL default 'N',
  PROVIS_BERECHNET enum('N','Y') NOT NULL default 'N',
  SHOP_ID tinyint(3) NOT NULL default '-1',
  SHOP_ORDERID int(11) NOT NULL default '-1',
  SHOP_STATUS tinyint(3) unsigned NOT NULL default '0',
  SHOP_CHANGE_FLAG enum('N','Y') NOT NULL default 'N',
  PRIMARY KEY  (REC_ID),
  KEY RJ_RENUM (QUELLE,JAHR,VRENUM),
  KEY ADDR_ID (ADDR_ID,JAHR,RDATUM)
);

CREATE TABLE JOURNALPOS (
  REC_ID int(11) NOT NULL auto_increment,
  QUELLE tinyint(4) NOT NULL default '0',
  QUELLE_SUB tinyint(4) NOT NULL default '0',
  QUELLE_SRC int(11) NOT NULL default '-1',
  JAHR int(11) NOT NULL default '0',
  JOURNAL_ID int(11) NOT NULL default '0',
  ARTIKELTYP char(1) NOT NULL default '',
  ARTIKEL_ID int(11) NOT NULL default '-1',
  TOP_POS_ID int(11) NOT NULL default '-1',
  ADDR_ID int(11) NOT NULL default '-1',
  ATRNUM int(11) NOT NULL default '-1',
  VRENUM int(11) NOT NULL default '-1',
  VLSNUM int(11) NOT NULL default '-1',
  POSITION int(11) NOT NULL default '0',
  VIEW_POS char(3) default NULL,
  MATCHCODE varchar(20) default NULL,
  ARTNUM varchar(20) default NULL,
  BARCODE varchar(20) default NULL,
  MENGE float(10,2) NOT NULL default '0.00',
  LAENGE varchar(20) default NULL,
  GROESSE varchar(20) default NULL,
  DIMENSION varchar(20) default NULL,
  GEWICHT float(10,3) NOT NULL default '0.000',
  ME_EINHEIT varchar(10) default NULL,
  PR_EINHEIT float(10,3) NOT NULL default '1.000',
  VPE int(11) unsigned NOT NULL default '1',
  EPREIS float(10,3) NOT NULL default '0.000',
  E_RGEWINN float(10,2) NOT NULL default '0.00',
  RABATT float(10,2) NOT NULL default '0.00',
  RABATT2 float(10,2) NOT NULL default '0.00',
  RABATT3 float(10,2) NOT NULL default '0.00',
  STEUER_CODE tinyint(4) NOT NULL default '0',
  ALTTEIL_PROZ float(10,2) NOT NULL default '0.10',
  ALTTEIL_STCODE tinyint(4) NOT NULL default '0',
  PROVIS_PROZ float(5,2) NOT NULL default '0.00',
  PROVIS_WERT float(10,2) NOT NULL default '0.00',
  GEBUCHT enum('N','Y') NOT NULL default 'N',
  GEGENKTO int(11) NOT NULL default '-1',
  BEZEICHNUNG text,
  SN_FLAG enum('N','Y') NOT NULL default 'N',
  ALTTEIL_FLAG enum('N','Y') NOT NULL default 'N',
  BEZ_FEST_FLAG enum('N','Y') NOT NULL default 'N',
  BRUTTO_FLAG enum('N','Y') NOT NULL default 'N',
  PRIMARY KEY  (REC_ID),
  KEY ARTIKEL_ID (ARTIKEL_ID,JAHR),
  KEY ADDR_ID (ADDR_ID,JAHR),
  KEY JOURNAL_ID (JOURNAL_ID,POSITION),
  KEY QUELLE_SRC (QUELLE_SRC)
);

CREATE TABLE KFZ (
  KFZ_ID int(11) NOT NULL auto_increment,
  ADDR_ID int(11) NOT NULL default '0',
  FGST_NUM varchar(20) NOT NULL default '',
  KFZ_GRUPPE tinyint(4) default NULL,
  POL_KENNZ varchar(10) NOT NULL default '',
  SCHL_ZU_2 varchar(20) default NULL,
  SCHL_ZU_3 varchar(20) default NULL,
  TYP_ID int(11) default NULL,
  TYP varchar(10) default NULL,
  AUSFUER varchar(10) default NULL,
  ART_ID int(11) default NULL,
  FABRIKAT_ID int(11) default NULL,
  KRAFTSTOFF_ID int(11) default NULL,
  GRUPPE int(11) default NULL,
  SCHLUES_NR varchar(10) default NULL,
  ZSCHL_NR varchar(10) default NULL,
  MOTOR_NR varchar(20) default NULL,
  KFZBRI_NR varchar(15) default NULL,
  MOTOR varchar(15) default NULL,
  GETRIEBE varchar(10) default NULL,
  KW int(11) default NULL,
  PS int(11) default NULL,
  KM_STAND int(11) default NULL,
  HUBRAUM int(11) default NULL,
  REIFEN varchar(10) default NULL,
  REIF_GR varchar(10) default NULL,
  FARBE varchar(10) default NULL,
  POLSTER varchar(10) default NULL,
  ZULASSUNG date default NULL,
  HERSTELLUNG date default NULL,
  KAUFDATUM date default NULL,
  LE_BESUCH date default NULL,
  NAE_TUEV date default NULL,
  NAE_AU date default NULL,
  NAE_SP date default NULL,
  NAE_TP date default NULL,
  EK_PREIS double default NULL,
  RUESTK double default NULL,
  VK_NETTO double default NULL,
  MWST_PROZ double default NULL,
  UMSATZ_GES double default NULL,
  UMSATZ_GAR double default NULL,
  VERTRETER_ID int(11) NOT NULL default '-1',
  INFO text,
  WKST_INFO text,
  USERFELD_01 varchar(255) default NULL,
  USERFELD_02 varchar(255) default NULL,
  USERFELD_03 varchar(255) default NULL,
  USERFELD_04 varchar(255) default NULL,
  USERFELD_05 varchar(255) default NULL,
  USERFELD_06 varchar(255) default NULL,
  USERFELD_07 varchar(255) default NULL,
  USERFELD_08 varchar(255) default NULL,
  USERFELD_09 varchar(255) default NULL,
  USERFELD_10 varchar(255) default NULL,
  PRIMARY KEY  (KFZ_ID),
  KEY KUNNUM (ADDR_ID),
  KEY KENNZ (POL_KENNZ),
  KEY FGST_NR (FGST_NUM)
);

CREATE TABLE LAND (
  ID char(2) NOT NULL default '',
  NAME varchar(100) NOT NULL default '',
  ISO_CODE_3 char(3) NOT NULL default '',
  FORMAT tinyint(3) NOT NULL default '1',
  VORWAHL varchar(10) default NULL,
  WAEHRUNG varchar(5) default NULL,
  SPRACHE char(3) default NULL,
  PRIMARY KEY  (ID),
  UNIQUE KEY IDX_NAME (NAME)
);

CREATE TABLE LINK (
  MODUL_ID tinyint(3) NOT NULL default '-1',
  REC_ID int(11) NOT NULL default '-1',
  PFAD varchar(255) NOT NULL default '',
  DATEI varchar(200) NOT NULL default '',
  BEMERKUNG text,
  LAST_CHANGE timestamp(14) NOT NULL,
  LAST_CHANGE_USER varchar(50) NOT NULL default '',
  OPEN_FLAG enum('N','Y') NOT NULL default 'N',
  OPEN_USER varchar(50) NOT NULL default '',
  OPEN_TIME timestamp(14) NOT NULL,
  PRIMARY KEY  (MODUL_ID,REC_ID,PFAD,DATEI)
);

CREATE TABLE MITARBEITER (
  MA_ID int(11) unsigned NOT NULL auto_increment,
  MA_NUMMER varchar(10) default NULL,
  NAME varchar(100) default NULL,
  VNAME varchar(100) default NULL,
  LOGIN_NAME varchar(50) NOT NULL default '',
  ANZEIGE_NAME varchar(50) NOT NULL default '',
  ANREDE varchar(15) default NULL,
  TITEL varchar(15) default NULL,
  ZUSATZ varchar(40) default NULL,
  ZUSATZ2 varchar(40) default NULL,
  ZUHAENDEN varchar(40) default NULL,
  STRASSE varchar(40) default NULL,
  LAND varchar(5) default NULL,
  PLZ varchar(10) default NULL,
  ORT varchar(40) default NULL,
  TELEFON varchar(100) default NULL,
  FAX varchar(100) default NULL,
  FUNK varchar(100) default NULL,
  EMAIL varchar(100) default NULL,
  INTERNET varchar(100) default NULL,
  SPRACH_ID smallint(6) default '2',
  BESCHAEFTIGUNGSART smallint(6) default NULL,
  BESCHAEFTIGUNGSGRAD smallint(6) default NULL,
  JAHRESURLAUB float default NULL,
  GUELTIG_VON datetime default NULL,
  GUELTIG_BIS datetime default NULL,
  GEBDATUM datetime default NULL,
  GESCHLECHT enum('M','W') NOT NULL default 'M',
  FAMSTAND smallint(6) default NULL,
  BANK varchar(40) default NULL,
  BLZ varchar(10) default NULL,
  KTO varchar(20) default NULL,
  BEMERKUNG text,
  ERSTELLT datetime default NULL,
  ERSTELLT_NAME varchar(20) default NULL,
  GEAEND datetime default NULL,
  GEAEND_NAME varchar(20) default NULL,
  USERFELD_01 varchar(255) default NULL,
  USERFELD_02 varchar(255) default NULL,
  USERFELD_03 varchar(255) default NULL,
  USERFELD_04 varchar(255) default NULL,
  USERFELD_05 varchar(255) default NULL,
  USERFELD_06 varchar(255) default NULL,
  USERFELD_07 varchar(255) default NULL,
  USERFELD_08 varchar(255) default NULL,
  USERFELD_09 varchar(255) default NULL,
  USERFELD_10 varchar(255) default NULL,
  PRIMARY KEY  (MA_ID),
  UNIQUE KEY IDX_MA_NUM (MA_NUMMER),
  UNIQUE KEY IDX_LOGINNAME (LOGIN_NAME)
);

CREATE TABLE PIM_AUFGABEN (
  RECORDID int(11) NOT NULL auto_increment,
  RESOURCEID int(11) NOT NULL default '0',
  COMPLETE enum('N','Y') default 'N',
  DESCRIPTION char(255) default NULL,
  DETAILS char(255) default NULL,
  CREATEDON DATETIME,
  PRIORITY int(11) default NULL,
  CATEGORY int(11) default NULL,
  COMPLETEDON DATETIME,
  DUEDATE DATETIME,
  USERFIELD0 char(100) default NULL,
  USERFIELD1 char(100) default NULL,
  USERFIELD2 char(100) default NULL,
  USERFIELD3 char(100) default NULL,
  USERFIELD4 char(100) default NULL,
  USERFIELD5 char(100) default NULL,
  USERFIELD6 char(100) default NULL,
  USERFIELD7 char(100) default NULL,
  USERFIELD8 char(100) default NULL,
  USERFIELD9 char(100) default NULL,
  PRIMARY KEY  (RECORDID),
  KEY ResID_ndx (RESOURCEID),
  KEY DueDate (DUEDATE),
  KEY CompletedOn (COMPLETEDON)
);

CREATE TABLE PIM_KONTAKTE (
  RECORDID int(11) NOT NULL auto_increment,
  RESOURCEID int(11) NOT NULL default '0',
  FIRSTNAME char(50) default NULL,
  LASTNAME char(50) NOT NULL default '',
  BIRTHDATE date default NULL,
  ANNIVERSARY date default NULL,
  TITLE char(50) default NULL,
  COMPANY char(50) NOT NULL default '',
  JOB_POSITION char(30) default NULL,
  ADDRESS char(100) default NULL,
  CITY char(50) default NULL,
  STATE char(25) default NULL,
  ZIP char(10) default NULL,
  COUNTRY char(25) default NULL,
  NOTE char(255) default NULL,
  PHONE1 char(25) default NULL,
  PHONE2 char(25) default NULL,
  PHONE3 char(25) default NULL,
  PHONE4 char(25) default NULL,
  PHONE5 char(25) default NULL,
  PHONETYPE1 int(11) default NULL,
  PHONETYPE2 int(11) default NULL,
  PHONETYPE3 int(11) default NULL,
  PHONETYPE4 int(11) default NULL,
  PHONETYPE5 int(11) default NULL,
  CATEGORY int(11) default NULL,
  EMAIL char(100) default NULL,
  CUSTOM1 char(100) default NULL,
  CUSTOM2 char(100) default NULL,
  CUSTOM3 char(100) default NULL,
  CUSTOM4 char(100) default NULL,
  USERFIELD0 char(100) default NULL,
  USERFIELD1 char(100) default NULL,
  USERFIELD2 char(100) default NULL,
  USERFIELD3 char(100) default NULL,
  USERFIELD4 char(100) default NULL,
  USERFIELD5 char(100) default NULL,
  USERFIELD6 char(100) default NULL,
  USERFIELD7 char(100) default NULL,
  USERFIELD8 char(100) default NULL,
  USERFIELD9 char(100) default NULL,
  PRIMARY KEY  (RECORDID),
  KEY ResID_ndx (RESOURCEID),
  KEY LName_ndx (LASTNAME),
  KEY Company_ndx (COMPANY)
);

CREATE TABLE PIM_TERMINE (
  RECORDID int(11) NOT NULL auto_increment,
  STARTTIME DATETIME,
  ENDTIME DATETIME,
  RESOURCEID int(11) NOT NULL default '0',
  DESCRIPTION char(255) default NULL,
  NOTES char(255) default NULL,
  CATEGORY int(11) default NULL,
  ALLDAYEVENT enum('N','Y') NOT NULL default 'N',
  DINGPATH char(255) default NULL,
  ALARMSET enum('N','Y') NOT NULL default 'N',
  ALARMADVANCE int(11) default NULL,
  ALARMADVANCETYPE int(11) default NULL,
  SNOOZETIME DATETIME,
  REPEATCODE int(11) default NULL,
  REPEATRANGEEND DATETIME,
  CUSTOMINTERVAL int(11) default NULL,
  USERFIELD0 char(100) default NULL,
  USERFIELD1 char(100) default NULL,
  USERFIELD2 char(100) default NULL,
  USERFIELD3 char(100) default NULL,
  USERFIELD4 char(100) default NULL,
  USERFIELD5 char(100) default NULL,
  USERFIELD6 char(100) default NULL,
  USERFIELD7 char(100) default NULL,
  USERFIELD8 char(100) default NULL,
  USERFIELD9 char(100) default NULL,
  PRIMARY KEY  (RECORDID),
  KEY rid_st_ndx (RESOURCEID,STARTTIME),
  KEY st_ndx (STARTTIME),
  KEY et_ndx (ENDTIME),
  KEY ResID_ndx (RESOURCEID)
);

CREATE TABLE PLZ (
  LAND char(3) NOT NULL default 'D',
  PLZ varchar(11) NOT NULL default '',
  NAME varchar(50) NOT NULL default '',
  VORWAHL varchar(12) default NULL,
  BUNDESLAND char(3) default NULL,
  PRIMARY KEY  (LAND,PLZ,NAME),
  KEY IDX_NAME (LAND,NAME),
  KEY IDX_VORWAHL (LAND,VORWAHL),
  KEY IDX_LAND (LAND)
);

CREATE TABLE RABATTGRUPPEN (
  RABGRP_ID varchar(10) NOT NULL default '',
  RABGRP_TYP tinyint(3) NOT NULL default '0',
  MIN_MENGE int(11) NOT NULL default '1',
  LIEF_RABGRP int(10) NOT NULL default '0',
  RABATT1 float(6,2) NOT NULL default '0.00',
  RABATT2 float(6,2) NOT NULL default '0.00',
  RABATT3 float(6,2) NOT NULL default '0.00',
  ADDR_ID int(11) NOT NULL default '-1',
  BESCHREIBUNG varchar(250) default NULL,
  PRIMARY KEY  (RABGRP_ID,RABGRP_TYP,LIEF_RABGRP,MIN_MENGE)
);

CREATE TABLE REGISTERY (
  MAINKEY varchar(255) NOT NULL default '',
  NAME varchar(100) NOT NULL default '',
  VAL_CHAR varchar(255) default NULL,
  VAL_DATE datetime default NULL,
  VAL_INT int(11) default NULL,
  VAL_INT2 bigint(20) default NULL,
  VAL_INT3 bigint(20) default NULL,
  VAL_DOUBLE double default NULL,
  VAL_BLOB longtext,
  VAL_BIN longblob,
  VAL_TYP smallint(6) default NULL,
  CACHABLE enum('N','Y') NOT NULL default 'N',
  READONLY enum('N','Y') NOT NULL default 'N',
  LAST_CHANGE timestamp(14) NOT NULL,
  PRIMARY KEY  (MAINKEY,NAME)
);

CREATE TABLE SCHRIFTVERKEHR (
  ID int(11) NOT NULL auto_increment,
  ADDR_ID int(11) default '-1',
  BESCHREIBUNG varchar(250) default NULL,
  LANG_TEXT text,
  CHANGE_LAST timestamp(14) NOT NULL,
  CHANGE_USER varchar(100) default NULL,
  PRIMARY KEY  (ID)
);

CREATE TABLE SPRACHEN (
  SPRACH_ID int(11) NOT NULL auto_increment,
  NAME varchar(32) NOT NULL default '',
  CODE char(2) NOT NULL default '',
  SORT int(3) default NULL,
  DEFAULT_FLAG enum('N','Y') NOT NULL default 'N',
  PRIMARY KEY  (SPRACH_ID),
  KEY IDX_NAME (NAME)
);

CREATE TABLE UEBERWEISUNGEN (
  ID int(11) NOT NULL auto_increment,
  UWNUM int(11) NOT NULL default '0',
  ART enum('U','L') NOT NULL default 'U',
  FERTIG tinyint(1) default '0',
  JOURNAL_ID int(11) NOT NULL default '0',
  UW_DATUM date default NULL,
  BETRAG float(10,2) default '0.00',
  KTO varchar(20) default NULL,
  BLZ varchar(8) default NULL,
  BINHABER varchar(50) default NULL,
  UW_TEXT varchar(250) default NULL,
  PRIMARY KEY  (ID,UWNUM)
);

CREATE TABLE VERTRAG (
  REC_ID int(11) NOT NULL auto_increment,
  ADDR_ID int(11) NOT NULL default '-1',
  VVTNUM int(11) NOT NULL default '-1',
  VERTRETER_ID int(11) NOT NULL default '-1',
  GLOBRABATT float(10,2) NOT NULL default '0.00',
  DATUM_START date NOT NULL default '0000-00-00',
  DATUM_ENDE date NOT NULL default '0000-00-00',
  DATUM_NEXT date NOT NULL default '0000-00-00',
  INTERVALL char(1) NOT NULL default 'M',
  INTERVALL_NUM tinyint(3) unsigned NOT NULL default '1',
  AKTIV_FLAG enum('N','Y') NOT NULL default 'N',
  PRINT_FLAG enum('N','Y') NOT NULL default 'N',
  PR_EBENE tinyint(4) default NULL,
  LIEFART tinyint(2) default NULL,
  ZAHLART tinyint(2) default NULL,
  KOST_NETTO float(10,2) NOT NULL default '0.00',
  WERT_NETTO float(10,2) NOT NULL default '0.00',
  LOHN float(10,2) NOT NULL default '0.00',
  WARE float(10,2) NOT NULL default '0.00',
  TKOST float(10,2) NOT NULL default '0.00',
  MWST_0 float(10,2) NOT NULL default '0.00',
  MWST_1 float(10,2) NOT NULL default '0.00',
  MWST_2 float(10,2) NOT NULL default '0.00',
  MWST_3 float(10,2) NOT NULL default '0.00',
  NSUMME float(10,2) NOT NULL default '0.00',
  MSUMME_0 float(10,2) NOT NULL default '0.00',
  MSUMME_1 float(10,2) NOT NULL default '0.00',
  MSUMME_2 float(10,2) NOT NULL default '0.00',
  MSUMME_3 float(10,2) NOT NULL default '0.00',
  MSUMME float(10,2) NOT NULL default '0.00',
  BSUMME float(10,2) NOT NULL default '0.00',
  ATSUMME float(10,2) NOT NULL default '0.00',
  ATMSUMME float(10,2) NOT NULL default '0.00',
  WAEHRUNG varchar(5) NOT NULL default '',
  GEGENKONTO int(11) NOT NULL default '-1',
  SOLL_STAGE tinyint(4) NOT NULL default '0',
  SOLL_SKONTO float(5,2) NOT NULL default '0.00',
  SOLL_NTAGE tinyint(4) NOT NULL default '0',
  SOLL_RATEN tinyint(4) NOT NULL default '1',
  SOLL_RATBETR float(10,2) NOT NULL default '0.00',
  SOLL_RATINTERVALL int(11) NOT NULL default '0',
  STADIUM tinyint(4) NOT NULL default '0',
  ERSTELLT date default NULL,
  ERST_NAME varchar(20) default NULL,
  KUN_NUM varchar(20) default NULL,
  KUN_ANREDE varchar(40) default NULL,
  KUN_NAME1 varchar(40) default NULL,
  KUN_NAME2 varchar(40) default NULL,
  KUN_NAME3 varchar(40) default NULL,
  KUN_ABTEILUNG varchar(40) default NULL,
  KUN_STRASSE varchar(40) default NULL,
  KUN_LAND varchar(5) default NULL,
  KUN_PLZ varchar(10) default NULL,
  KUN_ORT varchar(40) default NULL,
  USR1 varchar(80) default NULL,
  USR2 varchar(80) default NULL,
  PROJEKT varchar(80) default NULL,
  ORGNUM varchar(20) default NULL,
  BEST_NAME varchar(40) default NULL,
  BEST_CODE tinyint(4) default NULL,
  BEST_DATUM date default NULL,
  INFO text,
  BRUTTO_FLAG enum('N','Y') NOT NULL default 'N',
  MWST_FREI_FLAG enum('N','Y') NOT NULL default 'N',
  PRIMARY KEY  (REC_ID),
  KEY ADDR_ID (ADDR_ID)
);

CREATE TABLE VERTRAGPOS (
  REC_ID int(11) NOT NULL auto_increment,
  JOURNAL_ID int(11) NOT NULL default '0',
  ARTIKELTYP char(1) NOT NULL default '',
  ARTIKEL_ID int(11) NOT NULL default '-1',
  ADDR_ID int(11) NOT NULL default '-1',
  VVTNUM int(11) NOT NULL default '-1',
  POSITION int(11) NOT NULL default '0',
  VIEW_POS char(3) default NULL,
  MATCHCODE varchar(20) default NULL,
  ARTNUM varchar(20) default NULL,
  BARCODE varchar(20) default NULL,
  MENGE float(10,2) NOT NULL default '0.00',
  LAENGE varchar(20) default NULL,
  GROESSE varchar(20) default NULL,
  DIMENSION varchar(20) default NULL,
  GEWICHT float(10,3) NOT NULL default '0.000',
  ME_EINHEIT varchar(10) default NULL,
  PR_EINHEIT float(10,3) NOT NULL default '1.000',
  EPREIS float(10,3) NOT NULL default '0.000',
  E_RGEWINN float(10,2) NOT NULL default '0.00',
  RABATT float(10,2) NOT NULL default '0.00',
  RABATT2 float(10,2) NOT NULL default '0.00',
  RABATT3 float(10,2) NOT NULL default '0.00',
  STEUER_CODE tinyint(4) NOT NULL default '0',
  ALTTEIL_PROZ float(10,2) NOT NULL default '0.10',
  ALTTEIL_STCODE tinyint(4) NOT NULL default '0',
  GEGENKTO int(11) NOT NULL default '-1',
  BEZEICHNUNG text,
  ALTTEIL_FLAG enum('N','Y') NOT NULL default 'N',
  BEZ_FEST_FLAG enum('N','Y') NOT NULL default 'N',
  BRUTTO_FLAG enum('N','Y') NOT NULL default 'N',
  PRIMARY KEY  (REC_ID),
  KEY ARTIKEL_ID (ARTIKEL_ID),
  KEY ADDR_ID (ADDR_ID),
  KEY JOURNAL_ID (JOURNAL_ID,POSITION)
);

CREATE TABLE VERTRETER (
  VERTRETER_ID int(11) NOT NULL auto_increment,
  VERTR_NUMMER varchar(10) default NULL,
  VNAME varchar(30) default NULL,
  NAME varchar(30) default NULL,
  DatumNeu datetime default NULL,
  DatumBearbeiten datetime default NULL,
  BenutzerNeu varchar(50) default NULL,
  BenutzerBearbeiten varchar(50) default NULL,
  ANREDE varchar(15) default NULL,
  TITEL varchar(15) default NULL,
  ZUSATZ varchar(40) default NULL,
  ZUSATZ2 varchar(40) default NULL,
  ZUHAENDEN varchar(40) default NULL,
  STRASSE varchar(40) default NULL,
  LAND varchar(5) default NULL,
  PLZ varchar(10) default NULL,
  ORT varchar(40) default NULL,
  TELEFON varchar(100) default NULL,
  FAX varchar(100) default NULL,
  FUNK varchar(100) default NULL,
  EMAIL varchar(100) default NULL,
  INTERNET varchar(100) default NULL,
  SPRACH_ID smallint(6) default '2',
  PROVISIONSART char(1) default NULL,
  ABRECHNUNGSZEITPUNKT enum('R','Z') default 'Z',
  PROVISIONMITTRANSPORT enum('Y','N') default 'N',
  PROVISIONSATZ float(5,2) default NULL,
  LETZTEABRECHNUNG date default NULL,
  UMSATZ float(12,2) default NULL,
  PROVISION float(10,2) default NULL,
  BESCHAEFTIGUNGSART smallint(6) default NULL,
  BESCHAEFTIGUNGSGRAD smallint(6) default NULL,
  GUELTIG_VON datetime default NULL,
  GUELTIG_BIS datetime default NULL,
  GEBDATUM datetime default NULL,
  GESCHLECHT enum('M','W') NOT NULL default 'M',
  FAMSTAND smallint(6) default NULL,
  BANK varchar(40) default NULL,
  KTO_INHABER varchar(40) default NULL,
  BLZ varchar(10) default NULL,
  KTO varchar(20) default NULL,
  BEMERKUNG text,
  ERSTELLT datetime default NULL,
  ERSTELLT_NAME varchar(20) default NULL,
  GEAEND datetime default NULL,
  GEAEND_NAME varchar(20) default NULL,
  USERFELD_01 varchar(255) default NULL,
  USERFELD_02 varchar(255) default NULL,
  USERFELD_03 varchar(255) default NULL,
  USERFELD_04 varchar(255) default NULL,
  USERFELD_05 varchar(255) default NULL,
  USERFELD_06 varchar(255) default NULL,
  USERFELD_07 varchar(255) default NULL,
  USERFELD_08 varchar(255) default NULL,
  USERFELD_09 varchar(255) default NULL,
  USERFELD_10 varchar(255) default NULL,
  PRIMARY KEY  (VERTRETER_ID),
  UNIQUE KEY IDX_VE_NUM (VERTR_NUMMER)
);

CREATE TABLE WARENGRUPPEN (
  ID int(11) NOT NULL auto_increment,
  TOP_ID int(11) NOT NULL default '-1',
  NAME varchar(250) NOT NULL default '',
  BESCHREIBUNG text,
  DEF_EKTO int(11) default '-1',
  DEF_AKTO int(11) default '-1',
  STEUER_CODE tinyint(4) unsigned NOT NULL default '2',
  VK1_FAKTOR float(6,5) NOT NULL default '0.00000',
  VK2_FAKTOR float(6,5) NOT NULL default '0.00000',
  VK3_FAKTOR float(6,5) NOT NULL default '0.00000',
  VK4_FAKTOR float(6,5) NOT NULL default '0.00000',
  VK5_FAKTOR float(6,5) NOT NULL default '0.00000',
  VORGABEN text,
  PRIMARY KEY  (ID)
);

/* BASIS-DATEN */

INSERT INTO WARENGRUPPEN VALUES("1", "-1", "Artikel 1", NULL, "-1", "-1", "2", "0.00000", "0.00000", "0.00000", "0.00000", "0.00000", NULL);
INSERT INTO WARENGRUPPEN VALUES("2", "-1", "Artikel 2", NULL, "-1", "-1", "2", "0.00000", "0.00000", "0.00000", "0.00000", "0.00000", NULL);
INSERT INTO WARENGRUPPEN VALUES("3", "-1", "Artikel 3", NULL, "-1", "-1", "2", "0.00000", "0.00000", "0.00000", "0.00000", "0.00000", NULL);
INSERT INTO WARENGRUPPEN VALUES("4", "-1", "Artikel 4", NULL, "-1", "-1", "2", "0.00000", "0.00000", "0.00000", "0.00000", "0.00000", NULL);
INSERT INTO WARENGRUPPEN VALUES("5", "-1", "Artikel 5", NULL, "-1", "-1", "2", "0.00000", "0.00000", "0.00000", "0.00000", "0.00000", NULL);
INSERT INTO WARENGRUPPEN VALUES("6", "-1", "Artikel 6", NULL, "-1", "-1", "2", "0.00000", "0.00000", "0.00000", "0.00000", "0.00000", NULL);
INSERT INTO WARENGRUPPEN VALUES("7", "-1", "Artikel 7", NULL, "-1", "-1", "2", "0.00000", "0.00000", "0.00000", "0.00000", "0.00000", NULL);
INSERT INTO WARENGRUPPEN VALUES("8", "-1", "Artikel 8", NULL, "-1", "-1", "2", "0.00000", "0.00000", "0.00000", "0.00000", "0.00000", NULL);
INSERT INTO WARENGRUPPEN VALUES("9", "-1", "Artikel 9", NULL, "-1", "-1", "2", "0.00000", "0.00000", "0.00000", "0.00000", "0.00000", NULL);
INSERT INTO WARENGRUPPEN VALUES("10", "-1", "Artikel 10", NULL, "-1", "-1", "2", "0.00000", "0.00000", "0.00000", "0.00000", "0.00000", NULL);
INSERT INTO WARENGRUPPEN VALUES("11", "-1", "Artikel 11", NULL, "-1", "-1", "2", "0.00000", "0.00000", "0.00000", "0.00000", "0.00000", NULL);
INSERT INTO WARENGRUPPEN VALUES("12", "-1", "Artikel 12", NULL, "-1", "-1", "2", "0.00000", "0.00000", "0.00000", "0.00000", "0.00000", NULL);
INSERT INTO WARENGRUPPEN VALUES("13", "-1", "Artikel 13", NULL, "-1", "-1", "2", "0.00000", "0.00000", "0.00000", "0.00000", "0.00000", NULL);
INSERT INTO WARENGRUPPEN VALUES("14", "-1", "Artikel 14", NULL, "-1", "-1", "2", "0.00000", "0.00000", "0.00000", "0.00000", "0.00000", NULL);
INSERT INTO WARENGRUPPEN VALUES("15", "-1", "Artikel 15", NULL, "-1", "-1", "2", "0.00000", "0.00000", "0.00000", "0.00000", "0.00000", NULL);
INSERT INTO WARENGRUPPEN VALUES("16", "-1", "Artikel 16", NULL, "-1", "-1", "2", "0.00000", "0.00000", "0.00000", "0.00000", "0.00000", NULL);
INSERT INTO WARENGRUPPEN VALUES("17", "-1", "Artikel 17", NULL, "-1", "-1", "2", "0.00000", "0.00000", "0.00000", "0.00000", "0.00000", NULL);
INSERT INTO WARENGRUPPEN VALUES("18", "-1", "Artikel 18", NULL, "-1", "-1", "2", "0.00000", "0.00000", "0.00000", "0.00000", "0.00000", NULL);
INSERT INTO WARENGRUPPEN VALUES("19", "-1", "Artikel 19", NULL, "-1", "-1", "2", "0.00000", "0.00000", "0.00000", "0.00000", "0.00000", NULL);
INSERT INTO WARENGRUPPEN VALUES("20", "-1", "Artikel 20", NULL, "-1", "-1", "2", "0.00000", "0.00000", "0.00000", "0.00000", "0.00000", NULL);

CREATE TABLE WARTUNG (
  REC_ID int(11) NOT NULL auto_increment,
  ADDR_ID int(11) NOT NULL default '0',
  BESCHREIBUNG varchar(255) default NULL,
  WARTUNG date default NULL,
  INTERVALL int(11) default '0',
  WVERTRAG varchar(20) default NULL,
  BEMERKUNG text,
  LEBENSLAUF text,
  LISTE text,
  WARTUNG_TYP varchar(20) default NULL,
  WVERTRAG_NR smallint(6) default '0',
  SOLLZEIT_KW float(3,1) default NULL,
  SOLLZEIT_GW float(3,1) default NULL,
  ENTFERNUNG int(11) unsigned default NULL,
  BEM_OPT1 tinyint(1) unsigned default '0',
  BEM_OPT2 tinyint(1) unsigned default '0',
  BEM_OPT3 tinyint(1) unsigned default '0',
  BEM_OPT4 tinyint(1) unsigned default '0',
  BEM_OPT5 tinyint(1) unsigned default '0',
  BEM_OPT6 tinyint(1) unsigned default '0',
  BEM_OPT7 tinyint(1) unsigned default '0',
  BEM_OPT8 tinyint(1) unsigned default '0',
  BEM_OPT9 tinyint(1) unsigned default '0',
  ERSTELLT date default NULL,
  ERST_NAME varchar(20) default NULL,
  GEAENDERT date default NULL,
  GEAEND_NAME varchar(20) default NULL,
  PRIMARY KEY  (REC_ID)
);


INSERT INTO FIBU_KONTEN VALUES("1000", "Kasse", "0", "0", "0", "0", "0", "0", "0", "0.00");
INSERT INTO FIBU_KONTEN VALUES("1200", "Bank", "0", "0", "0", "0", "0", "0", "0", "0.00");
INSERT INTO FIBU_KONTEN VALUES("3400", "Aufwendungen f�r Waren", "0", "0", "0", "0", "0", "0", "0", "0.00");
INSERT INTO FIBU_KONTEN VALUES("8400", "Umsatzerl�se Waren", "0", "0", "0", "0", "0", "0", "0", "0.00");

INSERT INTO LAND VALUES("DE", "Deutschland", "DEU", "5", "0049", "�", "D");
INSERT INTO LAND VALUES("AT", "�sterreich", "AUT", "5", NULL, "ATS", "D");
INSERT INTO LAND VALUES("CH", "Schweiz", "CHE", "1", NULL, "CHF", "D");
INSERT INTO LAND VALUES("FR", "Frankreich", "FRA", "1", NULL, "FFR", "F");
INSERT INTO LAND VALUES("GB", "England", "GBR", "1", NULL, NULL, "E");

INSERT INTO RABATTGRUPPEN VALUES("-", "5", "1", "0", "0.00", "0.00", "0.00", "-1", "keine Rabattgruppe");

INSERT INTO REGISTERY VALUES("MAIN", "DEFAULT", "allg. Programmeinstellungen", NULL, NULL, NULL, NULL, NULL, NULL, NULL, "1", "N", "N", "20030914192347");
INSERT INTO REGISTERY VALUES("USERSETTINGS", "DEFAULT", "benutzerdefinierte Einstellungen", NULL, NULL, NULL, NULL, NULL, NULL, NULL, "1", "N", "N", "20030914192318");
INSERT INTO REGISTERY VALUES("MAIN\\LAYOUT", "DEFAULT", "Spaltenbreiten, NAMEn etc.", NULL, NULL, NULL, NULL, NULL, NULL, NULL, "1", "N", "N", "20030914192415");
INSERT INTO REGISTERY VALUES("MAIN\\REPORT", "DEFAULT", "Layout der Belege", NULL, NULL, NULL, NULL, NULL, NULL, NULL, "1", "N", "N", "20030914192425");
INSERT INTO REGISTERY VALUES("MAIN\\WAEHRUNG", "DM", "Deutsche Mark", NULL, NULL, NULL, NULL, "1", NULL, NULL, "1", "N", "N", "20030914192509");
INSERT INTO REGISTERY VALUES("MAIN\\WAEHRUNG", "�", "Euro", NULL, NULL, NULL, NULL, "1.95583", NULL, NULL, "1", "N", "N", "20030914192510");
INSERT INTO REGISTERY VALUES("MAIN\\WAEHRUNG", "$", "US Dollar", NULL, NULL, NULL, NULL, "2.2", NULL, NULL, "1", "N", "N", "20030914192510");
INSERT INTO REGISTERY VALUES("MAIN", "LEITWAEHRUNG", "�", NULL, NULL, NULL, NULL, NULL, NULL, NULL, "1", "N", "N", "20030914192346");
INSERT INTO REGISTERY VALUES("MAIN\\LIEFART", "DPD", NULL, NULL, "1", NULL, NULL, NULL, NULL, NULL, NULL, "N", "N", "20030914192416");
INSERT INTO REGISTERY VALUES("MAIN\\LIEFART", "frei Haus", NULL, NULL, "2", NULL, NULL, NULL, NULL, NULL, NULL, "N", "N", "20030914192416");
INSERT INTO REGISTERY VALUES("MAIN\\LIEFART", "Post-Nachnahme", NULL, NULL, "3", NULL, NULL, NULL, NULL, NULL, NULL, "N", "N", "20030914192416");
INSERT INTO REGISTERY VALUES("MAIN\\LIEFART", "Post", NULL, NULL, "4", NULL, NULL, NULL, NULL, NULL, NULL, "N", "N", "20030914192416");
INSERT INTO REGISTERY VALUES("MAIN\\LIEFART", "UPS", NULL, NULL, "5", NULL, NULL, NULL, NULL, NULL, NULL, "N", "N", "20030914192417");
INSERT INTO REGISTERY VALUES("MAIN\\LIEFART", "UPS-Nachnahme", NULL, NULL, "6", NULL, NULL, NULL, NULL, NULL, NULL, "N", "N", "20030914192417");
INSERT INTO REGISTERY VALUES("MAIN\\LIEFART", "Selbstabholung", NULL, NULL, "7", NULL, NULL, NULL, NULL, NULL, NULL, "N", "N", "20030914192417");
INSERT INTO REGISTERY VALUES("MAIN\\ADDR_HIR", "alle Adressen", NULL, NULL, "0", NULL, NULL, NULL, NULL, NULL, NULL, "N", "N", "20030914192412");
INSERT INTO REGISTERY VALUES("MAIN\\ADDR_HIR", "Kunden 1", NULL, NULL, "1", NULL, NULL, NULL, "KUNDENGRUPPE=1", NULL, NULL, "N", "N", "20030914192412");
INSERT INTO REGISTERY VALUES("MAIN\\ADDR_HIR", "Kunden 2", NULL, NULL, "2", NULL, NULL, NULL, "KUNDENGRUPPE=2", NULL, NULL, "N", "N", "20030914192412");
INSERT INTO REGISTERY VALUES("MAIN\\ADDR_HIR", "Kunden 3", NULL, NULL, "3", NULL, NULL, NULL, "KUNDENGRUPPE=3", NULL, NULL, "N", "N", "20030914192413");
INSERT INTO REGISTERY VALUES("MAIN\\ADDR_HIR", "Kunden 4", NULL, NULL, "4", NULL, NULL, NULL, "KUNDENGRUPPE=4", NULL, NULL, "N", "N", "20030914192413");
INSERT INTO REGISTERY VALUES("MAIN\\ADDR_HIR", "Kunden 5", NULL, NULL, "5", NULL, NULL, NULL, "KUNDENGRUPPE=5", NULL, NULL, "N", "N", "20030914192413");
INSERT INTO REGISTERY VALUES("MAIN\\ADDR_HIR", "Kunden 6", NULL, NULL, "6", NULL, NULL, NULL, "KUNDENGRUPPE=6", NULL, NULL, "N", "N", "20030914192413");
INSERT INTO REGISTERY VALUES("MAIN\\ADDR_HIR", "Kunden 7", NULL, NULL, "7", NULL, NULL, NULL, "KUNDENGRUPPE=7", NULL, NULL, "N", "N", "20030914192414");
INSERT INTO REGISTERY VALUES("MAIN\\ADDR_HIR", "Kunden 8", NULL, NULL, "8", NULL, NULL, NULL, "KUNDENGRUPPE=8", NULL, NULL, "N", "N", "20030914192414");
INSERT INTO REGISTERY VALUES("MAIN\\ADDR_HIR", "Kunden 9", NULL, NULL, "9", NULL, NULL, NULL, "KUNDENGRUPPE=9", NULL, NULL, "N", "N", "20030914192414");
INSERT INTO REGISTERY VALUES("MAIN\\ADDR_HIR", "Kunden 10", NULL, NULL, "10", NULL, NULL, NULL, "KUNDENGRUPPE=10", NULL, NULL, "N", "N", "20030914192414");
INSERT INTO REGISTERY VALUES("MAIN\\ADDR_HIR", "Lieferanten", NULL, NULL, "11", NULL, NULL, NULL, "KUNDENGRUPPE=11", NULL, NULL, "N", "N", "20030914192415");
INSERT INTO REGISTERY VALUES("MAIN\\ZAHLART", "Vorkasse", "0", NULL, "0", "30", NULL, "0", NULL, "Betrag per Vorkasse erhalten", NULL, "N", "N", "20030914192339");
INSERT INTO REGISTERY VALUES("MAIN\\ZAHLART", "Bar", "1", NULL, "1", "30", NULL, "3", NULL, "Betrag BAR erhalten", NULL, "N", "N", "20030914192338");
INSERT INTO REGISTERY VALUES("MAIN\\ZAHLART", "�berweisung Bank", "2", NULL, "14", "30", NULL, "2", NULL, "�berweisung Bank", NULL, "N", "N", "20030914192338");
INSERT INTO REGISTERY VALUES("MAIN\\ZAHLART", "Nachnahme", "3", NULL, "14", "30", NULL, "2", NULL, "Betrag per Post-Nachnahme erhalten", NULL, "N", "N", "20030914192337");
INSERT INTO REGISTERY VALUES("MAIN\\ZAHLART", "UPS Nachnahme", "4", NULL, "14", "30", NULL, "2", NULL, "Betrag per UPS-Nachnahme erhalten", NULL, "N", "N", "20030914192337");
INSERT INTO REGISTERY VALUES("MAIN\\ZAHLART", "Scheck", "5", NULL, "14", "30", NULL, "2", NULL, "Betrag per Scheck erhalten", NULL, "N", "N", "20030914192336");
INSERT INTO REGISTERY VALUES("MAIN\\ZAHLART", "EC-KARTE", "6", NULL, "14", "30", NULL, "2", NULL, "Betrag per EC-KARTE eingezogen", NULL, "N", "N", "20030914192336");
INSERT INTO REGISTERY VALUES("MAIN\\ZAHLART", "Kreditkarte", "7", NULL, "14", "30", NULL, "2", NULL, "Betrag wird per Kreditkarte eingezogen", NULL, "N", "N", "20030914192335");
INSERT INTO REGISTERY VALUES("MAIN\\ZAHLART", "PayPal", "8", NULL, "0", "7", NULL, "0", NULL, "Betrag per PayPal erhalten", NULL, "N", "N", "20030914192335");
INSERT INTO REGISTERY VALUES("MAIN\\ZAHLART", "Lastschrift", "9", NULL, "14", "30", NULL, "2", NULL, "Wir buchen den Betrag von ihrem Konto %KTONR% BLZ %BLZ% ab.", NULL, "N", "N", "20030914192842");
INSERT INTO REGISTERY VALUES("MAIN\\NUMBERS", "VK-AUF", "000000", NULL, "8", "230000", "6", NULL, NULL, NULL, "3", "N", "N", "20030914193004");
INSERT INTO REGISTERY VALUES("MAIN\\NUMBERS", "VK-AGB", "000000", NULL, "1", "230000", "6", NULL, NULL, NULL, "3", "N", "N", "20030914193006");
INSERT INTO REGISTERY VALUES("MAIN\\NUMBERS", "VK-LIEF", "000000", NULL, "2", "230000", "6", NULL, NULL, NULL, "3", "N", "N", "20030914193007");
INSERT INTO REGISTERY VALUES("MAIN\\NUMBERS", "VK-RECH", "000000", NULL, "3", "230000", "6", NULL, NULL, NULL, "3", "N", "N", "20030914193007");
INSERT INTO REGISTERY VALUES("MAIN\\NUMBERS", "EK-RECH", "000000", NULL, "5", "230000", "6", NULL, NULL, NULL, "3", "N", "N", "20030914193008");
INSERT INTO REGISTERY VALUES("MAIN\\NUMBERS", "EDIT", "000000", NULL, "10", "1000", "6", NULL, NULL, NULL, "3", "N", "N", "20030914193015");
INSERT INTO REGISTERY VALUES("MAIN\\NUMBERS", "KUNNUM", "000000", NULL, "99", "5000", "6", NULL, NULL, NULL, "3", "N", "N", "20030914192420");
INSERT INTO REGISTERY VALUES("MAIN\\NUMBERS", "VK-KASSE", "0000", NULL, "22", "1000", "4", NULL, NULL, NULL, "3", "N", "N", "20030914192421");
INSERT INTO REGISTERY VALUES("MAIN\\NUMBERS", "EK-BEST", "0000", NULL, "6", "1000", "4", NULL, NULL, NULL, "3", "N", "N", "20030914192421");
INSERT INTO REGISTERY VALUES("MAIN\\NUMBERS", "UEBERWEISUNG", "0000", NULL, "110", "1000", "4", NULL, NULL, NULL, "3", "N", "N", "20030914192424");
INSERT INTO REGISTERY VALUES("MAIN\\NUMBERS", "KRD-NUM", "000000", NULL, "30", "75000", "6", NULL, NULL, NULL, "3", "N", "N", "20030914192424");
INSERT INTO REGISTERY VALUES("MAIN\\NUMBERS", "DEB-NUM", "000000", NULL, "31", "15000", "6", NULL, NULL, NULL, "3", "N", "N", "20030914192424");
INSERT INTO REGISTERY VALUES("MAIN\\NUMBERS", "VERTRAG", "000000", NULL, "23", "1000", "6", NULL, NULL, NULL, "3", "N", "N", "20030914192425");
INSERT INTO REGISTERY VALUES("MAIN\\NUMBERS", "ARTIKELNUMMER", "000000", NULL, "98", "1000", "10", NULL, NULL, NULL, "3", "N", "N", "20030918215644");
INSERT INTO REGISTERY VALUES("MAIN\\MWST", "0", "ohne MwSt", NULL, NULL, NULL, NULL, "0", NULL, NULL, NULL, "N", "N", "20030914192417");
INSERT INTO REGISTERY VALUES("MAIN\\MWST", "1", "erm. MwSt-Satz", NULL, NULL, NULL, NULL, "7", NULL, NULL, NULL, "N", "N", "20030914192418");
INSERT INTO REGISTERY VALUES("MAIN\\MWST", "2", "voller MwSt-Satz", NULL, NULL, NULL, NULL, "16", NULL, NULL, NULL, "N", "N", "20030914192418");
INSERT INTO REGISTERY VALUES("MAIN\\MWST", "3", "Reserve", NULL, NULL, NULL, NULL, "0", NULL, NULL, NULL, "N", "N", "20030914192418");
INSERT INTO REGISTERY VALUES("MAIN\\REPORT", "VK_AGB_MAIL_SUBJECT", NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, "1", "N", "N", "20030914191257");
INSERT INTO REGISTERY VALUES("MAIN\\REPORT", "VK_RECH_MAIL_SUBJECT", NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, "1", "N", "N", "20030914191257");
INSERT INTO REGISTERY VALUES("MAIN\\REPORT", "VK_LIEF_MAIL_SUBJECT", NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, "1", "N", "N", "20030914191257");
INSERT INTO REGISTERY VALUES("MAIN\\REPORT", "EK_BEST_MAIL_SUBJECT", NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, "1", "N", "N", "20030914191257");
INSERT INTO REGISTERY VALUES("KASSE", "BON_UEBERSCHRIFT1", NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, "1", "N", "N", "20030914191257");
INSERT INTO REGISTERY VALUES("KASSE", "BON_UEBERSCHRIFT2", NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, "1", "N", "N", "20030914191257");
INSERT INTO REGISTERY VALUES("KASSE", "BON_UEBERSCHRIFT3", NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, "1", "N", "N", "20030914191257");
INSERT INTO REGISTERY VALUES("KASSE", "DRUCKERPORT", "LPT2", NULL, NULL, NULL, NULL, NULL, NULL, NULL, "1", "N", "N", "20030914191257");
INSERT INTO REGISTERY VALUES("MAIN\\ADRESSEN", "KTO_LEN", NULL, NULL, "10", NULL, NULL, NULL, NULL, NULL, "3", "N", "N", "20030914191257");
INSERT INTO REGISTERY VALUES("MAIN\\ADRESSEN", "BLZ_LEN", NULL, NULL, "8", NULL, NULL, NULL, NULL, NULL, "3", "N", "N", "20030914191257");
INSERT INTO REGISTERY VALUES("MAIN\\REPORT", "ZAHLUNGSZIEL_SOFORT_TEXT", "Sofort", NULL, NULL, NULL, NULL, NULL, NULL, NULL, "1", "N", "N", "20030914191257");
INSERT INTO REGISTERY VALUES("SHOP\\LIEFART_MAP", "DEFAULT", "Mapping f�r Lieferarten", NULL, NULL, NULL, NULL, NULL, NULL, NULL, "1", "N", "N", "20030513214242");
INSERT INTO REGISTERY VALUES("SHOP\\ZAHLART_MAP", "DEFAULT", "Mapping f�r Zahlungsarten", NULL, NULL, NULL, NULL, NULL, NULL, NULL, "1", "N", "N", "20030513214242");
INSERT INTO REGISTERY VALUES("SHOP", "DEFAULT", "allg. Shopeinstellungen", NULL, NULL, NULL, NULL, NULL, NULL, NULL, "1", "N", "N", "20030914191257");
INSERT INTO REGISTERY VALUES("MAIN", "DB_VERSION", "1.08", NULL, NULL, NULL, NULL, NULL, NULL, NULL, "1", "N", "N", "20030918215644");
INSERT INTO REGISTERY VALUES("SHOP", "DEFAULT_DEBNUM", NULL, NULL, "0", NULL, NULL, NULL, NULL, NULL, "3", "N", "N", "20030914191313");
INSERT INTO REGISTERY VALUES("KASSE", "BONDRUCKER", NULL, NULL, "1", NULL, NULL, NULL, NULL, NULL, "3", "N", "N", "20030914191313");
INSERT INTO REGISTERY VALUES("KASSE", "DRUCKDIALOG", NULL, NULL, "1", NULL, NULL, NULL, NULL, NULL, "3", "N", "N", "20030914191313");
INSERT INTO REGISTERY VALUES("KASSE", "SOFORTDRUCK", NULL, NULL, "0", NULL, NULL, NULL, NULL, NULL, "3", "N", "N", "20030914191313");
INSERT INTO REGISTERY VALUES("KASSE", "DEFAULT_FORMULAR", "", NULL, NULL, NULL, NULL, NULL, NULL, NULL, "1", "N", "N", "20030914191313");
INSERT INTO REGISTERY VALUES("KASSE", "DEFAULT_DRUCKER", "Default", NULL, NULL, NULL, NULL, NULL, NULL, NULL, "1", "N", "N", "20030914191313");
INSERT INTO REGISTERY VALUES("SHOP", "USE_SHOP", NULL, NULL, "1", NULL, NULL, NULL, NULL, NULL, "3", "N", "N", "20030720155147");
INSERT INTO REGISTERY VALUES("SHOP", "USE_SHOP_ORDERID", NULL, NULL, "1", NULL, NULL, NULL, NULL, NULL, "3", "N", "N", "20030729105222");
INSERT INTO REGISTERY VALUES("SHOP", "BRUTTO_SHOP", NULL, NULL, "1", NULL, NULL, NULL, NULL, NULL, "3", "N", "N", "20030727164940");
INSERT INTO REGISTERY VALUES("SHOP", "SHOPTRANS_USER", "", NULL, NULL, NULL, NULL, NULL, NULL, NULL, "1", "N", "N", "20030722235555");
INSERT INTO REGISTERY VALUES("SHOP", "SHOPTRANS_SECRET", "", NULL, NULL, NULL, NULL, NULL, NULL, NULL, "1", "N", "N", "20030722235555");
INSERT INTO REGISTERY VALUES("SHOP", "IMPORT_URL", "", NULL, NULL, NULL, NULL, NULL, NULL, NULL, "1", "N", "N", "20030725204915");
INSERT INTO REGISTERY VALUES("SHOP", "UPDATE_URL", "", NULL, NULL, NULL, NULL, NULL, NULL, NULL, "1", "N", "N", "20030720155147");
INSERT INTO REGISTERY VALUES("SHOP", "UPDATEORDERSTATUS_SENDMAIL", NULL, NULL, "0", NULL, NULL, NULL, NULL, NULL, "3", "N", "N", "20030720155147");
INSERT INTO REGISTERY VALUES("SHOP\\ORDERSTATUS", "Offen", NULL, NULL, "1", NULL, NULL, NULL, NULL, NULL, "3", "N", "N", "20030721230156");
INSERT INTO REGISTERY VALUES("SHOP\\ORDERSTATUS", "In Bearbeitung", NULL, NULL, "2", NULL, NULL, NULL, NULL, NULL, "3", "N", "N", "20030914193840");
INSERT INTO REGISTERY VALUES("SHOP\\ORDERSTATUS", "Versendet", NULL, NULL, "3", NULL, NULL, NULL, NULL, NULL, "3", "N", "N", "20030914193910");
INSERT INTO REGISTERY VALUES("SHOP\\LIEFART_MAP", "dp", NULL, NULL, "4", NULL, NULL, NULL, NULL, NULL, "3", "N", "N", "20030722012546");
INSERT INTO REGISTERY VALUES("SHOP\\LIEFART_MAP", "ups", NULL, NULL, "5", NULL, NULL, NULL, NULL, NULL, "3", "N", "N", "20030722211202");
INSERT INTO REGISTERY VALUES("SHOP\\LIEFART_MAP", "dpd", NULL, NULL, "1", NULL, NULL, NULL, NULL, NULL, "3", "N", "N", "20030722211158");
INSERT INTO REGISTERY VALUES("SHOP\\ZAHLART_MAP", "moneyorder", NULL, NULL, "0", NULL, NULL, NULL, NULL, NULL, "3", "N", "N", "20030801235239");
INSERT INTO REGISTERY VALUES("SHOP\\ZAHLART_MAP", "cod", NULL, NULL, "3", NULL, NULL, NULL, NULL, NULL, "3", "N", "N", "20030722211141");
INSERT INTO REGISTERY VALUES("SHOP\\ZAHLART_MAP", "paypal", NULL, NULL, "8", NULL, NULL, NULL, NULL, NULL, "3", "N", "N", "20030914192539");
INSERT INTO REGISTERY VALUES("SHOP\\ZAHLART_MAP", "banktransfer", NULL, NULL, "9", NULL, NULL, NULL, NULL, NULL, "3", "N", "N", "20030914192540");
INSERT INTO REGISTERY VALUES("SHOP\\ZAHLART_MAP", "cc", NULL, NULL, "7", NULL, NULL, NULL, NULL, NULL, "3", "N", "N", "20030914192541");

INSERT INTO SPRACHEN VALUES("1", "English", "en", "2", "N");
INSERT INTO SPRACHEN VALUES("2", "Deutsch", "de", "1", "Y");
INSERT INTO SPRACHEN VALUES("3", "Espa�ol", "es", "3", "N");
