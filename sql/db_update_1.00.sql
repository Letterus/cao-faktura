/* Update to DB Version 1.00 */

alter table JOURNAL
drop NSUMME_0,
drop NSUMME_1,
drop NSUMME_2,
drop NSUMME_3,
drop BSUMME_0,
drop BSUMME_1,
drop BSUMME_2,
drop BSUMME_3,
ADD MAHNSTUFE TINYINT(1) DEFAULT "0",
ADD MAHNDATUM DATE,
ADD MAHNPRINT TINYINT(1) DEFAULT "0";

alter table REGISTERY
add VAL_INT2 integer after VAL_INT;

alter table REGISTERY
add VAL_INT3 integer after VAL_INT2;

update JOURNAL
set IST_BETRAG='0.00'
where ISNULL(IST_BETRAG);

update JOURNAL
set IST_ANZAHLUNG='0.00'
where ISNULL(IST_ANZAHLUNG);

update KFZ
set LE_BESUCH=null
where LE_BESUCH='1900-01-01';

update KFZ
set NAE_TUEF=null
where NAE_TUEF='1900-01-01';

update KFZ
set NEA_AU=null
where NEA_AU='1900-01-01';

update KFZ
set HERSTELLUNG=null
where HERSTELLUNG='1900-01-01';

update KFZ
set KAUFDATUM=null
where KAUFDATUM='1900-01-01';

update KFZ
set ZULASSUNG=null
where ZULASSUNG='1900-01-01';

update JOURNAL
set UW_NUM=0
where QUELLE=3 and QUELLE_SUB=2 and JAHR>0 and JAHR<2002 and RDATUM < '2002-01-01';

replace into REGISTERY (MAINKEY, NAME, VAL_CHAR) Values ('MAIN', 'DB_VERSION', '1.00');
