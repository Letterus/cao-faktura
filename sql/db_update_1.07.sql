INSERT INTO REGISTERY VALUES("MAIN\\NUMBERS", "ARTIKELNUMMER", "\"AR\"000000", NULL, "98", NULL, NULL, "1000.00", NULL, NULL, NULL, NULL, "0", "0", "20030513214242");
INSERT INTO REGISTERY VALUES("SHOP\\LIEFART_MAP", "DEFAULT", "Mapping f�r Lieferarten", NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, "N", "N", "20030513214242");
INSERT INTO REGISTERY VALUES("SHOP\\ZAHLART_MAP", "DEFAULT", "Mapping f�r Zahlungsarten", NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, "N", "N", "20030513214242");

INSERT INTO REGISTERY VALUES("MAIN\\IMPORT", "DEFAULT", "Profile f�r den Import", NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, "N", "N", "20030513214242");
INSERT INTO REGISTERY VALUES("MAIN\\IMPORT\\ARTIKEL", "DEFAULT", "Profile f�r den Import von Artikeln", NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, "N", "N", "20030513214242");
INSERT INTO REGISTERY VALUES("MAIN\\IMPORT\\ADRESSEN", "DEFAULT", "Profile f�r den Import von Adressen", NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, "N", "N", "20030513214242");


/* BUGFIX */
ALTER TABLE RABATTGRUPPEN DROP PRIMARY KEY, ADD PRIMARY KEY (RABGRP_ID,RABGRP_TYP,LIEF_RABGRP,MIN_MENGE);

/* BRUTTOPREISE f�r alle VK */
ALTER TABLE ARTIKEL
 ADD VK1B FLOAT(10,2)  DEFAULT "0.00" NOT NULL AFTER VK1,
 ADD VK2B FLOAT(10,2)  DEFAULT "0.00" NOT NULL AFTER VK2,
 ADD VK3B FLOAT(10,2)  DEFAULT "0.00" NOT NULL AFTER VK3,
 ADD VK4B FLOAT(10,2)  DEFAULT "0.00" NOT NULL AFTER VK4,
 ADD VK5B FLOAT(10,2)  DEFAULT "0.00" NOT NULL AFTER VK5,
 ADD HERSTELLER_ID INT(11)  DEFAULT "-1" NOT NULL AFTER HERKUNFSLAND;

/* Felder verl�ngern */
ALTER TABLE ADRESSEN
 CHANGE TELE1 TELE1 VARCHAR(100),
 CHANGE TELE2 TELE2 VARCHAR(100),
 CHANGE FAX FAX VARCHAR(100),
 CHANGE FUNK FUNK VARCHAR(100),
 CHANGE EMAIL EMAIL VARCHAR(100),
 CHANGE INTERNET INTERNET VARCHAR(100),
 CHANGE DIVERSES DIVERSES VARCHAR(100);

ALTER TABLE APARTNER
 CHANGE TELEFON TELEFON VARCHAR(100),
 CHANGE TELEFAX TELEFAX VARCHAR(100),
 CHANGE MOBILFUNK MOBILFUNK VARCHAR(100),
 CHANGE TELEPRIVAT TELEPRIVAT VARCHAR(100);

CREATE TABLE IF NOT EXISTS HERSTELLER (
  SHOP_ID TINYINT(3) NOT NULL default '-1',
  HERSTELLER_ID int(11) NOT NULL auto_increment,
  ADDR_ID int(11) NOT NULL default '-1',
  HERSTELLER_NAME varchar(32) NOT NULL default '',
  HERSTELLER_IMAGE varchar(64) default NULL,
  LAST_CHANGE datetime default NULL,
  SHOP_DATE_ADDED datetime default NULL,
  SHOP_DATE_CHANGE datetime default NULL,
  SYNC_FLAG ENUM('N','Y')  DEFAULT "N" NOT NULL,
  CHANGE_FLAG ENUM('N','Y')  DEFAULT "N" NOT NULL,
  DEL_FLAG ENUM('N','Y')  DEFAULT "N" NOT NULL,
  PRIMARY KEY  (SHOP_ID, HERSTELLER_ID),
  KEY IDX_HERSTELLER_NAME (HERSTELLER_NAME)
);

CREATE TABLE IF NOT EXISTS HERSTELLER_INFO (
  SHOP_ID TINYINT(3) NOT NULL default '-1',
  HERSTELLER_ID int(11) NOT NULL default '0',
  SPRACHE_ID int(11) NOT NULL default '0',
  HERSTELLER_URL varchar(255) NOT NULL default '',
  URL_CLICKED int(5) NOT NULL default '0',
  DATE_LAST_CLICK datetime default NULL,
  LAST_CHANGE datetime default NULL,
  SHOP_DATE_ADDED datetime default NULL,
  SHOP_DATE_CHANGE datetime default NULL,
  SYNC_FLAG ENUM('N','Y')  DEFAULT "N" NOT NULL,
  CHANGE_FLAG ENUM('N','Y')  DEFAULT "N" NOT NULL,
  DEL_FLAG ENUM('N','Y')  DEFAULT "N" NOT NULL,
  PRIMARY KEY  (SHOP_ID, HERSTELLER_ID, SPRACHE_ID)
);

CREATE TABLE IF NOT EXISTS LINK (
  MODUL_ID TINYINT(3) NOT NULL DEFAULT '-1',
  REC_ID INT(11) NOT NULL DEFAULT '-1',
  PFAD VARCHAR(255) NOT NULL default '',
  DATEI VARCHAR(200) NOT NULL default '',
  BEMERKUNG TEXT,
  LAST_CHANGE TIMESTAMP default NULL,
  LAST_CHANGE_USER VARCHAR(50) NOT NULL,
  OPEN_FLAG ENUM('N','Y')  DEFAULT "N" NOT NULL,
  OPEN_USER VARCHAR(50)  NOT NULL,
  OPEN_TIME TIMESTAMP NOT NULL,
  PRIMARY KEY  (MODUL_ID, REC_ID, PFAD, DATEI)
); 

/* Tabelle f�r Lieferanten- und Kundenpreise */
CREATE TABLE IF NOT EXISTS ARTIKEL_PREIS (
  ARTIKEL_ID int(11) NOT NULL default '-1',
  ADRESS_ID int(11) NOT NULL default '-1',
  PREIS_TYP tinyint(2) unsigned NOT NULL default '0',
  BESTNUM varchar(50) default NULL,
  PREIS float(10,3) NOT NULL default '0.000',
  INFO text,
  GEAEND timestamp(14) NOT NULL,
  GEAEND_NAME varchar(20) default NULL,
  PRIMARY KEY  (ARTIKEL_ID, ADRESS_ID, PREIS_TYP)
);

/* Daten aus den aktuellen Artikelfeldern �bernehmen */
insert INTO ARTIKEL_PREIS
 select
 REC_ID as ARTIKEL_ID,
 LIEF1 as ADRESS_ID,
 5 as PREISTYP,
 LIEF1_BNUM as BESTNUM,
 LIEF1_PREIS as PREIS,
 '' as INFO,
 NOW(),
 'IMPORT'
 from ARTIKEL where LIEF1>0 and LIEF1_PREIS !=0;

insert INTO ARTIKEL_PREIS
 select
 REC_ID as ARTIKEL_ID,
 LIEF2 as ADRESS_ID,
 5 as PREISTYP,
 LIEF2_BNUM as BESTNUM,
 LIEF2_PREIS as PREIS,
 '' as INFO,
 NOW(),
 'IMPORT'
 from ARTIKEL where LIEF2>0 and LIEF2_PREIS !=0;

/* WARTUNG f�r Daniel*/
CREATE TABLE IF NOT EXISTS WARTUNG (
  REC_ID int(11) NOT NULL auto_increment,
  ADDR_ID int(11) NOT NULL default '0',
  BESCHREIBUNG varchar(255) default NULL,
  WARTUNG date default NULL,
  INTERVALL int(11) default '0',
  WVERTRAG varchar(20) default NULL,
  BEMERKUNG text,
  LEBENSLAUF text,
  LISTE text,
  WARTUNG_TYP varchar(20) default NULL,
  WVERTRAG_NR smallint(6) default '0',
  BEM_OPT1 tinyint(1) unsigned default '0',
  BEM_OPT2 tinyint(1) unsigned default '0',
  BEM_OPT3 tinyint(1) unsigned default '0',
  BEM_OPT4 tinyint(1) unsigned default '0',
  BEM_OPT5 tinyint(1) unsigned default '0',
  BEM_OPT6 tinyint(1) unsigned default '0',
  BEM_OPT7 tinyint(1) unsigned default '0',
  BEM_OPT8 tinyint(1) unsigned default '0',
  BEM_OPT9 tinyint(1) unsigned default '0',
  ERSTELLT date default NULL,
  ERST_NAME varchar(20) default NULL,
  GEAENDERT date default NULL,
  GEAEND_NAME varchar(20) default NULL,
  PRIMARY KEY  (REC_ID)
);


CREATE TABLE IF NOT EXISTS WARENGRUPPEN (
  ID int(11) NOT NULL auto_increment,
  TOP_ID int(11) NOT NULL default '-1',
  NAME varchar(250) NOT NULL default '',
  BESCHREIBUNG text,
  DEF_EKTO int(11) default '-1',
  DEF_AKTO int(11) default '-1',
  VORGABEN text,
  PRIMARY KEY (ID)
);

INSERT INTO WARENGRUPPEN
 select VAL_INT as ID, -1 as TOP_ID, NAME, NULL AS BESCHREIBUNG, VAL_INT3 as DEF_EKTO, VAL_INT2 as DEF_AKTO, NULL as VORGABEN
 from REGISTERY
 where MAINKEY='MAIN\\ART_HIR' and VAL_INT>0 and VAL_BLOB LIKE "WARENGRUPPE=%";

delete FROM REGISTERY
 where MAINKEY='MAIN\\ART_HIR' and VAL_BLOB LIKE "WARENGRUPPE=%";


INSERT INTO REGISTERY VALUES("MAIN\\ZAHLART", "Vorkasse", "0", NULL, "0", NULL, NULL, "0.00", "30", NULL, "Betrag per Vorkasse erhalten", NULL, "0", "0", "20030513214242");
INSERT INTO REGISTERY VALUES("MAIN\\ZAHLART", "Kreditkarte", "7", NULL, "14", NULL, NULL, "2.00", "30", NULL, "Betrag wird per Kreditkarte eingezogen", NULL, "0", "0", "20030513214242");
INSERT INTO REGISTERY VALUES("MAIN\\ZAHLART", "PayPal", "8", NULL, "0", NULL, NULL, "0.00", "7", NULL, "Betrag per PayPal erhalten", NULL, "0", "0", "20030513214242");

INSERT INTO REGISTERY VALUES("MAIN\\ARTIKEL", "VK1_CALC_FAKTOR", NULL , NULL, NULL, NULL, NULL, NULL, "0", NULL, NULL, 5, "0", "0", "20030513214242");
INSERT INTO REGISTERY VALUES("MAIN\\ARTIKEL", "VK2_CALC_FAKTOR", NULL , NULL, NULL, NULL, NULL, NULL, "0", NULL, NULL, 5, "0", "0", "20030513214242");
INSERT INTO REGISTERY VALUES("MAIN\\ARTIKEL", "VK3_CALC_FAKTOR", NULL , NULL, NULL, NULL, NULL, NULL, "0", NULL, NULL, 5, "0", "0", "20030513214242");
INSERT INTO REGISTERY VALUES("MAIN\\ARTIKEL", "VK4_CALC_FAKTOR", NULL , NULL, NULL, NULL, NULL, NULL, "0", NULL, NULL, 5, "0", "0", "20030513214242");
INSERT INTO REGISTERY VALUES("MAIN\\ARTIKEL", "VK5_CALC_FAKTOR", NULL , NULL, NULL, NULL, NULL, NULL, "0", NULL, NULL, 5, "0", "0", "20030513214242");
INSERT INTO REGISTERY VALUES("MAIN\\ARTIKEL", "SHOP_CALC_FAKTOR", NULL , NULL, NULL, NULL, NULL, NULL, "0", NULL, NULL, 5, "0", "0", "20030513214242");

INSERT INTO REGISTERY VALUES("MAIN\\ADRESSEN", "KUNNUM1_EDI", NULL , NULL, '0', NULL, NULL, NULL, NULL, NULL, NULL, 3, "0", "0", "20030513214242");

/* Update Versionsnummer */
DELETE FROM REGISTERY where MAINKEY="MAIN" and NAME="DB_VERSION";
INSERT INTO REGISTERY (MAINKEY, NAME, VAL_CHAR, VAL_TYP) Values ('MAIN', 'DB_VERSION', '1.07', '1');
