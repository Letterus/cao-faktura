/* START 11.08.2003 */

ALTER TABLE FIRMA
  ADD BANK1_IBAN VARCHAR(100)  AFTER BANK1_NAME,
  ADD BANK1_SWIFT VARCHAR(100)  AFTER BANK1_IBAN,
  ADD BANK2_IBAN VARCHAR(100)  AFTER BANK2_NAME,
  ADD BANK2_SWIFT VARCHAR(100)  AFTER BANK2_IBAN,
  ADD IMAGE1 BLOB,
  ADD IMAGE2 BLOB,
  ADD IMAGE3 BLOB;

ALTER TABLE UEBERWEISUNGEN
 ADD ART ENUM("U","L")  DEFAULT "U" NOT NULL AFTER UWNUM;

CREATE TABLE IF NOT EXISTS ADRESSEN_LIEF (
  REC_ID INTEGER NOT NULL Auto_Increment,
  ADDR_ID INTEGER NOT NULL,
  ANREDE VARCHAR(40),
  NAME1 VARCHAR(40) NOT NULL,
  NAME2 VARCHAR(40),
  NAME3 VARCHAR(40),
  ABTEILUNG VARCHAR(40),
  STRASSE VARCHAR(40) NOT NULL,
  LAND VARCHAR(5),
  PLZ VARCHAR(10) NOT NULL,
  ORT VARCHAR(40) NOT NULL,
  INFO TEXT,
  PRIMARY KEY (REC_ID, ADDR_ID)
);

insert into ADRESSEN_LIEF
 select NULL, REC_ID, L_ANREDE, L_NAME1, L_NAME2, L_NAME3,
 L_ABTEIL, L_STRASSE, LAND, L_PLZ, L_ORT, NULL
 from ADRESSEN
 where L_NAME1!='' and (L_NAME1 != NAME1 or L_NAME2 != NAME2 or L_NAME3 != NAME3
 or STRASSE != L_STRASSE or ORT !=L_ORT or L_PLZ != PLZ);


ALTER TABLE ADRESSEN
  CHANGE KUN_LIEFART KUN_LIEFART INT(11) DEFAULT "-1" NOT NULL,
  CHANGE KUN_ZAHLART KUN_ZAHLART INT(11) DEFAULT "-1" NOT NULL,
  CHANGE LIEF_LIEFART LIEF_LIEFART INT(11) DEFAULT "-1" NOT NULL,
  CHANGE LIEF_ZAHLART LIEF_ZAHLART INT(11) DEFAULT "-1" NOT NULL,
  ADD DEFAULT_LIEFANSCHRIFT_ID INT(11)  DEFAULT "-1" NOT NULL AFTER PF_PLZ,
  ADD SPRACH_ID INT(11)  DEFAULT "2" NOT NULL AFTER KUNDENGRUPPE,
  ADD KTO_INHABER VARCHAR(40) AFTER BANK,
  ADD PROVIS_PROZ FLOAT(5,2) DEFAULT "0" NOT NULL AFTER VERTRETER_ID,
  DROP HAT_LIEFANSR,
  DROP L_ANREDE,
  DROP L_NAME1,
  DROP L_NAME2,
  DROP L_NAME3,
  DROP L_ABTEIL,
  DROP L_STRASSE,
  DROP L_PLZ,
  DROP L_ORT,
  DROP L_POSTFACH,
  DROP L_PF_PLZ,
  ADD USERFELD_01 VARCHAR(255),
  ADD USERFELD_02 VARCHAR(255),
  ADD USERFELD_03 VARCHAR(255),
  ADD USERFELD_04 VARCHAR(255),
  ADD USERFELD_05 VARCHAR(255),
  ADD USERFELD_06 VARCHAR(255),
  ADD USERFELD_07 VARCHAR(255),
  ADD USERFELD_08 VARCHAR(255),
  ADD USERFELD_09 VARCHAR(255),
  ADD USERFELD_10 VARCHAR(255),
  ADD EMAIL2 VARCHAR(100) AFTER EMAIL,
  ADD IBAN VARCHAR(100) AFTER BANK,
  ADD SWIFT VARCHAR(100) AFTER IBAN,
  CHANGE KUN_PRLISTE KUN_PRLISTE ENUM("N","Y") DEFAULT "N" NOT NULL,
  CHANGE LIEF_PRLISTE LIEF_PRLISTE ENUM("N","Y") DEFAULT "N" NOT NULL;


ALTER TABLE APARTNER
 ADD EMAIL2 VARCHAR(100) NOT NULL AFTER EMAIL,
 ADD USERFELD_01 VARCHAR(255),
 ADD USERFELD_02 VARCHAR(255),
 ADD USERFELD_03 VARCHAR(255),
 ADD USERFELD_04 VARCHAR(255),
 ADD USERFELD_05 VARCHAR(255),
 ADD USERFELD_06 VARCHAR(255),
 ADD USERFELD_07 VARCHAR(255),
 ADD USERFELD_08 VARCHAR(255),
 ADD USERFELD_09 VARCHAR(255),
 ADD USERFELD_10 VARCHAR(255);


ALTER TABLE ARTIKEL
  DROP LIEF1,
  DROP LIEF1_BNUM,
  DROP LIEF1_PREIS,
  DROP LIEF2,
  DROP LIEF2_BNUM,
  DROP LIEF2_PREIS,
  DROP KAT1,
  DROP KAT2,
  DROP MEMO,
  DROP SHOP_DATASHEET_E,
  DROP SHOP_KATALOG_E,
  DROP SHOP_HANDBUCH_E,
  DROP SHOP_PREIS_EK,
  DROP SHOP_PREIS_SPEC,
  DROP SHOP_EK_RABATT,
  DROP SHOP_FAELLT_WEG,
  DROP SHOP_BESCHREIBUNG,
  DROP SHOP_SPEZIFIKATION,
  DROP SHOP_ZUBEHOER,
  DROP SHOP_ZUBEHOER_OPT,
  DROP SHOP_TOP_ID,
  DROP SHOP_ART_NR,
  DROP SHOP_ART_NAME,

  ADD HERST_ARTNUM VARCHAR(100) AFTER HERSTELLER_ID,
  ADD BARCODE2 VARCHAR(20)  AFTER BARCODE,
  ADD BARCODE3 VARCHAR(20)  AFTER BARCODE2,

  ADD DEFAULT_LIEF_ID INT(11)  DEFAULT "-1" NOT NULL AFTER LAST_VKDAT,
  ADD VPE INT(11)  DEFAULT "1" NOT NULL AFTER ME_EINHEIT,
  ADD PROVIS_PROZ FLOAT(5,2)  DEFAULT "0" NOT NULL AFTER VK5B,

  ADD USERFELD_01 VARCHAR(255),
  ADD USERFELD_02 VARCHAR(255),
  ADD USERFELD_03 VARCHAR(255),
  ADD USERFELD_04 VARCHAR(255),
  ADD USERFELD_05 VARCHAR(255),
  ADD USERFELD_06 VARCHAR(255),
  ADD USERFELD_07 VARCHAR(255),
  ADD USERFELD_08 VARCHAR(255),
  ADD USERFELD_09 VARCHAR(255),
  ADD USERFELD_10 VARCHAR(255),

  CHANGE SHOP_DATASHEET_D SHOP_DATENBLATT VARCHAR(100),
  CHANGE SHOP_HANDBUCH_D SHOP_HANDBUCH VARCHAR(100),
  CHANGE SHOP_DRAWING SHOP_ZEICHNUNG VARCHAR(100),
  CHANGE SHOP_KATALOG_D SHOP_KATALOG VARCHAR(100),

  ADD INDEX IDX_WGR (WARENGRUPPE),
  ADD INDEX IDX_MATCHCODE (MATCHCODE),
  ADD INDEX IDX_BARCODE (BARCODE),
  ADD INDEX IDX_ARTNUM (ARTNUM),
  ADD INDEX IDX_HERST_ARTNUM (HERST_ARTNUM);








ALTER TABLE ARTIKEL_LTEXT
 DROP FELD_ID,
 DROP LTEXT,
 ADD KURZNAME varchar(80) default NULL,
 ADD LANGNAME text,
 ADD KAS_NAME varchar(80) default NULL,
 ADD SHOP_KURZTEXT text,
 ADD SHOP_LANGTEXT text,
 ADD SHOP_DATENBLATT varchar(100) default NULL,
 ADD SHOP_KATALOG varchar(100) default NULL,
 ADD SHOP_ZEICHNUNG varchar(100) default NULL,
 ADD SHOP_HANDBUCH varchar(100) default NULL,
 ADD CHANGE_FLAG tinyint(1) unsigned NOT NULL default '0';

ALTER TABLE KFZ
 CHANGE VERTRETER VERTRETER_ID int(11) NOT NULL default '-1',
 CHANGE NEA_AU NAE_AU DATE,
 ADD NAE_SP DATE AFTER NAE_AU,
 ADD NAE_TP DATE AFTER NAE_SP,
 
 ADD USERFELD_01 VARCHAR(255),
 ADD USERFELD_02 VARCHAR(255),
 ADD USERFELD_03 VARCHAR(255),
 ADD USERFELD_04 VARCHAR(255),
 ADD USERFELD_05 VARCHAR(255),
 ADD USERFELD_06 VARCHAR(255),
 ADD USERFELD_07 VARCHAR(255),
 ADD USERFELD_08 VARCHAR(255),
 ADD USERFELD_09 VARCHAR(255),
 ADD USERFELD_10 VARCHAR(255);


ALTER TABLE ARTIKEL_KAT
  ADD SORT_NUM INT(3)  UNSIGNED DEFAULT "0" NOT NULL AFTER TOP_ID;

ALTER TABLE WARENGRUPPEN
 ADD STEUER_CODE TINYINT(4) UNSIGNED DEFAULT "2" NOT NULL AFTER DEF_AKTO,
 ADD VK1_FAKTOR FLOAT(2,5)  DEFAULT "0" NOT NULL AFTER STEUER_CODE,
 ADD VK2_FAKTOR FLOAT(2,5)  DEFAULT "0" NOT NULL AFTER VK1_FAKTOR,
 ADD VK3_FAKTOR FLOAT(2,5)  DEFAULT "0" NOT NULL AFTER VK2_FAKTOR,
 ADD VK4_FAKTOR FLOAT(2,5)  DEFAULT "0" NOT NULL AFTER VK3_FAKTOR,
 ADD VK5_FAKTOR FLOAT(2,5)  DEFAULT "0" NOT NULL AFTER VK4_FAKTOR;

ALTER TABLE WARTUNG
 ADD SOLLZEIT_KW FLOAT(3,1) AFTER WVERTRAG_NR,
 ADD SOLLZEIT_GW FLOAT(3,1) AFTER SOLLZEIT_KW,
 ADD ENTFERNUNG INT(11) UNSIGNED AFTER SOLLZEIT_GW;

UPDATE ADRESSEN SET KUN_LIEFART = '-1' where KUN_LIEFART=0;
UPDATE ADRESSEN SET KUN_ZAHLART = '-1' where KUN_ZAHLART=0;
UPDATE ADRESSEN SET LIEF_LIEFART = '-1' where LIEF_LIEFART=0;
UPDATE ADRESSEN SET LIEF_ZAHLART = '-1' where LIEF_ZAHLART=0;

CREATE TABLE IF NOT EXISTS SPRACHEN (
  SPRACH_ID int(11) NOT NULL auto_increment,
  NAME varchar(32) NOT NULL default '',
  CODE char(2) NOT NULL default '',
  SORT int(3) default NULL,
  DEFAULT_FLAG enum('N','Y') NOT NULL default 'N',
  PRIMARY KEY  (SPRACH_ID),
  KEY IDX_NAME (NAME)
);

INSERT INTO SPRACHEN VALUES("1", "English", "en", "2", "N");
INSERT INTO SPRACHEN VALUES("2", "Deutsch", "de", "1", "Y");
INSERT INTO SPRACHEN VALUES("3", "Espa�ol", "es", "3", "N");

ALTER TABLE PLZ ADD INDEX IDX_LAND (LAND);

ALTER TABLE HERSTELLER_INFO
  DROP LAST_CHANGE,
  DROP SHOP_DATE_ADDED,
  DROP SHOP_DATE_CHANGE,
  DROP SYNC_FLAG,
  DROP CHANGE_FLAG,
  DROP DEL_FLAG;

ALTER TABLE JOURNAL
  CHANGE LIEFART LIEFART TINYINT(2)  DEFAULT "-1" NOT NULL,
  CHANGE ZAHLART ZAHLART TINYINT(2)  DEFAULT "-1" NOT NULL,
  ADD PROVIS_WERT FLOAT(10,2)  DEFAULT "0" NOT NULL AFTER ATMSUMME,
  ADD PROVIS_BERECHNET ENUM('N','Y')  DEFAULT "N" NOT NULL AFTER MWST_FREI_FLAG,
  ADD TERM_ID INT(11) UNSIGNED DEFAULT "1" NOT NULL FIRST;

ALTER TABLE JOURNALPOS
  ADD TOP_POS_ID INT(11) DEFAULT "-1" NOT NULL AFTER ARTIKEL_ID,
  ADD PROVIS_PROZ FLOAT(5,2)  DEFAULT "0" NOT NULL AFTER ALTTEIL_STCODE,
  ADD PROVIS_WERT FLOAT(10,2)  DEFAULT "0" NOT NULL AFTER PROVIS_PROZ,
 ADD VPE INT(11) UNSIGNED DEFAULT "1" NOT NULL AFTER PR_EINHEIT;

ALTER TABLE ARTIKEL_PREIS
  ADD MENGE2 INT(6) UNSIGNED DEFAULT "0" NOT NULL AFTER PREIS,
  ADD PREIS2 FLOAT(10,3) DEFAULT "0" NOT NULL AFTER MENGE2,
  ADD MENGE3 INT(6) UNSIGNED DEFAULT "0" NOT NULL AFTER PREIS2,
  ADD PREIS3 FLOAT(10,3) DEFAULT "0" NOT NULL AFTER MENGE3,
  ADD MENGE4 INT(6) UNSIGNED DEFAULT "0" NOT NULL AFTER PREIS3,
  ADD PREIS4 FLOAT(10,3) DEFAULT "0" NOT NULL AFTER MENGE4,
  ADD MENGE5 INT(6) UNSIGNED DEFAULT "0" NOT NULL AFTER PREIS4,
  ADD PREIS5 FLOAT(10,3) DEFAULT "0" NOT NULL AFTER MENGE5,
  ADD GUELTIG_VON DATE AFTER PREIS5,
  ADD GUELTIG_BIS DATE AFTER GUELTIG_VON;


CREATE TABLE IF NOT EXISTS MITARBEITER (
  MA_ID int(11) unsigned NOT NULL auto_increment,
  MA_NUMMER varchar(10) default NULL,
  NAME varchar(100) default NULL,
  VNAME varchar(100) default NULL,
  LOGIN_NAME varchar(50) NOT NULL default '',
  ANZEIGE_NAME varchar(50) NOT NULL default '',
  ANREDE varchar(15) default NULL,
  TITEL varchar(15) default NULL,
  ZUSATZ varchar(40) default NULL,
  ZUSATZ2 varchar(40) default NULL,
  ZUHAENDEN varchar(40) default NULL,
  STRASSE varchar(40) default NULL,
  LAND varchar(5) default NULL,
  PLZ varchar(10) default NULL,
  ORT varchar(40) default NULL,
  TELEFON varchar(100) default NULL,
  FAX varchar(100) default NULL,
  FUNK varchar(100) default NULL,
  EMAIL varchar(100) default NULL,
  INTERNET varchar(100) default NULL,
  SPRACH_ID smallint(6) default '2',
  BESCHAEFTIGUNGSART smallint(6) default NULL,
  BESCHAEFTIGUNGSGRAD smallint(6) default NULL,
  JAHRESURLAUB float default NULL,
  GUELTIG_VON datetime default NULL,
  GUELTIG_BIS datetime default NULL,
  GEBDATUM datetime default NULL,
  GESCHLECHT enum('M','W') NOT NULL default 'M',
  FAMSTAND smallint(6) default NULL,
  BANK varchar(40) default NULL,
  BLZ varchar(10) default NULL,
  KTO varchar(20) default NULL,
  BEMERKUNG text,
  USERFELD_01 VARCHAR(255),
  USERFELD_02 VARCHAR(255),
  USERFELD_03 VARCHAR(255),
  USERFELD_04 VARCHAR(255),
  USERFELD_05 VARCHAR(255),
  USERFELD_06 VARCHAR(255),
  USERFELD_07 VARCHAR(255),
  USERFELD_08 VARCHAR(255),
  USERFELD_09 VARCHAR(255),
  USERFELD_10 VARCHAR(255),
  ERSTELLT datetime default NULL,
  ERSTELLT_NAME varchar(20) default NULL,
  GEAEND datetime default NULL,
  GEAEND_NAME varchar(20) default NULL,
  PRIMARY KEY  (MA_ID),
  UNIQUE KEY IDX_MA_NUM (MA_NUMMER),
  UNIQUE KEY IDX_LOGINNAME (LOGIN_NAME)
);

CREATE TABLE IF NOT EXISTS PIM_AUFGABEN (
  RECORDID int(11) NOT NULL auto_increment,
  RESOURCEID int(11) NOT NULL default '0',
  COMPLETE enum('N','Y') default 'N',
  DESCRIPTION char(255) default NULL,
  DETAILS char(255) default NULL,
  CREATEDON DATETIME,
  PRIORITY int(11) default NULL,
  CATEGORY int(11) default NULL,
  COMPLETEDON DATETIME,
  DUEDATE DATETIME,
  USERFIELD0 char(100) default NULL,
  USERFIELD1 char(100) default NULL,
  USERFIELD2 char(100) default NULL,
  USERFIELD3 char(100) default NULL,
  USERFIELD4 char(100) default NULL,
  USERFIELD5 char(100) default NULL,
  USERFIELD6 char(100) default NULL,
  USERFIELD7 char(100) default NULL,
  USERFIELD8 char(100) default NULL,
  USERFIELD9 char(100) default NULL,
  PRIMARY KEY (RECORDID),
  KEY ResID_ndx(RESOURCEID),
  KEY DueDate(DUEDATE),
  KEY CompletedOn(COMPLETEDON)
);

CREATE TABLE IF NOT EXISTS PIM_KONTAKTE (
  RECORDID int(11) NOT NULL auto_increment,
  RESOURCEID int(11) NOT NULL default '0',
  FIRSTNAME char(50) default NULL,
  LASTNAME char(50) NOT NULL default '',
  BIRTHDATE date default NULL,
  ANNIVERSARY date default NULL,
  TITLE char(50) default NULL,
  COMPANY char(50) NOT NULL default '',
  JOB_POSITION char(30) default NULL,
  ADDRESS char(100) default NULL,
  CITY char(50) default NULL,
  STATE char(25) default NULL,
  ZIP char(10) default NULL,
  COUNTRY char(25) default NULL,
  NOTE char(255) default NULL,
  PHONE1 char(25) default NULL,
  PHONE2 char(25) default NULL,
  PHONE3 char(25) default NULL,
  PHONE4 char(25) default NULL,
  PHONE5 char(25) default NULL,
  PHONETYPE1 int(11) default NULL,
  PHONETYPE2 int(11) default NULL,
  PHONETYPE3 int(11) default NULL,
  PHONETYPE4 int(11) default NULL,
  PHONETYPE5 int(11) default NULL,
  CATEGORY int(11) default NULL,
  EMAIL char(100) default NULL,
  CUSTOM1 char(100) default NULL,
  CUSTOM2 char(100) default NULL,
  CUSTOM3 char(100) default NULL,
  CUSTOM4 char(100) default NULL,
  USERFIELD0 char(100) default NULL,
  USERFIELD1 char(100) default NULL,
  USERFIELD2 char(100) default NULL,
  USERFIELD3 char(100) default NULL,
  USERFIELD4 char(100) default NULL,
  USERFIELD5 char(100) default NULL,
  USERFIELD6 char(100) default NULL,
  USERFIELD7 char(100) default NULL,
  USERFIELD8 char(100) default NULL,
  USERFIELD9 char(100) default NULL,
  PRIMARY KEY (RECORDID),
  KEY ResID_ndx(RESOURCEID),
  KEY LName_ndx(LASTNAME),
  KEY Company_ndx(COMPANY)
);

CREATE TABLE IF NOT EXISTS PIM_TERMINE (
  RECORDID int(11) NOT NULL auto_increment,
  STARTTIME DATETIME,
  ENDTIME DATETIME,
  RESOURCEID int(11) NOT NULL default '0',
  DESCRIPTION char(255) default NULL,
  NOTES char(255) default NULL,
  CATEGORY int(11) default NULL,
  ALLDAYEVENT enum('N','Y') NOT NULL default 'N',
  DINGPATH char(255) default NULL,
  ALARMSET enum('N','Y') NOT NULL default 'N',
  ALARMADVANCE int(11) default NULL,
  ALARMADVANCETYPE int(11) default NULL,
  SNOOZETIME DATETIME,
  REPEATCODE int(11) default NULL,
  REPEATRANGEEND DATETIME,
  CUSTOMINTERVAL int(11) default NULL,
  USERFIELD0 char(100) default NULL,
  USERFIELD1 char(100) default NULL,
  USERFIELD2 char(100) default NULL,
  USERFIELD3 char(100) default NULL,
  USERFIELD4 char(100) default NULL,
  USERFIELD5 char(100) default NULL,
  USERFIELD6 char(100) default NULL,
  USERFIELD7 char(100) default NULL,
  USERFIELD8 char(100) default NULL,
  USERFIELD9 char(100) default NULL,
  PRIMARY KEY (RECORDID),
  KEY rid_st_ndx(RESOURCEID,STARTTIME),
  KEY st_ndx(STARTTIME),
  KEY et_ndx(ENDTIME),
  KEY ResID_ndx(RESOURCEID)
);


ALTER TABLE VERTRETER
 CHANGE VERTR_VNAME VNAME VARCHAR(30),
 CHANGE VERTR_NAME NAME VARCHAR(30),
 ADD VERTR_NUMMER varchar(10) default NULL AFTER VERTRETER_ID,
 ADD ANREDE varchar(15) default NULL,
 ADD TITEL varchar(15) default NULL,
 ADD ZUSATZ varchar(40) default NULL,
 ADD ZUSATZ2 varchar(40) default NULL,
 ADD ZUHAENDEN varchar(40) default NULL,
 ADD STRASSE varchar(40) default NULL,
 ADD LAND varchar(5) default NULL,
 ADD PLZ varchar(10) default NULL,
 ADD ORT varchar(40) default NULL,
 ADD TELEFON varchar(100) default NULL,
 ADD FAX varchar(100) default NULL,
 ADD FUNK varchar(100) default NULL,
 ADD EMAIL varchar(100) default NULL,
 ADD INTERNET varchar(100) default NULL,
 ADD SPRACH_ID smallint(6) default '2',
 ADD PROVISIONSART char(1) default NULL,
 ADD ABRECHNUNGSZEITPUNKT enum('R','Z') default 'Z',
 ADD PROVISIONMITTRANSPORT enum('Y','N') default 'N',
 ADD PROVISIONSATZ float(5,2) default NULL,
 ADD LETZTEABRECHNUNG date default NULL,
 ADD UMSATZ float(12,2) default NULL,
 ADD PROVISION float(10,2) default NULL,
 ADD BESCHAEFTIGUNGSART smallint(6) default NULL,
 ADD BESCHAEFTIGUNGSGRAD smallint(6) default NULL,
 ADD GUELTIG_VON datetime default NULL,
 ADD GUELTIG_BIS datetime default NULL,
 ADD GEBDATUM datetime default NULL,
 ADD GESCHLECHT enum('M','W') NOT NULL default 'M',
 ADD FAMSTAND smallint(6) default NULL,
 ADD BANK varchar(40) default NULL,
 ADD KTO_INHABER VARCHAR(40) default NULL,
 ADD BLZ varchar(10) default NULL,
 ADD KTO varchar(20) default NULL,
 ADD BEMERKUNG text,
 ADD USERFELD_01 VARCHAR(255),
 ADD USERFELD_02 VARCHAR(255),
 ADD USERFELD_03 VARCHAR(255),
 ADD USERFELD_04 VARCHAR(255),
 ADD USERFELD_05 VARCHAR(255),
 ADD USERFELD_06 VARCHAR(255),
 ADD USERFELD_07 VARCHAR(255),
 ADD USERFELD_08 VARCHAR(255),
 ADD USERFELD_09 VARCHAR(255),
 ADD USERFELD_10 VARCHAR(255),
 ADD ERSTELLT datetime default NULL,
 ADD ERSTELLT_NAME varchar(20) default NULL,
 ADD GEAEND datetime default NULL,
 ADD GEAEND_NAME varchar(20) default NULL,
 ADD UNIQUE KEY IDX_VE_NUM (VERTR_NUMMER);

CREATE TABLE IF NOT EXISTS ADRESSEN_MERK (
 MERKMAL_ID int(11) NOT NULL auto_increment,
 NAME varchar(100) not NULL,
 PRIMARY KEY  (MERKMAL_ID)
);

CREATE TABLE IF NOT EXISTS ARTIKEL_MERK (
 MERKMAL_ID int(11) NOT NULL auto_increment,
 NAME varchar(100) not NULL,
 PRIMARY KEY  (MERKMAL_ID)
);

CREATE TABLE IF NOT EXISTS ADRESSEN_TO_MERK (
 MERKMAL_ID INT (11) UNSIGNED NOT NULL,
 ADDR_ID INT (11) UNSIGNED NOT NULL,
 PRIMARY KEY(MERKMAL_ID,ADDR_ID)
);

CREATE TABLE IF NOT EXISTS ARTIKEL_TO_MERK (
 MERKMAL_ID INT (11) UNSIGNED NOT NULL,
 ARTIKEL_ID INT (11) UNSIGNED NOT NULL,
 PRIMARY KEY(MERKMAL_ID,ARTIKEL_ID)
);


INSERT INTO REGISTERY (MAINKEY, NAME, VAL_INT, VAL_TYP, READONLY) VALUES("SHOP", "DEFAULT_DEBNUM", 0, 3, 'N');

INSERT INTO REGISTERY (MAINKEY, NAME, VAL_INT, VAL_TYP, READONLY) VALUES ('KASSE','BONDRUCKER',1,3,'N');
INSERT INTO REGISTERY (MAINKEY, NAME, VAL_INT, VAL_TYP, READONLY) VALUES ('KASSE','DRUCKDIALOG',1,3,'N');
INSERT INTO REGISTERY (MAINKEY, NAME, VAL_INT, VAL_TYP, READONLY) VALUES ('KASSE','SOFORTDRUCK',0,3,'N');
INSERT INTO REGISTERY (MAINKEY, NAME, VAL_CHAR, VAL_TYP, READONLY) VALUES ('KASSE','DEFAULT_FORMULAR','',1,'N');
INSERT INTO REGISTERY (MAINKEY, NAME, VAL_CHAR, VAL_TYP, READONLY) VALUES ('KASSE','DEFAULT_DRUCKER','Default',1,'N');



update REGISTERY
 SET VAL_INT2 = VAL_CURR
 WHERE MAINKEY='MAIN\\NUMBERS';

update REGISTERY
 SET VAL_INT2 = VAL_DOUBLE, VAL_DOUBLE = VAL_CURR
 WHERE MAINKEY='MAIN\\ZAHLART';

ALTER TABLE REGISTERY
 DROP VAL_CURR,
 CHANGE VAL_INT2 VAL_INT2 BIGINT,
 CHANGE VAL_INT3 VAL_INT3 BIGINT;

update REGISTERY SET VAL_INT3=LENGTH(VAL_CHAR), VAL_TYP=3
 where MAINKEY='MAIN\\NUMBERS';

update ARTIKEL SET VPE=0 WHERE ARTIKELTYP='L' or ARTIKELTYP='T';


/* Update Versionsnummer */
DELETE FROM REGISTERY where MAINKEY="MAIN" and NAME="DB_VERSION";
INSERT INTO REGISTERY (MAINKEY, NAME, VAL_CHAR, VAL_TYP) Values ('MAIN', 'DB_VERSION', '1.08', '1');
