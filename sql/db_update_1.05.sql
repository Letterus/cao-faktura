ALTER TABLE `JOURNALPOS` ADD INDEX QUELLE_SRC (QUELLE_SRC);

ALTER TABLE `ARTIKEL` CHANGE `MATCHCODE` `MATCHCODE` VARCHAR(255);

ALTER TABLE `INFO` ADD `QUELL_ID` INT(11)  DEFAULT "-1" NOT NULL AFTER `QUELLE`;

ALTER TABLE `ADRESSEN`
 CHANGE `KTO` `KTO` VARCHAR(20),
 CHANGE `BLZ` `BLZ` VARCHAR(20),
 CHANGE `MATCHCODE` `MATCHCODE` VARCHAR(255);


INSERT INTO REGISTERY (NAME, VAL_TYP, MAINKEY, VAL_CHAR) VALUES ('VK_AGB_MAIL_SUBJECT', 1, 'MAIN\\REPORT',NULL);
INSERT INTO REGISTERY (NAME, VAL_TYP, MAINKEY, VAL_CHAR) VALUES ('VK_RECH_MAIL_SUBJECT', 1, 'MAIN\\REPORT',NULL);
INSERT INTO REGISTERY (NAME, VAL_TYP, MAINKEY, VAL_CHAR) VALUES ('VK_LIEF_MAIL_SUBJECT', 1, 'MAIN\\REPORT',NULL);
INSERT INTO REGISTERY (NAME, VAL_TYP, MAINKEY, VAL_CHAR) VALUES ('EK_BEST_MAIL_SUBJECT', 1, 'MAIN\\REPORT',NULL);

INSERT INTO REGISTERY (NAME, VAL_TYP, MAINKEY, VAL_CHAR) VALUES ('ZAHLUNGSZIEL_SOFORT_TEXT', 1, 'MAIN\\REPORT','Sofort');

/*06.07.2003*/

INSERT INTO REGISTERY (NAME, VAL_TYP, MAINKEY, VAL_CHAR) VALUES ('BON_UEBERSCHRIFT1', 1, 'KASSE', NULL);
INSERT INTO REGISTERY (NAME, VAL_TYP, MAINKEY, VAL_CHAR) VALUES ('BON_UEBERSCHRIFT2', 1, 'KASSE', NULL);
INSERT INTO REGISTERY (NAME, VAL_TYP, MAINKEY, VAL_CHAR) VALUES ('BON_UEBERSCHRIFT3', 1, 'KASSE', NULL);

INSERT INTO REGISTERY (MAINKEY, VAL_TYP, NAME, VAL_INT) VALUES ('MAIN\\ADRESSEN', 3, 'KTO_LEN', 10);
INSERT INTO REGISTERY (MAINKEY, VAL_TYP, NAME, VAL_INT) VALUES ('MAIN\\ADRESSEN', 3, 'BLZ_LEN', 8);

CREATE TABLE IF NOT EXISTS  VERTRAG (
  REC_ID int(11) NOT NULL auto_increment,
  ADDR_ID int(11) NOT NULL default '-1',
  VVTNUM int(11) NOT NULL default '-1',
  VERTRETER_ID int(11) NOT NULL default '-1',
  GLOBRABATT float(10,2) NOT NULL default '0.00',
  DATUM_START date NOT NULL default '0000-00-00',
  DATUM_ENDE date NOT NULL default '0000-00-00',
  DAUM_NEXT date NOT NULL default '0000-00-00',
  INTERVALL char(1) NOT NULL default 'M',
  INTERVALL_NUM tinyint(3) unsigned NOT NULL default '1',
  AKTIV_FLAG enum('N','Y') NOT NULL default 'N',
  PRINT_FLAG enum('N','Y') NOT NULL default 'N',
  PR_EBENE tinyint(4) default NULL,
  LIEFART tinyint(2) default NULL,
  ZAHLART tinyint(2) default NULL,
  KOST_NETTO float(10,2) NOT NULL default '0.00',
  WERT_NETTO float(10,2) NOT NULL default '0.00',
  LOHN float(10,2) NOT NULL default '0.00',
  WARE float(10,2) NOT NULL default '0.00',
  TKOST float(10,2) NOT NULL default '0.00',
  MWST_0 float(10,2) NOT NULL default '0.00',
  MWST_1 float(10,2) NOT NULL default '0.00',
  MWST_2 float(10,2) NOT NULL default '0.00',
  MWST_3 float(10,2) NOT NULL default '0.00',
  NSUMME float(10,2) NOT NULL default '0.00',
  MSUMME_0 float(10,2) NOT NULL default '0.00',
  MSUMME_1 float(10,2) NOT NULL default '0.00',
  MSUMME_2 float(10,2) NOT NULL default '0.00',
  MSUMME_3 float(10,2) NOT NULL default '0.00',
  MSUMME float(10,2) NOT NULL default '0.00',
  BSUMME float(10,2) NOT NULL default '0.00',
  ATSUMME float(10,2) NOT NULL default '0.00',
  ATMSUMME float(10,2) NOT NULL default '0.00',
  WAEHRUNG varchar(5) NOT NULL default '',
  GEGENKONTO int(11) NOT NULL default '-1',
  SOLL_STAGE tinyint(4) NOT NULL default '0',
  SOLL_SKONTO float(5,2) NOT NULL default '0.00',
  SOLL_NTAGE tinyint(4) NOT NULL default '0',
  SOLL_RATEN tinyint(4) NOT NULL default '1',
  SOLL_RATBETR float(10,2) NOT NULL default '0.00',
  SOLL_RATINTERVALL int(11) NOT NULL default '0',
  STADIUM tinyint(4) NOT NULL default '0',
  ERSTELLT date default NULL,
  ERST_NAME varchar(20) default NULL,
  KUN_NUM varchar(20) default NULL,
  KUN_ANREDE varchar(30) default NULL,
  KUN_NAME1 varchar(30) default NULL,
  KUN_NAME2 varchar(30) default NULL,
  KUN_NAME3 varchar(30) default NULL,
  KUN_ABTEILUNG varchar(30) default NULL,
  KUN_STRASSE varchar(30) default NULL,
  KUN_LAND char(2) default NULL,
  KUN_PLZ varchar(5) default NULL,
  KUN_ORT varchar(30) default NULL,
  USR1 varchar(80) default NULL,
  USR2 varchar(80) default NULL,
  PROJEKT varchar(80) default NULL,
  ORGNUM varchar(20) default NULL,
  BEST_NAME varchar(40) default NULL,
  BEST_CODE tinyint(4) default NULL,
  BEST_DATUM date default NULL,
  INFO text,
  BRUTTO_FLAG enum('N','Y') NOT NULL default 'N',
  MWST_FREI_FLAG enum('N','Y') NOT NULL default 'N',
  PRIMARY KEY  (REC_ID),
  KEY ADDR_ID (ADDR_ID)
);


CREATE TABLE IF NOT EXISTS VERTRAGPOS (
  REC_ID int(11) NOT NULL auto_increment,
  JOURNAL_ID int(11) NOT NULL default '0',
  ARTIKELTYP char(1) NOT NULL default '',
  ARTIKEL_ID int(11) NOT NULL default '-1',
  ADDR_ID int(11) NOT NULL default '-1',
  VVTNUM int(11) NOT NULL default '-1',
  POSITION int(11) NOT NULL default '0',
  VIEW_POS char(3) default NULL,
  MATCHCODE varchar(20) default NULL,
  ARTNUM varchar(20) default NULL,
  BARCODE varchar(20) default NULL,
  MENGE float(10,2) NOT NULL default '0.00',
  LAENGE varchar(20) default NULL,
  GROESSE varchar(20) default NULL,
  DIMENSION varchar(20) default NULL,
  GEWICHT float(10,3) NOT NULL default '0.000',
  ME_EINHEIT varchar(10) default NULL,
  PR_EINHEIT float(10,3) NOT NULL default '1.000',
  EPREIS float(10,3) NOT NULL default '0.000',
  E_RGEWINN float(10,2) NOT NULL default '0.00',
  RABATT float(10,2) NOT NULL default '0.00',
  RABATT2 float(10,2) NOT NULL default '0.00',
  RABATT3 float(10,2) NOT NULL default '0.00',
  STEUER_CODE tinyint(4) NOT NULL default '0',
  ALTTEIL_PROZ float(10,2) NOT NULL default '0.10',
  ALTTEIL_STCODE tinyint(4) NOT NULL default '0',
  GEGENKTO int(11) NOT NULL default '-1',
  BEZEICHNUNG text,
  ALTTEIL_FLAG enum('N','Y') NOT NULL default 'N',
  BEZ_FEST_FLAG enum('N','Y') NOT NULL default 'N',
  BRUTTO_FLAG enum('N','Y') NOT NULL default 'N',
  PRIMARY KEY  (REC_ID),
  KEY ARTIKEL_ID (ARTIKEL_ID),
  KEY ADDR_ID (ADDR_ID),
  KEY JOURNAL_ID (JOURNAL_ID,POSITION)
);

INSERT INTO REGISTERY VALUES("MAIN\\NUMBERS", "VERTRAG", "000000", NULL, "23", NULL, NULL, "1000.00", NULL, NULL, NULL, NULL, "0", "0", "20030513214242");

DROP TABLE IF EXISTS LAND;
CREATE TABLE LAND (
  ID char(2) NOT NULL default '',
  NAME varchar(100) NOT NULL default '',
  ISO_CODE_3 char(3) NOT NULL default '',
  FORMAT tinyint(3) NOT NULL default '1',
  VORWAHL varchar(10) default NULL,
  WAEHRUNG varchar(5) default NULL,
  SPRACHE char(3) default NULL,
  PRIMARY KEY  (ID),
  UNIQUE KEY IDX_NAME (NAME)
);

DROP TABLE IF EXISTS BLZ;
CREATE TABLE BLZ (
  BLZ int(10) NOT NULL default '0',
  BANK_NAME varchar(255) NOT NULL default '',
  PRZ char(2) NOT NULL default '',
  PRIMARY KEY  (BLZ),
  KEY IDX_NAME (BANK_NAME,BLZ)
);

/* Update Versionsnummer */
DELETE FROM REGISTERY where MAINKEY="MAIN" and NAME="DB_VERSION";
INSERT INTO REGISTERY (MAINKEY, NAME, VAL_CHAR) Values ('MAIN', 'DB_VERSION', '1.05');