object AdrSuchForm: TAdrSuchForm
  Left = 327
  Top = 120
  BorderStyle = bsDialog
  Caption = 'erweiterte Adressensuche (aus einer DLL)'
  ClientHeight = 620
  ClientWidth = 635
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object BitBtn1: TBitBtn
    Left = 519
    Top = 588
    Width = 106
    Height = 25
    TabOrder = 1
    OnClick = BitBtn1Click
    Kind = bkOK
  end
  object BitBtn2: TBitBtn
    Left = 407
    Top = 588
    Width = 105
    Height = 25
    TabOrder = 2
    Kind = bkCancel
  end
  object JvPageControl1: TJvPageControl
    Left = 8
    Top = 16
    Width = 617
    Height = 565
    ActivePage = TabSheet1
    TabOrder = 3
    object TabSheet1: TTabSheet
      Caption = 'Von-Bis'
      object Label1: TLabel
        Left = 220
        Top = 8
        Width = 48
        Height = 13
        Caption = 'Von / In'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label2: TLabel
        Left = 416
        Top = 8
        Width = 18
        Height = 13
        Caption = 'Bis'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label3: TLabel
        Left = 12
        Top = 268
        Width = 70
        Height = 13
        Caption = 'Ausschliessen:'
      end
      object Label4: TLabel
        Left = 12
        Top = 8
        Width = 43
        Height = 13
        Caption = 'Auswahl:'
      end
      object Label11: TLabel
        Left = 184
        Top = 8
        Width = 22
        Height = 13
        Hint = 
          'M�gliche Datentypen'#13#10'T = Text/Blob'#13#10'A = Alphanumerisch'#13#10'N = Nume' +
          'risch'#13#10'D = Datum, Date'#13#10'J  = Ja/Nein, Yes/No'
        Caption = 'Typ'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object JvNoCB1: TJvComboBox
        Left = 12
        Top = 284
        Width = 165
        Height = 19
        MaxPixel.Font.Charset = DEFAULT_CHARSET
        MaxPixel.Font.Color = clWindowText
        MaxPixel.Font.Height = -11
        MaxPixel.Font.Name = 'MS Sans Serif'
        MaxPixel.Font.Style = []
        Style = csOwnerDrawFixed
        ItemHeight = 13
        TabOrder = 40
        OnChange = JvNoCB1Change
      end
      object JvNoCB2: TJvComboBox
        Left = 12
        Top = 308
        Width = 165
        Height = 19
        MaxPixel.Font.Charset = DEFAULT_CHARSET
        MaxPixel.Font.Color = clWindowText
        MaxPixel.Font.Height = -11
        MaxPixel.Font.Name = 'MS Sans Serif'
        MaxPixel.Font.Style = []
        Style = csOwnerDrawFixed
        ItemHeight = 13
        TabOrder = 44
        OnChange = JvNoCB2Change
      end
      object JvNoCB3: TJvComboBox
        Left = 12
        Top = 332
        Width = 165
        Height = 19
        MaxPixel.Font.Charset = DEFAULT_CHARSET
        MaxPixel.Font.Color = clWindowText
        MaxPixel.Font.Height = -11
        MaxPixel.Font.Name = 'MS Sans Serif'
        MaxPixel.Font.Style = []
        Style = csOwnerDrawFixed
        ItemHeight = 13
        TabOrder = 48
        OnChange = JvNoCB3Change
      end
      object JvNoCB4: TJvComboBox
        Left = 12
        Top = 356
        Width = 165
        Height = 19
        MaxPixel.Font.Charset = DEFAULT_CHARSET
        MaxPixel.Font.Color = clWindowText
        MaxPixel.Font.Height = -11
        MaxPixel.Font.Name = 'MS Sans Serif'
        MaxPixel.Font.Style = []
        Style = csOwnerDrawFixed
        ItemHeight = 13
        TabOrder = 52
        OnChange = JvNoCB4Change
      end
      object JvNoCB5: TJvComboBox
        Left = 12
        Top = 380
        Width = 165
        Height = 19
        MaxPixel.Font.Charset = DEFAULT_CHARSET
        MaxPixel.Font.Color = clWindowText
        MaxPixel.Font.Height = -11
        MaxPixel.Font.Name = 'MS Sans Serif'
        MaxPixel.Font.Style = []
        Style = csOwnerDrawFixed
        ItemHeight = 13
        TabOrder = 56
        OnChange = JvNoCB5Change
      end
      object JvNoCB1VON: TEdit
        Left = 220
        Top = 284
        Width = 185
        Height = 21
        Hint = 'Bitte geben Sie Ihre Artikelnummer ein  '
        TabOrder = 42
      end
      object JvNoCB2VON: TEdit
        Left = 220
        Top = 308
        Width = 185
        Height = 21
        TabOrder = 46
      end
      object JvNoCB3VON: TEdit
        Left = 220
        Top = 332
        Width = 185
        Height = 21
        TabOrder = 50
      end
      object JvNoCB4VON: TEdit
        Left = 220
        Top = 356
        Width = 185
        Height = 21
        TabOrder = 54
      end
      object JvNoCB5VON: TEdit
        Left = 220
        Top = 380
        Width = 185
        Height = 21
        TabOrder = 58
      end
      object JvNoCB1BIS: TEdit
        Left = 416
        Top = 284
        Width = 185
        Height = 21
        Hint = 'Bitte denken Sie daran, das der zweite Wert h�her ist!'
        TabOrder = 43
      end
      object JvNoCB2BIS: TEdit
        Left = 416
        Top = 308
        Width = 185
        Height = 21
        TabOrder = 47
      end
      object JvNoCB3BIS: TEdit
        Left = 416
        Top = 332
        Width = 185
        Height = 21
        TabOrder = 51
      end
      object JvNoCB4BIS: TEdit
        Left = 416
        Top = 356
        Width = 185
        Height = 21
        TabOrder = 55
      end
      object JvNoCB5BIS: TEdit
        Left = 416
        Top = 380
        Width = 185
        Height = 21
        TabOrder = 59
      end
      object JvCB1: TJvComboBox
        Left = 12
        Top = 24
        Width = 165
        Height = 19
        TabStop = False
        MaxPixel.Font.Charset = DEFAULT_CHARSET
        MaxPixel.Font.Color = clWindowText
        MaxPixel.Font.Height = -11
        MaxPixel.Font.Name = 'MS Sans Serif'
        MaxPixel.Font.Style = []
        Style = csOwnerDrawFixed
        ItemHeight = 13
        TabOrder = 0
        OnChange = JvCB1Change
      end
      object JvCB2: TJvComboBox
        Left = 12
        Top = 48
        Width = 165
        Height = 19
        MaxPixel.Font.Charset = DEFAULT_CHARSET
        MaxPixel.Font.Color = clWindowText
        MaxPixel.Font.Height = -11
        MaxPixel.Font.Name = 'MS Sans Serif'
        MaxPixel.Font.Style = []
        Style = csOwnerDrawFixed
        ItemHeight = 13
        TabOrder = 4
        OnChange = JvCB2Change
      end
      object JvCB3: TJvComboBox
        Left = 12
        Top = 72
        Width = 165
        Height = 19
        MaxPixel.Font.Charset = DEFAULT_CHARSET
        MaxPixel.Font.Color = clWindowText
        MaxPixel.Font.Height = -11
        MaxPixel.Font.Name = 'MS Sans Serif'
        MaxPixel.Font.Style = []
        Style = csOwnerDrawFixed
        ItemHeight = 13
        TabOrder = 8
        OnChange = JvCB3Change
      end
      object JvCB4: TJvComboBox
        Left = 12
        Top = 96
        Width = 165
        Height = 19
        MaxPixel.Font.Charset = DEFAULT_CHARSET
        MaxPixel.Font.Color = clWindowText
        MaxPixel.Font.Height = -11
        MaxPixel.Font.Name = 'MS Sans Serif'
        MaxPixel.Font.Style = []
        Style = csOwnerDrawFixed
        ItemHeight = 13
        TabOrder = 12
        OnChange = JvCB4Change
      end
      object JvCB5: TJvComboBox
        Left = 12
        Top = 120
        Width = 165
        Height = 19
        MaxPixel.Font.Charset = DEFAULT_CHARSET
        MaxPixel.Font.Color = clWindowText
        MaxPixel.Font.Height = -11
        MaxPixel.Font.Name = 'MS Sans Serif'
        MaxPixel.Font.Style = []
        Style = csOwnerDrawFixed
        ItemHeight = 13
        TabOrder = 16
        OnChange = JvCB5Change
      end
      object JvCB1VON: TEdit
        Left = 220
        Top = 24
        Width = 185
        Height = 21
        Hint = 'Bitte geben Sie Ihre Artikelnummer ein  '
        TabOrder = 2
      end
      object JvCB2VON: TEdit
        Left = 220
        Top = 48
        Width = 185
        Height = 21
        TabOrder = 6
      end
      object JvCB3VON: TEdit
        Left = 220
        Top = 72
        Width = 185
        Height = 21
        TabOrder = 10
      end
      object JvCB4VON: TEdit
        Left = 220
        Top = 96
        Width = 185
        Height = 21
        TabOrder = 14
      end
      object JvCB5VON: TEdit
        Left = 220
        Top = 120
        Width = 185
        Height = 21
        TabOrder = 18
      end
      object JvCB1BIS: TEdit
        Left = 416
        Top = 24
        Width = 185
        Height = 21
        Hint = 'Bitte denken Sie daran, das der zweite Wert h�her ist!'
        TabOrder = 3
      end
      object JvCB2BIS: TEdit
        Left = 416
        Top = 48
        Width = 185
        Height = 21
        TabOrder = 7
      end
      object JvCB3BIS: TEdit
        Left = 416
        Top = 72
        Width = 185
        Height = 21
        TabOrder = 11
      end
      object JvCB4BIS: TEdit
        Left = 416
        Top = 96
        Width = 185
        Height = 21
        TabOrder = 15
      end
      object JvCB5BIS: TEdit
        Left = 416
        Top = 120
        Width = 185
        Height = 21
        TabOrder = 19
      end
      object JvCB6: TJvComboBox
        Left = 12
        Top = 144
        Width = 165
        Height = 19
        MaxPixel.Font.Charset = DEFAULT_CHARSET
        MaxPixel.Font.Color = clWindowText
        MaxPixel.Font.Height = -11
        MaxPixel.Font.Name = 'MS Sans Serif'
        MaxPixel.Font.Style = []
        Style = csOwnerDrawFixed
        ItemHeight = 13
        TabOrder = 20
        OnChange = JvCB6Change
      end
      object JvCB7: TJvComboBox
        Left = 12
        Top = 168
        Width = 165
        Height = 19
        MaxPixel.Font.Charset = DEFAULT_CHARSET
        MaxPixel.Font.Color = clWindowText
        MaxPixel.Font.Height = -11
        MaxPixel.Font.Name = 'MS Sans Serif'
        MaxPixel.Font.Style = []
        Style = csOwnerDrawFixed
        ItemHeight = 13
        TabOrder = 24
        OnChange = JvCB7Change
      end
      object JvCB8: TJvComboBox
        Left = 12
        Top = 192
        Width = 165
        Height = 19
        MaxPixel.Font.Charset = DEFAULT_CHARSET
        MaxPixel.Font.Color = clWindowText
        MaxPixel.Font.Height = -11
        MaxPixel.Font.Name = 'MS Sans Serif'
        MaxPixel.Font.Style = []
        Style = csOwnerDrawFixed
        ItemHeight = 13
        TabOrder = 28
        OnChange = JvCB8Change
      end
      object JvCB9: TJvComboBox
        Left = 12
        Top = 216
        Width = 165
        Height = 19
        MaxPixel.Font.Charset = DEFAULT_CHARSET
        MaxPixel.Font.Color = clWindowText
        MaxPixel.Font.Height = -11
        MaxPixel.Font.Name = 'MS Sans Serif'
        MaxPixel.Font.Style = []
        Style = csOwnerDrawFixed
        ItemHeight = 13
        TabOrder = 32
        OnChange = JvCB9Change
      end
      object JvCB10: TJvComboBox
        Left = 12
        Top = 240
        Width = 165
        Height = 19
        MaxPixel.Font.Charset = DEFAULT_CHARSET
        MaxPixel.Font.Color = clWindowText
        MaxPixel.Font.Height = -11
        MaxPixel.Font.Name = 'MS Sans Serif'
        MaxPixel.Font.Style = []
        Style = csOwnerDrawFixed
        ItemHeight = 13
        TabOrder = 36
        OnChange = JvCB10Change
      end
      object JvCB6VON: TEdit
        Left = 220
        Top = 144
        Width = 185
        Height = 21
        TabOrder = 22
      end
      object JvCB7VON: TEdit
        Left = 220
        Top = 168
        Width = 185
        Height = 21
        TabOrder = 26
      end
      object JvCB8VON: TEdit
        Left = 220
        Top = 192
        Width = 185
        Height = 21
        TabOrder = 30
      end
      object JvCB9VON: TEdit
        Left = 220
        Top = 216
        Width = 185
        Height = 21
        TabOrder = 34
      end
      object JvCB10VON: TEdit
        Left = 220
        Top = 240
        Width = 185
        Height = 21
        TabOrder = 38
      end
      object JvCB6BIS: TEdit
        Left = 416
        Top = 144
        Width = 185
        Height = 21
        TabOrder = 23
      end
      object JvCB7BIS: TEdit
        Left = 416
        Top = 168
        Width = 185
        Height = 21
        TabOrder = 27
      end
      object JvCB8BIS: TEdit
        Left = 416
        Top = 192
        Width = 185
        Height = 21
        TabOrder = 31
      end
      object JvCB9BIS: TEdit
        Left = 416
        Top = 216
        Width = 185
        Height = 21
        TabOrder = 35
      end
      object JvCB10BIS: TEdit
        Left = 416
        Top = 240
        Width = 185
        Height = 21
        TabOrder = 39
      end
      object cbSQLAnzeige: TCheckBox
        Left = 520
        Top = 520
        Width = 85
        Height = 17
        Caption = 'SQL-Anzeige'
        TabOrder = 60
      end
      object GroupBox1: TGroupBox
        Left = 12
        Top = 408
        Width = 589
        Height = 109
        Caption = 'Beispiele'
        TabOrder = 61
        object Label12: TLabel
          Left = 212
          Top = 92
          Width = 14
          Height = 14
          Caption = 'A*'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Courier New'
          Font.Style = []
          ParentFont = False
        end
        object Label23: TLabel
          Left = 405
          Top = 92
          Width = 21
          Height = 14
          Caption = 'F?*'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Courier New'
          Font.Style = []
          ParentFont = False
        end
        object Label9: TLabel
          Left = 8
          Top = 60
          Width = 147
          Height = 15
          Caption = 'Meier/Maier/Meiermann'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Courier New'
          Font.Style = [fsItalic]
          ParentFont = False
        end
        object Label17: TLabel
          Left = 8
          Top = 92
          Width = 161
          Height = 15
          Caption = 'Von Aachen bis Freiburg'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Courier New'
          Font.Style = [fsItalic]
          ParentFont = False
        end
        object Label24: TLabel
          Left = 212
          Top = 12
          Width = 28
          Height = 14
          Caption = '1000'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Courier New'
          Font.Style = []
          ParentFont = False
        end
        object Label25: TLabel
          Left = 212
          Top = 28
          Width = 70
          Height = 14
          Caption = '1,3,5,8,15'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Courier New'
          Font.Style = []
          ParentFont = False
        end
        object Label26: TLabel
          Left = 212
          Top = 60
          Width = 42
          Height = 14
          Caption = 'M_ier*'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Courier New'
          Font.Style = []
          ParentFont = False
        end
        object Label27: TLabel
          Left = 212
          Top = 44
          Width = 49
          Height = 14
          Caption = 'A,C,F,Z'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Courier New'
          Font.Style = []
          ParentFont = False
        end
        object Label28: TLabel
          Left = 405
          Top = 12
          Width = 28
          Height = 14
          Caption = '3000'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Courier New'
          Font.Style = []
          ParentFont = False
        end
        object Label29: TLabel
          Left = 403
          Top = 44
          Width = 98
          Height = 14
          Caption = '<ohne Eingabe>'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Courier New'
          Font.Style = []
          ParentFont = False
        end
        object Label30: TLabel
          Left = 403
          Top = 60
          Width = 98
          Height = 14
          Caption = '<ohne Eingabe>'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Courier New'
          Font.Style = []
          ParentFont = False
        end
        object Label5: TLabel
          Left = 404
          Top = 28
          Width = 98
          Height = 14
          Caption = '<ohne Eingabe>'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Courier New'
          Font.Style = []
          ParentFont = False
        end
        object Label6: TLabel
          Left = 8
          Top = 76
          Width = 133
          Height = 15
          Caption = 'Alle eMails von GMX'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Courier New'
          Font.Style = [fsItalic]
          ParentFont = False
        end
        object Label7: TLabel
          Left = 212
          Top = 76
          Width = 35
          Height = 14
          Caption = '*GMX*'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Courier New'
          Font.Style = []
          ParentFont = False
        end
        object Label8: TLabel
          Left = 403
          Top = 76
          Width = 98
          Height = 14
          Caption = '<ohne Eingabe>'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Courier New'
          Font.Style = []
          ParentFont = False
        end
      end
      object JvCB1Typ: TJvComboBox
        Left = 180
        Top = 24
        Width = 37
        Height = 21
        MaxPixel.Font.Charset = DEFAULT_CHARSET
        MaxPixel.Font.Color = clWindowText
        MaxPixel.Font.Height = -11
        MaxPixel.Font.Name = 'MS Sans Serif'
        MaxPixel.Font.Style = []
        ItemHeight = 13
        TabOrder = 1
      end
      object JvCB2Typ: TJvComboBox
        Left = 180
        Top = 48
        Width = 37
        Height = 21
        MaxPixel.Font.Charset = DEFAULT_CHARSET
        MaxPixel.Font.Color = clWindowText
        MaxPixel.Font.Height = -11
        MaxPixel.Font.Name = 'MS Sans Serif'
        MaxPixel.Font.Style = []
        ItemHeight = 13
        TabOrder = 5
      end
      object JvCB3Typ: TJvComboBox
        Left = 180
        Top = 72
        Width = 37
        Height = 21
        MaxPixel.Font.Charset = DEFAULT_CHARSET
        MaxPixel.Font.Color = clWindowText
        MaxPixel.Font.Height = -11
        MaxPixel.Font.Name = 'MS Sans Serif'
        MaxPixel.Font.Style = []
        ItemHeight = 13
        TabOrder = 9
      end
      object JvCB4Typ: TJvComboBox
        Left = 180
        Top = 96
        Width = 37
        Height = 21
        MaxPixel.Font.Charset = DEFAULT_CHARSET
        MaxPixel.Font.Color = clWindowText
        MaxPixel.Font.Height = -11
        MaxPixel.Font.Name = 'MS Sans Serif'
        MaxPixel.Font.Style = []
        ItemHeight = 13
        TabOrder = 13
      end
      object JvCB5Typ: TJvComboBox
        Left = 180
        Top = 120
        Width = 37
        Height = 21
        MaxPixel.Font.Charset = DEFAULT_CHARSET
        MaxPixel.Font.Color = clWindowText
        MaxPixel.Font.Height = -11
        MaxPixel.Font.Name = 'MS Sans Serif'
        MaxPixel.Font.Style = []
        ItemHeight = 13
        TabOrder = 17
      end
      object JvCB6Typ: TJvComboBox
        Left = 180
        Top = 144
        Width = 37
        Height = 21
        MaxPixel.Font.Charset = DEFAULT_CHARSET
        MaxPixel.Font.Color = clWindowText
        MaxPixel.Font.Height = -11
        MaxPixel.Font.Name = 'MS Sans Serif'
        MaxPixel.Font.Style = []
        ItemHeight = 13
        TabOrder = 21
      end
      object JvCB7Typ: TJvComboBox
        Left = 180
        Top = 168
        Width = 37
        Height = 21
        MaxPixel.Font.Charset = DEFAULT_CHARSET
        MaxPixel.Font.Color = clWindowText
        MaxPixel.Font.Height = -11
        MaxPixel.Font.Name = 'MS Sans Serif'
        MaxPixel.Font.Style = []
        ItemHeight = 13
        TabOrder = 25
      end
      object JvCB8Typ: TJvComboBox
        Left = 180
        Top = 192
        Width = 37
        Height = 21
        MaxPixel.Font.Charset = DEFAULT_CHARSET
        MaxPixel.Font.Color = clWindowText
        MaxPixel.Font.Height = -11
        MaxPixel.Font.Name = 'MS Sans Serif'
        MaxPixel.Font.Style = []
        ItemHeight = 13
        TabOrder = 29
      end
      object JvCB9Typ: TJvComboBox
        Left = 180
        Top = 216
        Width = 37
        Height = 21
        MaxPixel.Font.Charset = DEFAULT_CHARSET
        MaxPixel.Font.Color = clWindowText
        MaxPixel.Font.Height = -11
        MaxPixel.Font.Name = 'MS Sans Serif'
        MaxPixel.Font.Style = []
        ItemHeight = 13
        TabOrder = 33
      end
      object JvCB10Typ: TJvComboBox
        Left = 180
        Top = 240
        Width = 37
        Height = 21
        MaxPixel.Font.Charset = DEFAULT_CHARSET
        MaxPixel.Font.Color = clWindowText
        MaxPixel.Font.Height = -11
        MaxPixel.Font.Name = 'MS Sans Serif'
        MaxPixel.Font.Style = []
        ItemHeight = 13
        TabOrder = 37
      end
      object JvNoCB1Typ: TJvComboBox
        Left = 180
        Top = 284
        Width = 37
        Height = 21
        MaxPixel.Font.Charset = DEFAULT_CHARSET
        MaxPixel.Font.Color = clWindowText
        MaxPixel.Font.Height = -11
        MaxPixel.Font.Name = 'MS Sans Serif'
        MaxPixel.Font.Style = []
        ItemHeight = 13
        TabOrder = 41
      end
      object JvNoCB2Typ: TJvComboBox
        Left = 180
        Top = 308
        Width = 37
        Height = 21
        MaxPixel.Font.Charset = DEFAULT_CHARSET
        MaxPixel.Font.Color = clWindowText
        MaxPixel.Font.Height = -11
        MaxPixel.Font.Name = 'MS Sans Serif'
        MaxPixel.Font.Style = []
        ItemHeight = 13
        TabOrder = 45
      end
      object JvNoCB3Typ: TJvComboBox
        Left = 180
        Top = 332
        Width = 37
        Height = 21
        MaxPixel.Font.Charset = DEFAULT_CHARSET
        MaxPixel.Font.Color = clWindowText
        MaxPixel.Font.Height = -11
        MaxPixel.Font.Name = 'MS Sans Serif'
        MaxPixel.Font.Style = []
        ItemHeight = 13
        TabOrder = 49
      end
      object JvNoCB4Typ: TJvComboBox
        Left = 180
        Top = 356
        Width = 37
        Height = 21
        MaxPixel.Font.Charset = DEFAULT_CHARSET
        MaxPixel.Font.Color = clWindowText
        MaxPixel.Font.Height = -11
        MaxPixel.Font.Name = 'MS Sans Serif'
        MaxPixel.Font.Style = []
        ItemHeight = 13
        TabOrder = 53
      end
      object JvNoCB5Typ: TJvComboBox
        Left = 180
        Top = 380
        Width = 37
        Height = 21
        MaxPixel.Font.Charset = DEFAULT_CHARSET
        MaxPixel.Font.Color = clWindowText
        MaxPixel.Font.Height = -11
        MaxPixel.Font.Name = 'MS Sans Serif'
        MaxPixel.Font.Style = []
        ItemHeight = 13
        TabOrder = 57
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'Individuell'
      ImageIndex = 1
      object Memo1: TMemo
        Left = 19
        Top = 12
        Width = 558
        Height = 433
        Lines.Strings = (
          '(KUNNUM1 > 1 AND KUNNUM1 < 1000)')
        TabOrder = 0
      end
    end
  end
  object BtnClear: TButton
    Left = 8
    Top = 588
    Width = 85
    Height = 25
    Caption = 'Felder &l�schen'
    TabOrder = 0
    OnClick = BtnClearClick
  end
end
