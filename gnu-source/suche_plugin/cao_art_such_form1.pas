unit cao_art_such_form1;

{
--------------------------------------------------------------------------------
Programm     : CAO-Faktura
Modul        : CAO_art_such_form1
Author       : Norbert Herfurth
Stand        : 06.04.2004
Version      : 1.0.0.0
Beschreibung : Artikelsuche �ber externe Suche.DLL

History :

06.04.2004   - Version 1.0.0.0 released Norbert Herfurth
07.04.2004   - inklusiver individueller Datentypenauswahl
             - Kompakte Speicherung der Auswahlparemeter in Stringfeldern
               f�r bessere Performance
             - Einbindung eines Patches f�r Delphi 7 in Verbindung mit leeren
               Comboboxen (D7ComboBoxStringsGetPatch.pas)
Todo :       - �hnliche Funktionen/Prozeduren f�r leichtere Pflege zusammenlegen
--------------------------------------------------------------------------------
}

interface

uses
  {$IFDEF VER150}D7ComboBoxStringsGetPatch,{$ENDIF}
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, JvStrUtils, JvCombobox, ComCtrls, JvComCtrls, Buttons ,
  JvTabControl, DB, DBTables;

Type
    Feld = record
           Name: String;
           DBName: String;
           Typ: Char;
           end;
Const
    cSuche      = '\ARTIKEL\SUCHE';
    cUserFelder = 'MAIN\ARTIKEL\USERFELDER';
    cSeperate  : Char = '|';
    //-Datentypen---------------------------------------------------------------
    // T = MYSQL text
    // A = MySQL varchar
    // N = MySQL int, float
    // D = MySQL date
    // J = MySQL (Yes/No)
    //--------------------------------------------------------------------------

    cMaxFields = 53;
    cFirstUserFieldPos = 4;  //  Position von Benutzerfeld 01 in der Tabelle
    cFelder: Array[0..cMaxFields-1] of Feld =
           // Neue Felder bitte sortiert in die Tabelle aufnehmen
           ((Name:'';                 DBName:'';              Typ:' '),
            (Name:'Artikel-Nr';       DBName:'ARTNUM';        Typ:'A'),
            (Name:'Artikel-Typ';      DBName:'ARTIKELTYP';    Typ:'A'),
            (Name:'Barcode';          DBName:'BARCODE';       Typ:'A'),
            (Name:'Benutzerfeld 01';  DBName:'USERFELD_01';   Typ:'A'),
            (Name:'Benutzerfeld 02';  DBName:'USERFELD_02';   Typ:'A'),
            (Name:'Benutzerfeld 03';  DBName:'USERFELD_03';   Typ:'A'),
            (Name:'Benutzerfeld 04';  DBName:'USERFELD_04';   Typ:'A'),
            (Name:'Benutzerfeld 05';  DBName:'USERFELD_05';   Typ:'A'),
            (Name:'Benutzerfeld 06';  DBName:'USERFELD_06';   Typ:'A'),
            (Name:'Benutzerfeld 07';  DBName:'USERFELD_07';   Typ:'A'),
            (Name:'Benutzerfeld 08';  DBName:'USERFELD_08';   Typ:'A'),
            (Name:'Benutzerfeld 09';  DBName:'USERFELD_09';   Typ:'A'),
            (Name:'Benutzerfeld 10';  DBName:'USERFELD_10';   Typ:'A'),
            (Name:'Bestellvorschlag'; DBName:'MENGE_BVOR';    Typ:'N'),
            (Name:'Breite';           DBName:'';              Typ:' '),
            (Name:'Dimension';        DBName:'DIMENSION';     Typ:'A'),
            (Name:'EK-Preis';         DBName:'EK_PREIS';      Typ:'N'),
            (Name:'Ersatz-Nr';        DBName:'ERSATZ_ARTNUM'; Typ:'A'),
            (Name:'Erstellt am';      DBName:'ERSTELLT';      Typ:'D'),
            (Name:'Erstellt durch';   DBName:'ERST_NAME';     Typ:'A'),
            (Name:'Geaendert am';     DBName:'GEAEND';        Typ:'D'),
            (Name:'Geaendert durch';  DBName:'GEAEND_NAME';   Typ:'A'),
            (Name:'Gewicht';          DBName:'GEWICHT';       Typ:'N'),
            (Name:'Groesse';          DBName:'GROESSE';       Typ:'A'),
            (Name:'Herkunftsland';    DBName:'HERKUNFTSLAND'; Typ:'A'),
            (Name:'Herst-ArtNr';      DBName:'HERST_ARTNUM';  Typ:'A'),
            (Name:'Hersteller';       DBName:'HERSTELLER_ID'; Typ:'A'),
            (Name:'Inv-Wert';         DBName:'INVENTUR_WERT'; Typ:'N'),
            (Name:'Kassenname';       DBName:'KAS_NAME';      Typ:'A'),
            (Name:'Kurzname';         DBName:'KURZNAME';      Typ:'A'),
            (Name:'Laenge';           DBName:'LAENGE';        Typ:'A'),
            (Name:'Lager-Ort';        DBName:'LAGERORT';      Typ:'A'),
            (Name:'Langname';         DBName:'LANGNAME';      Typ:'T'),
            (Name:'Menge';            DBName:'MENGE_AKT';     Typ:'N'),
            (Name:'Menge bestellt';   DBName:'MENGE_BESTELLT';Typ:'N'),
            (Name:'ME-Einheit';       DBName:'ME_EINHEIT';    Typ:'A'),
            (Name:'Min-Bestand';      DBName:'MENGE_MIN';     Typ:'N'),
            (Name:'Rabattgruppe';     DBName:'RABGRP_ID';     Typ:'A'),
            (Name:'Suchbegriff';      DBName:'MATCHCODE';     Typ:'A'),
            (Name:'Vertr-Prov';       DBName:'PROVIS_PROZ';   Typ:'N'),
            (Name:'VK-Preis 1 Brutto';DBName:'VK1B';          Typ:'N'),
            (Name:'VK-Preis 1 Netto'; DBName:'VK1';           Typ:'N'),
            (Name:'VK-Preis 2 Brutto';DBName:'VK2B';          Typ:'N'),
            (Name:'VK-Preis 2 Netto'; DBName:'VK2';           Typ:'N'),
            (Name:'VK-Preis 3 Brutto';DBName:'VK3B';          Typ:'N'),
            (Name:'VK-Preis 3 Netto'; DBName:'VK3';           Typ:'N'),
            (Name:'VK-Preis 4 Brutto';DBName:'VK4B';          Typ:'N'),
            (Name:'VK-Preis 4 Netto'; DBName:'VK4';           Typ:'N'),
            (Name:'VK-Preis 5 Brutto';DBName:'VK5B';          Typ:'N'),
            (Name:'VK-Preis 5 Netto'; DBName:'VK5';           Typ:'N'),
            (Name:'VPE';              DBName:'VPE';           Typ:'N'),
            (Name:'Warengruppe';      DBName:'WARENGRUPPE';   Typ:'A'));


type
  TArtSuchForm = class(TForm)
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    JvPageControl1: TJvPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Memo1: TMemo;
    JvNoCB1: TJvComboBox;
    JvNoCB2: TJvComboBox;
    JvNoCB3: TJvComboBox;
    JvNoCB4: TJvComboBox;
    JvNoCB5: TJvComboBox;
    JvNoCB1VON: TEdit;
    JvNoCB2VON: TEdit;
    JvNoCB3VON: TEdit;
    JvNoCB4VON: TEdit;
    JvNoCB5VON: TEdit;
    JvNoCB1BIS: TEdit;
    JvNoCB2BIS: TEdit;
    JvNoCB3BIS: TEdit;
    JvNoCB4BIS: TEdit;
    JvNoCB5BIS: TEdit;
    JvCB1: TJvComboBox;
    JvCB2: TJvComboBox;
    JvCB3: TJvComboBox;
    JvCB4: TJvComboBox;
    JvCB5: TJvComboBox;
    JvCB1VON: TEdit;
    JvCB2VON: TEdit;
    JvCB3VON: TEdit;
    JvCB4VON: TEdit;
    JvCB5VON: TEdit;
    JvCB1BIS: TEdit;
    JvCB2BIS: TEdit;
    JvCB3BIS: TEdit;
    JvCB4BIS: TEdit;
    JvCB5BIS: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    JvCB6: TJvComboBox;
    JvCB7: TJvComboBox;
    JvCB8: TJvComboBox;
    JvCB9: TJvComboBox;
    JvCB10: TJvComboBox;
    JvCB6VON: TEdit;
    JvCB7VON: TEdit;
    JvCB8VON: TEdit;
    JvCB9VON: TEdit;
    JvCB10VON: TEdit;
    JvCB6BIS: TEdit;
    JvCB7BIS: TEdit;
    JvCB8BIS: TEdit;
    JvCB9BIS: TEdit;
    JvCB10BIS: TEdit;
    Label3: TLabel;
    Label4: TLabel;
    cbSQLAnzeige: TCheckBox;
    GroupBox1: TGroupBox;
    Label12: TLabel;
    Label23: TLabel;
    Label9: TLabel;
    Label17: TLabel;
    Label24: TLabel;
    Label25: TLabel;
    Label26: TLabel;
    Label27: TLabel;
    Label28: TLabel;
    Label29: TLabel;
    Label30: TLabel;
    Label5: TLabel;
    btnClear: TButton;
    JvCB1Typ: TJvComboBox;
    JvCB2Typ: TJvComboBox;
    JvCB3Typ: TJvComboBox;
    JvCB4Typ: TJvComboBox;
    JvCB5Typ: TJvComboBox;
    JvCB6Typ: TJvComboBox;
    JvCB7Typ: TJvComboBox;
    JvCB8Typ: TJvComboBox;
    JvCB9Typ: TJvComboBox;
    JvCB10Typ: TJvComboBox;
    JvNoCB1Typ: TJvComboBox;
    JvNoCB2Typ: TJvComboBox;
    JvNoCB3Typ: TJvComboBox;
    JvNoCB4Typ: TJvComboBox;
    JvNoCB5Typ: TJvComboBox;
    Label11: TLabel;
    procedure BitBtn1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnClearClick(Sender: TObject);
    procedure JvCB1Change(Sender: TObject);
    procedure JvCB2Change(Sender: TObject);
    procedure JvCB3Change(Sender: TObject);
    procedure JvCB4Change(Sender: TObject);
    procedure JvCB5Change(Sender: TObject);
    procedure JvCB6Change(Sender: TObject);
    procedure JvCB7Change(Sender: TObject);
    procedure JvCB8Change(Sender: TObject);
    procedure JvCB9Change(Sender: TObject);
    procedure JvCB10Change(Sender: TObject);
    procedure JvNoCB1Change(Sender: TObject);
    procedure JvNoCB2Change(Sender: TObject);
    procedure JvNoCB3Change(Sender: TObject);
    procedure JvNoCB4Change(Sender: TObject);
    procedure JvNoCB5Change(Sender: TObject);
  private
    { Private-Deklarationen }
 public
    { Public-Deklarationen }
    O : tHandle;
   end;

//var
  {ArtSuchForm: TArtSuchForm; }

implementation

{$R *.DFM}


uses cao_such_var;

function strReplace(const S: String; C: Char; const Replace: String): String;
var
  i : Integer;
begin
  Result:='';
  for i:=Length(S) downto 1 do
      if S[i]=C then Result:=Replace+Result else Result:=S[i]+Result;
end;

function strSeperate(instr:string;    // String to Seperate
                     seperator:char;  // Character f�r Seperation
                     Index:byte):string;
  var tmpcnt:byte;
      tmpout:string;
      i : Integer;
begin
   tmpcnt:=0;
   tmpout:='';
   i:=pos(seperator,instr);
   while (i>0) and (tmpcnt<Index) do
     begin
      inc(tmpcnt);
      if (i>1) and (tmpcnt=Index) then tmpout:=copy(instr,1,i-1);
      delete(instr,1,i);
      i:=pos(seperator,instr);
    end;
    if (length (instr)>0) and (tmpcnt+1=Index) then tmpout:=instr;
    strSeperate:=tmpout;
end;

procedure TArtSuchForm.FormCreate(Sender: TObject);
var
  Index: Integer;
  i: Integer;
  s: String;
  pc: PChar;
begin
  try
    // Userfelder auslesen und in die Tabelle an der richtigen Stelle eintragen

    if (Assigned (CaoRegistry.ReadBoolean)) and
       (Assigned (CaoRegistry.ReadString))  then
       begin
       if CaoRegistry.ReadBoolean(cUserFelder,'AKTIV',False) then
          for i:= 1 to 10 do
             begin
             pc:= PCHAR('FELD'+FORMAT('%2.2d',[i])+'_AKTIV');
             if CaoRegistry.ReadBoolean(cUserFelder, pc, False) then
                begin
                pc:= PChar('FELD'+FORMAT('%2.2d',[i]));
                cFelder[cFirstUserFieldPos+i-1].Name :=
                   CaoRegistry.ReadString(cUserFelder,pc, '???');
                end;
             end; {for}
       end;

    // L�sche die Inhalte der Comboboxen f�r die Feldnamen
    JvCB1.Clear;
    JvCB2.Clear;
    JvCB3.Clear;
    JvCB4.Clear;
    JvCB5.Clear;
    JvCB6.Clear;
    JvCB7.Clear;
    JvCB8.Clear;
    JvCB9.Clear;
    JvCB10.Clear;
    JvNoCB1.Clear;
    JvNoCB2.Clear;
    JvNoCB3.Clear;
    JvNoCB4.Clear;
    JvNoCB5.Clear;

    // L�sche die Inhalte der Comboboxen f�r die Datenfeldtypen
    JvCB1Typ.Clear;
    JvCB2Typ.Clear;
    JvCB3Typ.Clear;
    JvCB4Typ.Clear;
    JvCB5Typ.Clear;
    JvCB6Typ.Clear;
    JvCB7Typ.Clear;
    JvCB8Typ.Clear;
    JvCB9Typ.Clear;
    JvCB10Typ.Clear;
    JvNoCB1Typ.Clear;
    JvNoCB2Typ.Clear;
    JvNoCB3Typ.Clear;
    JvNoCB4Typ.Clear;
    JvNoCB5Typ.Clear;

  // F�lle die Comboboxen mit den Datenfeldnamen
    for Index := 0 to cMaxFields-1 do
    begin
    JvCB1.Items.Append(cFelder[Index].Name);
    JvCB2.Items.Append(cFelder[Index].Name);
    JvCB3.Items.Append(cFelder[Index].Name);
    JvCB4.Items.Append(cFelder[Index].Name);
    JvCB5.Items.Append(cFelder[Index].Name);
    JvCB6.Items.Append(cFelder[Index].Name);
    JvCB7.Items.Append(cFelder[Index].Name);
    JvCB8.Items.Append(cFelder[Index].Name);
    JvCB9.Items.Append(cFelder[Index].Name);
    JvCB10.Items.Append(cFelder[Index].Name);
    JvNoCB1.Items.Append(cFelder[Index].Name);
    JvNoCB2.Items.Append(cFelder[Index].Name);
    JvNoCB3.Items.Append(cFelder[Index].Name);
    JvNoCB4.Items.Append(cFelder[Index].Name);
    JvNoCB5.Items.Append(cFelder[Index].Name);
    end;


     // Individuelle Vorbelegungen des Users laden
    cbSQLAnzeige.Checked:= CaoRegistry.ReadBooleanU(PCHar(cSuche),PChar('SQLAnzeige'), False);

    s:=CaoRegistry.ReadStringU(PChar(cSuche), PChar('JvCBx'), PCHAR(''));
    if length(s) > 0 then
    begin
    JvCB1.ItemIndex:= strtoint(strSeperate(s,cSeperate,1));
    JvCB2.ItemIndex:= strtoint(strSeperate(s,cSeperate,2));
    JvCB3.ItemIndex:= strtoint(strSeperate(s,cSeperate,3));
    JvCB4.ItemIndex:= strtoint(strSeperate(s,cSeperate,4));
    JvCB5.ItemIndex:= strtoint(strSeperate(s,cSeperate,5));
    JvCB6.ItemIndex:= strtoint(strSeperate(s,cSeperate,6));
    JvCB7.ItemIndex:= strtoint(strSeperate(s,cSeperate,7));
    JvCB8.ItemIndex:= strtoint(strSeperate(s,cSeperate,8));
    JvCB9.ItemIndex:= strtoint(strSeperate(s,cSeperate,9));
    JvCB10.ItemIndex:=strtoint(strSeperate(s,cSeperate,10));
    end;

    s:=CaoRegistry.ReadStringU(PChar(cSuche), PChar('JvNoCBx'), PCHAR(''));
    if length(s) > 0 then
    begin
    JvNoCB1.ItemIndex:= strtoint(strSeperate(s,cSeperate,1));
    JvNoCB2.ItemIndex:= strtoint(strSeperate(s,cSeperate,2));
    JvNoCB3.ItemIndex:= strtoint(strSeperate(s,cSeperate,3));
    JvNoCB4.ItemIndex:= strtoint(strSeperate(s,cSeperate,4));
    JvNoCB5.ItemIndex:= strtoint(strSeperate(s,cSeperate,5));
    end;

    s:=CaoRegistry.ReadStringU(PChar(cSuche), PChar('JvCBxVON'), PCHAR(''));
    if length(s) > 0 then
    begin
    JvCB1VON.Text  := strSeperate(s,cSeperate,1);
    JvCB2VON.Text  := strSeperate(s,cSeperate,2);
    JvCB3VON.Text  := strSeperate(s,cSeperate,3);
    JvCB4VON.Text  := strSeperate(s,cSeperate,4);
    JvCB5VON.Text  := strSeperate(s,cSeperate,5);
    JvCB6VON.Text  := strSeperate(s,cSeperate,6);
    JvCB7VON.Text  := strSeperate(s,cSeperate,7);
    JvCB8VON.Text  := strSeperate(s,cSeperate,8);
    JvCB9VON.Text  := strSeperate(s,cSeperate,9);
    JvCB10VON.Text := strSeperate(s,cSeperate,10);
    end;

    s:=CaoRegistry.ReadStringU(PChar(cSuche), PChar('JvNoCBxVON'), PCHAR(''));
    if length(s) > 0 then
    begin
    JvNoCB1VON.Text  := strSeperate(s,cSeperate,1);
    JvNoCB2VON.Text  := strSeperate(s,cSeperate,2);
    JvNoCB3VON.Text  := strSeperate(s,cSeperate,3);
    JvNoCB4VON.Text  := strSeperate(s,cSeperate,4);
    JvNoCB5VON.Text  := strSeperate(s,cSeperate,5);
    end;

    s:=CaoRegistry.ReadStringU(PChar(cSuche), PChar('JvCBxBIS'), PCHAR(''));
    if length(s) > 0 then
    begin
    JvCB1BIS.Text  := strSeperate(s,cSeperate,1);
    JvCB2BIS.Text  := strSeperate(s,cSeperate,2);
    JvCB3BIS.Text  := strSeperate(s,cSeperate,3);
    JvCB4BIS.Text  := strSeperate(s,cSeperate,4);
    JvCB5BIS.Text  := strSeperate(s,cSeperate,5);
    JvCB6BIS.Text  := strSeperate(s,cSeperate,6);
    JvCB7BIS.Text  := strSeperate(s,cSeperate,7);
    JvCB8BIS.Text  := strSeperate(s,cSeperate,8);
    JvCB9BIS.Text  := strSeperate(s,cSeperate,9);
    JvCB10BIS.Text := strSeperate(s,cSeperate,10);
    end;

    s:=CaoRegistry.ReadStringU(PChar(cSuche), PChar('JvNoCBxBIS'), PCHAR(''));
    if length(s) > 0 then
    begin
    JvNoCB1BIS.Text  := strSeperate(s,cSeperate,1);
    JvNoCB2BIS.Text  := strSeperate(s,cSeperate,2);
    JvNoCB3BIS.Text  := strSeperate(s,cSeperate,3);
    JvNoCB4BIS.Text  := strSeperate(s,cSeperate,4);
    JvNoCB5BIS.Text  := strSeperate(s,cSeperate,5);
    end;

    // Datentypen den Comboboxen (JvCB1Typ..JvNoCB5Typ) zuordnen
    // Da Funktionalit�t bereits im OnChangeEvent (JvCB1..JvNoCB5)
    // enthalten ist wird diese verwendet
    JvCB1Change(Sender);
    JvCB2Change(Sender);
    JvCB3Change(Sender);
    JvCB4Change(Sender);
    JvCB5Change(Sender);
    JvCB6Change(Sender);
    JvCB7Change(Sender);
    JvCB8Change(Sender);
    JvCB9Change(Sender);
    JvCB10Change(Sender);
    JvNoCB1Change(Sender);
    JvNoCB2Change(Sender);
    JvNoCB3Change(Sender);
    JvNoCB4Change(Sender);
    JvNoCB5Change(Sender);

    // Wichtig: Den JvCBxTyp.ItemIndex immer erst nach JvCBxChange setzen
    s:=CaoRegistry.ReadStringU(PChar(cSuche), PChar('JvCBxTyp'), PCHAR(''));
    if length(s) > 0 then
    begin
    JvCB1Typ.ItemIndex:= strtoint(strSeperate(s,cSeperate,1));
    JvCB2Typ.ItemIndex:= strtoint(strSeperate(s,cSeperate,2));
    JvCB3Typ.ItemIndex:= strtoint(strSeperate(s,cSeperate,3));
    JvCB4Typ.ItemIndex:= strtoint(strSeperate(s,cSeperate,4));
    JvCB5Typ.ItemIndex:= strtoint(strSeperate(s,cSeperate,5));
    JvCB6Typ.ItemIndex:= strtoint(strSeperate(s,cSeperate,6));
    JvCB7Typ.ItemIndex:= strtoint(strSeperate(s,cSeperate,7));
    JvCB8Typ.ItemIndex:= strtoint(strSeperate(s,cSeperate,8));
    JvCB9Typ.ItemIndex:= strtoint(strSeperate(s,cSeperate,9));
    JvCB10Typ.ItemIndex:=strtoint(strSeperate(s,cSeperate,10));
    end;

    s:=CaoRegistry.ReadStringU(PChar(cSuche), PChar('JvNoCBxTyp'), PCHAR(''));
    if length(s) > 0 then
    begin
    JvNoCB1Typ.ItemIndex:= strtoint(strSeperate(s,cSeperate,1));
    JvNoCB2Typ.ItemIndex:= strtoint(strSeperate(s,cSeperate,2));
    JvNoCB3Typ.ItemIndex:= strtoint(strSeperate(s,cSeperate,3));
    JvNoCB4Typ.ItemIndex:= strtoint(strSeperate(s,cSeperate,4));
    JvNoCB5Typ.ItemIndex:= strtoint(strSeperate(s,cSeperate,5));
    end;

  finally;
  end;
end;

Function AddFieldtoSQL(von,bis: TEdit; dbFeld: string; Typ: char = 'A') : string;
var
   mystr,s1,s2 : string;
   f1,f2   : Real;
   l1,l2   : integer;
   rc1,rc2 : integer;
   dt1,dt2 : TDateTime;
begin
   mystr := '';
   s1 := strReplace(trim(Von.Text),'*','%');
   s2 := strReplace(trim(Bis.Text),'*','%');
   s1 := strReplace(s1,'?','_');
   s2 := strReplace(s2,'?','_');
   l1 := length(s1);
   l2 := length(s2);

   if (length(dbFeld)= 0) or (l1+l2=0) then
   begin
   result := '';
   exit;
   end;

   case Upcase(Typ) of
   'A': begin // Alphanumerisches Feld
           if pos(',',s1) > 0 then
              begin
              s1 := strReplace(s1,',',''',''');
              mystr:= '('+dbfeld+' IN (''' + trim(s1) + '''))'
              end
           else
              if l1 > 0 then
              begin
                if (l2 > 0) and (s2 >= s1) then
                   mystr:= '('+dbfeld+'>=''' + trim(s1) + ''' AND '+dbfeld+'<=''' + trim(s2) +''')'
                else
                   mystr:= '('+dbfeld+' LIKE''' + trim(s1) + ''')';
              end
          end;

   'D': begin // Datumsfeld
           if l1 > 0 then
           begin
             try
               s1 := formatdatetime('yyyy-mm-dd',StrtoDate(s1));
             except
               s1:='';
               showMessage('Von: Ung�ltiges Datum im Feld "'+dbfeld+'"');
             end;
           end;
           if l2 > 0 then
           begin
             try
               s2 := formatdatetime('yyyy-mm-dd',StrtoDate(s2));
             except
               s2:='';
               showMessage('Bis: Ung�ltiges Datum im Feld "'+dbfeld+'"');
             end;
           end;
           l1 := length(s1);
           l2 := length(s2);

           if l1 > 0 then
              if (l2 > 0) and (s2 >= s1) then
                 mystr:= '('+dbfeld+'>=''' + s1 + ''' AND '+dbfeld+'<=''' + s2 +''')'
              else
                 mystr:= '('+dbfeld+'=''' + s1 + ''')';
        end;

   'N': begin // Numerisches Feld
           val(s1,f1,rc1);
           val(s2,f2,rc2);
           if rc1 = 0 then
              begin
              if (l1 > 0) then
                   if ((rc2 = 0) and (l2 > 0) and (f2 >= f1)) then
                      mystr:= '('+dbfeld+' BETWEEN ' + trim(s1) + ' AND '+ trim(s2) +')'
                   else
                      mystr:= '('+dbfeld+'=' + trim(s1) + ')'
              end
           else
              if pos(',',s1) > 0 then
                 mystr:= '('+ dbfeld + ' IN (' + trim(s1) + '))'
        end;

   'T': begin // Textfeld
           if l1 > 0 then
              if (l2 > 0) then
                 ShowMessage('F�r Textfelder wie ('+dbfeld+') sind Bis-Angaben ung�ltig')
              else
                 mystr:= '('+dbfeld+' LIKE''' + trim(s1) + ''')';
        end;

   'J': begin // Y/N Feld
           s1 := Uppercase(s1);
           s1 := strReplace(s1,'J','Y');

           if pos(',',s1) > 0 then
              begin
              s1 := strReplace(s1,',',''',''');
              mystr:= '('+dbfeld+' IN (''' + trim(s1) + '''))'
              end
           else
              if l1 > 0 then
              begin
                if (l2 > 0) and (s2 >= s1) then
                   mystr:= '('+dbfeld+'>=''' + trim(s1) + ''' AND '+dbfeld+'<=''' + trim(s2) +''')'
                else
                   mystr:= '('+dbfeld+' LIKE''' + trim(s1) + ''')';
              end
          end
   else
       begin end;      // Nichts tun wenn falscher Typ
   end;
   result := trim(mystr);
end;


procedure TArtSuchForm.BitBtn1Click(Sender: TObject);
var s : String;
    p : Integer;
    f : Boolean;

begin
  s:='';
  case JvPageControl1.ActivePage.PageIndex of
    0: begin
       Memo1.Lines.Clear;
       f:= false;

       if (JvCB1.ItemIndex > 0) and (JvCB1Typ.ItemIndex >= 0)  then
       begin
         P := JvCB1.ItemIndex;
         s := AddFieldtoSQL(JvCB1Von,JvCB1Bis,cFelder[P].DBName,JvCB1Typ.Text[1]);
         if length(s) > 0 then begin Memo1.Lines.Append(s); f:=true; end;
       end;

       if (JvCB2.ItemIndex > 0) and (JvCB2Typ.ItemIndex >= 0) then
       begin
         P := JvCB2.ItemIndex;
         s := AddFieldtoSQL(JvCB2Von,JvCB2Bis,cFelder[P].DBName,JvCB2Typ.Text[1]);
         if length(s) > 0 then
         begin
           if f = true then Memo1.Lines.Append(' AND '+s)
                       else begin Memo1.Lines.Append(s); f:=true; end;
         end;
       end;

       if (JvCB3.ItemIndex > 0)  and (JvCB3Typ.ItemIndex >= 0) then
       begin
         P := JvCB3.ItemIndex;
         s := AddFieldtoSQL(JvCB3Von,JvCB3Bis,cFelder[P].DBName,JvCB3Typ.Text[1]);
         if length(s) > 0 then
         begin
           if f = true then Memo1.Lines.Append(' AND '+s)
                       else begin Memo1.Lines.Append(s); f:=true; end;
           end;
       end;

       if (JvCB4.ItemIndex > 0)  and (JvCB4Typ.ItemIndex >= 0) then
       begin
         P := JvCB4.ItemIndex;
         s := AddFieldtoSQL(JvCB4Von,JvCB4Bis,cFelder[P].DBName,JvCB4Typ.Text[1]);
         if length(s) > 0 then
         begin
           if f = true then Memo1.Lines.Append(' AND '+s)
                     else begin Memo1.Lines.Append(s); f:=true; end;
           end;
       end;

       if (JvCB5.ItemIndex > 0)  and (JvCB5Typ.ItemIndex >= 0) then
       begin
         P := JvCB5.ItemIndex;
         s := AddFieldtoSQL(JvCB5Von,JvCB5Bis,cFelder[P].DBName,JvCB5Typ.Text[1]);
         if length(s) > 0 then
         begin
           if f = true then Memo1.Lines.Append(' AND '+s)
                       else begin Memo1.Lines.Append(s); f:=true; end;
         end;
       end;

       if (JvCB6.ItemIndex > 0)  and (JvCB6Typ.ItemIndex >= 0) then
       begin
         P := JvCB6.ItemIndex;
         s := AddFieldtoSQL(JvCB6Von,JvCB6Bis,cFelder[P].DBName,JvCB6Typ.Text[1]);
         if length(s) > 0 then
         begin
           if f = true then Memo1.Lines.Append(' AND '+s)
                       else begin Memo1.Lines.Append(s); f:=true; end;
         end;
       end;

       if (JvCB7.ItemIndex > 0)  and (JvCB7Typ.ItemIndex >= 0) then
       begin
         P := JvCB7.ItemIndex;
         s := AddFieldtoSQL(JvCB7Von,JvCB7Bis,cFelder[P].DBName,JvCB7Typ.Text[1]);
         if length(s) > 0 then
         begin
           if f = true then Memo1.Lines.Append(' AND '+s)
                       else begin Memo1.Lines.Append(s); f:=true; end;
         end;
       end;

       if (JvCB8.ItemIndex > 0)  and (JvCB8Typ.ItemIndex >= 0) then
       begin
         P := JvCB8.ItemIndex;
         s := AddFieldtoSQL(JvCB8Von,JvCB8Bis,cFelder[P].DBName,JvCB8Typ.Text[1]);
         if length(s) > 0 then
         begin
           if f = true then Memo1.Lines.Append(' AND '+s)
                       else begin Memo1.Lines.Append(s); f:=true; end;
         end;
       end;

       if (JvCB9.ItemIndex > 0)  and (JvCB9Typ.ItemIndex >= 0) then
       begin
         P := JvCB9.ItemIndex;
         s := AddFieldtoSQL(JvCB9Von,JvCB9Bis,cFelder[P].DBName,JvCB9Typ.Text[1]);
         if length(s) > 0 then
         begin
           if f = true then Memo1.Lines.Append(' AND '+s)
                       else begin Memo1.Lines.Append(s); f:=true; end;
         end;
       end;

       if (JvCB10.ItemIndex > 0)  and (JvCB10Typ.ItemIndex >= 0) then
       begin
         P := JvCB10.ItemIndex;
         s := AddFieldtoSQL(JvCB10Von,JvCB10Bis,cFelder[P].DBName,JvCB10Typ.Text[1]);
         if length(s) > 0 then
         begin
           if f = true then Memo1.Lines.Append(' AND '+s)
                       else begin Memo1.Lines.Append(s); f:=true; end;
         end;
       end;

       //
       // Und nun die Excludes
       //

       if (JvNoCB1.ItemIndex > 0)  and (JvNoCB1Typ.ItemIndex >= 0) then
       begin
         P := JvNoCB1.ItemIndex;
         s := AddFieldtoSQL(JvNoCB1Von,JvNoCB1Bis,cFelder[P].DBName, JvNoCB1Typ.Text[1]);
         if length(s) > 0 then
         begin
           if f = true then Memo1.Lines.Append(' AND NOT '+s)
                       else begin Memo1.Lines.Append(' NOT '+s); f:=true; end;
         end;
       end;

       if (JvNoCB2.ItemIndex > 0) and (JvNoCB2Typ.ItemIndex >= 0)  then
       begin
         P := JvNoCB2.ItemIndex;
         s := AddFieldtoSQL(JvNoCB2Von,JvNoCB2Bis,cFelder[P].DBName,JvNoCB2Typ.Text[1]);
         if length(s) > 0 then
         begin
           if f = true then Memo1.Lines.Append(' AND NOT '+s)
                       else begin Memo1.Lines.Append(' NOT '+s); f:=true; end;
         end;
       end;

       if (JvNoCB3.ItemIndex > 0)  and (JvNoCB3Typ.ItemIndex >= 0) then
       begin
         P := JvNoCB3.ItemIndex;
         s := AddFieldtoSQL(JvNoCB3Von,JvNoCB3Bis,cFelder[P].DBName,JvNoCB3Typ.Text[1]);
         if length(s) > 0 then
         begin
           if f = true then Memo1.Lines.Append(' AND NOT '+s)
                        else begin Memo1.Lines.Append(' NOT '+s); f:=true; end;
         end;
       end;

       if (JvNoCB4.ItemIndex > 0)  and (JvNoCB4Typ.ItemIndex >= 0) then
       begin
         P := JvNoCB4.ItemIndex;
         s := AddFieldtoSQL(JvNoCB4Von,JvNoCB4Bis,cFelder[P].DBName, JvNoCB4Typ.Text[1]);
         if length(s) > 0 then
         begin
           if f = true then Memo1.Lines.Append(' AND NOT '+s)
                       else begin Memo1.Lines.Append(' NOT '+s); f:=true; end;
         end;
       end;

       if (JvNoCB5.ItemIndex > 0)  and (JvNoCB5Typ.ItemIndex >= 0)  then
       begin
         P := JvNoCB5.ItemIndex;
         s := AddFieldtoSQL(JvNoCB5Von,JvNoCB5Bis,cFelder[P].DBName, JvNoCB5Typ.Text[1]);
         if length(s) > 0 then
         begin
           if f = true then Memo1.Lines.Append(' AND NOT '+s)
                       else begin Memo1.Lines.Append(' NOT '+s); f:=true; end;
         end;
       end;

       if (cbSQLAnzeige.State = cbChecked) and (length(memo1.Text)>0) then
           ShowMessage(Memo1.Text);
       end;

    1: begin
       // nichts mehr erforderlich, da Selection bereits im Memofeld
       // Memo1.Text := Memo1.Text;
       end;
   end;
end;


procedure TArtSuchForm.FormClose(Sender: TObject; var Action: TCloseAction);
Var s:String;
begin
  try
    CaoRegistry.WriteBooleanU(PCHar(cSuche),PChar('SQLAnzeige'), cbSQLAnzeige.Checked);

    s:= Trim(inttostr(JvCB1.ItemIndex))+cSeperate +
        Trim(inttostr(JvCB2.ItemIndex))+cSeperate +
        Trim(inttostr(JvCB3.ItemIndex))+cSeperate +
        Trim(inttostr(JvCB4.ItemIndex))+cSeperate +
        Trim(inttostr(JvCB5.ItemIndex))+cSeperate +
        Trim(inttostr(JvCB6.ItemIndex))+cSeperate +
        Trim(inttostr(JvCB7.ItemIndex))+cSeperate +
        Trim(inttostr(JvCB8.ItemIndex))+cSeperate +
        Trim(inttostr(JvCB9.ItemIndex))+cSeperate +
        Trim(inttostr(JvCB10.ItemIndex));
    CaoRegistry.WriteStringU (PChar(cSuche),PChar('JvCBx'), PChar(s));

    s:= Trim(inttostr(JvNoCB1.ItemIndex))+cSeperate +
        Trim(inttostr(JvNoCB2.ItemIndex))+cSeperate +
        Trim(inttostr(JvNoCB3.ItemIndex))+cSeperate +
        Trim(inttostr(JvNoCB4.ItemIndex))+cSeperate +
        Trim(inttostr(JvNoCB5.ItemIndex));
    CaoRegistry.WriteStringU (PChar(cSuche),PChar('JvNoCBx'), PChar(s));

    s:= Trim(inttostr(JvCB1Typ.ItemIndex))+cSeperate +
        Trim(inttostr(JvCB2Typ.ItemIndex))+cSeperate +
        Trim(inttostr(JvCB3Typ.ItemIndex))+cSeperate +
        Trim(inttostr(JvCB4Typ.ItemIndex))+cSeperate +
        Trim(inttostr(JvCB5Typ.ItemIndex))+cSeperate +
        Trim(inttostr(JvCB6Typ.ItemIndex))+cSeperate +
        Trim(inttostr(JvCB7Typ.ItemIndex))+cSeperate +
        Trim(inttostr(JvCB8Typ.ItemIndex))+cSeperate +
        Trim(inttostr(JvCB9Typ.ItemIndex))+cSeperate +
        Trim(inttostr(JvCB10Typ.ItemIndex));
    CaoRegistry.WriteStringU (PChar(cSuche),PChar('JvCBxTyp'), PChar(s));

    s:= Trim(inttostr(JvNoCB1Typ.ItemIndex))+cSeperate +
        Trim(inttostr(JvNoCB2Typ.ItemIndex))+cSeperate +
        Trim(inttostr(JvNoCB3Typ.ItemIndex))+cSeperate +
        Trim(inttostr(JvNoCB4Typ.ItemIndex))+cSeperate +
        Trim(inttostr(JvNoCB5Typ.ItemIndex));
    CaoRegistry.WriteStringU (PChar(cSuche),PChar('JvNoCBxTyp'), PChar(s));

    s:= Trim(JvCB1Von.Text)+cSeperate +
        Trim(JvCB2Von.Text)+cSeperate +
        Trim(JvCB3Von.Text)+cSeperate +
        Trim(JvCB4Von.Text)+cSeperate +
        Trim(JvCB5Von.Text)+cSeperate +
        Trim(JvCB6Von.Text)+cSeperate +
        Trim(JvCB7Von.Text)+cSeperate +
        Trim(JvCB8Von.Text)+cSeperate +
        Trim(JvCB9Von.Text)+cSeperate +
        Trim(JvCB10Von.Text);
    CaoRegistry.WriteStringU (PChar(cSuche),PChar('JvCBxVON'), PChar(s));

    s:= Trim(JvNoCB1Von.Text)+cSeperate +
        Trim(JvNoCB2Von.Text)+cSeperate +
        Trim(JvNoCB3Von.Text)+cSeperate +
        Trim(JvNoCB4Von.Text)+cSeperate +
        Trim(JvNoCB5Von.Text);
    CaoRegistry.WriteStringU (PChar(cSuche),PChar('JvNoCBxVON'), PChar(s));

    s:= Trim(JvCB1Bis.Text)+cSeperate +
        Trim(JvCB2Bis.Text)+cSeperate +
        Trim(JvCB3Bis.Text)+cSeperate +
        Trim(JvCB4Bis.Text)+cSeperate +
        Trim(JvCB5Bis.Text)+cSeperate +
        Trim(JvCB6Bis.Text)+cSeperate +
        Trim(JvCB7Bis.Text)+cSeperate +
        Trim(JvCB8Bis.Text)+cSeperate +
        Trim(JvCB9Bis.Text)+cSeperate +
        Trim(JvCB10Bis.Text);
    CaoRegistry.WriteStringU (PChar(cSuche),PChar('JvCBxBIS'), PChar(s));

    s:= Trim(JvNoCB1Bis.Text)+cSeperate +
        Trim(JvNoCB2Bis.Text)+cSeperate +
        Trim(JvNoCB3Bis.Text)+cSeperate +
        Trim(JvNoCB4Bis.Text)+cSeperate +
        Trim(JvNoCB5Bis.Text);
    CaoRegistry.WriteStringU (PChar(cSuche),PChar('JvNoCBxBIS'), PChar(s));

    finally;
    end;
end;


procedure TArtSuchForm.btnClearClick(Sender: TObject);
begin
    // L�sche nur den Text in der Combobox indem der ItemIndex auf Null gesetzt
   // wird. Die Combobox Inhalte muessen f�r weitere Auswahl bestehen bleiben
   JvCB1.ItemIndex:=0;
   JvCB2.ItemIndex:=0;
   JvCB3.ItemIndex:=0;
   JvCB4.ItemIndex:=0;
   JvCB5.ItemIndex:=0;
   JvCB6.ItemIndex:=0;
   JvCB7.ItemIndex:=0;
   JvCB8.ItemIndex:=0;
   JvCB9.ItemIndex:=0;
   JvCB10.ItemIndex:=0;
   JvNoCB1.ItemIndex := 0;
   JvNoCB2.ItemIndex := 0;
   JvNoCB3.ItemIndex := 0;
   JvNoCB4.ItemIndex := 0;
   JvNoCB5.ItemIndex := 0;

   // L�sche die Inhalte der Comboboxen f�r die Datenfeldtypen
   // Kann mit Clear erfolgen, da Typ mit Feldnamensauswahl neu gesetzt wird
   JvCB1Typ.Clear;
   JvCB2Typ.Clear;
   JvCB3Typ.Clear;
   JvCB4Typ.Clear;
   JvCB5Typ.Clear;
   JvCB6Typ.Clear;
   JvCB7Typ.Clear;
   JvCB8Typ.Clear;
   JvCB9Typ.Clear;
   JvCB10Typ.Clear;
   JvNoCB1Typ.Clear;
   JvNoCB2Typ.Clear;
   JvNoCB3Typ.Clear;
   JvNoCB4Typ.Clear;
   JvNoCB5Typ.Clear;

   // L�sche die VON/BIS Felder
   JvCB1VON.Text:='';   JvCB1BIS.Text:='';
   JvCB2VON.Text:='';   JvCB2BIS.Text:='';
   JvCB3VON.Text:='';   JvCB3BIS.Text:='';
   JvCB4VON.Text:='';   JvCB4BIS.Text:='';
   JvCB5VON.Text:='';   JvCB5BIS.Text:='';
   JvCB6VON.Text:='';   JvCB6BIS.Text:='';
   JvCB7VON.Text:='';   JvCB7BIS.Text:='';
   JvCB8VON.Text:='';   JvCB8BIS.Text:='';
   JvCB9VON.Text:='';   JvCB9BIS.Text:='';
   JvCB10VON.Text:='';  JvCB10BIS.Text:='';
   JvNoCB1VON.Text:=''; JvNoCB1BIS.Text:='';
   JvNoCB2VON.Text:=''; JvNoCB2BIS.Text:='';
   JvNoCB3VON.Text:=''; JvNoCB3BIS.Text:='';
   JvNoCB4VON.Text:=''; JvNoCB4BIS.Text:='';
   JvNoCB5VON.Text:=''; JvNoCB5BIS.Text:='';

   Memo1.Lines.Clear;
end;


procedure TArtSuchForm.JvCB1Change(Sender: TObject);
begin
  JvCB1Typ.Clear;
  If JvCB1.ItemIndex > 0 then
     begin
     JvCB1Typ.Items.Append(cFelder[JvCB1.ItemIndex].Typ);
     if cFelder[JvCB1.ItemIndex].Typ = 'A' then
        JvCB1Typ.Items.Append('N');
     if JvCB1Typ.ItemIndex = -1 then
        JvCB1Typ.ItemIndex := 0;
     end;
end;

procedure TArtSuchForm.JvCB2Change(Sender: TObject);
begin
  JvCB2Typ.Clear;
  If JvCB2.ItemIndex > 0 then
     begin
     JvCB2Typ.Items.Append(cFelder[JvCB2.ItemIndex].Typ);
      if cFelder[JvCB2.ItemIndex].Typ = 'A' then
         JvCB2Typ.Items.Append('N');
      if JvCB2Typ.ItemIndex = -1 then
         JvCB2Typ.ItemIndex := 0;
     end;
end;

procedure TArtSuchForm.JvCB3Change(Sender: TObject);
begin
  JvCB3Typ.Clear;
  If JvCB3.ItemIndex > 0 then
     begin
     JvCB3Typ.Items.Append(cFelder[JvCB3.ItemIndex].Typ);
      if cFelder[JvCB3.ItemIndex].Typ = 'A' then
         JvCB3Typ.Items.Append('N');
      if JvCB3Typ.ItemIndex = -1 then
         JvCB3Typ.ItemIndex := 0;
     end;
end;

procedure TArtSuchForm.JvCB4Change(Sender: TObject);
begin
  JvCB4Typ.Clear;
  If JvCB4.ItemIndex > 0 then
     begin
     JvCB4Typ.Items.Append(cFelder[JvCB4.ItemIndex].Typ);
      if cFelder[JvCB4.ItemIndex].Typ = 'A' then
         JvCB4Typ.Items.Append('N');
      if JvCB4Typ.ItemIndex = -1 then
         JvCB4Typ.ItemIndex := 0;
     end;
end;

procedure TArtSuchForm.JvCB5Change(Sender: TObject);
begin
  JvCB5Typ.Clear;
  If JvCB5.ItemIndex > 0 then
     begin
     JvCB5Typ.Items.Append(cFelder[JvCB5.ItemIndex].Typ);
      if cFelder[JvCB5.ItemIndex].Typ = 'A' then
         JvCB5Typ.Items.Append('N');
      if JvCB5Typ.ItemIndex = -1 then
         JvCB5Typ.ItemIndex := 0;
     end;
end;

procedure TArtSuchForm.JvCB6Change(Sender: TObject);
begin
  JvCB6Typ.Clear;
  If JvCB6.ItemIndex > 0 then
     begin
     JvCB6Typ.Items.Append(cFelder[JvCB6.ItemIndex].Typ);
     if cFelder[JvCB6.ItemIndex].Typ = 'A' then
        JvCB6Typ.Items.Append('N');
     if JvCB6Typ.ItemIndex = -1 then
        JvCB6Typ.ItemIndex := 0;
     end;
end;

procedure TArtSuchForm.JvCB7Change(Sender: TObject);
begin
  JvCB7Typ.Clear;
  If JvCB7.ItemIndex > 0 then
     begin
     JvCB7Typ.Items.Append(cFelder[JvCB7.ItemIndex].Typ);
      if cFelder[JvCB7.ItemIndex].Typ = 'A' then
         JvCB7Typ.Items.Append('N');
      if JvCB7Typ.ItemIndex = -1 then
         JvCB7Typ.ItemIndex := 0;
     end;
end;

procedure TArtSuchForm.JvCB8Change(Sender: TObject);
begin
  JvCB8Typ.Clear;
  If JvCB8.ItemIndex > 0 then
     begin
     JvCB8Typ.Items.Append(cFelder[JvCB8.ItemIndex].Typ);
      if cFelder[JvCB8.ItemIndex].Typ = 'A' then
         JvCB8Typ.Items.Append('N');
      if JvCB8Typ.ItemIndex = -1 then
         JvCB8Typ.ItemIndex := 0;
     end;
end;

procedure TArtSuchForm.JvCB9Change(Sender: TObject);
begin
  JvCB9Typ.Clear;
  If JvCB9.ItemIndex > 0 then
     begin
     JvCB9Typ.Items.Append(cFelder[JvCB9.ItemIndex].Typ);
      if cFelder[JvCB9.ItemIndex].Typ = 'A' then
         JvCB9Typ.Items.Append('N');
      if JvCB9Typ.ItemIndex = -1 then
         JvCB9Typ.ItemIndex := 0;
     end;
end;

procedure TArtSuchForm.JvCB10Change(Sender: TObject);
begin
  JvCB10Typ.Clear;
  If JvCB10.ItemIndex > 0 then
     begin
     JvCB10Typ.Items.Append(cFelder[JvCB10.ItemIndex].Typ);
      if cFelder[JvCB10.ItemIndex].Typ = 'A' then
         JvCB10Typ.Items.Append('N');
      if JvCB10Typ.ItemIndex = -1 then
         JvCB10Typ.ItemIndex := 0;
     end;
end;

procedure TArtSuchForm.JvNoCB1Change(Sender: TObject);
begin
  JvNoCB1Typ.Clear;
  If JvNoCB1.ItemIndex > 0 then
     begin
     JvNoCB1Typ.Items.Append(cFelder[JvNoCB1.ItemIndex].Typ);
      if cFelder[JvNoCB1.ItemIndex].Typ = 'A' then
         JvNoCB1Typ.Items.Append('N');
      if JvNoCB1Typ.ItemIndex = -1 then
         JvNoCB1Typ.ItemIndex := 0;
     end;
end;

procedure TArtSuchForm.JvNoCB2Change(Sender: TObject);
begin
  JvNoCB2Typ.Clear;
  If JvNoCB2.ItemIndex > 0 then
     begin
     JvNoCB2Typ.Items.Append(cFelder[JvNoCB2.ItemIndex].Typ);
      if cFelder[JvNoCB2.ItemIndex].Typ = 'A' then
         JvNoCB2Typ.Items.Append('N');
      if JvNoCB2Typ.ItemIndex = -1 then
         JvNoCB2Typ.ItemIndex := 0;
     end;

end;

procedure TArtSuchForm.JvNoCB3Change(Sender: TObject);
begin
  JvNoCB3Typ.Clear;
  If JvNoCB3.ItemIndex > 0 then
     begin
     JvNoCB3Typ.Items.Append(cFelder[JvNoCB3.ItemIndex].Typ);
      if cFelder[JvNoCB3.ItemIndex].Typ = 'A' then
         JvNoCB3Typ.Items.Append('N');
      if JvNoCB3Typ.ItemIndex = -1 then
         JvNoCB3Typ.ItemIndex := 0;
     end;

end;

procedure TArtSuchForm.JvNoCB4Change(Sender: TObject);
begin
  JvNoCB4Typ.Clear;
  If JvNoCB4.ItemIndex > 0 then
     begin
     JvNoCB4Typ.Items.Append(cFelder[JvNoCB4.ItemIndex].Typ);
      if cFelder[JvNoCB4.ItemIndex].Typ = 'A' then
         JvNoCB4Typ.Items.Append('N');
      if JvNoCB4Typ.ItemIndex = -1 then
         JvNoCB4Typ.ItemIndex := 0;
     end;

end;

procedure TArtSuchForm.JvNoCB5Change(Sender: TObject);
begin
  JvNoCB5Typ.Clear;
  If JvNoCB5.ItemIndex > 0 then
     begin
     JvNoCB5Typ.Items.Append(cFelder[JvNoCB5.ItemIndex].Typ);
      if cFelder[JvNoCB5.ItemIndex].Typ = 'A' then
         JvNoCB5Typ.Items.Append('N');
      if JvNoCB5Typ.ItemIndex = -1 then
         JvNoCB5Typ.ItemIndex := 0;
     end;

end;

end.




