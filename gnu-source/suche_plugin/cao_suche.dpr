library cao_suche;

{ Wichtiger Hinweis zur DLL-Speicherverwaltung: ShareMem mu� sich in der
  ersten Unit der unit-Klausel der Bibliothek und des Projekts befinden (Projekt-
  Quelltext anzeigen), falls die DLL Prozeduren oder Funktionen exportiert, die
  Strings als Parameter oder Funktionsergebnisse weitergibt. Dies trifft auf alle
  Strings zu, die von oder zur DLL weitergegeben werden -- auch diejenigen, die
  sich in Records oder Klassen befinden. ShareMem ist die Schnittstellen-Unit
  zu BORLNDMM.DLL, der gemeinsamen Speicherverwaltung, die zusammen mit
  der DLL weitergegeben werden mu�. Um die Verwendung von BORLNDMM.DLL
  zu vermeiden, sollten String-Informationen als PChar oder ShortString �bergeben werden. }

//------------------------------------------------------------------------------

uses
  SysUtils,
  Classes,
  Forms,
  Controls,
  Dialogs,
  Windows,
  cao_plugin_def,
  {$IFDEF VER150}
  D7ComboBoxStringsGetPatch in 'D7ComboBoxStringsGetPatch.pas',
  {$ENDIF}
  cao_such_var in 'cao_such_var.pas',
  cao_art_such_form1 in 'cao_art_such_form1.pas' {ArtSuchForm},
  cao_adr_such_form1 in 'cao_adr_such_form1.pas' {AdrSuchForm};



{$R *.RES}
//------------------------------------------------------------------------------
var AktMandant  : PChar;
    ResStr      : String; // Wichtig, mu� global sein, da in der Fkt.
                          // CaoPluginExecCmd ein Pointer(PChar) auf diesen
                          // String zur�ckgegeben wird
{//------------------------------------------------------------------------------

const IStr : String = 'erweiterte Suche'+#0;

const MyPluginFunktionInfo : Array [0..1] of TPlunginFunktionInfo =
         [ARTIKEL_PLUGIN, (ARTIKEL_SUCHE_CMD), (@IStr[1]);}

//------------------------------------------------------------------------------
function CaoPluginInfo (var Info : Array of TPlunginFunktionInfo) : TPluginfo; StdCall;

var MyPluginFunktionInfo : TPlunginFunktionInfo;

begin
     // Hiermit wird CAO ein offenes Array zur�ckgegeben,
     // in dem die DLL CAO "sagt" welche Funktionen sie unterst�tzt

     // DLL kann nur 1, die Artikelsuche
     Info[0] :=MyPluginFunktionInfo;

     // Info zur�ckgeben
     // Info :=PluginFunktionInfo;

     // allg. zur�ckgelieferte Informationen zum Plugin
     Result.APP   :='CAO';
     Result.Autor :='NLH/JP';
     Result.Name  :='Suche-Plugin';
     Result.ID    :=1000;
     Result.About :='***';
end;
//------------------------------------------------------------------------------
procedure CaoPluginInit (Mandant : PChar;                // Name des Mandanten
                         NewCaoRegistry : tCaoRegistry);
                         // Record mit Pointern auf die CAO-Registry-Funktionen
                         stdcall;
var S : String;
begin
     CaoRegistry.ReadBoolean       :=NewCaoRegistry.ReadBoolean;
     CaoRegistry.ReadBooleanU      :=NewCaoRegistry.ReadBooleanU;
     CaoRegistry.ReadDouble        :=NewCaoRegistry.ReadDouble;
     CaoRegistry.ReadDoubleU       :=NewCaoRegistry.ReadDoubleU;
     CaoRegistry.ReadInteger       :=NewCaoRegistry.ReadInteger;
     CaoRegistry.ReadIntegerU      :=NewCaoRegistry.ReadIntegerU;
     CaoRegistry.ReadLongString    :=NewCaoRegistry.ReadLongString;
     CaoRegistry.ReadLongStringU   :=NewCaoRegistry.ReadLongStringU;
     CaoRegistry.ReadString        :=NewCaoRegistry.ReadString;
     CaoRegistry.ReadStringU       :=NewCaoRegistry.ReadStringU;

     CaoRegistry.WriteBoolean      :=NewCaoRegistry.WriteBoolean;
     CaoRegistry.WriteBooleanU     :=NewCaoRegistry.WriteBooleanU;
     CaoRegistry.WriteDouble       :=NewCaoRegistry.WriteDouble;
     CaoRegistry.WriteDoubleU      :=NewCaoRegistry.WriteDoubleU;
     CaoRegistry.WriteInteger      :=NewCaoRegistry.WriteInteger;
     CaoRegistry.WriteIntegerU     :=NewCaoRegistry.WriteIntegerU;
     CaoRegistry.WriteLongString   :=NewCaoRegistry.WriteLongString;
     CaoRegistry.WriteLongStringU  :=NewCaoRegistry.WriteLongStringU;
     CaoRegistry.WriteString       :=NewCaoRegistry.WriteString;
     CaoRegistry.WriteStringU      :=NewCaoRegistry.WriteStringU;

     { Test um einen String aus dem Userbereich der SQL-Reg. zu lesen
     S :=CaoRegistry.ReadStringU ('','DLL_TEST','AAA');
     MessageDlg ('DLL-Init :'+#13#10#13#10+
                 'String aus der SQL-Registery von CAO:'+#13#10+
                 S,mtinformation,[mbok],0);
     }
end;
//------------------------------------------------------------------------------
function CaoPluginExecCmd (Typ, Cmd, Rechte : Integer;
                           var SQL : PChar) : Boolean; stdcall;
// Mu� True liefern wenn der Dialog mit OK geschlossen wurde,
// wenn diese Function False liefert wird das �bergebene SQL nicht ausgef�hrt

var  ArtSuchForm: TArtSuchForm;
     AdrSuchForm: TAdrSuchForm;
     retcode : Boolean;
begin
     retcode := false;

     if (Typ=ADRESS_PLUGIN)and(Cmd=ADRESS_SUCHE_CMD) then
     begin
       AdrSuchForm :=TAdrSuchForm.Create(Application);
       try
          retcode :=(AdrSuchForm.ShowModal = mrOK);
          if retcode then
          begin
            ResStr :=AdrSuchForm.Memo1.Text;
            SQL    :=PChar(ResStr);
          end;
       finally
         AdrSuchForm.Free;
       end;
       Result := retcode;
     end;

     if (Typ=ARTIKEL_PLUGIN)and(Cmd=ARTIKEL_SUCHE_CMD) then
     begin
       ArtSuchForm :=TArtSuchForm.Create(Application);
       try
          retcode :=(ArtSuchForm.ShowModal = mrOK);
          if retcode then
          begin
            ResStr :=ArtSuchForm.Memo1.Text;
            SQL    :=PChar(ResStr);
          end;
       finally
         ArtSuchForm.Free;
       end;
       Result := retcode;
     end;

     { Test um einen String in den USerbereich der SQL-Reg. zu schreiben
     CaoRegistry.WriteStringU (PCHar(''),
                               PChar('DLL_TEST'),
                               PChar('Hallo, dieser Text wurde von '+
                                     'der DLL geschrieben ! 1234'));
     }
end;
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
exports
    CaoPluginInit,
    CaoPluginInfo,
    CaoPluginExecCmd;
//------------------------------------------------------------------------------
// DLL-Init
begin
   CaoRegistry.ReadBoolean       :=nil;
   CaoRegistry.ReadBooleanU      :=nil;
   CaoRegistry.ReadDouble        :=nil;
   CaoRegistry.ReadDoubleU       :=nil;
   CaoRegistry.ReadInteger       :=nil;
   CaoRegistry.ReadIntegerU      :=nil;
   CaoRegistry.ReadLongString    :=nil;
   CaoRegistry.ReadLongStringU   :=nil;
   CaoRegistry.ReadString        :=nil;
   CaoRegistry.ReadStringU       :=nil;

   CaoRegistry.WriteBoolean      :=nil;
   CaoRegistry.WriteBooleanU     :=nil;
   CaoRegistry.WriteDouble       :=nil;
   CaoRegistry.WriteDoubleU      :=nil;
   CaoRegistry.WriteInteger      :=nil;
   CaoRegistry.WriteIntegerU     :=nil;
   CaoRegistry.WriteLongString   :=nil;
   CaoRegistry.WriteLongStringU  :=nil;
   CaoRegistry.WriteString       :=nil;
   CaoRegistry.WriteStringU      :=nil;
end.
