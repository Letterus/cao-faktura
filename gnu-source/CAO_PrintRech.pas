{
CAO-Faktura f�r Windows Version 1.0
Copyright (C) 2003 Jan Pokrandt / Jan@JP-SOFT.de

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.


******* CAO-Faktura comes with ABSOLUTELY NO WARRANTY ***************


Programm     : CAO-Faktura
Modul        : CAO_PRINTRECH
Stand        : 07.01.2004
Version      : 1.2 RC3
Beschreibung : Druckausgabe Belege + Journale

History :

06.05.2003 - Version 1.0.0.54 released Jan Pokrandt
           - in den Rechnungspositionen neue berechnete Felder f�r
             Brutto-Preise hinzugef�ht (Einzelprei-Brutto, EP-Rabatt-Brutto
             Gesamtpreis-Brutto, Mwst-Betrag)
10.05.2003 - neue Tabelle f�r allg. Firmendaten hinzugef�gt
11.05.2003 - neue (virtuelle) Tabelle f�r Seriennummern hinzugef�gt, diese
             liefert immer genau einen Datensatz mit der Liste der Seriennummern
             der aktuellen Artikelposition in der Rechnung, wenn diese
             Seriennummern enth�lt, sonst wird eine leere Datenmenge geliefert
03.07.2003 - EMAIL-Subjekt wird jetzt aus der Registery gelesen
             im Subjekt kann jetzt auch %FELDNAME% aus der Tabelle Journal
             verwendet werden
             Text f�r Zahlungsziel "Sofort" wird jetzt aus der Registery gelesen
16.09.2003 - neues berechnetes Feld "Umsatzsteuer-ID" hinzugef�gt
24.09.2003 - Bug#44
07.01.2004 - Artikel und Adressen (Zusatztabellen) wurden beim Lieferschein
             nicht mit Parametern best�ckt, das geht jetzt
15.01.2003 - EMAIL TEXT + BETREFF f�r Mahnung hinzugef�gt
23.01.2004 - neue Felder Skonto-Betrag und Rechnungssumme abzgl. Skonto
             zum Rechnungskopf hinzugef�gt
10.03.2004 - Unit f�r Stapeldruck vorbereitet

Todo :
 Aufruf der Druckfunktion auf JournalID umstellen

$Id: CAO_PrintRech.pas,v 1.34 2004/05/23 14:28:18 jan Exp $

}

unit CAO_PrintRech;

{$I CAO32.INC}

{$IFNDEF REPORTBUILDER}
DIESE U_N_I_T D�RFTE OHNE DIESES DEFINE NICHT AUFGERUFEN WERDEN,
BITTE ENTFERNEN SIE DIE U_N_I_T AUS ALLEN U_S_E_S KLAUSELN DES PROJEKTES
{$ENDIF}

interface

uses
  Db, PsRBExport_Main, ppDBBDE, ppDBJIT, ppCache, ppClass,
  ppBands, ppProd, ppReport, ppEndUsr, ppDB, ppComm, ppRelatv, ppDBPipe,
  ZQuery, ZMySqlQuery, StdCtrls, JvSpin, Buttons, Controls, JvLookup,
  Classes, Windows, Messages, SysUtils, Graphics, Forms,  Dialogs, ppPrnabl,
  ppStrtch, ppSubRpt, ppViewr,  ppDevice, ppPrnDev,  ppCtrls,  JvComponent,
  {$IFDEF PRO} PsRBExport_MasterControl,raIDE,ppRTTI,raFunc,PsRBExport_PDF,
  Email, SMapi, Mask, JvMaskEdit,Registry,ComObj,{$ENDIF} JvEdit;

type
  TPrintRechForm = class(TForm)
    Label1: TLabel;
    Label2: TLabel;
    FormularCB: TJvDBLookupCombo;
    ZielCB: TComboBox;
    LayoutBtn: TBitBtn;
    BinNamCB: TComboBox;
    AnzCopy: TJvSpinEdit;
    VorschauBtn: TBitBtn;
    PrintBtn: TBitBtn;
    DruEinrBtn: TBitBtn;                                              
    CloseBtn: TBitBtn;
    ReKopfTab: TZMySqlQuery;
    ReKopfTabLIEFART_KURZ: TStringField;
    ReKopfTabZAHLART_KURZ: TStringField;
    ReKopfTabZAHLART_LANG: TStringField;
    ReKopfTabSOLL_ZAHLUNGSZIEL: TStringField;
    ReKopfTabSOLL_STAGE: TIntegerField;
    ReKopfTabSOLL_SKONTO: TFloatField;
    ReKopfTabSOLL_NTAGE: TIntegerField;
    ReKopfTabLIEFART: TIntegerField;
    ReKopfTabZAHLART: TIntegerField;
    ReKopfTabVRENUM: TIntegerField;
    ReKopfTabQUELLE: TIntegerField;
    ReKopfTabREC_ID: TIntegerField;
    ReKopfTabQUELLE_SUB: TIntegerField;
    ReKopfTabADDR_ID: TIntegerField;
    ReKopfTabRDATUM: TDateField;
    ReKopfTabKUN_NUM: TStringField;
    ReKopfTabKUN_ANREDE: TStringField;
    ReKopfTabKUN_NAME1: TStringField;
    ReKopfTabKUN_NAME2: TStringField;
    ReKopfTabKUN_NAME3: TStringField;
    ReKopfTabKUN_ABTEILUNG: TStringField;
    ReKopfTabKUN_STRASSE: TStringField;
    ReKopfTabKUN_LAND: TStringField;
    ReKopfTabKUN_PLZ: TStringField;
    ReKopfTabKUN_ORT: TStringField;
    ReKopfTabVLSNUM: TIntegerField;
    ReKopfTabKM_STAND: TIntegerField;
    ReKopfTabLDATUM: TDateField;
    ReKopfTabLOHN: TFloatField;
    ReKopfTabWARE: TFloatField;
    ReKopfTabTKOST: TFloatField;
    ReKopfTabMWST_0: TFloatField;
    ReKopfTabMWST_1: TFloatField;
    ReKopfTabMWST_2: TFloatField;
    ReKopfTabMWST_3: TFloatField;
    ReKopfTabNSUMME: TFloatField;
    ReKopfTabMSUMME_0: TFloatField;
    ReKopfTabMSUMME_1: TFloatField;
    ReKopfTabMSUMME_2: TFloatField;
    ReKopfTabMSUMME_3: TFloatField;
    ReKopfTabMSUMME: TFloatField;
    ReKopfTabBSUMME: TFloatField;
    ReKopfTabWAEHRUNG: TStringField;
    ReKopfTabSOLL_RATEN: TIntegerField;
    ReKopfTabSOLL_RATBETR: TFloatField;
    ReKopfTabSOLL_RATINTERVALL: TIntegerField;
    ReKopfTabERSTELLT: TDateField;
    ReKopfTabERST_NAME: TStringField;
    ReKopfTabUSR1: TStringField;
    ReKopfTabUSR2: TStringField;
    ReKopfTabPROJEKT: TStringField;
    ReKopfTabORGNUM: TStringField;
    ReKopfTabBEST_NAME: TStringField;
    ReKopfTabBEST_CODE: TIntegerField;
    ReKopfTabBEST_DATUM: TDateField;
    ReKopfTabDS: TDataSource;
    ReKopfPipeline: TppDBPipeline;
    RePosPipeline: TppDBPipeline;
    RePosTabDS: TDataSource;
    RePosTab: TZMySqlQuery;
    ReportTab: TZMySqlQuery;
    ReportTabMAINKEY: TStringField;
    ReportTabNAME: TStringField;
    ReportTabVAL_BLOB: TMemoField;
    ReportTabVAL_CHAR: TStringField;
    ReportTabVAL_BIN: TBlobField;
    ReportDS: TDataSource;
    RepPipeline: TppDBPipeline;
    ppDesigner1: TppDesigner;
    RechReport: TppReport;
    ppDetailBand1: TppDetailBand;
    Label7: TLabel;
    Label8: TLabel;
    RePosTabMATCHCODE: TStringField;
    RePosTabARTNUM: TStringField;
    RePosTabBARCODE: TStringField;
    RePosTabMENGE: TFloatField;
    RePosTabLAENGE: TStringField;
    RePosTabGROESSE: TStringField;
    RePosTabDIMENSION: TStringField;
    RePosTabGEWICHT: TFloatField;
    RePosTabME_EINHEIT: TStringField;
    RePosTabPR_EINHEIT: TFloatField;
    RePosTabEPREIS: TFloatField;
    RePosTabRABATT: TFloatField;
    RePosTabSTEUER_CODE: TIntegerField;
    RePosTabALTTEIL_PROZ: TFloatField;
    RePosTabALTTEIL_STCODE: TIntegerField;
    RePosTabBEZEICHNUNG: TMemoField;
    RePosTabVIEW_POS: TStringField;
    RePosTabGPREIS: TFloatField;
    RePosTabMwstProz: TFloatField;
    KopfPipeline: TppJITPipeline;
    ppJITPipeline1ppField1: TppField;
    ppJITPipeline1ppField2: TppField;
    ppJITPipeline1ppField3: TppField;
    JournalName: TppField;
    KopfPipelineppField1: TppField;
    KopfPipelineppField2: TppField;
    KopfPipelineppField3: TppField;
    KopfPipelineppField4: TppField;
    KopfPipelineppField5: TppField;
    KopfPipelineppField6: TppField;
    SumQuery: TZMySqlQuery;
    SumQuerynetto: TFloatField;
    SumQuerymwst: TFloatField;
    SumQuerybrutto: TFloatField;
    SumQuerybezahlt: TFloatField;
    SumQuerywaehrung: TStringField;
    JournalTab: TZMySqlQuery;
    JournalTabATRNUM: TIntegerField;
    JournalTabVRENUM: TIntegerField;
    JournalTabVLSNUM: TIntegerField;
    JournalTabADATUM: TDateField;
    JournalTabRDATUM: TDateField;
    JournalTabLDATUM: TDateField;
    JournalTabLIEFART: TIntegerField;
    JournalTabZAHLART: TIntegerField;
    JournalTabLOHN: TFloatField;
    JournalTabWARE: TFloatField;
    JournalTabTKOST: TFloatField;
    JournalTabMWST_1: TFloatField;
    JournalTabMWST_2: TFloatField;
    JournalTabMWST_3: TFloatField;
    JournalTabNSUMME: TFloatField;
    JournalTabMSUMME_1: TFloatField;
    JournalTabMSUMME_2: TFloatField;
    JournalTabMSUMME_3: TFloatField;
    JournalTabMSUMME: TFloatField;
    JournalTabBSUMME: TFloatField;
    JournalTabATSUMME: TFloatField;
    JournalTabATMSUMME: TFloatField;
    JournalTabWAEHRUNG: TStringField;
    JournalTabGEGENKONTO: TIntegerField;
    JournalTabSOLL_STAGE: TIntegerField;
    JournalTabSOLL_SKONTO: TFloatField;
    JournalTabSOLL_NTAGE: TIntegerField;
    JournalTabSOLL_RATEN: TIntegerField;
    JournalTabSOLL_RATBETR: TFloatField;
    JournalTabSOLL_RATINTERVALL: TIntegerField;
    JournalTabIST_ANZAHLUNG: TFloatField;
    JournalTabIST_SKONTO: TFloatField;
    JournalTabIST_ZAHLDAT: TDateField;
    JournalTabIST_BETRAG: TFloatField;
    JournalTabMAHNKOSTEN: TFloatField;
    JournalTabKUN_NUM: TStringField;
    JournalTabKUN_ANREDE: TStringField;
    JournalTabKUN_NAME1: TStringField;
    JournalTabKUN_NAME2: TStringField;
    JournalTabKUN_NAME3: TStringField;
    JournalTabKUN_ABTEILUNG: TStringField;
    JournalTabKUN_STRASSE: TStringField;
    JournalTabKUN_LAND: TStringField;
    JournalTabKUN_PLZ: TStringField;
    JournalTabKUN_ORT: TStringField;
    JournalTabUSR1: TStringField;
    JournalTabUSR2: TStringField;
    JournalTabPROJEKT: TStringField;
    JournalTabORGNUM: TStringField;
    JournalTabINFO: TBlobField;
    JournalTabStatus: TStringField;
    JournalTabKonditionen: TStringField;
    JournalTabSTADIUM: TIntegerField;
    JournalTabKOST_NETTO: TFloatField;
    JournalTabWERT_NETTO: TFloatField;
    JournalDS: TDataSource;
    JournalPipeline: TppBDEPipeline;

    OposPipeline: TppDBPipeline;
    OposPipelineppField1: TppField;
    OposPipelineppField2: TppField;
    OposPipelineppField3: TppField;
    OposPipelineppField4: TppField;
    OposPipelineppField5: TppField;
    OposPipelineppField6: TppField;
    OposPipelineppField7: TppField;
    OposPipelineppField8: TppField;
    OposPipelineppField9: TppField;
    OposPipelineppField10: TppField;
    OposPipelineppField11: TppField;
    OposPipelineppField12: TppField;
    OposPipelineppField13: TppField;
    OposPipelineppField14: TppField;
    OposPipelineppField15: TppField;
    OposPipelineppField16: TppField;
    OposPipelineppField17: TppField;
    OposPipelineppField18: TppField;
    OposPipelineppField19: TppField;
    OposPipelineppField20: TppField;
    OposPipelineppField21: TppField;
    OposPipelineppField22: TppField;
    OposPipelineppField23: TppField;
    OposPipelineppField24: TppField;
    OposPipelineppField25: TppField;
    OposPipelineppField26: TppField;
    OposPipelineppField27: TppField;
    OposPipelineppField28: TppField;
    OposPipelineppField29: TppField;
    OposPipelineppField30: TppField;
    OposPipelineppField31: TppField;
    OposPipelineppField32: TppField;
    OposPipelineppField33: TppField;
    OposPipelineppField34: TppField;
    OposPipelineppField35: TppField;
    OposPipelineppField36: TppField;
    OposDS: TDataSource;
    OposTab: TZMySqlQuery;
    LsKopfTab: TZMySqlQuery;
    LsKopfTabQUELLE: TIntegerField;
    LsKopfTabREC_ID: TIntegerField;
    LsKopfTabVLSNUM: TIntegerField;
    LsKopfTabKM_STAND: TIntegerField;
    LsKopfTabLDATUM: TDateField;
    LsKopfTabLIEFART: TIntegerField;
    LsKopfTabKOST_NETTO: TFloatField;
    LsKopfTabWERT_NETTO: TFloatField;
    LsKopfTabWAEHRUNG: TStringField;
    LsKopfTabERSTELLT: TDateField;
    LsKopfTabERST_NAME: TStringField;
    LsKopfTabKUN_NUM: TStringField;
    LsKopfTabKUN_ANREDE: TStringField;
    LsKopfTabKUN_NAME1: TStringField;
    LsKopfTabKUN_NAME2: TStringField;
    LsKopfTabKUN_NAME3: TStringField;
    LsKopfTabKUN_ABTEILUNG: TStringField;
    LsKopfTabKUN_STRASSE: TStringField;
    LsKopfTabKUN_LAND: TStringField;
    LsKopfTabKUN_PLZ: TStringField;
    LsKopfTabKUN_ORT: TStringField;
    LsKopfTabUSR1: TStringField;
    LsKopfTabUSR2: TStringField;
    LsKopfTabPROJEKT: TStringField;
    LsKopfTabORGNUM: TStringField;
    LsKopfTabBEST_NAME: TStringField;
    LsKopfTabBEST_CODE: TIntegerField;
    LsKopfTabBEST_DATUM: TDateField;
    LsKopfTabLIEFART_KURZ: TStringField;
    LsKopfTabLIEFART_LANG: TStringField;
    LsPosTab: TZMySqlQuery;
    LsPosTabJOURNAL_ID: TIntegerField;
    LsPosTabARTIKELTYP: TStringField;
    LsPosTabVIEW_POS: TStringField;
    LsPosTabMATCHCODE: TStringField;
    LsPosTabARTNUM: TStringField;
    LsPosTabBARCODE: TStringField;
    LsPosTabMENGE: TFloatField;
    LsPosTabLAENGE: TStringField;
    LsPosTabGROESSE: TStringField;
    LsPosTabDIMENSION: TStringField;
    LsPosTabGEWICHT: TFloatField;
    LsPosTabME_EINHEIT: TStringField;
    LsPosTabBEZEICHNUNG: TMemoField;
    LsKopfTabDS: TDataSource;
    LsPosTabDS: TDataSource;
    LsKopfPipeline: TppBDEPipeline;
    ppField3: TppField;
    ppField5: TppField;
    ppField6: TppField;
    ppField7: TppField;
    ReKopfPipelineppField8: TppField;
    ReKopfPipelineppField9: TppField;
    ppField8: TppField;
    ReKopfPipelineppField11: TppField;
    ReKopfPipelineppField12: TppField;
    ReKopfPipelineppField13: TppField;
    ReKopfPipelineppField14: TppField;
    ReKopfPipelineppField15: TppField;
    ppField9: TppField;
    ppField10: TppField;
    ppField11: TppField;
    ppField12: TppField;
    ppField13: TppField;
    ppField14: TppField;
    ppField15: TppField;
    ppField16: TppField;
    ppField17: TppField;
    ppField18: TppField;
    ppField19: TppField;
    ppField20: TppField;
    ppField21: TppField;
    ppField22: TppField;
    ppField23: TppField;
    LsPosPipeline: TppBDEPipeline;
    KasBuch: TZMySqlQuery;
    KasBuchREC_ID: TIntegerField;
    KasBuchJAHR: TIntegerField;
    KasBuchBDATUM: TDateField;
    KasBuchQUELLE: TIntegerField;
    KasBuchJOURNAL_ID: TIntegerField;
    KasBuchZU_ABGANG: TFloatField;
    KasBuchBTXT: TMemoField;
    KasBuchBELEGNUM: TStringField;
    KasBuchGKONTO: TIntegerField;
    KasBuchSKONTO: TFloatField;
    KasBuchQuelleStr: TStringField;
    KasBuchDS: TDataSource;
    KasBuchPipeline: TppDBPipeline;
    KasKopfPipeline: TppJITPipeline;
    KasKopfPipelineppField1: TppField;
    KasKopfPipelineppField2: TppField;
    KasKopfPipelineppField3: TppField;
    KasKopfPipelineppField4: TppField;
    KasKopfPipelineppField5: TppField;
    KasKopfPipelineppField6: TppField;
    ReKopfTabLIEFART_LANG: TStringField;
    ReKopfPipelineppField37: TppField;
    RePosPipelineppField21: TppField;
    RePosTabERPREIS: TFloatField;
    KasTagSumPipeline: TppDBPipeline;
    KasTagSumDS: TDataSource;
    KasTagSumTab: TZMySqlQuery;
    KasTagDetailPipeline: TppDBPipeline;
    KasTagDetailDS: TDataSource;
    KasTagDetailTab: TZMySqlQuery;
    ReKopfPipelineppField38: TppField;
    ReKopfTabCALC_LIEF_ANREDE: TStringField;
    ReKopfTabCALC_LIEF_NAME1: TStringField;
    ReKopfTabCALC_LIEF_NAME2: TStringField;
    ReKopfTabCALC_LIEF_NAME3: TStringField;
    ReKopfTabCALC_LIEF_STRASSE: TStringField;
    ReKopfTabCALC_LIEF_PLZ: TStringField;
    ReKopfTabCALC_LIEF_ORT: TStringField;
    ReKopfTabCALC_LIEF_LAND: TStringField;
    ReKopfTabLIEF_ADDR_ID: TIntegerField;
    ReKopfPipelineppField39: TppField;
    ReKopfPipelineppField40: TppField;
    ReKopfPipelineppField47: TppField;
    ReKopfPipelineppField48: TppField;
    ReKopfPipelineppField49: TppField;
    ReKopfPipelineppField50: TppField;
    ReKopfPipelineppField65: TppField;
    ReKopfPipelineppField66: TppField;
    PDFBtn: TBitBtn;
    SendEMailBtn: TBitBtn;
    ReKopfTabKFZ_ID: TIntegerField;
    ReKopfTabCALC_POLKZ: TStringField;
    ReKopfTabCALC_FGSTNR: TStringField;
    ReKopfPipelineppField67: TppField;
    ReKopfPipelineppField68: TppField;
    MahnPrintTab: TZMySqlQuery;
    MahnPrintTabquelle: TIntegerField;
    MahnPrintTabvrenum: TIntegerField;
    MahnPrintTabrdatum: TDateField;
    MahnPrintTabaddr_id: TIntegerField;
    MahnPrintTabbsumme: TFloatField;
    MahnPrintTabwaehrung: TStringField;
    MahnPrintTabsoll_ntage: TIntegerField;
    MahnPrintTabmahnkosten: TFloatField;
    MahnPrintTabgegenkonto: TIntegerField;
    MahnPrintTabkun_num: TStringField;
    MahnPrintTabkun_name1: TStringField;
    MahnPrintTabsaldo: TFloatField;
    MahnPrintTabmahnstufe: TIntegerField;
    MahnPrintTabtage_offen: TLargeintField;
    MahnPrintTabrec_id: TIntegerField;
    MahnPrintTabmahnprint: TIntegerField;
    MahnPrintDS: TDataSource;
    MahnPrintPipeline: TppDBPipeline;
    ppField2: TppField;
    ppField4: TppField;
    ppField25: TppField;
    ppField30: TppField;
    ppField31: TppField;
    ppField34: TppField;
    ppField37: TppField;
    ppField38: TppField;
    ppField39: TppField;
    ppField40: TppField;
    ppField41: TppField;
    ppField42: TppField;
    ppField43: TppField;
    ppField44: TppField;
    ppField45: TppField;
    ppField46: TppField;
    ppField47: TppField;
    ppField48: TppField;
    ppField49: TppField;
    ppField50: TppField;
    ppField51: TppField;
    ppField52: TppField;
    ppField53: TppField;
    ppField54: TppField;
    ppField55: TppField;
    ppField56: TppField;
    MahnPrintTabnsumme: TFloatField;
    MahnPrintTabmsumme_0: TFloatField;
    MahnPrintTabmsumme_1: TFloatField;
    MahnPrintTabmsumme_2: TFloatField;
    MahnPrintTabmsumme_3: TFloatField;
    MahnPrintTabmsumme: TFloatField;
    MahnPrintTabzahlart: TIntegerField;
    MahnPrintTabstadium: TIntegerField;
    MahnPrintTabsoll_stage: TIntegerField;
    MahnPrintTabsoll_skonto: TFloatField;
    MahnPrintTabist_anzahlung: TFloatField;
    MahnPrintTabist_skonto: TFloatField;
    MahnPrintTabist_zahldat: TDateField;
    MahnPrintTabist_betrag: TFloatField;
    MahnPrintTabkun_anrede: TStringField;
    MahnPrintTabkun_name2: TStringField;
    MahnPrintTabkun_name3: TStringField;
    MahnPrintTabkun_abteilung: TStringField;
    MahnPrintTabkun_strasse: TStringField;
    MahnPrintTabkun_plz: TStringField;
    MahnPrintTabkun_land: TStringField;
    MahnPrintTabkun_ort: TStringField;
    ReportTabVAL_TYP: TIntegerField;
    OposPipelineppField37: TppField;
    ReKopfTabCALC_EMAIL: TStringField;
    LsKopfTabCALC_EMAIL: TStringField;
    LsKopfTabADDR_ID: TIntegerField;
    RePosTabMWST_BETRAG: TFloatField;
    RePosPipelineppField25: TppField;
    FirmaTabPipeline: TppDBPipeline;
    ReSNTab: TZMySqlQuery;
    ReSNTabARTIKEL_ID: TIntegerField;
    ReSNTabSERNUMMER: TStringField;
    ReSNTabVERK_NUM: TIntegerField;
    ReSNTabSNUM_ID: TIntegerField;
    ReSNTabVK_JOURNAL_ID: TIntegerField;
    ReSNTabVK_JOURNALPOS_ID: TIntegerField;
    ReSDNS: TDataSource;
    RePosTabREC_ID: TIntegerField;
    ReSNPipeline: TppJITPipeline;
    ReSNPipelineSN: TppField;
    RePosTabRABATT2: TFloatField;
    RePosTabRABATT3: TFloatField;
    RePosPipelineppField26: TppField;
    RePosPipelineppField27: TppField;
    RePosTabALTTEIL_FLAG: TBooleanField;
    BonTab: TZMySqlQuery;
    BonDS: TDataSource;
    BonPipeline: TppDBPipeline;
    BonPosPipeline: TppDBPipeline;
    BonPosDS: TDataSource;
    BonPosTab: TZMySqlQuery;
    BonTabBELEGNUMMER: TIntegerField;
    BonTabBELEGDATUM: TDateField;
    BonTabZAHLART: TIntegerField;
    BonTabMWST_0: TFloatField;
    BonTabMWST_1: TFloatField;
    BonTabMWST_2: TFloatField;
    BonTabMWST_3: TFloatField;
    BonTabNSUMME: TFloatField;
    BonTabMSUMME_0: TFloatField;
    BonTabMSUMME_1: TFloatField;
    BonTabMSUMME_2: TFloatField;
    BonTabMSUMME_3: TFloatField;
    BonTabBSUMME: TFloatField;
    BonTabWAEHRUNG: TStringField;
    BonTabBEDIENER: TStringField;
    BonTabZAHLART_KURZ: TStringField;
    BonPosTabMATCHCODE: TStringField;
    BonPosTabARTNUM: TStringField;
    BonPosTabBARCODE: TStringField;
    BonPosTabMENGE: TFloatField;
    BonPosTabLAENGE: TStringField;
    BonPosTabGROESSE: TStringField;
    BonPosTabDIMENSION: TStringField;
    BonPosTabGEWICHT: TFloatField;
    BonPosTabME_EINHEIT: TStringField;
    BonPosTabPR_EINHEIT: TFloatField;
    BonPosTabEPREIS: TFloatField;
    BonPosTabRABATT: TFloatField;
    BonPosTabRABATT2: TFloatField;
    BonPosTabRABATT3: TFloatField;
    BonPosTabSTEUER_CODE: TIntegerField;
    BonPosTabBEZEICHNUNG: TMemoField;
    BonPosTabGPREIS: TFloatField;
    BonPosTabERPREIS: TFloatField;
    BonPosTabMWST_BETRAG: TFloatField;
    BonPosTabMWSTPROZ: TFloatField;
    BonTabREC_ID: TIntegerField;
    ReKopfTabCALC_UST_ID: TStringField;
    ReKopfPipelineppField69: TppField;
    ReKopfTabCALC_BRIEFANREDE: TStringField;
    ReKopfPipelineppBRIEFANREDE: TppField;
    ReKopfTabCALC_LANDLANG: TStringField;
    ReKopfPipelineppLandLang: TppField;
    LsKopfTabCALC_LANDLANG: TStringField;
    LsKopfPipelineCALC_LANDLANG: TppField;
    MahnPrintTabfrist: TDateField;
    MahnPrintFrist: TppField;
    MahnPrintTabMAHNDATUM: TDateField;
    MahnPrintPipelineMAHNDATUM: TppField;
    SumQuerySTADIUM: TIntegerField;
    ReKopfPipelineppField70: TppField;
    JournalTabCALC_USTID: TStringField;
    JournalTabADDR_ID: TIntegerField;
    JournalPipelineCALC_USTID: TppField;
    TextTab: TZMySqlQuery;
    TextTabMATCHCODE: TStringField;
    TextTabKUNDENGRUPPE: TIntegerField;
    TextTabKUNNUM1: TStringField;
    TextTabKUNNUM2: TStringField;
    TextTabANREDE: TStringField;
    TextTabNAME1: TStringField;
    TextTabNAME2: TStringField;
    TextTabNAME3: TStringField;
    TextTabABTEILUNG: TStringField;
    TextTabSTRASSE: TStringField;
    TextTabLAND: TStringField;
    TextTabPLZ: TStringField;
    TextTabORT: TStringField;
    TextTabGRUPPE: TStringField;
    TextTabTELE1: TStringField;
    TextTabTELE2: TStringField;
    TextTabFAX: TStringField;
    TextTabFUNK: TStringField;
    TextTabEMAIL: TStringField;
    TextTabEMAIL2: TStringField;
    TextTabINTERNET: TStringField;
    TextTabDIVERSES: TStringField;
    TextTabBRIEFANREDE: TStringField;
    TextTabDEB_NUM: TIntegerField;
    TextTabKRD_NUM: TIntegerField;
    TextTabUST_NUM: TStringField;
    TextTabBRIEFTEXT: TMemoField;
    TextTabDATUM: TDateTimeField;
    TextTabERSTELLER: TStringField;
    TextTabBETREFF: TStringField;
    TextDS: TDataSource;
    TextPipeline: TppDBPipeline;
    TextPipelineppField1: TppField;
    TextPipelineppField2: TppField;
    TextPipelineppField3: TppField;
    TextPipelineppField4: TppField;
    TextPipelineppField5: TppField;
    TextPipelineppField6: TppField;
    TextPipelineppField7: TppField;
    TextPipelineppField8: TppField;
    TextPipelineppField9: TppField;
    TextPipelineppField10: TppField;
    TextPipelineppField11: TppField;
    TextPipelineppField12: TppField;
    TextPipelineppField13: TppField;
    TextPipelineppField14: TppField;
    TextPipelineppField16: TppField;
    TextPipelineppField17: TppField;
    TextPipelineppField18: TppField;
    TextPipelineppField19: TppField;
    TextPipelineppField20: TppField;
    TextPipelineppField21: TppField;
    TextPipelineppField22: TppField;
    TextPipelineppField23: TppField;
    TextPipelineppField24: TppField;
    TextPipelineppField25: TppField;
    TextPipelineppField26: TppField;
    TextPipelineppField27: TppField;
    TextPipelineppField28: TppField;
    TextPipelineppField29: TppField;
    TextPipelineppField30: TppField;
    TextPipelineppField31: TppField;
    TextTabID: TIntegerField;
    ReKopfTabTERMIN: TDateField;
    ReKopfTabGEWICHT: TFloatField;
    ReKopfPipelineppField71: TppField;
    LsKopfTabGEWICHT: TFloatField;
    LsPosTabREC_ID: TIntegerField;
    LsKopfPipelineppField1: TppField;
    ReKopfPipelineppField72: TppField;
    ReKopfPipelineppField73: TppField;
    ArtTab: TZMySqlQuery;
    ArtDS: TDataSource;
    ArtPipeline: TppDBPipeline;
    AddrTab: TZMySqlQuery;
    AddrDS: TDataSource;
    AddrPipeline: TppDBPipeline;
    RePosTabARTIKEL_ID: TIntegerField;
    ReKopfTabSOLL_NDATUM: TDateTimeField;
    ReKopfTabSOLL_SDATUM: TDateTimeField;
    ReKopfPipelineppField74: TppField;
    ReKopfPipelineppField75: TppField;
    LsPosTabARTIKEL_ID: TIntegerField;
    LsPosTabRABATT: TFloatField;
    LsPosPipelineppField1: TppField;
    KasKopfPipelineppField7: TppField;
    ReKopfTabCALC_SKONTOBETRAG: TFloatField;
    ReKopfTabCALC_BSUM_SKONTO: TFloatField;
    ReKopfPipelineppField76: TppField;
    ReKopfPipelineppField77: TppField;
    KopfPipelineppField7: TppField;
    KopfPipelineppField8: TppField;
    KopfPipelineppField9: TppField;
    SumQueryMWST_1: TFloatField;
    SumQueryMWST_2: TFloatField;
    SumQueryMWST_3: TFloatField;
    MahnPrintTabEMAIL: TStringField;
    MahnPrintTabEMAIL2: TStringField;
    LsPosPipelineppField14: TppField;
    ReKopfTabFREIGABE1_FLAG: TBooleanField;
    ReKopfTabPRINT_FLAG: TBooleanField;
    ReKopfTabBRUTTO_FLAG: TBooleanField;
    LsKopfTabPRINT_FLAG: TBooleanField;
    LsKopfTabBRUTTO_FLAG: TBooleanField;
    LsPosTabVPE: TIntegerField;
    LsPosTabEPREIS: TFloatField;
    RePosTabSN_FLAG: TBooleanField;
    LsPosTabSN_FLAG: TBooleanField;
    MahnPrintTabTELE1: TStringField;
    MahnPrintTabFAX: TStringField;
    TextTabADDR_ID: TIntegerField;


    procedure ReportTabBeforePost(DataSet: TDataSet);
    procedure ReportTabBeforeOpen(DataSet: TDataSet);
    procedure CloseBtnClick(Sender: TObject);
    procedure FormularCBChange(Sender: TObject);
    procedure ZielCBChange(Sender: TObject);
    procedure PrintBtnClick(Sender: TObject);
    procedure LayoutBtnClick(Sender: TObject);
    procedure RechReportPreviewFormCreate(Sender: TObject);
    procedure ReKopfTabCalcFields(DataSet: TDataSet);
    procedure VorschauBtnClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure AnzCopyChange(Sender: TObject);
    procedure ppDesigner1CloseQuery(Sender: TObject;
      var CanClose: Boolean);
    procedure DruEinrBtnClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure SumQueryAfterOpen(DataSet: TDataSet);
    function KopfPipelineGetFieldValue(aFieldName: String): Variant;
    procedure JournalTabCalcFields(DataSet: TDataSet);
    procedure RePosTabCalcFields(DataSet: TDataSet);
    function KasKopfPipelineGetFieldValue(aFieldName: String): Variant;
    procedure KasBuchCalcFields(DataSet: TDataSet);
    procedure PDFBtnClick(Sender: TObject);
    procedure SendEMailBtnClick(Sender: TObject);
    procedure LsKopfTabCalcFields(DataSet: TDataSet);
    procedure RePosTabAfterScroll(DataSet: TDataSet);
    function ReSNPipelineGetFieldValue(aFieldName: String): Variant;
    function ReSNPipelineCheckBOF: Boolean;
    function ReSNPipelineCheckEOF: Boolean;
    function ReSNPipelineGetActive: Boolean;
    procedure ReSNPipelineGotoFirstRecord(Sender: TObject);
    procedure ReSNPipelineGotoLastRecord(Sender: TObject);
    procedure ReSNPipelineTraverseBy(aIncrement: Integer);
    procedure BonTabCalcFields(DataSet: TDataSet);
    procedure BonPosTabCalcFields(DataSet: TDataSet);
    procedure MahnPrintTabCalcFields(DataSet: TDataSet);
    procedure LsPosTabAfterScroll(DataSet: TDataSet);
    procedure ReKopfTabAfterScroll(DataSet: TDataSet);
    procedure LsKopfTabAfterScroll(DataSet: TDataSet);
    procedure RechReportCancel(Sender: TObject);
  private
    { Private-Deklarationen }
    Quelle       : Integer;
    CurrBelegID  : Integer;
    //CurrAddrID   : Integer;
    MainKey      : String;

    jour_netto   : Double;
    jour_brutto  : Double;
    jour_mwst    : Double;
    jour_mwst1   : Double;
    jour_mwst2   : Double;
    jour_mwst3   : Double;
    jour_bezahlt : Double;
    jour_offen   : Double;

    JName        : String;
    VDat, BDat   : tDateTime;
    ZeitraumStr  : String;

    KasBuchStartDatum     : tDateTime;
    KasBuchLeBuchungDatum : tDateTime;
    KasBuchEndDatum       : tDateTime;
    KasBuchStartSaldo     : Double;
    KasBuchEndSaldo       : Double;
    KasBuchZeitraum       : String;

    TmpSN : String; // Liste der Seriennummern des akt. Artikels
    TmpSNPos : Integer;

    PrintAbort : Boolean;

    EMail, Fax : String;

    {$IFDEF PRO}PsRBExportMasterControl1 : tPsRBExportMasterControl;{$ENDIF}

    procedure MapiError(Sender: TObject; ErrorCode: Integer);

  public
    { Public-Deklarationen }
    procedure ShowBelegDlg (NewQuelle, BelegID : Integer; Preview : Boolean);
    procedure ShowJournalDlg (NewQuelle : Integer; Von, Bis : tDateTime; Z : String);
    procedure ShowOposDlg (NewQuelle : Integer; Von, Bis : tDateTime; Z :String);
    procedure ShowKasBuchDlg (Von, Bis, LeBuchung : tDateTime; StartSaldo, EndSaldo : Double; Z :String);
    procedure ShowMahnungDlg (Addr_ID : Integer);
    procedure ShowBriefDlg (TextID:Integer; Vorschau : Boolean);
    {$IFDEF PRO}procedure MakePDF (SendEmail:Boolean);{$ENDIF}
  end;

var
  PrintRechForm: TPrintRechForm;

implementation

{$R *.DFM}

uses cao_dm, cao_var_const, CAO_Link, cao_tool1,
     ppTypes, ppPrintr, filectrl, clipbrd, cao_printpreview;
//------------------------------------------------------------------------------
procedure TPrintRechForm.FormCreate(Sender: TObject);
begin
     Quelle :=-1;
     EMail  :='';
     Fax    :='';

     clientheight :={round(}155;
     
     {$IFDEF PRO}
     PPDesigner1.RAPInterface :=[riDialog,riNotebookTab];
     PsRBExportMasterControl1 :=TPsRBExportMasterControl.Create (Self);
     PsRBExportMasterControl1.PDF.CompressionMethod :=cmMaxCompress;
     PsRBExportMasterControl1.PDF.Creator :='CAO-Faktura';
     PsRBExportMasterControl1.PDF.EMailAfterGenerate :=False;
     PsRBExportMasterControl1.PDF.Encoding :=feWinAnsiEncoding;
     PsRBExportMasterControl1.PDF.ExportImageFormat :=ifBmp;
     PsRBExportMasterControl1.PDF.ImageDPI :=300;
     PsRBExportMasterControl1.PDF.ImagePixelFormat := pf15bit;
     PsRBExportMasterControl1.PDF.IncludeImages :=True;
     PsRBExportMasterControl1.PDF.IncludeLines :=True;
     PsRBExportMasterControl1.PDF.IncludeRichText :=True;
     PsRBExportMasterControl1.PDF.IncludeShapes :=True;
     PsRBExportMasterControl1.PDF.OpenAfterGenerate :=False;
     PsRBExportMasterControl1.PDF.JPEGQuality := 100;
     PsRBExportMasterControl1.PDF.RichTextEncodingType := rtImage;
     PsRBExportMasterControl1.PDF.UseCompression := True;
     PsRBExportMasterControl1.PDF.ShowSetupDialog :=False;
     PsRBExportMasterControl1.PDF.ShowProgress :=True;
     PsRBExportMasterControl1.PDF.UseCompression :=True;
     PsRBExportMasterControl1.PDF.Active :=True;

     SendEMailBtn.Visible     :=True;
     PDFBtn.Visible           :=True;
     {$ELSE}
     PPDesigner1.RAPInterface :=[];
     SendEMailBtn.Visible     :=False;
     PDFBtn.Visible           :=False;
     {$ENDIF}
end;
//------------------------------------------------------------------------------
procedure TPrintRechForm.ShowBelegDlg (NewQuelle, BelegID : Integer; Preview : Boolean);
var Res : Boolean; i:integer;
    PrintPreviewForm: TPrintPreviewForm;
begin
     CurrBelegID :=BelegID;

     if Preview then
     begin
       PrintPreviewForm :=TPrintPreviewForm.Create (Self);
     end;
     try
       case NewQuelle of
            VK_RECH,VK_RECH_EDI : begin
                        MainKey :='MAIN\REPORT\RECHNUNG';
                        Caption :='Rechnung drucken';
                        ppDesigner1.Caption :='Rechnungslayout bearbeiten';
                        RechReport.DataPipeline :=ReKopfPipeline;
                        if not DM1.ZahlartTab.Active then DM1.ZahlartTab.Open
                                                     else DM1.ZahlartTab.Refresh;
                        if not DM1.LiefartTab.Active then DM1.LiefartTab.Open
                                                     else DM1.LiefartTab.Refresh;

                        AddrTab.Close;
                        AddrTab.Sql.Text :=
                          'select JOURNAL.REC_ID as J_REC_ID, '+
                          'JOURNAL.ADDR_ID, ADRESSEN.* '+
                          'from JOURNAL '+
                          'left outer JOIN ADRESSEN on '+
                          'ADRESSEN.REC_ID=JOURNAL.ADDR_ID '+
                          'where JOURNAL.REC_ID='+IntToStr(BelegID);
                        AddrTab.Open;

                        ReKopfTab.Close;
                        ReKopfTab.ParamByName ('ID').Value :=BelegID;
                        ReKopfTab.ParamByName ('QUELLE').Value :=NewQuelle;
                        ReKopfTab.Open;


                      end;
            VK_AGB, VK_AGB_EDI  : begin
                        MainKey :='MAIN\REPORT\ANGEBOT';
                        Caption :='Angebot drucken';
                        ppDesigner1.Caption :='Angebotslayout bearbeiten';
                        RechReport.DataPipeline :=ReKopfPipeline;
                        if not DM1.ZahlartTab.Active then DM1.ZahlartTab.Open
                                                     else DM1.ZahlartTab.Refresh;
                        if not DM1.LiefartTab.Active then DM1.LiefartTab.Open
                                                     else DM1.LiefartTab.Refresh;

                        AddrTab.Close;
                        AddrTab.Sql.Text :=
                          'select JOURNAL.REC_ID as J_REC_ID, '+
                          'JOURNAL.ADDR_ID, ADRESSEN.* '+
                          'from JOURNAL '+
                          'left outer JOIN ADRESSEN on '+
                          'ADRESSEN.REC_ID=JOURNAL.ADDR_ID '+
                          'where JOURNAL.REC_ID='+IntToStr(BelegID);
                        AddrTab.Open;

                        ReKopfTab.Close;
                        ReKopfTab.ParamByName ('ID').Value :=BelegID;
                        ReKopfTab.ParamByName ('QUELLE').Value :=NewQuelle;
                        ReKopfTab.Open;

                      end;
            VK_LIEF : begin
                        MainKey :='MAIN\REPORT\LIEFERSCHEIN';
                        Caption :='Lieferschein drucken';
                        ppDesigner1.Caption :='Lieferscheinlayout bearbeiten';
                        RechReport.DataPipeline :=LsKopfPipeline;
                        if not DM1.ZahlartTab.Active then DM1.ZahlartTab.Open
                                                     else DM1.ZahlartTab.Refresh;
                        if not DM1.LiefartTab.Active then DM1.LiefartTab.Open
                                                     else DM1.LiefartTab.Refresh;

                        AddrTab.Close;
                        AddrTab.Sql.Text :=
                          'select JOURNAL.REC_ID as J_REC_ID, '+
                          'JOURNAL.ADDR_ID, ADRESSEN.* '+
                          'from JOURNAL '+
                          'left outer JOIN ADRESSEN on '+
                          'ADRESSEN.REC_ID=JOURNAL.ADDR_ID '+
                          'where JOURNAL.REC_ID='+IntToStr(BelegID);
                        AddrTab.Open;

                        LsKopfTab.Close;
                        LsKopfTab.ParamByName ('ID').Value :=BelegID;
                        LsKopfTab.ParamByName ('QUELLE').Value :=NewQuelle;
                        LsKopfTab.Open;

                      end;
            EK_BEST : begin
                        MainKey :='MAIN\REPORT\EK-BESTELLUNG';
                        Caption :='EK-Bestellung drucken';
                        ppDesigner1.Caption :='EK-Bestellung-Layout bearbeiten';
                        RechReport.DataPipeline :=ReKopfPipeline;
                        if not DM1.ZahlartTab.Active then DM1.ZahlartTab.Open
                                                     else DM1.ZahlartTab.Refresh;
                        if not DM1.LiefartTab.Active then DM1.LiefartTab.Open
                                                     else DM1.LiefartTab.Refresh;

                        AddrTab.Close;
                        AddrTab.Sql.Text :=
                          'select JOURNAL.REC_ID as J_REC_ID, '+
                          'JOURNAL.ADDR_ID, ADRESSEN.* '+
                          'from JOURNAL '+
                          'left outer JOIN ADRESSEN on '+
                          'ADRESSEN.REC_ID=JOURNAL.ADDR_ID '+
                          'where JOURNAL.REC_ID='+IntToStr(BelegID);
                        AddrTab.Open;

                        ReKopfTab.Close;
                        ReKopfTab.ParamByName ('ID').Value :=BelegID;
                        ReKopfTab.ParamByName ('QUELLE').Value :=NewQuelle;
                        ReKopfTab.Open;

                      end;
            VK_KASSE : begin
                        MainKey :='MAIN\REPORT\KASSENBON';
                        Caption :='Kassenbon drucken';
                        ppDesigner1.Caption :='Kassenbon-Layout bearbeiten';

                        RechReport.DataPipeline :=BonPipeline;
                        if not DM1.ZahlartTab.Active then DM1.ZahlartTab.Open
                                                     else DM1.ZahlartTab.Refresh;
                        BonTab.Close;
                        BonPosTab.Close;
                        BonTab.ParamByName ('ID').Value :=BelegID;
                        BonTab.ParamByName ('QUELLE').Value :=VK_RECH;
                        BonTab.ParamByName ('QUELLE_SUB').Value :=2;
                        BonTab.Open;
                        BonPosTab.ParamByName ('REC_ID').Value :=BelegID;
                        BonPosTab.Open;
                      end;

       end; //case Quelle

       if Quelle<>NewQuelle then
       begin
         Quelle :=NewQuelle;

         ReportTab.Close;
         ReportTab.Open;

         Res :=FormularCB.Locate (ReportTabVal_Char,'DEFAULT',True);
         if not Res then
         begin
           ReportTab.First;
           FormularCB.DisplayValue :=ReportTabName.Value;
         end
            else
         begin
              FormularCB.DisplayValue :=ReportTabName.Value;
         end;

         ZielCB.Items.Assign (RechReport.PrinterSetup.PrinterNames);
         I :=ZielCB.Items.IndexOf ('Screen');
         if I>=0 then ZielCB.Items.Delete (i);

         if ZielCB.Items.Count >=0 then
          for i:=0 to ZielCB.Items.Count-1 do
           if ZielCB.Items[i]=RechReport.PrinterSetup.PrinterName
             then ZielCB.ItemIndex :=I;

         if ZielCB.ItemIndex=-1 then
          if ZielCB.Items.IndexOf ('Default')>-1
           then ZielCB.ItemIndex := ZielCB.Items.IndexOf ('Default');

       end;
       ZielCBChange(Self);

       DM1.FirmaTab.Open;

       if not Preview then
       begin
          repeat until ShowModal<>mrRetry;
       end
          else
       begin
          PrintPreviewForm.FormularCB.LookupDisplay :='NAME';
          PrintPreviewForm.FormularCB.LookupField   :='NAME';
          PrintPreviewForm.FormularCB.LookupSource  :=ReportDS;
          PrintPreviewForm.FormularCB.DisplayValue  :=FormularCB.DisplayValue;

          PrintPreviewForm.ppViewer1.Reset;
          PrintPreviewForm.ppViewer1.Report :=RechReport;
          PrintPreviewForm.ShowModal;
          PrintPreviewForm.ppViewer1.Report :=nil;
       end;

     finally
       if Preview then
       begin
         PrintPreviewForm.Free;
         PrintPreviewForm :=nil;
       end;
     end;

     if DM1.FirmaTab.Active then DM1.FirmaTab.Close;
     if ReSNTab.Active then ReSNTab.Close;
     if RePosTab.Active then RePosTab.Close;
     if REKopfTab.Active then ReKopfTab.Close;
     if LsKopfTab.Active then LsKopfTab.Close;
     if LSPosTab.Active then LsPosTab.Close;
     if BonTab.Active then BonTab.Close;
     if BonPosTab.Active then BonPosTab.Close;
     if ArtTab.Active then ArtTab.Close;
     if AddrTab.Active then AddrTab.Close;
end;
//------------------------------------------------------------------------------
procedure TPrintRechForm.ShowJournalDlg (NewQuelle : Integer; Von, Bis : tDateTime; Z : String);
var Res : Boolean; i:integer;
begin
     EMail  :='';
     Fax    :='';

     VDat :=Von;
     BDat :=Bis;

     ZeitraumStr :=Z;

     case NewQuelle of
          VK_RECH : begin
                      MainKey :='MAIN\REPORT\VK_RE_JOURNAL';
                      Caption :='Rechnungsjournal drucken';
                      ppDesigner1.Caption :='Rechnungsjournal-Layout bearbeiten';
                      RechReport.DataPipeline :=KopfPipeline;
                      JName :='Rechnungsjournal';
                    end;
          VK_KASSE: begin
                      MainKey :='MAIN\REPORT\VK_KAS_JOURNAL';
                      Caption :='Kassenjournal drucken';
                      ppDesigner1.Caption :='Kassenjournal-Layout bearbeiten';
                      RechReport.DataPipeline :=KasTagSumPipeline;
                      JName :='Kassenjournal';
                    end;
          VK_LIEF : begin
                      MainKey :='MAIN\REPORT\VK_LI_JOURNAL';
                      Caption :='Lieferscheinjournal drucken';
                      ppDesigner1.Caption :='Lieferscheinjournal-Layout bearbeiten';
                      RechReport.DataPipeline :=KopfPipeline;
                      JName :='Lieferschein-Journal';
                    end;
          VK_AGB  : begin
                      MainKey :='MAIN\REPORT\VK_AGB_JOURNAL';
                      Caption :='Angebotsjournal drucken';
                      ppDesigner1.Caption :='Angebotsjournal-Layout bearbeiten';
                      RechReport.DataPipeline :=KopfPipeline;
                      JName :='Angebotsjournal';
                    end;
          EK_RECH : begin
                      MainKey :='MAIN\REPORT\EK_RE_JOURNAL';
                      Caption :='Einkaufsjournal drucken';
                      ppDesigner1.Caption :='Einkaufsjournal-Layout bearbeiten';
                      RechReport.DataPipeline :=KopfPipeline;
                      JName :='Einkaufsjournal';
                    end;
          EK_BEST : begin
                      MainKey :='MAIN\REPORT\EK_BEST_JOURNAL';
                      Caption :='EK-Bestelljournal drucken';
                      ppDesigner1.Caption :='EK-Bestelljournal-Layout bearbeiten';
                      RechReport.DataPipeline :=KopfPipeline;
                      JName :='EK-Bestelljournal';
                    end;
     end; //case Quelle

     if NewQuelle <> VK_KASSE then
     begin
       JournalTab.Close;
       JournalTab.Sql.Clear;
       JournalTab.Sql.Add ('select * from JOURNAL');
       JournalTab.Sql.Add ('where QUELLE=:QUELLE');

       // Bei VK-Rechnung keine Kasse anzeigen
       if NewQuelle=VK_RECH
        then JournalTab.Sql.Add ('and QUELLE_SUB <> 2');

       if NewQuelle = VK_LIEF
        then JournalTab.Sql.Add ('and LDATUM >=:VDAT and LDATUM <=:BDAT')
        else JournalTab.Sql.Add ('and RDATUM >=:VDAT and RDATUM <=:BDAT');

       if NewQuelle = VK_LIEF
        then JournalTab.Sql.Add ('ORDER BY LDATUM, VLSNUM')
        else JournalTab.Sql.Add ('ORDER BY RDATUM, VRENUM');


       JournalTab.ParamByName ('QUELLE').Value :=NewQuelle;
       JournalTab.ParamByName ('VDAT').Value   :=VDAT;
       JournalTab.ParamByName ('BDAT').Value   :=BDAT;
       JournalTab.Open;

       SumQuery.Close;

       SumQuery.Sql.Clear;
       SumQuery.Sql.Add('select');
       SumQuery.Sql.Add('STADIUM, sum(NSUMME) as NETTO,');
       SumQuery.Sql.Add('sum(MSUMME) as MWST,');
       SumQuery.Sql.Add('sum(MSUMME_1) as MWST_1,');
       SumQuery.Sql.Add('sum(MSUMME_2) as MWST_2,');
       SumQuery.Sql.Add('sum(MSUMME_3) as MWST_3,');
       SumQuery.Sql.Add('sum(BSUMME) as BRUTTO,');
       SumQuery.Sql.Add('sum(IST_BETRAG) as BEZAHLT,');
       SumQuery.Sql.Add('WAEHRUNG');

       SumQuery.Sql.Add('from JOURNAL');
       SumQuery.Sql.Add('where QUELLE=:QUELLE');
       SumQuery.Sql.Add('and RDATUM >=:VDAT and RDATUM <=:BDAT');

       if NewQuelle=VK_RECH // KAsse nicht mit hinzurechnen !!!
        then SumQuery.Sql.Add('and QUELLE_SUB != 2');

       SumQuery.Sql.Add('group by WAEHRUNG, STADIUM');



       SumQuery.ParamByName ('QUELLE').Value :=NewQuelle;
       SumQuery.ParamByName ('VDAT').Value   :=VDAT;
       SumQuery.ParamByName ('BDAT').Value   :=BDAT;
       SumQuery.Open;
     end else
     begin
       //VK-Kasse
       if KasTagSumTab.Active then KasTagSumTab.Close;
       if KasTagDetailTab.Active then KasTagDetailTab.Close;

       KasTagSumTab.ParamByName ('VDAT').Value   :=VDAT;
       KasTagSumTab.ParamByName ('BDAT').Value   :=BDAT;

       KasTagSumTab.Open;
     end;

     if Quelle<>NewQuelle+100 then
     begin
       Quelle :=NewQuelle+100;

       ReportTab.Close;
       ReportTab.Open;

       Res :=FormularCB.Locate (ReportTabVal_Char,'DEFAULT',True);
       if not Res then
       begin
         ReportTab.First;
         FormularCB.DisplayValue :=ReportTabName.Value;
       end
          else
       begin
            FormularCB.DisplayValue :=ReportTabName.Value;
       end;

       ZielCB.Items.Assign (RechReport.PrinterSetup.PrinterNames);
       I :=ZielCB.Items.IndexOf ('Screen');
       if I>=0 then ZielCB.Items.Delete (i);

       if ZielCB.Items.Count >=0 then
        for i:=0 to ZielCB.Items.Count-1 do
         if ZielCB.Items[i]=RechReport.PrinterSetup.PrinterName
           then ZielCB.ItemIndex :=I;

       if ZielCB.ItemIndex=-1 then
        if ZielCB.Items.IndexOf ('Default')>-1
         then ZielCB.ItemIndex := ZielCB.Items.IndexOf ('Default');

     end;
     ZielCBChange(Self);
     DM1.FirmaTab.Open;

     repeat until ShowModal<>mrRetry;

     if DM1.FirmaTab.Active then DM1.FirmaTab.Close;
     if JournalTab.Active then JournalTab.Close;
     if KasTagSumTab.Active then KasTagSumTab.Close;
     if KasTagDetailTab.Active then KasTagDetailTab.Close;

     //ReportTab.Close;
end;
//------------------------------------------------------------------------------
procedure TPrintRechForm.ShowOposDlg (NewQuelle : Integer; Von, Bis : tDateTime; Z :String);
var Res : Boolean; i:integer;
begin
     EMail  :='';
     Fax    :='';

     OPosTab.Close;

     case NewQuelle of
          DEB_OPOS : begin
                       MainKey :='MAIN\REPORT\OPOS_DEB';
                       Caption :='OPOS Debitoren drucken';
                       ppDesigner1.Caption :='Offene-Posten-Liste Debitoren bearbeiten';
                       RechReport.DataPipeline :=OposPipeline;
                       OPosTab.ParamByName ('QUELLE').Value :=VK_RECH;
                       OPosTab.ParamByName ('VONDATUM').Value :=Von;
                     end;
          KRD_OPOS : begin
                       MainKey :='MAIN\REPORT\OPOS_KRD';
                       Caption :='OPOS Kreditoren drucken';
                       ppDesigner1.Caption :='Offene-Posten-Liste Kreditoren bearbeiten';
                       RechReport.DataPipeline :=OposPipeline;
                       OPosTab.ParamByName ('QUELLE').Value :=EK_RECH;
                       OPosTab.ParamByName ('VONDATUM').Value :=Von;
                     end;

     end; //case Quelle

     OPosTab.Open;

     if Quelle<>NewQuelle then
     begin
       Quelle :=NewQuelle;

       ReportTab.Close;
       ReportTab.Open;

       Res :=FormularCB.Locate (ReportTabVal_Char,'DEFAULT',True);
       if not Res then
       begin
         ReportTab.First;
         FormularCB.DisplayValue :=ReportTabName.Value;
       end
          else
       begin
            FormularCB.DisplayValue :=ReportTabName.Value;
       end;

       ZielCB.Items.Assign (RechReport.PrinterSetup.PrinterNames);
       I :=ZielCB.Items.IndexOf ('Screen');
       if I>=0 then ZielCB.Items.Delete (i);

       if ZielCB.Items.Count >=0 then
        for i:=0 to ZielCB.Items.Count-1 do
         if ZielCB.Items[i]=RechReport.PrinterSetup.PrinterName
           then ZielCB.ItemIndex :=I;

       if ZielCB.ItemIndex=-1 then
        if ZielCB.Items.IndexOf ('Default')>-1
         then ZielCB.ItemIndex := ZielCB.Items.IndexOf ('Default');

     end;
     ZielCBChange(Self);
     DM1.FirmaTab.Open;

     repeat until ShowModal<>mrRetry;

     if DM1.FirmaTab.Active then DM1.FirmaTab.Close;

     if OPosTab.Active then OPosTab.Close;
     //ReportTab.Close;
end;
//------------------------------------------------------------------------------
procedure TPrintRechForm.ShowKasBuchDlg (Von, Bis, LeBuchung : tDateTime;
                                         StartSaldo, EndSaldo : Double;
                                         Z :String);

var Res : Boolean; i:integer; ja, mo, ta : word;
begin
     EMail  :='';
     Fax    :='';

     KasBuch.Close;

     KasBuchStartDatum :=Von;
     KasBuchEndDatum   :=Bis;
     KasBuchLeBuchungDatum :=leBuchung;
     KasBuchStartSaldo :=StartSaldo;
     KasBuchEndSaldo   :=EndSaldo;
     KasBuchZeitraum   :=Z;

     if KasBuchEndDatum > Now then KasBuchEndDatum :=Now;


     MainKey :='MAIN\REPORT\KASSENBUCH';
     Caption :='Kassenbuch drucken';
     ppDesigner1.Caption :='Kassenbuch bearbeiten';
     RechReport.DataPipeline :=KasBuchPipeline;

     DEcodeDate (Von, Ja, Mo, Ta);

     KasBuch.ParamByName ('JAHR').Value :=Ja;
     KasBuch.ParamByName ('VDAT').Value :=Von;
     KasBuch.ParamByName ('BDAT').Value :=Bis;

     KasBuch.Open;

     if Quelle<>KAS_BUCH then
     begin
       Quelle :=KAS_BUCH;

       ReportTab.Close;
       ReportTab.Open;

       Res :=FormularCB.Locate (ReportTabVal_Char,'DEFAULT',True);
       if not Res then
       begin
         ReportTab.First;
         FormularCB.DisplayValue :=ReportTabName.Value;
       end
          else
       begin
            FormularCB.DisplayValue :=ReportTabName.Value;
       end;

       ZielCB.Items.Assign (RechReport.PrinterSetup.PrinterNames);
       I :=ZielCB.Items.IndexOf ('Screen');
       if I>=0 then ZielCB.Items.Delete (i);

       if ZielCB.Items.Count >=0 then
        for i:=0 to ZielCB.Items.Count-1 do
         if ZielCB.Items[i]=RechReport.PrinterSetup.PrinterName
           then ZielCB.ItemIndex :=I;

       if ZielCB.ItemIndex=-1 then
        if ZielCB.Items.IndexOf ('Default')>-1
         then ZielCB.ItemIndex := ZielCB.Items.IndexOf ('Default');

     end;
     ZielCBChange(Self);
     DM1.FirmaTab.Open;

     repeat until ShowModal<>mrRetry;

     if DM1.FirmaTab.Active then DM1.FirmaTab.Close;
     if KasBuch.Active then KasBuch.Close;
end;
//------------------------------------------------------------------------------
procedure TPrintRechForm.ShowMahnungDlg (Addr_ID : Integer);
var Res : Boolean; I:Integer;
begin
     EMail  :='';
     Fax    :='';

     if DM1.ReadString ('MAIN\MAHNUNG','DEFAULT','')='' then
      DM1.WriteString ('MAIN\MAHNUNG','DEFAULT','Einstellungen f�r Mahnlauf');

     for i:=1 to 5 do
     begin
        DM1.MahnFrist[i] :=DM1.ReadInteger ('MAIN\MAHNUNG','FRIST_STUFE_'+Inttostr(I),-1);
        if DM1.MahnFrist[i]<0 then
        begin
           DM1.MahnFrist[i] :=14;
           DM1.WriteInteger ('MAIN\MAHNUNG','FRIST_STUFE_'+Inttostr(I),DM1.MahnFrist[i]);
        end;
     end;


     MahnPrintTab.Close;
     MahnPrintTab.Sql.Clear;
     MahnPrintTab.Sql.Add ('select J2.REC_ID,J2.QUELLE,J2.VRENUM,J2.RDATUM,J2.ADDR_ID,');
     MahnPrintTab.Sql.Add ('J2.NSUMME,J2.MSUMME_0,J2.MSUMME_1,J2.MSUMME_2,J2.MSUMME_3,J2.MSUMME,J2.BSUMME,');
     MahnPrintTab.Sql.Add ('J2.ZAHLART,J2.STADIUM,J2.WAEHRUNG,');
     MahnPrintTab.Sql.Add ('J2.SOLL_STAGE,J2.SOLL_SKONTO,J2.SOLL_NTAGE,');
     MahnPrintTab.Sql.Add ('J2.IST_ANZAHLUNG,J2.IST_SKONTO,J2.IST_ZAHLDAT,J2.IST_BETRAG,J2.MAHNKOSTEN,J2.GEGENKONTO,');
     MahnPrintTab.Sql.Add ('J2.KUN_NUM,J2.KUN_ANREDE,J2.KUN_NAME1,J2.KUN_NAME2,J2.KUN_NAME3,J2.KUN_ABTEILUNG,');
     MahnPrintTab.Sql.Add ('J2.KUN_STRASSE,J2.KUN_PLZ,J2.KUN_LAND,J2.KUN_ORT,');
     MahnPrintTab.Sql.Add ('(J2.BSUMME - J2.IST_ANZAHLUNG - J2.IST_BETRAG) as SALDO,');
     MahnPrintTab.Sql.Add ('J2.MAHNSTUFE, to_days(curdate())-TO_DAYS(J2.RDATUM) as TAGE_OFFEN,J2.MAHNPRINT,J2.MAHNDATUM,');
     MahnPrintTab.Sql.Add ('AD.EMAIL,AD.EMAIL2,AD.TELE1,AD.FAX');
     MahnPrintTab.Sql.Add ('from JOURNAL as J1');
     MahnPrintTab.Sql.Add ('INNER JOIN JOURNAL as J2 ON J1.ADDR_ID=J2.ADDR_ID');
     MahnPrintTab.Sql.Add ('LEFT OUTER JOIN ADRESSEN AD ON AD.REC_ID=J1.ADDR_ID');
     MahnPrintTab.Sql.Add ('where ');
     if Addr_ID=-1 then MahnPrintTab.Sql.Add ('J1.MAHNPRINT=1 and ');
     MahnPrintTab.Sql.Add ('J1.STADIUM BETWEEN 20 and 79 and J1.QUELLE=3 and YEAR(J1.RDATUM)>2000 and ');
     MahnPrintTab.Sql.Add ('J2.STADIUM BETWEEN 20 and 79 and J2.Quelle=3 and YEAR(J2.RDATUM)>2000 and J2.BSUMME !=0');
     if Addr_ID>0 then MahnPrintTab.Sql.Add ('and J2.ADDR_ID='+Inttostr(Addr_ID));
     MahnPrintTab.Sql.Add ('GROUP BY J2.REC_ID order by J2.KUN_NAME1, J2.ADDR_ID, MAHNSTUFE DESC, J2.RDATUM, J2.VRENUM');



     MainKey :='MAIN\REPORT\MAHNUNG';
     Caption :='Mahnung drucken';
     ppDesigner1.Caption :='Mahnung bearbeiten';
     RechReport.DataPipeline :=MahnPrintPipeline;

     MahnPrintTab.Open;

     EMail :=MahnPrintTabEMail.AsString;
     Fax   :=MahnPrintTabFax.AsString;

     if Quelle<>MAHNUNG then
     begin
       Quelle :=MAHNUNG;

       ReportTab.Close;
       ReportTab.Open;

       Res :=FormularCB.Locate (ReportTabVal_Char,'DEFAULT',True);
       if not Res then
       begin
         ReportTab.First;
         FormularCB.DisplayValue :=ReportTabName.Value;
       end
          else
       begin
            FormularCB.DisplayValue :=ReportTabName.Value;
       end;

       ZielCB.Items.Assign (RechReport.PrinterSetup.PrinterNames);
       I :=ZielCB.Items.IndexOf ('Screen');
       if I>=0 then ZielCB.Items.Delete (i);

       if ZielCB.Items.Count >=0 then
        for i:=0 to ZielCB.Items.Count-1 do
         if ZielCB.Items[i]=RechReport.PrinterSetup.PrinterName
           then ZielCB.ItemIndex :=I;

       if ZielCB.ItemIndex=-1 then
        if ZielCB.Items.IndexOf ('Default')>-1
         then ZielCB.ItemIndex := ZielCB.Items.IndexOf ('Default');

     end;
     ZielCBChange(Self);
     DM1.FirmaTab.Open;

     repeat until ShowModal<>mrRetry;

     if DM1.FirmaTab.Active then DM1.FirmaTab.Close;
     if MahnPrintTab.Active then MahnPrintTab.Close;
end;
//------------------------------------------------------------------------------
procedure TPrintRechForm.ShowBriefDlg (TextID:Integer; Vorschau : Boolean);
var Res : Boolean; i:integer;
begin
     EMail  :='';
     Fax    :='';

     try
       MainKey :='MAIN\REPORT\SCHRIFTVERKEHR';
       Caption :='Brief drucken';
       ppDesigner1.Caption :='Layout Brief bearbeiten';
       RechReport.DataPipeline :=TextPipeline;

       TextTab.Close;
       TextTab.ParamByName ('ID').AsInteger :=TextID;
       TextTab.Open;

       EMail :=TextTabEMail.AsString;
       Fax   :=TextTabFax.AsString;


       DM1.FirmaTab.Open;

       if Quelle<>TEXTVERARB then
       begin
         Quelle :=TEXTVERARB;

         ReportTab.Close;
         ReportTab.Open;

         Res :=FormularCB.Locate (ReportTabVal_Char,'DEFAULT',True);
         if not Res then
         begin
           ReportTab.First;
           FormularCB.DisplayValue :=ReportTabName.Value;
         end
            else
         begin
              FormularCB.DisplayValue :=ReportTabName.Value;
         end;

         ZielCB.Items.Assign (RechReport.PrinterSetup.PrinterNames);
         I :=ZielCB.Items.IndexOf ('Screen');
         if I>=0 then ZielCB.Items.Delete (i);

         if ZielCB.Items.Count >=0 then
          for i:=0 to ZielCB.Items.Count-1 do
           if ZielCB.Items[i]=RechReport.PrinterSetup.PrinterName
             then ZielCB.ItemIndex :=I;

         if ZielCB.ItemIndex=-1 then
          if ZielCB.Items.IndexOf ('Default')>-1
           then ZielCB.ItemIndex := ZielCB.Items.IndexOf ('Default');

       end;
       ZielCBChange(Self);

       if Vorschau
        then VorschauBtnClick(Self)
        else repeat until ShowModal<>mrRetry;

     finally
       TextTab.Close;
       DM1.FirmaTab.Close;
     end;
//     ReportTab.Close;
end;
//------------------------------------------------------------------------------
procedure TPrintRechForm.FormShow(Sender: TObject);
begin
     PrintBtn.SetFocus;
end;
//------------------------------------------------------------------------------
procedure TPrintRechForm.ReportTabBeforePost(DataSet: TDataSet);
begin
     ReportTabMAINKEY.AsString :=MainKey;
     ReportTabVAL_TYP.Value :=8; // Blob-Bin�r (Formular)
end;
//------------------------------------------------------------------------------
procedure TPrintRechForm.ReportTabBeforeOpen(DataSet: TDataSet);
begin
     ReportTab.ParamByName('KEY').Value :=MainKey;
end;
//------------------------------------------------------------------------------
procedure TPrintRechForm.CloseBtnClick(Sender: TObject);
begin
     ModalResult :=mrCancel;
     //Close;
end;
//------------------------------------------------------------------------------
procedure TPrintRechForm.FormularCBChange(Sender: TObject);
begin
     RechReport.Template.DatabaseSettings.Name :=FormularCB.DisplayValue;
     try RechReport.Template.LoadFromDatabase; except end;
     Rechreport.PrinterSetup.Copies :=trunc(ANzCopy.Value);
     RechReport.Language :=lgGerman;
     RechReport.OnPreviewFormCreate :=RechReportPreviewFormCreate;
     RechReport.AllowPrintToFile :=True;
end;
//------------------------------------------------------------------------------
procedure TPrintRechForm.ZielCBChange(Sender: TObject);
begin
     {set the printer name for the device}
     RechReport.PrinterSetup.PrinterName  := ZielCB.Items[ZielCB.ItemIndex];

     Application.ProcessMessages;

     BinNamCB.Items.Assign (RechReport.PrinterSetup.BinNames);

     if BinNamCB.Items.IndexOf(RechReport.PrinterSetup.BinName)>-1
      then BinNamCB.ItemIndex :=BinNamCB.Items.IndexOf(RechReport.PrinterSetup.BinName);

     if BinNamCB.ItemIndex=-1 then BinNamCB.ItemIndex :=BinNamCB.Items.IndexOf('Default');

     AnzCopy.Value :=Rechreport.PrinterSetup.Copies;
end;
//------------------------------------------------------------------------------
procedure TPrintRechForm.PrintBtnClick(Sender: TObject);
{$IFDEF PRO}
var Reg      : tRegistry;
    //FaxMaker : variant;
    //FaxError : Boolean;
    //OldPort  : String;
{$ENDIF}
begin
     case quelle of
          VK_RECH     : RechReport.PrinterSetup.DocumentName :='CAO-Faktura Rechnung Nr.:'+
                        Inttostr(ReKopfTabVRENUM.Value);

          VK_AGB      : RechReport.PrinterSetup.DocumentName :='CAO-Faktura Angebot Nr.:'+
                        Inttostr(ReKopfTabVRENUM.Value);

          VK_LIEF     : RechReport.PrinterSetup.DocumentName :='CAO-Faktura Lieferschein Nr.:'+
                        Inttostr(LsKopfTabVLSNUM.Value);

          EK_BEST     : RechReport.PrinterSetup.DocumentName :='CAO-Faktura EK-Bestellung Nr.:'+
                        Inttostr(ReKopfTabVRENUM.Value);

          VK_KASSE    : RechReport.PrinterSetup.DocumentName :='CAO-Faktura Kassenbon Nr.:'+
                        Inttostr(ReKopfTabVRENUM.Value);

          VK_RECH+100 : RechReport.PrinterSetup.DocumentName :='CAO-Faktura Verkaufsjournal';

          VK_KASSE+100: RechReport.PrinterSetup.DocumentName :='CAO-Faktura Kassenjournal';

          VK_AGB+100  : RechReport.PrinterSetup.DocumentName :='CAO-Faktura Angebotsjournal';

          EK_RECH+100 : RechReport.PrinterSetup.DocumentName :='CAO-Faktura Einkaufsjournal';

          VK_LIEF+100 : RechReport.PrinterSetup.DocumentName :='CAO-Faktura Lieferscheinjournal';

          EK_BEST+100 : RechReport.PrinterSetup.DocumentName :='CAO-Faktura EK-Bestelljournal';

          KRD_OPOS    : RechReport.PrinterSetup.DocumentName :='CAO-Faktura OPOS-Debitoren';

          DEB_OPOS    : RechReport.PrinterSetup.DocumentName :='CAO-Faktura OPOS-Kreditoren';

          KAS_BUCH    : RechReport.PrinterSetup.DocumentName :='CAO-Faktura Kassenbuch';

          TEXTVERARB  : RechReport.PrinterSetup.DocumentName :='CAO-Faktura Schriftverkehr';
     end;

     RechReport.DeviceType :=dtPrinter;
     RechReport.ShowPrintDialog :=False;

     Rechreport.PrinterSetup.Copies :=trunc(AnzCopy.Value);

     PrintAbort :=False;

     {$IFDEF PRO}
     // F�rs Faxen Faxnummer in die Zwischenablage
     Clipboard.AsText :=Fax;

     // Fritz-Fax
     if (uppercase(RechReport.PrinterSetup.PrinterName)='FRITZ!FAX')and
        (length(Fax)>0) then
     begin
        Reg :=tRegistry.Create;
        try
           Reg.RootKey:=HKEY_CURRENT_USER;
           Reg.Access :=KEY_WRITE;
           if Reg.OpenKey('Software\VB and VBA Program '+
                          'Settings\FRITZ!fax\Print',False) then
           begin
             Reg.WriteString (RechReport.PrinterSetup.DocumentName, Fax);
           end;
        finally
           Reg.Free;
        end;
     end;
       { else

     // GFI-Faxmaker �ber COM-API
     if (uppercase(RechReport.PrinterSetup.PrinterName)='FAXMAKER')and
        (length(Fax)>0) then
     begin
        try
           Faxerror :=False;
           FaxMaker :=createoleobject ('fmfaxapi.application');
           try
              FaxMaker.Reset(False);
              FaxMaker.Coverpage :='';
              FaxMaker.HighRes :=True;
              FaxMaker.Subject :='';
              FaxMaker.AddRecipient ('', //vorname
                                     '', //name
                                     '', //firma
                                     '', //abteilung
                                     Fax,//FAX
                                     '', //meintelefon
                                     '');//email

              FaxMaker.SetBodyText ('');

              // jetzt das Fax in Datei drucken

              OldPort :=RechReport.Printer.PrinterInfo.Port;

              RechReport.Printer.Port
              PrinterInfo.Port :=TempPpad+'temp.fax';
              RechReport.SetPrinter
              RechReport.Print;
              //RechReport.Printer.PrinterInfo.Port :=OldPort;

              // Als Attachment anh�ngen


              // Fax versenden
              if FaxMaker.SendFax = True
               then MessageDlg ('Fax erfolgreich gesendet !!!',mtinformation,[mbok],0)
               else begin MessageDlg ('Fax-Error',mtError,[mbok],0); FaxError :=True; End;
           finally
              FaxMaker :=null;
           end;
        except
           FaxError :=True;
        end;

        if FaxError then
        begin

        end;
     end
        else  }
     {$ENDIF}
     RechReport.Print;

     if not PrintAbort then
     begin
       if quelle in [VK_RECH, VK_AGB, EK_BEST] then
       begin
         ReKopfTab.DisableControls;
         try
           ReKopfTab.First;
           while not ReKopfTab.Eof do
           begin
             //Beleg als gedruckt markieren
             if (ReKopfTabPrint_Flag.AsBoolean=False) then
             begin
               DM1.Uniquery.Close;
               DM1.Uniquery.SQL.Text :='UPDATE JOURNAL SET PRINT_FLAG="Y" '+
                                       'WHERE REC_ID='+IntToStr(ReKopfTabRec_ID.AsInteger);
               DM1.Uniquery.ExecSQL;
             end;

             ReKopfTab.Next;
           end;
         finally
           ReKopfTab.EnableControls;
         end;
       end
       else if Quelle = VK_LIEF then
       begin
         LsKopfTab.DisableControls;
         try
           LsKopfTab.First;
           while not LsKopfTab.Eof do
           begin
             //Beleg als gedruckt markieren
             if (LsKopfTabPrint_Flag.AsBoolean=False) then
             begin
               DM1.Uniquery.Close;
               DM1.Uniquery.SQL.Text :='UPDATE JOURNAL SET PRINT_FLAG="Y" '+
                                       'WHERE REC_ID='+IntToStr(LsKopfTabRec_ID.AsInteger);
               DM1.Uniquery.ExecSQL;
             end;

             lsKopfTab.Next;
           end;
         finally
           LsKopfTab.EnableControls;
         end;
       end;
     end;
end;
//------------------------------------------------------------------------------
procedure TPrintRechForm.LayoutBtnClick(Sender: TObject);
begin
     // Journale
     if Quelle in [VK_RECH+100, VK_AGB+100, VK_LIEF+100, EK_RECH+100, EK_BEST+100] then
     begin
        JournalPipeline.Visible :=True;
        KopfPipeline.Visible :=True;

        ppDesigner1.ShowModal;
        if RechReport.Template.DatabaseSettings.Name<>FormularCB.DisplayValue
         then FormularCB.DisplayValue :=RechReport.Template.DatabaseSettings.Name;

        JournalPipeline.Visible :=False;
        KopfPipeline.Visible :=False;
     end else
     // Kassenjournal
     if Quelle = VK_KASSE+100 then
     begin
        KasTagSumPipeline.Visible :=True;
        KasTagDetailPipeline.Visible :=True;

        ppDesigner1.ShowModal;
        if RechReport.Template.DatabaseSettings.Name<>FormularCB.DisplayValue
         then FormularCB.DisplayValue :=RechReport.Template.DatabaseSettings.Name;

        KasTagSumPipeline.Visible :=False;
        KasTagDetailPipeline.Visible :=False;
     end else
     // Belege Rechnung, Angebot
     if Quelle in [VK_RECH, VK_AGB, EK_BEST] then
     begin
        ReKopfPipeline.Visible :=True;
        RePosPipeline.Visible  :=True;
        ReSNPipeline.Visible   :=True;
        ArtPipeline.Visible    :=True;
        AddrPipeline.Visible   :=True;

        ppDesigner1.ShowModal;
        if RechReport.Template.DatabaseSettings.Name<>FormularCB.DisplayValue
         then FormularCB.DisplayValue :=RechReport.Template.DatabaseSettings.Name;

        ReKopfPipeline.Visible :=False;
        RePosPipeline.Visible  :=False;
        ReSNPipeline.Visible   :=False;
        ArtPipeline.Visible    :=False;
        AddrPipeline.Visible   :=False;
     end else
     // Belege Lieferschein
     if Quelle = VK_LIEF then
     begin
        LsKopfPipeline.Visible :=True;
        LsPosPipeline.Visible  :=True;
        ReSNPipeline.Visible   :=True;
        ArtPipeline.Visible    :=True;
        AddrPipeline.Visible   :=True;

        ppDesigner1.ShowModal;
        if RechReport.Template.DatabaseSettings.Name<>FormularCB.DisplayValue
         then FormularCB.DisplayValue :=RechReport.Template.DatabaseSettings.Name;

        LsKopfPipeline.Visible :=False;
        LsPosPipeline.Visible  :=False;
        ReSNPipeline.Visible   :=False;
        ArtPipeline.Visible    :=False;
        AddrPipeline.Visible   :=False;
     end else
     // OPOS-Listen
     if Quelle in [KRD_OPOS, DEB_OPOS] then
     begin
        OPosPipeline.Visible :=True;

        ppDesigner1.ShowModal;
        if RechReport.Template.DatabaseSettings.Name<>FormularCB.DisplayValue
         then FormularCB.DisplayValue :=RechReport.Template.DatabaseSettings.Name;

         OPosPipeline.Visible :=False;
     end else
     if Quelle=KAS_BUCH then
     begin
        KasBuchPipeline.Visible :=True;
        KasKopfPipeline.Visible :=True;

        ppDesigner1.ShowModal;
        if RechReport.Template.DatabaseSettings.Name<>FormularCB.DisplayValue
         then FormularCB.DisplayValue :=RechReport.Template.DatabaseSettings.Name;

        KasBuchPipeline.Visible :=False;
        KasKopfPipeline.Visible :=False;
     end else
     if Quelle=MAHNUNG then
     begin
        MahnPrintPipeline.Visible :=True;

        ppDesigner1.ShowModal;
        if RechReport.Template.DatabaseSettings.Name<>FormularCB.DisplayValue
         then FormularCB.DisplayValue :=RechReport.Template.DatabaseSettings.Name;

        MahnPrintPipeline.Visible :=False;
     end else
     if Quelle = VK_KASSE then
     begin
        BonPipeline.Visible :=True;
        BonPosPipeline.Visible :=True;

        ppDesigner1.ShowModal;
        if RechReport.Template.DatabaseSettings.Name<>FormularCB.DisplayValue
         then FormularCB.DisplayValue :=RechReport.Template.DatabaseSettings.Name;

        BonPipeline.Visible :=False;
        BonPosPipeline.Visible :=False;
     end else
     if Quelle = TEXTVERARB then
     begin
        TextPipeline.Visible :=True;

        ppDesigner1.ShowModal;
        if RechReport.Template.DatabaseSettings.Name<>FormularCB.DisplayValue
         then FormularCB.DisplayValue :=RechReport.Template.DatabaseSettings.Name;

        TextPipeline.Visible :=False;
     end;
     RechReport.OnPreviewFormCreate :=RechReportPreviewFormCreate;
     RechReport.AllowPrintToFile :=True;
end;
//------------------------------------------------------------------------------
procedure TPrintRechForm.RechReportPreviewFormCreate(Sender: TObject);
begin
     RechReport.PreviewForm.WindowState := wsMaximized;
     TppViewer(RechReport.PreviewForm.Viewer).ZoomSetting := zs100Percent;
end;
//------------------------------------------------------------------------------
procedure TPrintRechForm.VorschauBtnClick(Sender: TObject);
begin
     RechReport.DeviceType :=dtScreen;
     RechReport.Print;
     RechReport.DeviceType :=dtPrinter;
end;
//------------------------------------------------------------------------------
procedure TPrintRechForm.FormDestroy(Sender: TObject);
begin
end;
//------------------------------------------------------------------------------
procedure TPrintRechForm.AnzCopyChange(Sender: TObject);
begin
     Rechreport.PrinterSetup.Copies :=trunc(ANzCopy.Value);
end;
//------------------------------------------------------------------------------
procedure TPrintRechForm.ppDesigner1CloseQuery(Sender: TObject;
  var CanClose: Boolean);

var Res : Integer;
begin
     if RechReport.Modified then
     begin
        Res :=MessageDlg ('Formular wurde ver�ndert.'+#13#10+
                          'Wollen Sie die �nderungen speichern ?',
                          mtconfirmation,mbyesnocancel,0);
        if Res=mryes then
        begin
           RechReport.Template.Save;
           CanClose :=True;
        end else
        if Res=mrNo then
        begin
           try RechReport.Template.Load; except end;
           CanClose :=True;
        end else CanClose :=False;
     end
     else CanClose :=True;
end;
//------------------------------------------------------------------------------
procedure TPrintRechForm.DruEinrBtnClick(Sender: TObject);
var myPrinter : TppPrinter;
begin
     myPrinter := TppPrinter.Create;
     try
        myPrinter.PrinterSetup :=RechReport.PrinterSetup;

        if myPrinter.ShowSetupDialog
         then RechReport.PrinterSetup := myPrinter.PrinterSetup;

        if ZielCB.Items.IndexOf (RechReport.PrinterSetup.PrinterName)>0
         then ZielCB.ItemIndex :=ZielCB.Items.IndexOf (RechReport.Printer.PrinterName);

       ZielCBChange(Sender);
     finally
        myPrinter.Free;
     end;
end;
//------------------------------------------------------------------------------
procedure TPrintRechForm.ReKopfTabAfterScroll(DataSet: TDataSet);
begin
     if ReKopfTab.ControlsDisabled then exit;

     RePosTab.Close;
     RePosTab.ParamByName ('REC_ID').Value :=ReKopfTabRec_ID.AsInteger;
     RePosTab.Open;

     AddrTab.Locate ('J_REC_ID',ReKopfTabRec_ID.AsInteger,[]);

     ArtTab.Close;
     ArtTab.ParamByName ('JOURNAL_ID').AsInteger :=ReKopfTabRec_ID.AsInteger;
     ArtTab.Open;
end;
//------------------------------------------------------------------------------
procedure TPrintRechForm.LsKopfTabAfterScroll(DataSet: TDataSet);
begin
     LsPosTab.Close;
     LsPosTab.ParamByName ('REC_ID').Value :=LsKopfTabRec_ID.AsInteger;
     LsPosTab.Open;

     AddrTab.Locate ('J_REC_ID',LsKopfTabAddr_ID.AsInteger,[]);

     ArtTab.Close;
     ArtTab.ParamByName ('JOURNAL_ID').AsInteger :=LsKopfTabRec_ID.AsInteger;
     ArtTab.Open;
end;
//------------------------------------------------------------------------------
procedure TPrintRechForm.ReKopfTabCalcFields(DataSet: TDataSet);
var S,Text : String; I : Integer; SK_Betrag : Double;
begin
     if (ReKopfTabSOLL_STAGE.Value>0)and(ReKopfTabSOLL_SKONTO.Value>0) then
     begin
       ReKopfTabSOLL_ZAHLUNGSZIEL.Value :=
          Inttostr(ReKopfTabSOLL_STAGE.Value)+' Tage '+
          FormatFloat ('0.0"%"',ReKopfTabSOLL_SKONTO.Value)+' Skonto, '+
          Inttostr(ReKopfTabSOLL_NTAGE.Value)+' Tage ohne Abzug';
     end
        else
     if (ReKopfTabSOLL_NTAGE.Value>1) then
     begin
        ReKopfTabSOLL_ZAHLUNGSZIEL.Value :=
        Inttostr(ReKopfTabSOLL_NTAGE.Value)+' Tage ohne Abzug';
     end
        else
     begin
        //ReKopfTabSOLL_ZAHLUNGSZIEL.Value :='Sofort';
        ReKopfTabSOLL_ZAHLUNGSZIEL.Value :=
           DM1.ReadString ('MAIN\REPORT','ZAHLUNGSZIEL_SOFORT_TEXT','Sofort');
     end;

     // Lieferanschrift f�r Bestellungen
     if (ReKopfTabQuelle.Value=EK_BEST)and(ReKopfTabLief_Addr_ID.Value>=0) then
     begin
         DM1.KunTab.Close;
         DM1.KunTab.ParamByName ('ID').AsInteger :=ReKopfTabLief_Addr_ID.Value;
         DM1.KunTab.Open;

         if (DM1.KunTab.RecordCount=1)and
            (DM1.KunTabDEFAULT_LIEFANSCHRIFT_ID.Value>0) then
         begin
            // Lieferanschrift aus Kundendaten �bernehmen
            with DM1.UniQuery do
            begin
               SQL.Text :='select * from ADRESSEN_LIEF where REC_ID='+
                          IntToStr(DM1.KunTabDEFAULT_LIEFANSCHRIFT_ID.AsInteger);
               Open;
               if RecordCount>0 then
               begin
                  ReKopfTabCALC_LIEF_ANREDE.AsString  :=FieldByName ('ANREDE').AsString;
                  ReKopfTabCALC_LIEF_NAME1.AsString   :=FieldByName ('NAME1').AsString;
                  ReKopfTabCALC_LIEF_NAME2.AsString   :=FieldByName ('NAME2').AsString;
                  ReKopfTabCALC_LIEF_NAME3.AsString   :=FieldByName ('NAME3').AsString;
                  ReKopfTabCALC_LIEF_STRASSE.AsString :=FieldByName ('STRASSE').AsString;
                  ReKopfTabCALC_LIEF_LAND.AsString    :=FieldByName ('LAND').AsString;
                  ReKopfTabCALC_LIEF_PLZ.AsString     :=FieldByName ('PLZ').AsString;
                  ReKopfTabCALC_LIEF_ORT.AsString     :=FieldByName ('ORT').AsString;
               end;
               Close;
             end;
         end
            else
         if (DM1.KunTab.RecordCount=1) then
         begin
            // Rechnungsanschrift des Kunden �bernehmen
            ReKopfTabCALC_LIEF_ANREDE.AsString  :=DM1.KunTabAnrede.Value;
            ReKopfTabCALC_LIEF_NAME1.AsString   :=DM1.KunTabName1.Value;
            ReKopfTabCALC_LIEF_NAME2.AsString   :=DM1.KunTabName2.Value;
            ReKopfTabCALC_LIEF_NAME3.AsString   :=DM1.KunTabName3.Value;
            ReKopfTabCALC_LIEF_STRASSE.AsString :=DM1.KunTabStrasse.Value;
            ReKopfTabCALC_LIEF_LAND.AsString    :=DM1.KunTabLand.Value;
            ReKopfTabCALC_LIEF_PLZ.AsString     :=DM1.KunTabPLZ.Value;
            ReKopfTabCALC_LIEF_ORT.AsString     :=DM1.KunTabOrt.Value;
         end;

         // EMail-Adresse etc.
         DM1.KunTab.Close;
         DM1.KunTab.ParamByName ('ID').AsInteger :=ReKopfTabAddr_ID.Value;
         DM1.KunTab.Open;

         if (DM1.KunTab.RecordCount=1) then
         begin
            ReKopfTabCALC_EMAIL.AsString :=DM1.KunTabEMail.AsString;
            ReKopfTabCALC_UST_ID.AsString :=DM1.KunTabUST_NUM.AsString;
            ReKopfTabCALC_BRIEFANREDE.AsString :=DM1.KunTabBriefanrede.AsString;

            EMail :=DM1.KunTabEMail.AsString;
            Fax   :=DM1.KunTabFax.AsString;
         end;
     end
        else
     begin
         DM1.KunTab.Close;
         DM1.KunTab.ParamByName ('ID').AsInteger :=ReKopfTabAddr_ID.Value;
         DM1.KunTab.Open;

         if (DM1.KunTab.RecordCount=1){and(DM1.KunTabHAT_LIEFANSR.Value=1)} then
         begin
            // Lieferanschrift aus Kundendaten �bernehmen
            with DM1.UniQuery do
            begin
              SQL.Text :='select * from ADRESSEN_LIEF where REC_ID='+
                         IntToStr(ReKopfTabLIEF_ADDR_ID.AsInteger);
              Open;
              if RecordCount>0 then
              begin
                 ReKopfTabCALC_LIEF_ANREDE.AsString  :=FieldByName ('ANREDE').AsString;
                 ReKopfTabCALC_LIEF_NAME1.AsString   :=FieldByName ('NAME1').AsString;
                 ReKopfTabCALC_LIEF_NAME2.AsString   :=FieldByName ('NAME2').AsString;
                 ReKopfTabCALC_LIEF_NAME3.AsString   :=FieldByName ('NAME3').AsString;
                 ReKopfTabCALC_LIEF_STRASSE.AsString :=FieldByName ('STRASSE').AsString;
                 ReKopfTabCALC_LIEF_LAND.AsString    :=FieldByName ('LAND').AsString;
                 ReKopfTabCALC_LIEF_PLZ.AsString     :=FieldByName ('PLZ').AsString;
                 ReKopfTabCALC_LIEF_ORT.AsString     :=FieldByName ('ORT').AsString;
              end;
            end;
            ReKopfTabCALC_EMAIL.AsString :=DM1.KunTabEMail.AsString;
            ReKopfTabCALC_UST_ID.AsString :=DM1.KunTabUST_NUM.AsString;
            ReKopfTabCALC_BRIEFANREDE.AsString :=DM1.KunTabBriefanrede.AsString;

            EMail :=DM1.KunTabEMail.AsString;
            Fax   :=DM1.KunTabFax.AsString;
         end;
     end;

     // Fahrzeugdaten
     if ReKopfTabKFZ_ID.AsInteger>-1 then
     begin
         DM1.UniQuery.Close;
         DM1.UniQuery.Sql.Clear;
         DM1.UniQuery.Sql.Add ('select * from KFZ where KFZ_ID='+ReKopfTabKFZ_ID.AsString);
         DM1.UniQuery.Open;
         if DM1.UniQuery.RecordCount=1 then
         begin
           ReKopfTabCALC_POLKZ.AsString  :=DM1.UniQuery.FieldByName ('POL_KENNZ').AsString;
           ReKopfTabCALC_FGSTNR.AsString :=DM1.UniQuery.FieldByName ('FGST_NUM').AsString;
         end;
         DM1.UniQuery.Close;
     end;


     if not DM1.ZahlartTab.Active then DM1.ZahlartTab.Open;

     if (DM1.ZahlartTab.Locate ('ZAHL_ID',ReKopfTabZahlart.AsInteger,[])) and
        (DM1.ZahlartTabZahl_ID.AsInteger=ReKopfTabZahlart.AsInteger) then
     begin
       Text :=DM1.ZahlartTabTEXT.AsString;

       // Kontonummer, BLZ und Bank aus Kundenstammdaten holen und ggf. einsetzen
       if (Pos('%BLZ%'  ,Text)>0) or
          (Pos('%KTONR%',Text)>0) or
          (Pos('%BANK%' ,Text)>0) then
       begin
          dm1.uniquery.close;
          dm1.uniquery.sql.text :='select * from ADRESSEN where REC_ID='+
                                  IntToStr(ReKopfTabAddr_ID.AsInteger);
          dm1.uniquery.open;
          if dm1.uniquery.recordcount=1 then
          begin
             S :=dm1.uniquery.fieldbyname ('KTO').AsString;
             I :=Pos('%KTONR%',Text);
             if i>0 then
             begin
                delete (Text,i,length('%KTONR%'));
                Insert (S,Text,I);
             end;

             S :=dm1.uniquery.fieldbyname ('BLZ').AsString;
             I :=Pos('%BLZ%',Text);
             if i>0 then
             begin
                delete (Text,i,length('%BLZ%'));
                Insert (S,Text,I);
             end;

             S :=dm1.uniquery.fieldbyname ('BANK').AsString;
             I :=Pos('%BANK%',Text);
             if i>0 then
             begin
                delete (Text,i,length('%BANK%'));
                Insert (S,Text,I);
             end;
          end;
          dm1.uniquery.close;
       end;

       ReKopfTabZAHLART_LANG.AsString :=Text;

     end;
     if (ReKopfTabKUN_LAND.AsString <>'') and
        (DM1.LandTab.Locate ('ID',ReKopfTabKUN_LAND.AsString,[]))
      then ReKopfTabCALC_LANDLANG.AsString :=DM1.LandTab.FieldByName ('NAME').AsString;


     ReKopfTabSoll_NDatum.AsDateTime :=ReKopfTabRDatum.AsDateTime+ReKopfTabSoll_NTage.AsInteger;
     ReKopfTabSoll_SDatum.AsDateTime :=ReKopfTabRDatum.AsDateTime+ReKopfTabSoll_STage.AsInteger;


     SK_Betrag :=0;
     ReKopfTabCALC_BSUM_SKONTO.AsFloat :=ReKopfTabBSUMME.AsFloat;
     if (ReKopfTabSoll_Skonto.Value>0)and(ReKopfTabBSUMME.AsFloat<>0) then
     begin
        SK_Betrag :=cao_round (ReKopfTabBSUMME.AsFloat * ReKopfTabSoll_Skonto.AsFloat) / 100;
        ReKopfTabCALC_BSUM_SKONTO.AsFloat :=ReKopfTabBSUMME.AsFloat - SK_Betrag;
     end;
     ReKopfTabCALC_SKONTOBETRAG.AsFloat :=SK_Betrag;
end;
//------------------------------------------------------------------------------
procedure TPrintRechForm.SumQueryAfterOpen(DataSet: TDataSet);
begin
     jour_netto   :=0;
     jour_brutto  :=0;
     jour_mwst    :=0;
     jour_mwst1   :=0;
     jour_mwst2   :=0;
     jour_mwst3   :=0;
     jour_bezahlt :=0;
     jour_offen   :=0;

     while not SumQuery.Eof do
     begin
          jour_netto   :=jour_netto   +
                         dm1.CalcLeitWaehrung (SumQueryNetto.Value,
                                               SumQueryWaehrung.Value);
          jour_brutto  :=jour_brutto  +
                         dm1.CalcLeitWaehrung (SumQueryBrutto.Value,
                                               SumQueryWaehrung.Value);
          jour_mwst    :=jour_mwst    +
                         dm1.CalcLeitWaehrung (SumQueryMwst.Value,
                                               SumQueryWaehrung.Value);
          jour_mwst1   :=jour_mwst1   +
                         dm1.CalcLeitWaehrung (SumQueryMWST_1.Value,
                                               SumQueryWaehrung.Value);
          jour_mwst2   :=jour_mwst2   +
                         dm1.CalcLeitWaehrung (SumQueryMWST_2.Value,
                                               SumQueryWaehrung.Value);
          jour_mwst3   :=jour_mwst3   +
                         dm1.CalcLeitWaehrung (SumQueryMWST_3.Value,
                                               SumQueryWaehrung.Value);

          if SumQuerySTADIUM.AsInteger in [80..99] then
          begin
             jour_bezahlt :=jour_bezahlt +
                            dm1.CalcLeitWaehrung (SumQueryBezahlt.Value,
                                                  SumQueryWaehrung.Value);
          end
             else
          begin
             jour_Offen :=jour_Offen +
                          dm1.CalcLeitWaehrung (SumQueryBrutto.Value,
                                                SumQueryWaehrung.Value);
          end;
          SumQuery.Next;
     end;
     SumQuery.Close;
     jour_Offen :=jour_Brutto-jour_Bezahlt;
end;
//------------------------------------------------------------------------------
function TPrintRechForm.KopfPipelineGetFieldValue(
  aFieldName: String): Variant;
begin
     if uppercase(aFieldName)='DATUM_VON'     then Result :=VDAT else
     if uppercase(aFieldName)='DATUM_BIS'     then Result :=BDAT else
     if uppercase(aFieldName)='ZEITRAUM'      then Result :=ZeitraumStr else
     if uppercase(aFieldName)='JOURNALNAME'   then Result :=JName else
     if uppercase(aFieldName)='SUMME_NETTO'   then Result :=jour_Netto else
     if uppercase(aFieldName)='SUMME_MWST'    then Result :=jour_Mwst else
     if uppercase(aFieldName)='SUMME_BRUTTO'  then Result :=jour_Brutto else
     if uppercase(aFieldName)='SUMME_OFFEN'   then Result :=jour_Offen else
     if uppercase(aFieldName)='SUMME_BEZAHLT' then Result :=jour_Bezahlt else
     if uppercase(aFieldName)='SUMME_MWST1'   then Result :=jour_mwst1 else
     if uppercase(aFieldName)='SUMME_MWST2'   then Result :=jour_mwst2 else
     if uppercase(aFieldName)='SUMME_MWST3'   then Result :=jour_mwst3 else
     if uppercase(aFieldName)='LEITWAEHRUNG'  then Result :=DM1.Leitwaehrung;
end;
//------------------------------------------------------------------------------
procedure TPrintRechForm.JournalTabCalcFields(DataSet: TDataSet);
var S : String;
begin
     if Quelle=VK_LIEF+100
      then JournalTabStatus.Value :=GetLiefStatus(JournalTabStadium.AsInteger,True)
      else
     if Quelle=EK_BEST+100
      then JournalTabStatus.Value :=GetEKBestStatus(JournalTabStadium.AsInteger)
      else
     if Quelle=VK_RECH+100
      then JournalTabStatus.Value :=GetRechStatus(JournalTabStadium.AsInteger,VK_RECH)
      else JournalTabStatus.Value :=GetRechStatus(JournalTabStadium.AsInteger,EK_RECH);

     if (JournalTabSOLL_SKONTO.Value>0)and(JournalTabSOLL_STAGE.Value>0)
      then S :=JournalTabSOLL_STAGE.AsString+' T. '+
               formatfloat ('0.0"%"',JournalTabSOLL_SKONTO.Value)+' '+
               JournalTabSOLL_NTAGE.AsString+' T.Netto'
      else if JournalTabSOLL_NTAGE.Value < 2
            then S :='sofort'
            else S :=JournalTabSOLL_NTAGE.AsString+' Tage Netto';

     if Quelle in [VK_RECH+100, VK_AGB+100, EK_RECH+100]
      then JournalTabKonditionen.Value :=S;

     DM1.KunTab.Close;
     DM1.KunTab.ParamByName ('ID').AsInteger :=JournalTabAddr_ID.Value;
     DM1.KunTab.Open;

     if (DM1.KunTab.RecordCount=1) then
     begin
        // aus Kundendaten �bernehmen
        JournalTabCALC_USTID.AsString :=DM1.KunTabUST_NUM.AsString;
     end;
     DM1.KunTab.Close;
end;
//------------------------------------------------------------------------------
procedure TPrintRechForm.RePosTabCalcFields(DataSet: TDataSet);
begin
     case RePosTabSTEUER_CODE.Value of
          0: RePosTabMwstProz.Value :=ReKopfTabMwst_0.Value;
          1: RePosTabMwstProz.Value :=ReKopfTabMwst_1.Value;
          2: RePosTabMwstProz.Value :=ReKopfTabMwst_2.Value;
          3: RePosTabMwstProz.Value :=ReKopfTabMwst_3.Value;
     end;
end;
//------------------------------------------------------------------------------
function TPrintRechForm.KasKopfPipelineGetFieldValue(
  aFieldName: String): Variant;
begin
     if uppercase(aFieldName)='STARTDATUM'
      then Result :=KasBuchStartDatum else
     if uppercase(aFieldName)='ENDDATUM'
      then Result :=KasBuchEndDatum   else
     if uppercase(aFieldName)='STARTSALDO'
      then Result :=KasBuchStartSaldo else
     if uppercase(aFieldName)='ENDSALDO'
      then Result :=KasBuchEndSaldo   else
     if uppercase(aFieldName)='ZEITRAUM'
      then Result :=KasBuchZeitraum   else              
     if uppercase(aFieldName)='LEITWAEHRUNG'
      then Result :=DM1.LeitWaehrung else
     if uppercase(aFieldName)='LE_BUCHUNG'
      then Result :=KasBuchLeBuchungDatum;
end;
//------------------------------------------------------------------------------
procedure TPrintRechForm.KasBuchCalcFields(DataSet: TDataSet);
begin
     Case KasBuchQuelle.Value of
           0 : KasBuchQuelleStr.Value :='Anfangsbestand';
           3 : KasBuchQuelleStr.Value :='Verkauf';
          13 : KasBuchQuelleStr.Value :='VK-Kasse';
           5 : KasBuchQuelleStr.Value :='Einkauf';
          99 : KasBuchQuelleStr.Value :='man. Buchung';
         else KasBuchQuelleStr.Value :='???';
     end;
end;
//------------------------------------------------------------------------------
procedure TPrintRechForm.PDFBtnClick(Sender: TObject);
begin
     {$IFDEF PRO}
     MakePdf (False);
     {$ENDIF}
end;
//------------------------------------------------------------------------------
{$IFDEF PRO}
procedure TPrintRechForm.MakePDF (SendEmail:Boolean);
var Title, UserTitel, T, FileName, Key : String;
    Count : Integer;
    MyEMail : tEMail;
    Res     : Integer;
    AddrID  : Integer;

    procedure ReplacePlaceHolder (DS : tDataset; var Txt : String);
    var I,J,L : Integer;
    begin
         if (not assigned(DS)) or
            (not DS.Active) or
            (DS.RecordCount=0) then exit;

         if DS.Fields.Count>0 then
         begin
           for i :=0 to DS.Fields.Count-1 do
           begin
             if Pos('%'+Uppercase(DS.Fields[i].FieldName)+'%',Txt)>0 then
             begin
                J :=Pos('%'+Uppercase(DS.Fields[i].FieldName+'%'),Txt);
                L :=Length (DS.Fields[i].FieldName)+2;
                delete (Txt,J,L);
                Insert (DS.Fields[i].AsString,Txt,J);
             end;
           end;
         end;
    end;


begin
     case quelle of
          VK_RECH     : Title :='CAO-Faktura Rechnung Nr.:'+
                                Inttostr(ReKopfTabVRENUM.Value);
          VK_AGB      : Title :='CAO-Faktura Angebot Nr.:'+
                                Inttostr(ReKopfTabVRENUM.Value);
          VK_LIEF     : Title :='CAO-Faktura Lieferschein Nr.:'+
                                Inttostr(LsKopfTabVLSNUM.Value);
          EK_BEST     : Title :='CAO-Faktura EK-Bestellung Nr.:'+
                                Inttostr(ReKopfTabVRENUM.Value);
          VK_RECH+100 : Title :='CAO-Faktura Verkaufsjournal';
          VK_KASSE+100: Title :='CAO-Faktura Kassenjournal';
          VK_AGB+100  : Title :='CAO-Faktura Angebotsjournal';
          EK_RECH+100 : Title :='CAO-Faktura Einkaufsjournal';
          VK_LIEF+100 : Title :='CAO-Faktura Lieferscheinjournal';
          KRD_OPOS    : Title :='CAO-Faktura OPOS-Debitoren';
          DEB_OPOS    : Title :='CAO-Faktura OPOS-Kreditoren';
          KAS_BUCH    : Title :='CAO-Faktura Kassenbuch';
          MAHNUNG     : Title :='CAO-Faktura Mahnung';
          TEXTVERARB  : Title :='CAO-Faktura Brief';
     end;

     // NEU, User-Titel (Subjekt) aus der Registery laden...
     UserTitel :='';
     Key :='';

     case Quelle of
       VK_RECH    : Key :='VK_RECH_MAIL_';
       VK_AGB     : Key:='VK_AGB_MAIL_';
       VK_LIEF    : Key :='VK_LIEF_MAIL_';
       EK_BEST    : Key :='EK_BEST_MAIL_';
       MAHNUNG    : Key :='MAHNUNG_MAIL_';
       TEXTVERARB : Key :='TEXTVERARB_MAIL_';
     end;

     if length(Key)>0 then
     begin
        UserTitel :=DM1.ReadString ('MAIN\REPORT',Key+'SUBJECT','');

        if Quelle in [VK_RECH, VK_AGB, EK_BEST] then
        begin
          ReplacePlaceHolder (ReKopfTab,UserTitel);
        end else
        if Quelle = VK_LIEF then
        begin
          ReplacePlaceHolder (LsKopfTab,UserTitel);
        end else
        if Quelle = MAHNUNG then
        begin
           ReplacePlaceHolder (MahnPrintTab,UserTitel);
        end else
        if Quelle = TEXTVERARB then
        begin
           ReplacePlaceHolder (TextTab,UserTitel);
        end;

        if length(UserTitel)>0 then Title :=UserTitel;
     end;

     case quelle of
          VK_RECH     : FileName :='RECH'+Inttostr(ReKopfTabVRENUM.Value);
          VK_AGB      : FileName :='ANGB'+Inttostr(ReKopfTabVRENUM.Value);
          VK_LIEF     : FileName :='LIEF'+Inttostr(LsKopfTabVLSNUM.Value);
          EK_BEST     : FileName :='BEST'+Inttostr(ReKopfTabVRENUM.Value);
          VK_RECH+100 : FileName :='CAO-Faktura-Verkaufsjournal';
          VK_KASSE+100: FileName :='CAO-Faktura-Kassenjournal';
          VK_AGB+100  : FileName :='CAO-Faktura-Angebotsjournal';
          EK_RECH+100 : FileName :='CAO-Faktura-Einkaufsjournal';
          VK_LIEF+100 : FileName :='CAO-Faktura-Lieferscheinjournal';
          EK_BEST+100 : FileName :='CAO-Faktura-EK-Bestelljournal';
          KRD_OPOS    : FileName :='CAO-Faktura-OPOS-Debitoren';
          DEB_OPOS    : FileName :='CAO-Faktura-OPOS-Kreditoren';
          KAS_BUCH    : FileName :='CAO-Faktura-Kassenbuch';
          MAHNUNG     : FileName :='MAHN_KU'+MahnPrintTabkun_num.AsString+'_RE'+MahnPrintTabvrenum.AsString; //+MahnPrintTabKun_Name1.AsString;
          TEXTVERARB  : FileName :='BRIEF_'+InttoStr(TextTabID.AsInteger);
     end;

     PsRBExportMasterControl1.PDF.Author :=DM1.view_user;
     PsRBExportMasterControl1.PDF.Subject :=UserTitel;
     PsRBExportMasterControl1.PDF.Creator :=Application.Title;

     RechReport.DeviceType :='Adobe Acrobat Document';
     RechReport.TextFileName :=DM1.ExportDir+FileName+'.pdf';
     RechReport.AllowPrintToFile :=True;

     RechReport.ShowPrintDialog :=False;
     RechReport.Print;


     if Not SendEmail then MessageDlg ('PDF wurde unter dem Dateinamen:'+#13#10+
                                       DM1.ExportDir+FileName+'.pdf '+
                                       'gespeichert.',mtinformation,[mbok],0);

     if Quelle in [VK_RECH, VK_AGB, VK_LIEF, EK_BEST, TEXTVERARB, MAHNUNG] then
     begin
       try
        // Link an Beleg anh�ngen
        LinkForm.AddLink (Quelle,
                          CurrBelegID,
                          DM1.ExportDir+FileName+'.pdf');

        // Link bei Adresse eintragen
        if (AddrTab.Active) and (AddrTab.RecordCount=1) then
        begin
          if Quelle=Mahnung
           then AddrID :=MahnPrintTabaddr_id.AsInteger
           else
          if Quelle=TEXTVERARB
           then AddrID :=TextTabADDR_ID.AsInteger
           else AddrID :=AddrTab.FieldByName('REC_ID').AsInteger;

          LinkForm.AddLink (MOD_ADRESSEN, AddrID,
                            DM1.ExportDir+FileName+'.pdf');
        end;
       except end;
     end;

     RechReport.ShowPrintDialog :=True;
     RechReport.DeviceType :=dtPrinter;

     if SendEMail then
     begin
        if length(Key)>0 then
        begin
          T :=DM1.ReadLongString ('MAIN\REPORT',Key+'TEXT','');

          if Quelle in [VK_RECH, VK_AGB, EK_BEST] then
          begin
            ReplacePlaceHolder (ReKopfTab,T);
          end else
          if Quelle = VK_LIEF then
          begin
            ReplacePlaceHolder (LsKopfTab,T);
          end else
          if Quelle = MAHNUNG then
          begin
             ReplacePlaceHolder (MahnPrintTab,T);
          end else
          if Quelle = TEXTVERARB then
          begin
             ReplacePlaceHolder (TextTab,T);
          end;
        end else T :='';

        MyEMail :=tEMail.Create (Self);
        try
           if not MyEMail.MapiAvail then
           begin
             messagedlg ('Die MAPI-Schnittstelle ist auf Ihrem System'+#13#10+
                         'nicht installiert, ein '+
                         'EMail-Versand ist nicht m�glich !',mterror,[mbOK],0);
           end
              else
           begin
             MyEMail.OnMapiError   :=MapiError;
             MyEMail.TruncAttFN    :=False; // Dateinamen nicht auf 8.3 reduzieren
             MyEMail.UseDefProfile :=True;
             MyEMail.NewSession    :=False;
             MyEMail.ShowDialog    :=True;
             MyEMail.Attachment.Add (DM1.ExportDir+FileName+'.pdf');
             MyEMail.Recipient.Text :=EMail;//Empf;
             MyEMail.Subject :=Title;
             MyEMail.SetLongText (PChar(T));
             MyEMail.ShowDialog :=True;
             Res :=MyEMail.SendMail;

             if not (Res in [SUCCESS_SUCCESS,MAPI_USER_ABORT,MAPI_E_UNKNOWN_RECIPIENT])
              then messagedlg ('Beim Senden der Nachricht '+
                               'ist ein Fehler aufgetreten !',mterror,[mbOK],0);
           end;
        finally
           MyEMail.Free;
        end;
     end;


     //Beleg als gedruckt markieren
     if (Quelle in [VK_RECH, VK_AGB, EK_BEST]) and
        (ReKopfTabPrint_Flag.AsBoolean=False) then
     begin
       DM1.Uniquery.Close;
       DM1.Uniquery.SQL.Text :='UPDATE JOURNAL SET PRINT_FLAG="Y" '+
                               'WHERE REC_ID='+IntToStr(ReKopfTabRec_ID.AsInteger);
       DM1.Uniquery.ExecSQL;
     end
        else
     if (Quelle in [VK_LIEF]) and
        (LsKopfTabPrint_Flag.AsBoolean=False) then
     begin
       DM1.Uniquery.Close;
       DM1.Uniquery.SQL.Text :='UPDATE JOURNAL SET PRINT_FLAG="Y" '+
                               'WHERE REC_ID='+IntToStr(LsKopfTabRec_ID.AsInteger);
       DM1.Uniquery.ExecSQL;
     end
end;
{$ENDIF}
//------------------------------------------------------------------------------
procedure TPrintRechForm.MapiError(Sender: TObject;
  ErrorCode: Integer);

var EStr : String;
begin
     if ErrorCode<2 then exit;

     case ErrorCode of
       3: EStr :='MAPI_E_LOGIN_FAILURE';
       4: EStr :='MAPI_E_DISK_FULL';
       5: EStr :='MAPI_E_INSUFFICIENT_MEMORY';
       6: EStr :='MAPI_E_ACCESS_DENIED';
       8: EStr :='MAPI_E_TOO_MANY_SESSIONS';
       9: EStr :='MAPI_E_TOO_MANY_FILES';
      10: EStr :='MAPI_E_TOO_MANY_RECIPIENTS';
      11: EStr :='MAPI_E_ATTACHMENT_NOT_FOUND';
      12: EStr :='MAPI_E_ATTACHMENT_OPEN_FAILURE';
      13: EStr :='MAPI_E_ATTACHMENT_WRITE_FAILURE';
      14: EStr :='MAPI_E_UNKNOWN_RECIPIENT';
      15: EStr :='MAPI_E_BAD_RECIPTYPE';
      16: EStr :='MAPI_E_NO_MESSAGES';
      17: EStr :='MAPI_E_INVALID_MESSAGE';
      18: EStr :='MAPI_E_TEXT_TOO_LARGE';
      19: EStr :='MAPI_E_INVALID_SESSION';
      20: EStr :='MAPI_E_TYPE_NOT_SUPPORTED';
      21: EStr :='MAPI_E_AMBIGUOUS_RECIPIENT';
      22: EStr :='MAPI_E_MESSAGE_IN_USE';
      23: EStr :='MAPI_E_NETWORK_FAILURE';
      24: EStr :='MAPI_E_INVALID_EDITFIELDS';
      25: EStr :='MAPI_E_INVALID_RECIPS';
      26: EStr :='MAPI_E_NOT_SUPPORTED';
      else EStr :='';
     end;


     MessageDlg ('Ein MAPI-Fehler ist aufgetreten.'+#13#10+
                 'Fehlercode : '+Inttostr(ErrorCode)+' '+EStr,
                 mterror,[mbok],0);
end;
//------------------------------------------------------------------------------
procedure TPrintRechForm.SendEMailBtnClick(Sender: TObject);
begin
     {$IFDEF PRO}
     if ReKopfTab.Active then Clipboard.AsText :=ReKopfTabCalc_Email.AsString;
     MakePDF (True);

     ModalResult :=mrRetry;
//     Close;

     {$ELSE}
     MessageDlg ('Die EMAIL-Funktion verwendet die PDF-Generierung,'+#13#10+
                 'welche derzeit nur in der "Sponsor-Edition" verf�gbar ist.',
                 mtinformation,[mbok],0);
     {$ENDIF}
end;
//------------------------------------------------------------------------------
procedure TPrintRechForm.LsKopfTabCalcFields(DataSet: TDataSet);
begin
     DM1.KunTab.Close;
     DM1.KunTab.ParamByName ('ID').AsInteger :=LsKopfTabAddr_ID.Value;
     DM1.KunTab.Open;
     if DM1.KunTab.RecordCount=1 then
     begin
       LsKopfTabCALC_EMAIL.AsString :=DM1.KunTabEMail.AsString;

       EMail :=DM1.KunTabEMail.AsString;
       Fax   :=DM1.KunTabFAX.AsString;
     end
      else
     begin
       LsKopfTabCALC_EMAIL.AsString :='';
       EMail :='';
       Fax   :='';
     end;
     if (LsKopfTabKUN_LAND.AsString <>'') and
        (DM1.LandTab.Locate ('ID',LsKopfTabKUN_LAND.AsString,[]))
      then lsKopfTabCALC_LANDLANG.AsString :=DM1.LandTab.FieldByName ('NAME').AsString;
end;
//------------------------------------------------------------------------------
// liefert die Daten f�r die virtuelle Pipline Seriennummern
//------------------------------------------------------------------------------
procedure TPrintRechForm.RePosTabAfterScroll(DataSet: TDataSet);
begin
     TmpSN :='';
     ReSNTab.Close;
     ReSNPipeline.RecordCount :=0;

     if RePosTabSN_FLAG.AsBoolean then
     begin
       ReSNTab.SQL.Text :='select * from ARTIKEL_SERNUM '+
                          'where VK_JOURNALPOS_ID=:REC_ID';
       ReSNTab.ParamByName('REC_ID').AsInteger :=RePosTabRec_ID.AsInteger;
       ReSNTab.Open;

       while not ReSNTab.Eof do
       begin
         if length(TmpSN)>0 then TmpSN :=TmpSN+', ';
         TmpSN :=TmpSN+ReSNTabSERNUMMER.AsString;
         ReSNTab.Next;
       end;
       ReSNTab.Close;

       ReSNPipeline.RecordCount :=Ord(length(TmpSN)>0); // liefert 0 oder 1
       TmpSNPos :=0;
     end;

     if ArtTab.Active
      then ArtTab.Locate ('J_REC_ID',RePosTabRec_ID.AsInteger,[]);
end;
//------------------------------------------------------------------------------
procedure TPrintRechForm.LsPosTabAfterScroll(DataSet: TDataSet);
begin
     TmpSN :='';
     ReSNTab.Close;
     ReSNPipeline.RecordCount :=0;
     if LsPosTabSN_FLAG.AsBoolean then
     begin
       ReSNTab.SQL.Text :='select * from ARTIKEL_SERNUM '+
                          'where LS_JOURNALPOS_ID=:REC_ID';
       ReSNTab.ParamByName('REC_ID').AsInteger :=LSPosTabRec_ID.AsInteger;
       ReSNTab.Open;



       while not ReSNTab.Eof do
       begin
         if length(TmpSN)>0 then TmpSN :=TmpSN+', ';
         TmpSN :=TmpSN+ReSNTabSERNUMMER.AsString;
         ReSNTab.Next;
       end;
       ReSNTab.Close;

       ReSNPipeline.RecordCount :=Ord(length(TmpSN)>0); // liefert 0 oder 1
       TmpSNPos :=0;
     end;

     if ArtTab.Active
      then ArtTab.Locate ('J_REC_ID',LsPosTabRec_ID.AsInteger,[]);
end;
//------------------------------------------------------------------------------
// Virtuelle Pipline f�r Seriennummern START
//------------------------------------------------------------------------------
function TPrintRechForm.ReSNPipelineGetFieldValue(
  aFieldName: String): Variant;
begin
     if aFieldName='SNPipelineppField1' then Result :=TmpSN;
end;
function TPrintRechForm.ReSNPipelineCheckBOF: Boolean;
begin
     Result :=(TmpSNPos=0)or(length(TmpSN)=0);
end;
function TPrintRechForm.ReSNPipelineCheckEOF: Boolean;
begin
     Result :=(TmpSNPos=1)or(length(TmpSN)=0);
end;
function TPrintRechForm.ReSNPipelineGetActive: Boolean;
begin
     Result :=True;
end;
procedure TPrintRechForm.ReSNPipelineGotoFirstRecord(Sender: TObject);
begin
     TmpSNPos:=0;
end;
procedure TPrintRechForm.ReSNPipelineGotoLastRecord(Sender: TObject);
begin
     TmpSNPos:=1;
end;
procedure TPrintRechForm.ReSNPipelineTraverseBy(aIncrement: Integer);
begin
     if TmpSNPos=0 then TmpSNPos:=1;
end;
//------------------------------------------------------------------------------
// Virtuelle Pipline f�r Seriennummern ENDE
//------------------------------------------------------------------------------
procedure TPrintRechForm.BonTabCalcFields(DataSet: TDataSet);
begin
     if not DM1.ZahlartTab.Active then DM1.ZahlartTab.Open;

     if (DM1.ZahlartTab.Locate ('ZAHL_ID',ReKopfTabZahlart.AsInteger,[])) and
        (DM1.ZahlartTabZahl_ID.AsInteger=ReKopfTabZahlart.AsInteger) then
     begin
       BonTabZAHLART_KURZ.AsString :=DM1.ZahlartTabTEXT.AsString;
     end;
end;
//------------------------------------------------------------------------------
procedure TPrintRechForm.BonPosTabCalcFields(DataSet: TDataSet);
begin
     case BonPosTabSTEUER_CODE.Value of
          0: BonPosTabMwstProz.Value :=BonTabMwst_0.Value;
          1: BonPosTabMwstProz.Value :=BonTabMwst_1.Value;
          2: BonPosTabMwstProz.Value :=BonTabMwst_2.Value;
          3: BonPosTabMwstProz.Value :=BonTabMwst_3.Value;
     end;
end;
//------------------------------------------------------------------------------
procedure TPrintRechForm.MahnPrintTabCalcFields(DataSet: TDataSet);
var D : Integer;
begin
     if (MahnPrintTabmahnstufe.AsInteger>0)and
        (MahnPrintTabmahnstufe.AsInteger<6) then
     begin
        D :=Round(Int(MahnPrintTabMAHNDATUM.AsDateTime));
        D :=D+DM1.MahnFrist[MahnPrintTabmahnstufe.AsInteger];
        MahnPrintTabfrist.AsDateTime :=D;
     end;
end;
//------------------------------------------------------------------------------
procedure TPrintRechForm.RechReportCancel(Sender: TObject);
begin
     PrintAbort :=True;
end;
//------------------------------------------------------------------------------

end.


{

ShellExecute(handle,
             'open',
             'mailto:martin@mustermann.de&Subject=Betreff&bcc=test1@mail.de;test2@mail.de&body=Hallo%20!!',
             nil,
             nil,
             SW_SHOW); 











}
