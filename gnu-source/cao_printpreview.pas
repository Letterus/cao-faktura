{

02.11.2003 - Position etc. wird jetzt inden Usereinstellungen gespeichert und
             wiederhergestellt
03.12.2003 - Umstellung auf Reportbuilder 7.03 Enterprise







}

unit cao_printpreview;

{$I CAO32.INC}

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, ppViewr, Buttons, ComCtrls, StdCtrls, JvLookup;

type
  TPrintPreviewForm = class(TForm)
    ppViewer1: TppViewer;
    StatusBar1: TStatusBar;
    Panel1: TPanel;
    ViewAllBtn: TSpeedButton;
    VieWidthBtn: TSpeedButton;
    View100PercentBtn: TSpeedButton;
    FirstPageBtn: TSpeedButton;
    PriorPageBtn: TSpeedButton;
    NextPageBtn: TSpeedButton;
    LastPageBtn: TSpeedButton;
    CloseBtn: TSpeedButton;
    FormularCB: TJvDBLookupCombo;
    Label1: TLabel;
    procedure FormShow(Sender: TObject);
    procedure FirstPageClick(Sender: TObject);
    procedure PriorPageClick(Sender: TObject);
    procedure NextPageClick(Sender: TObject);
    procedure LastPageClick(Sender: TObject);
    procedure ToolButton6Click(Sender: TObject);
    procedure ToolButton7Click(Sender: TObject);
    procedure ToolButton8Click(Sender: TObject);
    procedure CloseBtnClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ppViewer1StatusChange(Sender: TObject);
    procedure FormularCBChange(Sender: TObject);
  private
    { Private-Deklarationen }
    LastViewMode : Integer;
    LastLeft, LastTop, LastWidth, LastHeight : Integer;
    LastFormState : Integer;

  public
    { Public-Deklarationen }
  end;

var PrintPreviewForm: TPrintPreviewForm;

implementation

{$R *.DFM}

uses ppPreviewIcons, ppTypes, cao_dm, CAO_PrintRech;

//------------------------------------------------------------------------------
procedure TPrintPreviewForm.FormCreate(Sender: TObject);
begin
     {$IFDEF COMPILER_D6_UP}
     FirstPageBtn.Glyph.Handle :=TppPreviewIcon.CreateIcon(TppFirstPageIcon);
     PriorPageBtn.Glyph.Handle :=TppPreviewIcon.CreateIcon(TppPriorPageIcon);
     NextPageBtn.Glyph.Handle  :=TppPreviewIcon.CreateIcon(TppNextPageIcon);
     LastPageBtn.Glyph.Handle  :=TppPreviewIcon.CreateIcon(TppLastPageIcon);
     {$ELSE}
     FirstPageBtn.Glyph.Handle :=TppPreviewIcon.CreateIcon(TppFirstIcon);
     PriorPageBtn.Glyph.Handle :=TppPreviewIcon.CreateIcon(TppPriorIcon);
     NextPageBtn.Glyph.Handle  :=TppPreviewIcon.CreateIcon(TppNextIcon);
     LastPageBtn.Glyph.Handle  :=TppPreviewIcon.CreateIcon(TppLastIcon);
     {$ENDIF}
     ViewAllBtn.Glyph.Handle :=TppPreviewIcon.CreateIcon(TppZoomWholePageIcon);
     VieWidthBtn.Glyph.Handle :=TppPreviewIcon.CreateIcon(TppZoomPageWidthIcon);
     View100PercentBtn.Glyph.Handle :=TppPreviewIcon.CreateIcon(TppZoom100PercentIcon);
end;
//------------------------------------------------------------------------------
procedure TPrintPreviewForm.FormShow(Sender: TObject);
begin
     ppViewer1.Report.PrintToDevices;


     LastLeft   :=DM1.ReadIntegerU('DR_VORSCHAU','POS_LINKS',Self.Left);
     LastTop    :=DM1.ReadIntegerU('DR_VORSCHAU','POS_OBEN' ,Self.Top);
     LastWidth  :=DM1.ReadIntegerU('DR_VORSCHAU','BREITE'   ,Self.Width);
     LastHeight :=DM1.ReadIntegerU('DR_VORSCHAU','HOEHE'    ,Self.Height);

     LastViewMode  :=DM1.ReadIntegerU('DR_VORSCHAU','MODE' ,Ord(ppViewer1.ZoomSetting));
     LastFormState :=DM1.ReadIntegerU('DR_VORSCHAU','FENSTERSTATUS' ,Ord(WindowState));


     if LastLeft   <> Self.Left   then Self.Left   :=LastLeft;
     if LastTop    <> Self.Top    then Self.Top    :=LastTop;
     if LastWidth  <> Self.Width  then Self.Width  :=LastWidth;
     if LastHeight <> Self.Height then Self.Height :=LastHeight;

     if ppViewer1.ZoomSetting <> TppZoomSettingType(LastViewMode)
       then ppViewer1.ZoomSetting :=TppZoomSettingType(LastViewMode);
     if (WindowState <> tWindowState(LastFormState))and
        (LastFormState <> Ord(wsMinimized))
      then Windowstate :=tWindowState(LastFormState);
end;
//------------------------------------------------------------------------------
procedure TPrintPreviewForm.FirstPageClick(Sender: TObject);
begin
     ppViewer1.FirstPage;
end;
//------------------------------------------------------------------------------
procedure TPrintPreviewForm.PriorPageClick(Sender: TObject);
begin
     ppViewer1.PriorPage;
end;
//------------------------------------------------------------------------------
procedure TPrintPreviewForm.NextPageClick(Sender: TObject);
begin
     ppViewer1.NextPage;
end;
//------------------------------------------------------------------------------
procedure TPrintPreviewForm.LastPageClick(Sender: TObject);
begin
     ppViewer1.LastPage;
end;
//------------------------------------------------------------------------------
procedure TPrintPreviewForm.ToolButton6Click(Sender: TObject);
begin
     ppViewer1.ZoomSetting :=zsWholePage;
end;
//------------------------------------------------------------------------------
procedure TPrintPreviewForm.ToolButton7Click(Sender: TObject);
begin
     ppViewer1.ZoomSetting :=zsPageWidth;
end;
//------------------------------------------------------------------------------
procedure TPrintPreviewForm.ToolButton8Click(Sender: TObject);
begin
     ppViewer1.ZoomSetting :=zs100Percent;
end;
//------------------------------------------------------------------------------
procedure TPrintPreviewForm.CloseBtnClick(Sender: TObject);
begin
     Close;
end;
//------------------------------------------------------------------------------
procedure TPrintPreviewForm.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
     if Key=VK_UP then
     begin
       key :=0;
       PriorPageClick(Sender);
     end else
     if Key=VK_DOWN then
     begin
       key :=0;
       NextPageClick(Sender);
     end else
     if Key=VK_HOME then
     begin
       key :=0;
       FirstPageClick(Sender);
     end else
     if Key=VK_END then
     begin
       key :=0;
       LastPageClick(Sender);
     end else
     if Key=VK_ESCAPE then
     begin
       key :=0;
       Close;
     end;
end;
//------------------------------------------------------------------------------
procedure TPrintPreviewForm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
     if LastLeft   <> Self.Left   then DM1.WriteIntegerU('DR_VORSCHAU','POS_LINKS',Self.Left);
     if LastTop    <> Self.Top    then DM1.WriteIntegerU('DR_VORSCHAU','POS_OBEN' ,Self.Top);
     if LastWidth  <> Self.Width  then DM1.WriteIntegerU('DR_VORSCHAU','BREITE'   ,Self.Width);
     if LastHeight <> Self.Height then DM1.WriteIntegerU('DR_VORSCHAU','HOEHE'    ,Self.Height);
     if ppViewer1.ZoomSetting <> TppZoomSettingType(LastViewMode)
       then DM1.WriteIntegerU('DR_VORSCHAU','MODE' ,Ord(ppViewer1.ZoomSetting));
     if (Self.WindowState <> tWindowState(LastFormState))and
        (LastFormState <> Ord(wsMinimized))
      then DM1.WriteIntegerU('DR_VORSCHAU','FENSTERSTATUS' ,Ord(Self.WindowState));
end;
//------------------------------------------------------------------------------
procedure TPrintPreviewForm.ppViewer1StatusChange(Sender: TObject);
begin
     StatusBar1.Panels[0].Text :=ppViewer1.Status;
     Application.ProcessMessages;
end;
//------------------------------------------------------------------------------
procedure TPrintPreviewForm.FormularCBChange(Sender: TObject);
begin
     PrintRechForm.RechReport.Template.DatabaseSettings.Name :=FormularCB.DisplayValue;
     try
        ppViewer1.Reset;
        PrintRechForm.RechReport.Template.LoadFromDatabase;
        ppViewer1.Report :=PrintRechForm.RechReport;
        PrintRechForm.RechReport.PrintToDevices;
     except end;

end;

end.
