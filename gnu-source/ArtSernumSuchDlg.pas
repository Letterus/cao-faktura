{******************************************************************************}
{ PROJEKT      : CAO-FAKTURA                                                   }
{ DATEI        : ARTSERNUMSUCHDLG.PAS / DFM                                    }
{ BESCHREIBUNG : Dialog zur Suche nach Seriennummern                           }
{ STAND        : 09.05.2004                                                    }
{ VERSION      : 1.2.5.3                                                       }
{ � 2004 Jan Pokrandt / Jan@JP-Soft.de                                         }
{                                                                              }
{ Diese Unit geh�rt zum Projekt CAO-Faktura und wird unter der                 }
{ GNU General Public License Version 2.0 freigegeben                           }
{                                                                              }
{******************************************************************************}
{                                                                              }
{ This program is free software; you can redistribute it and/or                }
{ modify it under the terms of the GNU General Public License                  }
{ as published by the Free Software Foundation; either version 2               }
{ of the License, or any later version.                                        }
{                                                                              }
{ This program is distributed in the hope that it will be useful,              }
{ but WITHOUT ANY WARRANTY; without even the implied warranty of               }
{ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                }
{ GNU General Public License for more details.                                 }
{                                                                              }
{ You should have received a copy of the GNU General Public License            }
{ along with this program; if not, write to the Free Software                  }
{ Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.  }
{                                                                              }
{    ******* CAO-Faktura comes with ABSOLUTELY NO WARRANTY ***************     }
{                                                                              }
{******************************************************************************}
{                                                                              }
{ Historie :                                                                   }
{ 09.05.2004 - Unit ins CVS gestellt                                           }
{                                                                              }
{ Todo :                                                                       }
{                                                                              }
{******************************************************************************}

unit ArtSernumSuchDlg;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DBCtrls, Grids, DBGrids, JvDBCtrl, CaoDBGrid, Db, StdCtrls, Buttons,
  ComCtrls, ZQuery, ZMySqlQuery, ExtCtrls;

type
  TSerNumSuchForm = class(TForm)
    SNSucheArtTab: TZMySqlQuery;
    Label1: TLabel;
    SNEdi: TEdit;
    SNSucheArtDS: TDataSource;
    PC1: TPageControl;
    ArtikelTS: TTabSheet;
    SerNumTS: TTabSheet;
    SerNumDetailsTS: TTabSheet;
    CloseBtn: TBitBtn;
    HelpBtn: TBitBtn;
    SuchBtn: TBitBtn;
    SNSucheDS: TDataSource;
    SNSucheTab: TZMySqlQuery;
    SNSucheArtTabREC_ID: TIntegerField;
    SNSucheArtTabMATCHCODE: TStringField;
    SNSucheArtTabARTNUM: TStringField;
    SNSucheArtTabKURZNAME: TStringField;
    SNSucheTabSERNUMMER: TStringField;
    SNSucheTabEINK_NUM: TIntegerField;
    SNSucheTabLIEF_NUM: TIntegerField;
    SNSucheTabVERK_NUM: TIntegerField;
    SNSucheTabSNUM_ID: TIntegerField;
    SNSucheTabEK_JOURNAL_ID: TIntegerField;
    SNSucheTabVK_JOURNAL_ID: TIntegerField;
    SNSucheTabLS_JOURNAL_ID: TIntegerField;
    SNSucheTabEK_JOURNALPOS_ID: TIntegerField;
    SNSucheTabVK_JOURNALPOS_ID: TIntegerField;
    SNSucheTabLS_JOURNALPOS_ID: TIntegerField;
    SNSucheTabEK_DATUM: TDateField;
    SNSucheTabLIEFERANT: TStringField;
    SNSucheTabLS_DATUM: TDateField;
    SNSucheTabLS_KUNDE: TStringField;
    SNSucheTabVK_DATUM: TDateField;
    SNSucheTabVK_KUNDE: TStringField;
    CaoDBGrid1: TCaoDBGrid;
    CaoDBGrid2: TCaoDBGrid;
    Label2: TLabel;
    DBText1: TDBText;
    DBText2: TDBText;
    DBText3: TDBText;
    Label3: TLabel;
    Label4: TLabel;
    NotFoundPan: TPanel;
    NotFoundLab: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    DBText4: TDBText;
    DBText5: TDBText;
    Label7: TLabel;
    DBText6: TDBText;
    Bevel1: TBevel;
    Label8: TLabel;
    DBText7: TDBText;
    Bevel2: TBevel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    DBText8: TDBText;
    DBText9: TDBText;
    DBText10: TDBText;
    DBText11: TDBText;
    DBText12: TDBText;
    DBText13: TDBText;
    DBText14: TDBText;
    DBText15: TDBText;
    DBText16: TDBText;
    procedure SNEdiChange(Sender: TObject);
    procedure SuchBtnClick(Sender: TObject);
    procedure SNSucheArtTabAfterScroll(DataSet: TDataSet);
    procedure FormShow(Sender: TObject);
    procedure CaoDBGrid2DblClick(Sender: TObject);
    procedure CaoDBGrid1DblClick(Sender: TObject);
    procedure SNEdiKeyPress(Sender: TObject; var Key: Char);
  private
    { Private-Deklarationen }
  public
    { Public-Deklarationen }
  end;

//var SerNumSuchForm: TSerNumSuchForm;

implementation

{$R *.DFM}

//------------------------------------------------------------------------------
procedure TSerNumSuchForm.FormShow(Sender: TObject);
begin
     NotFoundLab.Caption :='Bitte geben Sie eine Seriennummer ein.';
     NotFoundPan.Visible :=True;
     PC1.Visible         :=False;
end;
//------------------------------------------------------------------------------
procedure TSerNumSuchForm.SNEdiChange(Sender: TObject);
begin
     SuchBtn.Enabled :=length(SNEdi.Text)>0;
     SNSucheArtTab.Close;
     SNSucheTab.Close;

     FormShow(Sender);
end;
//------------------------------------------------------------------------------
procedure TSerNumSuchForm.SNEdiKeyPress(Sender: TObject; var Key: Char);
begin
     if Key=#13 then
     begin
        key :=#0;
        SuchBtnClick(Sender);
     end;
end;
//------------------------------------------------------------------------------
procedure TSerNumSuchForm.SuchBtnClick(Sender: TObject);
var S : String;
begin
     S :='%'+SNEdi.Text+'%';
     SNSucheArtTab.ParamByName('SN').AsString :=S;
     SNSucheArtTab.Open;

     if SNSucheArtTab.RecordCount=1 then
     begin
        if SNSucheTab.RecordCount=1
         then PC1.ActivePage :=SerNumDetailsTS
         else PC1.ActivePage :=SerNumTS;
        ArtikelTS.TabVisible :=False;
        SerNumTS.TabVisible :=SNSucheTab.RecordCount>1;
     end
        else
     begin
        ArtikelTS.Caption :='Artikel ('+IntToStr(SNSucheArtTab.RecordCount)+')';
        ArtikelTS.TabVisible :=SNSucheArtTab.RecordCount>1;
        PC1.ActivePage :=ArtikelTS;
        SerNumTS.TabVisible :=SNSucheTab.RecordCount>0;
     end;

     PC1.Visible         :=SNSucheArtTab.RecordCount>0;
     NotFoundPan.Visible :=SNSucheArtTab.RecordCount=0;

     if SNSucheArtTab.RecordCount=0
      then NotFoundLab.Caption :='Zu Ihrem Suchbegriff'+#13#10+
                                 'wurden keine Seiennummern gefunden.';
end;
//------------------------------------------------------------------------------
procedure TSerNumSuchForm.CaoDBGrid1DblClick(Sender: TObject);
begin
     if SNSucheArtTab.RecordCount>0 then
     begin
      if SNSucheTab.RecordCount=1
       then PC1.ActivePage :=SerNumDetailsTS
       else PC1.ActivePage :=SerNumTS;
     end;
end;
//------------------------------------------------------------------------------
procedure TSerNumSuchForm.SNSucheArtTabAfterScroll(DataSet: TDataSet);
begin
     if SNSucheArtTab.RecordCount=0 then exit;

     SNSucheTab.Close;
     SNSucheTab.ParamByName ('SN').AsString :=SNSucheArtTab.ParamByName ('SN').AsString;
     SNSucheTab.ParamByName ('AID').AsInteger :=SNSucheArtTabREC_ID.AsInteger;
     SNSucheTab.Open;

     SernumTS.Caption :='Seriennummern ('+IntToStr(SNSucheTab.RecordCount)+')';
     SerNumDetailsTS.TabVisible :=SNSucheTab.RecordCount>0;
end;
//------------------------------------------------------------------------------
procedure TSerNumSuchForm.CaoDBGrid2DblClick(Sender: TObject);
begin
     if SNSucheTab.RecordCount>0
      then PC1.ActivePage :=SerNumDetailsTS;
end;
//------------------------------------------------------------------------------




end.
