{******************************************************************************}
{ PROJEKT      : CAO-FAKTURA                                                   }
{ DATEI        : CAO_TEXTEDIT.PAS / DFM                                        }
{ BESCHREIBUNG : RTF-Textverarbeitung                                          }
{ STAND        : 07.05.2004                                                    }
{ VERSION      : 1.2.5.3                                                       }
{ � 2004 Jan Pokrandt / Jan@JP-Soft.de                                         }
{                                                                              }
{ Diese Unit geh�rt zum Projekt CAO-Faktura und wird unter der                 }
{ GNU General Public License Version 2.0 freigegeben                           }
{                                                                              }
{******************************************************************************}
{                                                                              }
{ This program is free software; you can redistribute it and/or                }
{ modify it under the terms of the GNU General Public License                  }
{ as published by the Free Software Foundation; either version 2               }
{ of the License, or any later version.                                        }
{                                                                              }
{ This program is distributed in the hope that it will be useful,              }
{ but WITHOUT ANY WARRANTY; without even the implied warranty of               }
{ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                }
{ GNU General Public License for more details.                                 }
{                                                                              }
{ You should have received a copy of the GNU General Public License            }
{ along with this program; if not, write to the Free Software                  }
{ Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.  }
{                                                                              }
{    ******* CAO-Faktura comes with ABSOLUTELY NO WARRANTY ***************     }
{                                                                              }
{******************************************************************************}
{                                                                              }
{ Historie :                                                                   }
{ 07.05.2004 - Unit ins CVS gestellt                                           }
{                                                                              }
{                                                                              }
{ Todo : Serienbriefe, Serienmails                                             }
{                                                                              }
{******************************************************************************}

unit cao_textedit;

{$I WPINC.INC}
{$I CAO32.INC}

interface

uses
  Winprocs, wintypes, Messages,	SysUtils, Classes, Graphics, Controls, Forms,
  ExtCtrls, StdCtrls, Buttons, Menus, DBCtrls, Clipbrd, Spin, Tabs, Dialogs,
  ExtDlgs, ComCtrls, ToolWin, Wpdbrich, Db, ZQuery, ZMySqlQuery, Grids, DBGrids,
  JvDBCtrl, cao_var_const, JvComponent, JvArrowBtn, JvSpeedButton, CaoDBGrid,
  JvComCtrls,
  {$IFDEF REPORTBUILDER}cao_printrech,{$ENDIF}
  WPTbar, WPPanel, WPDefs, WpWinCtr, WPRich, Wpstat2, WPRuler, WPUtil, WPColSel,
  WPWrtHT, WPReadHT, WPrtfInp, WPrtfTXT, WPOleObj, WPObj, WPEmObj, WpParPrp,
  WpParBrd, WpPagPrp, WPTCtrl, WPSymDlg, WPTabdlg, WPBltDlg, WPTblDlg, WPPrint,
  WPRtfIO, WPrtfPA;

type
   {$IFNDEF WPREPORTER}	// Dummies if WPReporter not loaded
       TWPSuperMerge = class(TObject);
       TWPBand	= class(TObject);
   {$ENDIF}


  TTextEditForm = class(TForm)
    WPParagraphPropDlg1: TWPParagraphPropDlg;
    OpenDialog1: TOpenDialog;
    WPParagraphBorderDlg1: TWPParagraphBorderDlg;
    SaveDialog1: TSaveDialog;
    WPTabDlg1: TWPTabDlg;
    WPSymbolDlg1: TWPSymbolDlg;
    WPTableDlg1: TWPTableDlg;
    WPBulletDlg1: TWPBulletDlg;
    FontDialog1: TFontDialog;
    GraphicPopup: TPopupMenu;
    MovableGraphic1: TMenuItem;
    FixedPopup:	TMenuItem;
    ConvertintoBitmap1:	TMenuItem;
    OpenPictureDialog1:	TOpenPictureDialog;
    MovableGraphicWrapLeft1: TMenuItem;
    Auto1: TMenuItem;
    ConvertToMetafile1:	TMenuItem;
    Transparent1: TMenuItem;
    asCharacter1: TMenuItem;
    Convert1: TMenuItem;
    WPToolsEnviroment1: TWPToolsEnviroment;
    WPToolBar2: TWPToolBar;
    WPToolBar1: TWPToolBar;
    WPToolBar3: TWPToolBar;
    WPValueEdit1: TWPValueEdit;
    MainPanel: TPanel;
    ArtPan: TPanel;
    Label35: TLabel;
    ToolBar1: TToolBar;
    TextDS: TDataSource;
    TextTab: TZMySqlTable;
    TextTabID: TIntegerField;
    TextTabADDR_ID: TIntegerField;
    TextTabBESCHREIBUNG: TStringField;
    TextTabLANG_TEXT: TMemoField;
    PC1: TJvPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    WPRuler1: TWPRuler;
    WPVertRuler1: TWPVertRuler;
    WPRichText1: TDBWPRichText;
    EditBtn: TJvSpeedButton;
    AuswahlBtn: TJvSpeedButton;
    TextGrid: TCaoDBGrid;
    TextTabCalcAdresse: TStringField;
    ToolButton1: TToolButton;
    SetAddrBtn: TToolButton;
    ToolButton3: TToolButton;
    SaveBtn: TToolButton;
    NeuBtn: TToolButton;
    AbortBtn: TToolButton;
    ToolButton7: TToolButton;
    ToolButton8: TToolButton;
    DelBtn: TToolButton;
    MainMenu1: TMainMenu;
    Edit1: TMenuItem;
    Undo1: TMenuItem;
    N2: TMenuItem;
    Cut1: TMenuItem;
    Copy1: TMenuItem;
    Paste1: TMenuItem;
    Delete1: TMenuItem;
    SelectAll1: TMenuItem;
    HideSelection1: TMenuItem;
    N9: TMenuItem;
    Suchen1: TMenuItem;
    Ersetzen1: TMenuItem;
    View1: TMenuItem;
    WordWrap1: TMenuItem;
    N7: TMenuItem;
    Controlzeichenzeigen1: TMenuItem;
    Special1: TMenuItem;
    WindowObjectsasObjects1: TMenuItem;
    ParagraphSymbols1: TMenuItem;
    AbsoluteFontheight1: TMenuItem;
    Gridlines1: TMenuItem;
    DoubleBuffer1: TMenuItem;
    Insert1: TMenuItem;
    Symbol1: TMenuItem;
    InsertFile1: TMenuItem;
    Object1: TMenuItem;
    Graphicfile1: TMenuItem;
    Graphicfile2: TMenuItem;
    Table2: TMenuItem;
    Number1: TMenuItem;
    Page1: TMenuItem;
    NextPage1: TMenuItem;
    PriorPage1: TMenuItem;
    PageCount1: TMenuItem;
    Format1: TMenuItem;
    Font1: TMenuItem;
    Paragraph1: TMenuItem;
    Bullets1: TMenuItem;
    Borders1: TMenuItem;
    N10: TMenuItem;
    Tabstops1: TMenuItem;
    ToolButton2: TToolButton;
    TextTabCHANGE_USER: TStringField;
    N1: TMenuItem;
    SeitenAnsicht1: TMenuItem;
    Drucken1: TMenuItem;
    PrintPopUp: TPopupMenu;
    Seitenansicht2: TMenuItem;
    DBNavigator1: TDBNavigator;
    Panel37: TPanel;
    DruckenBtn: TJvArrowButton;
    N3: TMenuItem;
    StandardSchriftart1: TMenuItem;
    TextTabCHANGE_LAST: TDateTimeField;
    procedure WPValueEdit1Change(Sender: TObject);
    procedure Cut1Click(Sender:	TObject);
    procedure Copy1Click(Sender: TObject);
    procedure Paste1Click(Sender: TObject);
    procedure Delete1Click(Sender: TObject);
    procedure SelectAll1Click(Sender: TObject);
    procedure HideSelection1Click(Sender: TObject);
    procedure Undo1Click(Sender: TObject);
    procedure Paragraph1Click(Sender: TObject);
    procedure Object1Click(Sender: TObject);
    procedure Borders1Click(Sender: TObject);
    procedure DestTextTextObjectCheckProperties(Sender:	TObject;
      var CallAgain: Boolean);
    procedure DestTextUNDOKindChanged(Sender: TObject;
      kind: TWPUndoKind);
    procedure WordWrap1Click(Sender: TObject);
    procedure Gridlines1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure InsertFile1Click(Sender: TObject);
    procedure Tabstops1Click(Sender: TObject);
    procedure Symbol1Click(Sender: TObject);
    procedure Table2Click(Sender: TObject);
    procedure Bullets1Click(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure DestTextKeyPress(Sender: TObject;	var Key: Char);
    procedure Font1Click(Sender: TObject);
    procedure DestTextTextObjectMouseDown(Sender: TWPCustomRtfEdit;
      pobj: PTTextObj; obj: TWPObject; Button: TMouseButton;
      Shift: TShiftState; X, Y:	Integer);
    procedure DestTextTextObjectDestroying(Sender: TObject;
      wpobject:	TWPObjectBase);
    procedure MovableGraphic1Click(Sender: TObject);
    procedure ConvertintoBitmap1Click(Sender: TObject);
    procedure WindowObjectsasObjects1Click(Sender: TObject);
    procedure Graphicfile1Click(Sender:	TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure GetTextAtXY(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure WPRichText1ChangeZooming(Sender: TObject);
    procedure Graphicfile2Click(Sender:	TObject);
    procedure AbsoluteFontheight1Click(Sender: TObject);
    procedure ConvertToMetafile1Click(Sender: TObject);
    function WPRichText1GetPageGapText(Sender: TObject;
      PageNumber: Integer): String;
    procedure Transparent1Click(Sender:	TObject);
    procedure Auto1Click(Sender: TObject);
    procedure MovableGraphicWrapLeft1Click(Sender: TObject);
    procedure asCharacter1Click(Sender:	TObject);
    procedure WPRichText1AfterCompleteWordEvent(Sender:	TObject;
      var lastchar: Char);
    procedure PageCount1Click(Sender: TObject);
    procedure DoubleBuffer1Click(Sender: TObject);
    procedure WPRichText1MailMergeGetText(Sender: TObject;
      const inspname: String; const Contents: TWPMMInsertTextContents);
    procedure WPRichText1XPosChanged(Sender: TObject);
    procedure Suchen1Click(Sender: TObject);
    procedure Ersetzen1Click(Sender: TObject);
    procedure WPRichText1SetGaugeValue(Sender: TObject; Value: Integer);
    procedure WPRichText1EditStateEvent(Sender: TObject; selection_marked,
      clipboard_not_empty: Boolean);
    procedure WPRichText1ChangeModified(Sender: TObject);
    procedure Controlzeichenzeigen1Click(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormDeactivate(Sender: TObject);
    procedure MainPanelResize(Sender: TObject);
    procedure TextTabAfterOpen(DataSet: TDataSet);
    procedure AuswahlBtnClick(Sender: TObject);
    procedure EditBtnClick(Sender: TObject);
    procedure TextTabCalcFields(DataSet: TDataSet);
    procedure TextDSStateChange(Sender: TObject);
    procedure NeuBtnClick(Sender: TObject);
    procedure SetAddrBtnClick(Sender: TObject);
    procedure SaveBtnClick(Sender: TObject);
    procedure AbortBtnClick(Sender: TObject);
    procedure DelBtnClick(Sender: TObject);
    procedure PC1Change(Sender: TObject);
    procedure DruckenBtnClick(Sender: TObject);
    procedure TextTabBeforePost(DataSet: TDataSet);
    procedure SeitenAnsicht1Click(Sender: TObject);
    procedure StandardSchriftart1Click(Sender: TObject);
    procedure AuswahlBtnMouseEnter(Sender: TObject);
    procedure AuswahlBtnMouseLeave(Sender: TObject);
  private
    InResize : Boolean;
    InLoad   : Boolean;
    LastID   : Integer;

    CurrentObject : TWPObject;
    CurrentPTextObj : PTTextObj;

    procedure MoveObjTo(obj : TWPObject; relx,	rely : Integer);

    procedure UpdateStatus;
  public
    OnUpdateStatusBar : TOnUpdateStatusBar;
    procedure AutoSave;
  end;

//var TextEditForm: TTextEditForm;

implementation

uses cao_dm, CAO_MAIN, CAO_Kunde;

{$R *.DFM}

procedure TTextEditForm.WPValueEdit1Change(Sender: TObject);
begin
  if InResize or InLoad then exit;
  WPRichText1.Zooming := WPValueEdit1.Value;
  DM1.WriteIntegerU ('TEXTEDITOR','ZOOM',WPRichText1.Zooming);
end;

procedure TTextEditForm.Cut1Click(Sender: TObject);
begin
  WPRichText1.CutToClipboard;
end;

procedure TTextEditForm.Copy1Click(Sender: TObject);
begin
   WPRichText1.CopyToClipboard;
end;

procedure TTextEditForm.Paste1Click(Sender: TObject);
begin
  WPRichText1.PasteFromClipboard;
end;

procedure TTextEditForm.Delete1Click(Sender: TObject);
begin
   WPRichText1.ClearSelection;
end;

procedure TTextEditForm.SelectAll1Click(Sender: TObject);
begin
  WPRichText1.SelectAll;
end;

procedure TTextEditForm.HideSelection1Click(Sender: TObject);
begin
   WPRichText1.HideSelection;
end;

procedure TTextEditForm.Undo1Click(Sender: TObject);
begin
  WPRichText1.Undo;
end;

procedure TTextEditForm.Paragraph1Click(Sender: TObject);
begin
    WPParagraphPropDlg1.Execute;
end;

procedure TTextEditForm.MoveObjTo(obj : TWPObject; relx,	rely : Integer);
var  P : PTTextObj;
begin
   P :=	WPRichText1.Memo.FirstObj;
   while p<>nil	do
   begin
      if p^.obj	= obj then break;
      p	:= p^.next;
   end;
   if P<>nil then
   begin
    WPRichText1.TextObjects.ConvertType(P,wpotPar);
    P^.relx := relx;
    P^.rely := rely;
    WPRichText1.Memo.Repaint;
  end;
end;

procedure TTextEditForm.Object1Click(Sender: TObject);
var
  obj :	TWPObject;
begin
  OpenDialog1.Filter :=	'Alle Dateien|*.*';
  if OpenDialog1.Execute then
  begin
       { Dont forget to	put the	unit with the object definitions to
	 the uses clause. Basic	objects	are defined in WPEmObj }
       obj := WPLoadObjectFromFile(Self, OpenDialog1.FileName,FALSE);
       if obj=nil then
	   ShowMessage('Kein Objekt gefunden f�r' +	#13 +
			 OpenDialog1.FileName )
       else with obj do
       begin
	 { WriteRTFMode	:= wobBoth;   -	no - we	want on	wptools	format
	   TWPObjectWriteRTFMode = (wobOnlyStandard, wobOnlyRTF, wobBoth); }
	  WriteRTFMode := wobOnlyStandard;
	 { WidthTW  := w;
	  HeightTW := h;  }
	  Font.Style :=	[];
	  ParentColor := FALSE;
	  ParentFont :=	FALSE;
	  WPRichText1.TextObjects.Insert(obj);

	  MoveObjTo(obj,4000,0);
       end;
  end;
end;
//------------------------------------------------------------------------------
procedure TTextEditForm.Borders1Click(Sender: TObject);
begin
   WPParagraphBorderDlg1.Execute;
end;
//------------------------------------------------------------------------------
{ Prints Footer	and Header text	}
procedure TTextEditForm.DestTextTextObjectCheckProperties(
  Sender: TObject; var CallAgain: Boolean);
begin
  if Sender is TWPObject then
  begin
     { keep object loadable by other RTF reader	}
     TWPObject(Sender).WriteRTFMode := wobBoth;
     { defined in WPObj:
       WPObjectWriteRTFMode = (wobOnlyStandard,	wobOnlyRTF, wobBoth); }
  end;
end;
//------------------------------------------------------------------------------
procedure TTextEditForm.DestTextUNDOKindChanged(Sender: TObject;
  kind:	TWPUndoKind);
begin
  Undo1.Caption	:= 'R�ckg�nging: ' + WPUndoString(kind);
  Undo1.Enabled	:= kind	<> wputNone;
end;
//------------------------------------------------------------------------------
procedure TTextEditForm.WordWrap1Click(Sender: TObject);
begin
  WPRichText1.WordWrap := not WordWrap1.Checked;
  WordWrap1.Checked := WPRichText1.WordWrap;
end;
//------------------------------------------------------------------------------
procedure TTextEditForm.Gridlines1Click(Sender: TObject);
begin
  Gridlines1.Checked :=	not Gridlines1.Checked;
  if Gridlines1.Checked	then
     WPRichText1.ViewOptions :=
       WPRichText1.ViewOptions + [wpShowGridlines] else
     WPRichText1.ViewOptions :=
       WPRichText1.ViewOptions - [wpShowGridlines];
end;
//------------------------------------------------------------------------------
procedure TTextEditForm.FormCreate(Sender: TObject);
begin
    InLoad   :=True;
    OnUpdateStatusBar :=nil;

    // Hilfe-Context-ID's
    MainPanel.HelpContext :=8400;
    TabSheet1.HelpContext :=8400;
    TabSheet2.HelpContext :=8450;


    try
      InResize :=False;
      LastID   :=-1;
      //LastFileName :='';
      { Localization }
      WPRichText1.Header.DecimalTabChar := #0; //	use default !!
      { cursor width }
      WPRichText1.Memo.FCursorWidth := 2;

      WPRichText1.FNoKeyInOptimation	:= TRUE; // dont collect characters

      WPRichText1.Memo.FLegalNumberingStyle := TRUE;

      WPRichtext1.AutoZoom :=wpAutoZoomWidth;
    finally
      InLoad :=False;
    end;
end;
//------------------------------------------------------------------------------
procedure TTextEditForm.InsertFile1Click(Sender:TObject);
begin
  WPRichText1.Insert;
end;
//------------------------------------------------------------------------------
procedure TTextEditForm.Tabstops1Click(Sender: TObject);
begin
    WPTabDlg1.Execute;
end;
//------------------------------------------------------------------------------
procedure TTextEditForm.Symbol1Click(Sender: TObject);
begin
    WPSymbolDlg1.Execute;
end;
//------------------------------------------------------------------------------
procedure TTextEditForm.Table2Click(Sender: TObject);
begin
    WPTableDlg1.Execute;
end;
//------------------------------------------------------------------------------
procedure TTextEditForm.Bullets1Click(Sender: TObject);
begin
   WPBulletDlg1.Execute;
end;
//------------------------------------------------------------------------------
procedure TTextEditForm.FormCloseQuery(Sender: TObject;
  var CanClose:	Boolean);
begin
   CanClose := WPRichText1.CanClose;
end;
//------------------------------------------------------------------------------
procedure TTextEditForm.DestTextKeyPress(Sender:TObject; var Key: Char);
var
  r : Char;
begin

  with WPRichText1, CurrAttr do
  begin
    r := #0;
    { #1 = Ctrl	+ A and	so on }
    case Key of
       #2: if afsBold in Style then DeleteStyle([afsBold])
	    else AddStyle([afsBold]);
       #21: if afsUnderline in Style then DeleteStyle([afsUnderline])
	       else AddStyle([afsUnderline]);
    { ... }
	else r := Key;
    end;
    key	:= r;
  end;
end;
//------------------------------------------------------------------------------
procedure TTextEditForm.Font1Click(Sender: TObject);
begin
  WPRichText1.FontSelect;
end;
//------------------------------------------------------------------------------
function TestMultiByte(pch : PChar) : Boolean;
const  LeadByte	: set of Char =	[#$81..#$9F, #$E0..#$FC];
      TrailByte	: set of Char =	[#$40..#$7E, #$80..#$FC];
var p1 : PChar;
begin
  Result := False;
  If (pch <> nil) and (StrLen(pch) > 1)	then
   begin
    p1 := PChar(LongInt(pch) + 1);
    Result := (Char(pch^) in LeadByte) and (Char(p1^) in TrailByte)
   end;
end;
//------------------------------------------------------------------------------
function TestIsMultiByteString(Start, pch : PChar) : Boolean;
begin
    Result := TestIsMultiByteString(Start, pch);
end;
//------------------------------------------------------------------------------
procedure TTextEditForm.DestTextTextObjectMouseDown(
  Sender: TWPCustomRtfEdit; pobj: PTTextObj; obj: TWPObject;
  Button: TMouseButton;	Shift: TShiftState; X, Y: Integer);
var p :	TPoint;
begin
  CurrentObject	  := obj;
  CurrentPTextObj := pobj;
  if ssRight in	Shift then
  begin
     ConvertintoBitmap1.Visible	:= (obj	is TWPOImageObject) and
       ((obj.CurrentExt	= 'WMF') or (obj.CurrentExt = 'EMF'));
     ConvertToMetafile1.Visible	:= (obj	is TWPOOLEBox);
     GetCursorPos(p);
     //	p := WPRichText1.ClientToScreen(Point(x,y));;
     GraphicPopup.Popup(p.x,p.y);
  end;
end;
//------------------------------------------------------------------------------
procedure TTextEditForm.DestTextTextObjectDestroying(Sender: TObject;
  wpobject: TWPObjectBase);
begin
  CurrentObject	  := nil;
  CurrentPTextObj := nil;
end;
//------------------------------------------------------------------------------
procedure TTextEditForm.MovableGraphic1Click(Sender: TObject);
begin
   if CurrentPTextObj<>nil then
   begin
      WPRichText1.TextObjects.ChangePositionMode( CurrentPTextObj, wpotPar, wpwrLeft);
      WPRichText1.Invalidate;
   end;
end;
//------------------------------------------------------------------------------
procedure TTextEditForm.ConvertintoBitmap1Click(Sender: TObject);
var bit	: TBitmap;
begin
   if (CurrentObject<>nil) and (CurrentObject is TWPOImageObject) and
      (TWPOImageObject(CurrentObject).Picture.Metafile<>nil) then
   begin
      bit := TBitmap.Create;
      bit.Width	:= MulDiv( CurrentObject.ContentsWidth,	Screen.PixelsPerInch, 1440);
      bit.Height := MulDiv( CurrentObject.ContentsHeight, Screen.PixelsPerInch,	1440);
      bit.Canvas.Draw(0,0,TWPOImageObject(CurrentObject).Picture.Metafile);
      TWPOImageObject(CurrentObject).Picture.Assign(bit);
      bit.Free;
      WPRichText1.Invalidate;
   end;
end;
//------------------------------------------------------------------------------
procedure TTextEditForm.WindowObjectsasObjects1Click(Sender: TObject);
begin
   WPRichText1.TextObjects.WindowsActive :=
     not WPRichText1.TextObjects.WindowsActive;
end;
//------------------------------------------------------------------------------
procedure TTextEditForm.Graphicfile1Click(Sender: TObject);
begin
   WPRichText1.InsertGraphicAsLinkDialog('Grafik Dateien|*.BMP;*.JPG;*.WMF');
end;
//------------------------------------------------------------------------------
procedure TTextEditForm.Timer1Timer(Sender: TObject);
begin
end;
//------------------------------------------------------------------------------
{ Utility function to find text	at X,y - not used }
procedure TTextEditForm.GetTextAtXY(Sender: TObject; Shift: TShiftState;	X,
  Y: Integer);
var
  par :	PTParagraph;
  lin :	PTLine;
  pa  :	PTAttr;
  pos :	Integer;
  s   :	string;
begin
  with WPRichText1 do
  begin
    x := Memo.ScreenToTwip(x);
    y := Memo.ScreenToTwip(y);

    Memo.Get_Lin_From_Y(par,lin, y-1+Memo.top_offset,x-Memo.left_offset);
    pos	:= Memo.Get_Cp_from_LX(par,lin,x-Memo.left_offset);

    if pos<lin^.plen then
    begin
	pa := lin^.pa;
	if pa<>nil then	 inc(pa,pos);

	{// Now	we can use the characters at pc
	// we can also use the attributes at pa

	// In this case	we want	to detect a hyperlink
	// and show the	URL in this case.
	// it uses the procedures WPGetChar, WPGetAttr and
	// WPIncPos which work similar to the TWPRichText
	// procedures CPChar and CPAttr	and CPMoveNext}
	if afsHyperlink	in pa^.style then
	begin
	   while (WPGetChar(lin,pos)^<>#13)  and
	       (afsHyperlink in	WPGetAttr(lin,pos)^.style) and
	       not (afsHidden in WPGetAttr(lin,pos)^.style) do
	   begin
	      if not WPIncPos(par,lin,pos) then	break;
	   end;

	   if afsHidden	in WPGetAttr(lin,pos)^.style then
	   begin
	      s	:= '';
	      while (WPGetChar(lin,pos)^<>#13)	and
		  (afsHidden in	WPGetAttr(lin,pos)^.style) do
	      begin
		  s := s + WPGetChar(lin,pos)^;
		  if not WPIncPos(par,lin,pos) then break;
	      end;
	      Caption :=s;
	   end;
	end;
    end;
  end;
end;
//------------------------------------------------------------------------------
procedure TTextEditForm.WPRichText1ChangeZooming(Sender:TObject);
begin
  WPValueEdit1.Value :=	WPRichText1.Zooming;
end;
//------------------------------------------------------------------------------
procedure TTextEditForm.Graphicfile2Click(Sender: TObject);
begin
   WPRichText1.InsertGraphicDialog('Grafik Dateien|*.BMP;*.JPG;*.WMF');
end;
//------------------------------------------------------------------------------
procedure TTextEditForm.AbsoluteFontheight1Click(Sender:TObject);
begin
  if wpUseAbsoluteFontHeight in	WPRichText1.ViewOptions	then
    WPRichText1.ViewOptions := WPRichText1.ViewOptions - [wpUseAbsoluteFontHeight]
  else WPRichText1.ViewOptions := WPRichText1.ViewOptions + [wpUseAbsoluteFontHeight];
  WPRichText1.Memo.ReformatAll;
  AbsoluteFontheight1.Checked := wpUseAbsoluteFontHeight in WPRichText1.ViewOptions;
end;
//------------------------------------------------------------------------------
procedure TTextEditForm.ConvertToMetafile1Click(Sender: TObject);
begin
   if (CurrentObject<>nil) and (CurrentObject is TWPOOLEBox) then
   begin
      TWPOOLEBox(CurrentObject).ConvertToMetafile :=
	not TWPOOLEBox(CurrentObject).ConvertToMetafile;
      WPRichText1.Invalidate;
   end;
end;
//------------------------------------------------------------------------------
function TTextEditForm.WPRichText1GetPageGapText(Sender:TObject;
  PageNumber: Integer):	String;
begin
  Result := ' Seite ' + IntToStr(PageNumber);
end;
//------------------------------------------------------------------------------
procedure TTextEditForm.Transparent1Click(Sender: TObject);
begin
   if (CurrentObject<>nil) then
   begin
      CurrentObject.Transparent	:= not CurrentObject.Transparent;
      WPRichText1.Invalidate;
   end;
end;
//------------------------------------------------------------------------------
procedure TTextEditForm.Auto1Click(Sender: TObject);
begin
   if CurrentPTextObj<>nil then
   begin
      WPRichText1.TextObjects.ChangePositionMode( CurrentPTextObj, wpotPar, wpwrAutomatic);
      WPRichText1.Invalidate;
   end;
end;
//------------------------------------------------------------------------------
procedure TTextEditForm.MovableGraphicWrapLeft1Click(Sender: TObject);
begin
   if CurrentPTextObj<>nil then
   begin
      WPRichText1.TextObjects.ChangePositionMode( CurrentPTextObj, wpotPar, wpwrRight);
      WPRichText1.Invalidate;
   end;
end;
//------------------------------------------------------------------------------
procedure TTextEditForm.asCharacter1Click(Sender: TObject);
begin
   if CurrentPTextObj<>nil then
   begin
      WPRichText1.TextObjects.ConvertType( CurrentPTextObj, wpotChar);
      WPRichText1.Invalidate;
   end;
end;
//------------------------------------------------------------------------------
procedure TTextEditForm.WPRichText1MailMergeGetText(Sender: TObject;
  const	inspname: String; const	Contents: TWPMMInsertTextContents);
begin
  Contents.StringValue := '{' +	inspname + '}';
end;
//------------------------------------------------------------------------------
procedure TTextEditForm.WPRichText1AfterCompleteWordEvent(Sender: TObject;
  var lastchar:	Char);
//var s :	string;
begin
 {
 if lastchar='+' then	// Auto Words
 begin
  s := WPRichText1.CPWord;
  if  CompareText(s, 'wp')=0 then
  begin
       WPRichText1.CPWord := 'WPTools'+#13+'The	native Delphi ExpressTexter';
       lastchar	:= #0;
       WPRichText1.Repaint;
  end;
 end else if lastchar in [#32,#9] then // Quick Correct
 begin
    s := WPRichText1.CPWord;
    if s='teh' then WPRichText1.CPWord :='the';
 end; }
end;
//------------------------------------------------------------------------------
procedure TTextEditForm.PageCount1Click(Sender: TObject);
begin
   WPRichText1.InputTextField(TWPTextFieldType((Sender as TMenuItem).Tag));
end;
//------------------------------------------------------------------------------
procedure TTextEditForm.DoubleBuffer1Click(Sender: TObject);
begin
  DoubleBuffer1.Checked	:= not DoubleBuffer1.Checked;
  if DoubleBuffer1.Checked then
	WPRichText1.ViewOptions	:= WPRichText1.ViewOptions + [wpUseDoubleBuffer]
  else	WPRichText1.ViewOptions	:= WPRichText1.ViewOptions - [wpUseDoubleBuffer];

end;
//------------------------------------------------------------------------------
procedure TTextEditForm.WPRichText1XPosChanged(Sender: TObject);
begin
     UpdateStatus;
end;
//------------------------------------------------------------------------------
procedure TTextEditForm.Suchen1Click(Sender: TObject);
begin
     WPRichText1.FindDialog;
end;
//------------------------------------------------------------------------------
procedure TTextEditForm.Ersetzen1Click(Sender: TObject);
begin
     WPRichText1.ReplaceDialog;
end;
//------------------------------------------------------------------------------
procedure TTextEditForm.WPRichText1SetGaugeValue(Sender: TObject;
  Value: Integer);
begin
     //pb1.position :=value;
end;
//------------------------------------------------------------------------------
procedure TTextEditForm.WPRichText1EditStateEvent(Sender: TObject;
  selection_marked, clipboard_not_empty: Boolean);
begin
     Cut1.Enabled :=selection_marked;
     Copy1.Enabled :=selection_marked;
     Paste1.Enabled :=clipboard_not_empty;
end;
//------------------------------------------------------------------------------
procedure TTextEditForm.WPRichText1ChangeModified(Sender: TObject);
begin
     UpdateStatus;
end;
//------------------------------------------------------------------------------
procedure TTextEditForm.AutoSave;
begin
     if WPRichText1.Modified then
     begin
       if WPRichText1.LastFileName<>'' then
       begin
         WPRichText1.Save;
       end;
     end;
end;
//------------------------------------------------------------------------------
procedure TTextEditForm.Controlzeichenzeigen1Click(Sender: TObject);
begin
  Controlzeichenzeigen1.Checked := not Controlzeichenzeigen1.Checked;
  if Controlzeichenzeigen1.Checked
   then WPRichText1.ViewOptions :=
         WPRichText1.ViewOptions + [wpShowCR,wpShowNL,WPShowSPC,wpShowTAB]
   else WPRichText1.ViewOptions :=
         WPRichText1.ViewOptions - [wpShowCR,wpShowNL,WPShowSPC,wpShowTAB];
end;
//------------------------------------------------------------------------------
procedure TTextEditForm.FormActivate(Sender: TObject);
var I : Integer; N : String;
begin
     inLoad :=True;
     try
       if DM1.ReadIntegerU ('TEXTEDITOR','ZOOM',0)>0
        then WPRichtext1.Zooming :=DM1.ReadIntegerU ('TEXTEDITOR','ZOOM',0);

       N :=DM1.ReadStringU  ('TEXTEDITOR','FONT_NAME','');
       if length(N)>0 then WPRichText1.DefaultFont.Name :=N;
       I :=DM1.ReadIntegerU ('TEXTEDITOR','FONT_SIZE',-1);
       if I>0 then WPRichText1.DefaultFont.Size :=I;
       WPRichText1.LayoutMode :=twpLayOutMode(1);

       EditBtn.Align :=alRight;
       AuswahlBtn.Align :=alRight;
       AuswahlBtn.Left :=0;

       AuswahlBtnClick(Sender);

       TextGrid.RowColor1 :=DM1.C2Color;
       TextGrid.EditColor :=DM1.EditColor;

       For i:=0 to PC1.PageCount-1 do PC1.Pages[i].TabVisible :=False;
       PC1.ActivePage :=PC1.Pages[0];
       InLoad :=True;
       TextTab.Open;
       if LastID<>-1 then TextTab.Locate ('ID',LastID,[]);
       //WPRichText1.SetFocus;
       TextGrid.SetFocus;
     finally
        InLoad :=False;
     end;
     PC1Change(Sender);
end;
//------------------------------------------------------------------------------
procedure TTextEditForm.FormDeactivate(Sender: TObject);
begin
     if (TextTab.State in [dsEdit,dsInsert]) then
     begin
        if MessageDlg ('Der aktuelle Brief wurde ver�ndert,'+#13#10+
                       'wollen Sie die �nderung speichern ?',
                       mtconfirmation,[mbYes, mbNo],0) = mryes
         then TextTab.Post
         else TextTab.Cancel;
     end;

     AuswahlBtnClick(Sender);
     TextTab.Close;
end;
//------------------------------------------------------------------------------
procedure TTextEditForm.MainPanelResize(Sender: TObject);
begin
     inResize :=True;
     WPValueEdit1.Value :=WPRichText1.Zooming;
     InResize :=False;
end;
//------------------------------------------------------------------------------
procedure TTextEditForm.TextTabAfterOpen(DataSet: TDataSet);
begin
     WPRichText1XPosChanged(Self);
     if not InLoad then LastID :=TextTabID.Value;
     EditBtn.Enabled :=TextTab.RecordCount >0;
     TextDSStateChange(Self);
     UpdateStatus;
end;
//------------------------------------------------------------------------------
procedure TTextEditForm.AuswahlBtnClick(Sender: TObject);
begin
     AuswahlBtn.Font.Style :=[fsBold];
     EditBtn.Font.Style :=[];
     PC1.ActivePage :=TabSheet1;

     if (TextTab.Active)and(TextTab.State in [dsEdit, dsInsert]) then TextTab.Post;

     WPRichText1.DataSource :=nil;
     PC1Change(Sender);
end;
//------------------------------------------------------------------------------
procedure TTextEditForm.EditBtnClick(Sender: TObject);
begin
     AuswahlBtn.Font.Style :=[];
     EditBtn.Font.Style :=[fsBold];
     PC1.ActivePage :=TabSheet2;
     WPRichText1.DataSource :=TextDS;
     WPRichText1.SetFocus;
     PC1Change(Sender);
end;
//------------------------------------------------------------------------------
procedure TTextEditForm.TextTabCalcFields(DataSet: TDataSet);
var s : string;
begin
     if not DM1.GetLieferant (TextTabAddr_Id.AsInteger, S)
      then s:='???';
     TextTabCalcAdresse.AsString :=S;
end;
//------------------------------------------------------------------------------
procedure TTextEditForm.TextDSStateChange(Sender: TObject);
begin
     SaveBtn.Enabled :=TextTab.State in [dsEdit,dsInsert];
     AbortBtn.Enabled :=SaveBtn.Enabled;
     DelBtn.Enabled :=(TextTab.RecordCount>0)and(SaveBtn.Enabled=False);

     DruckenBtn.Enabled :=(TextTab.RecordCount>0);
     Drucken1.Enabled :=(TextTab.RecordCount>0);
     SeitenAnsicht1.Enabled :=(TextTab.RecordCount>0);

     NeuBtn.Enabled :=(SaveBtn.Enabled=False);
     SetAddrBtn.Enabled :=TextTab.RecordCount>0;
end;
//------------------------------------------------------------------------------
procedure TTextEditForm.NeuBtnClick(Sender: TObject);
begin
     TextTab.Append;
end;
//------------------------------------------------------------------------------
procedure TTextEditForm.SetAddrBtnClick(Sender: TObject);
begin
     if not assigned(MainForm.AddrForm) then
     begin
        MainForm.AddrForm :=TAdressForm.Create (Self{MainForm});
        with MainForm.AddrForm do
        begin
             BorderStyle :=bsSizeable;
             Height      :=500;
             Width       :=620;
             Left        :=MainForm.Left+160;
             Top         :=MainForm.Top+10;
             //Formactivate(Sender);
             UebernahmeBtn.Visible :=True;
             uebernehmen1.Visible :=True;
             Button1.Cancel :=True;
             Invalidate;
        end;
     end;
     with MainForm.AddrForm do
     begin
         ShowModal;
         if Uebern then
         begin
            // Adresse �bernehmen
            if not (TextTab.State in [dsEdit,dsInsert]) then TextTab.Edit;
            TextTabAddr_ID.AsInteger :=KSQueryRec_ID.AsInteger;
            TextTab.Post;
            Uebern :=False;
            FormDeactivate (Sender);
         end;
     end; // with
end;
//------------------------------------------------------------------------------
procedure TTextEditForm.SaveBtnClick(Sender: TObject);
begin
     TextTab.Post;
end;
//------------------------------------------------------------------------------
procedure TTextEditForm.AbortBtnClick(Sender: TObject);
begin
     TextTab.Cancel;
end;
//------------------------------------------------------------------------------
procedure TTextEditForm.DelBtnClick(Sender: TObject);
begin
     if messageDlg ('Wollen Sie diesen Brief wirklich l�schen ?',
                    mtconfirmation,mbyesnocancel,0)=mryes
      then TextTab.Delete;
end;
//------------------------------------------------------------------------------
procedure TTextEditForm.PC1Change(Sender: TObject);
begin
     Edit1.Enabled   :=PC1.ActivePage=TabSheet2;
     Insert1.Enabled :=PC1.ActivePage=TabSheet2;
     View1.Enabled   :=PC1.ActivePage=TabSheet2;
     Format1.Enabled :=PC1.ActivePage=TabSheet2;
     UpdateStatus;
end;
//------------------------------------------------------------------------------
procedure TTextEditForm.UpdateStatus;
var Suchzeit, Datensatz, Sortierung, Erstellt, Geaendert : String;
begin
     if PC1.ActivePage=TabSheet2 then
     begin
       SuchZeit   :='Zeile:'+Inttostr(WPRichText1.GetLineNumber+1);
       Datensatz  :='Spalte:'+Inttostr(WPRichText1.CPColNr+1);
       Sortierung :='Seite:'+Inttostr(WPRichText1.GetPageNumber+1)+' von '+
                             IntToStr(WPRichText1.CountPages);
     end
        else
     begin
       SuchZeit   :='';
       Sortierung :='';
       if TextTab.RecordCount=0
        then Datensatz :='keine'
        else Datensatz :=inttostr (TextTab.RecNo)+' von '+
                         inttostr (TextTab.RecordCount);
     end;

     if assigned (OnUpdateStatusBar)
      then OnUpdateStatusBar (SuchZeit,   //Zeile
                              Datensatz,  // Spalte
                              Sortierung, // Seite
                              Erstellt,   //ge�ndert
                              Geaendert);
end;
//------------------------------------------------------------------------------
procedure TTextEditForm.TextTabBeforePost(DataSet: TDataSet);
begin
     TextTabCHANGE_USER.AsString :=DM1.View_User;
end;
//------------------------------------------------------------------------------
procedure TTextEditForm.DruckenBtnClick(Sender: TObject);
begin
     if (TextTab.Active)and
        (TextTab.State in [dsEdit, dsInsert]) then TextTab.Post;

     {$IFDEF REPORTBUILDER}
     PrintRechForm.ShowBriefDlg (TextTabID.AsInteger,False);
     {$ENDIF}
end;
//------------------------------------------------------------------------------
procedure TTextEditForm.SeitenAnsicht1Click(Sender: TObject);
begin
     if (TextTab.Active)and
        (TextTab.State in [dsEdit, dsInsert]) then TextTab.Post;

     {$IFDEF REPORTBUILDER}
     PrintRechForm.ShowBriefDlg (TextTabID.AsInteger,True);
     {$ENDIF}
end;
//------------------------------------------------------------------------------
procedure TTextEditForm.StandardSchriftart1Click(Sender: TObject);
begin
     with fontdialog1 do
     begin
        font.assign (WPRichText1.DefaultFont);
        if execute then
        begin                                             
          WPRichText1.DefaultFont.Assign (font);

          DM1.WriteStringU  ('TEXTEDITOR','FONT_NAME',Font.Name);
          DM1.WriteIntegerU ('TEXTEDITOR','FONT_SIZE',Font.Size);
        end;

     end;
end;
//------------------------------------------------------------------------------
procedure TTextEditForm.AuswahlBtnMouseEnter(Sender: TObject);
begin
     if (sender is tJvSpeedButton)and
        (tJvSpeedButton(Sender).Enabled) then
     begin
       tJvSpeedButton(Sender).Transparent :=False;
       tJvSpeedButton(Sender).Font.Color :=clBlack;
     end;
end;
//------------------------------------------------------------------------------
procedure TTextEditForm.AuswahlBtnMouseLeave(Sender: TObject);
begin
     if (sender is tJvSpeedButton) then
     begin
       tJvSpeedButton(Sender).Transparent :=True;
       tJvSpeedButton(Sender).Font.Color :=clWhite;
     end;
end;
//------------------------------------------------------------------------------
initialization
  WPAutoLocalize; // new in V3.02
  DefaultDecimalTabChar := ',';
  GlobalLanguage	  := glGerman;
  GlobalValueUnit	  := euCM;
end.
//------------------------------------------------------------------------------
