{

CAO-Faktura f�r Windows Version 1.0
Copyright (C) 2003 Jan Pokrandt / Jan@JP-SOFT.de

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.


******* CAO-Faktura comes with ABSOLUTELY NO WARRANTY ***************


Programm     : CAO-Faktura
Modul        : CAO_HERSTELLER
Stand        : 09.02.2004
Version      : 1.2 RC3A
Beschreibung : Hersteller erstellen, bearbeiten

History :

16.08.2003 - JP Unit erstellt
09.02.2004 - Dialog �berarbeitet, da nicht gespeichert wurde wenn das Name-Feld
             nicht verlassen wurde


TODO:

- Hersteller l�schen

}

unit cao_hersteller;

{$I CAO32.INC}

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, ExtCtrls, StdCtrls, Mask, DBCtrls, ToolWin, Grids, DBGrids,
  JvDBCtrl, Db, ZQuery, ZMySqlQuery, CaoGroupBox, JvCombobox,
  Buttons, VolDBEdit, CaoDBGrid;

type
  THerstellerForm = class(TForm)
    MainPanel: TPanel;
    HerstInfoTab: TZMySqlQuery;
    HerstInfoDS: TDataSource;
    ListeGB: TCaoGroupBox;
    HerstellerGrid: TCaoDBGrid;
    CaoGroupBox7: TCaoGroupBox;
    Label76: TLabel;
    Label77: TLabel;
    HerstNameEdi: TDBEdit;
    HerstInfoUrlEdi: TDBEdit;
    DBNavigator1: TDBNavigator;
    Label1: TLabel;
    HerstellerTab: TZMySqlQuery;
    HerstellerDS: TDataSource;
    HerstellerTabSHOP_ID: TIntegerField;
    HerstellerTabHERSTELLER_ID: TIntegerField;
    HerstellerTabADDR_ID: TIntegerField;
    HerstellerTabHERSTELLER_NAME: TStringField;
    HerstellerTabHERSTELLER_IMAGE: TStringField;
    HerstellerTabLAST_CHANGE: TDateTimeField;
    HerstellerTabSHOP_DATE_ADDED: TDateTimeField;
    HerstellerTabSHOP_DATE_CHANGE: TDateTimeField;
    HerstInfoTabHERSTELLER_URL: TStringField;
    HerstInfoTabURL_CLICKED: TIntegerField;
    HerstInfoTabDATE_LAST_CLICK: TDateTimeField;
    HerstellerTabCHANGE_FLAG: TBooleanField;
    HerstellerTabDEL_FLAG: TBooleanField;
    DBEdit4: TDBEdit;
    Label2: TLabel;
    Label3: TLabel;
    ShopDelCB: TDBCheckBox;
    ProdImageUploadBtn: TSpeedButton;
    SprachLB: TVolgaDBEdit;
    HerstInfoTabSHOP_ID: TIntegerField;
    HerstInfoTabHERSTELLER_ID: TIntegerField;
    HerstInfoTabSPRACHE_ID: TIntegerField;
    AddBtn: TSpeedButton;
    SaveBtn: TSpeedButton;
    AbortBtn: TSpeedButton;
    DBText1: TDBText;
    DBText2: TDBText;
    Bevel1: TBevel;
    Bevel2: TBevel;
    procedure HerstellerTabAfterScroll(DataSet: TDataSet);
    procedure HerstellerTabBeforePost(DataSet: TDataSet);
    procedure HerstellerTabBeforeDelete(DataSet: TDataSet);
    procedure SprachLBChange(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure HerstInfoTabBeforePost(DataSet: TDataSet);
    procedure HerstellerDSDataChange(Sender: TObject; Field: TField);
    procedure AddBtnClick(Sender: TObject);
    procedure SaveBtnClick(Sender: TObject);
    procedure AbortBtnClick(Sender: TObject);
    procedure ProdImageUploadBtnClick(Sender: TObject);
    procedure HerstInfoTabAfterPost(DataSet: TDataSet);
    procedure FormCreate(Sender: TObject);
    procedure HerstellerDSStateChange(Sender: TObject);
  private
    { Private-Deklarationen }
    ShopID   : Integer;
    SprachID : Integer;
  public
    { Public-Deklarationen }
    procedure ShowDialog (NewShopID : Integer);
  end;

//var HerstellerForm: THerstellerForm;

implementation

{$R *.DFM}

uses cao_dm, CAO_ShopImageUpload, cao_progress;

//------------------------------------------------------------------------------
procedure THerstellerForm.FormCreate(Sender: TObject);
begin
     if Screen.PixelsPerInch <> 96 then
     begin
       Self.ScaleBy (96, Screen.PixelsPerInch);
       Refresh;
     end;
end;
//------------------------------------------------------------------------------
procedure THerstellerForm.ShowDialog (NewShopID : Integer);
begin
     ShopID :=NewShopID;
     if ShopID<1 then SHOPID :=1;

     SprachID :=DM1.DefSpracheID;

     //Sprachen in Listbox f�llen
     SprachLB.ComboProps.ComboItems.Clear;
     SprachLB.ComboProps.ComboValues.Clear;

     DM1.SprachTab.First;
     while not DM1.SprachTab.Eof do
     begin
        SprachLB.ComboProps.ComboItems.Add  (DM1.SprachTab.FieldByName ('NAME').AsString);
        SprachLB.ComboProps.ComboValues.Add (DM1.SprachTab.FieldByName ('SPRACH_ID').AsString);

        DM1.SprachTab.Next;
     end;
     SprachLB.CreateDropDownList;

     SprachLB.Value :=SprachID;

     HerstellerTab.Close;
     HerstellerTab.ParamByName ('SID').AsInteger :=ShopID;
     HerstInfoTab.ParamByName ('SID').AsInteger :=ShopID;
     HerstInfoTab.ParamByName ('SPID').AsInteger :=SprachID;

     HerstellerTab.Open;

     HerstellerGrid.RowColor1 :=DM1.C2Color;

     ShowModal;

     HerstInfoTab.Close;
     HerstellerTab.Close;
end;
//------------------------------------------------------------------------------
procedure THerstellerForm.HerstellerTabAfterScroll(DataSet: TDataSet);
begin
     if HerstInfoTab.State in [dsEdit, dsInsert] then HerstInfoTab.Post;

     HerstInfoTab.Close;

     if HerstellerTab.State=dsInsert then
     begin
        HerstInfoTab.ParamByName ('HID').AsInteger  :=-1;
        HerstInfoTab.ParamByName ('SPID').AsInteger :=-1;
        HerstInfoTab.ReadOnly :=True;
     end
        else
     begin
        HerstInfoTab.ParamByName ('HID').AsInteger :=HerstellerTabHERSTELLER_ID.AsInteger;
        HerstInfoTab.ParamByName ('SPID').AsInteger :=SprachID;
        HerstInfoTab.ReadOnly :=False;
     end;

     if HerstellerTab.RecordCount>0 then HerstInfoTab.Open;
     ProgressForm.Stop;
end;
//------------------------------------------------------------------------------
procedure THerstellerForm.HerstellerTabBeforePost(DataSet: TDataSet);
begin
     HerstellerTabSHOP_ID.AsInteger      :=ShopID;
     HerstellerTabLAST_CHANGE.AsDateTime :=Now;
     HerstellerTabCHANGE_FLAG.AsBoolean  :=True;
end;
//------------------------------------------------------------------------------
procedure THerstellerForm.HerstellerTabBeforeDelete(DataSet: TDataSet);
begin
     //

     // Hersteller-Info l�schen
     // in allen Shopartikeln den Hersteller l�schen
end;
//------------------------------------------------------------------------------
procedure THerstellerForm.SprachLBChange(Sender: TObject);
begin
     SprachID :=SprachLB.Value;
     HerstellerTabAfterScroll(nil);
end;
//------------------------------------------------------------------------------
procedure THerstellerForm.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
     if HerstInfoTab.State in [dsEdit, dsInsert] then HerstInfoTab.Post;
     if (HerstellerTab.State in [dsEdit, dsInsert]) then
     begin
       if HerstellerTabHERSTELLER_NAME.AsString=''
        then HerstellerTab.Cancel
        else HerstellerTab.Post;
     end;
end;
//------------------------------------------------------------------------------
procedure THerstellerForm.HerstInfoTabBeforePost(DataSet: TDataSet);
begin
     HerstInfoTabSHOP_ID.AsInteger :=ShopID;
     HerstInfoTabHERSTELLER_ID.AsInteger :=HerstellerTabHERSTELLER_ID.AsInteger;
     HerstInfoTabSPRACHE_ID.AsInteger :=SprachID;
end;
//------------------------------------------------------------------------------
procedure THerstellerForm.HerstellerDSDataChange(Sender: TObject;
  Field: TField);
begin
     SaveBtn.Enabled :=
        ((HerstInfoTab.Active)and(HerstInfoTab.State in [dsEdit, dsInsert]))or
        ((HerstellerTab.Active)and(HerstellerTab.State in [dsEdit, dsInsert]));

     AbortBtn.Enabled :=SaveBtn.Enabled;
     AddBtn.Enabled :=not SaveBtn.Enabled;

     HerstInfoUrlEdi.Enabled :=(HerstInfoTab.Active) and
                               (HerstellerTab.State <> dsInsert);
end;
//------------------------------------------------------------------------------
procedure THerstellerForm.AddBtnClick(Sender: TObject);
begin
     HerstellerTab.Append;
     HerstNameEdi.SetFocus;
end;
//------------------------------------------------------------------------------
procedure THerstellerForm.SaveBtnClick(Sender: TObject);
begin
     if HerstNameEdi.Focused then
     begin
        HerstellerTabHERSTELLER_NAME.AsString :=HerstNameEdi.Text;
     end;

     if length(HerstellerTabHERSTELLER_NAME.asString)=0 then
     begin
        MessageDlg ('Speichern nicht m�glich, '+#13#10+
                    'das Feld Name mu� einen Wert enthalten.',mterror,[mbok],0);
        exit;
     end;

     if (HerstInfoTab.State in [dsEdit, dsInsert]) then HerstInfoTab.Post;
     if (HerstellerTab.State in [dsEdit, dsInsert]) then
     begin
       if HerstellerTabHERSTELLER_NAME.AsString=''
        then HerstellerTab.Cancel
        else HerstellerTab.Post;
     end;
     ProgressForm.Stop;
end;
//------------------------------------------------------------------------------
procedure THerstellerForm.AbortBtnClick(Sender: TObject);
begin
     if HerstInfoTab.State in [dsEdit, dsInsert] then HerstInfoTab.Cancel;
     if HerstellerTab.State in [dsEdit, dsInsert] then HerstellerTab.Cancel;
end;
//------------------------------------------------------------------------------
procedure THerstellerForm.ProdImageUploadBtnClick(Sender: TObject);
var Name : String;
begin
     if HerstellerTabHERSTELLER_NAME.AsString='' then
     begin
        MessageDlg ('Bitte erst Herstellernamen eingeben !',mterror,[mbok],0);
        exit;
     end;

     if FileUpload.HerstellerImage (Name) then
     begin
        if not (HerstellerTab.State in [dsEdit, dsInsert]) then HerstellerTab.Edit;
        HerstellerTabHERSTELLER_IMAGE.AsString :=Name;
        HerstellerTab.Post;
     end;
     ProgressForm.Stop;
end;
//------------------------------------------------------------------------------
procedure THerstellerForm.HerstInfoTabAfterPost(DataSet: TDataSet);
begin
     ProgressForm.Stop;
end;
//------------------------------------------------------------------------------
procedure THerstellerForm.HerstellerDSStateChange(Sender: TObject);
begin
     HerstellerDSDataChange(Sender,nil);
end;
//------------------------------------------------------------------------------
end.
