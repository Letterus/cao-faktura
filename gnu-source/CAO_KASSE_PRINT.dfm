object PrintBonForm: TPrintBonForm
  Left = 398
  Top = 400
  ActiveControl = PrintBtn
  BorderStyle = bsDialog
  Caption = 'Bon drucken'
  ClientHeight = 297
  ClientWidth = 416
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Icon.Data = {
    0000010001002020100000000000E80200001600000028000000200000004000
    0000010004000000000080020000000000000000000000000000000000000000
    0000000080000080000000808000800000008000800080800000C0C0C0008080
    80000000FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF000000
    0000000008000000000000000000000000000000007800000000000000000000
    0000000780000800000000000000000000000070070807800000000000000000
    000078078080F778000000000000000000780070070FFF078000000000000000
    7800F70780FFF0F07800000000000078F0F770780FFF0F0F078000000000000F
    0F708700FFF0F0F0F078000000000000780F70FFFFFF0F0F0F07800000000008
    70F70FFFF0F0F0F0F0F078000000008F0F70FFFF0F0F0F0F0F0F0780000000F0
    770FFFF0F0F0F0F0F0F0F0780000000770FFFF0F0F0F0F0F0F0F0F078000007F
    0FFFFFF0F0F0F0F0F0F0F0F078000000FFFF0FFF0F0F0F0FFF0F0F0F0700000F
    FFF0F0FFF0F0F0FF8FF0F0F0F70000FFFF0FFF0FFF0F0FF788FF0F0FF000000F
    F0F0FFF0FFF0FF87F78FF0FF00000000FF0F0FFF0FFFF878F888FFF000000000
    0FF080F8F0FFF887778FF0000000000000FF080F8F0FFF8788F0000000000000
    000FF080FFF0FFF88F000000000000000000FF080FFF0FFFF000000000000000
    00000FF0F0FFF0FF0000000000000000000000FF0F0F0FF00000000000000000
    0000000FF0F0FFF0000000000000000000000000FF0FFF000000000000000000
    000000000FFFFF0000000000000000000000000000FFF0000000000000000000
    00000000000F000000000000000000000000000000000000000000000000FFF8
    7FFFFFE03FFFFFC01FFFFF000FFFFC0007FFF00003FF800001FF800000FFC000
    007FE000003F8000001F8000000F80000007800000038000000180000001C000
    000180000003C0000007E000000FF000001FF800007FFC0001FFFE0003FFFF00
    07FFFF800FFFFFC00FFFFFE01FFFFFF01FFFFFF83FFFFFFC7FFFFFFEFFFF}
  OldCreateOrder = False
  Position = poMainFormCenter
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 8
    Width = 43
    Height = 13
    Caption = 'Formular:'
  end
  object Label2: TLabel
    Left = 8
    Top = 50
    Width = 60
    Height = 13
    Caption = 'Ausgabeziel:'
  end
  object Label7: TLabel
    Left = 8
    Top = 75
    Width = 43
    Height = 13
    Caption = 'Schacht:'
  end
  object Label8: TLabel
    Left = 8
    Top = 102
    Width = 42
    Height = 13
    Caption = 'Kopieen:'
  end
  object FormularCB: TComboBox
    Left = 71
    Top = 6
    Width = 242
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 0
    OnChange = FormularCBChange
  end
  object ZielCB: TComboBox
    Left = 72
    Top = 46
    Width = 241
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 1
    OnChange = ZielCBChange
  end
  object BinNamCB: TComboBox
    Left = 72
    Top = 72
    Width = 241
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 2
  end
  object AnzCopy: TJvSpinEdit
    Left = 72
    Top = 98
    Width = 73
    Height = 21
    MaxValue = 99
    MinValue = 1
    Value = 1
    TabOrder = 3
    OnChange = AnzCopyChange
  end
  object VorschauBtn: TBitBtn
    Left = 320
    Top = 5
    Width = 89
    Height = 28
    Caption = '&Vorschau'
    TabOrder = 4
    OnClick = VorschauBtnClick
    Glyph.Data = {
      4E010000424D4E01000000000000760000002800000012000000120000000100
      040000000000D800000000000000000000001000000010000000000000000000
      BF0000BF000000BFBF00BF000000BF00BF00BFBF0000C0C0C000808080000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
      DDDDDD000000DDDDDDDDDDDDDDDDDD000000D000000000000DD00D000000D0FF
      FFFFFFFF0D000D000000D0FFFFFFF0000800DD000000D0FFFFFF0877808DDD00
      0000D0FFFFF0877E880DDD000000D0FFFFF07777870DDD000000D0FFFFF07E77
      870DDD000000D0FFFFF08EE7880DDD000000D0FFFFFF087780DDDD000000D0FF
      FFFFF0000DDDDD000000D0FFFFFFFFFF0DDDDD000000D0FFFFFFF0000DDDDD00
      0000D0FFFFFFF070DDDDDD000000D0FFFFFFF00DDDDDDD000000DD00000000DD
      DDDDDD000000DDDDDDDDDDDDDDDDDD000000}
  end
  object PrintBtn: TBitBtn
    Left = 320
    Top = 37
    Width = 89
    Height = 28
    Caption = '&Drucken'
    Default = True
    TabOrder = 5
    OnClick = PrintBtnClick
    Glyph.Data = {
      82060000424D8206000000000000420000002800000028000000140000000100
      1000030000004006000000000000000000000000000000000000007C0000E003
      00001F0000000042004200420042004200420042004200420042004200420042
      0042004200420042004200420042004200420042004200420042004200420042
      0042004200420042004200420042004200420042004200420042004200420042
      0042004200420042004200420042004200420042004200420042004200420042
      00420042FF7FFF7FFF7F0042004200420042004200420042004200420042FF7F
      FF7FFF7F00420042004200000000000000420042004200420042004200420042
      004200420000000000000042004200420042104210421042FF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7F104210421042FF7FFF7F00420000000000000000
      0000000000000000000000000000000000000000000000000000000000420042
      1042104210421042104210421042104210421042104210421042104210421042
      10421042FF7F00420000FF7F1863186318631863186318631863186318631863
      186318631863186318630000004200421042FF7F004200420042004200420042
      0042004200420042004200420042004200421042FF7F00420000FF7F18631863
      1863186318631863186318631863186318631863186318631863000000420042
      1042FF7F00420042004200420042004200420042004200420042004200420042
      00421042FF7F00420000FF7F1863186318631863186318631863186318631863
      1863007C007C186318630000004200421042FF7F004200420042004200420042
      0042004200420042004200420042004200421042FF7F00420000FF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F000000420042
      1042FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7F1042FF7F0042104200000000104210421042104210421042104210421042
      1042104210420000000010420042004210421042104210421042104210421042
      1042104210421042104210421042104210421042004200420042004200000000
      0000000000000000000000000000000000000000000000000042004200420042
      0042004210421042104210421042104210421042104210421042104210421042
      FF7F0042004200420042004200000000FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7F0000000000420042004200420042004210421042FF7F0042FF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7F1042104200420042004200420042004200420000
      FF7F00000000000000000000000000000000FF7F000000420042004200420042
      0042004200421042FF7F1042104210421042104210421042104200421042FF7F
      00420042004200420042004200420000FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7F0000004200420042004200420042004200421042FF7F0042FF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7F1042FF7F00420042004200420042004200420000
      FF7F00000000000000000000000000000000FF7F000000420042004200420042
      0042004200421042FF7F1042104210421042104210421042104200421042FF7F
      00420042004200420042004200420000FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7F0000004200420042004200420042004200421042FF7F0042FF7FFF7F
      0042004200420042004200421042FF7F00420042004200420042004200420000
      FF7F00000000FF7FFF7FFF7FFF7FFF7FFF7FFF7F000000420042004200420042
      0042004200421042FF7F1042104200420042004200420042004200421042FF7F
      00420042004200420042004200420000FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7F0000004200420042004200420042004200421042FF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7F1042FF7F00420042004200420042004200420000
      0000000000000000000000000000000000000000000000420042004200420042
      0042004200421042104210421042104210421042104210421042104210420042
      0042004200420042004200420042004200420042004200420042004200420042
      0042004200420042004200420042004200420042004200420042004200420042
      0042004200420042004200420042004200420042004200420042004200420042
      0042004200420042004200420042004200420042004200420042004200420042
      0042004200420042004200420042004200420042004200420042004200420042
      004200420042}
    NumGlyphs = 2
  end
  object DruEinrBtn: TBitBtn
    Left = 320
    Top = 69
    Width = 89
    Height = 28
    Caption = '&Einrichten'
    TabOrder = 6
    OnClick = DruEinrBtnClick
    Glyph.Data = {
      96090000424D9609000000000000360000002800000028000000140000000100
      1800000000006009000000000000000000000000000000000000008080008080
      0080800080800080800080800080800080800080800080800080800080800080
      8000808000808000808000808000808000808000808000808000808000808000
      8080008080008080008080008080008080008080008080008080008080008080
      0080800080800080800080800080800080800080800080800080800080800080
      8000808000808000808000808000808000808000808000808000808000808000
      8080008080008080008080008080008080008080008080FFFFFFFFFFFFFFFFFF
      008080008080008080008080008080008080008080008080008080008080FFFF
      FFFFFFFFFFFFFF00808000808000808000000000000000000000808000808000
      8080008080008080008080008080008080008080008080000000000000000000
      008080008080008080008080808080808080808080FFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF808080808080808080FFFFFFFF
      FFFF008080000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000080800080
      8080808080808080808080808080808080808080808080808080808080808080
      8080808080808080808080808080808080808080808080FFFFFF008080000000
      FFFFFFC0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
      C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0000000008080008080808080FFFFFF00
      8080008080008080008080008080008080008080008080008080008080008080
      008080008080008080008080808080FFFFFF008080000000FFFFFFC0C0C0C0C0
      C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
      C0C0C0C0C0C0C0C0000000008080008080808080FFFFFF008080008080008080
      0080800080800080800080800080800080800080800080800080800080800080
      80008080808080FFFFFF008080000000FFFFFFC0C0C0C0C0C0C0C0C0C0C0C0C0
      C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C00000FF0000FFC0C0C0C0C0C0
      000000008080008080808080FFFFFF0080800080800080800080800080800080
      80008080008080008080008080008080008080008080008080008080808080FF
      FFFF008080000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000000080800080
      80808080FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF808080FFFFFF008080808080
      0000000000008080808080808080808080808080808080808080808080808080
      8080808080808080808000000000000080808000808000808080808080808080
      8080808080808080808080808080808080808080808080808080808080808080
      8080808080808080808080808080800080800080800080800080800000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000008080008080008080008080008080008080808080808080808080
      8080808080808080808080808080808080808080808080808080808080808080
      80FFFFFF00808000808000808000808000808000000080808080808080808080
      8080808080808080808080808080808080808080808080808080000000008080
      0080800080800080800080800080808080808080808080808080808080808080
      80808080808080808080808080808080808080808080808080FFFFFF00808000
      8080008080008080008080000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000080800080800080800080
      8000808000808080808080808080808080808080808080808080808080808080
      8080808080808080808080808080808080008080008080008080008080008080
      0080800080800080800080800080800080800080800080800080800080800080
      8000808000808000808000808000808000808000808000808000808000808000
      8080008080008080008080008080008080008080008080008080008080008080
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0080800080800080800080800080800080
      8000808000808000808000808000808000808000808000808080808000000000
      0000000000808080008080008080008080008080008080008080008080008080
      0080800080800080800080800080800080800080808080808080808080808080
      80808080FFFFFF00808000808000808000808000808000808000808000808000
      8080008080008080008080008080008080000000FFFF00FFFFFFFFFF00000000
      008080008080008080008080008080008080FFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFF808080008080008080008080808080FFFFFF00
      8080008080008080008080808080000000000000000000000000000000000000
      000000000000000000FFFF00FFFFFF0000000000008080800080800080800080
      8000808000808080808080808080808080808080808080808080808080808080
      8080808080FFFFFF008080808080808080808080008080008080008080008080
      008080000000FFFF00800000FFFF00FFFFFFFFFF00FFFFFFFFFF00FFFFFF8080
      80FFFFFFFFFF0000000000808000808000808000808000808000808000808080
      8080FFFFFF808080FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF808080FFFFFF
      008080808080008080FFFFFFFFFFFF0080800080800080800080808080800000
      00000000000000000000000000000000000000000000000000FFFF00FFFFFF00
      0000000000808080008080008080008080008080008080808080808080808080
      808080808080808080808080808080808080808080008080FFFFFF8080808080
      80808080FFFFFF00808000808000808000808000808000808000808000808000
      8080008080008080008080008080008080000000FFFF00FFFFFFFFFF00000000
      0080800080800080800080800080800080800080800080800080800080800080
      80008080008080008080008080808080FFFFFFFFFFFFFFFFFF808080FFFFFF00
      8080008080008080008080008080008080008080008080008080008080008080
      0080800080800080808080800000000000000000008080800080800080800080
      8000808000808000808000808000808000808000808000808000808000808000
      8080008080808080808080808080808080808080008080008080}
    NumGlyphs = 2
  end
  object CloseBtn: TBitBtn
    Left = 320
    Top = 101
    Width = 89
    Height = 28
    Cancel = True
    Caption = 'S&chlie�en'
    TabOrder = 7
    OnClick = CloseBtnClick
    Glyph.Data = {
      DE010000424DDE01000000000000760000002800000024000000120000000100
      0400000000006801000000000000000000001000000000000000000000000000
      80000080000000808000800000008000800080800000C0C0C000808080000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00388888888877
      F7F787F8888888888333333F00004444400888FFF444448888888888F333FF8F
      000033334D5007FFF4333388888888883338888F0000333345D50FFFF4333333
      338F888F3338F33F000033334D5D0FFFF43333333388788F3338F33F00003333
      45D50FEFE4333333338F878F3338F33F000033334D5D0FFFF43333333388788F
      3338F33F0000333345D50FEFE4333333338F878F3338F33F000033334D5D0FFF
      F43333333388788F3338F33F0000333345D50FEFE4333333338F878F3338F33F
      000033334D5D0EFEF43333333388788F3338F33F0000333345D50FEFE4333333
      338F878F3338F33F000033334D5D0EFEF43333333388788F3338F33F00003333
      4444444444333333338F8F8FFFF8F33F00003333333333333333333333888888
      8888333F00003333330000003333333333333FFFFFF3333F00003333330AAAA0
      333333333333888888F3333F00003333330000003333333333338FFFF8F3333F
      0000}
    NumGlyphs = 2
  end
  object ReportTab: TZMySqlQuery
    Database = DM1.DB1
    Transaction = DM1.Transact1
    CachedUpdates = False
    ShowRecordTypes = [ztModified, ztInserted, ztUnmodified]
    Options = [doHourGlass, doAutoFillDefs, doUseRowId]
    LinkOptions = [loAlwaysResync]
    Constraints = <>
    ExtraOptions = [moStoreResult]
    Macros = <>
    Sql.Strings = (
      'select * from REGISTERY '
      'where MAINKEY= :KEY')
    RequestLive = False
    Left = 231
    Top = 170
    ParamData = <
      item
        DataType = ftString
        Name = 'KEY'
        ParamType = ptInput
      end>
    object ReportTabMAINKEY: TStringField
      FieldName = 'MAINKEY'
      Required = True
      Size = 255
    end
    object ReportTabNAME: TStringField
      FieldName = 'NAME'
      Required = True
      Size = 100
    end
    object ReportTabVAL_BLOB: TMemoField
      FieldName = 'VAL_BLOB'
      BlobType = ftMemo
    end
    object ReportTabVAL_CHAR: TStringField
      FieldName = 'VAL_CHAR'
      Size = 255
    end
    object ReportTabVAL_BIN: TBlobField
      FieldName = 'VAL_BIN'
      BlobType = ftBlob
    end
    object ReportTabVAL_TYP: TIntegerField
      FieldName = 'VAL_TYP'
    end
  end
  object ReportDS: TDataSource
    DataSet = ReportTab
    Left = 295
    Top = 170
  end
  object RepPipeline: TppDBPipeline
    DataSource = ReportDS
    AutoCreateFields = False
    UserName = 'RepPipeline'
    Visible = False
    Left = 232
    Top = 216
  end
  object RechReport: TppReport
    NoDataBehaviors = [ndMessageDialog, ndBlankPage]
    PassSetting = psTwoPass
    PrinterSetup.BinName = 'Default'
    PrinterSetup.DocumentName = 'Report'
    PrinterSetup.PaperName = 'A4'
    PrinterSetup.PrinterName = 'Default'
    PrinterSetup.mmMarginBottom = 6350
    PrinterSetup.mmMarginLeft = 6350
    PrinterSetup.mmMarginRight = 6350
    PrinterSetup.mmMarginTop = 6350
    PrinterSetup.mmPaperHeight = 297000
    PrinterSetup.mmPaperWidth = 210000
    PrinterSetup.PaperSize = 9
    Template.DatabaseSettings.DataPipeline = RepPipeline
    Template.DatabaseSettings.NameField = 'NAME'
    Template.DatabaseSettings.TemplateField = 'VAL_BLOB'
    Template.SaveTo = stDatabase
    Template.Format = ftASCII
    Units = utMillimeters
    AllowPrintToFile = True
    DeviceType = 'Printer'
    Icon.Data = {
      0000010001002020040000000000E80200001600000028000000200000004000
      0000010004000000000000020000000000000000000000000000000000000000
      000000008000008000000080800080000000800080008080000080808000C0C0
      C0000000FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF000000
      0000000000000000000000040000000000000000000000000000004EFC000000
      0000000000000000000004EFEC000000000000000000000000004EFECC000000
      00000000000000000004EFECC00000000000000000000000004EFECC00000000
      000000000000000004EFECC00000000000000000000000004EFECC0000000000
      0000000000000004EFECC000000000000000000000000078FECC000000000000
      000000000000078F8CC000000000000000778777777007F87700000000000000
      078888000000000770000000000000007888006FEFEFE0000000000000000007
      88806EFEFEFEFE60000000000000007FF806EFEFEFEFEFE6000000000000007F
      F06EFEFFFEFEFEF600000000000007FF06EFEFFFEFEFEFE680000000000007FF
      06FEFFFEFEFEFEF680000000000007F06FEFFFFFEFEFEFE680000000000007F0
      6EFFFFFEFEFEFEF680000000000007F06FEFFFEFEFEFEFE68000000000000780
      6EFFFFFEFEFEFE6F80000000000007806FEFFFEFEFEFEF6F8000000000000070
      6EFEFFFEFEFEF6FF00000000000000706FEFEFEFEFEF6FFF0000000000000007
      06FEFEFEFEF688F000000000000000000666EFEFE66888000000000000000000
      0077666668888000000000000000000000007777778000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FFFF
      FFE3FFFFFFC1FFFFFF81FFFFFF01FFFFFE03FFFFFC07FFFFF80FFFFFF01FFFFF
      E03FFFFFC07FFF0180FFFC0001FFF80003FFF00007FFE00007FFC00007FFC000
      07FF800003FF800003FF800003FF800003FF800003FF800003FF800003FFC000
      07FFC00007FFE0000FFFF0001FFFF8003FFFFC007FFFFF01FFFFFFFFFFFF}
    Language = lgGerman
    OnPreviewFormCreate = RechReportPreviewFormCreate
    Left = 296
    Top = 216
    Version = '6.03'
    mmColumnWidth = 0
    object ppDetailBand1: TppDetailBand
      PrintHeight = phDynamic
      mmBottomOffset = 0
      mmHeight = 3704
      mmPrintPosition = 0
    end
  end
  object FirmaTabPipeline: TppDBPipeline
    DataSource = DM1.FirmaDS
    OpenDataSource = False
    RangeEnd = reCurrentRecord
    RangeBegin = rbCurrentRecord
    UserName = 'Firmendaten'
    Left = 352
    Top = 168
    object FirmaTabPipelineppField1: TppField
      FieldAlias = 'ANREDE'
      FieldName = 'ANREDE'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 0
      Searchable = False
      Sortable = False
    end
    object FirmaTabPipelineppField2: TppField
      FieldAlias = 'NAME1'
      FieldName = 'NAME1'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 1
      Searchable = False
      Sortable = False
    end
    object FirmaTabPipelineppField3: TppField
      FieldAlias = 'NAME2'
      FieldName = 'NAME2'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 2
      Searchable = False
      Sortable = False
    end
    object FirmaTabPipelineppField4: TppField
      FieldAlias = 'NAME3'
      FieldName = 'NAME3'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 3
      Searchable = False
      Sortable = False
    end
    object FirmaTabPipelineppField5: TppField
      FieldAlias = 'STRASSE'
      FieldName = 'STRASSE'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 4
      Searchable = False
      Sortable = False
    end
    object FirmaTabPipelineppField6: TppField
      FieldAlias = 'LAND'
      FieldName = 'LAND'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 5
      Searchable = False
      Sortable = False
    end
    object FirmaTabPipelineppField7: TppField
      FieldAlias = 'PLZ'
      FieldName = 'PLZ'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 6
      Searchable = False
      Sortable = False
    end
    object FirmaTabPipelineppField8: TppField
      FieldAlias = 'ORT'
      FieldName = 'ORT'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 7
      Searchable = False
      Sortable = False
    end
    object FirmaTabPipelineppField9: TppField
      FieldAlias = 'VORWAHL'
      FieldName = 'VORWAHL'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 8
      Searchable = False
      Sortable = False
    end
    object FirmaTabPipelineppField10: TppField
      FieldAlias = 'TELEFON1'
      FieldName = 'TELEFON1'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 9
      Searchable = False
      Sortable = False
    end
    object FirmaTabPipelineppField11: TppField
      FieldAlias = 'TELEFON2'
      FieldName = 'TELEFON2'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 10
      Searchable = False
      Sortable = False
    end
    object FirmaTabPipelineppField12: TppField
      FieldAlias = 'MOBILFUNK'
      FieldName = 'MOBILFUNK'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 11
      Searchable = False
      Sortable = False
    end
    object FirmaTabPipelineppField13: TppField
      FieldAlias = 'FAX'
      FieldName = 'FAX'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 12
      Searchable = False
      Sortable = False
    end
    object FirmaTabPipelineppField14: TppField
      FieldAlias = 'EMAIL'
      FieldName = 'EMAIL'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 13
      Searchable = False
      Sortable = False
    end
    object FirmaTabPipelineppField15: TppField
      FieldAlias = 'WEBSEITE'
      FieldName = 'WEBSEITE'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 14
      Searchable = False
      Sortable = False
    end
    object FirmaTabPipelineppField16: TppField
      FieldAlias = 'BANK1_BLZ'
      FieldName = 'BANK1_BLZ'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 15
      Searchable = False
      Sortable = False
    end
    object FirmaTabPipelineppField17: TppField
      FieldAlias = 'BANK1_KONTONR'
      FieldName = 'BANK1_KONTONR'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 16
      Searchable = False
      Sortable = False
    end
    object FirmaTabPipelineppField18: TppField
      FieldAlias = 'BANK1_NAME'
      FieldName = 'BANK1_NAME'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 17
      Searchable = False
      Sortable = False
    end
    object FirmaTabPipelineppField19: TppField
      FieldAlias = 'BANK1_IBAN'
      FieldName = 'BANK1_IBAN'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 18
      Searchable = False
      Sortable = False
    end
    object FirmaTabPipelineppField20: TppField
      FieldAlias = 'BANK1_SWIFT'
      FieldName = 'BANK1_SWIFT'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 19
      Searchable = False
      Sortable = False
    end
    object FirmaTabPipelineppField21: TppField
      FieldAlias = 'BANK2_BLZ'
      FieldName = 'BANK2_BLZ'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 20
      Searchable = False
      Sortable = False
    end
    object FirmaTabPipelineppField22: TppField
      FieldAlias = 'BANK2_KONTONR'
      FieldName = 'BANK2_KONTONR'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 21
      Searchable = False
      Sortable = False
    end
    object FirmaTabPipelineppField23: TppField
      FieldAlias = 'BANK2_NAME'
      FieldName = 'BANK2_NAME'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 22
      Searchable = False
      Sortable = False
    end
    object FirmaTabPipelineppField24: TppField
      FieldAlias = 'BANK2_IBAN'
      FieldName = 'BANK2_IBAN'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 23
      Searchable = False
      Sortable = False
    end
    object FirmaTabPipelineppField25: TppField
      FieldAlias = 'BANK2_SWIFT'
      FieldName = 'BANK2_SWIFT'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 24
      Searchable = False
      Sortable = False
    end
    object FirmaTabPipelineppField26: TppField
      FieldAlias = 'KOPFTEXT'
      FieldName = 'KOPFTEXT'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 25
      Searchable = False
      Sortable = False
    end
    object FirmaTabPipelineppField27: TppField
      FieldAlias = 'FUSSTEXT'
      FieldName = 'FUSSTEXT'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 26
      Searchable = False
      Sortable = False
    end
    object FirmaTabPipelineppField28: TppField
      FieldAlias = 'ABSENDER'
      FieldName = 'ABSENDER'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 27
      Searchable = False
      Sortable = False
    end
    object FirmaTabPipelineppField29: TppField
      FieldAlias = 'STEUERNUMMER'
      FieldName = 'STEUERNUMMER'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 28
      Searchable = False
      Sortable = False
    end
    object FirmaTabPipelineppField30: TppField
      FieldAlias = 'UST_ID'
      FieldName = 'UST_ID'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 29
      Searchable = False
      Sortable = False
    end
    object FirmaTabPipelineppField31: TppField
      FieldAlias = 'IMAGE1'
      FieldName = 'IMAGE1'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 30
      Searchable = False
      Sortable = False
    end
    object FirmaTabPipelineppField32: TppField
      FieldAlias = 'IMAGE2'
      FieldName = 'IMAGE2'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 31
      Searchable = False
      Sortable = False
    end
    object FirmaTabPipelineppField33: TppField
      FieldAlias = 'IMAGE3'
      FieldName = 'IMAGE3'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 32
      Searchable = False
      Sortable = False
    end
    object FirmaTabPipelineppField34: TppField
      FieldAlias = 'USER_AKT'
      FieldName = 'USER_AKT'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 33
      Searchable = False
      Sortable = False
    end
    object FirmaTabPipelineppField35: TppField
      FieldAlias = 'LEITWAEHRUNG'
      FieldName = 'LEITWAEHRUNG'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 34
      Searchable = False
      Sortable = False
    end
    object FirmaTabPipelineppField36: TppField
      FieldAlias = 'MANDANT_NAME'
      FieldName = 'MANDANT_NAME'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 35
      Searchable = False
      Sortable = False
    end
  end
  object BonTab: TZMySqlQuery
    Database = DM1.DB1
    Transaction = DM1.Transact1
    CachedUpdates = False
    ShowRecordTypes = [ztModified, ztInserted, ztUnmodified]
    Options = [doHourGlass, doAutoFillDefs]
    LinkOptions = [loAlwaysResync]
    Constraints = <>
    OnCalcFields = BonTabCalcFields
    ExtraOptions = [moStoreResult]
    Macros = <>
    Sql.Strings = (
      'select '
      'REC_ID,'
      'VRENUM as BELEGNUMMER, '
      'RDATUM as BELEGDATUM, '
      'ZAHLART, '
      'MWST_0, MWST_1, MWST_2, MWST_3, '
      'NSUMME, MSUMME_0, MSUMME_1, MSUMME_2, MSUMME_3, BSUMME,'
      'WAEHRUNG, '
      'ERST_NAME as BEDIENER'
      'from JOURNAL'
      'where '
      'VRENUM=:VRENUM'
      'and QUELLE=:QUELLE'
      'and QUELLE_SUB=:QUELLE_SUB')
    RequestLive = False
    Left = 16
    Top = 168
    ParamData = <
      item
        DataType = ftInterface
        Name = 'VRENUM'
        ParamType = ptInput
        Value = '220014'
      end
      item
        DataType = ftInteger
        Name = 'QUELLE'
        ParamType = ptInput
        Value = '3'
      end
      item
        DataType = ftInteger
        Name = 'QUELLE_SUB'
        ParamType = ptInput
      end>
    object BonTabBELEGNUMMER: TIntegerField
      FieldName = 'BELEGNUMMER'
    end
    object BonTabBELEGDATUM: TDateField
      FieldName = 'BELEGDATUM'
    end
    object BonTabBEDIENER: TStringField
      FieldName = 'BEDIENER'
    end
    object BonTabMWST_0: TFloatField
      FieldName = 'MWST_0'
      Required = True
    end
    object BonTabMWST_1: TFloatField
      FieldName = 'MWST_1'
      Required = True
    end
    object BonTabMWST_2: TFloatField
      FieldName = 'MWST_2'
      Required = True
    end
    object BonTabMWST_3: TFloatField
      FieldName = 'MWST_3'
      Required = True
    end
    object BonTabNSUMME: TFloatField
      FieldName = 'NSUMME'
      Required = True
    end
    object BonTabMSUMME_0: TFloatField
      FieldName = 'MSUMME_0'
      Required = True
    end
    object BonTabMSUMME_1: TFloatField
      FieldName = 'MSUMME_1'
      Required = True
    end
    object BonTabMSUMME_2: TFloatField
      FieldName = 'MSUMME_2'
      Required = True
    end
    object BonTabMSUMME_3: TFloatField
      FieldName = 'MSUMME_3'
      Required = True
    end
    object BonTabBSUMME: TFloatField
      FieldName = 'BSUMME'
      Required = True
    end
    object BonTabWAEHRUNG: TStringField
      FieldName = 'WAEHRUNG'
      Required = True
      Size = 5
    end
    object BonTabZAHLART: TIntegerField
      DisplayLabel = 'ZAHLART_ID'
      FieldName = 'ZAHLART'
      Required = True
    end
    object BonTabZAHLART_KURZ: TStringField
      FieldKind = fkLookup
      FieldName = 'ZAHLART_KURZ'
      LookupDataSet = DM1.ZahlartTab
      LookupKeyFields = 'ZAHL_ID'
      LookupResultField = 'LANGBEZ'
      KeyFields = 'ZAHLART'
      Size = 100
      Lookup = True
    end
    object BonTabREC_ID: TIntegerField
      FieldName = 'REC_ID'
      Visible = False
    end
  end
  object BonDS: TDataSource
    DataSet = BonTab
    Left = 80
    Top = 168
  end
  object BonPipeline: TppDBPipeline
    DataSource = BonDS
    OpenDataSource = False
    UserName = 'BonKopf'
    Visible = False
    Left = 152
    Top = 168
    object BonPipelineppField1: TppField
      FieldAlias = 'BELEGNUMMER'
      FieldName = 'BELEGNUMMER'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 0
      Searchable = False
      Sortable = False
    end
    object BonPipelineppField2: TppField
      FieldAlias = 'BELEGDATUM'
      FieldName = 'BELEGDATUM'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 1
      Searchable = False
      Sortable = False
    end
    object BonPipelineppField3: TppField
      FieldAlias = 'BEDIENER'
      FieldName = 'BEDIENER'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 2
      Searchable = False
      Sortable = False
    end
    object BonPipelineppField4: TppField
      FieldAlias = 'MWST_0'
      FieldName = 'MWST_0'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 3
      Searchable = False
      Sortable = False
    end
    object BonPipelineppField5: TppField
      FieldAlias = 'MWST_1'
      FieldName = 'MWST_1'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 4
      Searchable = False
      Sortable = False
    end
    object BonPipelineppField6: TppField
      FieldAlias = 'MWST_2'
      FieldName = 'MWST_2'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 5
      Searchable = False
      Sortable = False
    end
    object BonPipelineppField7: TppField
      FieldAlias = 'MWST_3'
      FieldName = 'MWST_3'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 6
      Searchable = False
      Sortable = False
    end
    object BonPipelineppField8: TppField
      FieldAlias = 'NSUMME'
      FieldName = 'NSUMME'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 7
      Searchable = False
      Sortable = False
    end
    object BonPipelineppField9: TppField
      FieldAlias = 'MSUMME_0'
      FieldName = 'MSUMME_0'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 8
      Searchable = False
      Sortable = False
    end
    object BonPipelineppField10: TppField
      FieldAlias = 'MSUMME_1'
      FieldName = 'MSUMME_1'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 9
      Searchable = False
      Sortable = False
    end
    object BonPipelineppField11: TppField
      FieldAlias = 'MSUMME_2'
      FieldName = 'MSUMME_2'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 10
      Searchable = False
      Sortable = False
    end
    object BonPipelineppField12: TppField
      FieldAlias = 'MSUMME_3'
      FieldName = 'MSUMME_3'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 11
      Searchable = False
      Sortable = False
    end
    object BonPipelineppField13: TppField
      FieldAlias = 'BSUMME'
      FieldName = 'BSUMME'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 12
      Searchable = False
      Sortable = False
    end
    object BonPipelineppField14: TppField
      FieldAlias = 'WAEHRUNG'
      FieldName = 'WAEHRUNG'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 13
      Searchable = False
      Sortable = False
    end
    object BonPipelineppField15: TppField
      FieldAlias = 'ZAHLART'
      FieldName = 'ZAHLART'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 14
      Searchable = False
      Sortable = False
    end
    object BonPipelineppField16: TppField
      FieldAlias = 'ZAHLART_KURZ'
      FieldName = 'ZAHLART_KURZ'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 15
      Searchable = False
      Sortable = False
    end
  end
  object BonPosPipeline: TppDBPipeline
    DataSource = BonPosDS
    OpenDataSource = False
    SkipWhenNoRecords = False
    UserName = 'BonPositionen'
    Visible = False
    Left = 152
    Top = 216
    object BonPosPipelineppField1: TppField
      FieldAlias = 'MATCHCODE'
      FieldName = 'MATCHCODE'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 0
      Searchable = False
      Sortable = False
    end
    object BonPosPipelineppField2: TppField
      FieldAlias = 'ARTNUM'
      FieldName = 'ARTNUM'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 1
      Searchable = False
      Sortable = False
    end
    object BonPosPipelineppField3: TppField
      FieldAlias = 'BARCODE'
      FieldName = 'BARCODE'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 2
      Searchable = False
      Sortable = False
    end
    object BonPosPipelineppField4: TppField
      FieldAlias = 'LAENGE'
      FieldName = 'LAENGE'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 3
      Searchable = False
      Sortable = False
    end
    object BonPosPipelineppField5: TppField
      FieldAlias = 'GROESSE'
      FieldName = 'GROESSE'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 4
      Searchable = False
      Sortable = False
    end
    object BonPosPipelineppField6: TppField
      FieldAlias = 'DIMENSION'
      FieldName = 'DIMENSION'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 5
      Searchable = False
      Sortable = False
    end
    object BonPosPipelineppField7: TppField
      FieldAlias = 'GEWICHT'
      FieldName = 'GEWICHT'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 6
      Searchable = False
      Sortable = False
    end
    object BonPosPipelineppField8: TppField
      FieldAlias = 'BEZEICHNUNG'
      FieldName = 'BEZEICHNUNG'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 7
      Searchable = False
      Sortable = False
    end
    object BonPosPipelineppField9: TppField
      FieldAlias = 'ME_EINHEIT'
      FieldName = 'ME_EINHEIT'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 8
      Searchable = False
      Sortable = False
    end
    object BonPosPipelineppField10: TppField
      FieldAlias = 'MENGE'
      FieldName = 'MENGE'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 9
      Searchable = False
      Sortable = False
    end
    object BonPosPipelineppField11: TppField
      FieldAlias = 'PR_EINHEIT'
      FieldName = 'PR_EINHEIT'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 10
      Searchable = False
      Sortable = False
    end
    object BonPosPipelineppField12: TppField
      FieldAlias = 'EPREIS'
      FieldName = 'EPREIS'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 11
      Searchable = False
      Sortable = False
    end
    object BonPosPipelineppField13: TppField
      FieldAlias = 'EPREIS_B'
      FieldName = 'EPREIS_B'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 12
      Searchable = False
      Sortable = False
    end
    object BonPosPipelineppField14: TppField
      FieldAlias = 'RABATT'
      FieldName = 'RABATT'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 13
      Searchable = False
      Sortable = False
    end
    object BonPosPipelineppField15: TppField
      FieldAlias = 'RABATT2'
      FieldName = 'RABATT2'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 14
      Searchable = False
      Sortable = False
    end
    object BonPosPipelineppField16: TppField
      FieldAlias = 'RABATT3'
      FieldName = 'RABATT3'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 15
      Searchable = False
      Sortable = False
    end
    object BonPosPipelineppField17: TppField
      FieldAlias = 'ERPREIS'
      FieldName = 'ERPREIS'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 16
      Searchable = False
      Sortable = False
    end
    object BonPosPipelineppField18: TppField
      FieldAlias = 'ERPREIS_B'
      FieldName = 'ERPREIS_B'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 17
      Searchable = False
      Sortable = False
    end
    object BonPosPipelineppField19: TppField
      FieldAlias = 'GPREIS'
      FieldName = 'GPREIS'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 18
      Searchable = False
      Sortable = False
    end
    object BonPosPipelineppField20: TppField
      FieldAlias = 'GPREIS_B'
      FieldName = 'GPREIS_B'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 19
      Searchable = False
      Sortable = False
    end
    object BonPosPipelineppField21: TppField
      FieldAlias = 'STEUER_CODE'
      FieldName = 'STEUER_CODE'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 20
      Searchable = False
      Sortable = False
    end
    object BonPosPipelineppField22: TppField
      FieldAlias = 'MWST_BETRAG'
      FieldName = 'MWST_BETRAG'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 21
      Searchable = False
      Sortable = False
    end
  end
  object BonPosDS: TDataSource
    DataSet = BonPosTab
    Left = 80
    Top = 216
  end
  object BonPosTab: TZMySqlQuery
    Database = DM1.DB1
    Transaction = DM1.Transact1
    CachedUpdates = True
    ShowRecordTypes = [ztModified, ztInserted, ztUnmodified]
    Options = [doHourGlass, doAutoFillDefs]
    LinkOptions = [loAlwaysResync]
    MasterSource = BonDS
    Constraints = <>
    OnCalcFields = BonPosTabCalcFields
    ExtraOptions = [moStoreResult]
    AlternativeView = True
    Macros = <>
    Sql.Strings = (
      'select *, '
      
        'ROUND(MENGE * EPREIS - ((MENGE * EPREIS / 100) * RABATT),2) AS G' +
        'PREIS,'
      'ROUND(EPREIS - ((EPREIS / 100) * RABATT),2) AS ERPREIS'
      ''
      'from JOURNALPOS '
      'where JOURNAL_ID=:REC_ID'
      'order by POSITION')
    RequestLive = False
    Left = 16
    Top = 216
    ParamData = <
      item
        DataType = ftInteger
        Name = 'REC_ID'
        ParamType = ptInput
        Value = '8501'
      end>
    object BonPosTabMATCHCODE: TStringField
      FieldName = 'MATCHCODE'
    end
    object BonPosTabARTNUM: TStringField
      FieldName = 'ARTNUM'
    end
    object BonPosTabBARCODE: TStringField
      FieldName = 'BARCODE'
    end
    object BonPosTabLAENGE: TStringField
      FieldName = 'LAENGE'
    end
    object BonPosTabGROESSE: TStringField
      FieldName = 'GROESSE'
    end
    object BonPosTabDIMENSION: TStringField
      FieldName = 'DIMENSION'
    end
    object BonPosTabGEWICHT: TFloatField
      FieldName = 'GEWICHT'
    end
    object BonPosTabBEZEICHNUNG: TMemoField
      FieldName = 'BEZEICHNUNG'
      BlobType = ftMemo
    end
    object BonPosTabME_EINHEIT: TStringField
      FieldName = 'ME_EINHEIT'
      Size = 10
    end
    object BonPosTabMENGE: TFloatField
      FieldName = 'MENGE'
    end
    object BonPosTabPR_EINHEIT: TFloatField
      FieldName = 'PR_EINHEIT'
    end
    object BonPosTabEPREIS: TFloatField
      FieldName = 'EPREIS'
    end
    object BonPosTabEPREIS_B: TFloatField
      FieldKind = fkCalculated
      FieldName = 'EPREIS_B'
      Calculated = True
    end
    object BonPosTabRABATT: TFloatField
      FieldName = 'RABATT'
    end
    object BonPosTabRABATT2: TFloatField
      FieldName = 'RABATT2'
    end
    object BonPosTabRABATT3: TFloatField
      FieldName = 'RABATT3'
    end
    object BonPosTabERPREIS: TFloatField
      FieldName = 'ERPREIS'
    end
    object BonPosTabERPREIS_B: TFloatField
      FieldKind = fkCalculated
      FieldName = 'ERPREIS_B'
      Calculated = True
    end
    object BonPosTabGPREIS: TFloatField
      FieldName = 'GPREIS'
    end
    object BonPosTabGPREIS_B: TFloatField
      FieldKind = fkCalculated
      FieldName = 'GPREIS_B'
      Calculated = True
    end
    object BonPosTabSTEUER_CODE: TIntegerField
      FieldName = 'STEUER_CODE'
    end
    object BonPosTabMWST_BETRAG: TFloatField
      FieldKind = fkCalculated
      FieldName = 'MWST_BETRAG'
      Calculated = True
    end
    object BonPosTabMWSTPROZ: TFloatField
      FieldKind = fkCalculated
      FieldName = 'MWSTPROZ'
      Calculated = True
    end
  end
end
