{

CAO-Faktura f�r Windows Version 1.0
Copyright (C) 2003 Jan Pokrandt / Jan@JP-SOFT.de

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.


******* CAO-Faktura comes with ABSOLUTELY NO WARRANTY ***************


Programm     : CAO-Faktura
Modul        : CAO_DM
Stand        : 20.05.2004
Version      : 1.2.5.4
Beschreibung : allgemeines nichtvisuelles Datenmodul mit Grundfunktionen
               f�r das Programm

- Funktionen zum �ffnen eines Mandanten (Datenbank im MySQL-Server)
- Funktionen zur Anlage einer neuen Datenbank und aller Tabellen + Formualre
- Funktionen f�r den Zugriff auf die SQL-Registery
  ( ReadString, WriteString etc. )
- Funktionen zum lesen und wirderherstellen von Tabellenlayouts (tDBGrid)
- Funktionen zum Stornieren von Vorg�ngen
- Funktion CalcLeitWaehrung rechnet beliebige W�hrung in Leitw�hrung um


History :

13.01.2003 - Version 1.0.0.48 released Jan Pokrandt
17.01.2003 - Code f�r Einbindung der Formulare bei anlage eines neuen Mandanten
             implementiert ( Formulare aus Datei formulare.cao )
20.01.2003 - DB-SQL-Code f�r aktuelle Version der DB (1.03) angepa�t
           - RX-Komponenten durch JEDI-VCL-Komponenten ersetzt
12.02.2003 - Fehler beim Storno von Belegen gefixt
14.03.2003 - Bug in Fkt. Buche_Einkauf entfernt
             (EK-Preis im Artikelstammm wurde nach Einf�hrung der Rabattgruppen
              nicht mehr korrekt gesetzt)
           - neue Funktion "CalcRabGrpPreis" erstellt, die den EK-Preis bei
             �bergabe des VK-Preises bei Artikeln mit Rabattgruppe errechnet
10.05.2003 - neue Tabelle Firma hinzugef�gt, in der sich nur 1 DS mit den
             aktuellen Firmendaten f�r die Formulare befindet
           - Funktion Buche_Einkauf erweitert, damit der Status der Bestellungen
             korrekt aktualisiert wird.
11.05.2003 - neue Routine zum ermitteln der SQL-Benutzerrechte
             Routinen zum �ffnen, erzeugen und updaten eines Mandanten verbessert
             bei zu wenigen Benutzerrechten wird jetzt mit einer Fehlermeldung
             abgebrochen
           - Registery-Funktionen brechen jetzt sofort ab, wenn die DB nicht
             connected ist
26.05.2003 - Rabattgruppen um VK-Gruppen erweitert
31.05.2003 - neue Funktion zur aktualisierung der Artikelfelder MENGE_xxx_EDI
           - Funktionen Buche_xxxxx �berarbeitet, damit die MENGE_xxx_EDI korrekt
             aktualisiert werden
01.07.2003 - Fehler beim Storno VK mit Barzahlung beseitigt
           - Bug #18 Default '0' aus allen Create anweisungen entfernt
           - Port f�r MySQL-Server wird jetzt aus der .cfg gelesen
13.07.2003 - Code f�r Speichern der Mandantentabelle hinzugef�gt
           - Code zum einf�gen eines neuen Mandanten hinzugef�gt
           - Code f�r Backup + Restore entfernt (jetzt eigene Unit CAO_BACKUP.pas
27.07.2003 - 2 neue Funktionen zum Export vonb Datasets hinzugef�gt
30.07.2003 - neue DB-Version 1.07 eingebaut
           - Bug in Funktion CalcRabGrpPreis beseitigt
           - 2 neue Routinen zum Laden und Speichern von Langtexten in die
             SQL-Registery hinzugef�gt
20.08.2003 - Beim Buchen von Rechnungen werden jetzt die St�cklisten-Artikel
             mit Artikeltyp "Z" in die Rechnung als Unterartikel eingef�gt
06.09.2003 - Bug #32 und Bug #33 beseitigt
21.10.2003 - Automatisches Update f�r PLZ und BLZ eingebaut, daf�r vird die
             Version aus der .CFG-Datei mit der internen Version verglichen und
             dann ggf. die PLZ/BLZ gel�scht und neu eingelesen
16.11.2003 - Mandant-L�schen Funktion hinzugef�gt
02.12.2003 - neue Funktion IncNummerStr erstellt, welche eine Nummer aus einem
             best. Nummernkreis mit hilfe des Nummernformates erzeugt
06.12.2003 - SQL-Pa�wort wird jetzt verschl�sselt abgelegt
13.02.2004 - beim Storno von VK-Rechnungen werden jetzt auch St�cklistenartikel
             korrekt zur�ckgebucht
14.05.2004 - Beim Buchen von EK-Rechnungen werden jetzt die VK's berechnet,
             wenn Kalkulationsfaktoren mit im Spiel sind
20.05.2004 - Flag LINUX hinzugef�gt, das per Kommandozeile gesetzt werden kann
}

{*******************************************************************************

$Id: CAO_DM.pas,v 1.60 2004/05/23 14:22:47 jan Exp $

CVS-Log :
$Log: CAO_DM.pas,v $
Revision 1.60  2004/05/23 14:22:47  jan
Beim Buchen von EK-Rechnungen werden jetzt die VK's berechnet,  wenn Kalkulationsfaktoren mit im Spiel sind

Revision 1.39  2003/09/22 00:21:55  jan
neue Fkt. zur Ermittlung des Kalkulationsfaktors von Warengruppen eingebaut


*******************************************************************************}

unit CAO_DM;

{$I CAO32.INC}

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZTransact, ZMySqlTr, ZConnect, ZMySqlCon, ZQuery, ZMySqlQuery, Db,
  cao_var_const, dbgrids, JvStrHlder, JvComponent, JvVigenereCipher,
  {$IFDEF AVE}cao_ave_ssh, {$ENDIF}
  CaoSecurity;

type tEkUpdateTyp = (ekuNoUpdate, ekuNewPreis, ekuMittel);

type tDBUserRechte  = (urCreate, urDrop, urAlter,
                       urIndex, urSelect, urInsert,
                       urUpdate, urDelete);

type tSDBUserRechte = set of tDBUserRechte;

type tWgrFaktorCache = Record
         Wgr : Integer;
         FTab : array[1..5] of Double;
     end;

type
  TDM1 = class(TDataModule)
    LandDS: TDataSource;
    WhrungDS: TDataSource;
    WgrDS: TDataSource;
    LiefArtDS: TDataSource;
    ZahlArtDS: TDataSource;
    VertreterDS: TDataSource;
    Transact1: TZMySqlTransact;
    JourTab: TZMySqlQuery;
    JourTabQUELLE: TIntegerField;
    JourTabREC_ID: TIntegerField;
    JourTabVRENUM: TIntegerField;
    JourTabKM_STAND: TIntegerField;
    JourTabRDATUM: TDateField;
    JourTabNSUMME: TFloatField;
    JourTabMSUMME: TFloatField;
    JourTabBSUMME: TFloatField;
    JourTabWAEHRUNG: TStringField;
    JourTabSTADIUM: TIntegerField;
    JourTabKUN_NAME1: TStringField;
    JourTabPROJEKT: TStringField;
    JourTabORGNUM: TStringField;
    JourTabADDR_ID: TIntegerField;
    JourTabKFZ_ID: TIntegerField;
    JPosTab: TZMySqlQuery;
    ZahlartTab: TZMySqlQuery;
    VertreterTab: TZMySqlQuery;
    LandTab: TZMySqlQuery;
    WhrungTab: TZMySqlQuery;
    WgrTab: TZMySqlQuery;
    NummerTab: TZMySqlQuery;
    NummerTabQUELLE: TIntegerField;
    KGRTab: TZMySqlQuery;
    KGRTabLANGBEZ: TStringField;
    KgrDS: TDataSource;
    KGRTabSQL_STATEMENT: TMemoField;
    KGRTabGR: TIntegerField;
    WhrungTabWAEHRUNG: TStringField;
    WhrungTabLANGBEZ: TStringField;
    WhrungTabFAKTOR: TFloatField;
    ZahlartTabZAHL_ID: TFloatField;
    ZahlartTabSKONTO_PROZ: TFloatField;
    ZahlartTabSKONTO_TAGE: TIntegerField;
    ZahlartTabLANGBEZ: TStringField;
    LiefArtTab: TZMySqlQuery;
    NummerTabFORMAT: TStringField;
    NummerTabMAINKEY: TStringField;
    RegTab: TZMySqlQuery;
    RegTabMAINKEY: TStringField;
    RegTabNAME: TStringField;
    RegTabVAL_CHAR: TStringField;
    RegTabVAL_DATE: TDateTimeField;
    RegTabVAL_INT: TIntegerField;
    RegTabVAL_DOUBLE: TFloatField;
    RegTabVAL_BLOB: TBlobField;
    RegTabVAL_BIN: TBlobField;
    RegTabVAL_TYP: TIntegerField;
    ArtMengeTab: TZMySqlQuery;
    JPosTabREC_ID: TIntegerField;
    JPosTabQUELLE: TIntegerField;
    JPosTabQUELLE_SUB: TIntegerField;
    JPosTabJOURNAL_ID: TIntegerField;
    JPosTabARTIKELTYP: TStringField;
    JPosTabARTIKEL_ID: TIntegerField;
    JPosTabADDR_ID: TIntegerField;
    JPosTabATRNUM: TIntegerField;
    JPosTabVRENUM: TIntegerField;
    JPosTabVLSNUM: TIntegerField;
    JPosTabPOSITION: TIntegerField;
    JPosTabMATCHCODE: TStringField;
    JPosTabARTNUM: TStringField;
    JPosTabBARCODE: TStringField;
    JPosTabMENGE: TFloatField;
    JPosTabLAENGE: TStringField;
    JPosTabGROESSE: TStringField;
    JPosTabDIMENSION: TStringField;
    JPosTabGEWICHT: TFloatField;
    JPosTabME_EINHEIT: TStringField;
    JPosTabPR_EINHEIT: TFloatField;
    JPosTabEPREIS: TFloatField;
    JPosTabE_RGEWINN: TFloatField;
    JPosTabRABATT: TFloatField;
    JPosTabSTEUER_CODE: TIntegerField;
    JPosTabALTTEIL_PROZ: TFloatField;
    JPosTabALTTEIL_STCODE: TIntegerField;
    JPosTabGEGENKTO: TIntegerField;
    JPosTabBEZEICHNUNG: TMemoField;
    JourTabZAHLART: TIntegerField;
    JourTabSOLL_STAGE: TIntegerField;
    JourTabSOLL_SKONTO: TFloatField;
    JourTabIST_ANZAHLUNG: TFloatField;
    JourTabIST_SKONTO: TFloatField;
    JourTabIST_ZAHLDAT: TDateField;
    JourTabIST_BETRAG: TFloatField;
    JourTabKONTOAUSZUG: TIntegerField;
    JourTabBANK_ID: TIntegerField;
    JourTabUW_NUM: TIntegerField;
    CpySrcKopfTab: TZMySqlQuery;
    CpyDstKopfTab: TZMySqlQuery;
    CpySrcPosTab: TZMySqlQuery;
    CpyDstPosTab: TZMySqlQuery;
    CpySrcKopfTabQUELLE: TIntegerField;
    CpySrcKopfTabREC_ID: TIntegerField;
    CpySrcKopfTabQUELLE_SUB: TIntegerField;
    CpySrcKopfTabADDR_ID: TIntegerField;
    CpySrcKopfTabATRNUM: TIntegerField;
    CpySrcKopfTabVRENUM: TIntegerField;
    CpySrcKopfTabVLSNUM: TIntegerField;
    CpySrcKopfTabFOLGENR: TIntegerField;
    CpySrcKopfTabKM_STAND: TIntegerField;
    CpySrcKopfTabKFZ_ID: TIntegerField;
    CpySrcKopfTabVERTRETER_ID: TIntegerField;
    CpySrcKopfTabGLOBRABATT: TFloatField;
    CpySrcKopfTabADATUM: TDateField;
    CpySrcKopfTabRDATUM: TDateField;
    CpySrcKopfTabLDATUM: TDateField;
    CpySrcKopfTabTermin: TDateField;
    CpySrcKopfTabPR_EBENE: TIntegerField;
    CpySrcKopfTabLIEFART: TIntegerField;
    CpySrcKopfTabZAHLART: TIntegerField;
    CpySrcKopfTabKOST_NETTO: TFloatField;
    CpySrcKopfTabWERT_NETTO: TFloatField;
    CpySrcKopfTabLOHN: TFloatField;
    CpySrcKopfTabWARE: TFloatField;
    CpySrcKopfTabTKOST: TFloatField;
    CpySrcKopfTabMWST_0: TFloatField;
    CpySrcKopfTabMWST_1: TFloatField;
    CpySrcKopfTabMWST_2: TFloatField;
    CpySrcKopfTabMWST_3: TFloatField;
    CpySrcKopfTabNSUMME: TFloatField;
    CpySrcKopfTabMSUMME_0: TFloatField;
    CpySrcKopfTabMSUMME_1: TFloatField;
    CpySrcKopfTabMSUMME_2: TFloatField;
    CpySrcKopfTabMSUMME_3: TFloatField;
    CpySrcKopfTabMSUMME: TFloatField;
    CpySrcKopfTabBSUMME: TFloatField;
    CpySrcKopfTabATSUMME: TFloatField;
    CpySrcKopfTabATMSUMME: TFloatField;
    CpySrcKopfTabWAEHRUNG: TStringField;
    CpySrcKopfTabGEGENKONTO: TIntegerField;
    CpySrcKopfTabSOLL_STAGE: TIntegerField;
    CpySrcKopfTabSOLL_SKONTO: TFloatField;
    CpySrcKopfTabSOLL_NTAGE: TIntegerField;
    CpySrcKopfTabSOLL_RATEN: TIntegerField;
    CpySrcKopfTabSOLL_RATBETR: TFloatField;
    CpySrcKopfTabSOLL_RATINTERVALL: TIntegerField;
    CpySrcKopfTabIST_ANZAHLUNG: TFloatField;
    CpySrcKopfTabIST_SKONTO: TFloatField;
    CpySrcKopfTabIST_ZAHLDAT: TDateField;
    CpySrcKopfTabIST_BETRAG: TFloatField;
    CpySrcKopfTabMAHNKOSTEN: TFloatField;
    CpySrcKopfTabKONTOAUSZUG: TIntegerField;
    CpySrcKopfTabBANK_ID: TIntegerField;
    CpySrcKopfTabSTADIUM: TIntegerField;
    CpySrcKopfTabERSTELLT: TDateField;
    CpySrcKopfTabERST_NAME: TStringField;
    CpySrcKopfTabKUN_ANREDE: TStringField;
    CpySrcKopfTabKUN_NAME1: TStringField;
    CpySrcKopfTabKUN_NAME2: TStringField;
    CpySrcKopfTabKUN_NAME3: TStringField;
    CpySrcKopfTabKUN_ABTEILUNG: TStringField;
    CpySrcKopfTabKUN_STRASSE: TStringField;
    CpySrcKopfTabKUN_LAND: TStringField;
    CpySrcKopfTabKUN_PLZ: TStringField;
    CpySrcKopfTabKUN_ORT: TStringField;
    CpySrcKopfTabUSR1: TStringField;
    CpySrcKopfTabUSR2: TStringField;
    CpySrcKopfTabPROJEKT: TStringField;
    CpySrcKopfTabORGNUM: TStringField;
    CpySrcKopfTabBEST_NAME: TStringField;
    CpySrcKopfTabBEST_CODE: TIntegerField;
    CpySrcKopfTabINFO: TMemoField;
    CpySrcKopfTabUW_NUM: TIntegerField;
    CpyDstKopfTabQUELLE: TIntegerField;
    CpyDstKopfTabREC_ID: TIntegerField;
    CpyDstKopfTabQUELLE_SUB: TIntegerField;
    CpyDstKopfTabADDR_ID: TIntegerField;
    CpyDstKopfTabATRNUM: TIntegerField;
    CpyDstKopfTabVRENUM: TIntegerField;
    CpyDstKopfTabVLSNUM: TIntegerField;
    CpyDstKopfTabFOLGENR: TIntegerField;
    CpyDstKopfTabKM_STAND: TIntegerField;
    CpyDstKopfTabKFZ_ID: TIntegerField;
    CpyDstKopfTabVERTRETER_ID: TIntegerField;
    CpyDstKopfTabGLOBRABATT: TFloatField;
    CpyDstKopfTabADATUM: TDateField;
    CpyDstKopfTabRDATUM: TDateField;
    CpyDstKopfTabLDATUM: TDateField;
    CpyDstKopfTabTermin: TDateField;
    CpyDstKopfTabPR_EBENE: TIntegerField;
    CpyDstKopfTabLIEFART: TIntegerField;
    CpyDstKopfTabZAHLART: TIntegerField;
    CpyDstKopfTabKOST_NETTO: TFloatField;
    CpyDstKopfTabWERT_NETTO: TFloatField;
    CpyDstKopfTabLOHN: TFloatField;
    CpyDstKopfTabWARE: TFloatField;
    CpyDstKopfTabTKOST: TFloatField;
    CpyDstKopfTabMWST_0: TFloatField;
    CpyDstKopfTabMWST_1: TFloatField;
    CpyDstKopfTabMWST_2: TFloatField;
    CpyDstKopfTabMWST_3: TFloatField;
    CpyDstKopfTabNSUMME: TFloatField;
    CpyDstKopfTabMSUMME_0: TFloatField;
    CpyDstKopfTabMSUMME_1: TFloatField;
    CpyDstKopfTabMSUMME_2: TFloatField;
    CpyDstKopfTabMSUMME_3: TFloatField;
    CpyDstKopfTabMSUMME: TFloatField;
    CpyDstKopfTabBSUMME: TFloatField;
    CpyDstKopfTabATSUMME: TFloatField;
    CpyDstKopfTabATMSUMME: TFloatField;
    CpyDstKopfTabWAEHRUNG: TStringField;
    CpyDstKopfTabGEGENKONTO: TIntegerField;
    CpyDstKopfTabSOLL_STAGE: TIntegerField;
    CpyDstKopfTabSOLL_SKONTO: TFloatField;
    CpyDstKopfTabSOLL_NTAGE: TIntegerField;
    CpyDstKopfTabSOLL_RATEN: TIntegerField;
    CpyDstKopfTabSOLL_RATBETR: TFloatField;
    CpyDstKopfTabSOLL_RATINTERVALL: TIntegerField;
    CpyDstKopfTabIST_ANZAHLUNG: TFloatField;
    CpyDstKopfTabIST_SKONTO: TFloatField;
    CpyDstKopfTabIST_ZAHLDAT: TDateField;
    CpyDstKopfTabIST_BETRAG: TFloatField;
    CpyDstKopfTabMAHNKOSTEN: TFloatField;
    CpyDstKopfTabKONTOAUSZUG: TIntegerField;
    CpyDstKopfTabBANK_ID: TIntegerField;
    CpyDstKopfTabSTADIUM: TIntegerField;
    CpyDstKopfTabERSTELLT: TDateField;
    CpyDstKopfTabERST_NAME: TStringField;
    CpyDstKopfTabKUN_ANREDE: TStringField;
    CpyDstKopfTabKUN_NAME1: TStringField;
    CpyDstKopfTabKUN_NAME2: TStringField;
    CpyDstKopfTabKUN_NAME3: TStringField;
    CpyDstKopfTabKUN_ABTEILUNG: TStringField;
    CpyDstKopfTabKUN_STRASSE: TStringField;
    CpyDstKopfTabKUN_LAND: TStringField;
    CpyDstKopfTabKUN_PLZ: TStringField;
    CpyDstKopfTabKUN_ORT: TStringField;
    CpyDstKopfTabUSR1: TStringField;
    CpyDstKopfTabUSR2: TStringField;
    CpyDstKopfTabPROJEKT: TStringField;
    CpyDstKopfTabORGNUM: TStringField;
    CpyDstKopfTabBEST_NAME: TStringField;
    CpyDstKopfTabBEST_CODE: TIntegerField;
    CpyDstKopfTabINFO: TMemoField;
    CpyDstKopfTabUW_NUM: TIntegerField;
    CpySrcKopfTabKUN_NUM: TStringField;
    CpySrcKopfTabBEST_DATUM: TDateField;
    CpyDstKopfTabKUN_NUM: TStringField;
    CpyDstKopfTabBEST_DATUM: TDateField;
    KasBuch: TZMySqlQuery;
    KasBuchJAHR: TIntegerField;
    KasBuchBDATUM: TDateField;
    KasBuchQUELLE: TIntegerField;
    KasBuchJOURNAL_ID: TIntegerField;
    KasBuchZU_ABGANG: TFloatField;
    KasBuchBTXT: TMemoField;
    KasBuchBELEGNUM: TStringField;
    KasBuchGKONTO: TIntegerField;
    KasBuchSKONTO: TFloatField;
    JourTabGEGENKONTO: TIntegerField;
    JourTabKUN_NUM: TStringField;
    JourTabKUN_ANREDE: TStringField;
    JourTabKUN_NAME2: TStringField;
    JourTabKUN_NAME3: TStringField;
    JourTabKUN_ABTEILUNG: TStringField;
    JourTabKUN_STRASSE: TStringField;
    JourTabKUN_LAND: TStringField;
    JourTabKUN_PLZ: TStringField;
    JourTabKUN_ORT: TStringField;
    JourTabSOLL_NTAGE: TIntegerField;
    JourTabSOLL_RATEN: TIntegerField;
    JourTabSOLL_RATBETR: TFloatField;
    JourTabSOLL_RATINTERVALL: TIntegerField;
    JourTabIST_SKONTO_BETR: TFloatField;
    STListTab: TZMySqlQuery;
    STListTabREC_ID: TIntegerField;
    STListTabART_ID: TIntegerField;
    STListTabMENGE: TFloatField;
    STListTabName: TStringField;
    STListTabERSTELLT: TDateField;
    STListTabERST_NAME: TStringField;
    STListTabGEAEND: TDateField;
    STListTabGEAEND_NAME: TStringField;
    JPosTabVIEW_POS: TStringField;
    KunTab: TZMySqlQuery;
    KunTabREC_ID: TIntegerField;
    KunTabKUNNUM2: TStringField;
    KunTabDEB_NUM: TIntegerField;
    KunTabKRD_NUM: TIntegerField;
    KunTabSTATUS: TIntegerField;
    KunTabKUN_LIEFART: TIntegerField;
    KunTabKUN_ZAHLART: TIntegerField;
    KunTabLIEF_LIEFART: TIntegerField;
    KunTabLIEF_ZAHLART: TIntegerField;
    KunTabPR_EBENE: TIntegerField;
    JourTabVLSNUM: TIntegerField;
    JourTabLDATUM: TDateField;
    JourTabLIEFART: TIntegerField;
    JourTabKOST_NETTO: TFloatField;
    JourTabWERT_NETTO: TFloatField;
    ZahlartTabTEXT: TMemoField;
    NummerTabNAME: TStringField;
    CreateMandantStr: TJvStrHolder;
    JourTabQUELLE_SUB: TIntegerField;
    KGRTabMAINKEY: TStringField;
    KunTabLAND: TStringField;
    FirBankTab: TZMySqlQuery;
    FirBankTabkurzbez: TStringField;
    FirBankTabinhaber: TStringField;
    FirBankTabblz: TIntegerField;
    FirBankTabmainkey: TStringField;
    DbUpdTo1_0: TJvStrHolder;
    UpdateArtTab: TZMySqlQuery;
    DBUpdTo1_01: TJvStrHolder;
    ReKFZTab: TZMySqlQuery;
    ReKFZTabKFZ_ID: TIntegerField;
    ReKFZTabADDR_ID: TIntegerField;
    ReKFZTabFGST_NUM: TStringField;
    ReKFZTabPOL_KENNZ: TStringField;
    ReKFZTabSCHL_ZU_2: TStringField;
    ReKFZTabSCHL_ZU_3: TStringField;
    ReKFZTabKM_STAND: TIntegerField;
    ReKFZTabZULASSUNG: TDateField;
    ReKFZTabLE_BESUCH: TDateField;
    ReKFZTabNAE_TUEV: TDateField;
    ReKFZTabNAE_AU: TDateField;
    KunTabNAME1: TStringField;
    KunTabPLZ: TStringField;
    KunTabORT: TStringField;
    KunTabNAME2: TStringField;
    KunTabNAME3: TStringField;
    KunTabANREDE: TStringField;
    KunTabSTRASSE: TStringField;
    JourTabMAHNKOSTEN: TFloatField;
    KunTabBLZ: TStringField;
    KunTabKTO: TStringField;
    KunTabBANK: TStringField;
    CpySrcKopfTabLIEF_ADDR_ID: TIntegerField;
    CpySrcKopfTabMAHNSTUFE: TIntegerField;
    CpySrcKopfTabMAHNDATUM: TDateField;
    CpySrcKopfTabMAHNPRINT: TIntegerField;
    CpyDstKopfTabLIEF_ADDR_ID: TIntegerField;
    CpyDstKopfTabMAHNSTUFE: TIntegerField;
    CpyDstKopfTabMAHNDATUM: TDateField;
    CpyDstKopfTabMAHNPRINT: TIntegerField;
    LiefRabGrp: TZMySqlQuery;
    LiefRabGrpRABGRP_ID: TStringField;
    LiefRabGrpRABGRP_TYP: TIntegerField;
    LiefRabGrpMIN_MENGE: TIntegerField;
    LiefRabGrpLIEF_RABGRP: TIntegerField;
    LiefRabGrpRABATT1: TFloatField;
    LiefRabGrpRABATT2: TFloatField;
    LiefRabGrpRABATT3: TFloatField;
    LiefRabGrpADDR_ID: TIntegerField;
    LiefRabGrpBESCHREIBUNG: TStringField;
    RabGrpDS: TDataSource;
    DBUpdTo1_02: TJvStrHolder;
    ZBatchSql1: TZBatchSql;
    DBUpdTo1_03: TJvStrHolder;
    KunTabEMAIL: TStringField;
    FirmaTab: TZMySqlQuery;
    FirmaDS: TDataSource;
    DBUpdTo1_04: TJvStrHolder;
    JPosTabGEBUCHT: TBooleanField;
    JPosTabSN_FLAG: TBooleanField;
    JPosTabALTTEIL_FLAG: TBooleanField;
    JPosTabBEZ_FEST_FLAG: TBooleanField;
    JourTabFREIGABE1_FLAG: TBooleanField;
    CpySrcKopfTabFREIGABE1_FLAG: TBooleanField;
    CpyDstKopfTabFREIGABE1_FLAG: TBooleanField;
    JPosTabQUELLE_SRC: TIntegerField;
    JourTabBRUTTO_FLAG: TBooleanField;
    JourTabMWST_FREI_FLAG: TBooleanField;
    JPosTabBRUTTO_FLAG: TBooleanField;
    JPosTabRABATT2: TFloatField;
    JPosTabRABATT3: TFloatField;
    CpySrcKopfTabBRUTTO_FLAG: TBooleanField;
    CpySrcKopfTabMWST_FREI_FLAG: TBooleanField;
    CpyDstKopfTabBRUTTO_FLAG: TBooleanField;
    CpyDstKopfTabMWST_FREI_FLAG: TBooleanField;
    JourTabMWST_0: TFloatField;
    JourTabMWST_1: TFloatField;
    JourTabMWST_2: TFloatField;
    JourTabMWST_3: TFloatField;
    JourTabMSUMME_0: TFloatField;
    JourTabMSUMME_1: TFloatField;
    JourTabMSUMME_2: TFloatField;
    JourTabMSUMME_3: TFloatField;
    KunTabBRUTTO_FLAG: TBooleanField;
    KunTabMWST_FREI_FLAG: TBooleanField;
    KunRabGrp: TZMySqlQuery;
    KunRabGrpRABGRP_ID: TStringField;
    KunRabGrpRABGRP_TYP: TIntegerField;
    KunRabGrpMIN_MENGE: TIntegerField;
    KunRabGrpLIEF_RABGRP: TIntegerField;
    KunRabGrpRABATT1: TFloatField;
    KunRabGrpRABATT2: TFloatField;
    KunRabGrpRABATT3: TFloatField;
    KunRabGrpADDR_ID: TIntegerField;
    UniQuery: TZMySqlQuery;
    UniQuery2: TZMySqlQuery;
    ShopOrderStatusTab: TZMySqlQuery;
    ShopOrderStatusTabLANGBEZ: TStringField;
    ShopOrderStatusTabTEXT: TMemoField;
    ShopOrderStatusTabMAINKEY: TStringField;
    ShopOSDS: TDataSource;
    ShopOrderStatusTabORDERSTATUS_ID: TIntegerField;
    DBUpdTo1_05: TJvStrHolder;
    KunTabKUNNUM1: TStringField;
    DBUpdTo1_06: TJvStrHolder;
    DBUpdTo1_07: TJvStrHolder;
    HerstellerTab: TZMySqlQuery;
    HerstellerDS: TDataSource;
    WgrTabID: TIntegerField;
    WgrTabTOP_ID: TIntegerField;
    WgrTabNAME: TStringField;
    WgrTabBESCHREIBUNG: TMemoField;
    WgrTabDEF_EKTO: TIntegerField;
    WgrTabDEF_AKTO: TIntegerField;
    WgrTabVORGABEN: TMemoField;
    SprachTab: TZMySqlQuery;
    SprachDS: TDataSource;
    FirmaTabANREDE: TStringField;
    FirmaTabNAME1: TStringField;
    FirmaTabNAME2: TStringField;
    FirmaTabNAME3: TStringField;
    FirmaTabSTRASSE: TStringField;
    FirmaTabLAND: TStringField;
    FirmaTabPLZ: TStringField;
    FirmaTabORT: TStringField;
    FirmaTabVORWAHL: TStringField;
    FirmaTabTELEFON1: TStringField;
    FirmaTabTELEFON2: TStringField;
    FirmaTabMOBILFUNK: TStringField;
    FirmaTabFAX: TStringField;
    FirmaTabEMAIL: TStringField;
    FirmaTabWEBSEITE: TStringField;
    FirmaTabBANK1_BLZ: TStringField;
    FirmaTabBANK1_KONTONR: TStringField;
    FirmaTabBANK1_NAME: TStringField;
    FirmaTabBANK1_IBAN: TStringField;
    FirmaTabBANK1_SWIFT: TStringField;
    FirmaTabBANK2_BLZ: TStringField;
    FirmaTabBANK2_KONTONR: TStringField;
    FirmaTabBANK2_NAME: TStringField;
    FirmaTabBANK2_IBAN: TStringField;
    FirmaTabBANK2_SWIFT: TStringField;
    FirmaTabKOPFTEXT: TMemoField;
    FirmaTabFUSSTEXT: TMemoField;
    FirmaTabABSENDER: TStringField;
    FirmaTabSTEUERNUMMER: TStringField;
    FirmaTabUST_ID: TStringField;
    FirmaTabIMAGE1: TBlobField;
    FirmaTabIMAGE2: TBlobField;
    FirmaTabIMAGE3: TBlobField;
    FirmaTabUSER_AKT: TStringField;
    FirmaTabLEITWAEHRUNG: TStringField;
    FirmaTabMANDANT_NAME: TStringField;
    JourTabLIEF_ADDR_ID: TIntegerField;
    CpySrcKopfTabPROVIS_WERT: TFloatField;
    CpyDstKopfTabPROVIS_WERT: TFloatField;
    NummerTabNEXT_NUM: TLargeintField;
    RegTabVAL_INT2: TLargeintField;
    RegTabVAL_INT3: TLargeintField;
    FirBankTabKTONR: TLargeintField;
    ZahlartTabNETTO_TAGE: TLargeintField;
    ArtInfoTab: TZMySqlQuery;
    ArtInfoTabREC_ID: TIntegerField;
    ArtInfoTabEK_PREIS: TFloatField;
    ArtInfoTabVK1: TFloatField;
    ArtInfoTabVK2: TFloatField;
    ArtInfoTabVK3: TFloatField;
    ArtInfoTabVK4: TFloatField;
    ArtInfoTabVK5: TFloatField;
    ArtInfoTabVK1B: TFloatField;
    ArtInfoTabVK2B: TFloatField;
    ArtInfoTabVK3B: TFloatField;
    ArtInfoTabVK4B: TFloatField;
    ArtInfoTabVK5B: TFloatField;
    ArtInfoTabMENGE2: TIntegerField;
    ArtInfoTabPREIS2: TFloatField;
    ArtInfoTabMENGE3: TIntegerField;
    ArtInfoTabPREIS3: TFloatField;
    ArtInfoTabMENGE4: TIntegerField;
    ArtInfoTabPREIS4: TFloatField;
    ArtInfoTabMENGE5: TIntegerField;
    ArtInfoTabPREIS5: TFloatField;
    ArtInfoTabMENGE_AKT: TFloatField;
    ArtInfoTabRABGRP_ID: TStringField;
    ArtInfoTabMENGE_BESTELLT: TFloatField;
    ArtInfoTabMENGE_RESERVIERT: TFloatField;
    ArtInfoTabPROVIS_PROZ: TFloatField;
    ArtInfoTabSTEUER_CODE: TIntegerField;
    ArtInfoTabERLOES_KTO: TIntegerField;
    ArtInfoTabAUFW_KTO: TIntegerField;
    ArtInfoTabARTNUM: TStringField;
    ArtInfoTabERSATZ_ARTNUM: TStringField;
    ArtInfoTabMATCHCODE: TStringField;
    ArtInfoTabWARENGRUPPE: TIntegerField;
    ArtInfoTabBARCODE: TStringField;
    ArtInfoTabARTIKELTYP: TStringField;
    ArtInfoTabKAS_NAME: TStringField;
    ArtInfoTabME_EINHEIT: TStringField;
    ArtInfoTabPR_EINHEIT: TFloatField;
    ArtInfoTabLAENGE: TStringField;
    ArtInfoTabGROESSE: TStringField;
    ArtInfoTabDIMENSION: TStringField;
    ArtInfoTabGEWICHT: TFloatField;
    ArtInfoTabKURZNAME: TStringField;
    ArtInfoTabLANGNAME: TMemoField;
    ArtInfoTabALTTEIL_FLAG: TBooleanField;
    ArtInfoTabNO_RABATT_FLAG: TBooleanField;
    ArtInfoTabNO_PROVISION_FLAG: TBooleanField;
    ArtInfoTabNO_BEZEDIT_FLAG: TBooleanField;
    ArtInfoTabNO_VK_FLAG: TBooleanField;
    ArtInfoTabSN_FLAG: TBooleanField;
    ReKunTab: TZMySqlQuery;
    WgrTabSTEUER_CODE: TIntegerField;
    WgrTabVK1_FAKTOR: TFloatField;
    WgrTabVK2_FAKTOR: TFloatField;
    WgrTabVK3_FAKTOR: TFloatField;
    WgrTabVK4_FAKTOR: TFloatField;
    WgrTabVK5_FAKTOR: TFloatField;
    NummerTabMAXLEN: TLargeintField;
    KunTabDEFAULT_LIEFANSCHRIFT_ID: TIntegerField;
    KunTabUST_NUM: TStringField;
    ArtInfoTabPREIS: TFloatField;
    ArtInfoTabPREIS_TYP: TIntegerField;
    ArtInfoTabADRESS_ID: TIntegerField;
    DBUpdTo1_08: TJvStrHolder;
    ArtInfoTabVPE: TIntegerField;
    ArtInfoTabMENGE_LIEF: TFloatField;
    ArtInfoTabMENGE_SOLL: TFloatField;
    ArtInfoTabJID: TIntegerField;
    KunTabFAX: TStringField;
    KunTabBRIEFANREDE: TStringField;
    db1: TZMySqlDatabase;
    ArtInfoTabBESTNUM: TStringField;
    JourTabSHOP_ID: TIntegerField;
    JourTabSHOP_ORDERID: TIntegerField;
    CpyDstKopfTabGEWICHT: TFloatField;
    CpyDstKopfTabROHGEWINN: TFloatField;
    CpySrcKopfTabGEWICHT: TFloatField;
    CpySrcKopfTabROHGEWINN: TFloatField;
    DBUpDTo1_09: TJvStrHolder;
    Cipher: TJvVigenereCipher;
    LandTabID: TStringField;
    LandTabNAME: TStringField;
    LandTabISO_CODE_3: TStringField;
    LandTabFORMAT: TIntegerField;
    LandTabVORWAHL: TStringField;
    LandTabWAEHRUNG: TStringField;
    LandTabSPRACHE: TStringField;
    LandTabPOST_CODE: TStringField;
    CaoSecurity: tCaoSecurity;
    FirBankTabFIBU_KTO: TLargeintField;
    procedure DM1Create(Sender: TObject);
    procedure JourTabCalcFields(DataSet: TDataSet);
    procedure KGRTabBeforePost(DataSet: TDataSet);
    procedure WgrTabBeforePost(DataSet: TDataSet);
    procedure FirBankTabBeforePost(DataSet: TDataSet);
    procedure DataModuleDestroy(Sender: TObject);
    procedure NummerTabNewRecord(DataSet: TDataSet);
    procedure ShopOrderStatusTabBeforePost(DataSet: TDataSet);
    procedure ZBatchSql1BeforeExecute(Sender: TObject);
    procedure Transact1AfterBatchExec(Sender: TObject; var Res: Integer);
    procedure ZBatchSql1AfterExecute(Sender: TObject);
    procedure FirmaTabCalcFields(DataSet: TDataSet);
    procedure LandTabCalcFields(DataSet: TDataSet);
  private
    { Private-Deklarationen }
    InNewNummer : Boolean;
  public
    { Public-Deklarationen }

    user : String;
    view_user,      // Username, der in Formularen und zur Anzeige verwendet
                    // wird, dieser wird aus der Tabelle MITARBEITER ermittelt
    ntuser,
    comp,email      : String[100];
    UserID          : Integer; // Eindeutige Mitarbeiternummer (ID)
    LeitWaehrung    : String;
    LandK2          : String[2];

    MainDir         : String;
    BackupDir       : String;
    DTADir          : String;
    TmpDir          : String;
    LogDir          : String;
    ExportDir       : String;
    ImportDir       : String;

    C2Color         : TColor; // Farbe f. 2. Textzeile in Gittern
    EditColor       : TColor;

    // zum Cachen der Funktion Calcleitwaehrung
    // sonst mu� bei jeder Berechnung in der Registery
    // der Umrechnungskurs geholt werden
    CacheLastWaehrung : String;
    CacheLastKurs     : Double;

    LastVertrID       : Integer;
    LastVertrProz     : Double;

    SQLLog            : Boolean;

    TermID            : Integer; // Eindeutige ID der Arbeitsstation

    MwStTab           : Array[0..3] of Double; // Globale MwSt-Tabelle
    DefMwSt           : Double;  // Default MehrWertSteuer in %
    DefMwStCD         : Integer; // Verweis auf einen der Eintr�ge in MwStTab

    AnzPreis          : Integer; // max. Preis (VK1-VK5)

    GCalcFaktorTab    : array[1..6] of Double; // Globale Kalkulationsfaktoren

    MandantTab        : Array of MandantRec;
    AktMandant        : String;
    MandantOK         : Boolean;

    USE_KFZ           : Boolean;
    AtrisEnable       : Boolean; // True, wenn Atris-Pfad gesetzt ist
    AtrisPfad         : String;

    DefSpracheID      : Integer;
    DefSprachCode     : String;

    BR_RUND_WERT      : Integer; // ganze Cent !!!   z.B. 5 = immer auf ganz 5 Cent runden
    BR_SUM_RUND_WERT  : Integer; // dito, jedoch f�r die Belegerstellung

    WgrFaktorCache    : tWgrFaktorCache;

    MahnFrist         : array[1..5] of Integer; // Anz. Tage als frist pro Mahnstufe

    PLZ_VERSION,
    BLZ_VERSION       : Integer;


    DisplayDLL        : String;

    IsLinux           : Boolean;

    procedure ReadMandanten (App : String);
    procedure SaveMandanten (App : String);
    function GetMandant (Name : String; var Daten : MandantRec):Boolean;
    function OpenMandant(NewMandant, App : String; save : boolean) : boolean;
    procedure NewMandant (Daten : MandantRec);
    procedure DeleteMandant (Name : String);
    procedure InitMandantAfterOpen;


    function UpdateDatabase (Data : tJvStrHolder; var Warnings, Errors : Integer): boolean;

    function GetDBUserRechte (AktUser : Boolean; User,Secret : String): tSDBUserRechte;

    function BucheKasse (Datum      : tDateTime;
                         Quelle     : Integer;
                         Journal_ID : Integer;
                         BelNum     : String;
                         GKonto     : Integer;
                         Skonto     : Double;
                         Betrag     : Double;
                         Text       : String):Boolean;

    // Liefert True zur�ck, wenn die Bankverbindung ok ist
    Function  CheckBankverbindung (addr_id:integer) : boolean;

    // Liefert die Bankverbindung zur�ck, wenn ok
    Function  GetBankverbindung (addr_id:integer; Var BLZ : Integer; var KTO : String) : boolean; // True wenn ok

    Function  GetLieferant (addr_id:integer; Var Info : String) : boolean; // True wenn ok

    function IncNummer (Quelle : Integer) : Int64;
    function IncNummerStr (Quelle : Integer) :String;
    function GetNummerFormat (Quelle : Integer) : String;

    // Funktionen f�r SQL-Registery
    procedure WriteString     (Key, Name, Value : String);
    function  ReadString      (Key, Name, Default : String):String;
    procedure WriteBoolean    (Key, Name : String; Value : Boolean);
    function  ReadBoolean     (Key, Name : String; Default : Boolean):Boolean;
    procedure WriteInteger    (Key, Name : String; Value : Integer);
    function  ReadInteger     (Key, Name : String; Default : Integer):Integer;
    procedure WriteDouble     (Key, Name : String; Value : Double);
    function  ReadDouble      (Key, Name : String; Default : Double):Double;
    function  ReadLongString  (Key, Name, Default : String):String;
    procedure WriteLongString (Key, Name, Value : String);

    // USER-SETTINGS
    procedure WriteStringU     (Key, Name, Value : String);
    function  ReadStringU      (Key, Name, Default : String):String;
    procedure WriteBooleanU    (Key, Name : String; Value : Boolean);
    function  ReadBooleanU     (Key, Name : String; Default : Boolean):Boolean;
    procedure WriteIntegerU    (Key, Name : String; Value : Integer);
    function  ReadIntegerU     (Key, Name : String; Default : Integer):Integer;
    procedure WriteDoubleU     (Key, Name : String; Value : Double);
    function  ReadDoubleU      (Key, Name : String; Default : Double):Double;
    function  ReadLongStringU  (Key, Name, Default : String):String;
    procedure WriteLongStringU (Key, Name, Value : String);

    // Funktionen zum lesen und wiederherstellen von Tabellenlayouts (tDBGrid)
    function  ReadLayout (Key, Name : String; var Data : tStream; Version : Integer = 0) : Boolean;
    procedure WriteLayout (Key, Name : String; Data : tStream; Version : Integer = 0);

    procedure GridLoadLayout(var Grid : tDBGrid; Sec : String; Version : Integer = 0);
    procedure GridSaveLayout(Grid : tDBGrid; Sec : String; Version : Integer = 0);

    // Diverse funktionen zum verbuchen von Rechnungen etc.
    function Buche_Rechnung (Journal_ID : Integer):Integer; // liefert Rechnungnummer zur�ck
    function Buche_Einkauf (Journal_ID : Integer):Integer; // liefert Belegnummer zur�ck
    function Buche_Lieferschein (Journal_ID : Integer;
                                 Teillief : Boolean;
                                 var LieferscheinID : Integer):Integer; // liefert Belegnummer zur�ck
    function Buche_Angebot (Journal_ID : Integer):Integer; // liefert AGB-Nummer zur�ck
    function Buche_BCKasse (Journal_ID : Integer):Integer; // liefert BON-Nummer zur�ck

    function Buche_EKBest (Journal_ID : Integer):Integer; // liefert EK-BST-Nummer zur�ck

    // Diverse Funktionen zum Stornieren von Vorg�ngen
    function Storno_Einkauf (Journal_ID : Integer):Boolean; // True, Wenn OK
    function Storno_Verkauf (Journal_ID : Integer):Boolean; // True, Wenn OK
    function Storno_Angebot (Journal_ID : Integer):Boolean; // True, Wenn OK
    function Storno_Lieferschein (Journal_ID : Integer):Boolean; // True, Wenn OK
    function Storno_EKBestellung (Journal_ID : Integer):Boolean; // True, Wenn OK


    // Rechnet beliebige W�hrung in Leitw�hrung um
    function CalcLeitWaehrung (Betrag : Double; Waehrung : String) : Double;

    // Kopiert Belege
    function CopyRechnung (Journal_ID, Dest :Integer) : Integer;  // Liefert Rec-ID zur�ck

    function GetWGRDefaultKonten (WGR : Integer; var EKTO, AKTO : Integer) : Boolean;
    function CalcRabGrpPreis (RGID:String; PR_Ebene : Integer; var Preis : Double):Boolean;

    function UpdateArtikelEdiMenge (JournalTyp, ArtikelID : Integer; MengeDiff:Double) : Boolean;
    procedure UpdateArtikelPreis (RechTyp, Artikel_ID, Addr_ID : Integer; Preis : Double);

    // Export-Funktionen
    procedure ExportDatasetToStream(Stream: TStream; Dataset : TDataset; Delimiter : String; Spaltennamen : Boolean = True; TextInHochKomma : Boolean = True; DosZeichenSatz : Boolean = False);
    procedure ExportDatasetToFile (FileName: String; Dataset : TDataset; Delimiter : String; Append : Boolean; Spaltennamen : Boolean = True; TextInHochKomma : Boolean = True; DosZeichenSatz : Boolean = False);

    function GetSearchSQL (Felder : array of String; Suchbegriff : String) : String;

    function GetVertreterProv (ID : Integer) : Double;


    procedure CheckTerminalID;


    //--------------------
    function GetArtikelPreis (ArtikelID, KunID, PE : Integer;
                              Brutto : Boolean;
                              Menge : Double;
                              var Preis : Double) : Boolean;


    function GetWGRCalcFaktor (Wgr, PreisID : Integer; var Faktor : Double) : Boolean;
  end;

  function GetProjectVersion:string;

var
  DM1: TDM1;

implementation

uses inifiles, FileCtrl,
     {$IFDEF COMPILER_D6_UP}Variants,{$ENDIF}CAO_Link,
     CAO_Logging, ZExtra, cao_progress, cao_searchclass, ZSqlTypes, CAO_Tool1;

{$R *.DFM}

// aktuell verwendete Datenbank-Version (Struktur/Intern) = 1.09
// wird mit der Version in der SQL-Registry verglichen
// und das �ffnen einer DB mit zu gro�er DB-Version (Programm �lter als DB)
// abgebrochen
const DBVersion_Soll = 109;


procedure TDM1.DM1Create(Sender: TObject);
var P         : PChar;
    Size      : DWord;
    S         : String;
    I         : Integer;
begin
     InNewNummer     :=False;
     SQLLog          :=True;

     IsLinux         :=False;

     if ParamCount>0 then
     begin
       for i :=0 to ParamCount do
       begin
          S :=Uppercase(ParamStr(I));
          if (length(S)>0) and (S[1]='/') then delete (S,1,1);
          if (length(S)>0) and (S[1]='-') then delete (S,1,1);

          if S='LINUX' then IsLinux :=True;
       end;
     end;

     DB1.Connected   :=False;
     DB1.Database    :='';
     DB1.Host        :='';
     DB1.Login       :='';
     DB1.Password    :='';
     DB1.LoginPrompt :=False;

     TermID      :=-1;
     AtrisEnable :=False;

     // Usernamen ermitteln
     size :=1024;
     P:=StrAlloc(size);
     windows.getusername (p,Size);
     ntuser :=p;
     strdispose (p);

     // Rechnernamen ermitteln
     size :=1024;
     p :=StrAlloc (Size);
     windows.getcomputername (P,Size);
     comp :=p;
     strdispose (p);

     MainDir       :=ExtractFilePath (Paramstr(0));
     LogDir        :=MainDir+'LOG\';   ForceDirectories (LogDir);
     AnzPreis      :=5; //default = VK5
     USE_KFZ       :=False;
     DefMwSt       :=16; // in %
     DefMwStCD     :=2;
     DefSpracheID  :=2; // Deutsch
     DefSprachCode :='de';
     LastVertrID   :=-1;
     LastVertrProz :=0;
     AktMandant    :='';
     MandantOK     :=False;

     WgrFaktorCache.Wgr :=-1; // Cache ung�ltig

     SetLength(MandantTab,0);
     ReadMandanten (application.name);
end;

//------------------------------------------------------------------------------
// �ffnet einen bestehenden Mandanten, wenn dieser nicht existiert wird versucht
// die DB und die Tabellen anzulegen, dabei werden die akt. Benutzerrechte
// gepr�ft und bei zu wenigen Rechten abgebrochen.
// Weiterhin wird die Version der Tabellenstruktur gepr�ft und ggf. aktualisiert.
// auch hierbei werden die Benutzerrrechte gecheckt.
// bei einer zu neuen Tabellenversion wird das �ffnen des Mandanten abgebrochen.
//------------------------------------------------------------------------------
function TDM1.OpenMandant(NewMandant, App : String; save : boolean) : boolean;
var Mandant : MandantRec;
    IniName : String;
    MyIni   : tIniFile;
    NewDB   : Boolean;
    I       : Integer;
    DSTab   : Array of Boolean;
    V       : Double;
    S       : String;
    Warn    : Integer;
    Error   : Integer;
    ConOK,
    DBOK    : Boolean;
    Res     : tSDBUserRechte;
    ST      : tDateTime;


    procedure MsgNoSQLRights;
    begin
       MessageDlg
         ('Sie verf�gen �ber zu wenige Benutzerrechte auf dem MySQL-Server'+#13#10+
          'um die Tabellenstruktur f�r den akt. Mandanten zu aktualisieren.'+#13#10+
          'Sie ben�tigen mind. folgende Rechte :'+#13#10+
          'SELECT, INSERT, UPDATE, DELETE, CREATE, DROP, ALTER und INDEX'+#13#13+
          'Bitte loggen Sie sich mit gen�gend Rechten erneut ein.',mterror,[mbok],0);
    end;

    function ReadString (Key, Name, Default : String):String;
    begin
       if not DB1.Connected then exit;

       UniQuery.Close;
       UniQuery.Sql.Text :='select * from REGISTERY '+
                           'where MAINKEY=:KEY and NAME=:NAME';
       UniQuery.ParamByName('KEY').AsString :=Key;
       UniQuery.ParamByName('NAME').AsString :=Name;
       UniQuery.Open;
       if UniQuery.RecordCount>0
         then Result :=UniQuery.FieldByName('VAL_CHAR').AsString
         else Result :=Default;
       UniQuery.Close;
    end;

begin
     Result :=False;
     NewDB  :=False;
     if GetMandant (NewMandant,Mandant) then
     begin
          // offene Datasets merken
          SetLength(DSTab,DB1.DatasetCount);
          for i:=0 to DB1.DatasetCount-1
           do DSTab[i] :=tDataset(DB1.Datasets[i]).Active;

          DB1.Disconnect;

          {$IFDEF AVE}
          if (Assigned(SSHForm))and(SSHForm.EndeBtn.Enabled) then
          begin
             SSHForm.Show;
             SSHForm.Logout;
             ST :=Now();
             repeat
                Application.ProcessMessages;
             until (SSHForm.EndeBtn.Enabled=False)or
                   (Now>ST+ (1/24/60/60) * 30); // max. 30 Sek.
          end;
          {$ENDIF}


          GetMandant (NewMandant,Mandant);

          DB1.Host     :=Mandant.Server;
          DB1.Login    :=Mandant.User;
          DB1.Password :=Mandant.Pass;
          DB1.Database :=Mandant.DB;

          {$IFDEF AVE}
          if Mandant.UseSSH then
          begin
             if not Assigned(SSHForm) then SSHForm :=tSSHForm.Create(Self);
             SSHForm.Show;
             SSHForm.StartBtnClick (Self);
             ST :=Now();
             repeat
                Application.ProcessMessages;
             until ((SSHForm.StartBtn.Enabled=False)and
                   (Now>ST+ (1/24/60/60) * 15))or
                   (Now>ST+ (1/24/60/60) * 60); // max. 30 Sek.
          end;
          {$ENDIF}


          //NEU
          DB1.Port     :=IntToStr(Mandant.Port);
          if Mandant.UseNTUserName then DB1.Login :=User;
          DB1.LoginPrompt :=Mandant.ShowLoginDlg;

          ConOK :=False; DBOK :=False;
          try
             DB1.Connect;

             if DB1.LoginPrompt then User :=DB1.Login else User :=NTUser;

             ConOK :=True;
             DBOK  :=True;

             {$IFDEF AVE}
             if (Assigned(SSHForm)) then SSHForm.Hide;
             {$ENDIF}

          except
             on E: Exception do
             begin
               if (Pos('UNBEKANNTE DATENBANK',Uppercase(E.Message))=1)or
                  (Pos('UNKNOWN DATABASE',Uppercase(E.Message))=1) then
               begin
                 if MessageDlg ('Die Datenbank scheint noch nicht zu '+
                                'existieren.'+#13#10+
                                'Wollen Sie die Datenbank erstellen ?',
                                mtconfirmation,[mbyes,mbno],0)=mryes then
                 begin
                   try
                     DB1.CreateDatabase (Mandant.DB); // DB anlegen
                     DBOK :=True;
                     DB1.Connect;
                     NewDB :=True; ConOK :=True;
                   except
                     //MessageDlg ('Fehler beim erzeugen der Datenbank !',
                     //            mterror,[mbok],0);
                   end;
                 end
                 else exit;
               end
                  else
               begin
                  MessageDlg ('Fehler beim verbinden zum MySQL-Server.'+
                              #13#10#13#10+
                              'Meldung :'+#13#10+E.Message,mterror,[mbok],0);
                  exit;
               end;
             end;
          end;

          if not DBOK then
          begin
             MessageDlg ('Die Datenbank f�r diesen Mandant konnte nicht '+
                         'erstellt werden !'+#13#10+
                         'Bitte pr�fen Sie die Einstellungen und ob Sie �ber '+
                         'ausreichende Rechte'+#13#10+
                         'zum erstellen einer DB auf diesem Server verf�gen.',
                         mterror,[mbok],0);
             exit;
          end;

          if not ConOK then
          begin
             MessageDlg ('Der gew�nschte Mandant konnte nicht ge�ffnet werden !'+#13#10+
                         '�berpr�fen Sie die Verbindung zum Server, Benutzernamen'+#13#10+
                         'und Pa�wort.',mterror,[mbok],0);
             exit;
          end;

          if not NewDB then
          begin
               // pr�fen, ob Tabellen vorhanden sind, wenn nicht, dann neu erzeugen
               try
                  uniquery.close;
                  uniquery.sql.text :='SHOW TABLES';
                  uniquery.open;
                  NewDB :=(UniQuery.RecordCount=0)or
                          (
                           (UniQuery.RecordCount=1)and
                           (UniQuery.FieldByName('Tables_in_'+DB1.Database).AsString='UCHECK')
                          );
               except

               end;
               Uniquery.Close;
          end;


          try
             if NewDB then // neue DB wurde erstellt
             begin
                Res :=GetDBUserRechte (True, '', '');
                if (urSelect in Res) and (urInsert in Res) and
                   (urUpdate in Res) and (urDelete in Res) and
                   (urCreate in Res) and (urAlter  in Res) and
                   (urDrop   in Res) and (urIndex  in Res) then
                begin
                  // User hat die ben�tigten Rechte ! Jetzt Tabellen anlegen ...

                  if not UpdateDatabase (CreateMandantStr, Warn, Error)//CreateMandantSql
                   then MessageDlg ('Fehler beim erstellen der Tabellen !',
                                    mterror,[mbok],0);
                end
                   else
                begin
                   MessageDlg
                     ('Sie verf�gen �ber zu wenige Benutzerrechte auf '+
                      'dem MySQL-Server'+#13#10+
                      'um die Tabellenstruktur f�r einen neuen Mandant (DB) '+
                      'zu erzeugen.'+#13#10+
                      'Sie ben�tigen mind. folgende Rechte :'+#13#10+
                      'SELECT, INSERT, UPDATE, DELETE, CREATE, DROP, '+
                      'ALTER und INDEX'+#13#13+
                      'Bitte loggen Sie sich mit gen�gend Rechten erneut ein.',
                      mterror,[mbok],0);

                   DB1.Disconnect;
                   exit;
                end;
             end;
          except
             MessageDlg ('Fehler beim erstellen der Tabellen !',mterror,[mbok],0);
             exit;
          end;

          try
             if NewDB then   // wenn neue Tabellen angelegt, dann evt. Formulare importieren ...
              if (fileexists (extractfilepath(paramstr(0))+'formulare.cao'))and
                 (MessageDlg ('Sollen die Standardformulare installiert werden ?',
                              mtconfirmation,mbyesnocancel,0)=mryes) then
              begin
                 ZBatchSQL1.Sql.LoadFromFile (extractfilepath(paramstr(0))+'formulare.cao');
                 ProgressForm.Init ('Formulare installieren ...');
                 ZBatchSQL1.ExecSql;
                 ProgressForm.Stop;
              end;
          except
             MessageDlg ('Fehler beim anlegen der Formulare !',mterror,[mbok],0);
          end;
          ProgressForm.Stop;


          try
             S :=ReadString ('MAIN','DB_VERSION','0.00');

             if DecimalSeparator<>'.' then
               while (Pos('.',S)>0) do S[Pos('.',S)] :=DecimalSeparator;
             V :=StrToFloat(S);
          except
             V :=-1; // zur Sicherheit, damit die n�. Updateschritte nicht ausgef�hrt werden !!!

             MessageDlg ('Die Datenbankversion konnte nicht ermittelt werden !'+#13#10+
                         'Der ausgew�hlte Mandant wurde nicht ge�ffnet.',mterror,[mbok],0);
             DB1.Disconnect;
             exit;
          end;
          if V = 0.00 then
          begin
             MessageDlg ('Die ausgew�hlte Datenbank ist nicht auf dem aktuellen Stand'+#13#10+
                         'und wird deshalb nun aktualisiert (Version 0.00 auf Version 1.00)',
                         mtinformation,[mbok],0);


             Res :=GetDBUserRechte (True, '', '');
             if (urSelect in Res) and (urInsert in Res) and (urUpdate in Res) and
                (urDelete in Res) and (urCreate in Res) and (urAlter  in Res) and
                (urDrop   in Res) and (urIndex  in Res) then
             begin
               // User hat die ben�tigten Rechte !
               if not UpdateDataBase (DbUpdTo1_0, Warn, Error)
                then MessageDlg ('Fehler beim Update der Tabellenstruktur auf Version 1.00 !',
                                 mterror,[mbok],0);
             end
                else
             begin
                MsgNoSQLRights;
                DB1.Disconnect;
                exit;
             end;
          end;

          // DB Update-Check f�r Tabellenstruktur Version 1.01
          try
             S :=ReadString ('MAIN','DB_VERSION','0.00');
             if DecimalSeparator<>'.' then
               while (Pos('.',S)>0) do S[Pos('.',S)] :=DecimalSeparator;
             V :=StrToFloat(S);
          except
             V :=-1; // zur Sicherheit, damit die n�. Updateschritte nicht ausgef�hrt werden !!!

             MessageDlg ('Die Datenbankversion konnte nicht ermittelt werden !'+#13#10+
                         'Der ausgew�hlte Mandant wurde nicht ge�ffnet.',mterror,[mbok],0);
             DB1.Disconnect;
             exit;
          end;
          if V = 1.00 then
          begin
             MessageDlg ('Die ausgew�hlte Datenbank ist nicht auf dem aktuellen Stand'+#13#10+
                         'und wird deshalb nun aktualisiert (Version 1.00 auf Version 1.01)',
                         mtinformation,[mbok],0);

             Res :=GetDBUserRechte (True, '', '');
             if (urSelect in Res) and (urInsert in Res) and (urUpdate in Res) and
                (urDelete in Res) and (urCreate in Res) and (urAlter  in Res) and
                (urDrop   in Res) and (urIndex  in Res) then
             begin
               // User hat die ben�tigten Rechte !
               if not UpdateDataBase (DBUpdTo1_01, Warn, Error)
                then MessageDlg ('Fehler beim Update der Tabellenstruktur auf Version 1.01 !',
                                 mterror,[mbok],0);
             end
                else
             begin
                MsgNoSQLRights;
                DB1.Disconnect;
                exit;
             end;
          end;

          // DB Update-Check f�r Tabellenstruktur Version 1.02
          try
             S :=ReadString ('MAIN','DB_VERSION','0.00');
             if DecimalSeparator<>'.' then
               while (Pos('.',S)>0) do S[Pos('.',S)] :=DecimalSeparator;
             V :=StrToFloat(S)*100;
          except
             V :=-1; // zur Sicherheit, damit die n�. Updateschritte nicht ausgef�hrt werden !!!

             MessageDlg ('Die Datenbankversion konnte nicht ermittelt werden !'+#13#10+
                         'Der ausgew�hlte Mandant wurde nicht ge�ffnet.',mterror,[mbok],0);
             DB1.Disconnect;
             exit;
          end;
          if V = 101 then
          begin
             MessageDlg ('Die ausgew�hlte Datenbank ist nicht auf dem aktuellen Stand'+#13#10+
                         'und wird deshalb nun aktualisiert (Version 1.01 auf Version 1.02)',
                         mtinformation,[mbok],0);


             Res :=GetDBUserRechte (True, '', '');
             if (urSelect in Res) and (urInsert in Res) and (urUpdate in Res) and
                (urDelete in Res) and (urCreate in Res) and (urAlter  in Res) and
                (urDrop   in Res) and (urIndex  in Res) then
             begin
               // User hat die ben�tigten Rechte !
               if not UpdateDataBase (DBUpdTo1_02, Warn, Error)
                then MessageDlg ('Fehler beim Update der Tabellenstruktur auf Version 1.02 !',
                                 mterror,[mbok],0);
             end
                else
             begin
                MsgNoSQLRights;
                DB1.Disconnect;
                exit;
             end;
          end;


          // DB Update-Check f�r Tabellenstruktur Version 1.03
          try
             S :=ReadString ('MAIN','DB_VERSION','0.00');
             if DecimalSeparator<>'.' then
               while (Pos('.',S)>0) do S[Pos('.',S)] :=DecimalSeparator;
             V :=StrToFloat(S)*100;
          except
             V :=-1; // zur Sicherheit, damit die n�. Updateschritte nicht ausgef�hrt werden !!!

             MessageDlg ('Die Datenbankversion konnte nicht ermittelt werden !'+#13#10+
                         'Der ausgew�hlte Mandant wurde nicht ge�ffnet.',mterror,[mbok],0);
             DB1.Disconnect;
             exit;
          end;
          if V = 102 then
          begin
             MessageDlg ('Die ausgew�hlte Datenbank ist nicht auf dem aktuellen Stand'+#13#10+
                         'und wird deshalb nun aktualisiert (Version 1.02 auf Version 1.03)',
                         mtinformation,[mbok],0);

             Res :=GetDBUserRechte (True, '', '');
             if (urSelect in Res) and (urInsert in Res) and (urUpdate in Res) and
                (urDelete in Res) and (urCreate in Res) and (urAlter  in Res) and
                (urDrop   in Res) and (urIndex  in Res) then
             begin
               // User hat die ben�tigten Rechte !
               if not UpdateDataBase (DBUpdTo1_03, Warn, Error)
                then MessageDlg ('Fehler beim Update der Tabellenstruktur auf Version 1.03 !',
                                 mterror,[mbok],0);
             end
                else
             begin
                MsgNoSQLRights;
                DB1.Disconnect;
                exit;
             end;

          end;

          // DB Update-Check f�r Tabellenstruktur Version 1.04
          try
             S :=ReadString ('MAIN','DB_VERSION','0.00');
             if DecimalSeparator<>'.' then
               while (Pos('.',S)>0) do S[Pos('.',S)] :=DecimalSeparator;
             V :=StrToFloat(S)*100;
          except
             V :=-1; // zur Sicherheit, damit die n�. Updateschritte nicht ausgef�hrt werden !!!

             MessageDlg ('Die Datenbankversion konnte nicht ermittelt werden !'+#13#10+
                         'Der ausgew�hlte Mandant wurde nicht ge�ffnet.',mterror,[mbok],0);
             DB1.Disconnect;
             exit;
          end;
          if V = 103 then
          begin
             MessageDlg ('Die ausgew�hlte Datenbank ist nicht auf dem aktuellen Stand'+#13#10+
                         'und wird deshalb nun aktualisiert (Version 1.03 auf Version 1.04)',
                         mtinformation,[mbok],0);

             Res :=GetDBUserRechte (True, '', '');
             if (urSelect in Res) and (urInsert in Res) and (urUpdate in Res) and
                (urDelete in Res) and (urCreate in Res) and (urAlter  in Res) and
                (urDrop   in Res) and (urIndex  in Res) then
             begin
               // User hat die ben�tigten Rechte !
               if not UpdateDataBase (DBUpdTo1_04, Warn, Error)
                then MessageDlg ('Fehler beim Update der Tabellenstruktur auf Version 1.04 !',
                                 mterror,[mbok],0);
             end
                else
             begin
                MsgNoSQLRights;
                DB1.Disconnect;
                exit;
             end;

          end;


          // DB Update-Check f�r Tabellenstruktur Version 1.05
          try
             S :=ReadString ('MAIN','DB_VERSION','0.00');
             if DecimalSeparator<>'.' then
               while (Pos('.',S)>0) do S[Pos('.',S)] :=DecimalSeparator;
             V :=StrToFloat(S)*100;
          except
             V :=-1; // zur Sicherheit, damit die n�. Updateschritte nicht ausgef�hrt werden !!!

             MessageDlg ('Die Datenbankversion konnte nicht ermittelt werden !'+#13#10+
                         'Der ausgew�hlte Mandant wurde nicht ge�ffnet.',mterror,[mbok],0);
             DB1.Disconnect;
             exit;
          end;
          if V = 104 then
          begin
             MessageDlg ('Die ausgew�hlte Datenbank ist nicht auf dem aktuellen Stand'+#13#10+
                         'und wird deshalb nun aktualisiert (Version 1.04 auf Version 1.05)',
                         mtinformation,[mbok],0);

             Res :=GetDBUserRechte (True, '', '');
             if (urSelect in Res) and (urInsert in Res) and (urUpdate in Res) and
                (urDelete in Res) and (urCreate in Res) and (urAlter  in Res) and
                (urDrop   in Res) and (urIndex  in Res) then
             begin
               // User hat die ben�tigten Rechte !
               if not UpdateDataBase (DBUpdTo1_05, Warn, Error)
                then MessageDlg ('Fehler beim Update der Tabellenstruktur auf Version 1.05 !',
                                 mterror,[mbok],0);
             end
                else
             begin
                MsgNoSQLRights;
                DB1.Disconnect;
                exit;
             end;

          end;

          // DB Update-Check f�r Tabellenstruktur Version 1.06
          try
             S :=ReadString ('MAIN','DB_VERSION','0.00');
             if DecimalSeparator<>'.' then
               while (Pos('.',S)>0) do S[Pos('.',S)] :=DecimalSeparator;
             V :=StrToFloat(S)*100;
          except
             V :=-1; // zur Sicherheit, damit die n�. Updateschritte nicht ausgef�hrt werden !!!

             MessageDlg ('Die Datenbankversion konnte nicht ermittelt werden !'+#13#10+
                         'Der ausgew�hlte Mandant wurde nicht ge�ffnet.',mterror,[mbok],0);
             DB1.Disconnect;
             exit;
          end;
          if V = 105 then
          begin
             MessageDlg ('Die ausgew�hlte Datenbank ist nicht auf dem aktuellen Stand'+#13#10+
                         'und wird deshalb nun aktualisiert (Version 1.05 auf Version 1.06)',
                         mtinformation,[mbok],0);

             Res :=GetDBUserRechte (True, '', '');
             if (urSelect in Res) and (urInsert in Res) and (urUpdate in Res) and
                (urDelete in Res) and (urCreate in Res) and (urAlter  in Res) and
                (urDrop   in Res) and (urIndex  in Res) then
             begin
               // User hat die ben�tigten Rechte !
               if not UpdateDataBase (DBUpdTo1_06, Warn, Error)
                then MessageDlg ('Fehler beim Update der Tabellenstruktur auf Version 1.06 !',
                                 mterror,[mbok],0);
             end
                else
             begin
                MsgNoSQLRights;
                DB1.Disconnect;
                exit;
             end;

          end;

          // DB Update-Check f�r Tabellenstruktur Version 1.07
          try
             S :=ReadString ('MAIN','DB_VERSION','0.00');
             if DecimalSeparator<>'.' then
               while (Pos('.',S)>0) do S[Pos('.',S)] :=DecimalSeparator;
             V :=StrToFloat(S)*100;
          except
             V :=-1; // zur Sicherheit, damit die n�. Updateschritte nicht ausgef�hrt werden !!!

             MessageDlg ('Die Datenbankversion konnte nicht ermittelt werden !'+#13#10+
                         'Der ausgew�hlte Mandant wurde nicht ge�ffnet.',mterror,[mbok],0);
             DB1.Disconnect;
             exit;
          end;
          if V = 106 then
          begin
             MessageDlg ('Die ausgew�hlte Datenbank ist nicht auf dem aktuellen Stand'+#13#10+
                         'und wird deshalb nun aktualisiert (Version 1.06 auf Version 1.07)',
                         mtinformation,[mbok],0);

             Res :=GetDBUserRechte (True, '', '');
             if (urSelect in Res) and (urInsert in Res) and (urUpdate in Res) and
                (urDelete in Res) and (urCreate in Res) and (urAlter  in Res) and
                (urDrop   in Res) and (urIndex  in Res) then
             begin
               // User hat die ben�tigten Rechte !
               if not UpdateDataBase (DBUpdTo1_07, Warn, Error)
                then MessageDlg ('Fehler beim Update der Tabellenstruktur auf Version 1.07 !',
                                 mterror,[mbok],0);
             end
                else
             begin
                MsgNoSQLRights;
                DB1.Disconnect;
                exit;
             end;

          end;

          // DB Update-Check f�r Tabellenstruktur Version 1.08
          try
             S :=ReadString ('MAIN','DB_VERSION','0.00');
             if DecimalSeparator<>'.' then
               while (Pos('.',S)>0) do S[Pos('.',S)] :=DecimalSeparator;
             V :=StrToFloat(S)*100;
          except
             V :=-1; // zur Sicherheit, damit die n�. Updateschritte nicht ausgef�hrt werden !!!

             MessageDlg ('Die Datenbankversion konnte nicht ermittelt werden !'+#13#10+
                         'Der ausgew�hlte Mandant wurde nicht ge�ffnet.',mterror,[mbok],0);
             DB1.Disconnect;
             exit;
          end;
          if V = 107 then
          begin
             MessageDlg ('Die ausgew�hlte Datenbank ist nicht auf dem aktuellen Stand'+#13#10+
                         'und wird deshalb nun aktualisiert (Version 1.07 auf Version 1.08)',
                         mtinformation,[mbok],0);

             Res :=GetDBUserRechte (True, '', '');
             if (urSelect in Res) and (urInsert in Res) and (urUpdate in Res) and
                (urDelete in Res) and (urCreate in Res) and (urAlter  in Res) and
                (urDrop   in Res) and (urIndex  in Res) then
             begin
               // User hat die ben�tigten Rechte !
               if not UpdateDataBase (DBUpdTo1_08, Warn, Error)
                then MessageDlg ('Fehler beim Update der Tabellenstruktur auf Version 1.08 !',
                                 mterror,[mbok],0);
             end
                else
             begin
                MsgNoSQLRights;
                DB1.Disconnect;
                exit;
             end;

          end;

          // DB Update-Check f�r Tabellenstruktur Version 1.09
          try
             S :=ReadString ('MAIN','DB_VERSION','0.00');
             if DecimalSeparator<>'.' then
               while (Pos('.',S)>0) do S[Pos('.',S)] :=DecimalSeparator;
             V :=StrToFloat(S)*100;
          except
             V :=-1; // zur Sicherheit, damit die n�. Updateschritte nicht ausgef�hrt werden !!!

             MessageDlg ('Die Datenbankversion konnte nicht ermittelt werden !'+#13#10+
                         'Der ausgew�hlte Mandant wurde nicht ge�ffnet.',mterror,[mbok],0);
             DB1.Disconnect;
             exit;
          end;
          if V = 108 then
          begin
             MessageDlg ('Die ausgew�hlte Datenbank ist nicht auf dem aktuellen Stand'+#13#10+
                         'und wird deshalb nun aktualisiert (Version 1.08 auf Version 1.09)',
                         mtinformation,[mbok],0);

             Res :=GetDBUserRechte (True, '', '');
             if (urSelect in Res) and (urInsert in Res) and (urUpdate in Res) and
                (urDelete in Res) and (urCreate in Res) and (urAlter  in Res) and
                (urDrop   in Res) and (urIndex  in Res) then
             begin
               // User hat die ben�tigten Rechte !
               if not UpdateDataBase (DBUpdTo1_09, Warn, Error)
                then MessageDlg ('Fehler beim Update der Tabellenstruktur auf Version 1.09 !',
                                 mterror,[mbok],0);
             end
                else
             begin
                MsgNoSQLRights;
                DB1.Disconnect;
                exit;
             end;

          end;

          // DB Tabellenstruktur zu neu f�r das Programm ???
          try
             S :=ReadString ('MAIN','DB_VERSION','0.00');
             if DecimalSeparator<>'.' then
               while (Pos('.',S)>0) do S[Pos('.',S)] :=DecimalSeparator;
             V :=StrToFloat(S)*100;
          except
             V :=-1; // zur Sicherheit, damit die n�. Updateschritte nicht ausgef�hrt werden !!!

             MessageDlg ('Die Datenbankversion konnte nicht ermittelt werden !'+#13#10+
                         'Der ausgew�hlte Mandant wurde nicht ge�ffnet.',mterror,[mbok],0);
             DB1.Disconnect;
             exit;
          end;
          if V=DBVersion_Soll then
          begin

            for i:=0 to DB1.DatasetCount-1 do
             if DSTab[i] then tDataset(DB1.Datasets[i]).Active :=True;

            setlength(DSTab,0);

            AktMandant :=NewMandant;

            Result :=True;

            if Save then
            begin
               ininame :=extractfilepath(paramstr(0))+'CAO32_DB.CFG';
               MyIni :=tIniFile.Create (IniName);
               try
                  MyIni.WriteString ('MANDANTEN',APP,AktMandant);
               finally
                  MyIni.Free;
               end;
            end;

            MandantOK  :=True;
          end
             else
          begin
            if V>DBVersion_Soll then
            begin
              MessageDlg ('Der aktuelle Mandant hat eine neuere DB-Version'+#13#10+
                          'und kann mit dieser Programmversion nicht ge�ffnet werden.'+#13#10+
                          'Bitte aktualisieren Sie das Programm ...',mterror,[mbok],0);

              DB1.Disconnect;
              Result :=False;
            end;
          end;

     end else
     begin
       // Mandantendaten konnten aus der INI nicht gelesen werden.

       Result :=False;

       MessageDlg ('Die Einstellungen f�r den aktuellen Mandanten'+#13#10+
                   'konnten aus der Datei "CAO32_DB.CFG" nicht gelesen werden.'+#13#10+
                   'Bitte pr�fen Sie die Einstellungen.',mterror,[mbok],0);
     end;
end;
//------------------------------------------------------------------------------
// Sollte nach dem �ffnen eines neuen Mandanten aufgerufen werden
//------------------------------------------------------------------------------
procedure TDM1.InitMandantAfterOpen;
var S : String; ini : tinifile; i,v : Integer;
begin
     try
        // Bug im Create-Script der DB-Version 1.09
        // Das Feld NO_RABATT_FLAG wurde nicht angelegt

        Uniquery.Close;
        Uniquery.Sql.Text :='show fields from JOURNALPOS';
        UniQuery.Open;
        if not UniQuery.Locate ('Field','NO_RABATT_FLAG',[]) then
        begin
           UniQuery.Close;
           Uniquery.Sql.Text :='ALTER TABLE JOURNALPOS '+
                               'ADD NO_RABATT_FLAG ENUM("N","Y") DEFAULT "N" '+
                               'NOT NULL AFTER BRUTTO_FLAG';
           Uniquery.ExecSql;

        end else UniQuery.Close;

        // Ende Bugfix



        WhrungTab.Open;
        LandTab.Open;
        LiefArtTab.Open;
        ZahlArtTab.Open;
        ShopOrderStatusTab.Open;
        VertreterTab.Open;

        SprachTab.Open;

        if SprachTab.RecordCount>0 then
        begin
           DefSpracheID  :=SprachTab.FieldByName('SPRACH_ID').AsInteger;
           DefSprachCode :=SprachTab.FieldByName('CODE').AsString;
        end;

        // PLZ installieren
        try
          // Alte Version einlesen
          V :=DM1.ReadInteger ('MAIN','PLZ_VERSION',106);

          uniquery.Close;
          uniquery.sql.text :='select count(*) as ANZ from PLZ';
          uniquery.Open;
          if (uniquery.recordcount=1)and
             (
              (uniquery.fieldbyname('ANZ').asInteger<10) or
              (PLZ_VERSION > V)
             )and
             (fileexists (extractfilepath(paramstr(0))+'plz.cao')) then
          begin
             ProgressForm.Init ('PLZ importieren...');
             uniquery.close;
             uniquery.sql.text :='delete from PLZ';
             uniquery.execsql;
             ZBatchSql1.Sql.LoadFromFile (extractfilepath(paramstr(0))+'plz.cao');
             try
                SQLLog :=False;
                Screen.Cursor :=crSqlWait;
                ZBatchSql1.ExecSql;
             finally
                SQLLog :=True;
                Screen.Cursor :=crDefault;
             end;
             DM1.WriteInteger ('MAIN','PLZ_VERSION',PLZ_VERSION);
          end;
          uniquery.close;
        except
        end;
        ProgressForm.Stop;

        // BLZ installieren
        try
          // Alte Version einlesen
          V :=DM1.ReadInteger ('MAIN','BLZ_VERSION',106);

          uniquery.Close;
          uniquery.sql.text :='select count(*) as ANZ from BLZ';
          uniquery.Open;
          if (uniquery.recordcount=1)and
             (
              (uniquery.fieldbyname('ANZ').asInteger<10) or
              (BLZ_VERSION > V)
             ) and
             (fileexists (extractfilepath(paramstr(0))+'blz.cao')) then
          begin
             ProgressForm.Init ('BLZ importieren...');
             uniquery.close;
             uniquery.sql.text :='delete from BLZ';
             uniquery.execsql;
             ZBatchSql1.Sql.LoadFromFile (extractfilepath(paramstr(0))+'blz.cao');
             try
                SQLLog :=False;
                Screen.Cursor :=crSqlWait;
                ZBatchSql1.ExecSql;
             finally
                SQLLog :=True;
                Screen.Cursor :=crDefault;
             end;
             DM1.WriteInteger ('MAIN','BLZ_VERSION',BLZ_VERSION);
          end;
          uniquery.close;
        except
        end;
        ProgressForm.Stop;

        // L�nder installieren
        try
          uniquery.Close;
          uniquery.sql.text :='select count(*) as ANZ from LAND';
          uniquery.Open;
          if (uniquery.recordcount=1)and
             (uniquery.fieldbyname('ANZ').asInteger<20)and
             (fileexists (extractfilepath(paramstr(0))+'land.cao')) then
          begin
             ProgressForm.Init ('L�nder importieren...');
             ZBatchSql1.Sql.LoadFromFile (extractfilepath(paramstr(0))+'land.cao');
             if uniquery.fieldbyname('ANZ').asInteger>0
              then ZBatchSql1.Sql.Insert (0,'delete from LAND;');
             ZBatchSql1.ExecSql;
          end;
          uniquery.close;
        except
        end;
        ProgressForm.Stop;

        LeitWaehrung :=ReadString ('MAIN','LEITWAEHRUNG','@@');
        if LeitWaehrung='@@' then
        begin
           LeitWaehrung :='�';
           WriteString ('MAIN','LEITWAEHRUNG','�');
        end;

        LandK2 :=ReadString ('MAIN','LAND','DE');

        AnzPreis :=ReadInteger ('MAIN\ARTIKEL','ANZPREIS',-1);
        if AnzPreis=-1 then
        begin
           WriteInteger ('MAIN\ARTIKEL','ANZPREIS',5);
           AnzPreis :=5;
        end;

        // Globale MWST-Tabelle
        MWSTTab[0] :=ReadDouble  ('MAIN\MWST','0',0);
        MWSTTab[1] :=ReadDouble  ('MAIN\MWST','1',7);
        MWSTTab[2] :=ReadDouble  ('MAIN\MWST','2',16);
        MWSTTab[3] :=ReadDouble  ('MAIN\MWST','3',0);

        // Default-Steuer und Code laden
        I :=ReadInteger ('MAIN\MWST','DEFAULT',2);
        if (i<0)or(i>3) then I :=2;
        DefMwStCD  :=I;
        DefMwSt    :=MWSTTab[I];

        // Globale Kalkulationsfaktoren
        for i:=1 to 5 do
        begin
           GCalcFaktorTab[i] :=
            ReadDouble ('MAIN\ARTIKEL','VK'+IntToStr(i)+'_CALC_FAKTOR',0)
        end;

        // Shop-Calc-Faktor
        GCalcFaktorTab[6] :=ReadDouble ('MAIN\ARTIKEL','SHOP_CALC_FAKTOR',0);


        // Brutto-Rundungswert f�r Artikel
        BR_RUND_WERT     :=ReadInteger ('MAIN\ARTIKEL', 'BRUTTO_RUNDUNG_WERT',0);
        if BR_RUND_WERT<1 then BR_RUND_WERT :=1; //mind 1 cent

        // Brutto-Summen-Rundungswert f�r Belege
        BR_SUM_RUND_WERT :=ReadInteger ('MAIN\BELEGE', 'BRUTTO_RUNDUNG_WERT',0);
        if BR_SUM_RUND_WERT<1 then
        begin
           BR_SUM_RUND_WERT :=1; //mind 1 cent
           DM1.WriteInteger ('MAIN\BELEGE','BRUTTO_RUNDUNG_WERT',BR_SUM_RUND_WERT);
        end;


        i :=ReadInteger ('MAIN','USE_KFZ',-1);
        if i=-1 then
        begin
           i:=0;
           WriteInteger ('MAIN','USE_KFZ',0);
        end;

        Use_KFZ := i=1;

        if USE_KFZ then
        begin
           S :=ReadString ('MAIN\KFZ','ATRIS_PFAD','@@@');
           if S='@@@' then WriteString ('MAIN\KFZ','ATRIS_PFAD','');

           if (length(S)>0)and(S<>'@@@')and(DirectoryExists(S)) then
           begin
             if (length(s)>0)and(s[length(s)]<>'\') then s:=s+'\';
             AtrisPfad :=S;
             AtrisEnable :=True;
           end
              else
           begin
             ini :=tinifile.create ('atris_st.ini');
             try
                s :=ini.ReadString ('INTERFACE','BESTINFO32','');
             finally
                ini.free;
             end;
             if length(s)>0 then
             begin
                s :=extractfilepath (s);
                if (length(s)>0)and(s[length(s)]<>'\') then s:=s+'\';
                if DirectoryExists(S) then
                begin
                   AtrisPfad :=s;
                   AtrisEnable :=True;
                end;
             end;
           end;
        end;

        // Default-Sprache laden
        DM1.SprachTab.Open;
        if DM1.SprachTab.RecordCount>0 then
        begin
           DefSpracheID  :=SprachTab.FieldByName('SPRACH_ID').AsInteger;
           DefSprachCode :=SprachTab.FieldByName('CODE').AsString;
        end;


        // User initialisieren

        UniQuery.Close;
        UniQuery.Sql.Text :='select * from MITARBEITER '+
                            'where UPPER(LOGIN_NAME)=UPPER("'+User+'")';
        UniQuery.RequestLive :=True;
        UniQuery.Open;

        if (UniQuery.RecordCount=1) then
        begin
           // Benutzer existiert
           View_User :=UniQuery.FieldByName('ANZEIGE_NAME').AsString;
           UserID    :=UniQuery.FieldByName('MA_ID').AsInteger;
        end
           else
        begin
           // Neuen Benutzer anlegen
           UniQuery.Append;
           try
             UniQuery.FieldByName('LOGIN_NAME').AsString   :=User;
             UniQuery.FieldByName('ANZEIGE_NAME').AsString :=User;
             UniQuery.Post;

             UserID :=UniQuery.FieldByName('MA_ID').AsInteger;
           except
             UniQuery.Cancel;
             UserID :=-1;
             View_User :=User;
           end;
        end;
        UniQuery.Close;
        UniQuery.RequestLive :=False;

        // Pfade laden
        BackupDir :=ReadString ('MAIN\PFADE','BACKUP_DIR',MainDir+'BACKUP\');
        TmpDir    :=ReadString ('MAIN\PFADE','TMP_DIR',MainDir+'TMP\');
        DTADir    :=ReadString ('MAIN\PFADE','DTA_DIR',MainDir+'DTA\');
        ExportDir :=ReadString ('MAIN\PFADE','EXPORT_DIR',MainDir+'EXPORT\');
        ImportDir :=ReadString ('MAIN\PFADE','IMPORT_DIR',MainDir+'IMPORT\');

        if (length (BackupDir)>0) and (Backupdir[length(BackupDir)]<>'\')
         then BackupDir :=BackupDir + '\';
        if (length (TmpDir)>0) and (TmpDir[length(TmpDir)]<>'\')
         then TmpDir :=TmpDir + '\';
        if (length (DTADir)>0) and (DTADir[length(DTADir)]<>'\')
         then DTADir :=DTADir + '\';
        if (length (ExportDir)>0) and (ExportDir[length(ExportDir)]<>'\')
         then ExportDir :=ExportDir + '\';
        if (length (ImportDir)>0) and (ImportDir[length(ImportDir)]<>'\')
         then ImportDir :=ImportDir + '\';

        // wenn Pfad nicht existiert dann anlegen
        try ForceDirectories (BackupDir); except end;
        try ForceDirectories (TmpDir);    except end;
        try ForceDirectories (DTADir);    except end;
        try ForceDirectories (ExportDir); except end;
        try ForceDirectories (ImportDir); except end;
     except
        MessageDlg ('Beim �ffnen der Tabellen ist ein Fehler aufgetreten !',mterror,[mbok],0);
     end;
end;

//------------------------------------------------------------------------------
// pr�ft mittels TryAndError die DB-Rechte des Benutzers ...
// zur�ckgeliefert wird ein Set der Rechte
// wenn es dumm l�uft und der User zwar CREATE-Rechte hat, aber keine DROP-Rechte,
// dann bleibt die Tabelle UTEST als Leiche zur�ck
//------------------------------------------------------------------------------
function TDM1.GetDBUserRechte (AktUser : Boolean; User,Secret : String): tSDBUserRechte;
var LastUser, LastUserSecret : String; Error : Boolean; S : String;
begin
     Result :=[];
     Error  :=False;
     if AktUser=False then
     begin
        // akt. User sichern
        LastUser       :=DB1.Login;
        LastUserSecret :=DB1.Password;

        DB1.Disconnect;
        DB1.Login    :=User;
        DB1.Password :=Secret;

        try
           DB1.Connect;
        except
           Error :=True;
        end;
     end;

     if not Error then
     begin
          // jetzt Rechte pr�fen

          UniQuery.Close;
          UniQuery.Sql.Text :='DROP TABLE IF EXISTS UCHECK ';
          try
             UniQuery.ExecSql;
          except

          end;
          UniQuery.Close;


          UniQuery.Sql.Text :='CREATE TABLE UCHECK (SPALTE1 VARCHAR(10) '+
                              'NOT NULL, PRIMARY KEY(SPALTE1))';
          try
             UniQuery.ExecSql;
             include (Result,urCreate);
          except

          end;
          UniQuery.Close;

          UniQuery.Sql.Text :='ALTER TABLE UCHECK CHANGE SPALTE1 SPALTE1 '+
                              'INT(10) DEFAULT "1" NOT NULL';
          try
             UniQuery.ExecSql;
             include (Result,urAlter);
          except

          end;
          UniQuery.Close;


          UniQuery.Sql.Text :='CREATE INDEX TEST ON UCHECK (SPALTE1)';
          try
             UniQuery.ExecSql;
             include (Result,urIndex);
          except

          end;
          UniQuery.Close;

          UniQuery.Sql.Text :='INSERT INTO UCHECK (SPALTE1) VALUES (5)';
          try
             UniQuery.ExecSql;
             include (Result,urInsert);
          except

          end;
          UniQuery.Close;

          UniQuery.Sql.Text :='UPDATE UCHECK SET SPALTE1=6 WHERE SPALTE1=5';
          try
             UniQuery.ExecSql;
             include (Result,urUpdate);
          except

          end;
          UniQuery.Close;

          UniQuery.Sql.Text :='SELECT SPALTE1 FROM UCHECK WHERE SPALTE1=6';
          try
             UniQuery.Open;
             if (UniQuery.Active)and
                (UniQuery.RecordCount=1)
              then include (Result,urSelect);
          except

          end;
          UniQuery.Close;

          UniQuery.Sql.Text :='DELETE FROM UCHECK WHERE SPALTE1=6';
          try
             UniQuery.ExecSql;
             include (Result,urDelete);
          except

          end;
          UniQuery.Close;

          UniQuery.Sql.Text :='DROP TABLE UCHECK';
          try
             UniQuery.ExecSql;
             include (Result,urDrop);
          except

          end;
          UniQuery.Close;
     end;

     if AktUser=False then
     begin
        DB1.Disconnect;
        DB1.Login    :=LastUser;
        DB1.Password :=LastUserSecret;

        try
           DB1.Connect;
        except
           Error :=True;
        end;
     end;

     { // nur zu Testzwecken
     S :='';
     if urSelect in Result then S :=S+'SELECT : JA'+#13 else S :=S+'SELECT : NEIN'+#13;
     if urInsert in Result then S :=S+'INSERT : JA'+#13 else S :=S+'INSERT : NEIN'+#13;
     if urUpdate in Result then S :=S+'UPDATE : JA'+#13 else S :=S+'UPDATE : NEIN'+#13;
     if urDelete in Result then S :=S+'DELETE : JA'+#13 else S :=S+'DELETE : NEIN'+#13;
     if urCreate in Result then S :=S+'CREATE : JA'+#13 else S :=S+'CREATE : NEIN'+#13;
     if urAlter  in Result then S :=S+'ALTER : JA'+#13 else S :=S+'ALTER : NEIN'+#13;
     if urDrop   in Result then S :=S+'DROP : JA'+#13 else S :=S+'DROP : NEIN'+#13;
     //if urIndex  in Result then S :=S+'INDEX : JA'+#13 else S :=S+'INDEX : NEIN'+#13;


     //MessageDlg ('Userrechte :'+#13#10+S,mtinformation, [mbok],0);

     if (urSelect in Result)and
        (urInsert in Result)and
        (urUpdate in Result)and
        (urDelete in Result)and
        (urCreate in Result)and
        (urAlter in Result)and
        (urDrop in Result) then
     begin
       MessageDlg ('Benutzer darf Update ausf�hren !',mtinformation,[mbok],0);
     end; }
end;
//------------------------------------------------------------------------------
function TDM1.UpdateDatabase (Data : tJvStrHolder; var Warnings, Errors : Integer): boolean;
var Idx : Integer;
    S,S1,S2 : String;
begin
     Warnings :=0;
     Errors :=0;
     try
       UniQuery.Close;
       UniQuery.Sql.Clear;
       S2 :='';

       For idx :=0 to Data.Strings.Count-1 do
       begin
            s :=Data.Strings[idx];
            S1 :=s;
            while (length(S1)>0)and(S1[length(S1)]=' ') do delete (S1,length(S1),1);
            if (pos(';',s1)>0)and(pos(';',s1)=length(s1)) then
            begin
                 delete (s,length(s1),1);
                 if length(S)>0 then S2 :=S2+S;
                 try
                    if (length(S)>0)and(S[1]=';') then  delete (s,1,1);
                    UniQuery.Sql.Text :=S2;
                    UniQuery.ExecSql;
                 except
                    inc(Warnings);
                 end;

                 UniQuery.Close;
                 UniQuery.Sql.Clear;
                 S2 :='';
            end
            else if length(S)>0 then S2 :=S2+S;
       end;
       Result :=True;
     except
       inc(Errors);
       Result :=False;
       UniQuery.Close;
       UniQuery.Sql.Clear;
     end;
end;
//------------------------------------------------------------------------------
function TDM1.IncNummer (Quelle : Integer) :Int64;
var F : Integer;
begin
     if (Quelle>10)and(Quelle<20) then Quelle :=10;
     NummerTab.Close;
     NummerTab.Sql.Clear;
     NummerTab.Sql.Add('select VAL_INT as QUELLE, VAL_CHAR as FORMAT,');
     NummerTab.Sql.Add('VAL_INT2 as NEXT_NUM, VAL_INT3 as MAXLEN, MAINKEY, NAME');
     NummerTab.Sql.Add('from REGISTERY');
     NummerTab.Sql.Add('where MAINKEY="MAIN\\NUMBERS"');
     NummerTab.Sql.Add('and VAL_INT=:ID');

     NummerTab.ParamByName ('ID').Value :=Quelle;
     NummerTab.Open;
     if NummerTab.RecordCount>0 then
     begin
       Result :=NummerTabNext_Num.AsLargeInt;
       NummerTab.Edit;
       try
          F :=Length(NummerTabFormat.AsString);
          NummerTabNext_Num.AsLargeInt :=NummerTabNext_Num.AsLargeInt+1;

          if length(NummerTabNext_Num.AsString)>F
           then NummerTabNext_Num.AsLargeInt :=1;

          NummerTab.Post;
       except
          NummerTab.Cancel;
       end;
     end
        else
     begin
       // Nummer existiert nicht
       InNewNummer :=True;
       try
         NummerTab.Append;
         try
             NummerTabQUELLE.Value     :=Quelle;
             NummerTabNEXT_NUM.Value   :=1;
             NummerTabFORMAT.Value     :='000000';
             NummerTabMainKey.Value    :='MAIN\NUMBERS';
             NummerTabMAXLEN.AsInteger :=6;
             NummerTabNAME.AsString    :=IntToStr(QUELLE);
             NummerTab.Post;
             Result :=1;
         except
            NummerTab.Cancel;
         end;
       finally
          InNewNummer :=False;
       end;
     end;
     NummerTab.Close;
end;
//------------------------------------------------------------------------------
function TDM1.IncNummerStr (Quelle : Integer) :String;
var Max : Integer; Num : Int64; Format : String;
begin
     if (Quelle>10)and(Quelle<20) then Quelle :=10;
     NummerTab.Close;
     NummerTab.Sql.Clear;
     NummerTab.Sql.Add('select VAL_INT as QUELLE, VAL_CHAR as FORMAT,');
     NummerTab.Sql.Add('VAL_INT2 as NEXT_NUM, VAL_INT3 as MAXLEN, MAINKEY, NAME');
     NummerTab.Sql.Add('from REGISTERY');
     NummerTab.Sql.Add('where MAINKEY="MAIN\\NUMBERS"');
     NummerTab.Sql.Add('and VAL_INT=:ID');

     NummerTab.ParamByName ('ID').Value :=Quelle;
     NummerTab.Open;
     if NummerTab.RecordCount>0 then
     begin
       Num :=NummerTabNext_Num.AsLargeInt;
       NummerTab.Edit;
       try
          Max    :=NummerTabMaxLen.AsInteger;
          Format :=NummerTabFormat.AsString;

          NummerTabNext_Num.AsLargeInt :=Num+1;

          if Length(IntToStr(Num))>Max then NummerTabNext_Num.AsLargeInt :=1;

          NummerTab.Post;
       except
          NummerTab.Cancel;
       end;
     end
        else
     begin
       // Nummer existiert nicht
       Format :='000000';
       Num    :=1;
       Max    :=6;

       InNewNummer :=True;
       try
         NummerTab.Append;
         try
            NummerTabQUELLE.Value     :=Quelle;
            NummerTabNEXT_NUM.Value   :=1;
            NummerTabFORMAT.Value     :=Format;
            NummerTabMainKey.Value    :='MAIN\NUMBERS';
            NummerTabMAXLEN.AsInteger :=Max;
            NummerTabNAME.AsString    :=IntToStr(QUELLE);
            NummerTab.Post;
         except
            NummerTab.Cancel;
         end;
       finally
          InNewNummer :=False;
       end;
     end;
     NummerTab.Close;

     Result :=FormatFloat (Format,Num);
end;
//------------------------------------------------------------------------------
function tDM1.GetNummerFormat (Quelle : Integer) : String;
begin
     if NummerTab.Active then NummerTab.Close;

     NummerTab.Sql.Clear;
     NummerTab.Sql.Add('select VAL_INT as QUELLE, VAL_CHAR as FORMAT,');
     NummerTab.Sql.Add('VAL_INT2 as NEXT_NUM, VAL_INT3 as MAXLEN, MAINKEY, NAME');
     NummerTab.Sql.Add('from REGISTERY');
     NummerTab.Sql.Add('where MAINKEY="MAIN\\NUMBERS"');
     NummerTab.Sql.Add('and VAL_INT=:ID');

     NummerTab.ParamByName ('ID').Value :=Quelle;
     NummerTab.Open;

     if NummerTab.RecordCount=0 then
     begin
        InNewNummer :=True;
        try
          NummerTab.Append;
          try
             NummerTabQUELLE.Value     :=Quelle;
             NummerTabNEXT_NUM.Value   :=1;
             NummerTabFORMAT.Value     :='000000';
             NummerTabMainKey.Value    :='MAIN\NUMBERS';
             NummerTabName.AsString    :=Inttostr(Quelle);
             NummerTab.Post;
          except
             NummerTab.Cancel;
          end;
        finally
          InNewNummer :=False;
        end;
     end;

     Result :=NummerTabFORMAT.AsString;

     NummerTab.Close;
end;
//------------------------------------------------------------------------------
procedure tDM1.ReadMandanten (App : String);
var ininame     : string;
    ini         : tinifile;
    po,idx,i    : integer;
    S,S1,PW,PWC : String;

begin
     ininame :=extractfilepath(paramstr(0))+'CAO32_DB.CFG';
     ini :=tinifile.create (ininame);
     try
        po :=1;
        while (po<101)and
              (ini.readstring('MANDANTEN',
                              'M'+formatfloat ('000',po),
                              '@ERROR@')<>'@ERROR@') do
        begin
           idx :=length(MandantTab);
           setlength (MandantTab,idx+1);

           MandantTab[Idx].Name          :=ini.readstring ('MANDANTEN','M'+formatfloat ('000',po),'');
           MandantTab[Idx].Server        :=ini.readstring ('MANDANTEN','M'+formatfloat ('000',po)+'_SERVER','');
           MandantTab[Idx].User          :=ini.readstring ('MANDANTEN','M'+formatfloat ('000',po)+'_USER','');
           PW                            :=ini.readstring ('MANDANTEN','M'+formatfloat ('000',po)+'_PASS','');
           PWC                           :=ini.readstring ('MANDANTEN','M'+formatfloat ('000',po)+'_PASS_C','');

           if (length(PW)>0) then
           begin
              Cipher.Decoded :=PW;
              PWC :=Cipher.Encoded;
              if length(PWC)>0 then
              begin
                 S1 :='';
                 For I:=1 to length(PWC) do S1 :=S1+IntToHex(Ord(PWC[i]),2);
                 ini.writestring ('MANDANTEN','M'+formatfloat ('000',po)+'_PASS_C',S1);
                 ini.writestring ('MANDANTEN','M'+formatfloat ('000',po)+'_PASS','');
              end;
           end
              else
           begin
             if length(PWC)>=2 then
             begin
               try
                  S1 :='';
                  for i:=1 to length(PWC) div 2
                   do S1 :=S1+CHR(StrToInt('$'+Copy(PWC,(I-1)*2+1,2)));
                  Cipher.Encoded :=S1;
                  PW :=Cipher.Decoded;
               except
                  PW :='';
               end;
             end;
           end;


           MandantTab[Idx].Pass          :=PW;


           MandantTab[Idx].DB            :=ini.readstring ('MANDANTEN','M'+formatfloat ('000',po)+'_DB','');
           //NEU
           MandantTab[Idx].Port          :=ini.readinteger('MANDANTEN','M'+formatfloat ('000',po)+'_PORT',3306);
           MandantTab[Idx].ShowLoginDlg  :=ini.ReadBool   ('MANDANTEN','M'+formatfloat ('000',po)+'_SHOW_LOGINDIALOG',False);
           MandantTab[Idx].UseNTUserName :=ini.ReadBool   ('MANDANTEN','M'+formatfloat ('000',po)+'_USE_NTUSERNAME',False);

           {$IFDEF AVE}
           MandantTab[Idx].UseSsh        :=ini.ReadBool   ('MANDANTEN','M'+formatfloat ('000',po)+'_USE_SSH',False);
           {$ENDIF}

           inc(po);
        end;

        AktMandant :=Ini.ReadString ('MANDANTEN',APP,'');
        if (AktMandant='')and(idx>0) then AktMandant :=MandantTab[0].Name;


        //PLZ- und BLZ-Verion lesen
        S :=Ini.ReadString ('VERSION','PLZ','1.06');
        if DecimalSeparator<>'.'
         then while (Pos('.',S)>0) do S[Pos('.',S)] :=DecimalSeparator;
        PLZ_VERSION :=CAO_Round(StrToFloat(S)*100);

        S :=Ini.ReadString ('VERSION','BLZ','1.06');
        if DecimalSeparator<>'.'
         then while (Pos('.',S)>0) do S[Pos('.',S)] :=DecimalSeparator;
        BLZ_VERSION :=CAO_round(StrToFloat(S)*100);

        //Kassen-Display DLL-Name ermitteln

        DisplayDLL :=Ini.ReadString ('DISPLAY','DLL_NAME','');

     finally
        ini.free;
     end;
end;
//------------------------------------------------------------------------------
procedure tDM1.SaveMandanten (App : String);
var ininame   : string;
    ini       : tinifile;
    idx,i     : integer;
    S1,PW,PWC : String;

begin
     if length(MandantTab)=0 then exit;

     ininame :=extractfilepath(paramstr(0))+'CAO32_DB.CFG';
     ini :=tinifile.create (ininame);
     try
        ini.EraseSection ('MANDANTEN');

        for Idx :=0 to length(MandantTab)-1 do
        begin
           ini.Writestring ('MANDANTEN','M'+formatfloat ('000',Idx+1),MandantTab[Idx].Name);
           ini.Writestring ('MANDANTEN','M'+formatfloat ('000',Idx+1)+'_SERVER',MandantTab[Idx].Server);
           ini.Writestring ('MANDANTEN','M'+formatfloat ('000',Idx+1)+'_USER',MandantTab[Idx].User);
           PW :=MandantTab[Idx].Pass;
           if (length(PW)>0) then
           begin
              Cipher.Decoded :=PW;
              PWC :=Cipher.Encoded;
              if length(PWC)>0 then
              begin
                 S1 :='';
                 For I:=1 to length(PWC) do S1 :=S1+IntToHex(Ord(PWC[i]),2);
                 ini.writestring ('MANDANTEN','M'+formatfloat ('000',IDX+1)+'_PASS_C',S1);
                 ini.writestring ('MANDANTEN','M'+formatfloat ('000',IDX+1)+'_PASS','');
              end;
           end;

           ini.Writestring ('MANDANTEN','M'+formatfloat ('000',Idx+1)+'_DB',MandantTab[Idx].DB);
           //NEU
           ini.Writeinteger('MANDANTEN','M'+formatfloat ('000',Idx+1)+'_PORT',MandantTab[Idx].Port);
           ini.WriteBool   ('MANDANTEN','M'+formatfloat ('000',Idx+1)+'_SHOW_LOGINDIALOG',False);
           ini.WriteBool   ('MANDANTEN','M'+formatfloat ('000',Idx+1)+'_USE_NTUSERNAME',False);
        end;
     finally
        ini.free;
     end;
end;
//------------------------------------------------------------------------------
function tDM1.GetMandant (Name : String; var Daten : MandantRec):Boolean;
var po : integer;
begin
     Result :=False;
     name :=uppercase(name);
     if length(MandantTab)>0 then
     for po:=0 to length(mandanttab)-1 do
     begin
          if uppercase(mandanttab[po].name) = name then
          begin
               Daten :=mandanttab[po];
               Result :=True;
               Break;
          end;
     end;
end;
//------------------------------------------------------------------------------
procedure tDM1.NewMandant (Daten : MandantRec);
var I : Integer;
begin
     I :=length (MandantTab);
     SetLength(MandantTab,I+1);
     MandantTab[i] :=Daten;
     SaveMandanten (extractfilepath(paramstr(0))+'CAO32_DB.CFG');
end;
//------------------------------------------------------------------------------
procedure tDM1.DeleteMandant (Name : String);
var I,J : Integer;
begin
     if length (MandantTab)=0 then exit;
     for I :=0 to length (MandantTab)-1 do
     begin
        if MandantTab[i].Name=Name then
        begin
          if I< length(MandantTab)-1 then
          begin
             For J :=I+1 to length(MandantTab)-1 do
             begin
               MandantTab[J-1] :=MandantTab[j];
             end;
          end;
          SetLength(MandantTab,length(MandantTab)-1);
        end;
     end;
     SaveMandanten (extractfilepath(paramstr(0))+'CAO32_DB.CFG');
end;
//------------------------------------------------------------------------------
function tDM1.ReadInteger (Key, Name : String; Default : Integer):Integer;
begin
     if not DB1.Connected then exit;

     RegTab.RequestLive :=False;
     try
       RegTab.Close;
       RegTab.ParamByName('KEY').AsString :=Key;
       RegTab.ParamByName('NAME').AsString :=Name;
       RegTab.Open;
       if RegTab.RecordCount>0 then Result :=RegTabVal_Int.AsInteger
                               else Result :=Default;
       RegTab.Close;
     finally
       RegTab.RequestLive :=True;
     end;
end;
//------------------------------------------------------------------------------
function tDM1.ReadIntegerU (Key, Name : String; Default : Integer):Integer;
begin
     Result :=0;
     if length (Key)>0 then Key :='USERSETTINGS\'+USER+'\'+KEY
                       else Key :='USERSETTINGS\'+USER;
     Result :=ReadInteger (Key,Name,Default);
end;
//------------------------------------------------------------------------------
procedure tDM1.WriteInteger (Key, Name : String; Value : Integer);
begin
     if not DB1.Connected then exit;

     RegTab.Close;
     RegTab.ParamByName('KEY').AsString :=Key;
     RegTab.ParamByName('NAME').AsString :=Name;
     RegTab.Open;
     if RegTab.RecordCount=0 then
     begin
        RegTab.Append;
        try
           RegTabMainKey.Value :=Key;
           RegTabName.Value :=Name;
           RegTabVal_Int.AsInteger :=Value;
           RegTabVal_Typ.AsInteger :=3;
           RegTab.Post;
        except
           RegTab.Cancel;
        end;
     end
        else
     begin
        RegTab.Edit;
        try
           RegTabVal_Int.AsInteger :=Value;
           RegTabVal_Typ.AsInteger :=3;
           RegTab.Post;
        except
           RegTab.Cancel;
        end;
     end;

     RegTab.Close;
end;
//------------------------------------------------------------------------------
procedure tDM1.WriteIntegerU (Key, Name : String; Value : Integer);
begin
     if length (Key)>0 then Key :='USERSETTINGS\'+USER+'\'+KEY
                       else Key :='USERSETTINGS\'+USER;
     WriteInteger (Key, Name, Value);
end;
//------------------------------------------------------------------------------
function tDM1.ReadBoolean (Key, Name : String; Default : Boolean):Boolean;
begin
     if not DB1.Connected then exit;

     RegTab.RequestLive :=False;
     try
       RegTab.Close;
       RegTab.ParamByName('KEY').AsString :=Key;
       RegTab.ParamByName('NAME').AsString :=Name;
       RegTab.Open;
       if RegTab.RecordCount>0 then Result :=RegTabVal_Int.AsInteger=1
                               else Result :=Default;
       RegTab.Close;
     finally
       RegTab.RequestLive :=True;
     end;
end;
//------------------------------------------------------------------------------
function tDM1.ReadBooleanU (Key, Name : String; Default : Boolean):Boolean;
begin
     Result :=False;
     if length (Key)>0 then Key :='USERSETTINGS\'+USER+'\'+KEY
                       else Key :='USERSETTINGS\'+USER;
     Result :=ReadBoolean (Key, Name, Default);
end;
//------------------------------------------------------------------------------
procedure tDM1.WriteBoolean (Key, Name : String; Value : Boolean);
begin
     if not DB1.Connected then exit;

     RegTab.Close;
     RegTab.ParamByName('KEY').AsString :=Key;
     RegTab.ParamByName('NAME').AsString :=Name;
     RegTab.Open;
     if RegTab.RecordCount=0 then
     begin
        RegTab.Append;
        try
           RegTabMainKey.Value :=Key;
           RegTabName.Value :=Name;
           RegTabVal_Int.AsInteger :=ord(Value);
           RegTabVal_Typ.AsInteger :=3;
           RegTab.Post;
        except
           RegTab.Cancel;
        end;
     end
        else
     begin
        RegTab.Edit;
        try
           RegTabVal_Int.AsInteger :=ord(Value);
           RegTabVal_Typ.AsInteger :=3;
           RegTab.Post;
        except
           RegTab.Cancel;
        end;
     end;

     RegTab.Close;
end;
//------------------------------------------------------------------------------
procedure tDM1.WriteBooleanU (Key, Name : String; Value : Boolean);
begin
     if length (Key)>0 then Key :='USERSETTINGS\'+USER+'\'+KEY
                       else Key :='USERSETTINGS\'+USER;
     WriteBoolean (Key, Name, Value);
end;
//------------------------------------------------------------------------------
function tDM1.ReadString (Key, Name, Default : String):String;
begin
     if not DB1.Connected then exit;

     RegTab.RequestLive :=False;
     try
       RegTab.Close;
       RegTab.ParamByName('KEY').AsString :=Key;
       RegTab.ParamByName('NAME').AsString :=Name;
       RegTab.Open;
       if RegTab.RecordCount>0 then Result :=RegTabVal_Char.AsString
                               else Result :=Default;
       RegTab.Close;
     finally
       RegTab.RequestLive :=True;
     end;
end;
//------------------------------------------------------------------------------
function tDM1.ReadStringU (Key, Name, Default : String):String;
begin
     Result :='';
     if length (Key)>0 then Key :='USERSETTINGS\'+USER+'\'+KEY
                       else Key :='USERSETTINGS\'+USER;
     Result :=ReadString (Key,Name, Default);
end;
//------------------------------------------------------------------------------
procedure tDM1.WriteString (Key, Name, Value : String);
begin
     if not DB1.Connected then exit;

     RegTab.Close;
     RegTab.ParamByName('KEY').AsString :=Key;
     RegTab.ParamByName('NAME').AsString :=Name;
     RegTab.Open;
     if RegTab.RecordCount=0 then
     begin
        RegTab.Append;
        try
           RegTabMainKey.Value :=Key;
           RegTabName.Value :=Name;
           RegTabVal_Char.AsString :=Value;
           RegTabVal_Typ.AsInteger :=1;
           RegTab.Post;
        except
           RegTab.Cancel;
        end;
     end
        else
     begin
        RegTab.Edit;
        try
           RegTabVal_Char.AsString :=Value;
           RegTabVal_Typ.AsInteger :=1;
           RegTab.Post;
        except
           RegTab.Cancel;
        end;
     end;

     RegTab.Close;
end;
//------------------------------------------------------------------------------
procedure tDM1.WriteStringU (Key, Name, Value : String);
begin
     if length (Key)>0 then Key :='USERSETTINGS\'+USER+'\'+KEY
                       else Key :='USERSETTINGS\'+USER;
     WriteString (Key, Name, Value);
end;
//------------------------------------------------------------------------------
function tDM1.ReadLongString (Key, Name, Default : String):String;
begin
     if not DB1.Connected then exit;

     RegTab.RequestLive :=False;
     try
       RegTab.Close;
       RegTab.ParamByName('KEY').AsString :=Key;
       RegTab.ParamByName('NAME').AsString :=Name;
       RegTab.Open;
       if RegTab.RecordCount>0 then Result :=RegTabVal_Blob.AsString
                               else Result :=Default;
       RegTab.Close;
     finally
       RegTab.RequestLive :=True;
     end;
end;
//------------------------------------------------------------------------------
function tDM1.ReadLongStringU (Key, Name, Default : String):String;
begin
     Result :='';
     if length (Key)>0 then Key :='USERSETTINGS\'+USER+'\'+KEY
                       else Key :='USERSETTINGS\'+USER;
     Result :=ReadLongString (Key,Name, Default);
end;
//------------------------------------------------------------------------------
procedure tDM1.WriteLongString (Key, Name, Value : String);
begin
     if not DB1.Connected then exit;

     RegTab.Close;
     RegTab.ParamByName('KEY').AsString :=Key;
     RegTab.ParamByName('NAME').AsString :=Name;
     RegTab.Open;
     if RegTab.RecordCount=0 then
     begin
        RegTab.Append;
        try
           RegTabMainKey.Value :=Key;
           RegTabName.Value :=Name;
           RegTabVal_Blob.AsString :=Value;
           RegTabVal_Typ.AsInteger :=5;
           RegTab.Post;
        except
           RegTab.Cancel;
        end;
     end
        else
     begin
        RegTab.Edit;
        try
           RegTabVal_Blob.AsString :=Value;
           RegTabVal_Typ.AsInteger :=5;
           RegTab.Post;
        except
           RegTab.Cancel;
        end;
     end;

     RegTab.Close;
end;
//------------------------------------------------------------------------------
procedure tDM1.WriteLongStringU (Key, Name, Value : String);
begin
     if length (Key)>0 then Key :='USERSETTINGS\'+USER+'\'+KEY
                       else Key :='USERSETTINGS\'+USER;
     WriteLongString (Key,Name, Value);
end;
//------------------------------------------------------------------------------
function tDM1.ReadDouble (Key, Name : String; Default : Double):Double;
begin
     if not DB1.Connected then exit;


     RegTab.RequestLive :=False;
     try
       RegTab.Close;
       RegTab.ParamByName('KEY').AsString :=Key;
       RegTab.ParamByName('NAME').AsString :=Name;
       RegTab.Open;
       if RegTab.RecordCount>0 then Result :=RegTabVal_Double.AsFloat
                               else Result :=Default;
       RegTab.Close;
     finally
       RegTab.RequestLive :=True;
     end;
end;
//------------------------------------------------------------------------------
function tDM1.ReadDoubleU (Key, Name : String; Default : Double):Double;
begin
     Result :=0;
     if length (Key)>0 then Key :='USERSETTINGS\'+USER+'\'+KEY
                       else Key :='USERSETTINGS\'+USER;
     Result :=ReadDouble (Key, Name, Default);
end;
//------------------------------------------------------------------------------
procedure tDM1.WriteDouble (Key, Name : String; Value : Double);
begin
     if not DB1.Connected then exit;

     RegTab.Close;
     RegTab.ParamByName('KEY').AsString :=Key;
     RegTab.ParamByName('NAME').AsString :=Name;
     RegTab.Open;
     if RegTab.RecordCount=0 then
     begin
        RegTab.Append;
        try
           RegTabMainKey.Value :=Key;
           RegTabName.Value :=Name;
           RegTabVal_Double.AsFloat :=Value;
           RegTabVal_Typ.AsInteger :=4;
           RegTab.Post;
        except
           RegTab.Cancel;
        end;
     end
        else
     begin
        RegTab.Edit;
        try
           RegTabVal_Double.AsFloat :=Value;
           RegTabVal_Typ.AsInteger :=4;
           RegTab.Post;
        except
           RegTab.Cancel;
        end;
     end;

     RegTab.Close;
end;
//------------------------------------------------------------------------------
procedure tDM1.WriteDoubleU (Key, Name : String; Value : Double);
begin
     if length (Key)>0 then Key :='USERSETTINGS\'+USER+'\'+KEY
                       else Key :='USERSETTINGS\'+USER;
     WriteDouble (Key, Name, Value);
end;
//------------------------------------------------------------------------------
function tDM1.ReadLayout (Key, Name : String; var Data : tStream; Version : Integer = 0) : Boolean;
begin
     if not DB1.Connected then exit;

     Result :=False;

     RegTab.RequestLive :=False;
     try
       RegTab.Close;
       RegTab.ParamByName('KEY').AsString :=Key;
       RegTab.ParamByName('NAME').AsString :=Name;
       RegTab.Open;
       if (RegTab.RecordCount>0) and
          (not RegTabVal_Bin.IsNull) then
       begin
            Data.Size :=0;
            Data.Position :=0;
            RegTabVal_Bin.SaveToStream (Data);
            Data.Position :=0;
            Result  :=RegTabVal_Int.AsInteger=Version;
       end;
       RegTab.Close;
     finally
       RegTab.RequestLive :=True;
     end;
end;
//------------------------------------------------------------------------------
procedure tDM1.WriteLayout (Key, Name : String; Data : tStream; Version : Integer = 0);
begin
     if not DB1.Connected then exit;

     RegTab.Close;
     RegTab.ParamByName('KEY').AsString :=Key;
     RegTab.ParamByName('NAME').AsString :=Name;
     RegTab.Open;
     if RegTab.RecordCount=0 then
     begin
        RegTab.Append;
        try
           RegTabMainKey.Value    :=Key;
           RegTabName.Value       :=Name;
           RegTabVAL_TYP.Value    :=7;
           RegTabVal_Int.AsInteger :=Version;
           Data.Position :=0;
           RegTabVAL_BIN.LoadFromStream (Data);

           RegTab.Post;
        except
           RegTab.Cancel;
        end;
     end
        else
     begin
        RegTab.Edit;
        try
           Data.Position :=0;
           RegTabVAL_BIN.LoadFromStream (Data);
           RegTabVAL_TYP.Value :=7;
           RegTabVal_INT.AsInteger :=Version;
           RegTab.Post;
        except
           RegTab.Cancel;
        end;
     end;

     RegTab.Close;
end;
//------------------------------------------------------------------------------
procedure tDM1.GridSaveLayout(Grid : tDBGrid; Sec : String; Version : Integer=0);
var M : tMemoryStream;
begin
     m :=tmemorystream.create;
     try
        Grid.Columns.SaveToStream (M);
        WriteLayout ('USERSETTINGS\'+User+'\LAYOUT',SEC,M, Version);
     finally
      M.Free;
     end;
end;
//------------------------------------------------------------------------------
procedure TDM1.GridLoadLayout(var Grid : tDBGrid; Sec : String; Version : Integer=0);
var M : tMemoryStream; I : Integer; Found : Boolean;
begin
     M :=tMemoryStream.Create;
     try
       try
        if ReadLayout ('USERSETTINGS\'+User+'\LAYOUT',SEC,tStream(M),Version)
         then Grid.Columns.LoadFromStream (M);
        {
        if Grid.Columns.Count>0 then
        begin
          repeat
            Found :=False;
            for i:=0 to Grid.Columns.Count-1 do
            begin
              if not assigned(Grid.Columns[i].Field) then
              begin
                Grid.Columns[i].Free;
                Found :=True;
                Break;
              end;
            end;
          until not Found;
        end;
        }
       except end;
     finally
       M.Free;
     end;
end;
//------------------------------------------------------------------------------
function tDM1.Buche_Rechnung (Journal_ID : Integer):Integer; // liefert Rechnungnummer zur�ck
var Pos      : Integer;
    IStr     : String;
begin
     Result :=-1;

     Transact1.AutoCommit :=False;
     //Transact1.TransactSafe :=True;
     try
       try
          JourTab.Close;
          JourTab.ParamByName ('ID').Value :=Journal_ID;
          JourTab.Open;
          JourTab.Edit;

          // neue Rechnungsnummer holen, aber nur wenn USE_SHOP_ORDERID=False

          if (not ReadBoolean ('SHOP','USE_SHOP_ORDERID',False)) or
             (JourTabSHOP_ID.AsInteger<1)
           then JourTabVRENUM.Value  :=IncNummer (VK_RECH);

          Result :=JourTabVRENUM.Value;

          JourTabSTADIUM.Value :=22;
          JourTabRDatum.Value  :=now;
          JourTabQuelle.Value  :=VK_RECH;
          JourTabQuelle_Sub.Value :=1; // Rechnung

          JourTabKONTOAUSZUG.Value :=-1;
          JourTabUW_NUM.Value :=-1;
          JourTabBANK_ID.Value :=-1;

          JourTabIST_Betrag.Value :=0;
          JourTabIST_Anzahlung.Value :=0;
          JourTabFreigabe1_Flag.Value :=False;
          JourTabMAHNKOSTEN.Value :=0;


          // Kundendaten (Zahlungsart und Lieferart) aktualisieren,
          // falls diese noch nicht zugewiesen sind
          KunTab.Close;
          KunTab.ParamByName ('ID').AsInteger :=JourTabAddr_ID.Value;
          KunTab.Open;

          if KunTab.RecordCount=1 then
          begin
             KunTab.Edit;
             if KunTabKun_Zahlart.AsInteger<0
              then KunTabKun_Zahlart.Value :=JourTabZahlart.Value;
             if KunTabKun_Liefart.AsInteger<0
              then KunTabKun_Liefart.Value :=JourTabLiefart.Value;
             KunTab.Post;
          end;

          KunTab.Close;

          case JourTabZahlart.Value of
               //bar bzw. scheck
               1,5:begin
                   if JourTabSOLL_SKONTO.Value>0 then
                   begin
                      JourTabStadium.Value :=80+JourTabZahlart.Value;
                      JourTabIST_SKONTO.Value :=JourTabSOLL_SKONTO.Value;
                      JourTabIST_ANZAHLUNG.Value :=0;
                      JourTabIST_ZAHLDAT.Value :=JourTabRDatum.Value;
                      JourTabIST_BETRAG.Value :=
                        JourTabBSumme.Value-(JourTabBSumme.Value / 100) *
                        JourTabSOLL_SKONTO.Value;
                   end
                      else
                   begin
                      JourTabStadium.Value :=90+JourTabZahlart.Value;
                      JourTabIST_SKONTO.Value :=0;
                      JourTabIST_ANZAHLUNG.Value :=0;
                      JourTabIST_ZAHLDAT.Value :=JourTabRDatum.Value;
                      JourTabIST_BETRAG.Value :=JourTabBSumme.Value;
                   end;
                 end;
               //�berweisung
               2,3,4:JourTabStadium.Value :=20+JourTabZahlart.Value;
               //Lastschrift, EC-Karte
               6,9  :JourTabStadium.Value :=20+JourTabZahlart.Value;
          end;

          // Wenn MWST-Freie Rechnung dann alle MWST-Felder l�schen
          if JourTabMWST_FREI_FLAG.AsBoolean then
          begin
             JourTabMWST_0.AsInteger :=0;
             JourTabMWST_1.AsInteger :=0;
             JourTabMWST_2.AsInteger :=0;
             JourTabMWST_3.AsInteger :=0;
             JourTabMSUMME_0.AsFloat :=0;
             JourTabMSUMME_1.AsFloat :=0;
             JourTabMSUMME_2.AsFloat :=0;
             JourTabMSUMME_3.AsFloat :=0;
             JourTabMSUMME.AsFloat   :=0;
             JourTabBSUMME.AsFloat   :=JourTabNSUMME.AsFloat;
          end;

          JourTab.Post;

          if JourTabZahlArt.Value = 1 then  // Kassenbuchung
          begin
               BucheKasse (JourTabIST_Zahldat.Value,
                           VK_RECH,JourTabRec_ID.Value,
                           Inttostr(JourTabVReNum.Value),
                           JourTabGegenKonto.Value,
                           JourTabIST_Skonto.Value,
                           JourTabIST_Betrag.Value,
                           'ZE VK-RE '+JourTabKun_Name1.Value);

          end;

          JPosTab.Close;
          JPosTab.ParamByName ('ID').Value :=Journal_ID;
          JPosTab.Open;

          Pos :=0;

          IStr :=''; // Insert-String f�r St�cklistenartikel leeren

          while not JPosTab.Eof do
          begin
               JPosTab.Edit;

               //MWST_Code l�schen, wenn MwSt-freier Beleg
               if JourTabMWST_FREI_FLAG.AsBoolean
                 then JPosTabSteuer_Code.Value :=0;

               // Position schreiben, aber nur bei Artikeln, nicht bei Text
               if JPosTabArtikelTyp.Value<>'T' then
               begin
                    Inc(Pos);
                    JposTabView_Pos.Value :=Inttostr(Pos);
               end;

               // Artikel Buchen
               if (JPosTabGebucht.Value=False)and
                  (JPosTabArtikelTyp.Value='N')and
                  (JPosTabARTIKEL_ID.Value>-1) then
               begin
                    // Menge erniedrigen
                    ArtMengeTab.Close;
                    ArtMengeTab.ParamByName ('ID').Value :=JPosTabARTIKEL_ID.Value;
                    ArtMengeTab.ParamByName ('SUBMENGE').Value :=JPosTabMenge.Value;

                    //Bestellmenge nicht ver�ndern
                    ArtMengeTab.ParamByName ('BMENGE').Value :=0;
                    ArtMengeTab.ExecSql;

                    JPosTabGebucht.Value :=True;
               end
                  else
               if (JPosTabGebucht.Value=False)and
                  (JPosTabArtikelTyp.Value='S')and
                  (JPosTabARTIKEL_ID.Value>-1) then
               begin
                    // St�ckliste abarbeiten

                    // gut w�hre es, die St�cklistenartikel versteckt mit in die Rechnung einzuf�gen,
                    // dann w�rde man den verkauf auch in der Historie der Artikel sehen,
                    // allerdings m��te es dann ein Flag Visible in der Postentabelle geben,
                    // damit die Berechnungen der Preise und der Rechnungsausdruck korrekt funktionieren !!!

                    STListTab.Close;
                    STListTab.ParamByName ('ID').ASInteger :=JPosTabARTIKEL_ID.Value;
                    STListTab.Open;


                    while not STListTab.Eof do
                    begin
                        // Menge erniedrigen
                        ArtMengeTab.Close;
                        ArtMengeTab.ParamByName ('ID').Value :=STListTabART_ID.Value;
                        ArtMengeTab.ParamByName ('SUBMENGE').Value :=JPosTabMenge.Value * STListTabMENGE.Value;
                        //Bestellmenge nicht ver�ndern
                        ArtMengeTab.ParamByName ('BMENGE').Value :=0;
                        ArtMengeTab.ExecSql;

                        if length(IStr)>0 then IStr :=IStr+';'+#13#10;

                        // Batch-SQL erzeugen und die St�cklistenartikel mit in die Rechnung zu speichern
                        // mit Artikeltyp="X"

                        dm1.uniquery.close;
                        dm1.uniquery.sql.text :='select MATCHCODE,ARTNUM,BARCODE,LAENGE,'+
                                                'GROESSE,DIMENSION,GEWICHT,ME_EINHEIT,'+
                                                'LANGNAME from ARTIKEL where REC_ID='+
                                                IntToStr(STListTabART_ID.Value);
                        dm1.uniquery.open;

                        IStr :=Istr+
                          'INSERT INTO JOURNALPOS SET '+
                          'QUELLE='+IntToStr(VK_RECH)+
                          ',QUELLE_SUB='+IntToStr(1)+
                          ',JOURNAL_ID='+IntToStr(JPosTabJOURNAL_ID.Value)+
                          ',ARTIKELTYP="X"'+
                          ',ARTIKEL_ID='+IntToStr(STListTabART_ID.Value)+
                          ',TOP_POS_ID='+IntToStr(JPosTabRec_ID.Value)+
                          ',ADDR_ID='+IntToStr(JPosTabADDR_ID.Value)+
                          ',VRENUM='+IntToStr(JourTabVRENUM.Value)+
                          ',MENGE="'+FloatToStrEx(JPosTabMenge.Value * STListTabMENGE.Value)+'"'+
                          ',POSITION='+IntToStr(JPosTabPOSITION.Value)+
                          ',MATCHCODE="'+ZSqlTypes.StringToSql(dm1.uniquery.fieldbyname ('MATCHCODE').AsString)+'"'+
                          ',ARTNUM="'+ZSqlTypes.StringToSql(dm1.uniquery.fieldbyname ('ARTNUM').AsString)+'"'+
                          ',BARCODE="'+ZSqlTypes.StringToSql(dm1.uniquery.fieldbyname ('BARCODE').AsString)+'"'+
                          ',LAENGE="'+ZSqlTypes.StringToSql(dm1.uniquery.fieldbyname ('LAENGE').AsString)+'"'+
                          ',GROESSE="'+ZSqlTypes.StringToSql(dm1.uniquery.fieldbyname ('GROESSE').AsString)+'"'+
                          ',DIMENSION="'+ZSqlTypes.StringToSql(dm1.uniquery.fieldbyname ('DIMENSION').AsString)+'"'+
                          ',GEWICHT='+FloatToStrEx(dm1.uniquery.fieldbyname ('GEWICHT').AsFloat)+
                          ',ME_EINHEIT="'+ZSqlTypes.StringToSql(dm1.uniquery.fieldbyname ('ME_EINHEIT').AsString)+'"'+
                          ',BEZEICHNUNG="'+ZSqlTypes.StringToSql(dm1.uniquery.fieldbyname ('LANGNAME').AsString)+'"';

                        dm1.uniquery.close;
                        STListTab.Next;
                    end;
                    STListTab.Close;

                    JPosTabGebucht.Value :=True;
               end;


               // Daten aktualisieren
               JPosTabVRENUM.Value :=JourTabVRENUM.Value;
               JPosTabQuelle.Value  :=VK_RECH;
               JPosTabQuelle_Sub.Value :=1; // Rechnung

               JPosTab.Post;

               // Lieferschein(e) aktualisieren
               if JPosTabVLSNum.Value>=0 then
               begin
                  uniquery.close;
                  uniquery.sql.clear;
                  uniquery.sql.add ('update JOURNAL');
                  uniquery.sql.add ('set STADIUM=90, VRENUM='+Inttostr(JPosTabVRENum.Value)+', RDATUM=:RDATUM');
                  uniquery.sql.add ('where QUELLE=2 and VLSNUM='+Inttostr(JPosTabVLSNum.Value));
                  uniquery.parambyname ('RDATUM').AsDateTime :=JourTabRDatum.Value;
                  uniquery.ExecSql;
                  uniquery.close;
                  uniquery.sql.clear;
               end;

               // im Artkikel die MENGE_VKRE_EDI aktualisieren
               UpdateArtikelEdiMenge (VK_RECH_EDI, JPosTabArtikel_ID.AsInteger,0);

               JPosTab.Next;
          end;


          // St�cklistenunterartikel hinzuf�gen
          if length(IStr)>0 then
          begin
             DM1.ZBatchSql1.Sql.Text :=ISTr;
             try
                DM1.ZBatchSql1.ExecSql;
             except
                MessageDlg ('Fehler beim hinzuf�gen der St�cklisten-Unterartikel.',
                            mterror,[mbok],0);
             end;
          end;

          if JourTabKFZ_ID.Value>=0 then
          begin
             ReKFZTab.Close;
             ReKFZTab.ParamByName ('KID').AsInteger :=JourTabKFZ_ID.Value;
             ReKFZTab.Open;
             if ReKFZTab.RecordCount = 1 then
             begin
                ReKFZTab.Edit;
                ReKFZTabLE_BESUCH.Value :=JourTabRDAtum.Value;
                ReKFZTabKM_STAND.Value :=JourTabKM_Stand.Value;

                try ReKFZTab.Post; except ReKFZTab.Cancel; end;
             end;
             ReKFZTab.Close;
          end;

          //Seriennumern aktualisieren
          try
             dm1.UniQuery.close;
             dm1.UniQuery.sql.text :='UPDATE ARTIKEL_SERNUM SET VERK_NUM='+
                                     IntToStr(JourTabVRENUM.AsInteger)+
                                     ' where VK_JOURNAL_ID='+
                                     IntToStr(JourTabRec_ID.AsInteger);
             dm1.UniQuery.ExecSql;
             dm1.UniQuery.close;
          except
             MessageDlg ('Fehler beim aktualisieren der Seriennummern.',mterror,[mbok],0);
          end;

          JPosTab.Close;
          JourTab.Close;
          Transact1.Commit;

       except
          MessageDlg ('Fehler beim Buchen der Rechnung !',mterror,[mbok],0);
          Transact1.RollBack;
          Result :=-1;
       end;
     finally
       Transact1.AutoCommit :=True;
       //Transact1.TransactSafe :=False;
     end;
end;
//------------------------------------------------------------------------------
function tDM1.Buche_BCKasse (Journal_ID : Integer):Integer; // liefert BON-Nummer zur�ck
var Ja,Mo,Ta : Word;
    Pos : Integer;
begin
     Result :=-1;
     DecodeDate (Now, Ja, Mo, Ta);
     Transact1.AutoCommit :=False;
     //Transact1.TransactSafe :=True;
     try
       try
          JourTab.Close;
          JourTab.ParamByName ('ID').Value :=Journal_ID;
          JourTab.Open;
          JourTab.Edit;
          // neue Rechnungsnummer holen
          JourTabVRENUM.Value  :=IncNummer ({VK_KASSE}22);

          Result :=JourTabVRENUM.Value;

          JourTabSTADIUM.Value :=22;
          JourTabRDatum.Value  :=now;
          JourTabQuelle.Value  :=VK_RECH;
          JourTabQuelle_Sub.Value :=2; // Kasse

          JourTabKONTOAUSZUG.Value :=-1;
          JourTabUW_NUM.Value :=-1;
          JourTabBANK_ID.Value :=-1;
          JourTabFreigabe1_Flag.Value :=False;

          case JourTabZahlart.Value of
             //bar bzw. scheck oder ec-karte
             1,5,6:begin
                 if JourTabSOLL_SKONTO.Value>0 then
                 begin
                    JourTabStadium.Value :=80+JourTabZahlart.Value;
                    JourTabIST_SKONTO.Value :=JourTabSOLL_SKONTO.Value;
                    JourTabIST_ANZAHLUNG.Value :=0;
                    JourTabIST_ZAHLDAT.Value :=JourTabRDatum.Value;
                    JourTabIST_BETRAG.Value :=
                      JourTabBSumme.Value-(JourTabBSumme.Value / 100) *
                      JourTabSOLL_SKONTO.Value;
                 end
                    else
                 begin
                    JourTabStadium.Value :=90+JourTabZahlart.Value;
                    JourTabIST_SKONTO.Value :=0;
                    JourTabIST_ANZAHLUNG.Value :=0;
                    JourTabIST_ZAHLDAT.Value :=JourTabRDatum.Value;
                    JourTabIST_BETRAG.Value :=JourTabBSumme.Value;
                 end;
               end;
          end;

          // Wenn MWST-Freier Kassenbeleg dann alle MWST-Felder l�schen
          if JourTabMWST_FREI_FLAG.AsBoolean then
          begin
             JourTabMWST_0.AsInteger :=0;
             JourTabMWST_1.AsInteger :=0;
             JourTabMWST_2.AsInteger :=0;
             JourTabMWST_3.AsInteger :=0;
             JourTabMSUMME_0.AsFloat :=0;
             JourTabMSUMME_1.AsFloat :=0;
             JourTabMSUMME_2.AsFloat :=0;
             JourTabMSUMME_3.AsFloat :=0;
             JourTabMSUMME.AsFloat   :=0;
             JourTabBSUMME.AsFloat   :=JourTabNSUMME.AsFloat;
          end;

          JourTab.Post;

      {    if JourTabZahlArt.Value = 1 then  // Kassenbuchung
          begin
               BucheKasse (JourTabIST_Zahldat.Value,
                           VK_RECH,JourTabRec_ID.Value,
                           Inttostr(JourTabVReNum.Value),
                           JourTabGegenKonto.Value,
                           JourTabIST_Skonto.Value,
                           JourTabIST_Betrag.Value,
                           'ZE VK-RE '+JourTabKun_Name1.Value);

          end; }

          JPosTab.Close;
          JPosTab.ParamByName ('ID').Value :=Journal_ID;
          JPosTab.Open;

          Pos :=0;

          while not JPosTab.Eof do
          begin
             JPosTab.Edit;

             //MWST_Code l�schen, wenn MwSt-freier Beleg
             if JourTabMWST_FREI_FLAG.AsBoolean
               then JPosTabSteuer_Code.Value :=0;

             // Position schreiben, aber nur bei Artikeln, nicht bei Text
             if JPosTabArtikelTyp.Value<>'T' then
             begin
                Inc(Pos);
                JposTabView_Pos.Value :=Inttostr(Pos);
             end;



             // Artikel Buchen
             if (JPosTabGebucht.Value=False)and
                (JPosTabArtikelTyp.Value='N')and
                (JPosTabARTIKEL_ID.Value>-1) then
             begin
                // Menge erniedrigen
                ArtMengeTab.Close;
                ArtMengeTab.ParamByName ('ID').Value :=JPosTabARTIKEL_ID.Value;
                ArtMengeTab.ParamByName ('SUBMENGE').Value :=JPosTabMenge.Value;
                //Bestellmenge nicht ver�ndern
                ArtMengeTab.ParamByName ('BMENGE').Value :=0;
                ArtMengeTab.ExecSql;
                JPosTabGebucht.Value :=True;
             end
                else
             if (JPosTabGebucht.Value=False)and
                (JPosTabArtikelTyp.Value='S')and
                (JPosTabARTIKEL_ID.Value>-1) then
             begin
                // St�ckliste abarbeiten
                STListTab.Close;
                STListTab.ParamByName ('ID').ASInteger :=JPosTabARTIKEL_ID.Value;
                STListTab.Open;

                while not STListTab.Eof do
                begin
                   // Menge erniedrigen
                   ArtMengeTab.Close;
                   ArtMengeTab.ParamByName ('ID').Value :=STListTabART_ID.Value;
                   ArtMengeTab.ParamByName ('SUBMENGE').Value :=JPosTabMenge.Value * STListTabMENGE.Value;
                   //Bestellmenge nicht ver�ndern
                   ArtMengeTab.ParamByName ('BMENGE').Value :=0;
                   ArtMengeTab.ExecSql;

                   STListTab.Next;
                end;
                STListTab.Close;

                JPosTabGebucht.Value :=True;
             end;


             // Daten aktualisieren
             //JPosTabJahr.Value :=Ja;
             JPosTabVRENUM.Value :=JourTabVRENUM.Value;
             JPosTabQuelle.Value  :=VK_RECH;
             JPosTabQuelle_Sub.Value :=2; // Kasse

             JPosTab.Post;

             // im Artkikel die MENGE_VKRE_EDI aktualisieren
             UpdateArtikelEdiMenge (VK_RECH_EDI, JPosTabArtikel_ID.AsInteger,0);


             JPosTab.Next;
          end;

          //Seriennumern aktualisieren
          try
             dm1.UniQuery.close;
             dm1.UniQuery.sql.text :='UPDATE ARTIKEL_SERNUM SET VERK_NUM='+
                                     IntToStr(JourTabVRENUM.AsInteger)+
                                     ' where VK_JOURNAL_ID='+
                                     IntToStr(JourTabRec_ID.AsInteger);
             dm1.UniQuery.ExecSql;
             dm1.UniQuery.close;
          except
             MessageDlg ('Fehler beim aktualisieren der Seriennummern.',mterror,[mbok],0);
          end;

          JPosTab.Close;
          JourTab.Close;
          Transact1.Commit;

       except
          MessageDlg ('Fehler beim Buchen der Rechnung !',mterror,[mbok],0);
          Transact1.RollBack;
          Result :=-1;
       end;
     finally
       Transact1.AutoCommit :=True;
       //Transact1.TransactSafe :=False;
     end;
end;
//------------------------------------------------------------------------------
function tDM1.Buche_Angebot (Journal_ID : Integer):Integer; // liefert AGB-Nummer zur�ck
var //Ja,Mo,Ta : Word;
    Pos : Integer;
begin
     Result :=-1;
     //DecodeDate (Now, Ja, Mo, Ta);
     Transact1.AutoCommit :=False;
     //Transact1.TransactSafe :=True;
     try
       try
          JourTab.Close;
          JourTab.ParamByName ('ID').Value :=Journal_ID;
          JourTab.Open;
          JourTab.Edit;
          // neue Nummer holen
          JourTabVRENUM.Value  :=IncNummer (VK_AGB);

          Result :=JourTabVRENUM.Value;

          JourTabSTADIUM.Value :=0;
          //JourTabJahr.Value    :=ja;
          JourTabRDatum.Value  :=now;
          JourTabQuelle.Value  :=VK_AGB;

          JourTabKONTOAUSZUG.Value :=-1;
          JourTabUW_NUM.Value :=-1;
          JourTabBANK_ID.Value :=-1;

          JourTabIST_Betrag.Value :=0;
          JourTabIST_Anzahlung.Value :=0;
          JourTabFreigabe1_Flag.Value :=False;

          // Kundendaten (Zahlungsart und Lieferart) aktualisieren,
          // falls diese noch nicht zugewiesen sind
          KunTab.Close;
          KunTab.ParamByName ('ID').AsInteger :=JourTabAddr_ID.Value;
          KunTab.Open;
          if KunTab.RecordCount=1 then
          begin
             KunTab.Edit;
             if KunTabKun_Zahlart.AsInteger<0
               then KunTabKun_Zahlart.Value :=JourTabZahlart.Value;
             if KunTabKun_Liefart.AsInteger<0
               then KunTabKun_Liefart.Value :=JourTabLiefart.Value;
             KunTab.Post;
          end;

          KunTab.Close;


          // Wenn MWST-Freies Angebot dann alle MWST-Felder l�schen
          if JourTabMWST_FREI_FLAG.AsBoolean then
          begin
             JourTabMWST_0.AsInteger :=0;
             JourTabMWST_1.AsInteger :=0;
             JourTabMWST_2.AsInteger :=0;
             JourTabMWST_3.AsInteger :=0;
             JourTabMSUMME_0.AsFloat :=0;
             JourTabMSUMME_1.AsFloat :=0;
             JourTabMSUMME_2.AsFloat :=0;
             JourTabMSUMME_3.AsFloat :=0;
             JourTabMSUMME.AsFloat   :=0;
             JourTabBSUMME.AsFloat   :=JourTabNSUMME.AsFloat;
          end;


          JourTab.Post;

          JPosTab.Close;
          JPosTab.ParamByName ('ID').Value :=Journal_ID;
          JPosTab.Open;

          Pos :=0;

          while not JPosTab.Eof do
          begin
             JPosTab.Edit;

             //MWST_Code l�schen, wenn MwSt-freier Beleg
             if JourTabMWST_FREI_FLAG.AsBoolean
               then JPosTabSteuer_Code.Value :=0;

             // Position schreiben, aber nur bei Artikeln, nicht bei Text
             if JPosTabArtikelTyp.Value<>'T' then
             begin
                Inc(Pos);
                JposTabView_Pos.Value :=Inttostr(Pos);
             end;

             // Daten aktualisieren
             //JPosTabJahr.Value :=Ja;
             JPosTabVRENUM.Value :=JourTabVRENUM.Value;
             JPosTabQuelle.Value  :=VK_AGB;

             JPosTab.Post;
             JPosTab.Next;
          end;

          JPosTab.Close;
          JourTab.Close;
          Transact1.Commit;

       except
          MessageDlg ('Fehler beim Buchen des Angebotes !',mterror,[mbok],0);
          Transact1.RollBack;
          Result :=-1;
       end;
     finally
       Transact1.AutoCommit :=True;
       //Transact1.TransactSafe :=False;
     end;
end;
//------------------------------------------------------------------------------
function tDM1.Buche_EKBest (Journal_ID : Integer):Integer; // liefert EK-BST-Nummer zur�ck
var Pos      : Integer;
    NewEK    : Double;
begin
     Result :=-1;
     Transact1.AutoCommit :=False;
     //Transact1.TransactSafe :=True;
     try
       try
          JourTab.Close;
          JourTab.ParamByName ('ID').Value :=Journal_ID;
          JourTab.Open;
          JourTab.Edit;
          // neue Nummer holen
          JourTabVRENUM.Value  :=IncNummer (EK_BEST);

          Result :=JourTabVRENUM.Value;

          JourTabSTADIUM.Value :=0;
          //JourTabJahr.Value    :=ja;
          JourTabRDatum.Value  :=now;
          JourTabQuelle.Value  :=EK_BEST;

          JourTabKONTOAUSZUG.Value :=-1;
          JourTabUW_NUM.Value :=-1;
          JourTabBANK_ID.Value :=-1;

          JourTabIST_Betrag.Value :=0;
          JourTabIST_Anzahlung.Value :=0;
          JourTabFreigabe1_Flag.Value :=False;

          JourTabStadium.AsInteger :=20;


          // Wenn MWST-Freie Bestellung dann alle MWST-Felder l�schen
          if JourTabMWST_FREI_FLAG.AsBoolean then
          begin
             JourTabMWST_0.AsInteger :=0;
             JourTabMWST_1.AsInteger :=0;
             JourTabMWST_2.AsInteger :=0;
             JourTabMWST_3.AsInteger :=0;
             JourTabMSUMME_0.AsFloat :=0;
             JourTabMSUMME_1.AsFloat :=0;
             JourTabMSUMME_2.AsFloat :=0;
             JourTabMSUMME_3.AsFloat :=0;
             JourTabMSUMME.AsFloat   :=0;
             JourTabBSUMME.AsFloat   :=JourTabNSUMME.AsFloat;
          end;


          // Lieferantendaten (Zahlungsart) aktualisieren,
          // falls diese noch nicht zugewiesen ist
          KunTab.Close;
          KunTab.ParamByName ('ID').AsInteger :=JourTabAddr_ID.Value;
          KunTab.Open;
          if KunTab.RecordCount=1 then
          begin
             KunTab.Edit;
             if KunTabKrd_Num.Value<1 then
             begin
               // Wenn noch keine KRD-NUM, dann neue Nummer zuweisen
               KunTabKrd_Num.Value :=IncNummer (KRD_NUM_KEY);
               JourTabGEGENKONTO.Value :=KunTabKrd_Num.Value;
               // Bitcodiertes Flag f�r "ist Lieferant" setzen
               KunTabSTATUS.AsInteger :=KunTabSTATUS.AsInteger or 16;
             end;
             if KunTabLief_Zahlart.AsInteger<0
              then KunTabLief_Zahlart.Value :=JourTabZahlart.Value;
             KunTab.Post;
          end;


          JourTab.Post;

          JPosTab.Close;
          JPosTab.ParamByName ('ID').Value :=Journal_ID;
          JPosTab.Open;

          Pos :=0;

          while not JPosTab.Eof do
          begin
             JPosTab.Edit;

             //MWST_Code l�schen, wenn MwSt-freier Beleg
             if JourTabMWST_FREI_FLAG.AsBoolean
               then JPosTabSteuer_Code.Value :=0;

             // Position schreiben, aber nur bei Artikeln, nicht bei Text
             if JPosTabArtikelTyp.Value<>'T' then
             begin
                  Inc(Pos);
                  JposTabView_Pos.Value :=Inttostr(Pos);
             end;

             // Artikel Buchen  (Bestellmenge  erh�hen)
             if (JPosTabArtikelTyp.Value='N')and
                (JPosTabARTIKEL_ID.Value>-1) then
             begin
                // Menge nicht ver�ndern !!!
                ArtMengeTab.Close;
                ArtMengeTab.ParamByName ('ID').Value :=JPosTabARTIKEL_ID.Value;
                ArtMengeTab.ParamByName ('SUBMENGE').Value :=0;
                //Bestellmenge erh�hen
                ArtMengeTab.ParamByName ('BMENGE').Value :=JPosTabMenge.Value*-1; // Menge wird abgezogen, daher - ( - * - = +)
                ArtMengeTab.ExecSql;
                JPosTabGebucht.Value :=True;

                // Lieferanten im Artikelstamm aktualisieren

                NewEK    :=JPosTabEPREIS.AsFloat;

                if JPosTabRabatt.Value<>0 then
                begin
                  if JPosTabRabatt.Value = 100
                   then NewEK :=0
                   else NewEK :=NewEK - (NewEK * JPosTabRabatt.Value / 100);
                end;


                //Lieferantenpreis erstellen bzw. aktualisieren
                UpdateArtikelPreis (EK_RECH,
                                    JPosTabARTIKEL_ID.Value,
                                    JourTabAddr_ID.Value,
                                    NewEK);
             end;

             // Daten aktualisieren
             JPosTabVRENUM.Value :=JourTabVRENUM.Value;
             JPosTabQuelle.Value  :=EK_BEST;

             JPosTab.Post;

             // im Artkikel die MENGE_VKRE_EDI aktualisieren
             UpdateArtikelEdiMenge (EK_BEST_EDI, JPosTabArtikel_ID.AsInteger,0);

             JPosTab.Next;
          end;

          JPosTab.Close;
          JourTab.Close;
          Transact1.Commit;

       except
          MessageDlg ('Fehler beim Buchen der EK-Bestellung !',mterror,[mbok],0);
          Transact1.RollBack;
          Result :=-1;
       end;
     finally
       Transact1.AutoCommit :=True;
       //Transact1.TransactSafe :=False;
     end;
end;
//------------------------------------------------------------------------------
function tDM1.Buche_Einkauf (Journal_ID : Integer):Integer; // liefert Belegnummer zur�ck
var NewEK,
    NewMenge,
    OldEK,
    OldMenge,
    BM,ABM,
    Faktor,
    FaktorWGR,
    N,B       : Double;

    UseRabGrp : Boolean;
    STA,OFF,
    SUM, I    : Integer;
    LastID    : Integer;
begin
     Result :=-1;
     UseRabGrp :=False;
     Transact1.AutoCommit :=False;
     //Transact1.TransactSafe :=True;
     try
       try
          JourTab.Close;
          JourTab.ParamByName ('ID').Value :=Journal_ID;
          JourTab.Open;
          JourTab.Edit;
          // neue Rechnungsnummer holen
          JourTabVRENUM.Value  :=IncNummer (EK_RECH);

          Result :=JourTabVRENUM.Value;

          JourTabIST_Betrag.Value :=0;
          JourTabIST_Anzahlung.Value :=0;
          JourTabMAHNKOSTEN.Value :=0;
          JourTabFreigabe1_Flag.Value :=False;


          // Lieferantendaten (Zahlungsart) aktualisieren,
          // falls diese noch nicht zugewiesen ist
          KunTab.Close;
          KunTab.ParamByName ('ID').AsInteger :=JourTabAddr_ID.Value;
          KunTab.Open;
          if KunTab.RecordCount=1 then
          begin
             KunTab.Edit;
             if KunTabKrd_Num.Value<1 then
             begin
               // Wenn noch keine KRD-NUM, dann neue Nummer zuweisen
               KunTabKrd_Num.Value :=IncNummer (KRD_NUM_KEY);
               JourTabGEGENKONTO.Value :=KunTabKrd_Num.Value;
               // Bitcodiertes Flag f�r "ist Lieferant" setzen
               KunTabSTATUS.AsInteger :=KunTabSTATUS.AsInteger or 16;
             end;
             if KunTabLief_Zahlart.AsInteger<0
              then KunTabLief_Zahlart.Value :=JourTabZahlart.Value;
             KunTab.Post;
          end;


          case JourTabZahlart.Value of
             //bar bzw. scheck
             1,5:begin
                 if JourTabSOLL_SKONTO.Value>0 then
                 begin
                    JourTabStadium.Value :=80+JourTabZahlart.Value;
                    JourTabIST_SKONTO.Value :=JourTabSOLL_SKONTO.Value;
                    JourTabIST_ANZAHLUNG.Value :=0;
                    JourTabIST_ZAHLDAT.Value :=JourTabRDatum.Value;
                    JourTabIST_BETRAG.Value :=
                      JourTabBSumme.Value-(JourTabBSumme.Value / 100)*JourTabSOLL_SKONTO.Value;
                 end
                    else
                 begin
                    JourTabStadium.Value :=90+JourTabZahlart.Value;
                    JourTabIST_SKONTO.Value :=0;
                    JourTabIST_ANZAHLUNG.Value :=0;
                    JourTabIST_ZAHLDAT.Value :=JourTabRDatum.Value;
                    JourTabIST_BETRAG.Value :=JourTabBSumme.Value;
                 end;
               end;
             //�berweisung, NN, Lastschrift, EC-Karte
             2,3,4,6,9:JourTabStadium.Value :=20+JourTabZahlart.Value;
          end;

          JourTabQuelle.Value         :=EK_RECH;
          JourTabKONTOAUSZUG.Value    :=-1;
          JourTabUW_NUM.Value         :=-1;
          JourTabBANK_ID.Value        :=-1;
          JourTabFREIGABE1_Flag.Value :=False;

          // Wenn MWST-Freier Einkauf dann alle MWST-Felder l�schen
          if JourTabMWST_FREI_FLAG.AsBoolean then
          begin
             JourTabMWST_0.AsInteger :=0;
             JourTabMWST_1.AsInteger :=0;
             JourTabMWST_2.AsInteger :=0;
             JourTabMWST_3.AsInteger :=0;
             JourTabMSUMME_0.AsFloat :=0;
             JourTabMSUMME_1.AsFloat :=0;
             JourTabMSUMME_2.AsFloat :=0;
             JourTabMSUMME_3.AsFloat :=0;
             JourTabMSUMME.AsFloat   :=0;
             JourTabBSUMME.AsFloat   :=JourTabNSUMME.AsFloat;
          end;

          JourTab.Post;


          if JourTabZahlArt.Value = 1 then  // Kassenbuchung
          begin
             BucheKasse (JourTabIST_Zahldat.Value,
                         EK_RECH,
                         JourTabRec_ID.Value,
                         Inttostr(JourTabVReNum.Value),
                         JourTabGegenKonto.Value,
                         JourTabIST_Skonto.Value,
                         JourTabIST_Betrag.Value*-1, // negativ, da Ausgabe !!!
                         'ZA EK-RE '+JourTabKun_Name1.Value);
          end;

          JPosTab.Close;
          JPosTab.ParamByName ('ID').Value :=Journal_ID;
          JPosTab.Open;

          while not JPosTab.Eof do
          begin
             try
               JPosTab.Edit;

               //MWST_Code l�schen, wenn MwSt-freier Beleg
               if JourTabMWST_FREI_FLAG.AsBoolean
                 then JPosTabSteuer_Code.Value :=0;

               // EK-Preis, letzter EK, letzt. Lief, Lief-Datum
               // im Artikelstam aktualisieren !!!
               UpdateArtTab.Close;
               UpdateArtTab.ParamByName ('ID').AsInteger :=JPosTabARTIKEL_ID.Value;
               UpdateArtTab.Open;
               if UpdateArtTab.RecordCount=1 then
               begin
                 //UseRabGrp :=length(UpdateArtTab.FieldByName ('RABGRP_ID').AsString)>0;
                 UseRabGrp :=(length(UpdateArtTab.FieldByName('RABGRP_ID').AsString)>0)and
                             (UpdateArtTab.FieldByName ('RABGRP_ID').AsString<>'-');


                 try
                   UpdateArtTab.Edit;

                   OldEK    :=UpdateArtTab.FieldByName ('EK_PREIS').AsFloat;
                   OldMenge :=UpdateArtTab.FieldByName ('MENGE_AKT').AsFloat;

                   NewEK    :=JPosTabEPREIS.AsFloat;
                   NewMenge :=JPosTabMenge.AsFloat;

                   if JPosTabRabatt.Value<>0 then
                   begin
                      if JPosTabRabatt.Value = 100 then NewEK :=0 else
                      NewEK :=NewEK - (NewEK * JPosTabRabatt.Value / 100);
                   end;

                   NewMenge :=CAO_round(JPosTabMenge.AsFloat * 100)/100; // auf 2 Nachkommastellen


                   UpdateArtTab.FieldByName ('LAST_EK').AsFloat  :=CAO_round(NewEK*1000)/1000; // auf 3 Nachkommastellen
                   UpdateArtTab.FieldByName ('LAST_LIEF').AsInteger :=JourTabAddr_ID.Value;
                   UpdateArtTab.FieldByName ('LAST_LIEFDAT').AsDateTime :=JourTabRDatum.AsDateTime;


                   // Lieferantenpreis anlegen bzw. aktualisieren
                   UpdateArtikelPreis (EK_RECH,
                                       JPosTabARTIKEL_ID.Value,
                                       JourTabAddr_ID.Value,
                                       NewEK);

                   // EK Berechnen (Mittelwert)
                   if (OldMenge+NewMenge<>0)and(OldMenge>=0)
                     then NewEK :=(NewEK * NewMenge + OldEK * OldMenge) / (OldMenge+NewMenge);

                   // nur Speichern, wenn keine Rabattgruppe gesetzt
                   if not UseRabGrp then
                   begin
                       UpdateArtTab.FieldByName ('EK_PREIS').AsFloat :=CAO_round(NewEK*1000)/1000; // auf 3 Nachkommastellen


                       // Berechnung von VK-Preisen wenn Kalkulationsfaktoren verwendet werden
                       for i:=1 to AnzPreis do
                       begin
                          Faktor :=GCalcFaktorTab[i];
                          FaktorWgr :=0;
                          GetWGRCalcFaktor(UpdateArtTab.FieldByName ('WARENGRUPPE').AsInteger,i,FaktorWgr);
                          if FaktorWgr <> 0 then Faktor :=FaktorWgr;

                          if (Faktor>0) then
                          begin
                             N :=CAO_round(NewEK * Faktor * 100) / 100;

                             UpdateArtTab.FieldByName ('VK'+IntToStr(i)).AsFloat :=N;

                             B :=N * (100+MwStTab[UpdateArtTab.FieldByName ('STEUER_CODE').AsInteger]);

                             B :=CAO_Round (B / BR_RUND_WERT) * BR_RUND_WERT / 100;

                             UpdateArtTab.FieldByName ('VK'+IntToStr(i)+'B').AsFloat :=B
                          end;
                       end;
                   end;


                   //Bestelmenge aus Artikel holen
                   ABM :=UpdateArtTab.FieldByName ('MENGE_BESTELLT').AsFloat;

                   UpdateArtTab.Post;
                 except
                   UpdateArtTab.Cancel;
                   MessageDlg ('Fehler beim aktualisieren der Artikel-Mengen.',mterror,[mbok],0);
                 end;
               end
               else ABM :=999999;

               UpdateArtTab.Close;

               // Artikel Buchen
               if (JPosTabGebucht.Value=False)and
                  (JPosTabArtikelTyp.Value='N')and
                  (JPosTabARTIKEL_ID.Value>-1) then
               begin
                  if JPosTabQUELLE_SRC.AsInteger>0 then
                  begin

                    // Bestellmenge aktualisieren
                    BM :=JPosTabMenge.AsFloat;

                    //Nachschauen, ob die Menge gr��er als die Best.-Menge ist und ggf. die Menge vermindern
                    uniquery.close;
                    uniquery.sql.text :='select MENGE from JOURNALPOS where QUELLE=6 and REC_ID='+IntToStr(JPosTabQUELLE_SRC.AsInteger);
                    uniquery.open;

                    if (UniQuery.RecordCount=1) then
                    begin
                       if BM > UniQuery.FieldByName('MENGE').AsFloat
                        then BM :=UniQuery.FieldByName('MENGE').AsFloat;
                    end else BM :=0;

                    uniquery.close;

                    // Artikel-Bestellmenge darf nicht < 0 sein !!!
                    if ABM-BM<0 then BM :=ABM;

                  end else BM :=0; // war kein Artikel aus einer Bestellung


                  // Lager-Menge erh�hen
                  ArtMengeTab.Close;
                  ArtMengeTab.ParamByName ('ID').Value :=JPosTabARTIKEL_ID.Value;
                  ArtMengeTab.ParamByName ('SUBMENGE').Value :=(CAO_round(JPosTabMenge.Value*100)/100)*-1;
                  //Bestellmenge erniedrigen
                  ArtMengeTab.ParamByName ('BMENGE').Value :=BM; // Menge wird abgezogen
                  ArtMengeTab.ExecSql;
                  JPosTabGebucht.Value :=True;
               end;


               // Daten aktualisieren
               JPosTabVRENUM.Value :=JourTabVRENUM.Value;
               JPosTabQuelle.Value  :=EK_RECH;

               JPosTab.Post;
             except
               JPosTab.Cancel;
               MessageDlg ('Fehler beim aktualisieren der EK-Positionen',mterror,[mbok],0);
             end;
             //Seriennumern aktualisieren
             try
               dm1.UniQuery.close;
               dm1.UniQuery.sql.text :='UPDATE ARTIKEL_SERNUM SET EINK_NUM='+
                                       IntToStr(JourTabVRENUM.AsInteger)+
                                       ' where EK_JOURNALPOS_ID='+
                                       IntToStr(JPosTabRec_ID.AsInteger);
               dm1.UniQuery.ExecSql;
               dm1.UniQuery.close;
             except
               MessageDlg ('Fehler beim aktualisieren der Seriennummern.',mterror,[mbok],0);
             end;

             // im Artkikel die MENGE_VKRE_EDI aktualisieren
             UpdateArtikelEdiMenge (EK_RECH_EDI, JPosTabArtikel_ID.AsInteger,0);

             JPosTab.Next;
          end;

          // Status der Bestellungen dieses Lieferanten aktualisieren
          uniquery.close;
          uniquery.sql.clear;
          uniquery.sql.add ('select');
          uniquery.sql.add ('JOURNAL.REC_ID,SUM(JP2.MENGE) as ');
          uniquery.sql.add ('MENGE_EK,JOURNALPOS.MENGE as MENGE_BEST');
          uniquery.sql.add ('from JOURNALPOS, JOURNAL');
          uniquery.sql.add ('left outer join JOURNALPOS as JP2 on ');
          uniquery.sql.add ('JP2.QUELLE_SRC = JOURNALPOS.REC_ID and JP2.QUELLE<>15');
          uniquery.sql.add ('where JOURNAL.QUELLE=6 and JOURNALPOS.QUELLE=6 and ');
          uniquery.sql.add ('JOURNALPOS.ADDR_ID='+Inttostr(JourTabAddr_ID.AsInteger));
          uniquery.sql.add ('and (JOURNALPOS.ARTIKELTYP="N" or ');
          uniquery.sql.add ('JOURNALPOS.ARTIKELTYP="S") and ');
          uniquery.sql.add ('JOURNAL.REC_ID=JOURNALPOS.JOURNAL_ID and ');
          uniquery.sql.add ('JOURNAL.STADIUM>=20 and JOURNAL.STADIUM<100');
          uniquery.sql.add ('group by JOURNALPOS.REC_ID');

          LastID :=-1;
          try
             uniquery.open;

             while not uniquery.eof do
             begin
                if LastID <> UniQuery.FieldByName ('REC_ID').AsInteger then
                begin
                  if LastID<>-1 then
                  begin
                    if off=0 then STA :=100 else
                    if off=sum then STA :=20 else STA :=30;

                    uniquery2.close;
                    uniquery2.sql.text :='UPDATE JOURNAL SET STADIUM='+
                                         Inttostr(STA)+
                                         ' WHERE QUELLE=6 and REC_ID='+
                                         Inttostr(LastID);
                    uniquery2.execsql;
                  end;
                  sum :=0;
                  off :=0;
                  LastID :=UniQuery.FieldByName ('REC_ID').AsInteger;
                end;
                sum :=sum + UniQuery.FieldByName ('MENGE_BEST').AsInteger;
                off :=off + (UniQuery.FieldByName ('MENGE_BEST').AsInteger -
                             UniQuery.FieldByName ('MENGE_EK').AsInteger);

                uniquery.next;
             end;
             if LastID<>-1 then
             begin
                if off<1 then STA :=100 else
                 if off=sum then STA :=20 else STA :=30;

                uniquery2.close;
                uniquery2.sql.text :='UPDATE JOURNAL SET STADIUM='+
                                     Inttostr(STA)+
                                     ' WHERE QUELLE=6 and REC_ID='+
                                     Inttostr(LastID);
                uniquery2.execsql;
             end;
          except
             MessageDlg ('Fehler beim aktualisieren der EK-Bestellungen.',mterror,[mbok],0);
          end;
          uniquery.close;

          JPosTab.Close;
          JourTab.Close;
          Transact1.Commit;

       except
          MessageDlg ('Fehler beim Buchen der EK-Rechnung !',mterror,[mbok],0);
          Transact1.RollBack;
          Result :=-1;
       end;
     finally
       Transact1.AutoCommit :=True;
       //Transact1.TransactSafe :=False;
     end;
end;
//------------------------------------------------------------------------------
// Nur zur Probe aus der EDI-Rechnung einen Lieferschein erstellen !
function tDM1.Buche_Lieferschein (Journal_ID : Integer;
                                  Teillief : Boolean;
                                  var LieferscheinID : Integer):Integer; // liefert Belegnummer zur�ck
var NewId    : Integer;
    Pos      : Integer;
begin
     if not TeilLief then NewID :=CopyRechnung (Journal_ID, VK_LIEF_EDI)
                     else NewID :=Journal_ID;

     LieferscheinID :=NewID;

     Result :=-1;
     Transact1.AutoCommit :=False;
     //Transact1.TransactSafe :=True;
     try
       try
          JourTab.Close;
          JourTab.ParamByName ('ID').Value :=NewID;
          JourTab.Open;
          JourTab.Edit;

          // Nachschauen, ob Kunde eine Lieferanschrift hat
          KunTab.Close;
          KunTab.ParamByName ('ID').AsInteger :=JourTabAddr_ID.Value;
          KunTab.Open;

          // neu 22.08.2003
          // Liefersnschrift zuweisen
          with DM1.UniQuery do
          begin
             SQL.Text :='select * from ADRESSEN_LIEF where REC_ID='+
                        IntToStr(JourTabLIEF_ADDR_ID.AsInteger);
             Open;
             if RecordCount>0 then
             begin
                JourTabKUN_Anrede.Text  :=FieldByName ('ANREDE').AsString;
                JourTabKUN_NAME1.Text   :=FieldByName ('NAME1').AsString;
                JourTabKUN_NAME2.Text   :=FieldByName ('NAME2').AsString;
                JourTabKUN_NAME3.Text   :=FieldByName ('NAME3').AsString;
                JourTabKUN_Strasse.Text :=FieldByName ('STRASSE').AsString;
                JourTabKUN_LAND.Text    :=FieldByName ('LAND').AsString;
                JourTabKUN_PLZ.Text     :=FieldByName ('PLZ').AsString;
                JourTabKUN_Ort.Text     :=FieldByName ('ORT').AsString;
             end
          end;

          // neue Belegnummer holen
          JourTabVLSNUM.Value  :=IncNummer (VK_LIEF);
          JourTabVRENUM.Value  :=-1;
          JourTabRDatum.Value  :=0;
          JourTabFreigabe1_Flag.Value :=False;

          Result :=JourTabVLSNUM.Value;

          JourTabLDatum.Value :=Now;
          JourTabStadium.Value :=20+JourTabLiefart.Value;

          JourTabQuelle.Value      :=VK_LIEF;
          JourTabKONTOAUSZUG.Value :=-1;
          JourTabUW_NUM.Value      :=-1;
          JourTabBANK_ID.Value     :=-1;

          // Warenwert erst mal auf Nettosumme !!!
          // Mu� noch ge�ndert werden !!!
          JourTabWERT_NETTO.Value :=JourTabNSumme.Value;

          JourTab.Post;


          // Lieferscheinnummer in die Seriennummern eintragen
          UniQuery.Sql.Text :=
             'UPDATE ARTIKEL_SERNUM SET LIEF_NUM='+
             Inttostr(JourTabVLSNUM.AsInteger)+
             ' WHERE LS_JOURNAL_ID='+
             IntToStr(NewID);
          UniQuery.ExecSql;


          JPosTab.Close;
          JPosTab.ParamByName ('ID').Value :=NewID;
          JPosTab.Open;

          Pos :=0;

          while not JPosTab.Eof do
          begin
             JPosTab.Edit;

             // Position schreiben, aber nur bei Artikeln, nicht bei Text
             if JPosTabArtikelTyp.Value<>'T' then
             begin
                  Inc(Pos);
                  JposTabView_Pos.Value :=Inttostr(Pos);
             end;

             // Daten aktualisieren
             JPosTabVLSNUM.Value  :=JourTabVLSNUM.Value;
             JPosTabVRENUM.Value  :=JourTabVRENUM.Value;
             JPosTabQuelle.Value  :=VK_LIEF;

             JPosTab.Post;
             JPosTab.Next;
          end;

          JPosTab.Close;
          JourTab.Close;
          Transact1.Commit;

       except
          MessageDlg ('Fehler beim Buchen des VK-Lieferscheines !',mterror,[mbok],0);
          Transact1.RollBack;
          Result :=-1;
       end;
     finally
       Transact1.AutoCommit :=True;
       //Transact1.TransactSafe :=False;
     end;
end;
//------------------------------------------------------------------------------
function tDM1.Storno_Einkauf (Journal_ID : Integer):Boolean; // True, Wenn OK
begin
     Result :=False;
     try
        JourTab.Close;
        JourTab.ParamByName ('ID').Value :=Journal_ID;
        JourTab.Open;

        if JourTabZahlArt.Value = 1 then  // Kassenbuchung stornieren
        begin
           with UniQuery do
           begin
              Close;
              SQL.Clear;
              SQL.Add ('delete from FIBU_KASSE');
              SQL.Add ('where JOURNAL_ID='+Inttostr(Journal_ID));
              ExecSql;
              SQL.Clear;
           end;
        end;

        // ggf. Seriennummer freigeben
        with UniQuery do
        begin
           Close;
           SQL.Clear;
           SQL.Add ('update ARTIKEL_SERNUM set EK_JOURNAL_ID=-1, ');
           SQL.Add ('EK_JOURNALPOS_ID=-1 where EK_JOURNAL_ID='+Inttostr(Journal_ID));
           ExecSql;
           SQL.Clear;
        end;


        JPosTab.Close;
        JPosTab.ParamByName ('ID').Value :=Journal_ID;
        JPosTab.Open;

        while not JPosTab.Eof do
        begin
           // Artikel Buchen
           if (JPosTabArtikelTyp.Value='N')and
              (JPosTabARTIKEL_ID.Value>-1) then
           begin
              // Menge erniedrugen (EK STORNO)
              ArtMengeTab.Close;
              ArtMengeTab.ParamByName ('ID').Value :=JPosTabARTIKEL_ID.Value;
              ArtMengeTab.ParamByName ('SUBMENGE').Value :=JPosTabMenge.Value;
              ArtMengeTab.ParamByName ('BMENGE').Value :=0;
              ArtMengeTab.ExecSql;
              JPosTabGebucht.Value :=True;
           end;

           // Daten aktualisieren
           JPosTab.Delete;
        end;
        JPosTab.Close;

        JourTab.Delete;
        JourTab.Close;

        //Datei-Links l�schen
        LinkForm.DelLinks (EK_RECH, JOURNAL_ID);

        Result :=True;
     except
        MessageDlg ('Fehler beim Storno der EK-Rechnung !',mterror,[mbok],0);
        Result :=False;
     end;
end;
//------------------------------------------------------------------------------
function tDM1.Storno_Verkauf (Journal_ID : Integer):Boolean; // True, Wenn OK
begin
     Result :=False;
     try
        JourTab.Close;
        JourTab.ParamByName ('ID').Value :=Journal_ID;
        JourTab.Open;

        if JourTabZahlArt.Value = 1 then  // Kassenbuchung stornieren
        begin
           with UniQuery do
           begin
              Close;
              SQL.Clear;
              SQL.Add ('delete from FIBU_KASSE');
              SQL.Add ('where JOURNAL_ID='+Inttostr(Journal_ID));
              ExecSql;
              SQL.Clear;
           end;
        end;

        // ggf. Seriennummer freigeben
        with UniQuery do
        begin
           Close;
           SQL.Clear;
           SQL.Add ('update ARTIKEL_SERNUM set VK_JOURNAL_ID=-1, ');
           SQL.Add ('VK_JOURNALPOS_ID=-1 where VK_JOURNAL_ID='+Inttostr(Journal_ID));
           ExecSql;
           SQL.Clear;
        end;

        JPosTab.Close;
        JPosTab.ParamByName ('ID').Value :=Journal_ID;
        JPosTab.Open;

        while not JPosTab.Eof do
        begin
           // Artikel Buchen
           if (JPosTabArtikelTyp.Value='N')and
              (JPosTabARTIKEL_ID.Value>-1) then
           begin
              // Menge erh�hen (VK STORNO)
              ArtMengeTab.Close;
              ArtMengeTab.ParamByName ('ID').Value :=JPosTabARTIKEL_ID.Value;
              ArtMengeTab.ParamByName ('SUBMENGE').Value :=JPosTabMenge.Value*-1;
              ArtMengeTab.ParamByName ('BMENGE').Value :=0;
              ArtMengeTab.ExecSql;
              //JPosTabGebucht.Value :=True;
           end
              else
           if (JPosTabArtikelTyp.Value='S')and
              (JPosTabARTIKEL_ID.Value>-1) then
           begin
              // St�ckliste, Unterartikel Menge korregieren
              STListTab.Close;
              STListTab.ParamByName('ID').AsInteger :=JPosTabARTIKEL_ID.Value;
              STListTab.Open;
              while not STListTab.Eof do
              begin
                 // Menge erh�hen (VK STORNO)
                 ArtMengeTab.Close;
                 ArtMengeTab.ParamByName ('ID').Value :=STListTabART_ID.AsInteger;
                 ArtMengeTab.ParamByName ('SUBMENGE').Value :=STListTabMENGE.Value*JPosTabMenge.Value*-1;
                 ArtMengeTab.ParamByName ('BMENGE').Value :=0;
                 ArtMengeTab.ExecSql;

                 STListTab.Next;
              end;
              STListTab.Close;
           end;


           // Verweise l�schen
           UniQuery.Sql.Text :='UPDATE JOURNALPOS SET QUELLE_SRC=-1 '+
                               'where QUELLE_SRC='+
                               Inttostr(JPosTabRec_ID.AsInteger);
           UniQuery.ExecSql;


           // Daten aktualisieren
           JPosTab.Delete;
        end;
        JPosTab.Close;

        JourTab.Delete;
        JourTab.Close;

        //Datei-Links l�schen
        LinkForm.DelLinks (VK_RECH, JOURNAL_ID);

        Result :=True;
     except
        MessageDlg ('Fehler beim Storno der VK-Rechnung !',mterror,[mbok],0);
        Result :=False;
     end;
end;
//------------------------------------------------------------------------------
function tDM1.Storno_Angebot (Journal_ID : Integer):Boolean; // True, Wenn OK
begin
     Result :=False;
     try
        JourTab.Close;
        JourTab.ParamByName ('ID').Value :=Journal_ID;
        JourTab.Open;

        JPosTab.Close;
        JPosTab.ParamByName ('ID').Value :=Journal_ID;
        JPosTab.Open;

        while not JPosTab.Eof do JPosTab.Delete;
        JPosTab.Close;

        JourTab.Delete;
        JourTab.Close;

        //Datei-Links l�schen
        LinkForm.DelLinks (VK_AGB, JOURNAL_ID);

        Result :=True;
     except
        MessageDlg ('Fehler beim Storno des Angebotes !',mterror,[mbok],0);
        Result :=False;
     end;
end;
//------------------------------------------------------------------------------
function tDM1.Storno_EKBestellung (Journal_ID : Integer):Boolean; // True, Wenn OK
begin
     Result :=False;
     try
        JPosTab.Close;
        JPosTab.ParamByName ('ID').Value :=Journal_ID;
        JPosTab.Open;

        while not JPosTab.Eof do
        begin
           // Bestellmenge aktualsieren
           if (JPosTabArtikelTyp.Value='N')and
              (JPosTabARTIKEL_ID.Value>-1) then
           begin
              // Menge nicht ver�ndern !!!
              ArtMengeTab.Close;
              ArtMengeTab.ParamByName ('ID').Value :=JPosTabARTIKEL_ID.Value;
              ArtMengeTab.ParamByName ('SUBMENGE').Value :=0;
              //Bestellmenge erniedrigen
              ArtMengeTab.ParamByName ('BMENGE').Value :=JPosTabMenge.Value; // Menge wird abgezogen
              ArtMengeTab.ExecSql;
           end;


           // Position l�schen
           JPosTab.Delete;
        end;
        JPosTab.Close;

        JourTab.Close;
        JourTab.ParamByName ('ID').AsInteger :=Journal_ID;
        JourTab.Open;
        JourTab.Delete;
        JourTab.Close;

        //Datei-Links l�schen
        LinkForm.DelLinks (EK_BEST, JOURNAL_ID);

        Result :=True;
     except
        MessageDlg ('Fehler beim Storno der EK-Bestellung !',mterror,[mbok],0);
        Result :=False;
     end;
end;
//------------------------------------------------------------------------------
function tDM1.Storno_Lieferschein (Journal_ID : Integer):Boolean; // True, Wenn OK
begin
     Result :=False;
     try
        JourTab.Close;
        JourTab.ParamByName ('ID').Value :=Journal_ID;
        JourTab.Open;

        // ggf. Seriennummer freigeben
        with UniQuery do
        begin
           Close;
           SQL.Clear;
           SQL.Add ('update ARTIKEL_SERNUM set ');
           SQL.Add ('LS_JOURNAL_ID=-1, ');
           SQL.Add ('LS_JOURNALPOS_ID=-1, ');
           SQL.Add ('LIEF_NUM=-1 ');
           SQL.Add ('where LS_JOURNAL_ID='+Inttostr(Journal_ID));
           ExecSql;
           SQL.Clear;
        end;

        JPosTab.Close;
        JPosTab.ParamByName ('ID').Value :=Journal_ID;
        JPosTab.Open;

        while not JPosTab.Eof do JPosTab.Delete;
        JPosTab.Close;

        JourTab.Delete;
        JourTab.Close;

        //Datei-Links l�schen
        LinkForm.DelLinks (VK_LIEF, JOURNAL_ID);

        Result :=True;
     except
        MessageDlg ('Fehler beim Storno des Lieferscheins !',mterror,[mbok],0);
        Result :=False;
     end;
end;
//------------------------------------------------------------------------------
function tDM1.CalcLeitWaehrung (Betrag : Double; Waehrung : String) : Double;
var Kurs : Double;
begin
     if Waehrung=Leitwaehrung then
     begin
        Result :=Betrag;
        exit;
     end;
     if CacheLastWaehrung<>Waehrung then
     begin
       Kurs :=ReadDouble ('MAIN\WAEHRUNG',Waehrung,1);
       CacheLastKurs     :=Kurs;
       CacheLastWaehrung :=Waehrung;
     end else Kurs :=CacheLastKurs;

     if Kurs=0
      then Result :=0
      else Result :=(CAO_Round(Betrag / Kurs * 100))/100;
end;
//------------------------------------------------------------------------------
function tDM1.CopyRechnung (Journal_ID, Dest :Integer) : Integer;  // Liefert Rec-ID zur�ck
var id,i : integer;
    num  : integer;
    w    : string;
    N,M,m0,m1,m2,m3,B : Double;
    Summe, NSumme,MSumme,BSumme, Steuer, EK, Lohn, Ware, TKst : Double;
    T : Char;

begin
     Result :=-1;

     N :=0;
     M :=0; M0 :=0; M1 :=0; M2 :=0; M3 :=0;
     B :=0;
     Lohn :=0; Ware :=0; TKst :=0;

     CpySrcKopfTab.Close;
     CpySrcKopfTab.ParamByName ('ID').Value :=Journal_ID;
     CpySrcKopfTab.Open;

     if CpySrcKopfTab.RecordCount=0 then
     begin
        CpySrcKopfTab.Close;
        Exit;
     end;

     NUM :=IncNummer (Dest);

     CpyDstKopfTab.Open;
     CpyDstKopfTab.Append;

     w :=CpySrcKopfTabWAEHRUNG.Value;

     CpyDstKopfTabQUELLE.Value            :=Dest; //CpySrcKopfTabQUELLE.Value;
     CpyDstKopfTabQUELLE_SUB.Value        :=0;//CpySrcKopfTabQUELLE_SUB.Value;

     CpyDstKopfTabATRNUM.Value :=-1;
     CpyDstKopfTabLief_Addr_ID.Value :=CpySrcKopfTabLief_Addr_ID.Value;

     CpyDstKopfTabVRENUM.Value            :=num;
     CpyDstKopfTabVLSNUM.Value            :=-1;
     CpyDstKopfTabFOLGENR.Value           :=-1;
     CpyDstKopfTabKM_STAND.Value          :=-1;
     CpyDstKopfTabADATUM.Value            :=0;
     CpyDstKopfTabRDATUM.Value            :=Now;

     if Dest<>EK_BEST_EDI then CpyDstKopfTabLDATUM.Value :=0
                          else CpyDstKopfTabLDATUM.Value :=CpySrcKopfTabLDATUM.Value;

     CpyDstKopfTabTermin.Value            :=0;

     CpyDstKopfTabKOST_NETTO.Value        :=Calcleitwaehrung(CpySrcKopfTabKOST_NETTO.AsFloat,w);
     CpyDstKopfTabWERT_NETTO.Value        :=CalcLeitWaehrung(CpySrcKopfTabWERT_NETTO.AsFloat,W);
     CpyDstKopfTabLOHN.Value              :=CalcLeitWaehrung(CpySrcKopfTabLOHN.AsFloat,W);
     CpyDstKopfTabWARE.Value              :=CalcLeitWaehrung(CpySrcKopfTabWARE.AsFloat,W);
     CpyDstKopfTabTKOST.Value             :=CalcLeitWaehrung(CpySrcKopfTabTKOST.AsFloat,W);

     CpyDstKopfTabADDR_ID.Value           :=CpySrcKopfTabADDR_ID.Value;
     CpyDstKopfTabKFZ_ID.Value            :=CpySrcKopfTabKFZ_ID.Value;
     CpyDstKopfTabVERTRETER_ID.Value      :=CpySrcKopfTabVERTRETER_ID.Value;
     CpyDstKopfTabGLOBRABATT.Value        :=CpySrcKopfTabGLOBRABATT.Value;

     CpyDstKopfTabPR_EBENE.Value          :=CpySrcKopfTabPR_EBENE.Value;
     CpyDstKopfTabLIEFART.Value           :=CpySrcKopfTabLIEFART.Value;
     CpyDstKopfTabZAHLART.Value           :=CpySrcKopfTabZAHLART.Value;




     CpyDstKopfTabMWST_0.Value            :=CpySrcKopfTabMWST_0.Value;
     CpyDstKopfTabMWST_1.Value            :=CpySrcKopfTabMWST_1.Value;
     CpyDstKopfTabMWST_2.Value            :=CpySrcKopfTabMWST_2.Value;
     CpyDstKopfTabMWST_3.Value            :=CpySrcKopfTabMWST_3.Value;
     CpyDstKopfTabMWST_FREI_FLAG.Value    :=CpySrcKopfTabMWST_FREI_FLAG.Value;

     CpyDstKopfTabNSUMME.Value            :=CalcLeitWaehrung(CpySrcKopfTabNSUMME.AsFloat,W);
     CpyDstKopfTabMSUMME_0.Value          :=CalcLeitWaehrung(CpySrcKopfTabMSUMME_0.AsFloat,W);
     CpyDstKopfTabMSUMME_1.Value          :=CalcLeitWaehrung(CpySrcKopfTabMSUMME_1.AsFloat,W);
     CpyDstKopfTabMSUMME_2.Value          :=CalcLeitWaehrung(CpySrcKopfTabMSUMME_2.AsFloat,W);
     CpyDstKopfTabMSUMME_3.Value          :=CalcLeitWaehrung(CpySrcKopfTabMSUMME_3.AsFloat,W);
     CpyDstKopfTabMSUMME.Value            :=CalcLeitWaehrung(CpySrcKopfTabMSUMME.AsFloat,W);
     CpyDstKopfTabBSUMME.Value            :=CalcLeitWaehrung(CpySrcKopfTabBSUMME.AsFloat,W);
     CpyDstKopfTabATSUMME.Value           :=CalcLeitWaehrung(CpySrcKopfTabATSUMME.AsFloat,W);
     CpyDstKopfTabATMSUMME.Value          :=CalcLeitWaehrung(CpySrcKopfTabATMSUMME.AsFloat,W);

     CpyDstKopfTabWAEHRUNG.Value          :=LeitWaehrung;//CpySrcKopfTabWAEHRUNG.Value;
     CpyDstKopfTabGEGENKONTO.Value        :=CpySrcKopfTabGEGENKONTO.Value;
     CpyDstKopfTabSOLL_STAGE.Value        :=CpySrcKopfTabSOLL_STAGE.Value;
     CpyDstKopfTabSOLL_SKONTO.Value       :=CpySrcKopfTabSOLL_SKONTO.Value;
     CpyDstKopfTabSOLL_NTAGE.Value        :=CpySrcKopfTabSOLL_NTAGE.Value;
     CpyDstKopfTabSOLL_RATEN.Value        :=CpySrcKopfTabSOLL_RATEN.Value;

     CpyDstKopfTabSOLL_RATBETR.Value      :=CalcLeitWaehrung(CpySrcKopfTabSOLL_RATBETR.AsFloat,W);

     CpyDstKopfTabSOLL_RATINTERVALL.Value :=CpySrcKopfTabSOLL_RATINTERVALL.Value;

     CpyDstKopfTabIST_ANZAHLUNG.Value     :=0;
     CpyDstKopfTabIST_ZAHLDAT.Value       :=0;
     CpyDstKopfTabMAHNKOSTEN.Value        :=0;
     CpyDstKopfTabKONTOAUSZUG.Value       :=-1;
     CpyDstKopfTabBANK_ID.Value           :=-1;
     CpyDstKopfTabSTADIUM.Value           :=6;
     CpyDstKopfTabFREIGABE1_Flag.Value    :=False;
     CpyDstKopfTabERSTELLT.Value          :=Now;
     CpyDstKopfTabERST_NAME.Value         :=View_User;
     CpyDstKopfTabKUN_NUM.Value           :=CpySrcKopfTabKUN_NUM.Value;
     CpyDstKopfTabKUN_ANREDE.Value        :=CpySrcKopfTabKUN_ANREDE.Value;
     CpyDstKopfTabKUN_NAME1.Value         :=CpySrcKopfTabKUN_NAME1.Value;
     CpyDstKopfTabKUN_NAME2.Value         :=CpySrcKopfTabKUN_NAME2.Value;
     CpyDstKopfTabKUN_NAME3.Value         :=CpySrcKopfTabKUN_NAME3.Value;
     CpyDstKopfTabKUN_ABTEILUNG.Value     :=CpySrcKopfTabKUN_ABTEILUNG.Value;
     CpyDstKopfTabKUN_STRASSE.Value       :=CpySrcKopfTabKUN_STRASSE.Value;
     CpyDstKopfTabKUN_LAND.Value          :=CpySrcKopfTabKUN_LAND.Value;
     CpyDstKopfTabKUN_PLZ.Value           :=CpySrcKopfTabKUN_PLZ.Value;
     CpyDstKopfTabKUN_ORT.Value           :=CpySrcKopfTabKUN_ORT.Value;
     CpyDstKopfTabUSR1.Value              :=CpySrcKopfTabUSR1.Value;
     CpyDstKopfTabUSR2.Value              :=CpySrcKopfTabUSR2.Value;
     CpyDstKopfTabPROJEKT.Value           :=CpySrcKopfTabPROJEKT.Value;
     CpyDstKopfTabORGNUM.Value            :=CpySrcKopfTabORGNUM.Value;
     CpyDstKopfTabBEST_NAME.Value         :=CpySrcKopfTabBEST_NAME.Value;
     CpyDstKopfTabBEST_CODE.Value         :=CpySrcKopfTabBEST_CODE.Value;
     CpyDstKopfTabINFO.AsString           :=CpySrcKopfTabINFO.AsString;
     CpyDstKopfTabBEST_DATUM.Value        :=CpySrcKopfTabBEST_DATUM.Value;
     CpyDstKopfTabPROVIS_WERT.Value       :=CpySrcKopfTabPROVIS_WERT.Value;

     CpyDstKopfTabPROVIS_WERT.Value       :=CpySrcKopfTabPROVIS_WERT.Value;
     CpyDstKopfTabBRUTTO_FLAG.AsBoolean   :=CpySrcKopfTabBRUTTO_FLAG.AsBoolean;

     CpyDstKopfTabGewicht.AsFloat         :=CpySrcKopfTabGewicht.AsFloat;
     CpyDstKopfTabRohgewinn.AsFloat       :=CpySrcKopfTabRohgewinn.AsFloat;

     CpyDstKopfTabUW_NUM.Value            :=-1;

     if ((CpySrcKopfTabQuelle.AsInteger in [EK_RECH,EK_RECH_EDI])and(Dest=VK_RECH_EDI))or
        ((CpySrcKopfTabQuelle.AsInteger in [VK_RECH,VK_RECH_EDI,VK_AGB,VK_AGB_EDI])and(Dest=EK_BEST_EDI)) then
     begin
        CpyDstKopfTabKOST_NETTO.Value        :=0;
        CpyDstKopfTabWERT_NETTO.Value        :=0;
        CpyDstKopfTabLOHN.Value              :=0;
        CpyDstKopfTabWARE.Value              :=0;
        CpyDstKopfTabTKOST.Value             :=0;
        CpyDstKopfTabGLOBRABATT.Value        :=0;

        if Dest<>EK_BEST_EDI then
        begin
             CpyDstKopfTabPR_EBENE.Value     :=2;
        end
         else
        begin
             CpyDstKopfTabPR_EBENE.Value      :=0;
             CpyDstKopfTabKUN_NUM.Value       :='';
             CpyDstKopfTabKUN_ANREDE.Value    :='';
             CpyDstKopfTabKUN_NAME1.Value     :='';
             CpyDstKopfTabKUN_NAME2.Value     :='';
             CpyDstKopfTabKUN_NAME3.Value     :='';
             CpyDstKopfTabKUN_ABTEILUNG.Value :='';
             CpyDstKopfTabKUN_STRASSE.Value   :='';
             CpyDstKopfTabKUN_LAND.Value      :='';
             CpyDstKopfTabKUN_PLZ.Value       :='';
             CpyDstKopfTabKUN_ORT.Value       :='';

             CpyDstKopfTabLief_Addr_ID.Value  :=CpySrcKopfTabAddr_ID.Value;
             CpyDstKopfTabAddr_ID.Value       :=-1;
             CpyDstKopfTabLDatum.Value        :=int(Now)+3;
        end;

        CpyDstKopfTabLIEFART.Value :=-1;
        CpyDstKopfTabZAHLART.Value :=-1;
     end;

     CpyDstKopfTab.Post;

     ID :=CpyDstKopfTabREC_ID.Value;

     CpySrcPosTab.Close;
     CpySrcPosTab.ParamByName ('ID').AsInteger :=Journal_ID;
     CpySrcPosTab.Open;

     CpyDstPosTab.Open;

     while not CpySrcPosTab.Eof do
     begin
        // Bei Lieferscheinen die Transportkosten �berspringen
        if (Dest=VK_LIEF_EDI)and
           (CpySrcPosTab.FieldByName('ARTIKELTYP').AsString='K') then
        begin
           CpySrcPosTab.Next;
           Continue;
        end;


        CpyDstPosTab.Append;

        for i:=0 to CpySrcPosTab.Fields.Count-1 do
        begin
         if uppercase(CpySrcPosTab.Fields[i].FieldName)<>'REC_ID'
          then  CpyDstPosTab.FieldByName (CpySrcPosTab.Fields[i].FieldName).Value :=
                   CpySrcPosTab.Fields[i].Value;
        end;
        CpyDstPosTab.FieldByName('VRENUM').Value          :=num;
        CpyDstPosTab.FieldByName('QUELLE').Value          :=Dest;
        CpyDstPosTab.FieldByName('QUELLE_SUB').Value      :=0;
        //CpyDstPosTab.FieldByName('JAHR').Value            :=0;
        CpyDstPosTab.FieldByName('JOURNAL_ID').Value      :=ID;
        CpyDstPosTab.FieldByName('GEBUCHT').Value         :=False;
        CpyDstPosTab.FieldByName('EPREIS').Value          :=CalcLeitwaehrung(CpySrcPosTab.FieldByName('EPREIS').AsFloat,W);
        CpyDstPosTab.FieldByName('E_RGEWINN').Value       :=CalcLeitwaehrung(CpySrcPosTab.FieldByName('E_RGEWINN').AsFloat,W);

        //NEU
        //if (CpySrcKopfTabQuelle.AsInteger in [EK_RECH,EK_RECH_EDI])and(Dest=VK_RECH_EDI) then
        if ((CpySrcKopfTabQuelle.AsInteger in [EK_RECH,EK_RECH_EDI])and(Dest=VK_RECH_EDI))or
           ((CpySrcKopfTabQuelle.AsInteger in [VK_RECH,VK_RECH_EDI,VK_AGB,VK_AGB_EDI])and(Dest=EK_BEST_EDI))or
           (Dest=VK_LIEF_EDI) then
        begin
           if (DEST <> EK_BEST_EDI)
            then CpyDstPosTab.FieldByName('GEGENKTO').Value :=DM1.ReadInteger ('MAIN\KONTEN','DEF_ERLOESKTO',8400)
            else CpyDstPosTab.FieldByName('GEGENKTO').Value :=DM1.ReadInteger ('MAIN\KONTEN','DEF_AUFWANDSKTO',3400);

           if (CpyDstPosTab.FieldByName('ARTIKEL_ID').AsInteger >=0)and
              (Dest <> VK_LIEF_EDI) then
           begin
             UpdateArtTab.Close;
             UpdateArtTab.ParamByName ('ID').AsInteger :=CpyDstPosTab.FieldByName('ARTIKEL_ID').AsInteger;
             UpdateArtTab.Open;

             if UpdateArtTab.RecordCount=1 then
             begin
               if (DEST=EK_BEST_EDI)and // nur bei EK-Bestellungen
                  (UpdateArtTab.FieldByName ('RABGRP_ID').AsString<>'-')and
                  (length(UpdateArtTab.FieldByName ('RABGRP_ID').AsString)>0) then
               begin
                  // Ist ein Artikel mit Rabattgruppe !!!
                  case AnzPreis of
                       1: EK :=UpdateArtTab.FieldByName ('VK1').Value;
                       2: EK :=UpdateArtTab.FieldByName ('VK2').Value;
                       3: EK :=UpdateArtTab.FieldByName ('VK3').Value;
                       4: EK :=UpdateArtTab.FieldByName ('VK4').Value;
                     else EK :=UpdateArtTab.FieldByName ('VK5').Value;
                  end;

                  DM1.CalcRabGrpPreis (UpdateArtTab.FieldByName ('RABGRP_ID').AsString,0{EK},EK);
                  CpyDstPosTab.FieldByName('EPREIS').Value :=CAO_round(EK * 1000)/1000; //auf 3 Nachkommastellen runden
               end
                  else
               begin
                  // neuen VK-Preis zuweisen
                  case CpyDstKopfTabPR_EBENE.Value of
                     0:   CpyDstPosTab.FieldByName('EPREIS').Value :=UpdateArtTab.FieldByName ('EK_PREIS').Value;
                     1:   CpyDstPosTab.FieldByName('EPREIS').Value :=UpdateArtTab.FieldByName ('VK1').Value;
                     2:   CpyDstPosTab.FieldByName('EPREIS').Value :=UpdateArtTab.FieldByName ('VK2').Value;
                     3:   CpyDstPosTab.FieldByName('EPREIS').Value :=UpdateArtTab.FieldByName ('VK3').Value;
                     4:   CpyDstPosTab.FieldByName('EPREIS').Value :=UpdateArtTab.FieldByName ('VK4').Value;
                     else CpyDstPosTab.FieldByName('EPREIS').Value :=UpdateArtTab.FieldByName ('VK5').Value;
                  end;
               end;
             end;

             if DEST <> EK_BEST_EDI
              then CpyDstPosTab.FieldByName('GEGENKTO').Value :=UpdateArtTab.FieldByName ('ERLOES_KTO').AsInteger
              else CpyDstPosTab.FieldByName('GEGENKTO').Value :=UpdateArtTab.FieldByName ('AUFW_KTO').AsInteger;

             if DEST = EK_BEST_EDI then CpyDstPosTab.FieldByName('RABATT').Value  :=0;
             if DEST = EK_BEST_EDI then CpyDstPosTab.FieldByName('RABATT2').Value :=0;
             if DEST = EK_BEST_EDI then CpyDstPosTab.FieldByName('RABATT3').Value :=0;

             UpdateArtTab.Close;
           end;



           Summe :=CpyDstPosTab.FieldByName('EPREIS').Value * CpyDstPosTab.FieldByName('MENGE').Value;
           if CpyDstPosTab.FieldByName('RABATT').Value <> 0
            then Summe :=Summe - Summe * CpyDstPosTab.FieldByName('RABATT').Value / 100;

           Case CpyDstPosTab.FieldByName('STEUER_CODE').Value of
              0: Steuer :=CpyDstKopfTabMWST_0.Value;
              1: Steuer :=CpyDstKopfTabMWST_1.Value;
              2: Steuer :=CpyDstKopfTabMWST_2.Value;
              3: Steuer :=CpyDstKopfTabMWST_3.Value;
              else Steuer :=0;
           end;

           NSumme :=CAO_round(Summe*100)/100;  // Auf ganze Pfennige Runden
           MSumme :=CAO_round(Summe * (Steuer / 100) *100)/100; // Auf ganze Pfennige Runden
           BSumme :=NSumme+MSumme;

           N :=N+NSumme;
           M :=M+MSumme;
           B :=B+BSumme;

           case CpyDstPosTab.FieldByName('STEUER_CODE').Value of
                  0:M0 :=M0 + MSumme;
                  1:M1 :=M1 + MSumme;
                  2:M2 :=M2 + MSumme;
                  3:M3 :=M3 + MSumme;
           end;

           // Lohn, Ware, Transportkosten
           if length (CpyDstPosTab.FieldByName('ARTIKELTYP').AsString)=1
             then T :=CpyDstPosTab.FieldByName('ARTIKELTYP').AsString[1]
             else T :='?';

           case t of
               'N','S','V','F' : Ware :=Ware + NSumme;
               'L'             : Lohn :=Lohn + NSumme;
               'K'             : TKst :=TKst + NSumme;
           end;
        end;

        // Bei VK-Rechnungen (Edi) die Lieferscheinnummer der
        // zugrundeliegenden Rechnung l�schen
        if Dest=VK_RECH_EDI
          then CpyDstPosTab.FieldByName('VLSNUM').AsInteger :=-1;


        if ((Dest=EK_RECH_EDI) and (CpySrcKopfTabQuelle.AsInteger=EK_BEST)) or
           ((Dest=VK_LIEF_EDI) and (CpySrcKopfTabQuelle.AsInteger=VK_RECH_EDI)) then
        begin
             // Im Einkauf Verweis auf die zugrundeliegenden Bestell-Positionen machen
             // im Verkauf/Lieferschein Verweis auf zugrundeliegende EDI-Rechnunsposition machen
             CpyDstPosTab.FieldByName ('QUELLE_SRC').AsInteger :=
              CpySrcPosTab.FieldByName ('REC_ID').AsInteger;
        end;

        CpyDstPosTab.Post;

        // Seriennummern bei Lieferscheinen zuweisen
        if (Dest=VK_LIEF_EDI) and
           (CpySrcKopfTabQuelle.AsInteger=VK_RECH_EDI) then
        begin
            UniQuery.Sql.Text :=
              'UPDATE ARTIKEL_SERNUM SET '+
              'LS_JOURNALPOS_ID='+
              Inttostr(CpyDstPosTab.FieldByName ('REC_ID').AsInteger)+','+
              'LS_JOURNAL_ID='+Inttostr(ID)+
              ' where VK_JOURNALPOS_ID='+
              IntToStr(CpySrcPosTab.FieldByName ('REC_ID').AsInteger)+' and '+
              'LS_JOURNALPOS_ID=-1 and ARTIKEL_ID='+
              IntToStr(CpyDstPosTab.FieldByName('ARTIKEL_ID').AsInteger);

            UniQuery.ExecSql;
        end;


        // EDI-Mengen aktualisieren
        UpdateArtikelEdiMenge (Dest,
                               CpyDstPosTab.FieldByName('ARTIKEL_ID').AsInteger,
                               0);

        CpySrcPosTab.Next;
     end;

     if ((CpySrcKopfTabQuelle.AsInteger in [EK_RECH,EK_RECH_EDI])and(Dest=VK_RECH_EDI))or
        ((CpySrcKopfTabQuelle.AsInteger in [VK_RECH,VK_RECH_EDI,VK_AGB,VK_AGB_EDI])and(Dest=EK_BEST_EDI))or
        (Dest=VK_LIEF_EDI) then
     begin
        CpyDstKopfTab.Edit;

        CpyDstKopfTabNSumme.Value :=N;
        CpyDstKopfTabMSumme.Value :=M;
        CpyDstKopfTabBSumme.Value :=B;

        CpyDstKopfTabMSumme_0.Value :=M0;
        CpyDstKopfTabMSumme_1.Value :=M1;
        CpyDstKopfTabMSumme_2.Value :=M2;
        CpyDstKopfTabMSumme_3.Value :=M3;

        CpyDstKopfTabWare.Value     :=Ware;
        CpyDstKopfTabLohn.Value     :=Lohn;
        CpyDstKopfTabTKost.Value    :=TKst;

        CpyDstKopfTab.Post;
     end;
     CpySrcPosTab.Close;
     CpyDstPosTab.Close;
     CpyDstKopfTab.Close;
     CpySrcKopfTab.Close;

     Result :=ID;
end;
//------------------------------------------------------------------------------
function tDM1.BucheKasse (Datum      : tDateTime;
                          Quelle     : Integer;
                          Journal_ID : Integer;
                          BelNum     : String;
                          GKonto     : Integer;
                          Skonto     : Double;
                          Betrag     : Double;
                          Text       : String):Boolean;

var Ja, Mo, Ta : Word;
begin
     DecodeDate (Datum, Ja, Mo, Ta);

     KasBuch.Close;
     KasBuch.ParamByName ('JAHR').Value :=Ja;
     KasBuch.Open;
     KasBuch.Append;

     try
        KasBuchJAHR.Value       :=Ja;
        KasBuchBDATUM.Value     :=Datum;
        KasBuchQUELLE.Value     :=Quelle;
        KasBuchJOURNAL_ID.Value :=Journal_ID;
        KasBuchZU_ABGANG.Value  :=Betrag;
        KasBuchBTXT.AsString    :=Text;
        KasBuchBELEGNUM.Value   :=BelNum;
        KasBuchGKONTO.Value     :=GKonto;
        KasBuchSKONTO.Value     :=Skonto;

        KasBuch.Post;
        Result :=True;
     except
        KasBuch.Cancel;
        Result :=False;
     end;
     KasBuch.Close;
end;
//------------------------------------------------------------------------------
procedure TDM1.JourTabCalcFields(DataSet: TDataSet);
begin
     JourTabIST_SKONTO_BETR.Value :=JourTabBSumme.Value - JourTabIST_Betrag.Value;
end;
//------------------------------------------------------------------------------
procedure TDM1.KGRTabBeforePost(DataSet: TDataSet);
begin
     if KGRTabMainKey.Value='' then KGRTabMainKey.Value :='MAIN\ADDR_HIR';
end;
//------------------------------------------------------------------------------
procedure TDM1.WgrTabBeforePost(DataSet: TDataSet);
begin
     //if WGRTabMainKey.Value='' then WGRTabMainKey.Value :='MAIN\ART_HIR';
end;
//------------------------------------------------------------------------------
procedure TDM1.FirBankTabBeforePost(DataSet: TDataSet);
begin
     FirBankTabmainkey.Value :='MAIN\FIRMENKONTEN';
end;
//------------------------------------------------------------------------------
function TDM1.GetWGRDefaultKonten (WGR : Integer; var EKTO, AKTO : Integer) : Boolean;
begin
     Result :=False;
     if not WGRTab.Active then WGRTab.Open;

     if WGRTab.Locate ('ID',WGR,[]) then
     begin
        if WgrTabDEF_AKTO.AsInteger > 0 then AKTO :=WgrTabDEF_AKTO.AsInteger;
        if WGRTabDEF_EKTO.AsInteger > 0 then EKTO :=WGRTabDEF_EKTO.AsInteger;
        Result :=True;
     end;
end;
//------------------------------------------------------------------------------
Function tDM1.CheckBankverbindung (addr_id:integer) : boolean;
begin
     Result :=False;
     KunTab.Close;
     KunTab.ParamByName ('ID').AsInteger :=addr_id;
     KunTab.Open;
     if not KunTab.Eof then
     begin
       try
         Result :=(length(KunTabBLZ.AsString)=8)and
                  (length(KunTabKTO.AsString)>1)and
                  (length(KunTabBank.AsString)>3)and
                  (StrToInt(KunTabBLZ.AsString)>0);

       except
         Result :=False;
       end;
     end else Result :=False;
     KunTab.Close;
end;
//------------------------------------------------------------------------------
Function tDM1.GetBankverbindung (addr_id:integer; Var BLZ : Integer; var KTO : String) : boolean;
var OK : Boolean;
begin
     OK :=False;
     KunTab.Close;
     KunTab.ParamByName ('ID').AsInteger :=addr_id;
     KunTab.Open;
     if not KunTab.Eof then
     begin
       try
         OK :=(length(KunTabBLZ.AsString)=8)and
              (length(KunTabKTO.AsString)>1)and
              (length(KunTabBank.AsString)>3)and
              (StrToInt(KunTabBLZ.AsString)>0);
       except
         OK :=False;
       end;
     end else Result :=OK;

     if OK then
     begin
       BLZ :=StrToInt (KunTabBLZ.AsString);
       KTO :=KunTabKTO.AsString;
     end;

     KunTab.Close;
     Result :=OK;
end;
//------------------------------------------------------------------------------
Function tDM1.GetLieferant (addr_id:integer; Var Info : String) : boolean; // True wenn ok
var S : String;
begin
     if Addr_id<1 then begin Info :='???'; exit; end;

     KunTab.Close;
     KunTab.ParamByName ('ID').AsInteger :=addr_id;
     KunTab.Open;
     if not KunTab.Eof then
     begin
        S:=KunTabNAME1.AsString;
        if length (KunTabNAME2.AsString)>0 then
        begin
          if length(S)>0 then S :=S+', ';
          S :=S+KunTabNAME2.AsString;
        end;
        if length (KunTabNAME3.AsString)>0 then
        begin
          if length(S)>0 then S :=S+', ';
          S :=S+KunTabNAME3.AsString;
        end;

        if length (KunTabStrasse.AsString)>0 then
        begin
          if length(S)>0 then S :=S+', ';
          S :=S+KunTabStrasse.AsString;
        end;

        S :=S+', '+KunTabPlz.AsString+' '+KunTabOrt.AsString;

        Info :=S;
        Result :=True;
     end else
     begin
        Result :=False;
        S :='';
     end;
     KunTab.Close;
end;
//------------------------------------------------------------------------------
procedure TDM1.DataModuleDestroy(Sender: TObject);
begin
     SetLength(MandantTab,0);
end;
//------------------------------------------------------------------------------
function TDM1.CalcRabGrpPreis (RGID:String; PR_Ebene : Integer; var Preis : Double):Boolean;
begin
     Result :=False;
     if (RGID='-')or(length(RGID)=0)or
        (PR_Ebene<0)or(PR_EBENE>4)or
        (PR_EBENE>=ANZPREIS) then exit;

     if Preis=0 then begin Result :=True; exit; end;

     if PR_Ebene=0 then  //EK-Preis
     begin
       if not LiefRabGrp.Active then LiefRabGrp.Open;

       if (LiefRabGrp.RecordCount=0) then exit;
       if (LiefRabGrpRABGRP_ID.AsString=RGID)or
          (LiefRabGrp.Locate ('RABGRP_ID',RGID,[loCaseInsensitive])) then
       begin
         if (LiefRabGrpRABGRP_ID.Value=RGID) then
         begin
           Preis :=Preis - (Preis / 100 * LiefRabGrpRABATT1.AsFloat);
           Preis :=Preis - (Preis / 100 * LiefRabGrpRABATT2.AsFloat);
           Preis :=Preis - (Preis / 100 * LiefRabGrpRABATT3.AsFloat);

           // auf 3 Nachkommastellen runden
           Preis :=CAO_round(Preis*1000)/1000;

           Result :=True;
         end;
       end;
     end
        else
     begin
       if not KunRabGrp.Active then KunRabGrp.Open;

       if (KunRabGrp.RecordCount=0) then exit;
       if //KunRabGrpRABGRP_ID.AsString=RGID)or
          (KunRabGrp.Locate ('RABGRP_ID,LIEF_RABGRP',
                              VarArrayOf([RGID,PR_Ebene]),
                              [loCaseInsensitive])) then
       begin
         if (KunRabGrpRABGRP_ID.Value=RGID) and
            (KunRabGrpLIEF_RABGRP.AsInteger=PR_Ebene) then
         begin
           Preis :=Preis - (Preis / 100 * KunRabGrpRABATT1.AsFloat);
           Preis :=Preis - (Preis / 100 * KunRabGrpRABATT2.AsFloat);
           Preis :=Preis - (Preis / 100 * KunRabGrpRABATT3.AsFloat);

           // auf 2 Nachkommastellen runden
           Preis :=CAO_round(Preis*100)/100;

           Result :=True;
         end;
       end;
     end;
end;
//------------------------------------------------------------------------------
function TDM1.UpdateArtikelEdiMenge (JournalTyp, ArtikelID : Integer;
                                     MengeDiff:Double) : Boolean;
var ANZ : Integer; ME : Double; EdiFeld : String;
begin
     Result :=False;

     if (not (JournalTyp in [VK_RECH_EDI, EK_RECH_EDI, EK_BEST_EDI])) or
        (ArtikelID<0) then exit;

     MengeDiff :=CAO_round(MengeDiff*100)/100;

     case JournalTyp of
          VK_RECH_EDI : EdiFeld :='MENGE_VKRE_EDI';
          EK_RECH_EDI : EdiFeld :='MENGE_EKRE_EDI';
          EK_BEST_EDI : EdiFeld :='MENGE_EKBEST_EDI';
     end;

     ANZ :=0;
     uniquery.close;

     ProgressForm.Init ('EDI-Mengen aktualisieren ...');
     ZBatchSQL1.Sql.Clear;
     uniquery.sql.clear;
     uniquery.sql.add ('SELECT ARTIKEL_ID, SUM(MENGE) as ME from JOURNALPOS');
     uniquery.sql.add ('WHERE QUELLE='+IntToStr(JournalTyp));
     uniquery.sql.add ('and ARTIKEL_ID='+IntToStr(ArtikelID));
     uniquery.sql.add ('GROUP BY ARTIKEL_ID');
     uniquery.Open;
     while not UniQuery.EOF do
     begin
        if (UniQuery.FieldByName('ARTIKEL_ID').AsInteger>0) then
        begin
           ME :=UniQuery.FieldByName('ME').AsFloat-MengeDiff;
           ZBatchSQL1.Sql.Add ('UPDATE ARTIKEL SET ');
           ZBatchSQL1.Sql.Add (EdiFeld+'='+FloatToStrEx(ME));
           ZBatchSQL1.Sql.Add ('WHERE REC_ID='+IntToStr(UniQuery.FieldByName('ARTIKEL_ID').AsInteger)+';');
           inc (ANZ);
        end;
        uniquery.next;
     end;
     if ANZ>0 then ZBatchSql1.ExecSql;
     uniQuery.Close;
     Result :=True;
end;
//------------------------------------------------------------------------------
procedure TDM1.NummerTabNewRecord(DataSet: TDataSet);
begin
     // Neuanlage abbrechen, es sei denn die Neuanlage wird von
     // GetNummer aufgerufen, GetNummer setzt dann die Variable
     // InNewNummer auf True
     if not InNewNummer then Abort;
end;
//------------------------------------------------------------------------------
procedure TDM1.ShopOrderStatusTabBeforePost(DataSet: TDataSet);
begin
     if ShopOrderStatusTabMainKey.Value=''
      then ShopOrderStatusTabMainKey.Value :='SHOP\ORDERSTATUS';
end;
//------------------------------------------------------------------------------
procedure TDM1.ZBatchSql1BeforeExecute(Sender: TObject);
begin
     ProgressForm.Start;
end;
//------------------------------------------------------------------------------
procedure TDM1.Transact1AfterBatchExec(Sender: TObject; var Res: Integer);
begin
     ProgressForm.UpdateScreen;
end;
//------------------------------------------------------------------------------
procedure TDM1.ZBatchSql1AfterExecute(Sender: TObject);
begin
     ProgressForm.Stop;
end;
//------------------------------------------------------------------------------
procedure TDM1.ExportDatasetToStream(Stream: TStream;
                                     Dataset : TDataset;
                                     Delimiter : String;
                                     Spaltennamen : Boolean = True;
                                     TextInHochKomma : Boolean = True;
                                     DosZeichenSatz : Boolean = False);
var
  I, J      : Integer;
  Buffer,S  : string;
  FieldDesc : tField;
  OldC      : tCursor;
begin
   OldC :=Screen.Cursor;
   Screen.Cursor :=crSqlWait;
   try
     with Dataset do
     begin
       // Feldnamen als �berschrift ausgeben !!
       if (Fields.Count > 0)and(Stream.Position=0)and(Spaltennamen) then
       begin
        Buffer := '';
        for J := 0 to FieldCount-1 do
        begin
          if J > 0 then Buffer := Buffer + Delimiter;
          Buffer := Buffer + Fields[J].FieldName;
        end;

        if DosZeichenSatz then CharToOEM(PChar(Buffer), @Buffer[1]);

        Stream.Write(PChar(Buffer)^, Length(Buffer));
      end;
      First;
      while not EOF do
      begin
        Buffer := #13#10;
        for J := 0 to Fields.Count-1 do
        begin
          FieldDesc := Fields[J];
          if J > 0 then Buffer := Buffer + Delimiter;
          if not (FieldDesc.DataType in [ftInteger, ftSmallInt, ftFloat,
                                         ftAutoInc, ftCurrency, ftLargeInt,
                                         ftBCD])
            and not FieldDesc.IsNull then
          begin
            S :=FieldDesc.AsString;

            if TextInHochKomma
              then Buffer :=Buffer +
                            AnsiQuotedStr(Uniquery.StringToSql(S),'"')
              else Buffer :=Buffer +
                            Uniquery.StringToSql(S);
          end
          else Buffer := Buffer + FieldDesc.AsString;
        end;
        if DosZeichenSatz then CharToOEM(PChar(Buffer), @Buffer[1]);
        Stream.Write(PChar(Buffer)^, Length(Buffer));
        inc(i);
        Next;
      end;
     end;
   finally
     Screen.Cursor :=oldC;
   end;
end;
//------------------------------------------------------------------------------
procedure TDM1.ExportDatasetToFile (FileName: String;
                                    Dataset : TDataset;
                                    Delimiter : String;
                                    Append : Boolean;
                                    Spaltennamen : Boolean = True;
                                    TextInHochKomma : Boolean = True;
                                    DosZeichenSatz : Boolean = False);
var St : tFileStream; M : Word;
begin
     //Append :=True;
     // 1. Datei erzeugen wenn sie nicht existiert
     if ((fileexists (FileName))and(not Append))or
        (not fileexists (FileName))
      then FileClose(FileCreate(FileName));

     if Append then M :=fmOpenReadWrite else M :=fmOpenWrite;
     M :=M or fmShareDenyWrite;

     St :=tFileStream.Create (FileName, M);
     if Append then ST.Position :=ST.Size;
     try
        ExportDatasetToStream (St,
                               Dataset,
                               Delimiter,
                               Spaltennamen,
                               TextInHochKomma,
                               DosZeichenSatz);
     finally
        St.Free;
     end;
end;
//------------------------------------------------------------------------------
procedure TDM1.UpdateArtikelPreis (RechTyp, Artikel_ID, Addr_ID : Integer; Preis : Double);
begin
     Uniquery.Close;
     UniQuery.RequestLive :=True;
     try
        UniQuery.Sql.Text :='select * from ARTIKEL_PREIS where ARTIKEL_ID='+
                            IntToStr(Artikel_ID)+
                            ' and ADRESS_ID='+
                            IntToStr(Addr_ID);
        UniQuery.Open;
        if UniQuery.RecordCount=1 then
        begin
           UniQuery.Edit;
           try
              UniQuery.FieldByName ('PREIS').AsFloat :=Preis;
              UniQuery.FieldByName ('GEAEND').AsDateTime :=Now;
              UniQuery.FieldByName ('GEAEND_NAME').AsString :=View_User;

              UniQuery.Post;
           except
              UniQuery.Cancel;
           end;

        end else
        if UniQuery.RecordCount=0 then
        begin
           // neuen Eintrag anlegen
           UniQuery.Append;
           try
              UniQuery.FieldByName ('ARTIKEL_ID').AsInteger :=Artikel_ID;
              UniQuery.FieldByName ('ADRESS_ID').AsInteger :=Addr_ID;
              UniQuery.FieldByName ('PREIS_TYP').AsInteger :=RechTyp;
              UniQuery.FieldByName ('PREIS').AsFloat :=Preis;
              UniQuery.FieldByName ('GEAEND').AsDateTime :=Now;
              UniQuery.FieldByName ('GEAEND_NAME').AsString :=View_User;

              UniQuery.Post;
           except
              UniQuery.Cancel;
           end;
        end;
     finally
        Uniquery.Close;
        UniQuery.RequestLive :=False;
     end;


     { Ab 1.3 ...

     delete from ARTIKEL_BDATEN;
     insert into ARTIKEL_BDATEN
     select ARTIKEL_ID, QUELLE, SUM(MENGE) from JOURNALPOS
     where QUELLE IN (13,15,16) and ARTIKELTYP IN ('N','S') and ARTIKEL_ID>0
     group by ARTIKEL_ID, QUELLE;

     }
end;
//------------------------------------------------------------------------------
function TDM1.GetSearchSQL (Felder : array of String; Suchbegriff : String) : String;
var
  Stk          : TaaStringStack;
  Parser       : TaaSearchParser;
  RPNNode      : TaaRPNNode;
  Word1        : string;
  Word2        : string;
  SQL, SQL2, S : String;
  I, P         : Integer;

begin
  Stk := nil;
  Parser := nil;
  try
    Stk := TaaStringStack.Create;
    Parser := TaaSearchParser.Create(Suchbegriff);
    RPNNode := Parser.RPN;
    while (RPNNode <> nil) do
    begin
      if (RPNNode is TaaRPNWord) then
        Stk.Push('(@@@FN@@@ like ''%' + TaaRPNWord(RPNNode).PhraseWord + '%'')')
      else if (RPNNode is TaaRPN_AND) then
      begin
        Word2 := Stk.Pop;
        Word1 := Stk.Pop;
        Stk.Push('(' + Word1 + ' and ' + Word2 + ')');
      end
      else if (RPNNode is TaaRPN_OR) then
      begin
        Word2 := Stk.Pop;
        Word1 := Stk.Pop;
        Stk.Push('(' + Word1 + ' or ' + Word2 + ')');
      end
        else
      begin
        Word1 := Stk.Pop;
        Stk.Push('(not ' + Word1 + ')');
      end;
      RPNNode := RPNNode.Next;
    end;

    SQL :='';
    SQL2 :=Stk.Pop; // in SQL2 steht jetzt der SQL-Befehl mit dem Feldplatzhalter

    // Feldnamen einf�gen
    if Length(Felder)> 0 then // Alle Felder durchlaufen
    begin
      For i:=0 to length(Felder)-1 do
      begin
         S :=SQL2;
         while Pos('@@@FN@@@',S)>0 do // Platzhalter gefunden
         begin
            P :=Pos('@@@FN@@@',S);
            Delete (S,P,8);           // Platzhalter l�schen
            insert (Felder[i],S,P);   // und durch Feldnamen ersetzen
         end;

         // wenn in mehreren Feldern gesucht wird dan die einzelnen Suchen
         // mit oder verkn�pfen
         if length(SQL)>0 then SQL :=SQL + ' or ';
         SQL :=SQL+'('+S+')';
      end;
    end;
    Result :=SQL; // SQL-String zur�ckgeben
  finally
    Stk.Free;
    Parser.Free;
  end;
end;
//------------------------------------------------------------------------------
procedure TDM1.FirmaTabCalcFields(DataSet: TDataSet);
begin
     FirmaTabUSER_AKT.AsString     :=View_User;
     FirmaTabLEITWAEHRUNG.AsString :=Leitwaehrung;
     FirmaTabMANDANT_NAME.AsString :=AktMandant;
end;
//------------------------------------------------------------------------------
function TDM1.GetVertreterProv (ID : Integer) : Double;
begin
   Result :=0;
   if ID<1 then exit;

   if ID = LastVertrID then
   begin
      Result :=LastVertrProz;
      exit;
   end;

   if not VertreterTab.Active then VertreterTab.Open;

   if (VertreterTab.Locate ('VERTRETER_ID',ID,[])) and
      (VertreterTab.FieldByName ('VERTRETER_ID').AsInteger=ID) then
   begin
      LastVertrID   :=VertreterTab.FieldByName ('VERTRETER_ID').AsInteger;
      LastVertrProz :=VertreterTab.FieldByName ('PROVISIONSATZ').AsFloat;

      Result :=LastVertrProz;
   end;
end;
//------------------------------------------------------------------------------
procedure TDM1.CheckTerminalID;
var T : String; I : Integer;
begin
     Uniquery.SQL.Text :='select SUBSTRING_INDEX(USER(),"@",-1) as PC';
     Uniquery.Open;
     if Uniquery.RecordCount=1 then T :=Uniquery.FieldByName ('PC').AsString;
     Uniquery.Close;

     Uniquery.SQL.Text :='select MAINKEY, NAME, VAL_INT from REGISTERY '+
                          'where MAINKEY="MAIN\\TERMINAL" and NAME="'+T+'"';
     Uniquery.Open;
     if Uniquery.RecordCount=1 then
     begin
        TermID :=Uniquery.FieldByName ('VAL_INT').AsInteger;
        Uniquery.Close;
     end
        else
     begin
         Uniquery.Close;
         Uniquery.SQL.Text :='select COUNT(*) as ANZ FROM REGISTERY '+
                             'where MAINKEY="MAIN\\TERMINAL"';
         UniQuery.Open;
         I :=UniQuery.FieldByName ('ANZ').AsInteger+1;
         UniQuery.Close;

         UniQuery.SQL.Text :='INSERT INTO REGISTERY SET '+
                             'MAINKEY="MAIN\\TERMINAL", NAME="'+T+'", '+
                             'VAL_INT='+IntToStr(i)+', VAL_TYP=3';
         UniQuery.ExecSQL;

         TermID :=I;
     end;
end;
//------------------------------------------------------------------------------
function tDM1.GetArtikelPreis (ArtikelID, KunID, PE : Integer;
                               Brutto : Boolean;
                               Menge : Double;
                               var Preis : Double) : Boolean;

var PR, M, B : Double;
    PreisTyp : Integer;
begin
     Result :=False;

     if PE=0 then PreisTyp :=EK_RECH else PreisTyp :=VK_RECH;


     if (ArtInfoTab.Active)and
        (ArtInfoTab.RecordCount>0) and

        (
         (DM1.ArtInfoTab.Locate ('REC_ID;PREIS_TYP;ADRESS_ID',
                                 VarArrayOF([ArtikelID,PreisTyp,KunID]),
                                 []))or
         (DM1.ArtInfoTab.Locate ('REC_ID',
                                 ArtikelID,
                                 []))
        ) then
        //(ArtInfoTab.Locate ('REC_ID',ArtikelID,[])) then
     begin
          if Brutto then
          begin
            //Kundenpreis pr�fen
            if (ArtInfoTabAdress_ID.AsInteger=KunID) and
               (ArtInfoTabPreis.AsFloat<>0) then
            begin
               PR :=ArtInfoTabPreis.AsFloat;
               // Brutto berechnen
               case ArtInfoTabSTEUER_CODE.AsInteger of
                    1: M :=MWSTTab[1];
                    2: M :=MWSTTab[2];
                    3: M :=MWSTTab[3];
                  else M :=MWSTTab[0];
               end;
               M :=M+100; // jetzt z.B. 116

               B :=CAO_round(PR*M); // jetzt ganze Cent
               B :=CAO_Round (B / BR_RUND_WERT) * BR_RUND_WERT / 100;
               Preis :=B;
            end
               else
            begin
               case PE of
                   0:PR :=ArtInfoTabEK_PREIS.Value;
                   1:PR :=ArtInfoTabVK1B.Value;
                   2:PR :=ArtInfoTabVK2B.Value;
                   3:PR :=ArtInfoTabVK3B.Value;
                   4:PR :=ArtInfoTabVK4B.Value;
                else PR :=ArtInfoTabVK5B.Value;
               end;
               Preis :=PR;
            end;


            if (ArtInfoTabRABGRP_ID.AsString<>'-')and
               (length(ArtInfoTabRABGRP_ID.AsString)>0)and
               //(ArtInfoTabPreis.AsFloat=0) then
               (ArtInfoTabAdress_ID.AsInteger<>KunID) then
            begin
                 // Ist ein Artikel mit Rabattgruppe !!!
                 // Brutto mu� berechnet werden

                 // Listenpreis festlegen
                 case AnzPreis of
                     1: PR :=ArtInfoTabVK1.Value;
                     2: PR :=ArtInfoTabVK2.Value;
                     3: PR :=ArtInfoTabVK3.Value;
                     4: PR :=ArtInfoTabVK4.Value;
                   else PR :=ArtInfoTabVK5.Value;
                 end;

                 // nur wenn Rab.Gruppe gefunden
                 if CalcRabGrpPreis (ArtInfoTabRABGRP_ID.AsString,
                                     PE,PR) then
                 begin
                    case ArtInfoTabSTEUER_CODE.AsInteger of
                         1: M :=MWSTTab[1];
                         2: M :=MWSTTab[2];
                         3: M :=MWSTTab[3];
                       else M :=MWSTTab[0];
                    end;
                    M :=M+100; // jetzt z.B. 116

                    B :=CAO_round(PR*M); // jetzt ganze Cent
                    B :=CAO_Round (B / BR_RUND_WERT) * BR_RUND_WERT / 100;
                    Preis :=B;
                 end;
            end;
          end
             else
          begin
            // Netto
            //Kundenpreis pr�fen
            if (ArtInfoTabAdress_ID.AsInteger=KunID) and
               (ArtInfoTabPreis.AsFloat<>0) then
            begin
               // Kundenpreis verwenden
               Preis :=ArtInfoTabPreis.AsFloat;
            end
               else
            begin
               // normalen Preis verwenden
               case PE of
                    0:PR :=ArtInfoTabEK_Preis.Value;
                    1:PR :=ArtInfoTabVK1.Value;
                    2:PR :=ArtInfoTabVK2.Value;
                    3:PR :=ArtInfoTabVK3.Value;
                    4:PR :=ArtInfoTabVK4.Value;
                 else PR :=ArtInfoTabVK5.Value;
               end;
               Preis :=PR;
            end;


            if (ArtInfoTabRABGRP_ID.AsString<>'-')and
               (length(ArtInfoTabRABGRP_ID.AsString)>0)and
               (ArtInfoTabAdress_ID.AsInteger<>KunID) then
            begin
                 // Ist ein Artikel mit Rabattgruppe !!!

                 // Listenpreis festlegen
                 case AnzPreis of
                     1: PR :=ArtInfoTabVK1.Value;
                     2: PR :=ArtInfoTabVK2.Value;
                     3: PR :=ArtInfoTabVK3.Value;
                     4: PR :=ArtInfoTabVK4.Value;
                   else PR :=ArtInfoTabVK5.Value;
                 end;

                 // nur wenn Rab.Gruppe gefunden
                 if CalcRabGrpPreis (ArtInfoTabRABGRP_ID.AsString,
                                     PE,PR)
                  then Preis :=PR;
            end;
          end;
          Result :=True;
     end;
end;
//------------------------------------------------------------------------------
// Liefert True zur�ck wenn in der Warengruppe ein Kalkulationsfaktor festgelegt
// wurde
//------------------------------------------------------------------------------
function tDM1.GetWGRCalcFaktor (Wgr, PreisID : Integer; var Faktor : Double) : Boolean;
var I : Integer;
begin
     Result :=False;
     if (PreisID<1)or(PreisID>5) then exit; // VK geht nur von 1-5 !!!

     if WgrFaktorCache.Wgr=Wgr then
     begin
        Faktor :=WgrFaktorCache.FTab[PreisID];
     end
        else
     begin
       if not WgrTab.Active then WgrTab.Open;

       if (WgrTab.RecordCount>0)and
          (WgrTab.Locate ('ID',Wgr,[])) then
       begin
          for i:=1 to AnzPreis do
          begin
            WgrFaktorCache.FTab[i] :=WgrTab.FieldByName ('VK'+IntToStr(I)+'_FAKTOR').AsFloat;
          end;
          WgrFaktorCache.Wgr :=Wgr;
          Faktor :=WgrFaktorCache.FTab[PreisID];
       end
       else WgrFaktorCache.Wgr :=-1; // Cache ung�ltig da Wgr nicht gefunden

     end;
     Result :=Faktor <> 0;
end;
//------------------------------------------------------------------------------
procedure TDM1.LandTabCalcFields(DataSet: TDataSet);
begin
     if Uppercase(LandTabID.AsString)='DE' then LandTabPOST_CODE.AsString :='D'
     else if Uppercase(LandTabID.AsString)='IT' then LandTabPOST_CODE.AsString :='I'
     else if Uppercase(LandTabID.AsString)='AT' then LandTabPOST_CODE.AsString :='A'
     else if Uppercase(LandTabID.AsString)='BE' then LandTabPOST_CODE.AsString :='B'
     else if Uppercase(LandTabID.AsString)='FR' then LandTabPOST_CODE.AsString :='F'
     else if Uppercase(LandTabID.AsString)='FI' then LandTabPOST_CODE.AsString :='SF'
     else if Uppercase(LandTabID.AsString)='HU' then LandTabPOST_CODE.AsString :='H'
     else if Uppercase(LandTabID.AsString)='LU' then LandTabPOST_CODE.AsString :='L'
     else if Uppercase(LandTabID.AsString)='MT' then LandTabPOST_CODE.AsString :='M'
     else if Uppercase(LandTabID.AsString)='NO' then LandTabPOST_CODE.AsString :='N'
     else if Uppercase(LandTabID.AsString)='PT' then LandTabPOST_CODE.AsString :='P'
     else if Uppercase(LandTabID.AsString)='ES' then LandTabPOST_CODE.AsString :='E'
     else if Uppercase(LandTabID.AsString)='SE' then LandTabPOST_CODE.AsString :='S'
     else if Uppercase(LandTabID.AsString)='IT' then LandTabPOST_CODE.AsString :='I'
     else if Uppercase(LandTabID.AsString)='GB' then LandTabPOST_CODE.AsString :='UK'
     else if Uppercase(LandTabID.AsString)='US' then LandTabPOST_CODE.AsString :='USA'
     else LandTabPOST_CODE.AsString :=LandTabID.AsString;
end;
//------------------------------------------------------------------------------
function GetProjectVersion:string;
var Null,InfoSize,FixInfo:DWord; PFixInfo:PVSFixedFileInfo; Zeiger:Pointer;
begin
     result:='??';
     Null:=0;
     InfoSize:=GetFileVersionInfoSize(PChar(application.exename),Null);
     if InfoSize > 0 then
     begin
        Zeiger:=GetMemory(InfoSize);
        try
          if assigned(Zeiger) then
          begin
             GetFileVersionInfo(PChar(application.exename),Null,InfoSize,Zeiger);
             if VerQueryValue(Zeiger,'\',Pointer(PFixInfo),FixInfo) then
             begin
                result:=Format('%d.%d.%d.%d',
                               [HiWord(PFixInfo^.dwFileVersionMS),
                                LoWord(PFixInfo^.dwFileVersionMS),
                                HiWord(PFixInfo^.dwFileVersionLS),
                                LoWord(PFixInfo^.dwFileVersionLS)]);
             end;
          end;
        finally
           FreeMemory(Zeiger);
        end;
     end;
end;
//------------------------------------------------------------------------------


end.



{


function tmainform.DB_Releaselock(Prefix: string; Lockvar: string): boolean;
begin
 qrLOCK.SQL.Text := 'SELECT RELEASE_LOCK("'+Prefix+'_'+Lockvar+'") AS lockResult;';
 qrLOCK.Execute;
 if(qrLOCK.FieldValues['lockResult'] = '1') then begin
  result := true;
 end else begin
  result := false;
 end;
end;

function tmainform.DB_Releaselock(Prefix: string; Lockvar: string): boolean;
begin
 qrLOCK.SQL.Text := 'SELECT RELEASE_LOCK("'+Prefix+'_'+Lockvar+'") AS lockResult;';
 qrLOCK.Execute;
 if(qrLOCK.FieldValues['lockResult'] = '1') then begin
  result := true;
 end else begin
  result := false;
 end;
end;

function tmainform.DB_Isfreelock(Prefix: string; Lockvar: string): boolean;
begin
 qrLOCK.SQL.Text := 'SELECT IS_FREE_LOCK("'+Prefix+'_'+Lockvar+'") AS lockResult;';
 qrLOCK.Execute;
 if(qrLOCK.FieldValues['lockResult'] = '1') then begin
  result := true;
 end else begin
  result := false;
 end;
end;

function tmainform.DB_Getlock(Prefix: string; Lockvar: string): boolean;
begin
 qrLOCK.SQL.Text := 'SELECT GET_LOCK("'+Prefix+'_'+Lockvar+'",2) AS lockResult;';
 qrLOCK.Execute;
 if(qrLOCK.FieldValues['lockResult'] = '1') then begin
  result := true;
 end else begin
  result := false;
 end;
end;



  Tries to obtain a lock with a name given by the string str,
  with a timeout of timeout seconds.
  Returns 1 if the lock was obtained successfully,
  0 if the attempt timed out, or NULL if an error occurred
  (such as running out of memory or the thread was killed with mysqladmin kill).
  A lock is released when you execute RELEASE_LOCK(),
  execute a new GET_LOCK(), or the thread terminates.
  This function can be used to implement application locks or to simulate
  record locks. It blocks requests by other clients for locks with the same
  name; clients that agree on a given lock string name can use the string to
  perform cooperative advisory locking:
