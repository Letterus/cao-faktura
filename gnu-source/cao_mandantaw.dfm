object MandantAWForm: TMandantAWForm
  Left = 353
  Top = 148
  HelpContext = 100300
  BorderStyle = bsDialog
  Caption = 'Mandantenverwaltung'
  ClientHeight = 355
  ClientWidth = 475
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PC1: TPageControl
    Left = 0
    Top = 0
    Width = 475
    Height = 355
    ActivePage = NewTab
    Align = alClient
    Style = tsFlatButtons
    TabHeight = 18
    TabOrder = 0
    object AuswahlTab: TTabSheet
      Caption = 'AuswahlTab'
      object Label1: TLabel
        Left = 250
        Top = 3
        Width = 69
        Height = 13
        Caption = 'akt. Mandant :'
      end
      object Label2: TLabel
        Left = 250
        Top = 77
        Width = 37
        Height = 13
        Caption = 'Server :'
      end
      object Label3: TLabel
        Left = 250
        Top = 188
        Width = 74
        Height = 13
        Caption = 'Benutzername :'
        FocusControl = NewUserEdi
      end
      object Label4: TLabel
        Left = 250
        Top = 227
        Width = 45
        Height = 13
        Caption = 'Pa�wort :'
        FocusControl = NewPWEdi
      end
      object Label5: TLabel
        Left = 250
        Top = 151
        Width = 59
        Height = 13
        Caption = 'Datenbank :'
        FocusControl = NewMandantNameEdi
      end
      object AktMandantLab: TLabel
        Left = 250
        Top = 17
        Width = 211
        Height = 16
        AutoSize = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label6: TLabel
        Left = 250
        Top = 38
        Width = 58
        Height = 13
        Caption = 'Ausgew�hlt:'
      end
      object NextMandantLab: TLabel
        Left = 250
        Top = 55
        Width = 211
        Height = 16
        AutoSize = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label12: TLabel
        Left = 250
        Top = 115
        Width = 25
        Height = 13
        Caption = 'Port :'
      end
      object UebernehmenBtn: TBitBtn
        Left = 250
        Top = 302
        Width = 105
        Height = 25
        Caption = '�&bernehmen'
        Default = True
        Enabled = False
        TabOrder = 7
        OnClick = UebernehmenBtnClick
        Glyph.Data = {
          DE010000424DDE01000000000000760000002800000024000000120000000100
          0400000000006801000000000000000000001000000000000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
          3333333333333333333333330000333333333333333333333333F33333333333
          00003333344333333333333333388F3333333333000033334224333333333333
          338338F3333333330000333422224333333333333833338F3333333300003342
          222224333333333383333338F3333333000034222A22224333333338F338F333
          8F33333300003222A3A2224333333338F3838F338F33333300003A2A333A2224
          33333338F83338F338F33333000033A33333A222433333338333338F338F3333
          0000333333333A222433333333333338F338F33300003333333333A222433333
          333333338F338F33000033333333333A222433333333333338F338F300003333
          33333333A222433333333333338F338F00003333333333333A22433333333333
          3338F38F000033333333333333A223333333333333338F830000333333333333
          333A333333333333333338330000333333333333333333333333333333333333
          0000}
        NumGlyphs = 2
      end
      object CloseBtn: TBitBtn
        Left = 368
        Top = 302
        Width = 97
        Height = 25
        Caption = 'S&chlie�en'
        TabOrder = 8
        OnClick = CloseBtnClick
        Glyph.Data = {
          DE010000424DDE01000000000000760000002800000024000000120000000100
          0400000000006801000000000000000000001000000000000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00388888888877
          F7F787F8888888888333333F00004444400888FFF444448888888888F333FF8F
          000033334D5007FFF4333388888888883338888F0000333345D50FFFF4333333
          338F888F3338F33F000033334D5D0FFFF43333333388788F3338F33F00003333
          45D50FEFE4333333338F878F3338F33F000033334D5D0FFFF43333333388788F
          3338F33F0000333345D50FEFE4333333338F878F3338F33F000033334D5D0FFF
          F43333333388788F3338F33F0000333345D50FEFE4333333338F878F3338F33F
          000033334D5D0EFEF43333333388788F3338F33F0000333345D50FEFE4333333
          338F878F3338F33F000033334D5D0EFEF43333333388788F3338F33F00003333
          4444444444333333338F8F8FFFF8F33F00003333333333333333333333888888
          8888333F00003333330000003333333333333FFFFFF3333F00003333330AAAA0
          333333333333888888F3333F00003333330000003333333333338FFFF8F3333F
          0000}
        NumGlyphs = 2
      end
      object SrvEdi: TEdit
        Left = 250
        Top = 90
        Width = 215
        Height = 21
        Enabled = False
        TabOrder = 1
      end
      object UsrEdi: TEdit
        Left = 250
        Top = 202
        Width = 215
        Height = 21
        Enabled = False
        TabOrder = 4
      end
      object PasEdi: TEdit
        Left = 250
        Top = 241
        Width = 215
        Height = 21
        Enabled = False
        PasswordChar = '*'
        TabOrder = 5
      end
      object DBEdi: TEdit
        Left = 250
        Top = 164
        Width = 215
        Height = 21
        Enabled = False
        TabOrder = 3
      end
      object MandantLB: TListBox
        Left = 0
        Top = 0
        Width = 233
        Height = 327
        Align = alLeft
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ItemHeight = 16
        ParentFont = False
        TabOrder = 0
        OnClick = MandantCBChange
        OnDblClick = UebernehmenBtnClick
      end
      object NeuBtn: TBitBtn
        Left = 251
        Top = 272
        Width = 103
        Height = 25
        Caption = '&Neu'
        TabOrder = 6
        OnClick = NeuBtnClick
        Glyph.Data = {
          06020000424D0602000000000000760000002800000028000000140000000100
          0400000000009001000000000000000000001000000000000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
          3333333333333333333FFFFF333333333333330000033333333FFFFFFF88888F
          FFF333888888880AAA08883333888888888F3388883F38BFBFBFBF0AAA0FBF83
          38F33333FF8F338FFF8F38FBFBF0000AAA00008338F3333888833388888F38BF
          BFB0AAAAAAAAA08338F33338F3333333388F38FBFBF0AAAAAAAAA08338F33338
          F3333333388F38BFBFB0AAAAAAAAA08338F33338FFFF3333388F38FBFBF0000A
          AA00008338F33338888F3388888F38BFBFBFBF0AAA0FBF8338F33333338F338F
          338F38FBFBFBFB0AAA0BFB8338F33333338FFF8F338F38BFBFBFBF00000FBF83
          38F3333333888883338F38FBFBFBFBFBFBFBFB8338F3333333333333338F38BF
          BFBFBFBFBFBFBF8338F3333333333333338F38FBFBFBFBFBFBFBFB8338FFFFFF
          F3333333338F38888888BFBFBFBFBF83388888883FFFFFFFFF8338FBFBFB8888
          88888833383FFFFF888888888833338888883333333333333388888833333333
          3333333333333333333333333333333333333333333333333333333333333333
          33333333333333333333}
        NumGlyphs = 2
      end
      object PortEdi: TEdit
        Left = 249
        Top = 128
        Width = 215
        Height = 21
        Enabled = False
        TabOrder = 2
      end
      object MandantDelBtn: TBitBtn
        Left = 368
        Top = 272
        Width = 97
        Height = 25
        Caption = '&L�schen'
        TabOrder = 9
        OnClick = MandantDelBtnClick
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          04000000000000010000130B0000130B00001000000000000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
          333333333333333333FF33333333333330003333333333333777333333333333
          300033FFFFFF3333377739999993333333333777777F3333333F399999933333
          3300377777733333337733333333333333003333333333333377333333333333
          3333333333333333333F333333333333330033333F33333333773333C3333333
          330033337F3333333377333CC3333333333333F77FFFFFFF3FF33CCCCCCCCCC3
          993337777777777F77F33CCCCCCCCCC399333777777777737733333CC3333333
          333333377F33333333FF3333C333333330003333733333333777333333333333
          3000333333333333377733333333333333333333333333333333}
        NumGlyphs = 2
      end
    end
    object NewTab: TTabSheet
      Caption = 'NewTab'
      ImageIndex = 1
      object Label7: TLabel
        Left = 0
        Top = 1
        Width = 215
        Height = 20
        Caption = 'neuen Mandanten anlegen'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label8: TLabel
        Left = 2
        Top = 101
        Width = 65
        Height = 13
        AutoSize = False
        Caption = 'Ser&ver :'
        FocusControl = NewServerEdi
      end
      object Label9: TLabel
        Left = 2
        Top = 133
        Width = 65
        Height = 13
        AutoSize = False
        Caption = '&Datenbank :'
        FocusControl = NewDBEdi
      end
      object Label10: TLabel
        Left = 2
        Top = 164
        Width = 65
        Height = 13
        AutoSize = False
        Caption = '&Benutzer :'
        FocusControl = NewUserEdi
      end
      object Label11: TLabel
        Left = 2
        Top = 196
        Width = 65
        Height = 13
        AutoSize = False
        Caption = '&Pa�wort :'
        FocusControl = NewPWEdi
      end
      object Label13: TLabel
        Left = 346
        Top = 101
        Width = 25
        Height = 13
        Caption = 'P&ort :'
        FocusControl = NewPortEdi
      end
      object Label14: TLabel
        Left = 2
        Top = 45
        Width = 65
        Height = 13
        AutoSize = False
        Caption = '&Mandant :'
        FocusControl = NewMandantNameEdi
      end
      object Label15: TLabel
        Left = 2
        Top = 236
        Width = 65
        Height = 13
        AutoSize = False
        Caption = 'Hinweis :'
      end
      object Hinweis: TLabel
        Left = 67
        Top = 237
        Width = 398
        Height = 44
        AutoSize = False
        WordWrap = True
      end
      object NewManAbortBtn: TBitBtn
        Left = 368
        Top = 302
        Width = 97
        Height = 25
        Caption = '&Abbrechen'
        TabOrder = 8
        OnClick = NewManAbortBtnClick
        Glyph.Data = {
          DE010000424DDE01000000000000760000002800000024000000120000000100
          0400000000006801000000000000000000001000000000000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
          333333333333333333333333000033338833333333333333333F333333333333
          0000333911833333983333333388F333333F3333000033391118333911833333
          38F38F333F88F33300003339111183911118333338F338F3F8338F3300003333
          911118111118333338F3338F833338F3000033333911111111833333338F3338
          3333F8330000333333911111183333333338F333333F83330000333333311111
          8333333333338F3333383333000033333339111183333333333338F333833333
          00003333339111118333333333333833338F3333000033333911181118333333
          33338333338F333300003333911183911183333333383338F338F33300003333
          9118333911183333338F33838F338F33000033333913333391113333338FF833
          38F338F300003333333333333919333333388333338FFF830000333333333333
          3333333333333333333888330000333333333333333333333333333333333333
          0000}
        NumGlyphs = 2
      end
      object NewServerEdi: TEdit
        Left = 65
        Top = 98
        Width = 272
        Height = 21
        Hint = 'z.B. localhost oder 127.0.0.1'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnChange = NewServerEdiChange
      end
      object NewDBEdi: TEdit
        Left = 65
        Top = 130
        Width = 397
        Height = 21
        Hint = 'z.B. M001'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 3
        OnChange = NewServerEdiChange
        OnExit = NewDBEdiExit
      end
      object NewUserEdi: TEdit
        Left = 65
        Top = 162
        Width = 397
        Height = 21
        Hint = 'z.B. root'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 4
        OnChange = NewServerEdiChange
      end
      object NewPWEdi: TEdit
        Left = 65
        Top = 194
        Width = 397
        Height = 21
        ParentShowHint = False
        PasswordChar = '*'
        ShowHint = True
        TabOrder = 5
        OnChange = NewServerEdiChange
      end
      object ServerTestBtn: TBitBtn
        Left = 0
        Top = 302
        Width = 137
        Height = 25
        Caption = 'Einstellungen &testen'
        TabOrder = 6
        OnClick = ServerTestBtnClick
        Glyph.Data = {
          E6000000424DE60000000000000076000000280000000F0000000E0000000100
          0400000000007000000000000000000000001000000000000000000000000000
          BF0000BF000000BFBF00BF000000BF00BF00BFBF0000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00FFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFF0087FFFFFFFFFFFF0B3087FFFFFFFFFFF0BB0087FF
          FFFFFFFF0BB3008FFFFFFFFFF0BBB008FFFFFFFFF00BBB007FFFFFFF00BBB007
          FFF0FFFFF00BBB007FF0FFFFFFF00BB007F0FFFFFFFFF00B0070FFFFFFFFFFF0
          00F0FFFFFFFFFFFFFFF0}
      end
      object NewPortEdi: TEdit
        Left = 385
        Top = 98
        Width = 75
        Height = 21
        Hint = 'der Standardport ist 3306'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        OnChange = NewServerEdiChange
      end
      object NewMandantNameEdi: TEdit
        Left = 65
        Top = 42
        Width = 397
        Height = 21
        TabOrder = 0
        OnChange = NewServerEdiChange
        OnExit = NewMandantNameEdiExit
      end
      object SaveBtn: TBitBtn
        Left = 264
        Top = 302
        Width = 95
        Height = 25
        Caption = '&Speichern'
        Enabled = False
        TabOrder = 7
        OnClick = SaveBtnClick
        Glyph.Data = {
          06020000424D0602000000000000760000002800000028000000140000000100
          0400000000009001000000000000000000001000000000000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
          3333333333333333333333333333333333333333333333333333333333333333
          33333333333333333333333333333FFFFFFFFFFFF33333380000000000008333
          33338888888888883F333330CC08CCF770CC03333338F38F333338F38F333330
          CC08CCF770CC03333338F38F333338F38F333330CC07887770CC03333338F38F
          FFFFF8338F333330CC60000006CC03333338F338888883338F333330CCCCCCCC
          CCCC03333338F33FFFFFFFF38F333330C6000000006C03333338F3888888883F
          8F333330C0FFFFFFFF0C03333338F8F33333338F8F333330C0FFFFFFFF0C0333
          3338F8F33333338F8F333330C0FFFFFFFF0C03333338F8F33333338F8F333330
          C0FFFFFFFF0C03333338F8F33333338F8F33333000FFFFFFFF0003333338F8F3
          3333338F8F333330C0FFFFFFFF0C03333338F8FFFFFFFF8F8333333800000000
          0000833333338888888888883333333333333333333333333333333333333333
          3333333333333333333333333333333333333333333333333333333333333333
          33333333333333333333}
        NumGlyphs = 2
      end
    end
  end
  object TestTransact: TZMySqlTransact
    Options = [toHourGlass]
    AutoCommit = True
    Database = TestDB
    TransactSafe = False
    Left = 324
    Top = 256
  end
  object TestTab: TZMySqlQuery
    Transaction = TestTransact
    CachedUpdates = False
    ShowRecordTypes = [ztModified, ztInserted, ztUnmodified]
    Options = [doHourGlass, doQueryAllRecords, doAutoFillDefs, doUseRowId]
    LinkOptions = [loAlwaysResync]
    Constraints = <>
    ExtraOptions = [moStoreResult]
    Macros = <>
    Sql.Strings = (
      'select VAL_CHAR as DB_VERSION from REGISTERY '
      'where MAINKEY='#39'MAIN'#39' and NAME='#39'DB_VERSION'#39)
    RequestLive = False
    Left = 252
    Top = 256
  end
  object TestDB: TZMySqlDatabase
    Port = '3306'
    Encoding = etNone
    LoginPrompt = False
    Connected = False
    Left = 388
    Top = 256
  end
end
