{

CAO-Faktura f�r Windows Version 1.0
Copyright (C) 2003 Jan Pokrandt / Jan@JP-SOFT.de

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

******* CAO-Faktura comes with ABSOLUTELY NO WARRANTY ***************

Programm     : CAO-Faktura / Kasse
Modul        : CAO_KASSE_MAIN
Stand        : 28.02.2004
Version      : 1.2.5.1
Beschreibung : Kasse Hauptunit

History :

31.05.2003 - akt. Version zum CVS hinzugef�gt
31.05.2003 - JP: akt. Version zum CVS hinzugef�gt
21.08.2003 - DP: Funktionen f�r Kundendisplay eingef�gt
16.11.2003 - JP: Treiber (ESC-Sequenzen) in die Registry eingebaut
17.11.2003 - JP: Kassen-Setup eingebaut
28.02.2004 - JP: Druckertreiber f�r Abschneiden und Umlautersetzung erweitert
11.04.2004 - JP: Anzahl der Parameter f�r das Bon-Template erweitert
}

unit cao_kasse_main;

{$I CAO32.INC}

interface

uses
  {$IFDEF PRO}raFunc,ppRTTI,{$ENDIF}
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Menus, ComCtrls, AdvStatusBar, ZTransact, Db, ZQuery, ZMySqlQuery, Grids,
  DBGrids, JvDBCtrl, ExRxDBGrid, ExtCtrls, StdCtrls, DBCtrls, ImgList,
  ToolWin, JvPlacemnt, CaoGroupBox, XPMenu;


type tDruckerTreiber = Record
         Treiber         : String;
         SCHUBLADE_AUF   : String;
         BREIT_ON        : String;
         BREIT_OFF       : String;
         UNTER_ON        : String;
         UNTER_OFF       : String;
         RED_ON          : String;
         RED_OFF         : String;
         DINIT           : String;
         ABSCHNEIDEN     : String;
         HTAB            : String; // Horizontaler Tab (z.B. f�r Journaldruck)
         RLFEED          : String; // R�ckw�rtsschritt (Reverse Line Feed)

         //Umlaute
         UML_G_AE        : String;
         UML_K_AE        : String;
         UML_G_OE        : String;
         UML_K_OE        : String;
         UML_G_UE        : String;
         UML_K_UE        : String;
         UML_SS          : String;
      end;

type
  TKMainForm = class(TForm)
    MainMenu1: TMainMenu;
    Datei1: TMenuItem;
    Mandant1: TMenuItem;
    N1: TMenuItem;
    Einstellungen1: TMenuItem;
    N2: TMenuItem;
    Beenden1: TMenuItem;
    Bearbeiten1: TMenuItem;
    Ansicht1: TMenuItem;
    Hilfe1: TMenuItem;
    lePositionlschen1: TMenuItem;
    N3: TMenuItem;
    Suchen1: TMenuItem;
    N4: TMenuItem;
    Fertigstellen1: TMenuItem;
    N5: TMenuItem;
    letztenBondrucken1: TMenuItem;
    Schubladeffnen1: TMenuItem;
    Info1: TMenuItem;
    SB1: TAdvStatusBar;
    Log1: TMenuItem;
    REgistery1: TMenuItem;
    ZMonitor1: TZMonitor;
    PosDS: TDataSource;
    PosTab: TZMySqlQuery;
    KopfTab: TZMySqlQuery;
    KopfDS: TDataSource;
    KopfTabQUELLE: TIntegerField;
    KopfTabREC_ID: TIntegerField;
    KopfTabQUELLE_SUB: TIntegerField;
    KopfTabADDR_ID: TIntegerField;
    KopfTabATRNUM: TIntegerField;
    KopfTabVRENUM: TIntegerField;
    KopfTabVLSNUM: TIntegerField;
    KopfTabFOLGENR: TIntegerField;
    KopfTabKM_STAND: TIntegerField;
    KopfTabKFZ_ID: TIntegerField;
    KopfTabVERTRETER_ID: TIntegerField;
    KopfTabGLOBRABATT: TFloatField;
    KopfTabADATUM: TDateField;
    KopfTabRDATUM: TDateField;
    KopfTabLDATUM: TDateField;
    KopfTabPR_EBENE: TIntegerField;
    KopfTabLIEFART: TIntegerField;
    KopfTabZAHLART: TIntegerField;
    KopfTabKOST_NETTO: TFloatField;
    KopfTabWERT_NETTO: TFloatField;
    KopfTabLOHN: TFloatField;
    KopfTabWARE: TFloatField;
    KopfTabTKOST: TFloatField;
    KopfTabMWST_0: TFloatField;
    KopfTabMWST_1: TFloatField;
    KopfTabMWST_2: TFloatField;
    KopfTabMWST_3: TFloatField;
    KopfTabNSUMME: TFloatField;
    KopfTabMSUMME_0: TFloatField;
    KopfTabMSUMME_1: TFloatField;
    KopfTabMSUMME_2: TFloatField;
    KopfTabMSUMME_3: TFloatField;
    KopfTabMSUMME: TFloatField;
    KopfTabBSUMME: TFloatField;
    KopfTabATSUMME: TFloatField;
    KopfTabATMSUMME: TFloatField;
    KopfTabWAEHRUNG: TStringField;
    KopfTabGEGENKONTO: TIntegerField;
    KopfTabSOLL_STAGE: TIntegerField;
    KopfTabSOLL_SKONTO: TFloatField;
    KopfTabSOLL_NTAGE: TIntegerField;
    KopfTabSOLL_RATEN: TIntegerField;
    KopfTabSOLL_RATBETR: TFloatField;
    KopfTabSOLL_RATINTERVALL: TIntegerField;
    KopfTabIST_ANZAHLUNG: TFloatField;
    KopfTabIST_SKONTO: TFloatField;
    KopfTabIST_ZAHLDAT: TDateField;
    KopfTabIST_BETRAG: TFloatField;
    KopfTabMAHNKOSTEN: TFloatField;
    KopfTabKONTOAUSZUG: TIntegerField;
    KopfTabBANK_ID: TIntegerField;
    KopfTabSTADIUM: TIntegerField;
    KopfTabERSTELLT: TDateField;
    KopfTabERST_NAME: TStringField;
    KopfTabKUN_NUM: TStringField;
    KopfTabKUN_ANREDE: TStringField;
    KopfTabKUN_NAME1: TStringField;
    KopfTabKUN_NAME2: TStringField;
    KopfTabKUN_NAME3: TStringField;
    KopfTabKUN_ABTEILUNG: TStringField;
    KopfTabKUN_STRASSE: TStringField;
    KopfTabKUN_LAND: TStringField;
    KopfTabKUN_PLZ: TStringField;
    KopfTabKUN_ORT: TStringField;
    KopfTabUSR1: TStringField;
    KopfTabUSR2: TStringField;
    KopfTabPROJEKT: TStringField;
    KopfTabORGNUM: TStringField;
    KopfTabBEST_NAME: TStringField;
    KopfTabBEST_CODE: TIntegerField;
    KopfTabINFO: TMemoField;
    KopfTabUW_NUM: TIntegerField;
    KopfTabBEST_DATUM: TDateField;
    PosTabREC_ID: TIntegerField;
    PosTabQUELLE: TIntegerField;
    PosTabQUELLE_SUB: TIntegerField;
    PosTabJOURNAL_ID: TIntegerField;
    PosTabARTIKELTYP: TStringField;
    PosTabARTIKEL_ID: TIntegerField;
    PosTabADDR_ID: TIntegerField;
    PosTabVRENUM: TIntegerField;
    PosTabVLSNUM: TIntegerField;
    PosTabPOSITION: TIntegerField;
    PosTabMATCHCODE: TStringField;
    PosTabARTNUM: TStringField;
    PosTabBARCODE: TStringField;
    PosTabMENGE: TFloatField;
    PosTabLAENGE: TStringField;
    PosTabGROESSE: TStringField;
    PosTabDIMENSION: TStringField;
    PosTabGEWICHT: TFloatField;
    PosTabME_EINHEIT: TStringField;
    PosTabPR_EINHEIT: TFloatField;
    PosTabEPREIS: TFloatField;
    PosTabE_RGEWINN: TFloatField;
    PosTabRABATT: TFloatField;
    PosTabSTEUER_CODE: TIntegerField;
    PosTabALTTEIL_PROZ: TFloatField;
    PosTabALTTEIL_STCODE: TIntegerField;
    PosTabGEGENKTO: TIntegerField;
    PosTabBEZEICHNUNG: TMemoField;
    PosTabVIEW_POS: TStringField;
    PosTabATRNUM2: TIntegerField;
    Panel2: TPanel;
    ExRxDBGrid1: TExRxDBGrid;
    ImageList1: TImageList;
    ArtikelTab1: TZMySqlQuery;
    ArtikelTab1REC_ID: TIntegerField;
    ArtikelTab1ARTIKELTYP: TStringField;
    ArtikelTab1ARTNUM: TStringField;
    ArtikelTab1MATCHCODE: TStringField;
    ArtikelTab1BARCODE: TStringField;
    ArtikelTab1KAS_NAME: TStringField;
    ArtikelTab1LAENGE: TStringField;
    ArtikelTab1GROESSE: TStringField;
    ArtikelTab1DIMENSION: TStringField;
    ArtikelTab1GEWICHT: TFloatField;
    ArtikelTab1EK_PREIS: TFloatField;
    ArtikelTab1STEUER_CODE: TIntegerField;
    ArtikelTab1ME_EINHEIT: TStringField;
    ArtikelTab1PR_EINHEIT: TFloatField;
    ArtikelTab1WARENGRUPPE: TIntegerField;
    ArtikelTab1ERLOES_KTO: TIntegerField;
    ArtikelTab1AUFW_KTO: TIntegerField;
    FormPlacement1: TjvFormPlacement;
    Suchmodus1: TMenuItem;
    Barcode1: TMenuItem;
    Matchcode1: TMenuItem;
    Artikelnummer1: TMenuItem;
    PosTabMwstProz: TFloatField;
    Panel1: TPanel;
    ToolBar1: TToolBar;
    StornoBtn: TToolButton;
    ToolButton3: TToolButton;
    AddBtn: TToolButton;
    ToolButton5: TToolButton;
    BonPrintBtn: TToolButton;
    ToolButton2: TToolButton;
    BonFertigBtn: TToolButton;
    ToolButton1: TToolButton;
    ToolButton4: TToolButton;
    CaoGroupBox1: TCaoGroupBox;
    Edit1: TEdit;
    Panel3: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Panel4: TPanel;
    DBText6: TDBText;
    Panel5: TPanel;
    DBText4: TDBText;
    Panel6: TPanel;
    DBText1: TDBText;
    ImageList2: TImageList;
    PosTabGEBUCHT: TBooleanField;
    PosTabALTTEIL_FLAG: TBooleanField;
    KopfTabFREIGABE1_FLAG: TBooleanField;
    ArtikelTab1RABGRP_ID: TStringField;
    ArtikelTab1NO_RABATT_FLAG: TBooleanField;
    ArtikelTab1NO_BEZEDIT_FLAG: TBooleanField;
    ArtikelTab1NO_VK_FLAG: TBooleanField;
    ArtikelTab1ALTTEIL_FLAG: TBooleanField;
    PosTabBEZ_FEST_FLAG: TBooleanField;
    KopfTabTERM_ID: TIntegerField;
    Label5: TLabel;
    MengeLab: TLabel;
    KopfTabBRUTTO_FLAG: TBooleanField;
    PosTabSN_FLAG: TBooleanField;
    PosTabBRUTTO_FLAG: TBooleanField;
    ArtikelTab1VK1B: TFloatField;
    ArtikelTab1VK2B: TFloatField;
    ArtikelTab1VK3B: TFloatField;
    ArtikelTab1VK4B: TFloatField;
    ArtikelTab1VK5B: TFloatField;
    ArtikelTab1SN_FLAG: TBooleanField;
    ArtikelTab1VK1: TFloatField;
    ArtikelTab1VK2: TFloatField;
    ArtikelTab1VK3: TFloatField;
    ArtikelTab1VK4: TFloatField;
    ArtikelTab1VK5: TFloatField;
    PosTabNSUMME: TFloatField;
    PosTabMSUMME: TFloatField;
    PosTabBSUMME: TFloatField;
    PosTabPROVIS_PROZ: TFloatField;
    PosTabPROVIS_WERT: TFloatField;
    KopfTabPROVIS_WERT: TFloatField;
    PosTabVPE: TIntegerField;
    SNTab: TZMySqlQuery;
    SNTabARTIKEL_ID: TIntegerField;
    SNTabSERNUMMER: TStringField;
    SNTabVERK_NUM: TIntegerField;
    SNTabVK_JOURNAL_ID: TIntegerField;
    SNTabVK_JOURNALPOS_ID: TIntegerField;
    SNDS: TDataSource;
    EinstellungenKasse1: TMenuItem;
    N6: TMenuItem;
    XPMenu1: TXPMenu;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure Wechseln1Click(Sender: TObject);
    procedure Beenden1Click(Sender: TObject);
    procedure ZMonitor1MonitorEvent(Sql, Result: string);
    procedure Einstellungen1Click(Sender: TObject);
    procedure Log1Click(Sender: TObject);
    procedure REgistery1Click(Sender: TObject);
    procedure PosTabCalcFields(DataSet: TDataSet);
    procedure KopfTabAfterOpen(DataSet: TDataSet);
    procedure PosTabNewRecord(DataSet: TDataSet);
    procedure PosTabAfterPost(DataSet: TDataSet);
    procedure StornoBtnClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BonFertigBtnClick(Sender: TObject);
    procedure letztenBondrucken1Click(Sender: TObject);
    procedure Schubladeffnen1Click(Sender: TObject);
    procedure Suchen1Click(Sender: TObject);
    procedure Edit1KeyPress(Sender: TObject; var Key: Char);
    procedure Artikelnummer1Click(Sender: TObject);
    procedure Edit1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure PosTabAfterScroll(DataSet: TDataSet);
    procedure PosTabBeforePost(DataSet: TDataSet);
    procedure PosTabBeforeDelete(DataSet: TDataSet);
    procedure EinstellungenKasse1Click(Sender: TObject);
  private
    { Private-Deklarationen }
    ID              : Integer;
    FromPosNewRec   : Boolean; // Temp. Var.
    SuchFeld        : String;  // Aktuelles Suchfeld im Artikelstamm
    Menge           : Double;//Integer; // Menge f�r n�. Position
    LastBonNum      : Integer;
    USEBonDrucker   : Boolean; // Beondrucker benutzen
    DPort           : String;  // Druckerport f�r Bondrucker
    DefFormular     : String;  // Name des Standard-Formulares
    DefDrucker      : String;  // Name des Standard-Druckers
    ShowDruckDialog : Boolean; // Druckdialog anzeigen
    AutoPrint       : Boolean; // Drucken nach Fertigstellen
    LastKey         : Word;

    //Druckertreiber
    DT                 : tDruckerTreiber;
    DruckerTreiber     : String;

    procedure OnAddArtikelToKasse(ID: Integer; Menge: Double);
    procedure AddArtikel (Menge : Double; Eingabe : String; WGR : Integer);
    procedure MakeBon(Num: Integer; geg, rueck: double);
  public
    { Public-Deklarationen }
    PEbene   : Byte;    // aktuelle Preisebene
    Gebucht  : Boolean;
    waehrung : string; // W�hrungskurzzeichen (EUR / DEM / USD)
    OpenSchublade : Boolean;

    PosTemplateMengeN1 : String;
    PosTemplateMenge1  : String;
    BonTemplate        : String;

    // Kopfzeilen
    BON_HDR1           : String;
    BON_HDR2           : String;
    BON_HDR3           : String;
    // Fu�zeilen
    BON_FTR1           : String;
    BON_FTR2           : String;
    BON_FTR3           : String;
    // Freifelder
    BON_USR1           : String;
    BON_USR2           : String;
    BON_USR3           : String;
    BON_USR4           : String;
    BON_USR5           : String;

    procedure RecalcPreise(Faktor: Double);
    procedure BucheBon(Zahlart: Integer; Gegeben, Zuruck: Double);
    procedure ZeigeBild(BildName: string; Values: array of const; FuncName:
      string);
  end;

//const // Steuercodes f�r den Bondrucker
  {
  SchubladeAuf: string = #$1B#$07#50#50#$1C; // Schublade aufmachen
  breiton: string = #$0E; // Breitschrift an
  breitoff: string = #$14; // Breitschrift aus
  unteron: string = #$1B#$2D#$01; // Unterstreichen an
  unteroff: string = #$1B#$2D#$00; // Unterstreichen aus
  redon: string = #$1B#$34; // Rotschrift an
  redoff: string = #$1B#$35; // Rotschrift aus
  dinit: string = #$1B#$52#$02; // INIT
  }


const cPosTemplateMengeN1 : String ='%3:4.1f x %2:-20s a %4:8.2f *'+#13#10+
                                     '                            = %5:10.2f';
      cPosTemplateMenge1  : String ='%3:4.1f x %2:-20s = %5:10.2f';

      cBonTemplate : String =
       '@DINIT@@BREIT_ON@%0:-20S@BREIT_OFF@'                              +#13#10+
       '@BREIT_ON@%1:-20S@BREIT_OFF@'                                     +#13#10+
       '%2:-40S'                                                          +#13#10+
       #13#10+
       '@BREIT_ON@Rechnung %11:4D@BREIT_OFF@    %12:-8s'                  +#13#10+
       #13#10+
       'MENGE  BEZEICHNUNG                 PREIS'                         +#13#10+
       '----------------------------------------'                         +#13#10+
       '@POSITIONEN@'                                                     +#13#10+
       '----------------------------------------'                         +#13#10+
       '@BREIT_ON@Gesamt: %13:-3s %14:8.2f@BREIT_OFF@'                    +#13#10+
       #13#10+
       'MwSt(%15:2.0f%%)=%16:7.2f %13:-3S  (%17:2.0f%%)=%18:7.2f %13:-3s' +#13#10+
       'Gegeben:%19:9.2f %13:-3s  Zur�ck:%20:6.2f %13:-3s'                +#13#10+
       #13#10+
       '  *** Vielen Dank f�r Ihren Besuch ***  '                         +#13#10+
       '@ABSCHNEIDEN@';

type
  TKD_BILD = function(BildName: string; Values: array of const): boolean;
  TKD_INIT = function: boolean;
  TKD_EXIT = function: boolean;

var
  KMainForm: TKMainForm;
  DLLHandle: THandle;
  FuncPTR: TFarProc;
  KD_BILD: TKD_BILD;
  KD_INIT: TKD_INIT;
  KD_EXIT: TKD_EXIT;
  KD_OK: boolean;

procedure drucke_liste(dname: string; dr_name: string);

implementation

{$R *.DFM}

uses cao_dm, cao_mandantaw, CAO_Logging, CAO_Setup, CAO_RegEdit, CAO_VAR_CONST
     {$IFDEF REPORTBUILDER}, CAO_KASSE_PRINT{$ENDIF}
     ,CAO_KASSE_ARTIKEL, cao_kasse_bonfertig, Printers,cao_sn_auswahl,
     cao_tool1, jclstrings, CAO_KASSE_SETUP;

const
  ArtInfoSqlS =
   'select '+
   'A.REC_ID,A.EK_PREIS,'+
   'A.VK1,A.VK2,A.VK3,A.VK4,A.VK5,A.VK1B,A.VK2B,A.VK3B,A.VK4B,A.VK5B, '+
   'AP.PREIS,AP.MENGE2,AP.PREIS2,AP.MENGE3,AP.PREIS3,AP.MENGE4,AP.PREIS4, '+
   'AP.MENGE5,AP.PREIS5,A.MENGE_AKT,A.MENGE_BESTELLT, '+
   'A.RABGRP_ID,A.MENGE_VKRE_EDI as MENGE_RESERVIERT, '+
   'A.ALTTEIL_FLAG,A.NO_RABATT_FLAG,A.NO_PROVISION_FLAG,A.NO_BEZEDIT_FLAG,'+
   'A.NO_VK_FLAG,A.SN_FLAG,A.PROVIS_PROZ,A.STEUER_CODE,A.ERLOES_KTO,A.AUFW_KTO,'+
   'A.ARTNUM,A.ERSATZ_ARTNUM,A.MATCHCODE,A.WARENGRUPPE,A.BARCODE,A.ARTIKELTYP,'+
   'A.KAS_NAME,A.ME_EINHEIT,A.PR_EINHEIT,A.VPE,A.KAS_NAME,'+
   'A.LAENGE,A.GROESSE,A.DIMENSION,A.GEWICHT,A.INFO,KURZNAME,LANGNAME, '+
   'AP.PREIS_TYP,AP.ADRESS_ID,AP.BESTNUM ';
  ArtInfoSQLW1 =
   ',0.00 as MENGE_LIEF, 0.00 as MENGE_SOLL, A.REC_ID as JID '+
   'from ARTIKEL A '+
   'left outer join ARTIKEL_PREIS AP on AP.ARTIKEL_ID=A.REC_ID '+
   'where A.REC_ID=:AID '+
   'group by A.REC_ID, AP.PREIS_TYP, AP.ADRESS_ID';

//******************************************************************************
procedure TKMainForm.ZeigeBild(BildName: string; Values: array of const;
  FuncName: string);
begin
  if not KD_OK then exit; // Abbruch wenn DLL nicht OK !!!

  if not KD_BILD(BildName, Values) then
    MessageDlg('Ausgabeformat f�r Kundendisplay ung�ltig!' + #13 + #10 +
      BildName + ': ' + FuncName, mtWarning, [mbOK], 0);

end;
//******************************************************************************
procedure TKMainForm.FormCreate(Sender: TObject);
//var w: integer;
begin
     Scaled:=TRUE;
     //w :=Screen.Width;
     //if w>1024 then w :=1024;
     //if Screen.Width <> 800 then ScaleBy(w,800);
     SuchFeld :='BARCODE';
     KD_OK    :=False;
end;
//******************************************************************************
procedure TKMainForm.FormShow(Sender: TObject);
var T : String; LastKDOK : Boolean;
begin
     LastKDOK :=KD_OK;
     KD_OK := true;
     if (length(DM1.DisplayDLL)>0)and(fileexists(DM1.DisplayDLL)) then
     begin
        if LastKDOK then
        begin
           KD_EXIT;
           try
              FreeLibrary(DLLHandle);
           except
              MessageDlg('Fehler beim entladen der Funktionen f�r das '+
                         'Kundendisplay aus '+#13#10+
                         'DLL : '+DM1.DisplayDLL, mtWarning, [mbOK], 0);
           end;
        end;

        DLLHandle := LoadLibrary(PChar(DM1.DisplayDLL));
        // KD_INIT aus DLL laden
        FuncPTR := GetProcAddress(DLLHandle, 'KD_INIT');
        if FuncPTR <> nil then @KD_INIT := FuncPTR else KD_OK := false;

        FuncPTR := nil;
        // KD_EXIT aus DLL laden
        FuncPTR := GetProcAddress(DLLHandle, 'KD_EXIT');
        if FuncPTR <> nil then @KD_EXIT := FuncPTR else KD_OK := false;

        FuncPTR := nil;
        // KD_BILD aus DLL laden
        FuncPTR := GetProcAddress(DLLHandle, 'KD_BILD');
        if FuncPTR <> nil then @KD_BILD := FuncPTR else KD_OK := false;

        if KD_OK then
        begin
          if not KD_INIT then
            MessageDlg('Fehler beim initialisieren des Kundendisplay''s', mtWarning,
              [mbOK], 0) else
          begin
              ZeigeBild('BILD00', [], 'initialization');
          end;
        end
           else
        begin
          MessageDlg('Fehler beim laden der Funktionen f�r das Kundendisplay aus '
            + #13 + #10 + 'DLL : ' + DM1.DisplayDLL, mtWarning, [mbOK], 0);
        end;
     end
     else KD_OK :=False;

     DM1.C2Color   :=$00DDFFFF; // hellgelb
     DM1.EditColor :=$00A0FFFF;

     FromPosNewRec :=False;
     ExRXDBGrid1.RowColor1 :=DM1.C2Color;
     if DM1.AktMandant<>''
      then DM1.OpenMandant (DM1.AktMandant, Application.Name, False);

     if (DM1.AktMandant<>'')and(DM1.MandantOK) then
     begin
       DM1.InitMandantAfterOpen;

       DM1.CheckTerminalID; // Terminal-Nummer auslesen und ggf. neu setzen

       dport :=dm1.ReadString ('KASSE','DRUCKERPORT','@@@ERROR@@@');
       if DPOrt='@@@ERROR@@@' then
       begin
            DPort :='LPT2';
            dm1.writestring ('KASSE','DRUCKERPORT',DPort);
       end;

       USEBonDrucker   :=DM1.ReadBoolean ('KASSE','BONDRUCKER',True);
       DefFormular     :=DM1.ReadString  ('KASSE','DEFAULT_FORMULAR','');
       DefDrucker      :=DM1.ReadString  ('KASSE','DEFAULT_DRUCKER','');
       ShowDruckDialog :=DM1.ReadBoolean ('KASSE','DRUCKDIALOG',True);
       AutoPrint       :=DM1.ReadBoolean ('KASSE','SOFORTDRUCK', False);
       OpenSchublade   :=DM1.ReadBoolean ('KASSE','SCHUBLADE_OEFFNEN',True);


       BonTemplate     :=DM1.ReadLongString ('KASSE','BON_TEMPLATE','');
       if BonTemplate ='' then
       begin
         BonTemplate :=cBonTemplate;
         DM1.WriteLongString('KASSE','BON_TEMPLATE',BonTemplate);
       end;

       PosTemplateMenge1  :=DM1.ReadLongString ('KASSE','BON_POSZEILE_M1','');
       if PosTemplateMenge1 ='' then
       begin
         PosTemplateMenge1 :=cPosTemplateMenge1;
         DM1.WriteLongString('KASSE','BON_POSZEILE_M1',PosTemplateMenge1);
       end;

       PosTemplateMengeN1 :=DM1.ReadLongString ('KASSE','BON_POSZEILE_MX','');
       if PosTemplateMengeN1 ='' then
       begin
         PosTemplateMengeN1 :=cPosTemplateMengeN1;
         DM1.WriteLongString('KASSE','BON_POSZEILE_MX',PosTemplateMengeN1);
       end;


       // Druckertreiber laden
       if UseBonDrucker then
       begin
          DruckerTreiber :=DM1.ReadString ('KASSE','DRUCKERTREIBER','');
          T :='TREIBER\BONDRUCKER\'+DruckerTreiber;
          if (length(DruckerTreiber)>0)and
             (length(DM1.ReadString(T,'DEFAULT',''))>0) then
          begin
             DT.SCHUBLADE_AUF :=StrEscapedToString(DM1.ReadString(T,'SCHUBLADE_AUF',''));
             DT.BREIT_ON      :=StrEscapedToString(DM1.ReadString(T,'BREIT_EIN',''));
             DT.BREIT_OFF     :=StrEscapedToString(DM1.ReadString(T,'BREIT_AUS',''));
             DT.UNTER_ON      :=StrEscapedToString(DM1.ReadString(T,'UNTER_EIN',''));
             DT.UNTER_OFF     :=StrEscapedToString(DM1.ReadString(T,'UNTER_AUS',''));
             DT.RED_ON        :=StrEscapedToString(DM1.ReadString(T,'ROT_EIN',''));
             DT.RED_OFF       :=StrEscapedToString(DM1.ReadString(T,'ROT_AUS',''));
             DT.DINIT         :=StrEscapedToString(DM1.ReadString(T,'INIT',''));
             DT.ABSCHNEIDEN   :=StrEscapedToString(DM1.ReadString(T,'ABSCHNEIDEN',''));

             DT.HTAB          :=StrEscapedToString(DM1.ReadString(T,'HTAB',''));
             DT.RLFEED        :=StrEscapedToString(DM1.ReadString(T,'RLFEED',''));

             //DE-Umlaute
             DT.UML_G_AE      :=StrEscapedToString(DM1.ReadString(T,'UMLAUT_GR_AE','�'));
             DT.UML_K_AE      :=StrEscapedToString(DM1.ReadString(T,'UMLAUT_KL_AE','�'));
             DT.UML_G_OE      :=StrEscapedToString(DM1.ReadString(T,'UMLAUT_GR_OE','�'));
             DT.UML_K_OE      :=StrEscapedToString(DM1.ReadString(T,'UMLAUT_KL_OE','�'));
             DT.UML_G_UE      :=StrEscapedToString(DM1.ReadString(T,'UMLAUT_GR_UE','�'));
             DT.UML_K_UE      :=StrEscapedToString(DM1.ReadString(T,'UMLAUT_KL_UE','�'));
             DT.UML_SS        :=StrEscapedToString(DM1.ReadString(T,'UMLAUT_SS'   ,'�'));
          end;
       end;

       // Kopf und Fu� f�r Bon
       BON_HDR1 :=DM1.ReadString ('KASSE','BON_UEBERSCHRIFT1','�berschrift 1');
       BON_HDR2 :=DM1.ReadString ('KASSE','BON_UEBERSCHRIFT2','�berschrift 2');
       BON_HDR3 :=DM1.ReadString ('KASSE','BON_UEBERSCHRIFT3','�berschrift 3');

       BON_FTR1 :=DM1.ReadString ('KASSE','BON_UNTERSCHRIFT1','Unterschrift 1');
       BON_FTR2 :=DM1.ReadString ('KASSE','BON_UNTERSCHRIFT2','Unterschrift 2');
       BON_FTR3 :=DM1.ReadString ('KASSE','BON_UNTERSCHRIFT3','Unterschrift 3');

       BON_USR1 :=DM1.ReadString ('KASSE','BON_USERFELD_1','Benutzerfeld 1');
       BON_USR2 :=DM1.ReadString ('KASSE','BON_USERFELD_2','Benutzerfeld 2');
       BON_USR3 :=DM1.ReadString ('KASSE','BON_USERFELD_3','Benutzerfeld 3');
       BON_USR4 :=DM1.ReadString ('KASSE','BON_USERFELD_4','Benutzerfeld 4');
       BON_USR5 :=DM1.ReadString ('KASSE','BON_USERFELD_5','Benutzerfeld 5');


       Pebene :=dm1.ReadInteger ('KASSE','PREISEBENE',0);
       if Pebene<1 then
       begin
            Pebene :=5;
            dm1.WriteInteger ('KASSE','PREISEBENE',Pebene);
       end;

       SB1.Panels[5].Text :='Preis : VK'+Inttostr(PEbene);

       SuchFeld :=DM1.ReadString ('KASSE','SUCHFELD','BARCODE');
       if SuchFeld='MATCHCODE' then Matchcode1.Checked :=True else
       if SuchFeld='ARTNUM' then Artikelnummer1.Checked :=True
       else Barcode1.Checked :=True;

       LastBonNum :=-1;
       BonPrintBtn.Enabled :=UseBonDrucker;
       letztenBondrucken1.Enabled :=UseBonDrucker;

       SB1.Panels[0].Text :='akt. Benutzer :'+dm1.view_user;
       sb1.panels[1].text :=dm1.leitwaehrung;

       KopfTabNSUMME.DisplayFormat :=',#0.00 "'+DM1.LeitWaehrung+' "';
       KopfTabMSUMME.DisplayFormat :=',#0.00 "'+DM1.LeitWaehrung+' "';
       KopfTabBSUMME.DisplayFormat :=',#0.00 "'+DM1.LeitWaehrung+' "';


       Caption :=Application.Title+
                ' ('+
                GetProjectVersion+
                ') Mandant : '+
                DM1.AktMandant+
                '              � 2004 JP-SOFT / JP';

       PosTab.Open;
       KopfTab.ParamByName ('TID').AsInteger :=DM1.TermID;
       KopfTab.Open;

       KasseArtikelForm.OnAddArtikel :=OnAddArtikelToKasse;

       waehrung := DM1.LeitWaehrung;
       if waehrung = '�' then waehrung:= 'EUR';
     end
        else
     begin
       Caption :=Application.Title+
                ' ('+GetProjectVersion+') kein Mandant ausgew�hlt'+
                '              � 2004 JP-SOFT / JP';
     end;

     SB1.Panels[4].Text :=FormatDateTime ('dddd, dd. mmmm yyyy',now);
     FormResize(Sender);

     Menge :=1;

     KasseArtikelForm.MainMenu1.Images :=ImageList2;
     KasseArtikelForm.ArtikelToolBar1.Images :=ImageList2;

     // neu Zeichnen lassen (Auto-Spalten-Breite)
     ExRXDbGrid1.Align :=alNone;
     ExRXDbGrid1.Width :=ExRXDbGrid1.Width - 10;
     Application.ProcessMessages;
     ExRXDbGrid1.Align :=alClient;

     FormResize(Sender);
end;
//******************************************************************************
procedure TKMainForm.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
    if MessageDlg('Wollen Sie die Kasse wirklich beenden ?',
      mtconfirmation, mbyesnocancel, 0) = mryes then
    begin
      PosTab.Close;
      KopfTab.Close;
      ZMonitor1.Transaction := nil;
      DM1.DB1.Disconnect;
      CanClose := True;

      ZeigeBild ('BILD40',[],'KMainForm.FormCloseQuery');

    end else CanClose := False;
end;
//******************************************************************************
procedure TKMainForm.FormResize(Sender: TObject);
begin
     SB1.Panels[2].Text := Inttostr(Width) + 'x' + Inttostr(Height);
     SB1.Top := Height + 10;
end;
//******************************************************************************
procedure TKMainForm.Wechseln1Click(Sender: TObject);
var Old: string;  MandantAWForm: TMandantAWForm;
begin
     Old := DM1.AktMandant;

     MandantAWForm:=TMandantAWForm.Create(Self);
     try
        MandantAWForm.Left :=Left+200;
        MandantAWForm.Top  :=Top +150;
        MandantAWForm.ShowModal;
     finally
        MandantAWForm.Free;
     end;
     
     if Old <> DM1.AktMandant then FormShow(Sender);
end;
//******************************************************************************
procedure TKMainForm.Beenden1Click(Sender: TObject);
begin
     Close;
end;
//******************************************************************************
procedure TKMainForm.ZMonitor1MonitorEvent(Sql, Result: string);
var d : string;
    f : textfile;
begin
     if not DM1.SqlLog then exit;
     try
        if assigned(LogForm)
          then logform.addlog ('SQL:'+sql+#13#10+'RES:'+Result+#13#10);

        if (uppercase(dm1.user)='JAN')and
           (uppercase(dm1.Comp)='JPC') then
        begin
           d :=DM1.LogDir+'cao_sql_'+formatdatetime ('dd_mm_yyyy',now)+'.log';
           if not fileexists (d) then fileclose (filecreate (d));
           assignfile (F,D);
           append (f);
           try
              writeln (f,'SQL:'+sql+#13#10+'RES:'+Result);
           finally
              closefile (f);
           end;
        end;
     except end;
end;
//******************************************************************************
procedure TKMainForm.Einstellungen1Click(Sender: TObject);
var SetupForm : tSetupForm;
begin
     SetupForm :=tSetupForm.Create(Self);
     try
        SetupForm.ShowModal;
     finally
        FreeAndNil(SetupForm);
     end;
end;
//------------------------------------------------------------------------------
procedure TKMainForm.EinstellungenKasse1Click(Sender: TObject);
begin
     KasseSetupForm.ShowModal;
end;
//------------------------------------------------------------------------------
procedure TKMainForm.Log1Click(Sender: TObject);
begin
     LogForm.Show;
end;
//------------------------------------------------------------------------------
procedure TKMainForm.REgistery1Click(Sender: TObject);
var RegEdiForm : tRegEdiForm;
begin
     RegEdiForm :=tRegEdiForm.Create(Self);
     try
        RegEdiForm.ShowModal;
     finally
        FreeAndNil(RegEdiForm);
     end;
end;
//------------------------------------------------------------------------------
procedure TKMainForm.PosTabCalcFields(DataSet: TDataSet);
var Summe, Steuer : Double;
begin
     Summe :=PosTabEPreis.Value * PosTabMenge.Value;
     if PosTabRabatt.Value <> 0
      then Summe :=Summe - Summe * PosTabRabatt.Value / 100;

     Case PosTabSteuer_Code.Value of
        0: Steuer :=KopfTabMwst_0.Value;
        1: Steuer :=KopfTabMwst_1.Value;
        2: Steuer :=KopfTabMwst_2.Value;
        3: Steuer :=KopfTabMwst_3.Value;
        else Steuer :=0;
     end;

     PosTabMwstProz.AsFloat :=Steuer;

     if not KopfTabBrutto_Flag.AsBoolean then
     begin
        PosTabNSumme.Value :=cao_round(Summe*100)/100;  // Auf ganze Pfennige Runden
        PosTabMSumme.Value :=cao_round(Summe * (Steuer / 100) *100)/100; // Auf ganze Pfennige Runden
        PosTabBSumme.Value :=PosTabNSumme.Value+PosTabMSumme.Value;
     end
        else
     begin
        PosTabBSumme.Value :=cao_round(Summe*100)/100;  // Auf ganze Pfennige Runden
        PosTabMSumme.Value :=cao_round(Summe / (100 + Steuer) * Steuer * 100)/100; // Auf ganze Pfennige Runden
        PosTabNSumme.Value :=PosTabBSumme.Value-PosTabMSumme.Value;
     end;
end;
//------------------------------------------------------------------------------
procedure TKMainForm.KopfTabAfterOpen(DataSet: TDataSet);
begin
     if FromPosNewRec then exit;

     if KopfTab.RecordCount = 1 then
     begin
        PosTab.Close;
        ID := KopfTabRec_ID.Value;
        PosTab.ParamByName('ID').Value := ID;
        PosTab.Open;
        PosTab.Last;
     end
        else
     begin
        ID := -1;
        PosTab.Close;
        PosTab.ParamByName('ID').Value := ID;
        PosTab.Open;
     end;
end;
//------------------------------------------------------------------------------
procedure TKMainForm.PosTabNewRecord(DataSet: TDataSet);
begin
     if id=-1 then
     begin
       // Kopf anlegen
       FromPosNewRec :=True;
       try
         KopfTab.Append;
       finally
         FromPosNewRec :=False;
       end;

       KopfTabTERM_ID.AsInteger  :=DM1.TermID;


       KopfTabBrutto_Flag.Value  :=True;
       KopfTabQuelle.Value       :=VK_RECH_EDI;
       KopfTabQUELLE_SUB.Value   :=2; // Kasse
       KopfTabVLSNUM.Value       :=-1;
       KopfTabATRNUM.Value       :=-1;
       KopfTabGegenKonto.Value   :=-1;
       KopfTabADDR_ID.Value      :=-2; // Kassenverkauf, kein Kunde
       KopfTabRDatum.Value       :=Trunc (Now);
       KopfTabLDatum.Value       :=0;
       KopfTabADatum.Value       :=0;
       KopfTabKOST_NETTO.Value   :=0;
       KopfTabWERT_NETTO.Value   :=0;
       KopfTabLOHN.Value         :=0;
       KopfTabWARE.Value         :=0;
       KopfTabTKOST.Value        :=0;
       KopfTabNSUMME.Value       :=0;
       KopfTabMSUMME.Value       :=0;
       KopfTabBSUMME.Value       :=0;
       KopfTabATMSUMME.Value     :=0;
       KopfTabWaehrung.Value     :=DM1.LeitWaehrung;
       KopfTabPR_Ebene.Value     :=PEbene;
       KopfTabFolgeNr.Value      :=-1;
       KopfTabKM_Stand.Value     :=-1;
       KopfTabKFZ_ID.Value       :=-1;
       KopfTabVertreter_ID.Value :=-1;
       KopfTabGlobRabatt.Value   :=0;
       KopfTabLiefart.Value      :=0;
       KopfTabZahlArt.Value      :=1; // BAR

       KopfTabVReNum.Value       :=-1;

       KopfTabErstellt.Value     :=now;
       KopfTabERST_NAME.Value    :=dm1.view_user;

       KopfTabMWST_0.Value    :=DM1.MWSTTab[0];
       KopfTabMWST_1.Value    :=DM1.MWSTTab[1];
       KopfTabMWST_2.Value    :=DM1.MWSTTab[2];
       KopfTabMWST_3.Value    :=DM1.MWSTTab[3];

       KopfTabSOLL_STAGE.Value   :=0;
       KopfTabSOLL_SKONTO.Value  :=0;
       KopfTabSOLL_NTAGE.Value   :=1;
       KopfTabSOLL_RATEN.Value   :=1;
       KopfTabSOLL_RATBETR.Value :=0;

       KopfTabKun_Name1.Value :='Barverkauf';

       FromPosNewRec :=True;
       try
         KopfTab.Post;
       finally
         FromPosNewRec :=False;
       end;

       ID :=KopfTabRec_ID.Value;
     end;

     PosTabJournal_ID.Value :=ID;
     PosTabQuelle.Value     :=KopfTabQuelle.Value;
     PosTabMenge.Value      :=1;
     PosTabQuelle_Sub.Value :=KopfTabQuelle_Sub.Value;
     PosTabAddr_ID.Value    :=KopfTabAddr_ID.Value;
     PosTabVRENUM.Value     :=KopfTabVRENUM.Value;
     PosTABVLSNUM.Value     :=-1;
     PosTabGEBUCHT.Value    :=False;
     PosTabRabatt.Value     :=0;
     PosTabPosition.Value   :=PosTab.RecordCount+1;
     PosTabView_Pos.Value   :=Inttostr(PosTab.RecordCount+1);
end;
//------------------------------------------------------------------------------
procedure TKMainForm.PosTabAfterPost(DataSet: TDataSet);
var N, n0, n1, n2, n3,
    M, m0, m1, m2, m3,
    B, b0, b1, b2, b3,
    P, Lohn, Ware, TKst : Double;
    ID : Integer;
    Res : Boolean;
begin
     if PosTab.ControlsDisabled then exit;
     // Seriennummern
     if PosTabSN_Flag.AsBoolean then
     begin
        //Pr�fen ob gen�gend Seriennummern verf�gabr sind und wenn nicht
        //dann Menge anpassen

        // Pr�fen ob �berhaupt gen�gend Seriennummern vorhanden sind
        SNTab.Close;
        SNTab.Sql.Clear;
        SNTab.Sql.Add ('SELECT * FROM ARTIKEL_SERNUM');
        SNTab.Sql.Add ('WHERE ARTIKEL_ID='+Inttostr(PosTabArtikel_ID.AsInteger));
        SNTab.Sql.Add ('AND ((VK_JOURNAL_ID=-1 AND VK_JOURNALPOS_ID=-1)or');
        SNTab.Sql.Add ('(VK_JOURNAL_ID='+Inttostr(KopfTabRec_ID.AsInteger));
        SNTab.Sql.Add ('AND VK_JOURNALPOS_ID='+Inttostr(PosTabRec_ID.AsInteger)+'))');

        SNTab.Open;
        if SNTab.RecordCount<PosTabMenge.AsInteger then
        begin
           // Nicht gen�gend SN, Fehlerdialog anzeigen, Menge reduzieren
           MessageDlg ('Es sind nicht gen�gend Seriennummern vorhanden,'+#13#10+
                       'deshalb wurde die Menge reduziert.',mterror,
                       [mbok],0);
           PosTab.Edit;
           PosTabMenge.AsFloat :=SNTab.RecordCount;
           PosTab.Post;
           SNTab.Close;
           Exit;
        end;
        SNTab.Close;

        while SNTab.RecordCount<>PosTabMenge.AsInteger do
        begin
          SNTab.Close;
          SNTab.Sql.Clear;
          SNTab.Sql.Add ('SELECT * FROM ARTIKEL_SERNUM');
          SNTab.Sql.Add ('WHERE ARTIKEL_ID='+Inttostr(PosTabArtikel_ID.AsInteger));
          SNTab.Sql.Add ('AND VK_JOURNAL_ID='+Inttostr(KopfTabRec_ID.AsInteger));
          SNTab.Sql.Add ('AND VK_JOURNALPOS_ID='+Inttostr(PosTabRec_ID.AsInteger));
          SNTab.Open;

          if SNTab.RecordCount<>PosTabMenge.AsInteger then
          begin
             // Disloag zur SN-Auswahl aufrufen
             Res :=SNAuswahlForm.AuswahlSN (PosTabMenge.AsInteger,
                                            PosTabArtikel_ID.AsInteger,
                                            SNTab,
                                            KopfTabRec_ID.AsInteger,
                                            PosTabRec_ID.AsInteger,
                                            SNDS);

             // Wenn Anzahl der SN nicht stimmt oder der Benutzer den SN-Disloag
             // abgebrochen hat dann die Position l�schen
             if (Res=False) or (SNTab.RecordCount<>PosTabMenge.AsInteger) then
             begin
               // le. Position l�schen
               Edit1.Text :='';
               StornoBtnClick(Self);
               Exit;
             end;
          end;
        end;
        SNTab.Close;
     end;


     ID :=PosTabRec_ID.Value;
     N :=0; N0 :=0; N1 :=0; N2 :=0; N3 :=0;
     M :=0; M0 :=0; M1 :=0; M2 :=0; M3 :=0;
     B :=0; B0 :=0; B1 :=0; B2 :=0; B3 :=0;
     P :=0;
     try
        PosTab.DisableControls;
        PosTab.First;


        while not PosTab.Eof do
        begin
           N :=N+PosTabNSumme.Value;
           B :=B+PosTabBSumme.Value;
           P :=P+PosTabPROVIS_WERT.Value;

           case PosTabSteuer_code.Value of
             0:begin N0 :=N0 + PosTabNSumme.Value; B0 :=B0 + PosTabBSumme.Value;{M0 :=M0+PosTabMSumme.Value;} end;
             1:begin N1 :=N1 + PosTabNSumme.Value; B1 :=B1 + PosTabBSumme.Value;{M1 :=M1+PosTabMSumme.Value;} end;
             2:begin N2 :=N2 + PosTabNSumme.Value; B2 :=B2 + PosTabBSumme.Value;{M2 :=M2+PosTabMSumme.Value;} end;
             3:begin N3 :=N3 + PosTabNSumme.Value; B3 :=B3 + PosTabBSumme.Value;{M3 :=M3+PosTabMSumme.Value;} end;
           end;
           PosTab.Next;
        end;

        if KopfTabBrutto_Flag.ASBoolean then
        begin
             //Bruttofakturierung
             M1 :=cao_round(B1 / (100+KopfTabMwSt_1.AsFloat)*KopfTabMwSt_1.AsFloat*100)/100;
             M2 :=cao_round(B2 / (100+KopfTabMwSt_2.AsFloat)*KopfTabMwSt_2.AsFloat*100)/100;
             M3 :=cao_round(B3 / (100+KopfTabMwSt_3.AsFloat)*KopfTabMwSt_3.AsFloat*100)/100;

             M :=M0+M1+M2+M3;
             N :=B-M;
        end
           else
        begin
             // Nettofakturierung
             M1 :=cao_round(N1*KopfTabMwSt_1.AsFloat)/100;
             M2 :=cao_round(N2*KopfTabMwSt_2.AsFloat)/100;
             M3 :=cao_round(N3*KopfTabMwSt_3.AsFloat)/100;

             M :=M0+M1+M2+M3;
             B :=N+M;
        end;
        PosTab.Refresh;

        PosTab.Locate ('REC_ID',variant(ID),[]);
     finally
        PosTab.EnableControls;
        dm1.uniquery.close;
        dm1.uniquery.sql.text :='select ARTIKELTYP, SUM(EPREIS * MENGE) AS GSUM'+
                                ' from JOURNALPOS where JOURNAL_ID='+
                                Inttostr(KopfTabRec_ID.AsInteger)+
                                ' group by ARTIKELTYP';
        dm1.uniquery.open;

        Lohn :=0;
        Ware :=0;
        TKst :=0;

        if dm1.uniquery.locate ('ARTIKELTYP','N',[]) then Ware :=Ware + dm1.uniquery.fieldbyname ('GSUM').AsFloat;
        if dm1.uniquery.locate ('ARTIKELTYP','S',[]) then Ware :=Ware + dm1.uniquery.fieldbyname ('GSUM').AsFloat;
        if dm1.uniquery.locate ('ARTIKELTYP','V',[]) then Ware :=Ware + dm1.uniquery.fieldbyname ('GSUM').AsFloat;
        if dm1.uniquery.locate ('ARTIKELTYP','F',[]) then Ware :=Ware + dm1.uniquery.fieldbyname ('GSUM').AsFloat;
        if dm1.uniquery.locate ('ARTIKELTYP','L',[]) then Lohn :=Lohn + dm1.uniquery.fieldbyname ('GSUM').AsFloat;
        if dm1.uniquery.locate ('ARTIKELTYP','K',[]) then TKst :=TKst + dm1.uniquery.fieldbyname ('GSUM').AsFloat;

        dm1.uniquery.close;

        // Brutto-Wert-Rundung
        if DM1.BR_SUM_RUND_WERT>1
          then B :=CAO_Round (B*100 / DM1.BR_SUM_RUND_WERT)* DM1.BR_SUM_RUND_WERT /100;

        if (KopfTabNSumme.Value      <> N)or
           (KopfTabMSumme.Value      <> M)or
           (KopfTabBSumme.Value      <> B)or
           (KopfTabWare.Value        <> Ware)or
           (KopfTabLohn.Value        <> Lohn)or
           (KopfTabTKost.Value       <> TKst)then
        begin
           try
              if not (KopfTab.State in [dsEdit,dsInsert]) then KopfTab.Edit;
              KopfTabNSumme.Value :=N;
              KopfTabMSumme.Value :=M;
              KopfTabBSumme.Value :=B;
              KopfTabMSumme_0.Value :=M0;
              KopfTabMSumme_1.Value :=M1;
              KopfTabMSumme_2.Value :=M2;
              KopfTabMSumme_3.Value :=M3;
              KopfTabRDATUM.Value :=now;
              KopfTabWare.Value  :=Ware;
              KopfTabLohn.Value  :=Lohn;
              KopfTabTKost.Value :=TKst;

              KopfTab.Post;
           except
              KopfTab.Cancel;
           end;
        end;
     end;
     PosTabAfterScroll (nil);
end;
//------------------------------------------------------------------------------
procedure TKMainForm.StornoBtnClick(Sender: TObject);
var P : Integer;
begin
     if PosTab.RecordCount>0 then
     begin
          if length(Edit1.Text)>0 then
          begin
             try
                P :=StrToInt (Edit1.Text);
             except
                P :=-1;
                MessageBeep(0);
             end;
             if (PosTab.Locate ('POSITION',P,[])) and
                (PosTabPosition.Value=P) then
             begin
                // Artikel auf dem Kundendisplay anzeigen

                ZeigeBild('BILD15',
                          [PosTabBarCode.Value,
                           PosTabArtNum.Value,
                           PosTabMatchCode.Value,
                           PosTabMenge.Value,
                           PosTabEPreis.Value * -1,
                           0.00,
                           PosTabEPREIS.Value * -1,
                           PosTabBSUMME.Value * -1,
                           PosTabBezeichnung.AsString,
                           PosTabMwstProz.Value,
                           PosTabPOSITION,
                           waehrung],
                          'TKMainForm.CancelClick');

                try PosTab.Delete; except end;

                try
                  PosTab.DisableControls;
                  PosTab.First;
                  while not PosTab.Eof do
                  begin
                     if PosTabPosition.Value<>PosTab.RecNo then
                     begin
                       PosTab.Edit;
                       PosTabPosition.Value :=PosTab.RecNo;
                       try PosTab.Post; except end;
                     end;
                     PosTab.Next;
                  end;
                finally
                  PosTab.EnableControls;
                end;
                PosTabAfterPost(nil);
             end;
             Edit1.Text :='';
          end
             else
          begin
             PosTab.Last;
             // Artikel auf dem Kundendisplay anzeigen

             ZeigeBild('BILD15',
                       [PosTabBarCode.Value,
                        PosTabArtNum.Value,
                        PosTabMatchCode.Value,
                        PosTabMenge.Value,
                        PosTabEPreis.Value * -1,
                        0.00,
                        PosTabEPREIS.Value * -1,
                        PosTabBSUMME.Value * -1,
                        PosTabBezeichnung.AsString,
                        PosTabMwstProz.Value,
                        PosTabPOSITION,
                        waehrung],
                       'TKMainForm.CancelClick');

             PosTab.Delete;
          end;
     end;
end;
//------------------------------------------------------------------------------
procedure TKMainForm.BonFertigBtnClick(Sender: TObject);
begin
     //if KopfTabBSumme.Value=0 then exit;
     if PosTab.RecordCount=0 then exit;
     //Fertig
     Gebucht :=False;
     BonFertigForm.ShowModal;
     if Gebucht then
     begin
          // Bon abschneiden
          {
          if not USEBonDrucker then
          begin
            // Drucker �ber Windows-Treiber ansteuern
            Printer.PrinterIndex :=Printer.Printers.IndexOf (DefDrucker);
            Printer.BeginDoc;
            Printer.Canvas.Font.Name :='control';
            Printer.Canvas.TextOut (1,1,'F');
            Printer.EndDoc;
          end;
          }
          //................
          KopfTab.Close;
          KopfTab.Open;
          ZeigeBild('BILD00', [], 'TKMainForm.BonFertigBtnClick');
     end
        else
     begin
      // Artikel auf dem Kundendisplay anzeigen

      ZeigeBild('BILD10',
                [PosTabBarCode.Value,
                 PosTabArtNum.Value,
                 PosTabMatchCode.Value,
                 PosTabMenge.Value,
                 PosTabEPreis.Value,
                 0.00,
                 PosTabEPREIS.Value,
                 PosTabBSUMME.Value,
                 PosTabBezeichnung.AsString,
                 PosTabMwstProz.Value,
                 PosTabPOSITION,
                 waehrung],
                'TKMainForm.BonFertigBtnClick');
     end;
     BonPrintBtn.Enabled :=UseBonDrucker or (LastBonNum > -1);
     letztenBondrucken1.Enabled :=BonPrintBtn.Enabled;
end;
//------------------------------------------------------------------------------
procedure TKMainForm.letztenBondrucken1Click(Sender: TObject);
begin
     if USEBonDrucker then
     begin
       // le. Bon drucken aus Datei
       if (fileexists (dm1.tmpdir+'BON_'+Inttostr(DM1.TermID)+'.TMP'))and
          (length(DPort)>0)
        then Drucke_Liste (dm1.tmpdir+'BON_'+Inttostr(DM1.TermID)+'.TMP',DPort);
     end
        else
     begin
        {$IFDEF REPORTBUILDER}
        PrintBonForm.ShowDlg (1006,ShowDruckDialog,DefFormular,DefDrucker);
        {$ENDIF}
     end;
end;
//------------------------------------------------------------------------------
procedure TKMainForm.Schubladeffnen1Click(Sender: TObject);
var
  f: textfile;
begin
  if USEBonDrucker then
  begin
    // Direkt zu Drucker senden, WIN-Treiber umgehen
    if Length(DPort) > 0 then
    begin
      // Schublade auf
      assignfile(f, dport);
      try
        rewrite(f);
        try
          write(f, DT.Schublade_Auf);
        finally
          CloseFile(f);
        end;
      except
      end;
    end
  end
     else
  begin
    // Drucker �ber Windows-Treiber ansteuern
    Printer.PrinterIndex :=Printer.Printers.IndexOf (DefDrucker);
    Printer.BeginDoc;
    Printer.Canvas.Font.Name :='control';
    Printer.Canvas.TextOut (1,1,'A');
    Printer.EndDoc;
  end;
end;
//------------------------------------------------------------------------------
procedure TKMainForm.OnAddArtikelToKasse (ID : Integer; Menge : Double);
var PR : Double;
begin
     //wird vom Artikelstamm aufgerufen
     DM1.ArtInfoTab.Close;
     DM1.ArtInfoTab.SQL.Text :=ArtInfoSqlS+ArtInfoSqlW1;
     DM1.ArtInfoTab.ParamByName ('AID').AsInteger :=ID;
     DM1.ArtInfoTab.Open;

     if (DM1.ArtInfoTab.Locate ('REC_ID',ID,[])) and
        (DM1.ArtInfoTabRec_ID.AsInteger=ID) and
        (DM1.ArtInfoTabNo_VK_Flag.Value=False) then
     begin
          PosTab.Append;
          Try
            PosTabGEBUCHT.Value         :=False;
            PosTabBRUTTO_FLAG.Value     :=True;

            PosTabArtikel_ID.Value      :=ID;
            PosTabArtikelTyp.Value      :=DM1.ArtInfoTabArtikelTyp.Value;

            PosTabArtNum.Value          :=DM1.ArtInfoTabArtnum.Value;
            PosTabMatchCode.Value       :=DM1.ArtInfoTabMatchCode.Value;
            PosTabBarCode.Value         :=DM1.ArtInfoTabBarCode.Value;

            PosTabBezeichnung.AsString  :=DM1.ArtInfoTab.FieldByName('KAS_NAME').AsString;

            PosTabLaenge.Value          :=DM1.ArtInfoTabLaenge.Value;
            PosTabGroesse.Value         :=DM1.ArtInfoTabGroesse.Value;
            PosTabDimension.Value       :=DM1.ArtInfoTabDimension.Value;
            PosTabGewicht.Value         :=DM1.ArtInfoTabGewicht.Value;
            PosTabME_Einheit.Value      :=DM1.ArtInfoTabME_Einheit.Value;
            PosTabPR_Einheit.Value      :=DM1.ArtInfoTabPR_Einheit.Value;
            PosTabSN_Flag.AsBoolean     :=DM1.ArtInfoTabSN_Flag.AsBoolean;

            PosTabMenge.Value           :={KMainForm.}Menge;

            KMainForm.Menge :=1;
            MengeLab.Caption :=FormatFloat(',#0.0#',KMainForm.Menge);//IntToStr(KMainForm.Menge);

            if DM1.GetArtikelPreis (ID,
                                    -1,
                                    //KopfTabPR_Ebene.Value,
                                    PEbene,
                                    True {Brutto},
                                    PosTabMenge.AsFloat,
                                    PR)

             then PosTabEPreis.Value :=PR;


            PosTabSteuer_Code.Value     :=DM1.ArtInfoTabSteuer_Code.Value;
            PosTabAltteil_Flag.Value    :=DM1.ArtInfoTabAltTeil_Flag.Value;
            PosTabALTTEIL_PROZ.Value    :=0.1; // 10%
            PosTabALTTEIL_STCODE.Value  :=DM1.ArtInfoTabSteuer_Code.Value;
            PosTabBEZ_FEST_Flag.Value   :=DM1.ArtInfoTabNO_BEZEDIT_Flag.Value;
            PosTabGEGENKTO.Value        :=DM1.ArtInfoTabErloes_Kto.Value;
            //PosTabVPE.Value             :=DM1.ArtInfoTabVPE.AsInteger;
            PosTabRabatt.Value          :=0;
            PosTabGEBUCHT.Value         :=False;
            PosTab.Post;

            // Artikel auf dem Kundendisplay anzeigen

            ZeigeBild('BILD10',
                      [PosTabBarCode.Value,
                       PosTabArtNum.Value,
                       PosTabMatchCode.Value,
                       PosTabMenge.Value,
                       PosTabEPreis.Value,
                       0.00,
                       PosTabEPREIS.Value,
                       PosTabBSUMME.Value,
                       PosTabBezeichnung.AsString,
                       PosTabMwstProz.Value,
                       PosTabPOSITION,
                       waehrung],
                      'TKMainForm.OnAddArtikelToKasse');
            
          except
            PosTab.Cancel;
          end;
     end else
     begin
          // Hier Fehler, Artikel nicht gefunden !!!
     end;
     DM1.ArtInfoTab.Close;//ArtikelTab.Close;
end;
//------------------------------------------------------------------------------
procedure TKMainForm.Suchen1Click(Sender: TObject);
begin
     // Artikel suchen
     KasseArtikelForm.Borderstyle :=bsSizeable;
     //KasseArtikelForm.Width :=660;
     //KasseArtikelForm.Height :=586;
     //KasseArtikelForm.Position :=poMainFormCenter;
     KasseArtikelForm.Invalidate;
     KasseArtikelForm.AddMengeEdi.Value :=KMainForm.Menge;
     KasseArtikelForm.ShowModal;
     KasseArtikelForm.F9Change (False);
end;
//------------------------------------------------------------------------------
procedure TKMainForm.Edit1KeyPress(Sender: TObject; var Key: Char);
begin
     if LastKey in [VK_SUBTRACT, VK_ADD] then begin key :=#0; exit; end;
     if key=#13 then
     begin
          if length(Edit1.Text)>0 then AddArtikel (Menge,Edit1.Text,-1);
          Edit1.Text :='';
          key :=#0;
     end
        else
     if key=#27 then // Eingabe l�schen
     begin
          Menge :=1;
          MengeLab.Caption :=FormatFloat (',#0.0#',Menge);//IntToStr(Menge);
          Edit1.Text :='';
          key :=#0;
     end
        else
     if key='*' then
     begin
          try Menge :=StrToFloat{StrToInt} (Edit1.Text); except Menge :=1; end;
          MengeLab.Caption :=FormatFloat (',#0.0#',Menge);//IntToStr(Menge);
          Edit1.Text :='';
          key :=#0;
     end;
end;
//------------------------------------------------------------------------------
procedure TKMainForm.Edit1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var OldSuchFeld,S : String;

  procedure UpdateDisplay;
  begin
     ZeigeBild('BILD10',
               [PosTabBarCode.Value,
                PosTabArtNum.Value,
                PosTabMatchCode.Value,
                PosTabMenge.Value,
                PosTabEPreis.Value,
                0.00,
                PosTabEPREIS.Value,
                PosTabBSUMME.Value,
                PosTabBezeichnung.AsString,
                PosTabMwstProz.Value,
                PosTabPOSITION,
                waehrung],
               'TKMainForm.BonFertigBtnClick');
  end;

begin
     lastkey :=key;
     if (Key=66)and(Shift=[ssCtrl]) then  //STRG+B
     begin
        OldSuchFeld :=SuchFeld;
        SuchFeld :='BARCODE';
        AddArtikel (Menge,Edit1.Text,-1);

        Edit1.Text :='';
        SuchFeld :=OldSuchFeld;
        Key :=0;
     end
        else
     if (Key=77)and(Shift=[ssCtrl]) then  //STRG+M
     begin
        OldSuchFeld :=SuchFeld;
        SuchFeld :='MATCHCODE';
        AddArtikel (Menge,Edit1.Text,-1);

        Edit1.Text :='';
        SuchFeld :=OldSuchFeld;
        Key :=0;
     end
        else
     if (Key=65)and(Shift=[ssCtrl]) then  //STRG+A
     begin
        OldSuchFeld :=SuchFeld;
        SuchFeld :='ARTNUM';
        AddArtikel (Menge,Edit1.Text,-1);

        Edit1.Text :='';
        SuchFeld :=OldSuchFeld;
        Key :=0;
     end
        else
     if (key in[vk_F1..vk_F10])and(Shift=[ssCtrl]) then // SRRG+F1 bis SRG+F10
     begin
        // Warengruppen 01 - 10
        AddArtikel (Menge,Edit1.Text,key-vk_F1+1);
        Edit1.Text :='';
        Key :=0;
     end
        else
     if (key in[vk_F1..vk_F10])and(Shift=[ssShift,ssCtrl]) then // SRRG+F1 bis SRG+F10
     begin
        // Warengruppen 11 - 20
        AddArtikel (Menge,Edit1.Text,key-vk_F1+11);
        Edit1.Text :='';
        Key :=0;
     end
        else
     if (Key=vk_Up)and(Shift=[]) then
     begin
        PosTab.Prior;
        Key :=0;

        UpdateDisplay;
     end
        else
     if (Key=vk_Down)and(Shift=[]) then
     begin
        PosTab.Next;
        Key :=0;

        UpdateDisplay;
     end
        else
     if (Key=vk_Home)and(Shift=[]) then
     begin
        PosTab.First;
        Key :=0;

        UpdateDisplay;
     end
        else
     if (Key=vk_End)and(Shift=[]) then
     begin
        PosTab.Last;
        Key :=0;

        UpdateDisplay;
     end
        else
     if (key=vk_add)and(Shift=[]) then
     begin
          If PosTab.RecordCount>0 then
          begin
             //Pr�fen ob gen�gend Seriennummern verf�gabr sind und wenn nicht
             //dann Menge anpassen
             if PosTabSN_Flag.AsBoolean then
             begin
                SNTab.Close;
                SNTab.Sql.Clear;
                SNTab.Sql.Add ('SELECT * FROM ARTIKEL_SERNUM');
                SNTab.Sql.Add ('WHERE ARTIKEL_ID='+Inttostr(PosTabArtikel_ID.AsInteger));
                SNTab.Sql.Add ('AND ((VK_JOURNAL_ID=-1 AND VK_JOURNALPOS_ID=-1)or');
                SNTab.Sql.Add ('(VK_JOURNAL_ID='+Inttostr(KopfTabRec_ID.AsInteger));
                SNTab.Sql.Add ('AND VK_JOURNALPOS_ID='+Inttostr(PosTabRec_ID.AsInteger)+'))');
                SNTab.Open;
                if SNTab.RecordCount<PosTabMenge.AsInteger+1 then
                begin
                  MessageDlg ('Es sind nicht gen�gend Seriennummern vorhanden,'+#13#10+
                              'deshalb wurde die Menge nicht erh�ht.',mterror,
                              [mbok],0);
                  PosTabMenge.AsFloat :=SNTab.RecordCount;
                end
                   else
                begin
                  PosTab.Edit;
                  PosTabMenge.Value :=PosTabMenge.Value+1;
                  PosTab.Post;
                end;
                SNTab.Close;
             end
                else
             begin
               PosTab.Edit;
               PosTabMenge.Value :=PosTabMenge.Value+1;
               PosTab.Post;

               UpdateDisplay;
             end;
          end;
          key :=0;
     end
        else
     if (key=VK_SUBTRACT)and(Shift=[]) then
     begin
          If (PosTab.RecordCount>0)and(PosTabMenge.Value>=2) then
          begin
             PosTab.Edit;
             PosTabMenge.Value :=PosTabMenge.Value-1;
             PosTab.Post;

             UpdateDisplay;
          end;
          key :=0;
     end;
end;
//------------------------------------------------------------------------------
procedure TKMainForm.AddArtikel (Menge : Double; Eingabe : String; WGR : Integer);

var PR, M, B : Double;
    I        : Integer;
    BC, P, Name    : String;

begin
     bc :=Eingabe;

     while (length(bc)>0)and(bc[length(bc)]=' ') do delete (bc,length(bc),1);

     if length(bc)=0 then
     begin
          messagebeep(0); exit;
     end;

     bc :=uppercase(bc);

     if WGR>0 then
     begin
          // Eingabefeld enth�lt den Preis
          P :=Eingabe;
          for i:=1 to length(P) do if not (P[i] in ['0'..'9',',']) then exit;

          if Pos(',',P)>0 then
          begin
               PR :=StrToInt( Copy(P,1,Pos(',',P)-1) );
               Delete (P,1,Pos(',',P));
               try
                  PR :=PR + StrToInt(P)/100;
               except
                  Exit;
               end;
          end else
          begin
              try
                 PR :=StrToInt(P);
              except
                 exit;
              end;
          end;

          // Namen der Warengruppe laden
          if not DM1.WgrTab.Active then DM1.WgrTab.Open;

          if DM1.WgrTab.Locate ('ID',WGR,[])
           then Name :=DM1.WgrTab.FieldByName ('NAME').AsString
           else Name :='Warengruppe '+IntToStr(WGR);

          // jetzt Artikel (Warengruppe) hinzuf�gen
          PosTab.Append;
          Try
            PosTabArtikel_ID.Value      :=-99;
            PosTabArtikelTyp.Value      :='F';

            PosTabArtNum.Value          :='';
            PosTabMatchCode.Value       :='';
            PosTabBarCode.Value         :='';

            PosTabBezeichnung.AsString  :=Name;
            PosTabLaenge.Value          :='';
            PosTabGroesse.Value         :='';
            PosTabDimension.Value       :='';
            PosTabGewicht.Value         :=0;
            PosTabME_Einheit.Value      :='';
            PosTabPR_Einheit.Value      :=1;



            PosTabMenge.Value           :=Menge;
            PosTabEPreis.Value          :=PR;
            PosTabRabatt.Value          :=0;
            PosTabSteuer_Code.Value     :=DM1.WgrTabSTEUER_CODE.AsInteger;
            PosTabAltteil_Flag.Value    :=False;
            PosTabALTTEIL_PROZ.Value    :=0.1; // 10%
            PosTabALTTEIL_STCODE.Value  :=2;
            PosTabBEZ_FEST_Flag.Value   :=False;
            PosTabGEBUCHT.Value         :=False;
            PosTabGEGENKTO.Value        :=-1;

            PosTab.Post;
          except
            PosTab.Cancel;
          end;
          Menge :=1;
          MengeLab.Caption :=FormatFloat (',#0.0#',Menge);//IntToStr(Menge);

          // Artikel auf dem Kundendisplay anzeigen


          ZeigeBild('BILD10',
                    [PosTabBarCode.Value,
                     PosTabArtNum.Value,
                     PosTabMatchCode.Value,
                     PosTabMenge.Value,
                     PosTabEPreis.Value,
                     0.00,
                     PosTabEPREIS.Value,
                     PosTabBSUMME.Value,
                     PosTabBezeichnung.AsString,
                     PosTabMwstProz.Value,
                     PosTabPOSITION,
                     waehrung],
                    'TKMainForm.OnAddArtikelToKasse');

          exit;
     end;
     //wir vom Artikelstamm aufgerufen
     ArtikelTab1.Close;
     ArtikelTab1.Sql.Clear;
     ArtikelTab1.Sql.Add('select * from ARTIKEL');

     ArtikelTab1.Sql.Add('where UPPER('+SuchFeld+')=:BC');
     ArtikelTab1.Sql.Add('LIMIT 0,10');
     ArtikelTab1.ParamByName ('BC').AsString :=BC;
     ArtikelTab1.Open;
     if ArtikelTab1.RecordCount=1
      then OnAddArtikelToKasse (ArtikelTab1REC_ID.Value, Menge);
     ArtikelTab1.Close;
end;
//------------------------------------------------------------------------------
procedure TKMainForm.BucheBon (Zahlart : Integer; Gegeben, Zuruck : Double);
var num : integer;
begin
     KopfTab.Edit;
     KopfTabZahlart.Value :=Zahlart;
     KopfTab.Post;

     num :=dm1.Buche_BCKasse (KopfTabRec_ID.Value);
     if Num>=0 then
     begin
          LastBonNum :=Num;
          // Bon erstellen

          if USEBonDrucker then
          begin
            try
               MakeBon (num,gegeben,zuruck);
            except end;
            if AutoPrint then
            begin
              if (fileexists (dm1.tmpdir+'BON_'+Inttostr(DM1.TermID)+'.TMP'))and
                 (length(DPort)>0)
               then Drucke_Liste (dm1.tmpdir+'BON_'+Inttostr(DM1.TermID)+'.TMP',DPort);
            end;
          end
          {$IFDEF REPORTBUILDER}
             else
          begin
             if AutoPrint
               then PrintBonForm.ShowDlg (Num,
                                          ShowDruckDialog,
                                          DefFormular,
                                          DefDrucker);
          end
          {$ENDIF}
          ;
     end;
     Gebucht :=True;
end;
//------------------------------------------------------------------------------
procedure TKMainForm.MakeBon (Num : Integer; geg, rueck : double);
var    bondat        : textfile;
       I             : Integer;
       B, WA, PosStr : String;

{ procedure PrintPos;
  var Name : String;
  begin
       PosTab.First;
       while not PosTab.Eof do
       begin
         write (bondat,postabmenge.value:4:1,' x ');
         name :=copy (postabbezeichnung.asstring,1,20);
         write (bondat,name,' ':21-length(name));
         if postabmenge.value<>1 then
         begin
              writeln (bondat,'a ',postabepreis.value:8:2,' *');
              writeln (bondat,' ':28,'= ',postabBSUMME.value:10:2);
         end else
         begin
              writeln (bondat,'= ',postabBSUMME.value:10:2);
         end;
         PosTab.Next;
       end;
  end; }

  Function MakePos : String;
  var S,S1,Name,ArtNum,Match : String;
  begin
       PosTab.First;
       S :='';
       while not PosTab.Eof do
       begin
         Artnum :=PosTabArtnum.AsString;
         Match  :=PosTabMatchcode.AsString;
         name :=copy (postabbezeichnung.asstring,1,20); // Auf 20 Zeichen begrenzen

         if postabmenge.value<>1 then
         begin
              S1 :=Format (PosTemplateMengeN1,
                           [Artnum,
                            Match,
                            Name,
                            PosTabMenge.AsFloat,
                            PosTabEPreis.AsFloat,
                            PosTabBSumme.AsFloat,
                            PosTabSTEUER_CODE.AsInteger,
                            PosTabMwStProz.AsFloat
                           ])
         end else
         begin // Menge = 1
              S1 :=Format (PosTemplateMenge1,
                           [Artnum,
                            Match,
                            Name,
                            PosTabMenge.AsFloat,
                            PosTabEPreis.AsFloat,
                            PosTabBSumme.AsFloat,
                            PosTabSTEUER_CODE.AsInteger,
                            PosTabMwStProz.AsFloat
                           ]);
         end;
         S :=S+S1; if PosTab.RecNo<PosTab.RecordCount then s :=s+#13#10;
         PosTab.Next;
       end;
       Result :=S;
  end;


begin
     wa :=KopfTabWaehrung.Value;
     if wa='�' then WA :='EUR';
     if wa='DM' then WA :='DEM';

     assignfile (bondat,dm1.tmpdir+'BON_'+Inttostr(DM1.TermID)+'.TMP');
     {
     rewrite (bondat);
     if length(DT.DINIT)>0 then Write (Bondat,DT.DINIT);

     writeln (bondat,
              DT.breit_on,
              dm1.readstring ('KASSE',
                              'BON_UEBERSCHRIFT1',
                              '   UEBERSCHRIFT'),
              DT.breit_off);
     writeln (bondat,
              DT.breit_on,
              dm1.readstring ('KASSE',
                              'BON_UEBERSCHRIFT2',
                              '      ------'),
              DT.breit_off);
     //                        1234567890123456789012345678901234567890
     //                                 10        20        30       40
     writeln (bondat,
              dm1.readstring ('KASSE',
                              'BON_UEBERSCHRIFT3',
                              '         Telefon : -----/------         ')
              );
     writeln (bondat);

     writeln (bondat,
              DT.breit_on,
              'Rechnung ',
              formatfloat('0000',num),
              DT.breit_off,
              '    ',
              formatdatetime('dd.mm.yy',KopfTabRDatum.Value)
              );
     writeln (bondat);

     PrintPos;

     writeln (bondat,        '----------------------------------------'); // 40 Zeichen
     writeln (bondat,DT.breit_on,'Gesamt: '+WA,' ',KopfTabBSumme.Value:8:2,DT.breit_off);
     writeln (bondat);
     writeln (bondat,'MWST(',KopfTabMWST_1.value:2:0,'%)=',KopfTabMSumme_1.Value:7:2,' ',WA,
                     '  (',KopfTabMWST_2.value:2:0,'%)=',KopfTabMSumme_2.Value:7:2,' ',WA);
     writeln (bondat,'Gegeben:',geg:9:2,' '+WA,'  Zur�ck:',rueck:6:2,' ',WA);
     WRITELN (bondat);
     writeln (bondat,'  *** Vielen Dank f�r Ihren Besuch ***');
     WRITELN (bondat);

     if length(DT.ABSCHNEIDEN)>0 then
     begin
        Write (BonDat,DT.ABSCHNEIDEN);
     end
        else
     begin
        for i :=1 to 8 do writeln (bondat);
     end;
     closefile (bondat); }

     //-------------------------------------------------------------------------
     // NEUE VARIANTE MIT VARIABLEM LAYOUT
     //-------------------------------------------------------------------------

     PosStr :=MakePos; // Positonszeilen erzeugen lassen

     // Template mit Werten f�llen
     B :=Format (BonTemplate,
                 [//Kopf
                  BON_HDR1,  //%0
                  BON_HDR2,  //%1
                  BON_HDR3,
                  // Fu�
                  BON_FTR1,  //%3
                  BON_FTR2,
                  BON_FTR3,
                  // Userfelder
                  BON_USR1, //%6
                  BON_USR2,
                  BON_USR3,
                  BON_USR4,
                  BON_USR5,

                  NUM, //%11
                  FormatDateTime('dd.mm.yy',KopfTabRDatum.AsDateTime),
                  WA,
                  KopfTabBSumme.AsFloat,
                  KopfTabMWST_1.AsFloat,
                  KopfTabMSumme_1.AsFloat,
                  KopfTabMWST_2.AsFloat,
                  KopfTabMSumme_2.AsFloat,
                  geg,
                  rueck
                  ]);

     //Positionen einf�gen
     StrReplace (B,'@POSITIONEN@'    ,PosStr           ,[rfReplaceAll]);

     // Druckertreiber-Steuercodes einf�gen
     StrReplace (B,'@DINIT@'         ,DT.DINIT         ,[rfReplaceAll]);
     StrReplace (B,'@ABSCHNEIDEN@'   ,DT.ABSCHNEIDEN   ,[rfReplaceAll]);
     StrReplace (B,'@SCHUBLADE_AUF@' ,DT.SCHUBLADE_AUF ,[rfReplaceAll]);
     StrReplace (B,'@BREIT_ON@'      ,DT.BREIT_ON      ,[rfReplaceAll]);
     StrReplace (B,'@BREIT_OFF@'     ,DT.BREIT_OFF     ,[rfReplaceAll]);
     StrReplace (B,'@UNTER_ON@'      ,DT.UNTER_ON      ,[rfReplaceAll]);
     StrReplace (B,'@UNTER_OFF@'     ,DT.UNTER_OFF     ,[rfReplaceAll]);
     StrReplace (B,'@RED_ON@'        ,DT.RED_ON        ,[rfReplaceAll]);
     StrReplace (B,'@RED_OFF@'       ,DT.RED_OFF       ,[rfReplaceAll]);
     StrReplace (B,'@HTAB@'          ,DT.HTAB          ,[rfReplaceAll]);
     StrReplace (B,'@RLFEED@'        ,DT.RLFEED        ,[rfReplaceAll]);

     // Druckertreiber Umlaute ersetzen
     StrReplace (B,'�'               ,DT.UML_G_AE      ,[rfReplaceAll]);
     StrReplace (B,'�'               ,DT.UML_K_AE      ,[rfReplaceAll]);
     StrReplace (B,'�'               ,DT.UML_G_OE      ,[rfReplaceAll]);
     StrReplace (B,'�'               ,DT.UML_K_OE      ,[rfReplaceAll]);
     StrReplace (B,'�'               ,DT.UML_G_UE      ,[rfReplaceAll]);
     StrReplace (B,'�'               ,DT.UML_K_UE      ,[rfReplaceAll]);
     StrReplace (B,'�'               ,DT.UML_SS        ,[rfReplaceAll]);

     rewrite (bondat);
     Write (BonDat,B);
     closefile (bondat);
end;
//------------------------------------------------------------------------------
procedure drucke_liste(dname:string;dr_name:string);
var       textdatei    : textfile;
          zeile        : string;
          drucker      : textfile;
begin
     assignfile (drucker ,dr_name);
     rewrite (drucker);
     try
       assignfile(textdatei,dname);
       reset (textdatei);
       try
         while not eof(textdatei) do
         begin
              readln(textdatei,zeile);
              writeln(drucker,zeile);
         end;
       finally
         closefile (textdatei);
       end;
     finally
       closefile (drucker);
     end;
end;
//------------------------------------------------------------------------------
procedure TKMainForm.Artikelnummer1Click(Sender: TObject);
begin
     case tMenuItem(Sender).Tag of
          1:SuchFeld :='BARCODE';
          2:SuchFeld :='MATCHCODE';
          3:SuchFeld :='ARTNUM';
     end;
     tMenuItem(Sender).Checked :=True;
     DM1.WriteString ('KASSE','SUCHFELD',SuchFeld);
end;
//------------------------------------------------------------------------------
procedure TKMainForm.RecalcPreise (Faktor : Double);
var e : Double;
begin
     PosTab.DisableControls;
     try
        PosTab.First;
        while not PosTab.Eof Do
        begin
             PosTab.Edit;

             E :=PosTabEPreis.Value;
             E :=cao_round(E * Faktor * 100)/100; // Auf ganze Cent runden !!!
             PosTabEPreis.Value :=E;
             PosTab.Post;
             PosTab.Next;
        end;
     finally
        PosTab.EnableControls;
     end;
     PosTabAfterPost(nil);
end;
//------------------------------------------------------------------------------
procedure TKMainForm.PosTabAfterScroll(DataSet: TDataSet);
begin
     StornoBtn.Enabled         :=PosTab.RecordCount>0;
     BonFertigBtn.Enabled      :=PosTab.RecordCount>0;
     Fertigstellen1.Enabled    :=PosTab.RecordCount>0;
     lePositionlschen1.Enabled :=PosTab.RecordCount>0;
end;
//------------------------------------------------------------------------------
procedure TKMainForm.PosTabBeforePost(DataSet: TDataSet);
var ID: Integer;
begin
     ID :=PosTabArtikel_ID.AsInteger;
     DM1.ArtInfoTab.Close;
     DM1.ArtInfoTab.SQL.Text :=ArtInfoSqlS+ArtInfoSqlW1;
     DM1.ArtInfoTab.ParamByName ('AID').AsInteger :=ID;
     DM1.ArtInfoTab.Open;

     if (DM1.ArtInfoTab.Locate ('REC_ID',ID,[])) and
        (DM1.ArtInfoTabRec_ID.AsInteger=ID) then
     begin
        PosTabE_Rgewinn.AsFloat :=PosTabNSumme.AsFloat -
                                  (DM1.ArtInfoTabEK_Preis.AsFloat *
                                   PosTabMenge.AsFloat
                                  );
     end;
     DM1.ArtInfoTab.Close;
end;
//------------------------------------------------------------------------------
procedure TKMainForm.PosTabBeforeDelete(DataSet: TDataSet);
begin
     if PosTabSN_Flag.AsBoolean then
     begin
       // Seriennummern Verweis l�schen ....
       dm1.UniQuery.close;
       dm1.UniQuery.sql.text :='UPDATE ARTIKEL_SERNUM SET VK_JOURNALPOS_ID=-1, '+
                               'VK_JOURNAL_ID=-1 where VK_JOURNALPOS_ID='+
                               IntToStr(PosTabRec_ID.AsInteger)+
                               ' and VK_JOURNAL_ID='+IntToStr(KopfTabRec_ID.AsInteger);
       dm1.UniQuery.ExecSql;
       dm1.UniQuery.close;
     end;
end;
//------------------------------------------------------------------------------


initialization

finalization
  if KD_OK then KD_EXIT;
  try
    FreeLibrary(DLLHandle);
  except
    MessageDlg('Fehler beim entladen der Funktionen f�r das Kundendisplay aus '
      + #13 + #10 + 'DLL : ' + DM1.DisplayDLL, mtWarning, [mbOK], 0);
  end;
end.
//******************************************************************************
