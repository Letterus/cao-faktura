object ExportForm: TExportForm
  Left = 342
  Top = 233
  Width = 783
  Height = 540
  Caption = 'Export'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Menu = MainMenu1
  OldCreateOrder = False
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnDeactivate = FormDeactivate
  PixelsPerInch = 96
  TextHeight = 13
  object MainPanel: TPanel
    Left = 0
    Top = 0
    Width = 775
    Height = 494
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object TopPan: TPanel
      Left = 0
      Top = 0
      Width = 775
      Height = 27
      Align = alTop
      BevelOuter = bvNone
      BorderStyle = bsSingle
      Color = clBtnShadow
      Ctl3D = True
      ParentCtl3D = False
      TabOrder = 0
      object ReEdiTopLab: TLabel
        Left = 0
        Top = 0
        Width = 69
        Height = 23
        Align = alLeft
        Caption = '  Export ...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        Layout = tlCenter
      end
      object ButtonPan: TPanel
        Left = 484
        Top = 0
        Width = 287
        Height = 23
        Align = alRight
        AutoSize = True
        BevelOuter = bvNone
        Color = clBtnShadow
        TabOrder = 0
        object AuswahlBtn: TJvSpeedButton
          Tag = 1
          Left = 0
          Top = 0
          Width = 65
          Height = 22
          Caption = 'Aus&wahl'
          Flat = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          OnClick = AuswahlBtnClick
          HotTrackFont.Charset = DEFAULT_CHARSET
          HotTrackFont.Color = clWindowText
          HotTrackFont.Height = -11
          HotTrackFont.Name = 'MS Sans Serif'
          HotTrackFont.Style = []
          OnMouseEnter = AuswahlBtnMouseEnter
          OnMouseLeave = AuswahlBtnMouseLeave
        end
        object AllgemeinBtn: TJvSpeedButton
          Tag = 2
          Left = 66
          Top = 0
          Width = 68
          Height = 22
          Caption = 'All&gemein'
          Flat = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          Visible = False
          OnClick = AuswahlBtnClick
          HotTrackFont.Charset = DEFAULT_CHARSET
          HotTrackFont.Color = clWindowText
          HotTrackFont.Height = -11
          HotTrackFont.Name = 'MS Sans Serif'
          HotTrackFont.Style = []
          OnMouseEnter = AuswahlBtnMouseEnter
          OnMouseLeave = AuswahlBtnMouseLeave
        end
        object SQLBtn: TJvSpeedButton
          Tag = 3
          Left = 134
          Top = 0
          Width = 73
          Height = 22
          Caption = 'S&QL'
          Flat = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          OnClick = AuswahlBtnClick
          HotTrackFont.Charset = DEFAULT_CHARSET
          HotTrackFont.Color = clWindowText
          HotTrackFont.Height = -11
          HotTrackFont.Name = 'MS Sans Serif'
          HotTrackFont.Style = []
          OnMouseEnter = AuswahlBtnMouseEnter
          OnMouseLeave = AuswahlBtnMouseLeave
        end
        object FelderBtn: TJvSpeedButton
          Tag = 4
          Left = 207
          Top = 0
          Width = 80
          Height = 22
          Caption = '&Felder'
          Flat = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          Visible = False
          OnClick = AuswahlBtnClick
          HotTrackFont.Charset = DEFAULT_CHARSET
          HotTrackFont.Color = clWindowText
          HotTrackFont.Height = -11
          HotTrackFont.Name = 'MS Sans Serif'
          HotTrackFont.Style = []
          OnMouseEnter = AuswahlBtnMouseEnter
          OnMouseLeave = AuswahlBtnMouseLeave
        end
      end
    end
    object PC1: TJvPageControl
      Left = 0
      Top = 27
      Width = 775
      Height = 467
      ActivePage = AuswahlTS
      Align = alClient
      MultiLine = True
      Style = tsFlatButtons
      TabOrder = 1
      OnChange = PC1Change
      ClientBorderWidth = 0
      HideAllTabs = True
      object AuswahlTS: TTabSheet
        Caption = 'Auswahl'
        object ToolBar1: TToolBar
          Left = 0
          Top = 414
          Width = 775
          Height = 30
          Align = alBottom
          AutoSize = True
          BorderWidth = 2
          ButtonWidth = 87
          Caption = 'ToolBar1'
          EdgeInner = esNone
          EdgeOuter = esNone
          Flat = True
          Images = MainForm.ImageList1
          List = True
          ShowCaptions = True
          TabOrder = 0
          object DBNavigator1: TDBNavigator
            Left = 0
            Top = 0
            Width = 240
            Height = 22
            DataSource = ExportDS
            Flat = True
            TabOrder = 0
          end
          object ToolButton1: TToolButton
            Left = 240
            Top = 0
            Width = 8
            Caption = 'ToolButton1'
            Style = tbsSeparator
          end
          object AuswExportBtn: TToolButton
            Left = 248
            Top = 0
            AutoSize = True
            Caption = 'Export (CSV)'
            ImageIndex = 37
            OnClick = AuswExportBtnClick
          end
          object ToolButton3: TToolButton
            Left = 339
            Top = 0
            Width = 8
            Caption = 'ToolButton3'
            ImageIndex = 38
            Style = tbsSeparator
          end
          object EdiBtn: TToolButton
            Left = 347
            Top = 0
            AutoSize = True
            Caption = '&Bearbeiten'
            ImageIndex = 30
            OnClick = EdiBtnClick
          end
        end
        object ExportListGrid: TCaoDBGrid
          Left = 0
          Top = 0
          Width = 775
          Height = 414
          Align = alClient
          DataSource = ExportDS
          DefaultDrawing = False
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgConfirmDelete]
          TabOrder = 1
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          RowColor1 = 12255087
          RowColor2 = clWindow
          DefaultRowHeight = 17
          EditColor = clBlack
          Columns = <
            item
              Expanded = False
              FieldName = 'ID'
              Width = 40
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'KURZBEZ'
              Width = 490
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'LAST_CHANGE'
              Width = 70
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CHANGE_NAME'
              Width = 110
              Visible = True
            end>
        end
      end
      object AllgTS: TTabSheet
        Caption = 'Allgemein'
        ImageIndex = 3
      end
      object SQLExecTS: TTabSheet
        Caption = 'SQL'
        ImageIndex = 1
        object SQLEditor: TJvHLEditor
          Left = 0
          Top = 0
          Width = 775
          Height = 127
          Cursor = crIBeam
          ScrollBars = ssVertical
          GutterWidth = 0
          RightMarginVisible = False
          RightMarginColor = clSilver
          Completion.ItemHeight = 13
          Completion.Interval = 800
          Completion.ListBoxStyle = lbStandard
          Completion.CaretChar = '|'
          Completion.CRLF = '/n'
          Completion.Separator = '='
          TabStops = '3 5'
          SelForeColor = clHighlightText
          SelBackColor = clHighlight
          OnChange = SQLEditorChange
          Align = alTop
          Ctl3D = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Courier New'
          Font.Style = []
          ParentColor = False
          ParentFont = False
          TabStop = True
          UseDockManager = False
          HighLighter = hlSql
          Colors.Comment.Style = [fsItalic]
          Colors.Comment.ForeColor = clOlive
          Colors.Number.ForeColor = clNavy
          Colors.Strings.ForeColor = clPurple
          Colors.Symbol.ForeColor = clBlue
          Colors.Reserved.Style = [fsBold]
          Colors.Preproc.ForeColor = clGreen
          Colors.Statement.Style = [fsBold]
        end
        object JvxSplitter1: TJvxSplitter
          Left = 0
          Top = 127
          Width = 775
          Height = 3
          ControlFirst = SQLEditor
          Align = alTop
          TopLeftLimit = 100
          BottomRightLimit = 100
        end
        object ToolBar2: TToolBar
          Left = 0
          Top = 414
          Width = 775
          Height = 30
          Align = alBottom
          AutoSize = True
          BorderWidth = 2
          ButtonWidth = 96
          Caption = 'ToolBar1'
          EdgeInner = esNone
          EdgeOuter = esNone
          Flat = True
          Images = MainForm.ImageList1
          List = True
          ShowCaptions = True
          TabOrder = 2
          Wrapable = False
          object Auswahl2Btn: TToolButton
            Tag = 1
            Left = 0
            Top = 0
            AutoSize = True
            Caption = 'Zur�ck'
            ImageIndex = 36
            OnClick = AuswahlBtnClick
          end
          object ToolButton4: TToolButton
            Left = 65
            Top = 0
            Width = 8
            Caption = 'ToolButton4'
            ImageIndex = 38
            Style = tbsSeparator
          end
          object DBNavigator2: TDBNavigator
            Left = 73
            Top = 0
            Width = 190
            Height = 22
            DataSource = EDS
            VisibleButtons = [nbFirst, nbPrior, nbNext, nbLast, nbRefresh]
            Flat = True
            TabOrder = 0
          end
          object ToolButton5: TToolButton
            Left = 263
            Top = 0
            Width = 8
            Caption = 'ToolButton5'
            ImageIndex = 0
            Style = tbsSeparator
          end
          object SQLExecBtn: TToolButton
            Left = 271
            Top = 0
            AutoSize = True
            Caption = 'Ausf�hren (F8)'
            ImageIndex = 18
            OnClick = SQLExecBtnClick
          end
          object ToolButton2: TToolButton
            Left = 371
            Top = 0
            Width = 8
            Caption = 'ToolButton1'
            Style = tbsSeparator
          end
          object ExportBtn: TToolButton
            Left = 379
            Top = 0
            AutoSize = True
            Caption = 'Export (CSV)'
            ImageIndex = 37
            OnClick = ExportBtnClick
          end
        end
        object SqlGrid: TCaoDBGrid
          Left = 0
          Top = 130
          Width = 775
          Height = 284
          Align = alClient
          DataSource = EDS
          DefaultDrawing = False
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgConfirmDelete, dgCancelOnExit]
          ReadOnly = True
          TabOrder = 3
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          RowColor1 = 12255087
          RowColor2 = clWindow
          ShowTitleEllipsis = True
          DefaultRowHeight = 17
          EditColor = clBlack
        end
      end
      object FelderTS: TTabSheet
        Caption = 'Felder'
        ImageIndex = 2
      end
    end
  end
  object MainMenu1: TMainMenu
    Images = MainForm.ImageList1
    Left = 212
    Top = 84
    object Bearbeiten1: TMenuItem
      Caption = '&Bearbeiten'
      GroupIndex = 2
      object Ausfhren1: TMenuItem
        Caption = 'Ausf�hren'
        ImageIndex = 18
        ShortCut = 119
        OnClick = Ausfhren1Click
      end
    end
    object Sortierung1: TMenuItem
      Caption = '&Sortierung'
      Enabled = False
      GroupIndex = 3
      Visible = False
    end
    object Ansicht1: TMenuItem
      Caption = '&Ansicht'
      Enabled = False
      GroupIndex = 4
      Visible = False
    end
  end
  object ExportTab: TZMySqlQuery
    Database = DM1.db1
    Transaction = DM1.Transact1
    CachedUpdates = False
    ShowRecordTypes = [ztModified, ztInserted, ztUnmodified]
    Options = [doHourGlass, doAutoFillDefs, doUseRowId]
    LinkOptions = [loAlwaysResync]
    Constraints = <>
    AfterOpen = ExportTabAfterPost
    BeforePost = ExportTabBeforePost
    AfterPost = ExportTabAfterPost
    AfterScroll = ExportTabAfterPost
    ExtraOptions = [moStoreResult]
    Macros = <>
    Sql.Strings = (
      'select * from EXPORT'
      'order by KURZBEZ')
    RequestLive = True
    Left = 56
    Top = 85
    object ExportTabID: TIntegerField
      DisplayLabel = 'lfd. Nr.'
      FieldName = 'ID'
      DisplayFormat = '00000'
    end
    object ExportTabKURZBEZ: TStringField
      DisplayLabel = 'Beschreibung'
      DisplayWidth = 50
      FieldName = 'KURZBEZ'
      Required = True
      Size = 255
    end
    object ExportTabINFO: TMemoField
      FieldName = 'INFO'
      Required = True
      Visible = False
      BlobType = ftMemo
    end
    object ExportTabQUERY: TMemoField
      FieldName = 'QUERY'
      Required = True
      Visible = False
      BlobType = ftMemo
    end
    object ExportTabFELDER: TMemoField
      FieldName = 'FELDER'
      Visible = False
      BlobType = ftMemo
    end
    object ExportTabFORMULAR: TBlobField
      FieldName = 'FORMULAR'
      Visible = False
      BlobType = ftBlob
    end
    object ExportTabFORMAT: TStringField
      FieldName = 'FORMAT'
      Visible = False
      Size = 3
    end
    object ExportTabFILENAME: TStringField
      FieldName = 'FILENAME'
      Visible = False
      Size = 255
    end
    object ExportTabLAST_CHANGE: TDateTimeField
      DisplayLabel = 'le. �nderung'
      FieldName = 'LAST_CHANGE'
      DisplayFormat = 'dd.mm.yyyy'
    end
    object ExportTabCHANGE_NAME: TStringField
      DisplayLabel = 'ge�nd. von'
      DisplayWidth = 10
      FieldName = 'CHANGE_NAME'
      Size = 100
    end
  end
  object ExportDS: TDataSource
    DataSet = ExportTab
    Left = 137
    Top = 85
  end
  object EQuery: TZMySqlQuery
    Database = DM1.db1
    Transaction = DM1.Transact1
    CachedUpdates = False
    ShowRecordTypes = [ztModified, ztInserted, ztUnmodified]
    Options = [doHourGlass, doAutoFillDefs, doUseRowId]
    LinkOptions = [loAlwaysResync]
    Constraints = <>
    AfterOpen = EQueryAfterOpen
    AfterClose = EQueryAfterOpen
    ExtraOptions = [moStoreResult]
    Macros = <>
    RequestLive = False
    Left = 284
    Top = 86
  end
  object EDS: TDataSource
    DataSet = EQuery
    Left = 348
    Top = 86
  end
  object SaveDialog1: TSaveDialog
    Left = 407
    Top = 86
  end
end
