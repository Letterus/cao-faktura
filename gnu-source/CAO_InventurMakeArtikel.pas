{******************************************************************************}
{ PROJEKT      : CAO-FAKTURA                                                   }
{ DATEI        : CAO_INVENTURMAKEARTIKEL.PAS / DFM                             }
{ BESCHREIBUNG : Dialog zur Erstellung von Inventuren                          }
{ STAND        : 07.05.2004                                                    }
{ VERSION      : 1.2.5.3                                                       }
{ � 2004 Jan Pokrandt / Jan@JP-Soft.de                                         }
{                                                                              }
{ Diese Unit geh�rt zum Projekt CAO-Faktura und wird unter der                 }
{ GNU General Public License Version 2.0 freigegeben                           }
{                                                                              }
{******************************************************************************}
{                                                                              }
{ This program is free software; you can redistribute it and/or                }
{ modify it under the terms of the GNU General Public License                  }
{ as published by the Free Software Foundation; either version 2               }
{ of the License, or any later version.                                        }
{                                                                              }
{ This program is distributed in the hope that it will be useful,              }
{ but WITHOUT ANY WARRANTY; without even the implied warranty of               }
{ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                }
{ GNU General Public License for more details.                                 }
{                                                                              }
{ You should have received a copy of the GNU General Public License            }
{ along with this program; if not, write to the Free Software                  }
{ Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.  }
{                                                                              }
{    ******* CAO-Faktura comes with ABSOLUTELY NO WARRANTY ***************     }
{                                                                              }
{******************************************************************************}
{                                                                              }
{ Historie :                                                                   }
{ 07.05.2004 - Unit ins CVS gestellt                                           }
{                                                                              }
{                                                                              }
{ Todo :                                                                       }
{                                                                              }
{******************************************************************************}

unit CAO_InventurMakeArtikel;

{$I CAO32.INC}

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, StdCtrls, ExtCtrls, Db, ZQuery, ZMySqlQuery;

type
  TInventurMakeArtikelForm = class(TForm)
    Label1: TLabel;
    PB1: TProgressBar;
    Timer1: TTimer;
    Query1: TZMySqlQuery;
    Query1ARTIKEL_ID: TIntegerField;
    Query1MENGE_DIFF: TFloatField;
    Query1STATUS: TIntegerField;
    Label2: TLabel;
    procedure FormActivate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure Timer1Timer(Sender: TObject);
    procedure Query1Progress(Sender: TObject; Stage: TZProgressStage;
      Proc: TZProgressProc; Position, Max: Integer; var Cancel: Boolean);
  private
    { Private-Deklarationen }
    InvID    : Integer;
    Run      : Boolean;
    Job      : Integer;
    Akt      : Integer;
    Err      : Integer;
  public
    { Public-Deklarationen }
    procedure MakeInventur (ID:Integer);
    function SaveInventur (ID:Integer;
                           var aktualisiert,
                               fehler : Integer):Boolean;
  end;

//var InventurMakeArtikelForm: TInventurMakeArtikelForm;

implementation

uses cao_dm, zextra;

{$R *.DFM}
//------------------------------------------------------------------------------
procedure TInventurMakeArtikelForm.MakeInventur (ID:Integer);
begin
     Label1.Caption :='Die Artikel-SOLL-Daten werden jetzt erstellt.'+#13#10+
                      'Bitte haben Sie etwas Geduld,'+#13#10+
                      'dieser Vorgang kann einige Minuten in Anspruch nehmen ...';
     Caption :='Inventur wird erstellt ...';
     InvID :=ID;
     Job   :=1;
     ShowModal;
end;
//------------------------------------------------------------------------------
function TInventurMakeArtikelForm.SaveInventur (ID:Integer;
                                                var aktualisiert,
                                                    fehler : Integer):Boolean;
begin
     Label1.Caption :='Die Inventur wird nun abgeschlossen,'+#13#10+
                      'dabei werden die Fehlmengen im Lager korregiert.'+#13#10+
                      'Bitte haben Sie etwas Geduld,'+#13#10+
                      'dieser Vorgang kann einige Minuten in Anspruch nehmen ...';

     Caption :='Inventur wird abgesclossen ...';
     InvID :=ID;
     Job   :=2;
     Akt   :=0;
     Err   :=0;
     ShowModal;

     aktualisiert :=akt;
     fehler       :=err;
     Result       :=True;
end;
//------------------------------------------------------------------------------
procedure TInventurMakeArtikelForm.FormCreate(Sender: TObject);
begin
     Run :=False;
     Timer1.Enabled :=False;
     Job :=0;
end;
//------------------------------------------------------------------------------
procedure TInventurMakeArtikelForm.FormActivate(Sender: TObject);
var s,i : integer;
begin
     Run :=True;

     if Job=1 then // Inventur erstellen
     begin
       try
         Application.ProcessMessages;

         query1.sql.text :=
          'insert into ARTIKEL_INVENTUR (INVENTUR_ID,ARTIKEL_ID,WARENGRUPPE,'+
          'ARTNUM,MATCHCODE,BARCODE,KURZTEXT,MENGE_SOLL,INVENTUR_WERT,EK_PREIS)'+
          ' select :INV_ID,REC_ID,WARENGRUPPE,ARTNUM,MATCHCODE,BARCODE,'+
          'KURZNAME,MENGE_AKT,INVENTUR_WERT,EK_PREIS from ARTIKEL '+
          'where ARTIKEL.ARTIKELTYP in ("N","S")';
         query1.ParamByName('INV_ID').asInteger :=INVID;

         query1.OnProgress :=Query1Progress;
         query1.ExecSql;
       finally
         Run :=False;
       end;
     end
        else
     if Job=2 then // Inventur abschlie�en
     begin
         Application.ProcessMessages;
         try
           Screen.Cursor :=crSqlWait;
           query1.sql.text :=
            'select ARTIKEL_ID,MENGE_DIFF,STATUS from ARTIKEL_INVENTUR '+
           // 'where INVENTUR_ID=:INV_ID';
            'where INVENTUR_ID=:INV_ID and MENGE_DIFF !=0 and STATUS=1';
           query1.ParamByName('INV_ID').asInteger :=INVID;

           PB1.Position :=0;
           query1.open;
           PB1.Max :=Query1.RecordCount;
           I :=1;

           dm1.uniquery.close;
           dm1.uniquery.sql.text :='';

           while not query1.eof do
           begin
              PB1.Position :=query1.RecNo;
              Label2.Caption :=IntToStr(Query1.RecNo)+ ' von '+
                               IntToStr(Query1.RecordCount);

              if i>=100 then
              begin
                   i:=1;
                   Application.ProcessMessages;
              end
              else inc(i);

              try
                dm1.uniquery.sql.text :=
                  'UPDATE ARTIKEL SET '+
                  'MENGE_AKT=MENGE_AKT+ '+
                  FloatToStrEx(query1menge_diff.asfloat)+
                  ' WHERE REC_ID='+IntToStr(Query1Artikel_ID.AsInteger);
                dm1.uniquery.execsql;

                s :=dm1.uniquery.RowsAffected;

                if s=1
                 then begin s:=2; inc(akt); end
                 else begin s:=3; inc(err); end;
              except
                s :=4; inc(err);
              end;

              try
                dm1.uniquery.sql.text :=
                  'UPDATE ARTIKEL_INVENTUR SET STATUS='+Inttostr(S)+
                  ' WHERE INVENTUR_ID='+Inttostr(InvID)+
                  ' and ARTIKEL_ID='+IntTostr(Query1Artikel_ID.AsInteger);
                dm1.uniquery.execsql;
              except end;
              query1.next;
           end;
           query1.close;
         finally
           Run :=False;
           Screen.Cursor :=crDefault;
         end;
     end;
     Timer1.Enabled :=True;
end;
//------------------------------------------------------------------------------
procedure TInventurMakeArtikelForm.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
     CanClose :=not Run;
end;
//------------------------------------------------------------------------------
procedure TInventurMakeArtikelForm.Timer1Timer(Sender: TObject);
begin
     Timer1.Enabled :=False;
     Close;
end;
//------------------------------------------------------------------------------
procedure TInventurMakeArtikelForm.Query1Progress(Sender: TObject;
  Stage: TZProgressStage; Proc: TZProgressProc; Position, Max: Integer;
  var Cancel: Boolean);
begin
     PB1.Max :=Max;
     PB1.Position :=Position;
     Application.ProcessMessages;
end;
//------------------------------------------------------------------------------
end.
