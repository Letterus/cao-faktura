{******************************************************************************}
{ PROJEKT      : CAO-FAKTURA                                                   }
{ DATEI        : CAO_INVENTURPRINT.PAS / DFM                                   }
{ BESCHREIBUNG : Dialog zum Drucken von Inventur-Listen                        }
{ STAND        : 16.05.2004                                                    }
{ VERSION      : 1.2.5.4                                                       }
{ � 2004 Jan Pokrandt / Jan@JP-Soft.de                                         }
{                                                                              }
{ Diese Unit geh�rt zum Projekt CAO-Faktura und wird unter der                 }
{ GNU General Public License Version 2.0 freigegeben                           }
{                                                                              }
{******************************************************************************}
{                                                                              }
{ This program is free software; you can redistribute it and/or                }
{ modify it under the terms of the GNU General Public License                  }
{ as published by the Free Software Foundation; either version 2               }
{ of the License, or any later version.                                        }
{                                                                              }
{ This program is distributed in the hope that it will be useful,              }
{ but WITHOUT ANY WARRANTY; without even the implied warranty of               }
{ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                }
{ GNU General Public License for more details.                                 }
{                                                                              }
{ You should have received a copy of the GNU General Public License            }
{ along with this program; if not, write to the Free Software                  }
{ Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.  }
{                                                                              }
{    ******* CAO-Faktura comes with ABSOLUTELY NO WARRANTY ***************     }
{                                                                              }
{******************************************************************************}
{                                                                              }
{ Historie :                                                                   }
{ 07.05.2004 - Unit ins CVS gestellt                                           }
{                                                                              }
{                                                                              }
{ Todo :                                                                       }
{                                                                              }
{******************************************************************************}

unit CAO_InventurPrint;

{$I CAO32.INC}

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, JvLookup, Db, ppDB, ppComm, ppRelatv, ppDBPipe,
  ZQuery, ZMySqlQuery, JvSpin, ppBands, ppReport, ppPrnabl, ppClass,
  ppStrtch, ppSubRpt, ppCache, ppProd, ppEndUsr, ppViewr, ppDevice, ppPrnDev,
  JvEdit, Mask, JvMaskEdit, ExtCtrls;

type tInventurFormular = (ivZaehlliste, ivWertListe, ivAuswertung);

type
  TInventurPrintForm = class(TForm)
    Label1: TLabel;
    Label2: TLabel;
    FormularCB: TJvDBLookupCombo ;
    ZielCB: TComboBox;
    LayoutBtn: TBitBtn;
    BinNamCB: TComboBox;
    AnzCopy: TJvSpinEdit;
    VorschauBtn: TBitBtn;
    PrintBtn: TBitBtn;
    DruEinrBtn: TBitBtn;
    CloseBtn: TBitBtn;
    InvArtTab: TZMySqlQuery;
    InvArtDS: TDataSource;
    InvArtPipeline: TppDBPipeline;
    ReportTab: TZMySqlQuery;
    ReportTabMAINKEY: TStringField;
    ReportTabNAME: TStringField;
    ReportTabVAL_BLOB: TMemoField;
    ReportTabVAL_CHAR: TStringField;
    ReportTabVAL_BIN: TBlobField;
    ReportDS: TDataSource;
    RepPipeline: TppDBPipeline;
    ppDesigner1: TppDesigner;
    Report: TppReport;
    ppHeaderBand1: TppHeaderBand;
    ppDetailBand1: TppDetailBand;
    ppFooterBand1: TppFooterBand;
    Label7: TLabel;
    Label8: TLabel;
    InvArtTabWGR_NUMERISCH: TIntegerField;
    InvArtTabARTNUM: TStringField;
    InvArtTabMATCHCODE: TStringField;
    InvArtTabBARCODE: TStringField;
    InvArtTabKURZTEXT: TStringField;
    InvArtTabMENGE_SOLL: TFloatField;
    InvArtTabWARENGRUPPE: TStringField;
    InvWertTab: TZMySqlQuery;
    IntegerField1: TIntegerField;
    StringField1: TStringField;
    StringField2: TStringField;
    StringField3: TStringField;
    StringField4: TStringField;
    StringField5: TStringField;
    InvWertTabWERT_EINZEL: TFloatField;
    InvWertTabMENGE_IST: TFloatField;
    InvWertTabEK_PREIS: TFloatField;
    InvWertTabINVENTUR_WERT: TFloatField;
    InvWertTabWERT_SUM: TFloatField;
    InvWertDS: TDataSource;
    InvWertPipeline: TppDBPipeline;
    InvArtTabLAENGE: TStringField;
    InvArtTabGROESSE: TStringField;
    InvArtTabDIMENSION: TStringField;
    InvArtTabGEWICHT: TFloatField;
    InvWertTabLAENGE: TStringField;
    InvWertTabGROESSE: TStringField;
    InvWertTabDIMENSION: TStringField;
    InvWertTabGEWICHT: TFloatField;


    procedure ReportTabBeforePost(DataSet: TDataSet);
    procedure ReportTabBeforeOpen(DataSet: TDataSet);
    procedure CloseBtnClick(Sender: TObject);
    procedure FormularCBChange(Sender: TObject);
    procedure ZielCBChange(Sender: TObject);
    procedure PrintBtnClick(Sender: TObject);
    procedure LayoutBtnClick(Sender: TObject);
    procedure ReportPreviewFormCreate(Sender: TObject);
    procedure VorschauBtnClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure AnzCopyChange(Sender: TObject);
    procedure ppDesigner1CloseQuery(Sender: TObject;
      var CanClose: Boolean);
    procedure DruEinrBtnClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure InvArtTabCalcFields(DataSet: TDataSet);
  private
    { Private-Deklarationen }
    MainKey     : String;
    InvNum      : Integer;
    WGR         : Integer;
    InvFormular : tInventurFormular;

    //JName        : String;
    //VDat, BDat   : tDateTime;
    //ZeitraumStr  : String;

  public
    { Public-Deklarationen }
    procedure PrintArtikelZaehlliste (NewNum, NewWGR : Integer);
    procedure PrintArtikelWertliste (NewNum, NewWGR : Integer);
  end;

//var
//  PrintArtikelForm: TPrintArtikelForm;

implementation

{$R *.DFM}

uses cao_dm, cao_var_const, cao_tool1, ppTypes, ppPrintr, CAO_ARTIKEL1;

//------------------------------------------------------------------------------
procedure TInventurPrintForm.FormCreate(Sender: TObject);
begin
     height :=183;

     if Screen.PixelsPerInch <> 96 then
     begin
       Self.ScaleBy (96, Screen.PixelsPerInch);
       Refresh;
     end;
end;
//------------------------------------------------------------------------------
procedure TInventurPrintForm.PrintArtikelZaehlliste (NewNum, NewWGR : Integer);
var Res : Boolean; i:integer;
begin
     WGR    :=NewWGR; // Warengruppe
     InvNum :=NewNum; // Inventurnummer

     InvFormular :=ivZaehlliste;

     MainKey :='MAIN\REPORT\INVENTUR\ZAEHLLISTE';
     Caption :='Inventurliste drucken';
     ppDesigner1.Caption :='Layout Inventur Artikelz�hlliste bearbeiten';
     Report.DataPipeline :=InvArtPipeline;

     InvArtTab.Close;

     InvArtTab.Sql.Clear;
     InvArtTab.Sql.Add ('select '+
                        'AI.WARENGRUPPE as WGR_NUMERISCH,'+
                        'AI.ARTNUM,'+
                        'AI.MATCHCODE,'+
                        'AI.BARCODE,'+
                        'AI.KURZTEXT,'+
                        'AI.MENGE_SOLL,'+
                        'A.LAENGE,'+
                        'A.GROESSE,'+
                        'A.DIMENSION,'+
                        'A.GEWICHT,'+
                        'W.NAME as WARENGRUPPE '+
                        'from ARTIKEL_INVENTUR AI, WARENGRUPPEN W '+
                        'left outer join ARTIKEL A on AI.ARTIKEL_ID=A.REC_ID '+
                        'where AI.WARENGRUPPE=W.ID and '+
                        'AI.INVENTUR_ID='+Inttostr(InvNum)+' ');

     if Wgr>0 then
     InvArtTab.Sql.Add ('and AI.WARENGRUPPE='+IntToStr(Wgr)+' ');

     InvArtTab.Sql.Add ('order by WGR_NUMERISCH, AI.ARTNUM, AI.MATCHCODE, AI.KURZTEXT');



     InvArtTab.Open;

     ReportTab.Close;
     ReportTab.Open;

     Res :=FormularCB.Locate (ReportTabVal_Char,'DEFAULT',True);
     if not Res then
     begin
         ReportTab.First;
         FormularCB.DisplayValue :=ReportTabName.Value;
     end
       else
     begin
          FormularCB.DisplayValue :=ReportTabName.Value;
     end;

     ZielCB.Items.Assign (Report.PrinterSetup.PrinterNames);
     I :=ZielCB.Items.IndexOf ('Screen');
     if I>=0 then ZielCB.Items.Delete (i);

     if ZielCB.Items.Count >=0 then
      for i:=0 to ZielCB.Items.Count-1 do
       if ZielCB.Items[i]=Report.PrinterSetup.PrinterName
         then ZielCB.ItemIndex :=I;

     if ZielCB.ItemIndex=-1 then
      if ZielCB.Items.IndexOf ('Default')>-1
       then ZielCB.ItemIndex := ZielCB.Items.IndexOf ('Default');

     ZielCBChange(Self);

     ShowModal;

     if InvArtTab.Active then InvArtTab.Close;

     ReportTab.Close;
end;
//------------------------------------------------------------------------------
procedure TInventurPrintForm.PrintArtikelWertliste (NewNum, NewWGR : Integer);
var Res : Boolean; i:integer;
begin
     WGR    :=NewWGR; // Warengruppe
     InvNum :=NewNum; // Inventurnummer

     InvFormular :=ivWertliste;

     MainKey :='MAIN\REPORT\INVENTUR\WERTLISTE';
     Caption :='Inventurliste drucken';
     ppDesigner1.Caption :='Layout Inventur Artikelwertliste bearbeiten';
     Report.DataPipeline :=InvWertPipeline;

     InvWertTab.Close;

     InvWertTab.Sql.Text :=
      'select '+
      'AI.WARENGRUPPE as WGR_NUMERISCH,AI.ARTNUM,AI.MATCHCODE,AI.BARCODE,'+
      'AI.KURZTEXT,A.LAENGE,A.GROESSE,A.DIMENSION,A.GEWICHT,AI.MENGE_IST,'+
      'AI.EK_PREIS,AI.INVENTUR_WERT,'+
      '(AI.MENGE_IST * AI.EK_PREIS / 100 * AI.INVENTUR_WERT) as WERT_SUM,'+
      '(AI.EK_PREIS / 100 * AI.INVENTUR_WERT) as WERT_EINZEL,'+
      'W.NAME as WARENGRUPPE from ARTIKEL_INVENTUR AI, WARENGRUPPEN W '+
      'left outer JOIN ARTIKEL A on A.REC_ID=AI.ARTIKEL_ID '+
      'where AI.WARENGRUPPE= W.ID and AI.INVENTUR_ID=:ID '+
      'and MENGE_IST != 0 ';

     if Wgr>0 then InvWertTab.Sql.Text :=InvWertTab.Sql.Text +
      'and AI.WARENGRUPPE='+IntToStr(Wgr)+' ';

     InvWertTab.Sql.Text :=InvWertTab.Sql.Text +
      'order by WGR_NUMERISCH, AI.ARTNUM, AI.MATCHCODE, AI.KURZTEXT';


     InvWertTab.ParamByName ('ID').AsInteger :=InvNum;
     InvWertTab.Open;

     ReportTab.Close;
     ReportTab.Open;

     Res :=FormularCB.Locate (ReportTabVal_Char,'DEFAULT',True);
     if not Res then
     begin
         ReportTab.First;
         FormularCB.DisplayValue :=ReportTabName.Value;
     end
       else
     begin
          FormularCB.DisplayValue :=ReportTabName.Value;
     end;

     ZielCB.Items.Assign (Report.PrinterSetup.PrinterNames);
     I :=ZielCB.Items.IndexOf ('Screen');
     if I>=0 then ZielCB.Items.Delete (i);

     if ZielCB.Items.Count >=0 then
      for i:=0 to ZielCB.Items.Count-1 do
       if ZielCB.Items[i]=Report.PrinterSetup.PrinterName
         then ZielCB.ItemIndex :=I;

     if ZielCB.ItemIndex=-1 then
      if ZielCB.Items.IndexOf ('Default')>-1
       then ZielCB.ItemIndex := ZielCB.Items.IndexOf ('Default');

     ZielCBChange(Self);


     ShowModal;

     if InvWertTab.Active then InvArtTab.Close;

     ReportTab.Close;
end;
//------------------------------------------------------------------------------
procedure TInventurPrintForm.FormShow(Sender: TObject);
begin
     PrintBtn.SetFocus;
end;
//------------------------------------------------------------------------------
procedure TInventurPrintForm.ReportTabBeforePost(DataSet: TDataSet);
begin
     ReportTabMAINKEY.AsString :=MainKey;
end;
//------------------------------------------------------------------------------
procedure TInventurPrintForm.ReportTabBeforeOpen(DataSet: TDataSet);
begin
     ReportTab.ParamByName('KEY').Value :=MainKey;
end;
//------------------------------------------------------------------------------
procedure TInventurPrintForm.CloseBtnClick(Sender: TObject);
begin
     Close;
end;
//------------------------------------------------------------------------------
procedure TInventurPrintForm.FormularCBChange(Sender: TObject);
begin
     Report.Template.DatabaseSettings.Name :=FormularCB.DisplayValue;
     try Report.Template.LoadFromDatabase; except end;
     Report.PrinterSetup.Copies :=1;
     Report.Language :=lgGerman;
     Report.OnPreviewFormCreate :=ReportPreviewFormCreate;
     Report.AllowPrintToFile :=True;
end;
//------------------------------------------------------------------------------
procedure TInventurPrintForm.ZielCBChange(Sender: TObject);
begin
     {set the printer name for the device}
     Report.PrinterSetup.PrinterName  := ZielCB.Items[ZielCB.ItemIndex];

     Application.ProcessMessages;

     BinNamCB.Items.Assign (Report.PrinterSetup.BinNames);

     if BinNamCB.Items.IndexOf(Report.PrinterSetup.BinName)>-1
      then BinNamCB.ItemIndex :=BinNamCB.Items.IndexOf(Report.PrinterSetup.BinName);

     if BinNamCB.ItemIndex=-1 then BinNamCB.ItemIndex :=BinNamCB.Items.IndexOf('Default');

     AnzCopy.Value :=Report.PrinterSetup.Copies;
end;
//------------------------------------------------------------------------------
procedure TInventurPrintForm.PrintBtnClick(Sender: TObject);
begin
     Report.PrinterSetup.DocumentName :='CAO-Faktura Inventurliste';

     Report.DeviceType :=dtPrinter;
     Report.ShowPrintDialog :=False;
     Report.Print;
end;
//------------------------------------------------------------------------------
procedure TInventurPrintForm.LayoutBtnClick(Sender: TObject);
begin
     case InvFormular of
        ivZaehlliste : InvArtPipeline.Visible :=True;
        ivWertliste  : InvWertPipeline.Visible :=True;
     end;

     ppDesigner1.ShowModal;
     if Report.Template.DatabaseSettings.Name<>FormularCB.DisplayValue
      then FormularCB.DisplayValue :=Report.Template.DatabaseSettings.Name;

     case InvFormular of
        ivZaehlliste : InvArtPipeline.Visible :=False;
        ivWertliste  : InvWertPipeline.Visible :=False;
     end;

     Report.OnPreviewFormCreate :=ReportPreviewFormCreate;
     Report.AllowPrintToFile :=True;
end;
//------------------------------------------------------------------------------
procedure TInventurPrintForm.ReportPreviewFormCreate(Sender: TObject);
begin
     Report.PreviewForm.WindowState := wsMaximized;
     TppViewer(Report.PreviewForm.Viewer).ZoomSetting := zs100Percent;
end;
//------------------------------------------------------------------------------
procedure TInventurPrintForm.VorschauBtnClick(Sender: TObject);
begin
     Report.DeviceType :=dtScreen;
     Report.Print;
     Report.DeviceType :=dtPrinter;
end;
//------------------------------------------------------------------------------
procedure TInventurPrintForm.FormDestroy(Sender: TObject);
begin
end;
//------------------------------------------------------------------------------
procedure TInventurPrintForm.AnzCopyChange(Sender: TObject);
begin
     Report.PrinterSetup.Copies :=round(ANzCopy.Value);
end;
//------------------------------------------------------------------------------
procedure TInventurPrintForm.ppDesigner1CloseQuery(Sender: TObject;
  var CanClose: Boolean);

var Res : Integer;
begin
     if Report.Modified then
     begin
        Res :=MessageDlg ('Bericht wurde ver�ndert.'+#13#10+
                          'Wollen Sie die �nderungen speichern ?',
                          mtconfirmation,mbyesnocancel,0);
        if Res=mryes then
        begin
           Report.Template.Save;
           CanClose :=True;
        end else
        if Res=mrNo then
        begin
           Report.Template.Load;
           CanClose :=True;
        end else CanClose :=False;
     end
     else CanClose :=True;
end;
//------------------------------------------------------------------------------
procedure TInventurPrintForm.DruEinrBtnClick(Sender: TObject);
var myPrinter : TppPrinter;
begin
     myPrinter := TppPrinter.Create;
     try
        myPrinter.PrinterSetup :=Report.PrinterSetup;

        if myPrinter.ShowSetupDialog
         then Report.PrinterSetup := myPrinter.PrinterSetup;

        if ZielCB.Items.IndexOf (Report.PrinterSetup.PrinterName)>0
         then ZielCB.ItemIndex :=ZielCB.Items.IndexOf (Report.Printer.PrinterName);

       ZielCBChange(Sender);
     finally
        myPrinter.Free;
     end;
end;
//------------------------------------------------------------------------------
procedure TInventurPrintForm.InvArtTabCalcFields(DataSet: TDataSet);
begin
     //
end;
//------------------------------------------------------------------------------
end.
