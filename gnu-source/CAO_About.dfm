object AboutBox: TAboutBox
  Left = 526
  Top = 215
  BorderStyle = bsToolWindow
  Caption = 'Programm-Information'
  ClientHeight = 288
  ClientWidth = 484
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = True
  Position = poMainFormCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PC1: TPageControl
    Left = 0
    Top = 0
    Width = 484
    Height = 254
    ActivePage = TabSheet3
    Align = alTop
    TabOrder = 0
    object TabSheet1: TTabSheet
      Caption = 'Allgemein'
      object Copyright: TLabel
        Left = 0
        Top = 144
        Width = 476
        Height = 82
        Align = alBottom
        Alignment = taCenter
        AutoSize = False
        Caption = 
          #13#10'� 1995-2004 - JP-SOFT Jan Pokrandt und das CAO-Faktura Team'#13#10'D' +
          'ieses Programm ist freie Software. '#13#10'Gro�e Teile des Sourcecodes' +
          ' stehen ebenfalls unter der '#13#10'GNU General Public License zur Ver' +
          'f�gung.'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = True
        WordWrap = True
        IsControl = True
      end
      object Image1: TImage
        Left = 0
        Top = 0
        Width = 476
        Height = 144
        Align = alClient
        Center = True
        Picture.Data = {
          0B544A76474946496D616765211500004749463839616A007D00F70000000000
          FFFFFF362F2D736357252122010001C3C3C4ACACAD020406E9EAEBD7D8D92F30
          300606052222213C372FCAB69BA797828175652D2A26D5BFA3D1BBA0C8B39973
          6758DBC4A8D7C1A5D3BDA2D2BCA1D0BAA0CFBA9FCDB79DCBB69CCAB59BC9B49A
          C7B299C6B198D3BDA3D1BCA1CFBAA0CEB99FCDB89EC8B39AC2AE96BFAB93C7B3
          9ABBA891B19F89AC9B869A8B78918372756A5D7D7264786D60CCB69DC5B098B6
          A38DA2917E8B7D6D7B6F6171665A85786A5E554B5A52496D645A4B453E5A544D
          6A5F54574E453B352F63594F504840665C52332E2937322D332F2B1E1C1A5E58
          5212111027262531302F7876745352514D4C4B565554706255706357564C434F
          463E6D61565249415E554D7A695B746457685A4E594D437E6D5F7B6B5E66594E
          65584D61554B62564C5B50475A4F465D52495E534A49413A3F383236302B3832
          2D4D453E463F3949423C544D4779675965564B7766596F5F53816F6179685B7B
          6A5D7A695C7262566E5F536B5C5166584D7C6B5E756559746458605349736458
          726357695B5075665A6F60556C5E535A4E4564574D675A50413933544A424B42
          3B564C444D443D4E453E504740473F392C28252E2A273E39358A766874635777
          665A76655979685C78675B7161565C4F467363584E443D594E464C433D3D3631
          413A354D4641504944262321554F4B2F2C2A34312F5F5C5A64615F55473E7463
          5876655A433B36443C37312C2935302D3A35322C26232E2825322C29332F2D32
          2E2C1917161B1918383432211F1E2523222D2B2A3B393839373642403F474544
          3A312D37302D39322F2823212A2523302B29312C2A3732303A322F201D1C2421
          202522212724232623222926252B28272D2A296A6766302927322B29342D2B35
          2E2C38312F37302E393230372F2D3A3230231F1E24201F2824232B27262D2928
          2F2B2A2621202B26252D28272C27262E29282F2A2935302F2E2827312B2A3029
          28332C2B322B2A342D2C362F2E3931302621212924240F0E0E2927271514142F
          2D2D3533332A2929F9F9F99292923C3C3C0C0C0C090909010101FFFFFF21F904
          01000001002C000000006A007D000008FF00FF091C48B0A0C18308132A5CC8B0
          A1C38710234A9C48B1A2C58B18336ADCC8B1A3C78F20438A1C49B2A4C9932853
          AA5C080C58AF953007DE8312C51BB45A03ED3DF3560B5BAD6CB18E3123752A66
          C866CD9E1D5BD78E5D3A6291B02531A6849E3176D7B659DBBA155B366CD36CD5
          A247CF68465DCBD01DA196159BDB6B6E05C0B3A5ABD8356D02F2EADD8B6DD8B5
          57B66C31314B119F37765BE1EEDD7BCD5A376BD9164BE68B8DD63165C0083FE4
          D58CD63561D826EBC5E64ED7B9C8A2530B701B4E734367C6BEAACE7B8D9DB372
          A867A7C656CC896B84C8D2C5D57DED9CB35AD774EB1EB64ED96F82D0884DCB3D
          FB9A249EC9951367D7E4F93F7AE48285FF563E4CA8B5F1DA974B82F6BB092DF4
          C4252D7B073FFDEC61C47E696E2620BBF26CC6E8D29F7DF661330E61CA0CA75C
          68BB78466081EBF86214378A69E75633C70CF32081D628111337C3D4A79A36D7
          78338E861B12C80E4CDD28B8A03151A5482036D5E8A25233E711488B80FEC998
          1E3929FD724C8FCA5D034D3844FAA85B65BE9D142381D7B8538B921B46739229
          C2A488179504E267522FB46CC9E598AA09334B1425E5F28A9864B6299935C894
          444E926ED6E9E5484D5423629D6E5E93DF48CF6CC3E7A07A69C38E8722B543E8
          A279D9185214C6B0C92899DABCB28F48C85433E9A0ECDC23524F4B62B30D5C6E
          7D95E5A6A251F3524891D059A84FE190FFC28D38CC70A3CC4DDAB888AAA1F688
          64CEA99335468E246BA4814519860881451BD998330E39BA328A4D246585644C
          7DA79E038E398E14E2C72596846B49267204220823483C53CE7BC02E9A0D3799
          85240E75D858F34A39AFFC8187257DF8E1C7000003BC451F7D585207188E48E0
          4C35AEDA274CC3026CE3CD48E5F8378C3AA4A461062077FC1BF0C7200F20071F
          7808018B5D7B5AB80D39CFA4664D2E23D9A2D730F82A62C91D7DE011F2CE01E3
          718917790CE14CB4DA95C8CD2B7B6AB38E7E22A9B31A36E40CA3871772E8CCF3
          D50163E2872BCF58B3E130CF0848DD62EC5C2AD23749C0E2CD3263102146215B
          F85109D6580F824927CFA49CDA359711FF9DD7309188344C27030032CC3C69F0
          400623678401061E7DD07DB51F76A0F1CC5D450BA00BD2AA2513F847F4FCA087
          167784A1080E1E7070820935B8108319904BCEB31F78A48BE26CA2722301C402
          0C23CF47695421C71D96E801CA0D177C10020A287CA0810930749187C7B2839C
          891EC7B8A3B75ED9ACBDFD6AD478CA512C588C4108228F17B183062084E0BEFB
          22D030010E8A0C407DF501DB81453C7EE715DA32439A8D36A6112F8DDC23145D
          08C215A81083361061041E10C1FBDE27820E9C200863B81FFE0690893CD0E25A
          233A8724C636196CBCA2234DB8020B52978115F0A0051C90E004299801089021
          72F8DB821CFA45392C382D35B952C639FF6EB71B0174240D2C10810A5460021C
          BC80023214410540D03E1956E00346805BF532D1073098810CAAD884158EA11A
          13EA2219922AA12D3C620419EC60074630460B9417820A6820032638C104D817
          02118C20068CA883E42E710741A061113188400E2210845B7C2365D8F8863988
          289A775863622251840750B03C0ADC800A410842046ED00113541002429003D6
          0291094DB4E20D3268C1063430010C70000B92A45336A041C625598314232165
          083E6002191001062F78010E6610040850A00221B8C2F4AE960930B821022CA0
          C006DAD7C70BE0C00CAE908788AEE18CF4B083172291860B3E00020EC8200622
          C8230736E00117E8E0052738810CFE60FF899DF5611058E881333BC0490A6AE0
          0588E04428CC212225F02E2FD5C8861444F2061778A0032CD8410A3C70820C94
          800624B8400B7650030AC0C0157708991FFA608531B04003D09CA1084EE0021E
          648210411113D47441C2C958C31423D1410B1ED0C7E5756005302082157CD082
          093CE0948BB88406EB8005319860033590E10C3D6083330CE00E658884D7FC57
          8E713C5400D5A8C74864C002E54DF1032A70433F0040D765BC000320A86711EC
          17B040DCC1106C4881092AB0810974E003158862075A40864B5CA20F43A8C696
          AEF18D5C8C7536ECE8954866D056F8652002740D2D0098F082099CE00690E817
          C00251873DB4018621A8C10D70E00215FF7C80039C1481066E90052A0CC00B55
          78C558B7510ECB2E091C2489810A3CE03E0FA8E008A205803F10900D1B68C006
          3C98A6FDFAB0081864C00329C0410EAE30831CE800061FE0240666808B62AC01
          12A0D8CB36BE71D9D90069244450010DFA988117F023BA00A0C71360D0811A10
          411038BC832278D0CE0CD8E0053750C1044CC0581C90C0031F40833C6C510D75
          E04212E391180805680C92F860B97D9C400E1800E00528E00936088111103C80
          3E68421110C0C00670000A54A462092FE8800644100317883416AFA00D32F6D2
          3DCE55079D23C9C10AEB380233001800FB08C001821C83305C62008330841126
          4C0406F043010130C0132CE0010DB880FF0AF33386A6F2C2A6F9B2238D92A906
          94D36903B762C0CA0096460012B08307E4000CFDD2C4226E408122D0D51F4F08
          C0A0A57183E7C9E00531F046CAB6318E7710A718E21BC90E304C011B80A21FDD
          508612EA01807A1840D294A602822B11061D8C8008A255029AD33C83106C0002
          A030467D1743B3580C5B32D7E8460145320AAECE40073868010B54C082910241
          1A0748800122100344ACB4113778C15C45AB0F492BC00736A080077EA00C4189
          4618A6307675C4611254C4200638F8C0080EFB810F74600213B8812A9E708018
          0441137298831020200100973B000A48850B30E08260B76B31506B5575EE5B12
          64D8000328D02AFC4240010CDCA0080DFF30051EBCC085088C01C0F74880A413
          A0CE0FB0C11B10E30D29DC9D9A61E0A424A860B4C8656AC71A200100B8584419
          72800BE9FA23B407C8C7CC5361831910E07BD790C73278EED36C14A52459A0E2
          0C87C94D105CD106AC06000398708B7AF4A300FEF0872FA22E6988CB800BC25E
          D23970C1BB6158A92463D8E40451908216A080022748C1726D700BD1FA431F06
          38C0010C20F3BAA7D91ECA10009E25830D6E6C8F46D522C91B52C0CDE6D6E00A
          4640A60C5C70020FF420BA51D087E4271F79C9EB03154C08E07F8AA12720B6A3
          492571660D26D8EF1C90A2077D4681738D7084FF2A610730D04221C010064118
          C10C66C8042290C08EF464E31973168DFF36AA815C93F420051B10810C6B0082
          0B4080083648FF4C47908217E4A0D200BFA306F64F010D5C000342707516520C
          C2311BCF801246500319A06E1DA501CE7303426003199058EDA47F2870451E80
          61280002CC434C3C204EE9610CE4C07592310C5F7712C060063800012F700544
          80063880012FD00337600287C53C204003A6C40236600335C05CEE53608AF00D
          170744E7B00C3D3533A19712A9C00B90D00AE7D7011CF00246E0032E90021590
          0113C0023950056550058E205019200228800110100BC6901E34A20C39221ADB
          B0672A41045EF005AEB003B6444F3270056EA44866100A63800778C0099B4004
          21F07F36D003E6F020E4600CAE620DD7FFF0732A510A03100895B30638C002B5
          740135A00239700BC120087C9009FEE20775E0096580033BD0068F3423B4100D
          94C417E6B012A5B0077DC00978C0088880053F90032F000101570560403C2053
          48CDD004CF000E1B52198F9432C3B00C2B71065F960961907D7EC00586800544
          A0038B2008A21832766008E38018DF231AFF3344A2710DA4B06C25310A73D007
          0363056110397D50078490088FF02D2173097590074770393E720DCDA071A2D1
          29293106AA640985C00679D02F767008AE600899B0051F1308E1A209668006AF
          20593E42339AB637C78012A2A007833000AC50058C202E65E08776603D85A408
          58A0087AE00A0CB59154D17B3E055427FFD10382D40784E00661E0057AD0086E
          500899F0317E504D65E0068A5008755007AE50313E121AB7002DA2610DEA8012
          7AF0657E900764900967000A66503002235582800590603E8320075BA07DA4A1
          24DBB00C33291A9048124B20912203069F000990500872F02F5B7009AB00068F
          50048750637D303786E90A3F2423E5410EBB2109506012FCC44197B008925006
          7DA04A03600983100658C0088790099780981F930964907732521950391AFE23
          00ED106A22510A95200776200690E00684F005B4338F6120048A300682C00957
          530998C00654A925D640004B860DC9C188D82033C2E01C2431067C40058AD00A
          66500796A0337D49068E20067D8008E0FF8935B5180AC7B921DBA004D0D0187A
          A20CC6405CDE200CDF4012BF300063100A8A80075A033008F90842D007D58708
          78A04121D307809006E5308EA20138DDB00DAFA00CC3D00DF1100BEE800BEC80
          0C13151250200665F00985F05802630961E00688705360A009415009041A3296
          A009A1700E5AE20EC4302ACB600CE8C053F3E50DC8C0342021098D50052B1530
          7E700760500484200772400488900762703EBE4537A29806E7691FB1A004D5B0
          0DE7C01F4A000BFFB30E38F911CEA006676009F7530767000979205581C0087A
          80076DE03854B0A2D68353A6A0A07B0119BAA00B7AA204B5E00C04B00DD8100D
          E600331D310A64B00964400877609700FF53076130048220487E100856A00763
          500669693F72DA57752008D8E06905F239CF300CA4100D59F70EDA600D0BD311
          68300776700776D04F9CCA05B9F0087C2030805006604006FE3200FBB8909263
          075630AAF6B10DDC20104DF00AE7A004B1E00C28530EE2A08E17A10882A45296
          2006FAE1039A39009720068C806895D00764F0088440085673357D3007B41097
          0BE228FF700AB3300FD6400EB8E088ECE00D5F8A11FB3004470A32AB600968D0
          00021104D4A37D6DA0073A74068EA00984C0AB8CCA3399200849F01EDA410BA1
          D604E2700FBE300C4A3082B0800BC4B0116F50053F500838E4075A600945B008
          A1201046F06500730953400A67502E64FF900799807DD54A37666A0E36A91AD7
          B00E05310F0B000CEBA00BCA900DD7500FB840A81891077420068E7098F67308
          60200484B00402B10473403D5A500463D0A95830A0A10006230932040A0876F0
          08E7B0797BA10D7F4710B8F00FC8200E4C700DD9C00CCA600AD26911BA108C96
          E00A60A0050DE90893F00970800A02810A5D1B3097500862E0A8A2D00779C006
          DB0A327500B3AB350881100B8BB9A0EBD01D07D10B92300FF2B00DB5E00491E0
          0D916911AEF008AC50076CC0036C8006A1E00762D00672C003032108DBEA0753
          200407830603000665509420230762D00A63C0451F830962700CDBA30DEC9110
          B9D076D77026522000E5471152500945FF500566B0082D55044370077900097E
          200403D1951F6309DD82078A400863400697DB576630048F3005640A30B33909
          E1C779D5D00C0AB10FF6C00BBC400DB9800AB6200BB01911A910069AE0098C00
          865E70083D200784000971000403510A5929A47AC008602508842004C81B3275
          E0079D9008637A0981203263006255B9460BB100023B0BB3F00FBD900E273811
          B0E00A5F40078E300C5EF0079F8009723004B190A1FF10058770B60103099E30
          06888008A2A00531BC3394A3076DD00A8090093AD30793E00E0359BD0B3118FF
          400C93F00FCE200BDD6011863008006B05946008DDA00766202105D106DA653F
          85E0098BA0067A00097CC53381800978FFE008A130059940918BF0B97A710D07
          12114A6023C710AD1531A096E00668A00A8A3B0A09B108292C3296C005B3300C
          52A3B93BE3337760058DCC998AE00C63330CC5C0A310310F02710A882211D220
          927A10CA0DA108DD08307E200797F009F550057670AEB3A305D7498A64A00C3D
          720D91C0C716D10FF830116F500977800510710F66A005F7D3077C3006688009
          A53C3B7720044550076540CD937C0CBB6014AEA00578D0C610C10B63A005FD52
          0945090662900489F0C7E8EA07AD00067FE050ADC90C66910874400613610F5D
          10089A40A4AF5B065DF00AA260075205A58200098A400A1A720DD390AF30B104
          5D50114CE00655900668F08777C0078AFE800C6810045B6009972007C50C325A
          100AA47017C3900EC06716AE30116EF00762800729C94172F00745F008652006
          42E0853C20065560B22193A4B8A015E2B02ADEF11089403C9780076840947DA0
          07A120087730087D700984A00754A009823005A41930726004CA500D3F1CD60D
          F10BCEEB0784A008AA74098DC08DE7EA2F2AEA0780D033FE92095AF00870E8D7
          0F510676F0055FE0058A80085E30050EC0433CA4C45A90099530305B50077CB0
          058450086F3097940D11A09006B12D0BCA200677D0088F600984C00554700564
          300655F0073A30D7B9DA086F9005AFCD11DB200A3FF00FD9200A4E9CDCD23DDD
          D45DDDD67DDDD89DDDDABDDDFF1010003B}
        Transparent = True
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'Entwickler / Komponenten'
      ImageIndex = 1
      object RichEdit2: TRichEdit
        Left = 0
        Top = 0
        Width = 476
        Height = 226
        Align = alClient
        Lines.Strings = (
          'Entwickler (alphabetisch):'
          ''
          '- Daniel Pust '#9#9'(Tom)'
          '   - Wartungsmodul'
          '   - Kasse/Displayansteuerung'
          ''
          '- Jan Pokrandt '#9#9'(Admin)'
          '   - hat den Rest codiert ;-)'
          ''
          '- Karl Langmann '#9#9'(Karl)'
          '   - Mitarbeiterstamm'
          '   - Vertreterstamm'
          ''
          '- Matthias St�bner'#9#9'(MStuebner)'
          '   - Einbindung Hilfe'
          ''
          '- Norbert Herfurth'#9#9'(NLH)'
          '  - IBM-Fibu-Anbindung'
          ''
          'PHP-Scripte :'
          ''
          '- Henri Schmidthuber'#9'(henri)'
          '- Jan Pokrandt '#9#9'(Admin)'
          ''
          'Dokuteam / FAQ :'
          ''
          '- Daniel Pust '#9#9'(Tom)'
          '- Matthias St�bner '#9#9'(mstuebner) '
          '- Dirk Paulsen '#9#9'(Mac)'
          ''
          ''
          'verwendete Komponenten und Tools :'
          ''
          'Jedi-VCL'
          
            'This program contains JVCL source code that can be obtained from' +
            ' '
          'http://jvcl.sourceforge.net/'
          
            'Der Code vom Project JEDI steht unter der Mozilla Public Licence' +
            ' ("MPL") '
          'Version 1.1.'
          'http://delphi-jedi.org               '
          ''
          'TurboPower Visual PlanIt Mozilla Public License 1.1 (MPL 1.1)'
          'http://sourceforge.net/projects/tpvplanit/'
          ''
          'TmpsDTAUS  Version 1.1 14.09.2001 � Robert Wachtel '
          '(rwachtel@gmx.de) http://www.mpscologne.de'
          
            'Robert Wachtel hat uns diese Komponente im Rahmen dieses Projekt' +
            'es '
          'kostenlos zur Verf�gung gestellt. Danke !!!'
          ''
          
            'tSortGrid � 1996,1997,1999  Author: Bill Menees (bmenees@usit.ne' +
            't) '
          'http://mywebpages.comcast.net/bmenees/'
          ''
          'TVolgaDBEdit Copyright � 2000, Olga Vlasova, Russia '
          '(volgatable@chat.ru) http://volgatable.tripod.com'
          ''
          'TExDBGrid Version 2.4 � 1999 & 2000 by GJL Software '
          '(ExDBGrid@gjl-software.co.uk) http://www.gjl-software.co.uk'
          ''
          'ZeosDBO-Komponenten f�r den Zugriff auf den MySQL-Server '
          'http://sourceforge.net/projects/zeoslib/'
          
            'GNU General Public License (GPL), GNU Library or Lesser General ' +
            'Public '
          'License (LGPL)'
          ''
          'TZipMaster VCL by Chris Vleghert and Eric W. Engler'
          'e-mail: englere@abraxis.com'
          'www:  http://www.geocities.com/SiliconValley/Network/2114'
          'v1.70 by Russell Peters August 2, 2002.')
        ReadOnly = True
        ScrollBars = ssVertical
        TabOrder = 0
      end
    end
    object TabSheet3: TTabSheet
      Caption = 'Versionsinfo'
      ImageIndex = 2
      object SG1: TStringGrid
        Left = 0
        Top = 0
        Width = 476
        Height = 226
        Align = alClient
        ColCount = 2
        DefaultColWidth = 226
        DefaultRowHeight = 16
        FixedCols = 0
        RowCount = 22
        FixedRows = 0
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine]
        ParentFont = False
        TabOrder = 0
      end
    end
    object TabSheet4: TTabSheet
      Caption = 'Lizenzbedingungen'
      ImageIndex = 3
      object RichEdit1: TRichEdit
        Left = 0
        Top = 0
        Width = 476
        Height = 226
        Align = alClient
        Ctl3D = True
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        HideScrollBars = False
        Lines.Strings = (
          'Freie Warenwirtschaftssoftware CAO-Faktura f�r Windows'
          ''
          
            'Die Bezeichnung "freie" Software bezieht sich auf Freiheit, nich' +
            't auf den Preis. Unsere Lizenzen '
          
            'sollen Ihnen die Freiheit garantieren, Kopien freier Software zu' +
            ' verbreiten (und etwas f�r diesen '
          'Service zu berechnen, wenn Sie m�chten). '
          ''
          
            'Um uns (die Autoren) zu sch�tzen, wollen wir dar�berhinaus siche' +
            'rstellen, da� jeder erf�hrt, '
          'da� f�r diese freie Software keinerlei Garantie besteht. '
          ''
          
            'Paragraph 1. Sie d�rfen auf beliebigen Medien unver�nderte Kopie' +
            'n des '
          
            'Programms, wie sie es erhalten haben, anfertigen und verbreiten.' +
            ' Voraussetzung hierf�r ist, da� '
          
            'Sie mit jeder Kopie einen entsprechenden Copyright-Vermerk sowie' +
            ' einen Haftungsausschlu� '
          
            'ver�ffentlichen, alle Vermerke, die sich auf diese Lizenz und da' +
            's Fehlen einer Garantie '
          
            'beziehen, unver�ndert lassen und desweiteren allen anderen Empf�' +
            'ngern des Programms '
          
            'zusammen mit dem Programm eine Kopie dieser Lizenz zukommen lass' +
            'en. '
          ''
          
            'Sie d�rfen f�r den eigentlichen Kopiervorgang eine Geb�hr verlan' +
            'gen. Wenn Sie es w�nschen, '
          
            'd�rfen Sie auch gegen Entgeld eine Garantie f�r das Programm anb' +
            'ieten. '
          ''
          
            'Paragraph 2. Sie sind nicht verpflichtet, diese Lizenz anzunehme' +
            'n, da Sie sie nicht '
          
            'unterzeichnet haben. Jedoch gibt Ihnen nichts anderes die Erlaub' +
            'nis, das Programm zu '
          'benutzen '
          'oder zu verbreiten. Diese Handlungen sind gesetzlich '
          
            'verboten, wenn Sie diese Lizenz nicht anerkennen. Indem Sie das ' +
            'Programm verbreiten, '
          'erkl�ren Sie Ihr Einverst�ndnis mit dieser Lizenz '
          
            'und mit allen ihren Bedingungen bez�glich der Vervielf�ltigung u' +
            'nd Verbreitung. '
          ''
          
            'Paragraph 3. Sollten Ihnen infolge eines Gerichtsurteils, des Vo' +
            'rwurfs einer Patentverletzung '
          
            'oder aus einem anderen Grunde (nicht auf Patentfragen begrenzt) ' +
            'Bedingungen (durch '
          
            'Gerichtsbeschlu�, Vergleich oder anderweitig) auferlegt werden, ' +
            'die den Bedingungen dieser '
          
            'Lizenz widersprechen, so befreien Sie diese Umst�nde nicht von d' +
            'en Bestimmungen dieser '
          
            'Lizenz. Wenn es Ihnen nicht m�glich ist, das Programm unter glei' +
            'chzeitiger Beachtung der '
          
            'Bedingungen in dieser Lizenz und Ihrer anderweitigen Verpflichtu' +
            'ngen zu verbreiten, dann '
          
            'd�rfen Sie als Folge das Programm �berhaupt nicht verbreiten. We' +
            'nn zum Beispiel ein Patent '
          
            'nicht die geb�hrenfreie Weiterverbreitung des Programms durch di' +
            'ejenigen erlaubt, die das '
          
            'Programm direkt oder indirekt von Ihnen erhalten haben, dann bes' +
            'teht der einzige Weg, sowohl '
          
            'das Patentrecht als auch diese Lizenz zu befolgen, darin, ganz a' +
            'uf die Verbreitung des '
          'Programms zu verzichten. '
          ''
          
            'Sollte sich ein Teil dieses Paragraphen als ung�ltig oder unter ' +
            'bestimmten Umst�nden nicht '
          
            'durchsetzbar erweisen, so soll dieser Paragraph seinem Sinne nac' +
            'h angewandt werden; im '
          '�brigen soll dieser Paragraph als Ganzes gelten. '
          ''
          
            'Zweck dieses Paragraphen ist nicht, Sie dazu zu bringen, irgendw' +
            'elche Patente oder andere '
          
            'Eigentumsanspr�che zu verletzen oder die G�ltigkeit solcher Ansp' +
            'r�che zu bestreiten; dieser '
          
            'Paragraph hat einzig den Zweck, die Integrit�t des Verbreitungss' +
            'ystems der freien Software zu '
          
            'sch�tzen, das durch die Praxis �ffentlicher Lizenzen verwirklich' +
            't wird. Viele Leute haben '
          
            'gro�z�gige Beitr�ge zu dem gro�en Angebot der mit diesem System ' +
            'verbreiteten Software im '
          
            'Vertrauen auf die konsistente Anwendung dieses Systems geleistet' +
            '; es liegt am Autor/Geber, zu '
          
            'entscheiden, ob er die Software mittels irgendeines anderen Syst' +
            'ems verbreiten will; ein '
          'Lizenznehmer hat auf diese Entscheidung keinen Einflu�. '
          ''
          
            'Dieser Paragraph ist dazu gedacht, deutlich klarzustellen, was a' +
            'ls Konsequenz aus dem Rest '
          'dieser Lizenz betrachtet wird. '
          ''
          
            'Paragraph 4. Wenn die Verbreitung und/oder die Benutzung des Pro' +
            'gramms in bestimmten '
          
            'Staaten entweder durch Patente oder durch urheberrechtlich gesch' +
            '�tzte Schnittstellen '
          
            'eingeschr�nkt ist, kann der Urheberrechtsinhaber, der das Progra' +
            'mm unter diese Lizenz gestellt '
          
            'hat, eine explizite geographische Begrenzung der Verbreitung ang' +
            'eben, in der diese Staaten '
          
            'ausgeschlossen werden, so da� die Verbreitung nur innerhalb und ' +
            'zwischen den Staaten erlaubt '
          
            'ist, die nicht ausgeschlossen sind. In einem solchen Fall beinha' +
            'ltet diese Lizenz die '
          'Beschr�nkung, als w�re sie in diesem Text niedergeschrieben. '
          ''
          ''
          'Keine Gew�hrleistung'
          ''
          
            'Paragraph 5. Da das Programm ohne jegliche Kosten lizenziert wir' +
            'd, besteht keinerlei '
          
            'Gew�hrleistung f�r das Programm, soweit dies gesetzlich zul�ssig' +
            ' ist. Sofern nicht anderweitig '
          
            'schriftlich best�tigt, stellen die Copyright-Inhaber und/oder Dr' +
            'itte das Programm so zur '
          
            'Verf�gung, "wie es ist", ohne irgendeine Gew�hrleistung, weder a' +
            'usdr�cklich noch implizit, '
          
            'einschlie�lich - aber nicht begrenzt auf - Marktreife oder Verwe' +
            'ndbarkeit f�r einen bestimmten '
          
            'Zweck. Das volle Risiko bez�glich Qualit�t und Leistungsf�higkei' +
            't des Programms liegt bei '
          
            'Ihnen. Sollte sich das Programm als fehlerhaft herausstellen, li' +
            'egen die Kosten f�r notwendigen '
          'Service, Reparatur oder Korrektur bei Ihnen. '
          ''
          'Ende der Bedingungen')
        ParentCtl3D = False
        ParentFont = False
        ReadOnly = True
        ScrollBars = ssVertical
        TabOrder = 0
      end
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 254
    Width = 484
    Height = 34
    Align = alClient
    Alignment = taLeftJustify
    BevelOuter = bvNone
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    object OKButton: TBitBtn
      Left = 365
      Top = 3
      Width = 115
      Height = 26
      Hint = 'Klicken Sie hier, um das Info zu beenden'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnClick = OKButtonClick
      Kind = bkClose
      IsControl = True
    end
  end
end
