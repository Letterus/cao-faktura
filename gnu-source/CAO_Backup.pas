{

CAO-Faktura f�r Windows Version 1.0
Copyright (C) 2003 Jan Pokrandt / Jan@JP-SOFT.de

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.


******* CAO-Faktura comes with ABSOLUTELY NO WARRANTY ***************


Programm     : CAO-Faktura
Modul        : CAO_BACKUP
Stand        : 14.09.2003
Version      : 1.1.3.1
Beschreibung : Dialog zur Erstellung eines Backup-Scriptes (SQL)


History :

13.01.2003 - Version 1.1.1.6 released Jan Pokrandt
14.09.2003 - Beim einlesen der Tabellendaten werden jetzt jeweils max. 1000
             Datens�tze euf einmal angefordert (Speichernutzung verringert)

}

unit CAO_backup;

interface

{$I CAO32.INC}
{$O-}

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ComCtrls, ZipMstr, Buttons;

type
  TBackupForm = class(TForm)
    StartBackupBtn: TBitBtn;
    pb1: TProgressBar;
    pb2: TProgressBar;
    Zip: TZipMaster;
    Label1: TLabel;
    Label2: TLabel;
    TabNameLab: TLabel;
    PB3: TProgressBar;
    Label3: TLabel;
    ZipSizeLab: TLabel;
    CloseBtn: TBitBtn;
    Label4: TLabel;
    ZipFileName: TLabel;
    BackupPLZBLZ: TCheckBox;
    Label5: TLabel;
    ZipPfad: TLabel;
    procedure StartBackupBtnClick(Sender: TObject);
    procedure ZipProgress(Sender: TObject; ProgrType: ProgressType;
      Filename: String; FileSize: Integer);
    procedure ZipMessage(Sender: TObject; ErrCode: Integer;
      Message: String);
    procedure FormShow(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure CloseBtnClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private-Deklarationen }
    ZipError : Boolean;

    ZIPTotalSize1,
    ZIPTotalProgress1,
    ZIPTotalSize2,
    ZIPTotalProgress2 : Int64;

    Busy              : Boolean;
  public
    { Public-Deklarationen }
  end;

//var
//  BackupForm: TBackupForm;

implementation

uses CAO_DM, DB, ZExtra, FileCtrl, CAO_Logging;

{$R *.DFM}

//------------------------------------------------------------------------------
procedure TBackupForm.StartBackupBtnClick(Sender: TObject);
var s ,tab, lastkey, ks, d : string; i, row : integer;
     f : textfile; fn : string;
begin
   Busy :=True;
   screen.cursor :=crSqlWait;
   try
     fn :=dm1.BackupDir+dm1.AktMandant+'\';
     ForceDirectories (FN);

     fn :=fn+application.name+'_backup_'+
          formatdatetime ('yyyy_mm_dd_hh_mm',now)+'.sql';

     ZipFileName.Caption :=extractfilename(fn);
     ZipPfad.Caption     :=extractfilepath(fn);

     assignfile (f,fn);
     rewrite (f);
     closefile (f);
     append(f);

     pb3.Position :=0;
     StartBackupBtn.Enabled :=False;
     CloseBtn.Enabled       :=False;
     BackupPLZBLZ.Enabled   :=False;

     try

       s :='# '+Application.Title+#13#10+
           '# Datum :'+FormatDateTime ('dd.mm.yyyy hh:mm:ss "Uhr"',now)+#13#10+
           '# Mandant :'+DM1.AktMandant+#13#10+
           '#'+#13#10;

       dm1.uniquery.close;
       dm1.uniquery.sql.clear;
       dm1.uniquery.sql.text :='SHOW TABLE STATUS';
       dm1.uniquery.open;

       pb1.Max :=dm1.uniquery.recordcount+1;
       pb1.Position :=0;

       while not dm1.uniquery.eof do
       begin
            pb1.position :=dm1.uniquery.RecNo;

            tab :=uppercase(dm1.uniquery.fieldbyName('Name').AsString);

            TabNameLab.Caption :=Tab;

            Application.ProcessMessages;

            s :=s +
               '#'+#13#10+
               '# TABLE ['+Tab+']'+#13#10+
               '#'+#13#10+
               'drop table if exists '+ tab +';'#13#10#13#10;

            s :=s + 'create table '+ tab +' ('+#13#10;

            // Feldnamen
            dm1.UniQuery2.close;
            dm1.UniQuery2.sql.text :='show fields from '+ tab;
            dm1.UniQuery2.Open;

            while not dm1.UniQuery2.eof do
            begin
                 s :=s + '  '+
                     dm1.UniQuery2.FieldByName('Field').AsString+' '+
                     dm1.UniQuery2.FieldByName('Type').AsString;

                 if length(dm1.UniQuery2.FieldByName('Default').AsString)>0
                  then s :=s+' default '''+dm1.UniQuery2.FieldByName('Default').AsString+'''';

                 if dm1.UniQuery2.FieldByName('Null').AsString <>'YES'
                  then s :=s+' not null';

                 if length(dm1.UniQuery2.FieldByName('Extra').AsString)>0
                  then s :=s+' '+ dm1.UniQuery2.FieldByName('Extra').AsString;

                 if dm1.UniQuery2.RecNo < dm1.UniQuery2.RecordCount
                  then s :=s+','+#13#10;

                 dm1.UniQuery2.next;
            end;
            dm1.UniQuery2.Close;

            // Keys
            dm1.UniQuery2.close;
            dm1.UniQuery2.sql.text :='show keys from '+ tab;
            dm1.UniQuery2.Open;

            lastkey :=''; ks :='';
            while not dm1.UniQuery2.eof do
            begin
               if (lastkey<>dm1.UniQuery2.FieldByName('Key_name').AsString)then
               begin
                  if length(ks)>0 then s :=s+','+#13#10 + '  ' + ks +')';
                  ks :='';
                  lastkey :=dm1.UniQuery2.FieldByName('Key_name').AsString;
               end;

               if (lastkey='PRIMARY')and(ks='') then
               begin
                 ks :='PRIMARY KEY ('+dm1.UniQuery2.FieldByName('Column_name').AsString;
               end else
               if (dm1.UniQuery2.FieldByName('Non_unique').AsString='0')and(ks='') then
               begin
                  ks :='UNIQUE KEY '+lastkey+' ('+dm1.UniQuery2.FieldByName('Column_name').AsString;
               end else
               if ks='' then
               begin
                  ks :='KEY '+lastkey+' ('+dm1.UniQuery2.FieldByName('Column_name').AsString;
               end else

               begin
                  ks :=ks+', '+dm1.UniQuery2.FieldByName('Column_name').AsString;
               end;

               dm1.UniQuery2.next;
            end;
            dm1.UniQuery2.Close;

            // evt. letzten Key hinzuf�gen
            if length (ks)>0 then s :=s+','#13#10 + '  '+ ks + ')'+#13#10;

            s :=s+');'+#13#10#13#10;

            writeln (f,s);
            s :='';

            //Daten
            pb2.Max := dm1.UniQuery.FieldByName ('Rows').AsInteger+1;
            pb2.Position :=0;

            if (dm1.UniQuery.FieldByName ('Rows').AsInteger>0) and
               (
                (BackupPLZBLZ.Checked) or
                (
                 (not BackupPLZBLZ.Checked) and
                 (Tab<>'PLZ') and (Tab<>'BLZ') and (Tab<>'LAND')
                )
               ) then
            begin
              s :=s +
              '#'+#13#10+
              '# TABLE ['+Tab+'] DATA'+#13#10+
              '#'+#13#10;

              write (f,s); s :='';

              row :=-1;
              repeat
                inc(row);
                dm1.UniQuery2.close;
                dm1.UniQuery2.sql.text :='select * from '+ tab + ' limit '+ IntToStr(Row*1000)+',1000';
                dm1.UniQuery2.Open;

                while not dm1.UniQuery2.eof do
                begin
                     pb2.Position :=dm1.UniQuery2.RecNo+Row*1000;
                     Application.ProcessMessages;

                     d:='INSERT INTO ' + Tab + ' VALUES (';

                     for i:=0 to dm1.UniQuery2.FieldCount-1 do
                     begin
                         if i>0 then d:=d+',';
                         if dm1.UniQuery2.Fields[i].IsNull
                          then d :=d + 'NULL'
                          else
                         begin
                           if dm1.UniQuery2.Fields[i].DataType in
                            [ftSmallint, ftInteger, ftWord,
                             ftBoolean, ftBCD, ftBytes,
                             ftVarBytes, ftAutoInc, ftLargeint] then
                           begin
                             d :=d + ''''+dm1.UniQuery2.Fields[i].AsString+'''';
                           end
                              else
                           if dm1.UniQuery2.Fields[i].DataType in
                            [ftDate, ftTime, ftDateTime, ftFloat, ftCurrency] then
                           begin
                             case dm1.UniQuery2.Fields[i].DataType of
                               ftDate:     d :=d + ''''+FormatSqlDate(dm1.UniQuery2.Fields[i].AsDateTime)+'''';
                               ftTime:     d :=d + ''''+FormatSqlTime(dm1.UniQuery2.Fields[i].AsDateTime)+'''';
                               ftDateTime: d :=d + ''''+DateTimeToSqlDate(dm1.UniQuery2.Fields[i].AsDateTime)+'''';
                               ftFloat:    d :=d + ''''+FloatToStrEx(dm1.UniQuery2.Fields[i].AsFloat)+'''';
                               ftCurrency: d :=d + ''''+FloatToStrEx(dm1.UniQuery2.Fields[i].AsFloat)+'''';
                             end;
                           end
                              else
                           begin
                              d :=d + ''''+dm1.UniQuery2.StringToSql(dm1.UniQuery2.Fields[i].AsString)+'''';
                           end;
                         end;
                     end;

                   dm1.UniQuery2.next;

                   d:=d+');';
                   writeln (f,d);
                end;
              until DM1.UniQuery2.RecordCount=0;


              d:=#13#10#13#10;  writeln (f,d);
            end;


            dm1.UniQuery2.Close;
            dm1.uniquery.next;

            pb2.position :=0;
            Application.ProcessMessages;
       end;

       pb1.Position :=dm1.uniquery.RecNo+1;
       Application.ProcessMessages;
       dm1.uniquery.close;
     finally
       closefile (f);
     end;

     // Script packen (ZIP erstellen)
     pb1.position       :=0;
     TabNameLab.Caption :='';
     ZipError           :=False;

     try
       Zip.TempDir :=DM1.TmpDir;
       Zip.FSpecArgs.Clear;
       Zip.FSpecArgs.Add (FN);
       Zip.ZipFileName :=ChangeFileExt (FN,'.zip');
       Zip.Add;

       ZipSizeLab.Caption :=FormatFloat(',#0.0',Zip.ZipFileSize / 1024)+' kB';

       if not ZipError then
       begin
          AssignFile (F, ChangeFileExt (FN,'.sql'));
          Erase (F); // SQL-Datei l�schen
       end
          else
       begin
          MessageDlg
            ('W�hrend der Komprimierung ist ein Fehler aufgetreten,'+#13#10+
             'deshalb wurde die Sicherungsdatei :'+#13#10+
             ChangeFileExt (FN,'.sql')+#13#10+
             'nicht gel�scht, Sie m�ssen dies ggf. selbst tun.'+#13#10+
             'N�here Informationen zum Fehler finden Sie im Log.',
             mterror,[mbok],0);
       end;

     finally
       screen.cursor :=crDefault;
       StartBackupBtn.Enabled :=True;
       CloseBtn.Enabled :=True;
       BackupPLZBLZ.Enabled :=True;
     end;

     // Datum der le. Sicherung merken
     DM1.WriteString ('MAIN\SICHERUNG','LAST_BACKUP',
                      FormatDateTime ('dd.mm.yyyy',now));
   finally
     Busy :=False;
     screen.cursor :=crDefault;
     StartBackupBtn.Enabled :=True;
     CloseBtn.Enabled :=True;
   end;
end;
//------------------------------------------------------------------------------
procedure TBackupForm.ZipMessage(Sender: TObject; ErrCode: Integer;
  Message: String);

var p : integer;
begin
    try
       P :=Pos('ADDING:',Uppercase(Message));
       if P>0 then delete (Message,P,8); Insert('zu ZIP hinzuf�gen:',Message,P);

       if assigned(LogForm)
         then logform.addlog ('ZIP:ERR-CODE:'+Inttostr(ErrCode)+#13#10+
                              'MLD:'+Message+#13#10);

       if ErrCode <> 0 then ZipError :=True;
    except
    end;
end;
//------------------------------------------------------------------------------
procedure TBackupForm.FormShow(Sender: TObject);
begin
     Zip.DLLDirectory       :=ExtractFilePath(ParamStr(0));
     Busy                   :=False;
     StartBackupBtn.Enabled :=True;
     ZipSizeLab.Caption     :='';
     PB3.Position           :=0;
     ZipFileName.Caption    :='';
end;
//------------------------------------------------------------------------------
procedure TBackupForm.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
     CanClose := not Busy;
end;
//------------------------------------------------------------------------------
procedure TBackupForm.ZipProgress(Sender: TObject; ProgrType: ProgressType;
  Filename: String; FileSize: Integer);
var
   Step: Integer;
begin
   case ProgrType of
      TotalSize2Process:
         begin
            ZIPTotalSize2     := FileSize;
            ZIPTotalProgress2 := 0;
         end;
      NewFile:
         begin
            PB3.Position      := 1;  // Current position of bar.
            ZIPTotalSize1     := FileSize;
            ZIPTotalProgress1 := 0;
         end;
      ProgressUpdate:
         begin
            // FileSize gives now the bytes processed since the last call.
            ZIPTotalProgress1 := ZIPTotalProgress1 + FileSize;
            ZIPTotalProgress2 := ZIPTotalProgress2 + FileSize;
            if ZIPTotalSize1 <> 0 then
            begin
               Step := Integer( Int64(ZIPTotalProgress1) *
                                Int64(10000) div Int64(ZipTotalSize1)
                              );

               PB3.Position := 1 + Step;
            end else
               PB3.Position := 10001;
         end;
   end; // EOF Case
end;
//------------------------------------------------------------------------------
procedure TBackupForm.CloseBtnClick(Sender: TObject);
begin
     Close;
end;
//------------------------------------------------------------------------------
procedure TBackupForm.FormCreate(Sender: TObject);
begin
     if Screen.PixelsPerInch <> 96 then
     begin
       Self.ScaleBy (96, Screen.PixelsPerInch);
       Refresh;
     end;
end;
//------------------------------------------------------------------------------
end.


{

// Restore


procedure TDM1.RestoreMandant (ZipFileName : String);
var RPfad   : string;
    RP      : String;
    DSTab   : Array of Boolean;
    I       : Integer;
    SQL,
    SQL2, S : String;
begin
     rpfad :=dm1.maindir+'Restore\';

     rp :=readstring ('MAIN\SICHERUNG','RESTORE_PFAD','@@@');
     if rp = '@@@' then writestring ('MAIN\SICHERUNG','RESTORE_PFAD','')
                   else if rp <> '' then rpfad :=rp;



     //rpfad :='C:\BACKUP\';
     ForceDirectories (RPfad);

     ZipError :=False;

     zip.ZipFilename :=zipfilename;
     Zip.ExtrBaseDir :=RPfad;
     zip.ExtrOptions :=[ExtrOverWrite];

     if rpfad[length(rpfad)]<>'\' then rpfad :=rpfad+'\';

     try
        Zip.Extract;
     except
        ZipError :=True;

     end;

     if ZipError then
     begin
        MessageDlg ('Beim Entpacken der Datensicherung ist ein Fehler aufgetreten.'+#13#10+
                    'Aus Sicherheitsgr�nden wurde die R�cksicherung abgebrochen !',mterror,[mbok],0);
        Exit;
     end;

     //exit;

     // Datenbank schlie�en

     // offene Datasets merken
     setlength(DSTab,DB1.DatasetCount);
     for i:=0 to DB1.DatasetCount-1 do
     begin
          DSTab[i] :=tDataset(DB1.Datasets[i]).Active;
          tDataset(DB1.Datasets[i]).Close;
     end;

     // Tabellen umbenennen
     UniQuery.Close;
     UniQuery.Sql.Clear;
     UniQuery.Sql.Add ('FLUSH TABLES');
     UniQuery.ExecSql;

     //evt. alte Backups l�schen
     UniQuery.Close;
     UniQuery.Sql.Text :='show Tables from '+DB1.Database;
     UniQuery.Open;

     while not UniQuery.Eof do
     begin
        S :=UniQuery.FieldByName ('Tables_In_'+DB1.Database).AsString;
        if Pos('BACK_',UpperCase(S))=1 then
        begin
           if length(SQL)>0 then SQL :=SQL+',';
           SQL :=SQL+S;
        end;
        UniQuery.Next;
     end;

     // alte Backups l�schen
     if length(SQL)>0 then
     begin
       SQL :='DROP TABLE IF EXISTS '+SQL;
       UniQuery.Close;
       UniQuery.Sql.Text :=SQL;
       UniQuery.ExecSql;
     end;

     // Tabellen, die im Backup existieren, in BACK_.... unbenennen
     UniQuery.Close;
     UniQuery.Sql.Text :='show Tables from '+DB1.Database;
     UniQuery.Open;

     Sql :=''; SQL2 :='';

     while not UniQuery.Eof do
     begin
        S :=UniQuery.FieldByName ('Tables_In_'+DB1.Database).AsString;
        if (Pos('BACK_',Uppercase(S))=0)and(fileexists (rpfad+S+'.MYD')) then
        begin
           if length(SQL)>0 then SQL :=SQL+',';
           SQL :=SQL+S+' TO BACK_'+S;

           if length(SQL2)>0 then SQL2 :=SQL2+',';
           SQL2 :=SQL2+S;
        end;
        UniQuery.Next;
     end;

     if length(SQL)>0 then
     begin
        SQL :='RENAME TABLE '+SQL;
        UniQuery.Close;
        UniQuery.Sql.Text :=SQL;
        UniQuery.ExecSql;
     end;


     // MYSQL-Restore ausf�hren
     UniQuery.Close;
     UniQuery.Sql.Text :='RESTORE TABLE '+SQL2;

     while pos('\',rpfad)>0 do rpfad[pos('\',rpfad)] :='/';
     UniQuery.Sql.Add ('FROM "'+rpfad+'"');

     UniQuery.ExecSql;


     OpenMandant (AktMandant,Application.Name,True);

     // offene Datasets erneut �ffnen
     for i:=0 to DB1.DatasetCount-1 do
     begin
          if DSTab[i] then tDataset(DB1.Datasets[i]).Open;
     end;
     MessageDlg ('R�cksicherung abgeschlossen !!!',mtinformation,[mbok],0);
end;


}
