object ShopTransForm: TShopTransForm
  Left = 294
  Top = 207
  Width = 829
  Height = 523
  Caption = 'ShopTransfer'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Menu = MainMenu1
  OldCreateOrder = False
  Position = poDesktopCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnDeactivate = FormDeactivate
  PixelsPerInch = 96
  TextHeight = 13
  object MainPanel: TPanel
    Left = 0
    Top = 0
    Width = 821
    Height = 496
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object PC1: TPageControl
      Left = 0
      Top = 27
      Width = 821
      Height = 469
      ActivePage = ArtikelTS
      Align = alClient
      Style = tsFlatButtons
      TabOrder = 0
      OnChange = PC1Change
      object ArtikelTS: TTabSheet
        Caption = 'Artikel'
        object Splitter2: TSplitter
          Left = 0
          Top = 193
          Width = 813
          Height = 6
          Cursor = crVSplit
          Align = alTop
          Beveled = True
        end
        object Panel5: TPanel
          Left = 0
          Top = 0
          Width = 813
          Height = 22
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 0
          object Label5: TLabel
            Left = 4
            Top = 5
            Width = 242
            Height = 13
            Caption = 'Liste der Artikel (max 100 Artikel werden mitgeloggt)'
          end
          object Label6: TLabel
            Left = 363
            Top = 5
            Width = 43
            Height = 13
            Caption = 'Fehlerlog'
          end
        end
        object Panel6: TPanel
          Left = 0
          Top = 22
          Width = 813
          Height = 171
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 1
          object PLB1: TListBox
            Left = 0
            Top = 0
            Width = 355
            Height = 171
            Align = alLeft
            ItemHeight = 13
            TabOrder = 0
            TabWidth = 10
          end
          object PLOG: TListBox
            Left = 355
            Top = 0
            Width = 458
            Height = 171
            Align = alClient
            ItemHeight = 13
            TabOrder = 1
          end
        end
        object Panel7: TPanel
          Left = 0
          Top = 199
          Width = 813
          Height = 239
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 2
          object Panel8: TPanel
            Left = 0
            Top = 209
            Width = 813
            Height = 30
            Align = alBottom
            BevelOuter = bvNone
            TabOrder = 0
            object Label8: TLabel
              Left = 504
              Top = 8
              Width = 32
              Height = 13
              Caption = 'Label8'
            end
            object ImportArtikelBtn: TBitBtn
              Left = 88
              Top = 4
              Width = 105
              Height = 25
              Caption = 'Import'
              Enabled = False
              TabOrder = 0
              OnClick = ImportArtikelBtnClick
            end
            object ShopArtReadBtn: TButton
              Tag = 1
              Left = 2
              Top = 4
              Width = 75
              Height = 25
              Caption = 'Einlesen'
              TabOrder = 1
              OnClick = ShopBestReadBtnClick
            end
            object ImportAlleArtikelBtn: TBitBtn
              Left = 205
              Top = 4
              Width = 140
              Height = 25
              Caption = 'alle Artikel importieren'
              Enabled = False
              TabOrder = 2
              OnClick = ImportAlleArtikelBtnClick
            end
            object ArtRefreshBtn: TBitBtn
              Left = 352
              Top = 4
              Width = 133
              Height = 25
              Caption = 'Artikel aktualisieren'
              Enabled = False
              TabOrder = 3
              OnClick = ArtRefreshBtnClick
            end
          end
          object ExRxDBGrid1: TExRxDBGrid
            Left = 0
            Top = 0
            Width = 813
            Height = 209
            Align = alClient
            BorderStyle = bsNone
            DataSource = OscArt_DS
            Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgConfirmDelete, dgCancelOnExit]
            ReadOnly = True
            TabOrder = 1
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            TitleButtons = True
            Controls = <>
            ScrollBars = ssHorizontal
            EditColor = 10485663
            DefaultRowHeight = 30
            RowColor1 = 14680063
            RowColor2 = clWindow
            UseRowColors = True
            DrawFocusRect = False
            HighlightColor = clNavy
            ImageHighlightColor = clWindow
            HighlightFontColor = clWhite
            HotTrackColor = clNavy
            LockedCols = 0
            LockedFont.Charset = DEFAULT_CHARSET
            LockedFont.Color = clWindowText
            LockedFont.Height = -11
            LockedFont.Name = 'MS Sans Serif'
            LockedFont.Style = []
            LockedColor = clGray
            ShowTitleEllipsis = True
            ExMenuOptions = [exAutoSize, exAutoWidth, exDisplayBoolean, exDisplayImages, exDisplayMemo, exDisplayDateTime, exShowTextEllipsis, exShowTitleEllipsis, exFullSizeMemo, exAllowRowSizing, exCellHints, exMultiLineTitles, exUseRowColors, exFixedColumns, exPrintGrid, exPrintDataSet, exExportGrid, exSelectAll, exUnSelectAll, exQueryByForm, exSortByForm, exMemoInplaceEditors, exCustomize, exSearchMode, exSaveLayout, exLoadLayout]
            MaskedColumnDrag = True
            ShowTitlesWhenInactive = True
            ValueChecked = 1
            ValueUnChecked = 0
            Columns = <
              item
                Expanded = False
                FieldName = 'ID'
                Width = 35
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Importiert'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ArtikelExists'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Modell'
                Width = 80
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Menge'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Preis'
                Visible = True
              end
              item
                Alignment = taCenter
                Expanded = False
                FieldName = 'Status'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Bild'
                Width = 80
                Visible = True
              end
              item
                Alignment = taCenter
                Expanded = False
                FieldName = 'MwStID'
                Width = 25
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'TextDE'
                Width = 385
                Visible = True
              end>
          end
        end
      end
      object KatTS: TTabSheet
        Caption = 'Katalog'
        ImageIndex = 3
        object Splitter3: TSplitter
          Left = 0
          Top = 193
          Width = 813
          Height = 6
          Cursor = crVSplit
          Align = alTop
          Beveled = True
        end
        object Panel10: TPanel
          Left = 0
          Top = 0
          Width = 813
          Height = 22
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 0
          object Label12: TLabel
            Left = 4
            Top = 5
            Width = 117
            Height = 13
            Caption = 'Liste der Katalogeintr�ge'
          end
          object Label13: TLabel
            Left = 363
            Top = 5
            Width = 43
            Height = 13
            Caption = 'Fehlerlog'
          end
        end
        object Panel11: TPanel
          Left = 0
          Top = 22
          Width = 813
          Height = 171
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 1
          object KLB1: TListBox
            Left = 0
            Top = 0
            Width = 355
            Height = 171
            Align = alLeft
            ItemHeight = 13
            TabOrder = 0
            TabWidth = 10
          end
          object KLog: TListBox
            Left = 355
            Top = 0
            Width = 458
            Height = 171
            Align = alClient
            ItemHeight = 13
            TabOrder = 1
          end
        end
        object Panel12: TPanel
          Left = 0
          Top = 408
          Width = 813
          Height = 30
          Align = alBottom
          BevelOuter = bvNone
          TabOrder = 2
          object ImportKatBtn: TBitBtn
            Left = 88
            Top = 4
            Width = 105
            Height = 25
            Caption = 'Import'
            Enabled = False
            TabOrder = 0
            OnClick = ImportKatBtnClick
          end
          object ShopKatReadBtn: TButton
            Tag = 3
            Left = 2
            Top = 4
            Width = 75
            Height = 25
            Caption = 'Einlesen'
            TabOrder = 1
            OnClick = ShopBestReadBtnClick
          end
          object KatImportAlleBtn: TBitBtn
            Left = 205
            Top = 4
            Width = 140
            Height = 25
            Caption = 'alle Kategorien importieren'
            Enabled = False
            TabOrder = 2
            OnClick = KatImportAlleBtnClick
          end
        end
        object ExRxDBGrid2: TExRxDBGrid
          Left = 0
          Top = 199
          Width = 813
          Height = 209
          Align = alClient
          BorderStyle = bsNone
          DataSource = OscKat_DS
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgConfirmDelete, dgCancelOnExit]
          ReadOnly = True
          TabOrder = 3
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          TitleButtons = True
          Controls = <>
          ScrollBars = ssHorizontal
          EditColor = 10485663
          DefaultRowHeight = 16
          RowColor1 = 14680063
          RowColor2 = clWindow
          UseRowColors = True
          DrawFocusRect = False
          HighlightColor = clNavy
          ImageHighlightColor = clWindow
          HighlightFontColor = clWhite
          HotTrackColor = clNavy
          LockedCols = 0
          LockedFont.Charset = DEFAULT_CHARSET
          LockedFont.Color = clWindowText
          LockedFont.Height = -11
          LockedFont.Name = 'MS Sans Serif'
          LockedFont.Style = []
          LockedColor = clGray
          ShowTextEllipsis = True
          ShowTitleEllipsis = True
          ExMenuOptions = [exAutoSize, exAutoWidth, exDisplayBoolean, exDisplayImages, exDisplayMemo, exDisplayDateTime, exShowTextEllipsis, exShowTitleEllipsis, exFullSizeMemo, exAllowRowSizing, exCellHints, exMultiLineTitles, exUseRowColors, exFixedColumns, exPrintGrid, exPrintDataSet, exExportGrid, exSelectAll, exUnSelectAll, exQueryByForm, exSortByForm, exMemoInplaceEditors, exCustomize, exSearchMode, exSaveLayout, exLoadLayout]
          MaskedColumnDrag = True
          ShowTitlesWhenInactive = True
          ValueChecked = 1
          ValueUnChecked = 0
          Columns = <
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'ID'
              Width = 50
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'PID'
              Width = 50
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'Importiert'
              Width = 50
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'Exists'
              Width = 45
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Name'
              Width = 160
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ImageURL'
              Width = 160
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SortOrder'
              Width = 55
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DateAdded'
              Width = 100
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'LastModified'
              Width = 100
              Visible = True
            end>
        end
      end
      object HerstellerTS: TTabSheet
        Caption = 'Hersteller'
        ImageIndex = 4
        object Splitter4: TSplitter
          Left = 0
          Top = 193
          Width = 813
          Height = 6
          Cursor = crVSplit
          Align = alTop
          Beveled = True
        end
        object Panel13: TPanel
          Left = 0
          Top = 0
          Width = 813
          Height = 22
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 0
          object Label1: TLabel
            Left = 4
            Top = 5
            Width = 87
            Height = 13
            Caption = 'Liste der Hersteller'
          end
          object Label4: TLabel
            Left = 363
            Top = 5
            Width = 43
            Height = 13
            Caption = 'Fehlerlog'
          end
        end
        object Panel14: TPanel
          Left = 0
          Top = 22
          Width = 813
          Height = 171
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 1
          object Hlb1: TListBox
            Left = 0
            Top = 0
            Width = 355
            Height = 171
            Align = alLeft
            ItemHeight = 13
            TabOrder = 0
            TabWidth = 10
          end
          object HLog: TListBox
            Left = 355
            Top = 0
            Width = 458
            Height = 171
            Align = alClient
            ItemHeight = 13
            TabOrder = 1
          end
        end
        object Panel15: TPanel
          Left = 0
          Top = 199
          Width = 813
          Height = 239
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 2
          object Panel16: TPanel
            Left = 0
            Top = 209
            Width = 813
            Height = 30
            Align = alBottom
            BevelOuter = bvNone
            TabOrder = 0
            object ImportHerstellerBtn: TBitBtn
              Left = 88
              Top = 4
              Width = 105
              Height = 25
              Caption = 'Import'
              Enabled = False
              TabOrder = 0
              OnClick = ImportHerstellerBtnClick
            end
            object ShopHerReadBtn: TButton
              Tag = 4
              Left = 2
              Top = 4
              Width = 75
              Height = 25
              Caption = 'Einlesen'
              TabOrder = 1
              OnClick = ShopBestReadBtnClick
            end
            object ImportAlleHerstellerBtn: TBitBtn
              Left = 205
              Top = 4
              Width = 140
              Height = 25
              Caption = 'alle Hersteller importieren'
              Enabled = False
              TabOrder = 2
              OnClick = ImportAlleHerstellerBtnClick
            end
          end
          object ExRxDBGridHersteller: TExRxDBGrid
            Left = 0
            Top = 0
            Width = 813
            Height = 209
            Align = alClient
            BorderStyle = bsNone
            DataSource = OscHer_DS
            Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgConfirmDelete, dgCancelOnExit]
            ReadOnly = True
            TabOrder = 1
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            TitleButtons = True
            Controls = <>
            ScrollBars = ssHorizontal
            EditColor = 10485663
            DefaultRowHeight = 18
            RowColor1 = 14680063
            RowColor2 = clWindow
            UseRowColors = True
            DrawFocusRect = False
            HighlightColor = clNavy
            ImageHighlightColor = clWindow
            HighlightFontColor = clWhite
            HotTrackColor = clNavy
            LockedCols = 0
            LockedFont.Charset = DEFAULT_CHARSET
            LockedFont.Color = clWindowText
            LockedFont.Height = -11
            LockedFont.Name = 'MS Sans Serif'
            LockedFont.Style = []
            LockedColor = clGray
            ShowTitleEllipsis = True
            ExMenuOptions = [exAutoSize, exAutoWidth, exDisplayBoolean, exDisplayImages, exDisplayMemo, exDisplayDateTime, exShowTextEllipsis, exShowTitleEllipsis, exFullSizeMemo, exAllowRowSizing, exCellHints, exMultiLineTitles, exUseRowColors, exFixedColumns, exPrintGrid, exPrintDataSet, exExportGrid, exSelectAll, exUnSelectAll, exQueryByForm, exSortByForm, exMemoInplaceEditors, exCustomize, exSearchMode, exSaveLayout, exLoadLayout]
            MaskedColumnDrag = True
            ShowTitlesWhenInactive = True
            ValueChecked = 1
            ValueUnChecked = 0
            Columns = <
              item
                Expanded = False
                FieldName = 'ID'
                Width = 40
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Importiert'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Exists'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'HERSTELLER_NAME'
                Title.Caption = 'Name'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'HERSTELLER_IMAGE'
                Title.Caption = 'Bild-URL'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'HERSTELLER_URL'
                Width = 200
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DateAdded'
                Title.Caption = 'erstellt'
                Width = 97
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'LastModified'
                Title.Caption = 'le. �nderung'
                Width = 97
                Visible = True
              end>
          end
        end
      end
      object BestellTS: TTabSheet
        Caption = 'Bestellungen'
        ImageIndex = 1
        object Splitter1: TSplitter
          Left = 0
          Top = 193
          Width = 813
          Height = 6
          Cursor = crVSplit
          Align = alTop
          Beveled = True
        end
        object Panel1: TPanel
          Left = 0
          Top = 0
          Width = 813
          Height = 22
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 0
          object Label2: TLabel
            Left = 4
            Top = 5
            Width = 104
            Height = 13
            Caption = 'Liste der Bestellungen'
          end
          object Label3: TLabel
            Left = 363
            Top = 5
            Width = 43
            Height = 13
            Caption = 'Fehlerlog'
          end
        end
        object Panel2: TPanel
          Left = 0
          Top = 22
          Width = 813
          Height = 171
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 1
          object Olb1: TListBox
            Left = 0
            Top = 0
            Width = 355
            Height = 171
            Align = alLeft
            ItemHeight = 13
            TabOrder = 0
            TabWidth = 10
          end
          object OLog: TListBox
            Left = 355
            Top = 0
            Width = 458
            Height = 171
            Align = alClient
            ItemHeight = 13
            TabOrder = 1
          end
        end
        object Panel3: TPanel
          Left = 0
          Top = 199
          Width = 813
          Height = 239
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 2
          object Panel4: TPanel
            Left = 0
            Top = 209
            Width = 813
            Height = 30
            Align = alBottom
            BevelOuter = bvNone
            TabOrder = 0
            object Label7: TLabel
              Left = 351
              Top = 9
              Width = 89
              Height = 13
              Caption = 'ab Bestellnummer :'
            end
            object ShopBestImpBtn: TBitBtn
              Left = 88
              Top = 4
              Width = 105
              Height = 25
              Caption = 'Import'
              Enabled = False
              TabOrder = 0
              OnClick = ShopBestImportBtnClick
            end
            object ShopBestReadBtn: TButton
              Tag = 2
              Left = 2
              Top = 4
              Width = 75
              Height = 25
              Caption = 'Einlesen'
              TabOrder = 1
              OnClick = ShopBestReadBtnClick
            end
            object FromOrderNum: TJvSpinEdit
              Left = 447
              Top = 6
              Width = 81
              Height = 21
              MaxValue = 9999999
              TabOrder = 2
            end
            object AlleBestImportBtn: TBitBtn
              Left = 205
              Top = 4
              Width = 140
              Height = 25
              Caption = 'alle Bestellungen import.'
              Enabled = False
              TabOrder = 3
              OnClick = AlleBestImportBtnClick
            end
          end
          object OscReGrid: TExRxDBGrid
            Left = 0
            Top = 0
            Width = 813
            Height = 209
            Align = alClient
            BorderStyle = bsNone
            DataSource = OscRe_DS
            Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgConfirmDelete, dgCancelOnExit]
            ReadOnly = True
            TabOrder = 1
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            TitleButtons = True
            Controls = <>
            ScrollBars = ssHorizontal
            EditColor = 10485663
            DefaultRowHeight = 16
            RowColor1 = 14680063
            RowColor2 = clWindow
            UseRowColors = True
            DrawFocusRect = False
            HighlightColor = clNavy
            ImageHighlightColor = clWindow
            HighlightFontColor = clWhite
            HotTrackColor = clNavy
            LockedCols = 0
            LockedFont.Charset = DEFAULT_CHARSET
            LockedFont.Color = clWindowText
            LockedFont.Height = -11
            LockedFont.Name = 'MS Sans Serif'
            LockedFont.Style = []
            LockedColor = clGray
            ShowTextEllipsis = True
            ShowTitleEllipsis = True
            ExMenuOptions = [exAutoSize, exAutoWidth, exDisplayBoolean, exDisplayImages, exDisplayMemo, exDisplayDateTime, exShowTextEllipsis, exShowTitleEllipsis, exFullSizeMemo, exAllowRowSizing, exCellHints, exMultiLineTitles, exUseRowColors, exFixedColumns, exPrintGrid, exPrintDataSet, exExportGrid, exSelectAll, exUnSelectAll, exQueryByForm, exSortByForm, exMemoInplaceEditors, exCustomize, exSearchMode, exSaveLayout, exLoadLayout]
            MaskedColumnDrag = True
            ShowTitlesWhenInactive = True
            ValueChecked = 1
            ValueUnChecked = 0
            Columns = <
              item
                Expanded = False
                FieldName = 'Importiert'
                Width = 30
                Visible = True
              end
              item
                Alignment = taCenter
                Expanded = False
                FieldName = 'ID'
                Visible = True
              end
              item
                Alignment = taCenter
                Expanded = False
                FieldName = 'Kun_ID'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'KundeExists'
                ReadOnly = False
                Width = 30
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Datum'
                Visible = True
              end
              item
                Alignment = taCenter
                Expanded = False
                FieldName = 'Waehrung'
                Width = 35
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Kurs'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Firma'
                Width = 100
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Name'
                Width = 100
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Strasse'
                Width = 100
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Land'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'PLZ'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Ort'
                Width = 100
                Visible = True
              end
              item
                Alignment = taCenter
                Expanded = False
                FieldName = 'AnzArtikel'
                Width = 35
                Visible = True
              end>
          end
        end
      end
      object UpdateTS: TTabSheet
        Caption = 'Update CAO->SHOP'
        ImageIndex = 3
        object Panel9: TPanel
          Left = 0
          Top = 408
          Width = 813
          Height = 30
          Align = alBottom
          BevelOuter = bvNone
          TabOrder = 0
          object JobsRefreshBtn: TButton
            Left = 2
            Top = 4
            Width = 75
            Height = 25
            Caption = 'Refresh'
            TabOrder = 0
            OnClick = JobsRefreshBtnClick
          end
          object ExecKatBtn: TButton
            Left = 88
            Top = 4
            Width = 112
            Height = 25
            Caption = 'Update Ausf�hren'
            TabOrder = 1
            OnClick = ExecKatBtnClick
          end
        end
        object SyncLV: TListView
          Left = 0
          Top = 0
          Width = 813
          Height = 408
          Align = alClient
          Columns = <
            item
              Caption = 'Bereich'
              Width = 110
            end
            item
              Alignment = taRightJustify
              Caption = 'CAO-ID'
              Width = 75
            end
            item
              Alignment = taRightJustify
              Caption = 'Shop-ID'
              Width = 60
            end
            item
              Alignment = taCenter
              Caption = 'Status'
              Width = 60
            end
            item
              Caption = 'Bemerkung'
              Width = 125
            end
            item
              Caption = 'Ergebnis/ Fehlercode'
              Width = 250
            end>
          TabOrder = 1
          ViewStyle = vsReport
        end
      end
    end
    object ArtPan: TPanel
      Left = 0
      Top = 0
      Width = 821
      Height = 27
      Align = alTop
      BevelOuter = bvNone
      BorderStyle = bsSingle
      Color = clBtnShadow
      TabOrder = 1
      object BestellungenBtn: TJvSpeedButton
        Tag = 2
        Left = 528
        Top = 0
        Width = 85
        Height = 22
        Caption = 'Bestellungen'
        Flat = True
        Visible = False
        HotTrackFont.Charset = DEFAULT_CHARSET
        HotTrackFont.Color = clWindowText
        HotTrackFont.Height = -11
        HotTrackFont.Name = 'MS Sans Serif'
        HotTrackFont.Style = []
      end
      object KatalogBtn: TJvSpeedButton
        Tag = 1
        Left = 376
        Top = 0
        Width = 97
        Height = 22
        Caption = 'Kategorien'
        Flat = True
        Visible = False
        HotTrackFont.Charset = DEFAULT_CHARSET
        HotTrackFont.Color = clWindowText
        HotTrackFont.Height = -11
        HotTrackFont.Name = 'MS Sans Serif'
        HotTrackFont.Style = []
      end
      object PedHaendBtn: TJvSpeedButton
        Left = 264
        Top = 0
        Width = 72
        Height = 22
        Caption = 'Artikel'
        Flat = True
        Visible = False
        HotTrackFont.Charset = DEFAULT_CHARSET
        HotTrackFont.Color = clWindowText
        HotTrackFont.Height = -11
        HotTrackFont.Name = 'MS Sans Serif'
        HotTrackFont.Style = []
      end
      object Label35: TLabel
        Left = 0
        Top = 0
        Width = 124
        Height = 23
        Align = alLeft
        Caption = '  Shop-Transfer ...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        Layout = tlCenter
      end
    end
  end
  object IdHTTP1: TIdHTTP
    MaxLineAction = maException
    AllowCookies = True
    ProxyParams.BasicAuthentication = False
    ProxyParams.ProxyPort = 0
    Request.ContentLength = 0
    Request.ContentRangeEnd = 0
    Request.ContentRangeStart = 0
    Request.ContentType = 'text/html'
    Request.Accept = 'text/html, */*'
    Request.BasicAuthentication = False
    Request.UserAgent = 'CAO-Faktura'
    HTTPOptions = [hoForceEncodeParams]
    Left = 204
    Top = 112
  end
  object OscReTab: TJvMemoryData
    FieldDefs = <
      item
        Name = 'ID'
        DataType = ftInteger
      end
      item
        Name = 'Kun_ID'
        DataType = ftInteger
      end
      item
        Name = 'Datum'
        DataType = ftDate
      end
      item
        Name = 'Waehrung'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'Kurs'
        DataType = ftFloat
      end
      item
        Name = 'Firma'
        DataType = ftString
        Size = 100
      end
      item
        Name = 'Name'
        DataType = ftString
        Size = 100
      end
      item
        Name = 'Strasse'
        DataType = ftString
        Size = 100
      end
      item
        Name = 'PLZ'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'Ort'
        DataType = ftString
        Size = 100
      end
      item
        Name = 'Land'
        DataType = ftString
        Size = 100
      end
      item
        Name = 'AnzArtikel'
        DataType = ftInteger
      end
      item
        Name = 'Importiert'
        DataType = ftBoolean
      end
      item
        Name = 'KundeExists'
        DataType = ftBoolean
      end
      item
        Name = 'CaoKunID'
        DataType = ftInteger
      end>
    AfterOpen = OscReTabAfterScroll
    AfterScroll = OscReTabAfterScroll
    Left = 696
    Top = 325
    object OscReTabID: TIntegerField
      DisplayLabel = 'OrderID'
      DisplayWidth = 4
      FieldName = 'ID'
      DisplayFormat = '0000'
    end
    object OscReTabKun_ID: TIntegerField
      DisplayLabel = 'KunID'
      DisplayWidth = 4
      FieldName = 'Kun_ID'
      DisplayFormat = '0000'
    end
    object OscReTabDatum: TDateField
      FieldName = 'Datum'
      DisplayFormat = 'dd.mm.yyyy'
    end
    object OscReTabWaehrung: TStringField
      DisplayWidth = 3
      FieldName = 'Waehrung'
    end
    object OscReTabKurs: TFloatField
      FieldName = 'Kurs'
      DisplayFormat = ',#0.000000'
    end
    object OscReTabFirma: TStringField
      DisplayWidth = 20
      FieldName = 'Firma'
      Size = 100
    end
    object OscReTabName: TStringField
      DisplayWidth = 20
      FieldName = 'Name'
      Size = 100
    end
    object OscReTabStrasse: TStringField
      DisplayLabel = 'Stra�e'
      DisplayWidth = 20
      FieldName = 'Strasse'
      Size = 100
    end
    object OscReTabPLZ: TStringField
      DisplayWidth = 6
      FieldName = 'PLZ'
    end
    object OscReTabOrt: TStringField
      DisplayWidth = 20
      FieldName = 'Ort'
      Size = 100
    end
    object OscReTabLand: TStringField
      DisplayWidth = 8
      FieldName = 'Land'
      Size = 100
    end
    object OscReTabAnzArtikel: TIntegerField
      DisplayLabel = 'Positionen'
      FieldName = 'AnzArtikel'
      DisplayFormat = ',#0'
    end
    object OscReTabImportiert: TBooleanField
      FieldName = 'Importiert'
    end
    object OscReTabKundeExists: TBooleanField
      DisplayLabel = 'Kunde Importiert'
      FieldName = 'KundeExists'
    end
    object OscReTabCaoKunID: TIntegerField
      FieldName = 'CaoKunID'
    end
  end
  object OscRe_DS: TDataSource
    DataSet = OscReTab
    OnDataChange = OscRe_DSDataChange
    Left = 631
    Top = 325
  end
  object ReEdiTab: TZMySqlQuery
    Database = DM1.db1
    Transaction = DM1.Transact1
    CachedUpdates = False
    ShowRecordTypes = [ztModified, ztInserted, ztUnmodified]
    Options = [doHourGlass, doQueryAllRecords, doAutoFillDefs]
    LinkOptions = [loCascadeUpdate]
    Constraints = <>
    BeforePost = ReEdiTabBeforePost
    OnNewRecord = ReEdiTabNewRecord
    ExtraOptions = [moStoreResult]
    AlternativeView = True
    Macros = <>
    Sql.Strings = (
      'select * FROM JOURNAL'
      'where QUELLE=:QUELLE and QUELLE_SUB<>2'
      'order by VRENUM'
      'limit 0,1')
    RequestLive = True
    Left = 628
    Top = 116
    ParamData = <
      item
        DataType = ftString
        Name = 'QUELLE'
        ParamType = ptUnknown
        Value = '13'
      end>
    object ReEdiTabQUELLE: TIntegerField
      FieldName = 'QUELLE'
      Required = True
      Visible = False
    end
    object ReEdiTabJAHR: TIntegerField
      FieldName = 'JAHR'
      Required = True
      Visible = False
    end
    object ReEdiTabREC_ID: TIntegerField
      FieldName = 'REC_ID'
      Visible = False
    end
    object ReEdiTabQUELLE_SUB: TIntegerField
      FieldName = 'QUELLE_SUB'
      Visible = False
    end
    object ReEdiTabADDR_ID: TIntegerField
      FieldName = 'ADDR_ID'
      Required = True
      Visible = False
    end
    object ReEdiTabATRNUM: TIntegerField
      FieldName = 'ATRNUM'
      Visible = False
    end
    object ReEdiTabVRENUM: TIntegerField
      Alignment = taCenter
      DisplayLabel = 'int. Nr.'
      DisplayWidth = 5
      FieldName = 'VRENUM'
      Required = True
      DisplayFormat = '"EDI-"0000'
    end
    object ReEdiTabVLSNUM: TIntegerField
      FieldName = 'VLSNUM'
      Visible = False
    end
    object ReEdiTabFOLGENR: TIntegerField
      FieldName = 'FOLGENR'
      Visible = False
    end
    object ReEdiTabKM_STAND: TIntegerField
      FieldName = 'KM_STAND'
      Visible = False
      DisplayFormat = ',#0;-;-'
    end
    object ReEdiTabKFZ_ID: TIntegerField
      FieldName = 'KFZ_ID'
      Visible = False
    end
    object ReEdiTabVERTRETER_ID: TIntegerField
      FieldName = 'VERTRETER_ID'
      Visible = False
    end
    object ReEdiTabGLOBRABATT: TFloatField
      FieldName = 'GLOBRABATT'
      Visible = False
      DisplayFormat = '0.0%'
    end
    object ReEdiTabADATUM: TDateField
      FieldName = 'ADATUM'
      Visible = False
    end
    object ReEdiTabLDATUM: TDateField
      FieldName = 'LDATUM'
      Visible = False
    end
    object ReEdiTabWVORLAGE: TIntegerField
      DisplayLabel = 'WV'
      DisplayWidth = 2
      FieldName = 'WVORLAGE'
      Visible = False
      DisplayFormat = 'Ja;-;-'
    end
    object ReEdiTabWV_DATUM: TDateField
      FieldName = 'WV_DATUM'
      Visible = False
    end
    object ReEdiTabPR_EBENE: TIntegerField
      FieldName = 'PR_EBENE'
      Visible = False
    end
    object ReEdiTabKOST_NETTO: TFloatField
      FieldName = 'KOST_NETTO'
      Visible = False
    end
    object ReEdiTabWERT_NETTO: TFloatField
      FieldName = 'WERT_NETTO'
      Visible = False
    end
    object ReEdiTabLOHN: TFloatField
      FieldName = 'LOHN'
      Visible = False
    end
    object ReEdiTabWARE: TFloatField
      FieldName = 'WARE'
      Visible = False
    end
    object ReEdiTabTKOST: TFloatField
      FieldName = 'TKOST'
      Visible = False
    end
    object ReEdiTabMWST_0: TFloatField
      FieldName = 'MWST_0'
      Visible = False
    end
    object ReEdiTabMWST_1: TFloatField
      FieldName = 'MWST_1'
      Visible = False
    end
    object ReEdiTabMWST_2: TFloatField
      FieldName = 'MWST_2'
      Visible = False
    end
    object ReEdiTabMWST_3: TFloatField
      FieldName = 'MWST_3'
      Visible = False
    end
    object ReEdiTabMSUMME_0: TFloatField
      FieldName = 'MSUMME_0'
      Visible = False
      DisplayFormat = ',###,##0.00'
      currency = True
    end
    object ReEdiTabMSUMME_1: TFloatField
      FieldName = 'MSUMME_1'
      Visible = False
      DisplayFormat = ',###,##0.00'
      currency = True
    end
    object ReEdiTabMSUMME_2: TFloatField
      FieldName = 'MSUMME_2'
      Visible = False
      DisplayFormat = ',###,##0.00'
      currency = True
    end
    object ReEdiTabMSUMME_3: TFloatField
      FieldName = 'MSUMME_3'
      Visible = False
      DisplayFormat = ',###,##0.00'
      currency = True
    end
    object ReEdiTabMSUMME: TFloatField
      FieldName = 'MSUMME'
      Visible = False
      DisplayFormat = ',###,##0.00'
      currency = True
    end
    object ReEdiTabBSUMME: TFloatField
      DisplayLabel = 'Brutto'
      FieldName = 'BSUMME'
      Visible = False
      DisplayFormat = ',###,##0.00'
      currency = True
    end
    object ReEdiTabATSUMME: TFloatField
      FieldName = 'ATSUMME'
      Visible = False
      DisplayFormat = ',###,##0.00'
      currency = True
    end
    object ReEdiTabATMSUMME: TFloatField
      FieldName = 'ATMSUMME'
      Visible = False
      DisplayFormat = ',###,##0.00'
      currency = True
    end
    object ReEdiTabGEGENKONTO: TIntegerField
      FieldName = 'GEGENKONTO'
      Visible = False
    end
    object ReEdiTabSOLL_STAGE: TIntegerField
      FieldName = 'SOLL_STAGE'
      Visible = False
    end
    object ReEdiTabSOLL_SKONTO: TFloatField
      FieldName = 'SOLL_SKONTO'
      Visible = False
      DisplayFormat = '0.0%'
      EditFormat = '0.0'
    end
    object ReEdiTabSOLL_NTAGE: TIntegerField
      FieldName = 'SOLL_NTAGE'
      Visible = False
    end
    object ReEdiTabSOLL_RATEN: TIntegerField
      FieldName = 'SOLL_RATEN'
      Visible = False
    end
    object ReEdiTabSOLL_RATBETR: TFloatField
      FieldName = 'SOLL_RATBETR'
      Visible = False
    end
    object ReEdiTabSOLL_RATINTERVALL: TIntegerField
      FieldName = 'SOLL_RATINTERVALL'
      Visible = False
    end
    object ReEdiTabSTADIUM: TIntegerField
      FieldName = 'STADIUM'
      Visible = False
    end
    object ReEdiTabERSTELLT: TDateField
      FieldName = 'ERSTELLT'
      Visible = False
    end
    object ReEdiTabERST_NAME: TStringField
      FieldName = 'ERST_NAME'
      Visible = False
    end
    object ReEdiTabKUN_NUM: TStringField
      Alignment = taCenter
      FieldName = 'KUN_NUM'
      Visible = False
    end
    object ReEdiTabKUN_ANREDE: TStringField
      FieldName = 'KUN_ANREDE'
      Visible = False
      Size = 30
    end
    object ReEdiTabKUN_NAME1: TStringField
      DisplayLabel = 'Kunde'
      FieldName = 'KUN_NAME1'
      Size = 30
    end
    object ReEdiTabKUN_NAME2: TStringField
      FieldName = 'KUN_NAME2'
      Visible = False
      Size = 30
    end
    object ReEdiTabKUN_NAME3: TStringField
      FieldName = 'KUN_NAME3'
      Visible = False
      Size = 30
    end
    object ReEdiTabKUN_ABTEILUNG: TStringField
      FieldName = 'KUN_ABTEILUNG'
      Visible = False
      Size = 30
    end
    object ReEdiTabKUN_STRASSE: TStringField
      FieldName = 'KUN_STRASSE'
      Visible = False
      Size = 30
    end
    object ReEdiTabKUN_LAND: TStringField
      FieldName = 'KUN_LAND'
      Visible = False
      Size = 2
    end
    object ReEdiTabKUN_PLZ: TStringField
      FieldName = 'KUN_PLZ'
      Visible = False
      Size = 5
    end
    object ReEdiTabKUN_ORT: TStringField
      FieldName = 'KUN_ORT'
      Visible = False
      Size = 30
    end
    object ReEdiTabUSR1: TStringField
      FieldName = 'USR1'
      Visible = False
      Size = 80
    end
    object ReEdiTabUSR2: TStringField
      FieldName = 'USR2'
      Visible = False
      Size = 80
    end
    object ReEdiTabPROJEKT: TStringField
      DisplayLabel = 'Projekt / Beschreibung'
      FieldName = 'PROJEKT'
      Size = 40
    end
    object ReEdiTabORGNUM: TStringField
      FieldName = 'ORGNUM'
      Visible = False
    end
    object ReEdiTabBEST_NAME: TStringField
      FieldName = 'BEST_NAME'
      Visible = False
      Size = 40
    end
    object ReEdiTabBEST_CODE: TIntegerField
      FieldName = 'BEST_CODE'
      Visible = False
    end
    object ReEdiTabBEST_DATUM: TDateField
      FieldName = 'BEST_DATUM'
    end
    object ReEdiTabINFO: TBlobField
      FieldName = 'INFO'
      Visible = False
      BlobType = ftBlob
    end
    object ReEdiTabLIEFART: TIntegerField
      FieldName = 'LIEFART'
      Visible = False
    end
    object ReEdiTabZAHLART: TIntegerField
      FieldName = 'ZAHLART'
      Visible = False
    end
    object ReEdiTabNSUMME: TFloatField
      DisplayLabel = 'Gesamt (netto)'
      DisplayWidth = 9
      FieldName = 'NSUMME'
      DisplayFormat = ',###,##0.00'
      currency = True
    end
    object ReEdiTabWAEHRUNG: TStringField
      DisplayLabel = 'WA'
      DisplayWidth = 3
      FieldName = 'WAEHRUNG'
      Size = 5
    end
    object ReEdiTabRDATUM: TDateField
      DisplayLabel = 'le.�nderung'
      FieldName = 'RDATUM'
      Required = True
      DisplayFormat = 'dd.mm.yyyy'
    end
    object ReEdiTabWV_DatumStr: TStringField
      DisplayLabel = 'Termin'
      FieldKind = fkCalculated
      FieldName = 'WV_Datum-Str'
      Size = 15
      Calculated = True
    end
    object ReEdiTabSHOP_ID: TIntegerField
      FieldName = 'SHOP_ID'
    end
    object ReEdiTabSHOP_ORDERID: TIntegerField
      FieldName = 'SHOP_ORDERID'
    end
    object ReEdiTabSHOP_STATUS: TIntegerField
      FieldName = 'SHOP_STATUS'
    end
    object ReEdiTabBRUTTO_FLAG: TBooleanField
      FieldName = 'BRUTTO_FLAG'
      Required = True
    end
    object ReEdiTabMWST_FREI_FLAG: TBooleanField
      FieldName = 'MWST_FREI_FLAG'
      Required = True
    end
    object ReEdiTabLIEF_ADDR_ID: TIntegerField
      FieldName = 'LIEF_ADDR_ID'
    end
  end
  object PosTab: TZMySqlQuery
    Database = DM1.db1
    Transaction = DM1.Transact1
    CachedUpdates = False
    ShowRecordTypes = [ztModified, ztInserted, ztUnmodified]
    Options = [doHourGlass, doQueryAllRecords, doAutoFillDefs, doQuickOpen]
    LinkOptions = [loCascadeUpdate]
    Constraints = <>
    BeforeEdit = PosTabBeforeEdit
    BeforePost = PosTabBeforePost
    AfterPost = PosTabAfterPost
    OnCalcFields = PosTabCalcFields
    OnNewRecord = PosTabNewRecord
    ExtraOptions = [moStoreResult]
    AlternativeView = True
    Macros = <>
    Sql.Strings = (
      'select * from JOURNALPOS'
      'where JOURNAL_ID=:ID')
    RequestLive = True
    Left = 683
    Top = 116
    ParamData = <
      item
        DataType = ftInteger
        Name = 'ID'
        ParamType = ptInput
        Value = '8488'
      end>
    object PosTabREC_ID: TIntegerField
      FieldName = 'REC_ID'
      Visible = False
    end
    object PosTabQUELLE: TIntegerField
      FieldName = 'QUELLE'
      Visible = False
    end
    object PosTabQUELLE_SUB: TIntegerField
      FieldName = 'QUELLE_SUB'
      Visible = False
    end
    object PosTabJAHR: TIntegerField
      FieldName = 'JAHR'
      Required = True
      Visible = False
    end
    object PosTabJOURNAL_ID: TIntegerField
      FieldName = 'JOURNAL_ID'
      Required = True
      Visible = False
    end
    object PosTabPOSITION: TIntegerField
      Alignment = taCenter
      DisplayLabel = 'Pos.'
      DisplayWidth = 3
      FieldName = 'POSITION'
      Required = True
      DisplayFormat = '000'
    end
    object PosTabARTIKELTYP: TStringField
      Alignment = taCenter
      DisplayLabel = 'T'
      DisplayWidth = 2
      FieldName = 'ARTIKELTYP'
      Size = 1
    end
    object PosTabARTIKEL_ID: TIntegerField
      FieldName = 'ARTIKEL_ID'
      Required = True
      Visible = False
    end
    object PosTabADDR_ID: TIntegerField
      FieldName = 'ADDR_ID'
      Required = True
      Visible = False
    end
    object PosTabATRNUM: TIntegerField
      FieldName = 'ATRNUM'
      Visible = False
    end
    object PosTabVRENUM: TIntegerField
      FieldName = 'VRENUM'
      Visible = False
    end
    object PosTabVLSNUM: TIntegerField
      DisplayLabel = 'Lief.-Nr.'
      FieldName = 'VLSNUM'
      Visible = False
      DisplayFormat = '0;-;-'
    end
    object PosTabMATCHCODE: TStringField
      FieldName = 'MATCHCODE'
      Visible = False
    end
    object PosTabARTNUM: TStringField
      FieldName = 'ARTNUM'
      Visible = False
    end
    object PosTabBARCODE: TStringField
      FieldName = 'BARCODE'
      Visible = False
    end
    object PosTabBEZEICHNUNG: TMemoField
      DisplayLabel = 'Artikelbezeichnung'
      DisplayWidth = 70
      FieldName = 'BEZEICHNUNG'
      BlobType = ftMemo
    end
    object PosTabLAENGE: TStringField
      FieldName = 'LAENGE'
      Visible = False
    end
    object PosTabGROESSE: TStringField
      FieldName = 'GROESSE'
      Visible = False
    end
    object PosTabDIMENSION: TStringField
      FieldName = 'DIMENSION'
      Visible = False
    end
    object PosTabGEWICHT: TFloatField
      FieldName = 'GEWICHT'
      Visible = False
    end
    object PosTabME_EINHEIT: TStringField
      DisplayLabel = 'M.-Einheit'
      FieldName = 'ME_EINHEIT'
      Visible = False
      Size = 10
    end
    object PosTabPR_EINHEIT: TFloatField
      FieldName = 'PR_EINHEIT'
      Visible = False
    end
    object PosTabMENGE: TFloatField
      DisplayLabel = 'Menge'
      DisplayWidth = 6
      FieldName = 'MENGE'
      DisplayFormat = ',###,##0.0;-,###,##0.0; '
    end
    object PosTabEPREIS: TFloatField
      DisplayLabel = 'E-Preis'
      DisplayWidth = 8
      FieldName = 'EPREIS'
      DisplayFormat = ',###,##0.00;-,###,##0.00; '
    end
    object PosTabRABATT: TFloatField
      DisplayLabel = 'Rabatt'
      DisplayWidth = 5
      FieldName = 'RABATT'
      DisplayFormat = ',###,##0.0"%";-,###,##0.0"%"; '
    end
    object PosTabE_RGEWINN: TFloatField
      FieldName = 'E_RGEWINN'
      Visible = False
      DisplayFormat = ',###,##0.00;-,###,##0.00; '
    end
    object PosTabALTTEIL_STCODE: TIntegerField
      FieldName = 'ALTTEIL_STCODE'
      Visible = False
    end
    object PosTabGEGENKTO: TIntegerField
      FieldName = 'GEGENKTO'
      Visible = False
    end
    object PosTabNSumme: TCurrencyField
      DisplayLabel = 'G-Preis'
      DisplayWidth = 9
      FieldKind = fkCalculated
      FieldName = 'NSumme'
      DisplayFormat = ',###,##0.00;-,###,##0.00; '
      Calculated = True
    end
    object PosTabMSumme: TCurrencyField
      FieldKind = fkCalculated
      FieldName = 'MSumme'
      Visible = False
      DisplayFormat = ',###,##0.00;-,###,##0.00; '
      Calculated = True
    end
    object PosTabBSumme: TCurrencyField
      FieldKind = fkCalculated
      FieldName = 'BSumme'
      Visible = False
      DisplayFormat = ',###,##0.00;-,###,##0.00; '
      Calculated = True
    end
    object PosTabSTEUER_CODE: TIntegerField
      Alignment = taCenter
      DisplayLabel = 'S'
      DisplayWidth = 2
      FieldName = 'STEUER_CODE'
      DisplayFormat = '0;-;-'
    end
    object PosTabMwSt: TStringField
      DisplayWidth = 5
      FieldKind = fkCalculated
      FieldName = 'MwSt'
      Size = 8
      Calculated = True
    end
    object PosTabALTTEIL_PROZ: TFloatField
      FieldName = 'ALTTEIL_PROZ'
      Visible = False
    end
    object PosTabGEBUCHT: TBooleanField
      FieldName = 'GEBUCHT'
      Required = True
    end
    object PosTabALTTEIL_FLAG: TBooleanField
      FieldName = 'ALTTEIL_FLAG'
      Required = True
    end
    object PosTabBEZ_FEST_FLAG: TBooleanField
      FieldName = 'BEZ_FEST_FLAG'
      Required = True
    end
    object PosTabBRUTTO_FLAG: TBooleanField
      FieldName = 'BRUTTO_FLAG'
      Required = True
    end
    object PosTabPROVIS_PROZ: TFloatField
      FieldName = 'PROVIS_PROZ'
      Required = True
    end
    object PosTabPROVIS_WERT: TFloatField
      FieldName = 'PROVIS_WERT'
      Required = True
    end
    object PosTabSN_FLAG: TBooleanField
      FieldName = 'SN_FLAG'
      Required = True
    end
    object PosTabVPE: TIntegerField
      FieldName = 'VPE'
    end
  end
  object KunTab: TZMySqlQuery
    Database = DM1.db1
    Transaction = DM1.Transact1
    CachedUpdates = False
    ShowRecordTypes = [ztModified, ztInserted, ztUnmodified]
    Options = [doHourGlass, doAutoFillDefs, doUseRowId]
    LinkOptions = [loCascadeUpdate]
    Constraints = <>
    ExtraOptions = [moStoreResult]
    AlternativeView = True
    Macros = <>
    Sql.Strings = (
      'select * from ADRESSEN'
      'where REC_ID = :ID'
      'limit 0,1')
    RequestLive = True
    Left = 630
    Top = 165
    ParamData = <
      item
        DataType = ftInteger
        Name = 'ID'
        ParamType = ptInput
      end>
    object KunTabREC_ID: TIntegerField
      FieldName = 'REC_ID'
    end
    object KunTabMATCHCODE: TStringField
      FieldName = 'MATCHCODE'
    end
    object KunTabNAME1: TStringField
      FieldName = 'NAME1'
      Size = 40
    end
    object KunTabPLZ: TStringField
      FieldName = 'PLZ'
      Size = 10
    end
    object KunTabORT: TStringField
      FieldName = 'ORT'
      Size = 40
    end
    object KunTabLAND: TStringField
      FieldName = 'LAND'
      Size = 5
    end
    object KunTabNAME2: TStringField
      FieldName = 'NAME2'
      Size = 40
    end
    object KunTabNAME3: TStringField
      FieldName = 'NAME3'
      Size = 40
    end
    object KunTabABTEILUNG: TStringField
      FieldName = 'ABTEILUNG'
      Size = 40
    end
    object KunTabANREDE: TStringField
      FieldName = 'ANREDE'
      Size = 40
    end
    object KunTabSTRASSE: TStringField
      FieldName = 'STRASSE'
      Size = 40
    end
    object KunTabDEB_NUM: TIntegerField
      FieldName = 'DEB_NUM'
    end
    object KunTabWAERUNG: TStringField
      FieldName = 'WAERUNG'
      Size = 5
    end
    object KunTabGRABATT: TFloatField
      FieldName = 'GRABATT'
    end
    object KunTabKUN_LIEFART: TIntegerField
      FieldName = 'KUN_LIEFART'
    end
    object KunTabKUN_ZAHLART: TIntegerField
      FieldName = 'KUN_ZAHLART'
    end
    object KunTabPR_EBENE: TIntegerField
      FieldName = 'PR_EBENE'
    end
    object KunTabNET_SKONTO: TFloatField
      FieldName = 'NET_SKONTO'
    end
    object KunTabNET_TAGE: TIntegerField
      FieldName = 'NET_TAGE'
    end
    object KunTabBRT_TAGE: TIntegerField
      FieldName = 'BRT_TAGE'
    end
    object KunTabTELE1: TStringField
      FieldName = 'TELE1'
      Size = 100
    end
    object KunTabFAX: TStringField
      FieldName = 'FAX'
      Size = 100
    end
    object KunTabEMAIL: TStringField
      FieldName = 'EMAIL'
      Size = 100
    end
    object KunTabKUN_GEBDATUM: TDateField
      FieldName = 'KUN_GEBDATUM'
    end
    object KunTabBRUTTO_FLAG: TBooleanField
      FieldName = 'BRUTTO_FLAG'
      Required = True
    end
    object KunTabMWST_FREI_FLAG: TBooleanField
      FieldName = 'MWST_FREI_FLAG'
      Required = True
    end
    object KunTabSHOP_ID: TIntegerField
      FieldName = 'SHOP_ID'
    end
    object KunTabSHOP_KUNDE_ID: TIntegerField
      FieldName = 'SHOP_KUNDE_ID'
    end
    object KunTabKUNDENGRUPPE: TIntegerField
      FieldName = 'KUNDENGRUPPE'
    end
    object KunTabKUNNUM1: TStringField
      FieldName = 'KUNNUM1'
    end
    object KunTabBLZ: TStringField
      FieldName = 'BLZ'
    end
    object KunTabKTO: TStringField
      FieldName = 'KTO'
    end
    object KunTabBANK: TStringField
      FieldName = 'BANK'
      Size = 40
    end
    object KunTabKUN_SEIT: TDateField
      FieldName = 'KUN_SEIT'
    end
    object KunTabSTATUS: TIntegerField
      FieldName = 'STATUS'
    end
  end
  object OscArtTab: TJvMemoryData
    FieldDefs = <>
    AfterOpen = OscArtTabAfterScroll
    AfterScroll = OscArtTabAfterScroll
    Left = 696
    Top = 278
    object OscArtTabID: TIntegerField
      DisplayLabel = 'ArtID'
      DisplayWidth = 4
      FieldName = 'ID'
      DisplayFormat = '0000'
    end
    object OscArtTabImportiert: TBooleanField
      FieldName = 'Importiert'
    end
    object OscArtTabArtikelExists: TBooleanField
      DisplayLabel = 'Existiert'
      FieldName = 'ArtikelExists'
    end
    object OscArtTabCaoArtID: TIntegerField
      FieldName = 'CaoArtID'
    end
    object OscArtTabModell: TStringField
      DisplayWidth = 10
      FieldName = 'Modell'
      Size = 100
    end
    object OscArtTabMenge: TFloatField
      DisplayWidth = 5
      FieldName = 'Menge'
      DisplayFormat = ',#0 '
    end
    object OscArtTabStatus: TIntegerField
      DisplayWidth = 3
      FieldName = 'Status'
      DisplayFormat = '0'
    end
    object OscArtTabBild: TStringField
      DisplayWidth = 10
      FieldName = 'Bild'
    end
    object OscArtTabPreis: TFloatField
      FieldName = 'Preis'
      DisplayFormat = ',#0.00 '
    end
    object OscArtTabMwStID: TIntegerField
      DisplayWidth = 3
      FieldName = 'MwStID'
      DisplayFormat = '0'
    end
    object OscArtTabHersteller: TIntegerField
      DisplayWidth = 11
      FieldName = 'Hersteller'
    end
    object OscArtTabTextDE: TStringField
      FieldName = 'TextDE'
      Size = 254
    end
  end
  object OscArt_DS: TDataSource
    DataSet = OscArtTab
    OnDataChange = OscArt_DSDataChange
    Left = 631
    Top = 278
  end
  object ArtTab: TZMySqlQuery
    Database = DM1.db1
    Transaction = DM1.Transact1
    CachedUpdates = False
    ShowRecordTypes = [ztModified, ztInserted, ztUnmodified]
    Options = [doHourGlass, doAutoFillDefs, doUseRowId]
    LinkOptions = [loAlwaysResync]
    Constraints = <>
    ExtraOptions = [moStoreResult]
    AlternativeView = True
    Macros = <>
    Sql.Strings = (
      'select * from ARTIKEL '
      'where REC_ID=:ID'
      'limit 0,1')
    RequestLive = True
    Left = 548
    Top = 115
    ParamData = <
      item
        DataType = ftInteger
        Name = 'ID'
        ParamType = ptInput
      end>
    object ArtTabREC_ID: TIntegerField
      FieldName = 'REC_ID'
    end
    object ArtTabARTNUM: TStringField
      FieldName = 'ARTNUM'
    end
    object ArtTabMATCHCODE: TStringField
      FieldName = 'MATCHCODE'
    end
    object ArtTabWARENGRUPPE: TIntegerField
      FieldName = 'WARENGRUPPE'
    end
    object ArtTabARTIKELTYP: TStringField
      FieldName = 'ARTIKELTYP'
      Size = 1
    end
    object ArtTabKAS_NAME: TStringField
      FieldName = 'KAS_NAME'
      Size = 80
    end
    object ArtTabKURZNAME: TStringField
      FieldName = 'KURZNAME'
      Size = 80
    end
    object ArtTabLANGNAME: TMemoField
      FieldName = 'LANGNAME'
      BlobType = ftMemo
    end
    object ArtTabSHOP_LANGTEXT: TMemoField
      FieldName = 'SHOP_LANGTEXT'
      BlobType = ftMemo
    end
    object ArtTabGEWICHT: TFloatField
      FieldName = 'GEWICHT'
    end
    object ArtTabVK1: TFloatField
      FieldName = 'VK1'
    end
    object ArtTabVK1B: TFloatField
      FieldName = 'VK1B'
    end
    object ArtTabVK2: TFloatField
      FieldName = 'VK2'
    end
    object ArtTabVK2B: TFloatField
      FieldName = 'VK2B'
    end
    object ArtTabVK3: TFloatField
      FieldName = 'VK3'
    end
    object ArtTabVK3B: TFloatField
      FieldName = 'VK3B'
    end
    object ArtTabVK4: TFloatField
      FieldName = 'VK4'
    end
    object ArtTabVK4B: TFloatField
      FieldName = 'VK4B'
    end
    object ArtTabVK5: TFloatField
      FieldName = 'VK5'
    end
    object ArtTabVK5B: TFloatField
      FieldName = 'VK5B'
    end
    object ArtTabSTEUER_CODE: TIntegerField
      FieldName = 'STEUER_CODE'
    end
    object ArtTabSHOP_ID: TIntegerField
      FieldName = 'SHOP_ID'
    end
    object ArtTabSHOP_ARTIKEL_ID: TIntegerField
      FieldName = 'SHOP_ARTIKEL_ID'
    end
    object ArtTabSHOP_DATENBLATT: TStringField
      FieldName = 'SHOP_DATENBLATT'
      Size = 100
    end
    object ArtTabSHOP_IMAGE_BIG: TStringField
      FieldName = 'SHOP_IMAGE_BIG'
      Size = 100
    end
    object ArtTabSHOP_PREIS_LISTE: TFloatField
      FieldName = 'SHOP_PREIS_LISTE'
    end
    object ArtTabERSTELLT: TDateField
      FieldName = 'ERSTELLT'
    end
    object ArtTabERST_NAME: TStringField
      FieldName = 'ERST_NAME'
    end
    object ArtTabSHOP_VISIBLE: TIntegerField
      FieldName = 'SHOP_VISIBLE'
    end
    object ArtTabERLOES_KTO: TIntegerField
      FieldName = 'ERLOES_KTO'
    end
    object ArtTabAUFW_KTO: TIntegerField
      FieldName = 'AUFW_KTO'
    end
    object ArtTabMENGE_AKT: TFloatField
      FieldName = 'MENGE_AKT'
    end
    object ArtTabHERSTELLER_ID: TIntegerField
      FieldName = 'HERSTELLER_ID'
      Required = True
    end
  end
  object MainMenu1: TMainMenu
    Left = 144
    Top = 112
  end
  object OscKatTab: TJvMemoryData
    FieldDefs = <>
    AfterOpen = OscKatTabAfterScroll
    AfterScroll = OscKatTabAfterScroll
    Left = 696
    Top = 372
    object OscKatTabID: TIntegerField
      DisplayWidth = 4
      FieldName = 'ID'
      DisplayFormat = '0000'
    end
    object OscKatTabPID: TIntegerField
      DisplayLabel = 'ParentID'
      DisplayWidth = 4
      FieldName = 'PID'
      DisplayFormat = '0000'
    end
    object OscKatTabImportiert: TBooleanField
      FieldName = 'Importiert'
    end
    object OscKatTabExists: TBooleanField
      DisplayLabel = 'Existiert'
      FieldName = 'Exists'
    end
    object OscKatTabName: TStringField
      DisplayWidth = 30
      FieldName = 'Name'
      Size = 200
    end
    object OscKatTabImageURL: TStringField
      DisplayLabel = 'Bild-URL'
      DisplayWidth = 40
      FieldName = 'ImageURL'
      Size = 250
    end
    object OscKatTabSortOrder: TIntegerField
      DisplayWidth = 3
      FieldName = 'SortOrder'
    end
    object OscKatTabDateAdded: TDateTimeField
      DisplayLabel = 'erstellt'
      FieldName = 'DateAdded'
      DisplayFormat = 'dd.mm.yyyy'
    end
    object OscKatTabLastModified: TDateTimeField
      DisplayLabel = 'le. �nderung'
      FieldName = 'LastModified'
      DisplayFormat = 'dd.mm.yyyy'
    end
  end
  object OscKat_DS: TDataSource
    DataSet = OscKatTab
    OnDataChange = OscKat_DSDataChange
    Left = 631
    Top = 372
  end
  object KatTab: TZMySqlQuery
    Database = DM1.db1
    Transaction = DM1.Transact1
    CachedUpdates = False
    ShowRecordTypes = [ztModified, ztInserted, ztUnmodified]
    Options = [doHourGlass, doAutoFillDefs, doUseRowId]
    LinkOptions = [loAlwaysResync]
    Constraints = <>
    ExtraOptions = [moStoreResult]
    AlternativeView = True
    Macros = <>
    Sql.Strings = (
      'select * from ARTIKEL_KAT '
      'where SHOP_ID=:SHOP_ID'
      'order by ID, TOP_ID')
    RequestLive = True
    Left = 500
    Top = 115
    ParamData = <
      item
        DataType = ftInteger
        Name = 'SHOP_ID'
        ParamType = ptInput
        Value = '1'
      end>
    object KatTabSHOP_ID: TIntegerField
      FieldName = 'SHOP_ID'
      Required = True
    end
    object KatTabID: TIntegerField
      FieldName = 'ID'
    end
    object KatTabTOP_ID: TIntegerField
      FieldName = 'TOP_ID'
      Required = True
    end
    object KatTabSORT_NUM: TIntegerField
      FieldName = 'SORT_NUM'
    end
    object KatTabNAME: TStringField
      FieldName = 'NAME'
      Required = True
      Size = 250
    end
    object KatTabIMAGE: TStringField
      FieldName = 'IMAGE'
      Size = 250
    end
    object KatTabVISIBLE_FLAG: TBooleanField
      FieldName = 'VISIBLE_FLAG'
      Required = True
    end
    object KatTabCHANGE_FLAG: TBooleanField
      FieldName = 'CHANGE_FLAG'
      Required = True
    end
    object KatTabDEL_FLAG: TBooleanField
      FieldName = 'DEL_FLAG'
      Required = True
    end
  end
  object OscHer_DS: TDataSource
    DataSet = OscHerTab
    OnDataChange = OscArt_DSDataChange
    Left = 631
    Top = 419
  end
  object OscHerTab: TJvMemoryData
    FieldDefs = <>
    AfterOpen = OscHerTabAfterScroll
    AfterScroll = OscHerTabAfterScroll
    Left = 696
    Top = 419
    object OscHerTabID: TIntegerField
      FieldName = 'ID'
      DisplayFormat = '0000'
    end
    object OscHerTabExists: TBooleanField
      FieldName = 'Exists'
    end
    object OscHerTabImportiert: TBooleanField
      FieldName = 'Importiert'
    end
    object OscHerTabDateAdded: TDateTimeField
      FieldName = 'DateAdded'
    end
    object OscHerTabLastModified: TDateTimeField
      FieldName = 'LastModified'
    end
    object OscHerTabHERSTELLER_IMAGE: TStringField
      FieldName = 'HERSTELLER_IMAGE'
      Size = 64
    end
    object OscHerTabHERSTELLER_NAME: TStringField
      FieldName = 'HERSTELLER_NAME'
    end
    object OscHerTabHERSTELLER_URL: TStringField
      DisplayLabel = 'Hersteller-URL'
      FieldName = 'HERSTELLER_URL'
      Size = 250
    end
  end
  object HerTab: TZMySqlQuery
    Database = DM1.db1
    Transaction = DM1.Transact1
    CachedUpdates = False
    ShowRecordTypes = [ztModified, ztInserted, ztUnmodified]
    Options = [doHourGlass, doAutoFillDefs, doUseRowId]
    LinkOptions = [loAlwaysResync]
    Constraints = <>
    ExtraOptions = [moStoreResult]
    AlternativeView = True
    Macros = <>
    Sql.Strings = (
      'select * from HERSTELLER '
      'where SHOP_ID=:SID '
      'order by HERSTELLER_ID')
    RequestLive = True
    Left = 501
    Top = 164
    ParamData = <
      item
        DataType = ftInteger
        Name = 'SID'
        ParamType = ptInput
      end>
    object HerTabSHOP_ID: TIntegerField
      FieldName = 'SHOP_ID'
      Required = True
    end
    object HerTabHERSTELLER_ID: TIntegerField
      FieldName = 'HERSTELLER_ID'
    end
    object HerTabHERSTELLER_NAME: TStringField
      FieldName = 'HERSTELLER_NAME'
      Required = True
      Size = 32
    end
    object HerTabHERSTELLER_IMAGE: TStringField
      FieldName = 'HERSTELLER_IMAGE'
      Size = 64
    end
    object HerTabLAST_CHANGE: TDateTimeField
      FieldName = 'LAST_CHANGE'
    end
    object HerTabSHOP_DATE_ADDED: TDateTimeField
      FieldName = 'SHOP_DATE_ADDED'
    end
    object HerTabSHOP_DATE_CHANGE: TDateTimeField
      FieldName = 'SHOP_DATE_CHANGE'
    end
    object HerTabSYNC_FLAG: TBooleanField
      FieldName = 'SYNC_FLAG'
      Required = True
    end
    object HerTabCHANGE_FLAG: TBooleanField
      FieldName = 'CHANGE_FLAG'
      Required = True
    end
    object HerTabDEL_FLAG: TBooleanField
      FieldName = 'DEL_FLAG'
      Required = True
    end
  end
  object HerDesTab: TZMySqlQuery
    Database = DM1.db1
    Transaction = DM1.Transact1
    CachedUpdates = False
    ShowRecordTypes = [ztModified, ztInserted, ztUnmodified]
    Options = [doHourGlass, doAutoFillDefs, doUseRowId]
    LinkOptions = [loAlwaysResync]
    Constraints = <>
    ExtraOptions = [moStoreResult]
    AlternativeView = True
    Macros = <>
    Sql.Strings = (
      'select * from HERSTELLER_INFO '
      'where HERSTELLER_ID=:ID'
      'limit 0,1')
    RequestLive = True
    Left = 549
    Top = 164
    ParamData = <
      item
        DataType = ftInteger
        Name = 'ID'
        ParamType = ptInput
      end>
    object HerDesTabSHOP_ID: TIntegerField
      FieldName = 'SHOP_ID'
    end
    object HerDesTabHERSTELLER_ID: TIntegerField
      FieldName = 'HERSTELLER_ID'
    end
    object HerDesTabSPRACHE_ID: TIntegerField
      FieldName = 'SPRACHE_ID'
    end
    object HerDesTabHERSTELLER_URL: TStringField
      FieldName = 'HERSTELLER_URL'
      Size = 255
    end
    object HerDesTabURL_CLICKED: TIntegerField
      FieldName = 'URL_CLICKED'
    end
    object HerDesTabDATE_LAST_CLICK: TDateTimeField
      FieldName = 'DATE_LAST_CLICK'
    end
  end
  object KunLiefTab: TZMySqlQuery
    Database = DM1.db1
    Transaction = DM1.Transact1
    CachedUpdates = False
    ShowRecordTypes = [ztModified, ztInserted, ztUnmodified]
    Options = [doHourGlass, doAutoFillDefs, doUseRowId]
    LinkOptions = [loAlwaysResync]
    Constraints = <>
    ExtraOptions = [moStoreResult]
    Macros = <>
    Sql.Strings = (
      'select * from ADRESSEN_LIEF'
      
        'where ADDR_ID=:AID and NAME1=:NAME1 and NAME2=:NAME2 and STRASSE' +
        '=:STRASSE and ORT=:ORT and PLZ=:PLZ and LAND=:LAND')
    RequestLive = True
    Left = 684
    Top = 164
    ParamData = <
      item
        DataType = ftInteger
        Name = 'AID'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'NAME1'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'NAME2'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'STRASSE'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'ORT'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'PLZ'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'LAND'
        ParamType = ptInput
      end>
  end
end
