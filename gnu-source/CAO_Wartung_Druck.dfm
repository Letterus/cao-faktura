object FWartung_Druck: TFWartung_Druck
  Left = 530
  Top = 168
  BorderStyle = bsToolWindow
  Caption = 'Optionen ausw�hlen...'
  ClientHeight = 232
  ClientWidth = 415
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnHide = FormHide
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 8
    Width = 43
    Height = 13
    Caption = 'Formular:'
  end
  object Label2: TLabel
    Left = 8
    Top = 50
    Width = 60
    Height = 13
    Caption = 'Ausgabeziel:'
  end
  object Label7: TLabel
    Left = 8
    Top = 73
    Width = 43
    Height = 13
    Caption = 'Schacht:'
  end
  object Label8: TLabel
    Left = 8
    Top = 98
    Width = 42
    Height = 13
    Caption = 'Kopieen:'
  end
  object FormularCB: TJvDBLookupCombo
    Left = 71
    Top = 6
    Width = 242
    Height = 21
    DropDownCount = 8
    Ctl3D = True
    LookupField = 'NAME'
    LookupDisplay = 'NAME'
    LookupSource = DS_ReportQuery
    ParentCtl3D = False
    TabOrder = 0
    OnChange = FormularCBChange
  end
  object ZielCB: TComboBox
    Left = 72
    Top = 46
    Width = 241
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 1
    OnChange = ZielCBChange
  end
  object LayoutBtn: TBitBtn
    Left = 320
    Top = 3
    Width = 88
    Height = 28
    Caption = '&Bearbeiten'
    TabOrder = 2
    OnClick = LayoutBtnClick
    Glyph.Data = {
      42040000424D4204000000000000420000002800000020000000100000000100
      1000030000000004000000000000000000000000000000000000007C0000E003
      00001F0000000042004200420042004200420000000000000000000000000000
      000000000000004200420042004200420042EF3DEF3DEF3DEF3DEF3DEF3DEF3D
      EF3DEF3DEF3D0042004200420042004200420000FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7F00000042FF7FFF7F0042FF7FFF7FEF3DFF7FFF7F00420042FF7F0042
      FF7FFF7FEF3D00000000004200000000000000000000FF7FFF7F0000FF7F0000
      0000FF7F0000EF3DEF3DFF7FEF3DEF3DEF3DEF3DEF3D0042FF7FEF3D0042EF3D
      EF3D0042EF3DFF0300000000E07FFF7FE07FFF7FE07F0000FF7FFF7FFF7FFF7F
      FF7FFF7F0000EF3DEF3DEF3D0042004200420042FF7FEF3DFF7F004200420042
      0042FF7FEF3DFF030000E07FFF7FE07FFF7F000000000000FF7FFF7FFF7FFF7F
      0000FF7F0000EF3DEF3DFF7F004200420042EF3DEF3DEF3D0042FF7F0042FF7F
      EF3D0042EF3DFF030000FF7FE07FFF7FE07FFF7FE07FFF7F0000FF7F00000000
      FF7FFF7F0000EF3DEF3DFF7F0042004200420042FF7FFF7FEF3DFF7FEF3DEF3D
      FF7F0042EF3DFF030000E07FFF7FE07FFF7F00000000000000000000E07F0000
      FF7FFF7F0000EF3DEF3DFF7F004200420042EF3DEF3DEF3DEF3DEF3D0042EF3D
      00420042EF3DFF030000FF7FE07FFF7FE07FFF7FE07FFF7FE07FFF7F0000FF7F
      FF7FFF7F0000EF3DEF3DFF7F00420042FF7FFF7FFF7FFF7FFF7FFF7FEF3D0042
      00420042EF3DFF030000E07FFF7F0000000000000000000000000000FF7FFF7F
      FF7FFF7F0000EF3DEF3DFF7FFF7FEF3DEF3DEF3DEF3DEF3DEF3DEF3D00420042
      FF7FFF7FEF3D000000000000E07FFF7FE07F00000000E07F0000FF7FFF7F0000
      0000FF7F0000EF3DEF3DEF3D0042FF7FFF7FEF3DEF3D0042EF3D00420042EF3D
      EF3D0042EF3D0042004200420000000000000000E07F0000FF7FFF7FFF7FFF7F
      FF7FFF7F0000004200420042EF3DEF3DEF3DEF3D0042EF3D0042004200420042
      FF7FFF7FEF3D004200420042004200420000E07F0000FF7FFF7FFF7FFF7F0000
      00000000000000420042004200420042EF3D0042EF3D00420042FF7FFF7FEF3D
      EF3DEF3DEF3D00420042004200420000E07F0000FF7FFF7F00000000FF7F0000
      FF7FFF7F00000042004200420042EF3D0042EF3DFF7F0042EF3DEF3D0042EF3D
      FF7F0042EF3D0042004200420000E07F00000000FF7FFF7FFF7FFF7FFF7F0000
      FF7F00000042004200420042EF3DFF7FEF3DEF3DFF7F0042004200420042EF3D
      FF7FEF3D0042004200420000007C000000420000FF7FFF7FFF7FFF7FFF7F0000
      00000042004200420042EF3DEF3DEF3D0042EF3DFF7FFF7FFF7FFF7FFF7FEF3D
      EF3D004200420042004200420000004200420000000000000000000000000000
      004200420042004200420042EF3D00420042EF3DEF3DEF3DEF3DEF3DEF3DEF3D
      004200420042}
    NumGlyphs = 2
  end
  object BinNamCB: TComboBox
    Left = 72
    Top = 70
    Width = 241
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 3
  end
  object AnzCopy: TJvSpinEdit
    Left = 72
    Top = 94
    Width = 73
    Height = 21
    MaxValue = 99
    MinValue = 1
    Value = 1
    TabOrder = 4
  end
  object VorschauBtn: TBitBtn
    Left = 8
    Top = 128
    Width = 81
    Height = 28
    Caption = '&Vorschau'
    TabOrder = 5
    OnClick = VorschauBtnClick
    Glyph.Data = {
      4E010000424D4E01000000000000760000002800000012000000120000000100
      040000000000D800000000000000000000001000000010000000000000000000
      BF0000BF000000BFBF00BF000000BF00BF00BFBF0000C0C0C000808080000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
      DDDDDD000000DDDDDDDDDDDDDDDDDD000000D000000000000DD00D000000D0FF
      FFFFFFFF0D000D000000D0FFFFFFF0000800DD000000D0FFFFFF0877808DDD00
      0000D0FFFFF0877E880DDD000000D0FFFFF07777870DDD000000D0FFFFF07E77
      870DDD000000D0FFFFF08EE7880DDD000000D0FFFFFF087780DDDD000000D0FF
      FFFFF0000DDDDD000000D0FFFFFFFFFF0DDDDD000000D0FFFFFFF0000DDDDD00
      0000D0FFFFFFF070DDDDDD000000D0FFFFFFF00DDDDDDD000000DD00000000DD
      DDDDDD000000DDDDDDDDDDDDDDDDDD000000}
  end
  object PrintBtn: TBitBtn
    Left = 96
    Top = 128
    Width = 89
    Height = 28
    Caption = '&Drucken'
    TabOrder = 6
    OnClick = PrintBtnClick
    Glyph.Data = {
      82060000424D8206000000000000420000002800000028000000140000000100
      1000030000004006000000000000000000000000000000000000007C0000E003
      00001F0000000042004200420042004200420042004200420042004200420042
      0042004200420042004200420042004200420042004200420042004200420042
      0042004200420042004200420042004200420042004200420042004200420042
      0042004200420042004200420042004200420042004200420042004200420042
      00420042FF7FFF7FFF7F0042004200420042004200420042004200420042FF7F
      FF7FFF7F00420042004200000000000000420042004200420042004200420042
      004200420000000000000042004200420042104210421042FF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7F104210421042FF7FFF7F00420000000000000000
      0000000000000000000000000000000000000000000000000000000000420042
      1042104210421042104210421042104210421042104210421042104210421042
      10421042FF7F00420000FF7F1863186318631863186318631863186318631863
      186318631863186318630000004200421042FF7F004200420042004200420042
      0042004200420042004200420042004200421042FF7F00420000FF7F18631863
      1863186318631863186318631863186318631863186318631863000000420042
      1042FF7F00420042004200420042004200420042004200420042004200420042
      00421042FF7F00420000FF7F1863186318631863186318631863186318631863
      1863007C007C186318630000004200421042FF7F004200420042004200420042
      0042004200420042004200420042004200421042FF7F00420000FF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F000000420042
      1042FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7F1042FF7F0042104200000000104210421042104210421042104210421042
      1042104210420000000010420042004210421042104210421042104210421042
      1042104210421042104210421042104210421042004200420042004200000000
      0000000000000000000000000000000000000000000000000042004200420042
      0042004210421042104210421042104210421042104210421042104210421042
      FF7F0042004200420042004200000000FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7F0000000000420042004200420042004210421042FF7F0042FF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7F1042104200420042004200420042004200420000
      FF7F00000000000000000000000000000000FF7F000000420042004200420042
      0042004200421042FF7F1042104210421042104210421042104200421042FF7F
      00420042004200420042004200420000FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7F0000004200420042004200420042004200421042FF7F0042FF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7F1042FF7F00420042004200420042004200420000
      FF7F00000000000000000000000000000000FF7F000000420042004200420042
      0042004200421042FF7F1042104210421042104210421042104200421042FF7F
      00420042004200420042004200420000FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7F0000004200420042004200420042004200421042FF7F0042FF7FFF7F
      0042004200420042004200421042FF7F00420042004200420042004200420000
      FF7F00000000FF7FFF7FFF7FFF7FFF7FFF7FFF7F000000420042004200420042
      0042004200421042FF7F1042104200420042004200420042004200421042FF7F
      00420042004200420042004200420000FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7F0000004200420042004200420042004200421042FF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7F1042FF7F00420042004200420042004200420000
      0000000000000000000000000000000000000000000000420042004200420042
      0042004200421042104210421042104210421042104210421042104210420042
      0042004200420042004200420042004200420042004200420042004200420042
      0042004200420042004200420042004200420042004200420042004200420042
      0042004200420042004200420042004200420042004200420042004200420042
      0042004200420042004200420042004200420042004200420042004200420042
      0042004200420042004200420042004200420042004200420042004200420042
      004200420042}
    NumGlyphs = 2
  end
  object DruEinrBtn: TBitBtn
    Left = 320
    Top = 92
    Width = 89
    Height = 28
    Caption = '&Einrichten'
    TabOrder = 7
    OnClick = DruEinrBtnClick
    Glyph.Data = {
      96090000424D9609000000000000360000002800000028000000140000000100
      1800000000006009000000000000000000000000000000000000008080008080
      0080800080800080800080800080800080800080800080800080800080800080
      8000808000808000808000808000808000808000808000808000808000808000
      8080008080008080008080008080008080008080008080008080008080008080
      0080800080800080800080800080800080800080800080800080800080800080
      8000808000808000808000808000808000808000808000808000808000808000
      8080008080008080008080008080008080008080008080FFFFFFFFFFFFFFFFFF
      008080008080008080008080008080008080008080008080008080008080FFFF
      FFFFFFFFFFFFFF00808000808000808000000000000000000000808000808000
      8080008080008080008080008080008080008080008080000000000000000000
      008080008080008080008080808080808080808080FFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF808080808080808080FFFFFFFF
      FFFF008080000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000080800080
      8080808080808080808080808080808080808080808080808080808080808080
      8080808080808080808080808080808080808080808080FFFFFF008080000000
      FFFFFFC0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
      C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0000000008080008080808080FFFFFF00
      8080008080008080008080008080008080008080008080008080008080008080
      008080008080008080008080808080FFFFFF008080000000FFFFFFC0C0C0C0C0
      C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
      C0C0C0C0C0C0C0C0000000008080008080808080FFFFFF008080008080008080
      0080800080800080800080800080800080800080800080800080800080800080
      80008080808080FFFFFF008080000000FFFFFFC0C0C0C0C0C0C0C0C0C0C0C0C0
      C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C00000FF0000FFC0C0C0C0C0C0
      000000008080008080808080FFFFFF0080800080800080800080800080800080
      80008080008080008080008080008080008080008080008080008080808080FF
      FFFF008080000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000000080800080
      80808080FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF808080FFFFFF008080808080
      0000000000008080808080808080808080808080808080808080808080808080
      8080808080808080808000000000000080808000808000808080808080808080
      8080808080808080808080808080808080808080808080808080808080808080
      8080808080808080808080808080800080800080800080800080800000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000008080008080008080008080008080008080808080808080808080
      8080808080808080808080808080808080808080808080808080808080808080
      80FFFFFF00808000808000808000808000808000000080808080808080808080
      8080808080808080808080808080808080808080808080808080000000008080
      0080800080800080800080800080808080808080808080808080808080808080
      80808080808080808080808080808080808080808080808080FFFFFF00808000
      8080008080008080008080000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000080800080800080800080
      8000808000808080808080808080808080808080808080808080808080808080
      8080808080808080808080808080808080008080008080008080008080008080
      0080800080800080800080800080800080800080800080800080800080800080
      8000808000808000808000808000808000808000808000808000808000808000
      8080008080008080008080008080008080008080008080008080008080008080
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0080800080800080800080800080800080
      8000808000808000808000808000808000808000808000808080808000000000
      0000000000808080008080008080008080008080008080008080008080008080
      0080800080800080800080800080800080800080808080808080808080808080
      80808080FFFFFF00808000808000808000808000808000808000808000808000
      8080008080008080008080008080008080000000FFFF00FFFFFFFFFF00000000
      008080008080008080008080008080008080FFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFF808080008080008080008080808080FFFFFF00
      8080008080008080008080808080000000000000000000000000000000000000
      000000000000000000FFFF00FFFFFF0000000000008080800080800080800080
      8000808000808080808080808080808080808080808080808080808080808080
      8080808080FFFFFF008080808080808080808080008080008080008080008080
      008080000000FFFF00800000FFFF00FFFFFFFFFF00FFFFFFFFFF00FFFFFF8080
      80FFFFFFFFFF0000000000808000808000808000808000808000808000808080
      8080FFFFFF808080FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF808080FFFFFF
      008080808080008080FFFFFFFFFFFF0080800080800080800080808080800000
      00000000000000000000000000000000000000000000000000FFFF00FFFFFF00
      0000000000808080008080008080008080008080008080808080808080808080
      808080808080808080808080808080808080808080008080FFFFFF8080808080
      80808080FFFFFF00808000808000808000808000808000808000808000808000
      8080008080008080008080008080008080000000FFFF00FFFFFFFFFF00000000
      0080800080800080800080800080800080800080800080800080800080800080
      80008080008080008080008080808080FFFFFFFFFFFFFFFFFF808080FFFFFF00
      8080008080008080008080008080008080008080008080008080008080008080
      0080800080800080808080800000000000000000008080800080800080800080
      8000808000808000808000808000808000808000808000808000808000808000
      8080008080808080808080808080808080808080008080008080}
    NumGlyphs = 2
  end
  object CloseBtn: TBitBtn
    Left = 320
    Top = 128
    Width = 89
    Height = 28
    Cancel = True
    Caption = 'S&chlie�en'
    TabOrder = 8
    OnClick = CloseBtnClick
    Glyph.Data = {
      DE010000424DDE01000000000000760000002800000024000000120000000100
      0400000000006801000000000000000000001000000000000000000000000000
      80000080000000808000800000008000800080800000C0C0C000808080000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00388888888877
      F7F787F8888888888333333F00004444400888FFF444448888888888F333FF8F
      000033334D5007FFF4333388888888883338888F0000333345D50FFFF4333333
      338F888F3338F33F000033334D5D0FFFF43333333388788F3338F33F00003333
      45D50FEFE4333333338F878F3338F33F000033334D5D0FFFF43333333388788F
      3338F33F0000333345D50FEFE4333333338F878F3338F33F000033334D5D0FFF
      F43333333388788F3338F33F0000333345D50FEFE4333333338F878F3338F33F
      000033334D5D0EFEF43333333388788F3338F33F0000333345D50FEFE4333333
      338F878F3338F33F000033334D5D0EFEF43333333388788F3338F33F00003333
      4444444444333333338F8F8FFFF8F33F00003333333333333333333333888888
      8888333F00003333330000003333333333333FFFFFF3333F00003333330AAAA0
      333333333333888888F3333F00003333330000003333333333338FFFF8F3333F
      0000}
    NumGlyphs = 2
  end
  object BitBtn1: TBitBtn
    Left = 320
    Top = 48
    Width = 89
    Height = 25
    Caption = 'PDF'
    TabOrder = 9
    Visible = False
    Glyph.Data = {
      06020000424D0602000000000000760000002800000028000000140000000100
      0400000000009001000000000000000000001000000000000000000000000000
      80000080000000808000800000008000800080800000C0C0C000808080000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
      3333333333333333333333333333333333333333333333333333333333333333
      33333333333333333333333333333FFFFFFFFFFFF33333380000000000008333
      33338888888888883F333330CC08CCF770CC03333338F38F333338F38F333330
      CC08CCF770CC03333338F38F333338F38F333330CC07887770CC03333338F38F
      FFFFF8338F333330CC60000006CC03333338F338888883338F333330CCCCCCCC
      CCCC03333338F33FFFFFFFF38F333330C6000000006C03333338F3888888883F
      8F333330C0FFFFFFFF0C03333338F8F33333338F8F333330C0FFFFFFFF0C0333
      3338F8F33333338F8F333330C0FFFFFFFF0C03333338F8F33333338F8F333330
      C0FFFFFFFF0C03333338F8F33333338F8F33333000FFFFFFFF0003333338F8F3
      3333338F8F333330C0FFFFFFFF0C03333338F8FFFFFFFF8F8333333800000000
      0000833333338888888888883333333333333333333333333333333333333333
      3333333333333333333333333333333333333333333333333333333333333333
      33333333333333333333}
    NumGlyphs = 2
  end
  object BitBtn2: TBitBtn
    Left = 192
    Top = 128
    Width = 89
    Height = 28
    Caption = 'eMail'
    TabOrder = 10
    Visible = False
    Glyph.Data = {
      42010000424D4201000000000000760000002800000011000000110000000100
      040000000000CC00000000000000000000001000000010000000000000000000
      BF0000BF000000BFBF00BF000000BF00BF00BFBF0000C0C0C000808080000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
      77777000000070000000000077777000000070FFFFFFFFF0777770000000700F
      FFFFF00000000000000070F0FFFFF0FF77770000000070FF0FFF00B800870000
      000070F0F000F0F07F0700000000700F7F7F70B07B070000000070F7F7F7F0F0
      7F0700000000770F7F7F70B07B07000000007770F7F7F0F07F07000000007777
      0F7F00B07BFB000000007777700070008800000000007777777777707F077000
      0000777777777778007770000000777777777777777770000000777777777777
      777770000000}
  end
  object ppDesigner1: TppDesigner
    Caption = 'ReportBuilder'
    DataSettings.SessionType = 'BDESession'
    DataSettings.AllowEditSQL = False
    DataSettings.CollationType = ctASCII
    DataSettings.DatabaseType = dtParadox
    DataSettings.IsCaseSensitive = True
    DataSettings.SQLType = sqBDELocal
    Position = poScreenCenter
    Report = ppReport1
    IniStorageType = 'IniFile'
    IniStorageName = '($WINSYS)\RBuilder.ini'
    WindowHeight = 400
    WindowLeft = 100
    WindowTop = 50
    WindowWidth = 600
    Left = 8
    Top = 168
  end
  object ppReport1: TppReport
    AutoStop = False
    DataPipeline = Kundendaten
    PassSetting = psTwoPass
    PrinterSetup.BinName = 'Default'
    PrinterSetup.DocumentName = 'Report'
    PrinterSetup.PaperName = 'A4'
    PrinterSetup.PrinterName = 'Default'
    PrinterSetup.mmMarginBottom = 6350
    PrinterSetup.mmMarginLeft = 6350
    PrinterSetup.mmMarginRight = 6350
    PrinterSetup.mmMarginTop = 6350
    PrinterSetup.mmPaperHeight = 297000
    PrinterSetup.mmPaperWidth = 210000
    PrinterSetup.PaperSize = 9
    Template.DatabaseSettings.DataPipeline = ppRepPipeline
    Template.DatabaseSettings.Name = 'Test'
    Template.DatabaseSettings.NameField = 'Name'
    Template.DatabaseSettings.TemplateField = 'VAL_BLOB'
    Template.SaveTo = stDatabase
    Units = utMillimeters
    DeviceType = 'Screen'
    Left = 40
    Top = 168
    Version = '6.03'
    mmColumnWidth = 0
    DataPipelineName = 'Kundendaten'
    object ppTitleBand2: TppTitleBand
      PrintHeight = phDynamic
      mmBottomOffset = 0
      mmHeight = 10054
      mmPrintPosition = 0
    end
    object ppDetailBand2: TppDetailBand
      PrintHeight = phDynamic
      mmBottomOffset = 0
      mmHeight = 10054
      mmPrintPosition = 0
    end
    object ppFooterBand2: TppFooterBand
      mmBottomOffset = 0
      mmHeight = 10054
      mmPrintPosition = 0
    end
  end
  object ppRepPipeline: TppDBPipeline
    DataSource = DS_ReportQuery
    UserName = 'RepPipeline'
    Visible = False
    Left = 72
    Top = 168
  end
  object DS_ReportQuery: TDataSource
    DataSet = ReportQuery
    Left = 104
    Top = 168
  end
  object Wartungsliste: TppDBPipeline
    DataSource = WartungsForm.DS_Wartungsliste
    UserName = 'Wartungsliste'
    Left = 8
    Top = 200
  end
  object Anlagen: TppDBPipeline
    DataSource = WartungsForm.DS_Anlagen
    RangeEnd = reCount
    RangeEndCount = 1
    RangeBegin = rbCurrentRecord
    UserName = 'Anlagen'
    Left = 40
    Top = 200
    object AnlagenppField1: TppField
      Alignment = taRightJustify
      FieldAlias = 'REC_ID'
      FieldName = 'REC_ID'
      FieldLength = 0
      DataType = dtInteger
      DisplayWidth = 10
      Position = 0
    end
    object AnlagenppField2: TppField
      Alignment = taRightJustify
      FieldAlias = 'ADDR_ID'
      FieldName = 'ADDR_ID'
      FieldLength = 0
      DataType = dtInteger
      DisplayWidth = 10
      Position = 1
    end
    object AnlagenppField3: TppField
      FieldAlias = 'BESCHREIBUNG'
      FieldName = 'BESCHREIBUNG'
      FieldLength = 255
      DisplayWidth = 255
      Position = 2
    end
    object AnlagenppField4: TppField
      FieldAlias = 'WARTUNG'
      FieldName = 'WARTUNG'
      FieldLength = 0
      DataType = dtDate
      DisplayWidth = 10
      Position = 3
    end
    object AnlagenppField5: TppField
      Alignment = taRightJustify
      FieldAlias = 'INTERVALL'
      FieldName = 'INTERVALL'
      FieldLength = 0
      DataType = dtInteger
      DisplayWidth = 10
      Position = 4
    end
    object AnlagenppField6: TppField
      Alignment = taRightJustify
      FieldAlias = 'WVERTRAG'
      FieldName = 'WVERTRAG'
      FieldLength = 0
      DataType = dtInteger
      DisplayWidth = 10
      Position = 5
    end
    object AnlagenppField7: TppField
      FieldAlias = 'BEMERKUNG'
      FieldName = 'BEMERKUNG'
      FieldLength = 0
      DataType = dtMemo
      DisplayWidth = 10
      Position = 6
      Searchable = False
      Sortable = False
    end
    object AnlagenppField8: TppField
      FieldAlias = 'LEBENSLAUF'
      FieldName = 'LEBENSLAUF'
      FieldLength = 0
      DataType = dtMemo
      DisplayWidth = 10
      Position = 7
      Searchable = False
      Sortable = False
    end
    object AnlagenppField9: TppField
      FieldAlias = 'LISTE'
      FieldName = 'LISTE'
      FieldLength = 0
      DataType = dtMemo
      DisplayWidth = 10
      Position = 8
      Searchable = False
      Sortable = False
    end
  end
  object Ansprechpartner: TppDBPipeline
    DataSource = WartungsForm.DS_Ansprechpartner
    UserName = 'Ansprechpartner'
    Left = 72
    Top = 200
    object AnsprechpartnerppField1: TppField
      Alignment = taRightJustify
      FieldAlias = 'ADDR_ID'
      FieldName = 'ADDR_ID'
      FieldLength = 0
      DataType = dtInteger
      DisplayWidth = 10
      Position = 0
    end
    object AnsprechpartnerppField2: TppField
      Alignment = taRightJustify
      FieldAlias = 'REC_ID'
      FieldName = 'REC_ID'
      FieldLength = 0
      DataType = dtInteger
      DisplayWidth = 10
      Position = 1
    end
    object AnsprechpartnerppField3: TppField
      FieldAlias = 'NAME'
      FieldName = 'NAME'
      FieldLength = 40
      DisplayWidth = 40
      Position = 2
    end
    object AnsprechpartnerppField4: TppField
      FieldAlias = 'VORNAME'
      FieldName = 'VORNAME'
      FieldLength = 40
      DisplayWidth = 40
      Position = 3
    end
    object AnsprechpartnerppField5: TppField
      FieldAlias = 'ANREDE'
      FieldName = 'ANREDE'
      FieldLength = 40
      DisplayWidth = 40
      Position = 4
    end
    object AnsprechpartnerppField6: TppField
      FieldAlias = 'STRASSE'
      FieldName = 'STRASSE'
      FieldLength = 40
      DisplayWidth = 40
      Position = 5
    end
    object AnsprechpartnerppField7: TppField
      FieldAlias = 'LAND'
      FieldName = 'LAND'
      FieldLength = 5
      DisplayWidth = 5
      Position = 6
    end
    object AnsprechpartnerppField8: TppField
      FieldAlias = 'PLZ'
      FieldName = 'PLZ'
      FieldLength = 10
      DisplayWidth = 10
      Position = 7
    end
    object AnsprechpartnerppField9: TppField
      FieldAlias = 'ORT'
      FieldName = 'ORT'
      FieldLength = 40
      DisplayWidth = 40
      Position = 8
    end
    object AnsprechpartnerppField10: TppField
      FieldAlias = 'FUNKTION'
      FieldName = 'FUNKTION'
      FieldLength = 40
      DisplayWidth = 40
      Position = 9
    end
    object AnsprechpartnerppField11: TppField
      FieldAlias = 'TELEFON'
      FieldName = 'TELEFON'
      FieldLength = 40
      DisplayWidth = 40
      Position = 10
    end
    object AnsprechpartnerppField12: TppField
      FieldAlias = 'TELEFAX'
      FieldName = 'TELEFAX'
      FieldLength = 40
      DisplayWidth = 40
      Position = 11
    end
    object AnsprechpartnerppField13: TppField
      FieldAlias = 'MOBILFUNK'
      FieldName = 'MOBILFUNK'
      FieldLength = 40
      DisplayWidth = 40
      Position = 12
    end
    object AnsprechpartnerppField14: TppField
      FieldAlias = 'TELEPRIVAT'
      FieldName = 'TELEPRIVAT'
      FieldLength = 40
      DisplayWidth = 40
      Position = 13
    end
    object AnsprechpartnerppField15: TppField
      FieldAlias = 'EMAIL'
      FieldName = 'EMAIL'
      FieldLength = 100
      DisplayWidth = 100
      Position = 14
    end
    object AnsprechpartnerppField16: TppField
      FieldAlias = 'INFO'
      FieldName = 'INFO'
      FieldLength = 0
      DataType = dtMemo
      DisplayWidth = 10
      Position = 15
      Searchable = False
      Sortable = False
    end
    object AnsprechpartnerppField17: TppField
      FieldAlias = 'GEBDATUM'
      FieldName = 'GEBDATUM'
      FieldLength = 0
      DataType = dtDate
      DisplayWidth = 10
      Position = 16
    end
  end
  object Kundendaten: TppDBPipeline
    DataSource = WartungsForm.DS_KundenListe
    RangeEnd = reCount
    RangeEndCount = 1
    RangeBegin = rbCurrentRecord
    UserName = 'Kundendaten'
    Left = 104
    Top = 200
    object KundendatenppField1: TppField
      Alignment = taRightJustify
      FieldAlias = 'REC_ID'
      FieldName = 'REC_ID'
      FieldLength = 0
      DataType = dtInteger
      DisplayWidth = 10
      Position = 0
    end
    object KundendatenppField2: TppField
      FieldAlias = 'MATCHCODE'
      FieldName = 'MATCHCODE'
      FieldLength = 255
      DisplayWidth = 255
      Position = 1
    end
    object KundendatenppField3: TppField
      Alignment = taRightJustify
      FieldAlias = 'KUNDENGRUPPE'
      FieldName = 'KUNDENGRUPPE'
      FieldLength = 0
      DataType = dtInteger
      DisplayWidth = 10
      Position = 2
    end
    object KundendatenppField4: TppField
      FieldAlias = 'KUNNUM1'
      FieldName = 'KUNNUM1'
      FieldLength = 20
      DisplayWidth = 20
      Position = 3
    end
    object KundendatenppField5: TppField
      FieldAlias = 'KUNNUM2'
      FieldName = 'KUNNUM2'
      FieldLength = 20
      DisplayWidth = 20
      Position = 4
    end
    object KundendatenppField6: TppField
      FieldAlias = 'NAME1'
      FieldName = 'NAME1'
      FieldLength = 40
      DisplayWidth = 40
      Position = 5
    end
    object KundendatenppField7: TppField
      FieldAlias = 'PLZ'
      FieldName = 'PLZ'
      FieldLength = 10
      DisplayWidth = 10
      Position = 6
    end
    object KundendatenppField8: TppField
      FieldAlias = 'ORT'
      FieldName = 'ORT'
      FieldLength = 40
      DisplayWidth = 40
      Position = 7
    end
    object KundendatenppField9: TppField
      FieldAlias = 'LAND'
      FieldName = 'LAND'
      FieldLength = 5
      DisplayWidth = 5
      Position = 8
    end
    object KundendatenppField10: TppField
      FieldAlias = 'VWAHL'
      FieldName = 'VWAHL'
      FieldLength = 10
      DisplayWidth = 10
      Position = 9
    end
    object KundendatenppField11: TppField
      FieldAlias = 'NAME2'
      FieldName = 'NAME2'
      FieldLength = 40
      DisplayWidth = 40
      Position = 10
    end
    object KundendatenppField12: TppField
      FieldAlias = 'NAME3'
      FieldName = 'NAME3'
      FieldLength = 40
      DisplayWidth = 40
      Position = 11
    end
    object KundendatenppField13: TppField
      FieldAlias = 'ABTEILUNG'
      FieldName = 'ABTEILUNG'
      FieldLength = 40
      DisplayWidth = 40
      Position = 12
    end
    object KundendatenppField14: TppField
      FieldAlias = 'ANREDE'
      FieldName = 'ANREDE'
      FieldLength = 40
      DisplayWidth = 40
      Position = 13
    end
    object KundendatenppField15: TppField
      FieldAlias = 'STRASSE'
      FieldName = 'STRASSE'
      FieldLength = 40
      DisplayWidth = 40
      Position = 14
    end
    object KundendatenppField16: TppField
      FieldAlias = 'POSTFACH'
      FieldName = 'POSTFACH'
      FieldLength = 40
      DisplayWidth = 40
      Position = 15
    end
    object KundendatenppField17: TppField
      FieldAlias = 'PF_PLZ'
      FieldName = 'PF_PLZ'
      FieldLength = 10
      DisplayWidth = 10
      Position = 16
    end
    object KundendatenppField18: TppField
      Alignment = taRightJustify
      FieldAlias = 'HAT_LIEFANSR'
      FieldName = 'HAT_LIEFANSR'
      FieldLength = 0
      DataType = dtInteger
      DisplayWidth = 10
      Position = 17
    end
    object KundendatenppField19: TppField
      FieldAlias = 'L_ANREDE'
      FieldName = 'L_ANREDE'
      FieldLength = 40
      DisplayWidth = 40
      Position = 18
    end
    object KundendatenppField20: TppField
      FieldAlias = 'L_NAME1'
      FieldName = 'L_NAME1'
      FieldLength = 40
      DisplayWidth = 40
      Position = 19
    end
    object KundendatenppField21: TppField
      FieldAlias = 'L_NAME2'
      FieldName = 'L_NAME2'
      FieldLength = 40
      DisplayWidth = 40
      Position = 20
    end
    object KundendatenppField22: TppField
      FieldAlias = 'L_NAME3'
      FieldName = 'L_NAME3'
      FieldLength = 40
      DisplayWidth = 40
      Position = 21
    end
    object KundendatenppField23: TppField
      FieldAlias = 'L_ABTEIL'
      FieldName = 'L_ABTEIL'
      FieldLength = 40
      DisplayWidth = 40
      Position = 22
    end
    object KundendatenppField24: TppField
      FieldAlias = 'L_STRASSE'
      FieldName = 'L_STRASSE'
      FieldLength = 40
      DisplayWidth = 40
      Position = 23
    end
    object KundendatenppField25: TppField
      FieldAlias = 'L_PLZ'
      FieldName = 'L_PLZ'
      FieldLength = 10
      DisplayWidth = 10
      Position = 24
    end
    object KundendatenppField26: TppField
      FieldAlias = 'L_ORT'
      FieldName = 'L_ORT'
      FieldLength = 40
      DisplayWidth = 40
      Position = 25
    end
    object KundendatenppField27: TppField
      FieldAlias = 'L_POSTFACH'
      FieldName = 'L_POSTFACH'
      FieldLength = 40
      DisplayWidth = 40
      Position = 26
    end
    object KundendatenppField28: TppField
      FieldAlias = 'L_PF_PLZ'
      FieldName = 'L_PF_PLZ'
      FieldLength = 10
      DisplayWidth = 10
      Position = 27
    end
    object KundendatenppField29: TppField
      FieldAlias = 'GRUPPE'
      FieldName = 'GRUPPE'
      FieldLength = 4
      DisplayWidth = 4
      Position = 28
    end
    object KundendatenppField30: TppField
      FieldAlias = 'TELE1'
      FieldName = 'TELE1'
      FieldLength = 10
      DisplayWidth = 10
      Position = 29
    end
    object KundendatenppField31: TppField
      FieldAlias = 'TELE2'
      FieldName = 'TELE2'
      FieldLength = 10
      DisplayWidth = 10
      Position = 30
    end
    object KundendatenppField32: TppField
      FieldAlias = 'FAX'
      FieldName = 'FAX'
      FieldLength = 10
      DisplayWidth = 10
      Position = 31
    end
    object KundendatenppField33: TppField
      FieldAlias = 'FUNK'
      FieldName = 'FUNK'
      FieldLength = 25
      DisplayWidth = 25
      Position = 32
    end
    object KundendatenppField34: TppField
      FieldAlias = 'EMAIL'
      FieldName = 'EMAIL'
      FieldLength = 50
      DisplayWidth = 50
      Position = 33
    end
    object KundendatenppField35: TppField
      FieldAlias = 'INTERNET'
      FieldName = 'INTERNET'
      FieldLength = 50
      DisplayWidth = 50
      Position = 34
    end
    object KundendatenppField36: TppField
      FieldAlias = 'DIVERSES'
      FieldName = 'DIVERSES'
      FieldLength = 50
      DisplayWidth = 50
      Position = 35
    end
    object KundendatenppField37: TppField
      FieldAlias = 'BRIEFANREDE'
      FieldName = 'BRIEFANREDE'
      FieldLength = 100
      DisplayWidth = 100
      Position = 36
    end
    object KundendatenppField38: TppField
      FieldAlias = 'BLZ'
      FieldName = 'BLZ'
      FieldLength = 20
      DisplayWidth = 20
      Position = 37
    end
    object KundendatenppField39: TppField
      FieldAlias = 'KTO'
      FieldName = 'KTO'
      FieldLength = 20
      DisplayWidth = 20
      Position = 38
    end
    object KundendatenppField40: TppField
      FieldAlias = 'BANK'
      FieldName = 'BANK'
      FieldLength = 40
      DisplayWidth = 40
      Position = 39
    end
    object KundendatenppField41: TppField
      Alignment = taRightJustify
      FieldAlias = 'DEB_NUM'
      FieldName = 'DEB_NUM'
      FieldLength = 0
      DataType = dtInteger
      DisplayWidth = 10
      Position = 40
    end
    object KundendatenppField42: TppField
      Alignment = taRightJustify
      FieldAlias = 'KRD_NUM'
      FieldName = 'KRD_NUM'
      FieldLength = 0
      DataType = dtInteger
      DisplayWidth = 10
      Position = 41
    end
    object KundendatenppField43: TppField
      Alignment = taRightJustify
      FieldAlias = 'STATUS'
      FieldName = 'STATUS'
      FieldLength = 0
      DataType = dtInteger
      DisplayWidth = 10
      Position = 42
    end
    object KundendatenppField44: TppField
      Alignment = taRightJustify
      FieldAlias = 'NET_SKONTO'
      FieldName = 'NET_SKONTO'
      FieldLength = 0
      DataType = dtDouble
      DisplayWidth = 10
      Position = 43
    end
    object KundendatenppField45: TppField
      Alignment = taRightJustify
      FieldAlias = 'NET_TAGE'
      FieldName = 'NET_TAGE'
      FieldLength = 0
      DataType = dtInteger
      DisplayWidth = 10
      Position = 44
    end
    object KundendatenppField46: TppField
      Alignment = taRightJustify
      FieldAlias = 'BRT_TAGE'
      FieldName = 'BRT_TAGE'
      FieldLength = 0
      DataType = dtInteger
      DisplayWidth = 10
      Position = 45
    end
    object KundendatenppField47: TppField
      FieldAlias = 'WAERUNG'
      FieldName = 'WAERUNG'
      FieldLength = 5
      DisplayWidth = 5
      Position = 46
    end
    object KundendatenppField48: TppField
      FieldAlias = 'UST_NUM'
      FieldName = 'UST_NUM'
      FieldLength = 25
      DisplayWidth = 25
      Position = 47
    end
    object KundendatenppField49: TppField
      Alignment = taRightJustify
      FieldAlias = 'VERTRETER_ID'
      FieldName = 'VERTRETER_ID'
      FieldLength = 0
      DataType = dtInteger
      DisplayWidth = 10
      Position = 48
    end
    object KundendatenppField50: TppField
      FieldAlias = 'INFO'
      FieldName = 'INFO'
      FieldLength = 0
      DataType = dtMemo
      DisplayWidth = 10
      Position = 49
      Searchable = False
      Sortable = False
    end
    object KundendatenppField51: TppField
      Alignment = taRightJustify
      FieldAlias = 'GRABATT'
      FieldName = 'GRABATT'
      FieldLength = 0
      DataType = dtDouble
      DisplayWidth = 10
      Position = 50
    end
    object KundendatenppField52: TppField
      Alignment = taRightJustify
      FieldAlias = 'KUN_KRDLIMIT'
      FieldName = 'KUN_KRDLIMIT'
      FieldLength = 0
      DataType = dtDouble
      DisplayWidth = 10
      Position = 51
    end
    object KundendatenppField53: TppField
      Alignment = taRightJustify
      FieldAlias = 'KUN_LIEFART'
      FieldName = 'KUN_LIEFART'
      FieldLength = 0
      DataType = dtInteger
      DisplayWidth = 10
      Position = 52
    end
    object KundendatenppField54: TppField
      Alignment = taRightJustify
      FieldAlias = 'KUN_ZAHLART'
      FieldName = 'KUN_ZAHLART'
      FieldLength = 0
      DataType = dtInteger
      DisplayWidth = 10
      Position = 53
    end
    object KundendatenppField55: TppField
      Alignment = taRightJustify
      FieldAlias = 'KUN_PRLISTE'
      FieldName = 'KUN_PRLISTE'
      FieldLength = 0
      DataType = dtInteger
      DisplayWidth = 10
      Position = 54
    end
    object KundendatenppField56: TppField
      Alignment = taRightJustify
      FieldAlias = 'LIEF_LIEFART'
      FieldName = 'LIEF_LIEFART'
      FieldLength = 0
      DataType = dtInteger
      DisplayWidth = 10
      Position = 55
    end
    object KundendatenppField57: TppField
      Alignment = taRightJustify
      FieldAlias = 'LIEF_ZAHLART'
      FieldName = 'LIEF_ZAHLART'
      FieldLength = 0
      DataType = dtInteger
      DisplayWidth = 10
      Position = 56
    end
    object KundendatenppField58: TppField
      Alignment = taRightJustify
      FieldAlias = 'LIEF_PRLISTE'
      FieldName = 'LIEF_PRLISTE'
      FieldLength = 0
      DataType = dtInteger
      DisplayWidth = 10
      Position = 57
    end
    object KundendatenppField59: TppField
      Alignment = taRightJustify
      FieldAlias = 'PR_EBENE'
      FieldName = 'PR_EBENE'
      FieldLength = 0
      DataType = dtInteger
      DisplayWidth = 10
      Position = 58
    end
    object KundendatenppField60: TppField
      FieldAlias = 'BRUTTO_FLAG'
      FieldName = 'BRUTTO_FLAG'
      FieldLength = 1
      DisplayWidth = 1
      Position = 59
    end
    object KundendatenppField61: TppField
      FieldAlias = 'MWST_FREI_FLAG'
      FieldName = 'MWST_FREI_FLAG'
      FieldLength = 1
      DisplayWidth = 1
      Position = 60
    end
    object KundendatenppField62: TppField
      FieldAlias = 'KUN_SEIT'
      FieldName = 'KUN_SEIT'
      FieldLength = 0
      DataType = dtDate
      DisplayWidth = 10
      Position = 61
    end
    object KundendatenppField63: TppField
      FieldAlias = 'KUN_GEBDATUM'
      FieldName = 'KUN_GEBDATUM'
      FieldLength = 0
      DataType = dtDate
      DisplayWidth = 10
      Position = 62
    end
    object KundendatenppField64: TppField
      Alignment = taRightJustify
      FieldAlias = 'ENTFERNUNG'
      FieldName = 'ENTFERNUNG'
      FieldLength = 0
      DataType = dtInteger
      DisplayWidth = 10
      Position = 63
    end
    object KundendatenppField65: TppField
      FieldAlias = 'ERSTELLT'
      FieldName = 'ERSTELLT'
      FieldLength = 0
      DataType = dtDate
      DisplayWidth = 10
      Position = 64
    end
    object KundendatenppField66: TppField
      FieldAlias = 'ERST_NAME'
      FieldName = 'ERST_NAME'
      FieldLength = 20
      DisplayWidth = 20
      Position = 65
    end
    object KundendatenppField67: TppField
      FieldAlias = 'GEAEND'
      FieldName = 'GEAEND'
      FieldLength = 0
      DataType = dtDate
      DisplayWidth = 10
      Position = 66
    end
    object KundendatenppField68: TppField
      FieldAlias = 'GEAEND_NAME'
      FieldName = 'GEAEND_NAME'
      FieldLength = 20
      DisplayWidth = 20
      Position = 67
    end
    object KundendatenppField69: TppField
      FieldAlias = 'SHOP_KUNDE'
      FieldName = 'SHOP_KUNDE'
      FieldLength = 1
      DisplayWidth = 1
      Position = 68
    end
    object KundendatenppField70: TppField
      Alignment = taRightJustify
      FieldAlias = 'SHOP_ID'
      FieldName = 'SHOP_ID'
      FieldLength = 0
      DataType = dtInteger
      DisplayWidth = 10
      Position = 69
    end
  end
  object ReportQuery: TZMySqlQuery
    Database = DM1.DB1
    Transaction = DM1.Transact1
    CachedUpdates = False
    ShowRecordTypes = [ztModified, ztInserted, ztUnmodified]
    Options = [doHourGlass, doAutoFillDefs, doUseRowId]
    LinkOptions = [loAlwaysResync]
    Constraints = <>
    BeforePost = ReportQueryBeforePost
    ExtraOptions = [moStoreResult]
    Macros = <>
    Sql.Strings = (
      'SELECT * FROM REGISTERY WHERE MAINKEY= '#39'MAIN\\REPORT\\WARTUNG'#39
      '')
    RequestLive = True
    Left = 136
    Top = 168
  end
end
