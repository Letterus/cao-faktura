{

CAO-Faktura f�r Windows Version 1.0
Copyright (C) 2003 Jan Pokrandt / Jan@JP-SOFT.de

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.


******* CAO-Faktura comes with ABSOLUTELY NO WARRANTY ***************
}

{

Programm     : CAO-Kasse
Modul        : Projektmodul
Stand        : 11.04.2004
Version      : 1.2.5.1

}


{$I CAO32.INC}
program cao_kasse;

uses
  Forms,
  Dialogs,
  cao_kasse_main in 'cao_kasse_main.pas' {KMainForm},
  CAO_DM in 'CAO_DM.pas' {DM1: TDataModule},
  cao_mandantaw in 'cao_mandantaw.pas' {MandantAWForm},
  CAO_Logging in 'CAO_Logging.pas' {LogForm},
  cao_kasse_bonfertig in 'cao_kasse_bonfertig.pas' {BonFertigForm},
  cao_var_const in 'cao_var_const.pas',
  cao_progress in 'cao_progress.pas' {ProgressForm},
  CAO_KASSE_ARTIKEL in 'CAO_KASSE_ARTIKEL.pas' {KasseArtikelForm},
  cao_dbgrid_layout in 'cao_dbgrid_layout.pas' {VisibleSpaltenForm},
  CAO_KASSE_PRINT in 'CAO_KASSE_PRINT.pas' {PrintBonForm},
  cao_sn_auswahl in 'cao_sn_auswahl.pas' {SNAuswahlForm},
  CAO_KASSE_SETUP in 'CAO_KASSE_SETUP.pas' {KasseSetupForm};

{$R *.RES}

begin
  Application.Initialize;
  Application.Name :='cao_kasse';
  Application.Title := 'CAO-Kasse 1.2 Stable';
  Application.CreateForm(TKMainForm, KMainForm);
  Application.CreateForm(TProgressForm, ProgressForm);
  Application.CreateForm(TLogForm, LogForm);
  Application.CreateForm(TBonFertigForm, BonFertigForm);
  Application.CreateForm(TKasseArtikelForm, KasseArtikelForm);
  Application.CreateForm(TVisibleSpaltenForm, VisibleSpaltenForm);
  Application.CreateForm(TDM1, DM1);
  Application.CreateForm(TPrintBonForm, PrintBonForm);
  Application.CreateForm(TSNAuswahlForm, SNAuswahlForm);
  Application.CreateForm(TKasseSetupForm, KasseSetupForm);
  Application.Run;
end.
