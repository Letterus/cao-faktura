{

CAO-Faktura f�r Windows Version 1.0
Copyright (C) 2003 Jan Pokrandt / Jan@JP-SOFT.de

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.


******* CAO-Faktura comes with ABSOLUTELY NO WARRANTY ***************

Programm     : CAO-Faktura
Modul        : CAO_TOOL1
Stand        : 26.10.2003
Version      : 1.1.3.24
Beschreibung : 2 Funktionen, die von den Journalen zur Ermittlung des
               Klartext-Status (Stadium) verwendet werden

History      :

13.01.2003 - Version 1.0.0.48 released Jan Pokrandt
17.01.2002 - Glob. Variable IsNT + Betriebssystemerkennung eingebaut
10.05.2003 - Funktion GetEKBestStatus hinzugef�gt
29.07.2003 - neue Zahlungsarten hinzugef�gt
26.10.2003 - neue Funktion CAO_ROUND hinzugef�gt, die richtig rundet ;-)
             ab 0,5 wird aufgerundet (Kaufm�nnisch)
15.03.2004 - Diverse Datumsfunktionen hinzugef�gt
13.04.2004 - Funktionen zur Pr�fung von EMail eingebunden (von LEXA)
           - Funktion zur umwandlung von bel. Text in Dateinamen eingebunden
             (von LEXA)
}

//------------------------------------------------------------------------------
unit CAO_Tool1;
//------------------------------------------------------------------------------
interface

{$R-}

uses Windows;
//------------------------------------------------------------------------------
FUNCTION GetRechStatus(NumStatus:Byte;Quelle:Integer) : String;
FUNCTION GetLiefStatus(NumStatus:Byte; Print : Boolean) : String;
FUNCTION GetEKBestStatus(NumStatus:Byte) : String;
function CAO_ROUND (Val : Extended): Int64;
function tapiRequestMakeCall( lpszDestAddress,
                              lpszAppName,
                              lpszCalledParty,
                              lpszComment: LPCSTR): DWORD; stdcall;

function Kalenderwoche(Datum:TDateTime) : byte;
function FirstDayOfWeek (D : tDateTime) : tDateTime; // liefert Mo der Woche
function LastDayOfWeek  (D : tDateTime) : tDateTime; // liefert So der Woche

function StringToFilename(StringToConvert: String; MaxLength: integer): String;

var IsNT : boolean;  // True, Wenn Windown NT, 2000 oder XP
//------------------------------------------------------------------------------
implementation

uses cao_var_const, sysutils;
//------------------------------------------------------------------------------
function tapiRequestMakeCall; external 'TAPI32.DLL' name 'tapiRequestMakeCall';
//------------------------------------------------------------------------------
function CAO_ROUND (Val : Extended): Int64;
begin
     if Val>0
      then Result :=Trunc(Trunc((Val+0.5)*100)/100)
      else Result :=Trunc(Trunc((Val-0.5)*100)/100);
end;
//------------------------------------------------------------------------------
FUNCTION GetLiefStatus(NumStatus:Byte; Print : Boolean) : String;
var status : string[20];
begin
     if print then
     begin
       case NumStatus of
         00..19 : status :='  bearbeiten  ';
         20..79 : status :=' LS gedruckt  ';
         80..99 : status :='LS+RE gedruckt';
         else     status :='- UNBEKANNT - ';
       end;
     end
        else
     begin
       case NumStatus of
         00..19 : status :='  bearbeiten  ';
         20..99 : status :='nicht gedruckt';
         else     status :='- UNBEKANNT - ';
       end;
     end;
     Result :=Status;
end;
//------------------------------------------------------------------------------
FUNCTION GetEKBestStatus(NumStatus:Byte) : String;
var status : string[20];
begin
     case NumStatus of
       00..19 : status :='bearbeiten';
       20     : status :='offen';
       30     : status :='Teillieferung';
       100    : status :='voll geliefert';
       101    : status :='nicht lieferbar';
       else     status :='- UNBEKANNT - ';
     end;
     Result :=Status;
end;
//------------------------------------------------------------------------------
FUNCTION GetRechStatus(NumStatus:Byte;Quelle:Integer) : String;
var status : string[20];
begin
     case NumStatus of
      00..09 : status :='editieren       ';
      10..19 : status :='Liefersch. gedr.';
      20     : status :='Vorkasse offen  ';
      21..23 : status :='-> o f f e n <- ';
      24     : status :='Nachnahme offen ';
      25     : status :='-> o f f e n <- ';
      26     : status :='EC-KARTE        ';
      27     : status :='Kreditkarte     ';
      28     : status :='PayPal          ';
      29     : status :='off. Lastschrift';
      30..39 : status :='1x gemahnt      ';
      40..49 : status :='2x gemahnt      ';
      50..59 : status :='3x gemahnt      ';
      60..69 : status :='INKASSO ! ';
      70..79 : status :='Teilzahlung';

      80     : status :='Vorkasse m. Sk. ';
      81     : status :='BAR mit Skonto  ';
      82     : status :='�berw. m. Skonto';
      83     : status :='Post NN m. Sk.  ';
      84     : status :='UPS NN m. Sk.   ';
      85     : status :='Scheck m. Skonto';
      86     : status :='EC-Karte m. Sk. ';
      87     : status :='Kreditk. m. Sk  ';
      88     : status :='PayPal          ';
      89     : status :='Lastschr. m. Sk.';

      90     : status :='Vorkasse        ';
      91     : status :='BAR erhalten    ';
      92     : status :='�berweisung BANK';
      93     : status :='Post Nachnahme  ';
      94     : status :='UPS Nachnahme   ';
      95     : status :='Scheck erhalten ';
      96     : status :='EC-KARTE        ';
      97     : status :='Kreditkarte     ';
      98     : status :='PayPal          ';
      99     : status :='Lastschrift     ';
      110    : if Quelle = VK_RECH
                then status :='Lastschrift'
                else status :='Angewiesen';
      else     status :='-  UNBEKANNT  - ';
     end;
     Result :=Status;
end;
//------------------------------------------------------------------------------
{
*** Bemerkungen zur Berechnung der Wochennummer nach DIN 1355 ***
Der Montag ist der erste Tag der Woche
Eine Woche geh�rt zu demjenigen Kalenderjahr,
in dem 3 oder mehr Tage der Woche liegen.
Der Donnerstag einer Woche liegt immer in demjenigen Kalenderjahr,
dem die Woche zugerechnet wird.
Der 4. Januar liegt immer in der ersten Kalenderwoche.
Der 28. Dezember liegt immer in der letzten Kalenderwoche. }

//Quelle : http://www.delphi-fundgrube.de/faq04.htm

function Kalenderwoche(Datum:TDateTime):byte;
var
  AYear,
  dummy : word;
  First : TDateTime;
begin
  DecodeDate(Datum+((8-DayOfWeek(Datum)) mod 7)-3, AYear, dummy, dummy);
  First := EncodeDate(AYear, 1, 1);
  Result:=(trunc(Datum-First-3+(DayOfWeek(First)+1) mod 7) div 7)+1;
end;

//------------------------------------------------------------------------------
function FirstDayOfWeek (D : tDateTime) : tDateTime; // liefert Mo der Woche
var WT : Integer;
begin
   D :=Int(D);  // nur Datum ohne Zeit
   WT :=DayOfWeek (D); //Wochentag ermitteln 1=So, 2=Mo ...
   if WT=1 then WT :=8; // Montag bleibt 2, Sonntag = 8
   dec(WT,2); // 0=Montag, 1= Dienstag u.s.w.

   if WT>0 then D :=D-WT; // Wenn akt. Tag > Montag, dann x Tage abziehen um
                          // den Montag zu erhalten
   Result :=D;
end;
//------------------------------------------------------------------------------
function LastDayOfWeek  (D : tDateTime) : tDateTime; // liefert So der Woche
var WT : Integer;
begin
   D :=Int(D);  // nur Datum ohne Zeit
   WT :=DayOfWeek (D); //Wochentag ermitteln 1=So, 2=Mo ...
   if WT=1 then WT :=8; // Montag bleibt 2, Sonntag = 8
   dec(WT,2); // 0=Montag, 1= Dienstag u.s.w.

   if WT<6 then D :=D+6-WT; // Wenn akt. Tag < Sonntag, dann x Tage
                            // hinzuaddieren um den Sonntag zu erhalten
   Result :=D;
end;
//------------------------------------------------------------------------------



function StringToFilename(StringToConvert: String; MaxLength: integer): String;
{*** function by lexa for CAO-Faktura MailClient, 31.3.2004 ***}
//
// Wandelt einen beliebigen String in einen g�ltigen Windows-Dateinamen um
// Der Name wird aus 'StringToConvert' generiert.
//
// * Pr�fkriterien f�r 'StringToConvert':
//   - ung�ltige Zeichen werden ersetzt durch DefaultReplaceChar
//     Es werden auch Sonderzeichen abgefangen, z.B. ALT+0160 auf Num-Block
//   - Punkte, Kommas, Leerzeichen usw. am Anfang und Ende werden entfernt (TrimString)
//   - Falsche Hochkommas sind in Dateinamen prinzipiell g�ltig, diese werden aber
//     trotzdem ersetzt durch normales Hochkomma -> ' (f�r Samba gut???)
//   - Der String wird bei MaxLength abgeschnitten.
//       : Ist MaxLength > MaxAllowedLength wird MaxLength auf MaxAllowedLength gesetzt
//       : Ist MaxLength ung�ltig (kleiner 1), wird MaxLength auf MaxAllowedLength gesetzt
//   - Ist der generierte Name ein Windows Ger�tename, wird er ge�ndert (con, aux, ...)
//
// * R�ckgabewerte
//   - Ein Kennzeichner (Marker) kann optional vorangestellt werden: 'CAO-MC ' + ...
//   - OKAY: Ist der R�ckgabewert <> '', wurde ein g�ltiger Dateiname generiert
//   - ERROR: Ist der R�ckgabewert = '', konnte aus folgenden Gr�nden kein Name generiert werden:
//       : der String war von Anfang an leer, oder
//       : der String enthielt ausschlie�lich ung�ltige Zeichen und ist nach dem Filtern leer
//   - Die Datei-Erweiterung wird NICHT geliefert und muss au�erhalb angeh�ngt werden (.html etc.)
// -----------------------------------------------------------------------------

const
   // g�ltige Zeichen f. einen Dateinamen. Bei Bedarf solche nachr�sten: �, � ... etc.
   // Euro Zeichen '�' rausgenommen, kann Probleme auf WinNT machen (??)
   // Nach Test wieder einf�gen (NT mit SP5 sollte jeder haben)
   ValidChars: set of char = ['A'..'Z', 'a'..'z', '0'..'9',
                              '�', '�', '�', '�', '�', '�', '�',
                              '_', '-', '+', ',', ' ', '~', '#',
                              '(', ')', '=', '[', ']', '&', '%',
                              ';', '$', '!', '@', '�', '^',
                              '{', '}', '.'];

   // ung�ltige Zeichen werden durch dieses ersetzt.
   // Optional �nderbar, ABER es darf NICHT in CharsToDel enthalten sein!!!
   DefaultReplaceChar = '_';

   // diese Zeichen werden am Anfang u. Ende des Strings ersatzlos gel�scht
   CharsToDel: set of char = [' ', '.', ',', ';'];

   // max. L�nge des Dateinamens
   // Windows FAT : max. 255 Zeichen Dateiname inkl. Pfad
   // Windows NTFS: max. 255 Zeichen Dateiname, Pfad unbegrenzt
   MaxAllowedLength = 45; // Zeichen

   // sicherheitshalber Ger�tenamen abfangen!
   InvalidFilenames: array[1..3] of String = ('nul', 'aux', 'con');

   // Optional. Wird jedem generierten Dateinamen vorangesetzt, kann auch leer sein
   // und ist dann wirkungslos. Kennzeichnet den Ursprung der Datei
   CAO_Marker: String = 'CAO-MC ';

var
   i: integer;

   { Sub-Function }
   function StringIsConvertable(s: String): boolean;
   // ist ein konvertierbarer String vorhanden? d.h. <> '' und nach L�schen f�hrender
   // u. abschlie�ender Zeichen verbleibt mindestens 1 g�ltiges oder ung�ltiges Zeichen
   var
      b: boolean;
      i: integer;
   begin
      if (length(s) = 0) then                // der String ist leer
      begin
         b := false;
      end
       else
      begin
         b := true;
         for i := 1 to length(s) do          // ist mindestens 1 g�ltiges Zeichen drin?
         begin
            if not (s[i] in CharsToDel) then break;
         end;
         if i > length(s) then
            b := false;                      // enth�lt kein g�ltiges Zeichen
      end;
      StringIsConvertable := b;
   end;

   { Sub-Function }
   function TrimString(s: String): String;
   // Punkte, Kommas, Leerzeichen usw. (CharsToDel) am Anfang u. Ende entfernen
   // Der String kann am Ende leer sein, darf aber nicht leer reinkommen!
   begin
      while (length(s) > 0) and (s[1] in CharsToDel) do          // f�hrende CharsToDel entfernen
         s := copy(s, 2, length(s));
      while (length(s) > 0) and (s[length(s)] in CharsToDel) do // abschlie�ende CharsToDel entfernen
         s := copy(s, 1, length(s) - 1);
      TrimString := s;
   end;

   { Sub-Function }
   function ReplaceWrongApostrophes(s: String): String;
   // ersetzt 'falsche' Hochkommas (st�rt evtl. auf Samba Shares)
   var
      i: integer;
   const
      WrongApostrophes: set of char = ['�', '`'];
      GoodApostrophe = '''';   // normales Hochkomma -> '
   begin
      for i := 1 to length(s) do if s[i] in WrongApostrophes then
         s[i] := GoodApostrophe;
      ReplaceWrongApostrophes := s;
   end;

{ Main-Function }
begin
   // ist StringToConvert leer bzw. �berhaupt konvertierbar?
   if not StringIsConvertable(StringToConvert) then
   begin
      StringToFilename := '';
      exit;
   end;

   // String trimmen (f�hrende/abschlie�ende Leerzeichen etc)
   StringToConvert := TrimString(StringToConvert);

   // ist StringToConvert nach dem Trimmen konvertierbar oder leer?
   if not StringIsConvertable(StringToConvert) then
   begin
      StringToFilename := '';
      exit;
   end;

   // MaxLength (gew�nschte maximale Stringl�nge) pr�fen bzw. korrigieren
   if (MaxLength > MaxAllowedLength) or (MaxLength < 1) then
      MaxLength := MaxAllowedLength;

   // 'falsche' Hochkommas ersetzen
   StringToConvert := ReplaceWrongApostrophes(StringToConvert);

   // CAO_Marker vor den Betreff setzen (Dateiursprung kennzeichnen)
   // Der Marker kann leer sein und hat dann keine Funktion
   StringToConvert := CAO_Marker + StringToConvert;

   // String auf MaxLength abschneiden
   StringToConvert := copy(StringToConvert, 1, MaxLength);

   // ung�ltige Dateinamen-Zeichen im verbliebenen String ersetzen
   for i := 1 to length(StringToConvert) do
   begin
      if not (char(StringToConvert[i]) in ValidChars) then
         StringToConvert[i] := DefaultReplaceChar;
   end;

   // reservierte Ger�tenamen abfangen, falls Windows das mal vergi�t ;-)
   // Ist nicht n�tig, wenn der Marker davorsteht
   if (CAO_Marker = '') then
   begin
      for i := low(InvalidFilenames) to high(InvalidFilenames) do
      begin
         if lowercase(InvalidFilenames[i]) = lowercase(StringToConvert) then
            StringToConvert := DefaultReplaceChar + StringToConvert;
      end;
   end;

   // hier ist StringToConvert niemals leer !!
   StringToFilename := StringToConvert;

end; { Main-Function }


//------------------------------------------------------------------------------
//     *** function by lexa for CAO-Faktura MailClient, 04.04.2004 ***
//------------------------------------------------------------------------------
function ValidateEMailAdresse(Adresse: string): boolean;

// Lockere Pr�fung eines Strings anhand RFC #822 auf einige Merkmale einer
// Mailadresse mit Dom�nenpart oder IP-Adresse
// Quellen:
//  - RFC #733 (veraltet), RFC #822 (ersetzt RFC #733, Format von Internet-Mails
//  - RFC #822: die Adresse darf Leerzeichen enthalten,
//              wenn gequotet: "Hans Wurst"@foo.bar
//              (Relikt aus RFC #733, deshalb hier 'verboten')
//  - RFC #733: der lokale Part muss keinen Dom�nenpart enthalten, "ich@daheim"
//              ist nach RFC #733  g�ltig, jedoch nicht nach RFC #822 und
//              deshalb hier 'verboten' }
//
// Return: FALSE: der String kann keine g�ltige Mailadresse sein
//         TRUE : der String ist als Mailadresse zul�ssig
//                (blah@da.heim, foo@192.168.1.1)
const
   Punkt = '.';
   At = '@';
   ValidChars: set of char =[At, 'A'..'Z', '0'..'9', Punkt, '-', '_', '[', ']'];

var
   s: String;
   b: boolean;
   i: integer;

begin
      b := true;
      Adresse := uppercase(Adresse);

      // String hat zu wenig Zeichen oder enth�lt nicht '@'
      if (length(Adresse) <= 4) or (pos(At, Adresse) = 0) then
         b := false;

      // Dom�nenpart oder IP Adresse vorhanden?
      //(mind. ein '.' MUSS rechts vom '@' enthalten sein)
      if b then
      begin
         s := copy( Adresse,
                    pos(At, Adresse) + 1,
                    length(Adresse) - pos(At, Adresse) + 1);

         if (pos(Punkt, s) = 0) or (s[1] = Punkt) or (s[length(s)] = Punkt)
          then b := false;
      end;

      // ung�ltige Zeichen enthalten?
      if b then
      begin
         for i := 1 to length(Adresse) do
         begin
            if not (Adresse[i] in ValidChars) then
            begin
               b := false;
               break;
            end; {if}
         end; {for }
      end;

      ValidateEMailAdresse := b;
end;
//------------------------------------------------------------------------------



























//------------------------------------------------------------------------------
// Unit-Init
//------------------------------------------------------------------------------
var JvVerInf : TOSVersionInfo;
begin
   IsNT:=false;
   JvVerInf.dwOSVersionInfoSize:=sizeof(JvVerInf);
   if (GetVersionEx(JvVerInf))
    then IsNT:=JvVerInf.dwPlatformId=VER_PLATFORM_WIN32_NT;
end.
//------------------------------------------------------------------------------



{

// Quelle : http://www.delphi-fundgrube.de/files/feiertg.txt

const
  Feiertage : array [1..19] of string[25] =
   ('Neujahr','Maifeiertag','Tag der deutschen Einheit','Allerheiligen',
    'Totensonntag','Volkstrauertag','1. Weihnachtstag','2. Weihnachtstag',
    'Karfreitag','Ostersonntag','Ostermontag','Christi Himmelfahrt',
    'Pfingstsonntag','Pfingstmontag','Fronleichnam','Heilige 3 K�nige',
    'Mari� Himmelfahrt','Reformationstag','Bu�- und Bettag');
  Sondertage : array [1..24] of string[25] =
   ('Mari� Lichtme�','Valentinstag','Weiberfastnacht','Rosenmontag','Fastnacht',
    'Aschermittwoch','Mari� Verk�ndigung','Palmsonntag','Gr�ndonnerstag','Muttertag',
    'Peter und Paul','Mari� Geburt','Erntedankfest','Mari� Empf�ngnis','Silvester',
    '1. Advent','2. Advent','3. Advent','4. Advent','Heiligabend','Fr�hlingsanfang',
    'Sommmeranfang','Herbstanfang','Winteranfang');

var
  Feiertag      : array [1..365] of ShortInt;

function TagImJahr(Datum:TDateTime):word;
var T,M,J  : word;
    Erster : TDateTime;
begin
  try
    DecodeDate(Datum,J,M,T);
    Erster:=EncodeDate(J,1,1);
    Result:=trunc(Datum-Erster+1);
  except
    Result:=0;
  end;
end;

procedure FeiertageBerechnen(Y:word);
var D,dw,OM,aw : word;
    Dat        : TDateTime;
    Ostern     : TDateTime;
    Weihnacht  : TDateTime;

Function OsterSonntag(Y:word):TDateTime;
var a,b,c,d,e,tag,monat : integer;
begin
  a:=y MOD 19 ;
  b:=y MOD 4;
  c:=y MOD 7;
  d:=(19*a+24) MOD 30;
  e:=(2*b+4*c+6*d+5) MOD 7;
  Tag:=22+d+e;
  monat:=3;
  IF Tag>31 then begin
    tag:=d+e-9;
    monat:=4;
  end;
  IF (tag=26) AND (monat=4) then
    tag:=19;
  IF (tag=25) AND (monat=4) AND (d=28) AND (e=6) AND (a>10) then
    tag:=18;
  try
    Result:= EncodeDate(y,monat,tag);
  except
    Result:=0;
  end;
end;

begin
  for D:=1 to 365 do
    Feiertag[D]:=0;
  Ostern:=OsterSonntag(Y);
  try
    DecodeDate(Ostern,Y,OM,D);
  except
    OM:=4;
  end;
  try
    Weihnacht:=EncodeDate(Y,12,25);
    if (DayOfWeek(Weihnacht)-1)=0 then
      dw:=7
    else
      dw:=DayOfWeek(Weihnacht)-1;
  except
    Weihnacht:=-1;
    dw:=0;
  end;
  //Mari� Lichtme�                    Sondertage
  Dat:=Encodedate(Y,2,2);
  Feiertag[TagImJahr(Dat)]:=-1;
  //Valentinstag
  Dat:=Encodedate(Y,2,14);
  Feiertag[TagImJahr(Dat)]:=-2;
  //Weiberfastnacht
  Dat:=Ostern-45;
  while DayOfWeek(Dat)<>2 do
    Dat:=Dat-1;
  Feiertag[TagImJahr(Dat)-4]:=-3;
  //Rosenmontag
  Feiertag[TagImJahr(Dat)]:=-4;
  //Fastnacht
  Feiertag[TagImJahr(Dat)+1]:=-5;
  //Aschermittwoch
  Feiertag[TagImJahr(Dat)+2]:=-6;
  //Mari� Verk�ndigung
  Dat:=Encodedate(Y,3,25);
  Feiertag[TagImJahr(Dat)]:=-7;
  //Palmsonntag
  Feiertag[TagImJahr(Ostern)-7]:=-8;
  //Gr�ndonnerstag
  Feiertag[TagImJahr(Ostern)-3]:=-9;
  //Muttertag
  Dat:=EncodeDate(y,4,30);
  aw:=DayOfWeek(Dat)-1;
  Dat:=Dat-aw+14;
  if Dat=(Ostern+49) then
    Dat:=Dat-7;
  Feiertag[TagImJahr(Dat)]:=-10;
  {Peter und Paul
  Dat:=Encodedate(Y,6,29);
  Feiertag[TagImJahr(Dat)]:=-11;
  //Mari� Geburt
  Dat:=Encodedate(Y,9,8);
  Feiertag[TagImJahr(Dat)]:=-12;
  //Erntedankfest
  Dat:=Encodedate(Y,10,1);
  while DayOfWeek(Dat)<>1 do
    Dat:=Dat+1;
  Feiertag[TagImJahr(Dat)]:=-13;
  //Mari� Empf�ngnis
  Dat:=Encodedate(Y,12,8);
  Feiertag[TagImJahr(Dat)]:=-14;
  //Silvester
  Dat:=Encodedate(Y,12,31);
  Feiertag[TagImJahr(Dat)]:=-15;
  //1. Advent
  Dat:=Weihnacht;
  while DayOfWeek(Dat)<>1 do
    Dat:=Dat-1;
  Feiertag[TagImJahr(Dat)-21]:=-16;
  //2. Advent
  Feiertag[TagImJahr(Dat)-14]:=-17;
  //3. Advent
  Feiertag[TagImJahr(Dat)-7]:=-18;
  //4. Advent
  Feiertag[TagImJahr(Dat)]:=-19;
  //Heiligabend
  Feiertag[TagImJahr(Weihnacht)-1]:=-20;
  //Fr�hlingsanfang
  Dat:=Encodedate(Y,3,21);
  Feiertag[TagImJahr(Dat)]:=-21;
  //Sommmeranfang
  Dat:=Encodedate(Y,6,21);
  Feiertag[TagImJahr(Dat)]:=-22;
  //Herbstanfang
  Dat:=Encodedate(Y,9,23);
  Feiertag[TagImJahr(Dat)]:=-23;
  //Winteranfang
  Dat:=Encodedate(Y,12,22);
  Feiertag[TagImJahr(Dat)]:=-24;
  //Neujahr                         Feiertage
  Feiertag[1]:=1;
  //Maifeiertag
  Dat:=Encodedate(Y,5,1);
  Feiertag[TagImJahr(Dat)]:=2;
  //Tag der deutschen Einheit
  Dat:=Encodedate(Y,10,3);
  Feiertag[TagImJahr(Dat)]:=3;
  //Allerheiligen
  Dat:=Encodedate(Y,11,1);
  Feiertag[TagImJahr(Dat)]:=4;
  //Totensonntag
  if Weihnacht>=0 then
    Feiertag[TagImJahr(Weihnacht-dw-28)]:=5;
  //Bu�- und Bettag
  if Config.Land=12 then
    Feiertag[TagImJahr(Weihnacht-dw-32)]:=19;
  //Volkstrauertag
  if Weihnacht>=0 then
    Feiertag[TagImJahr(Weihnacht-dw-35)]:=6;
  //1. Weihnachtstag
  if Weihnacht>=0 then
    Feiertag[TagImJahr(Weihnacht)]:=7;
  //2. Weihnachtstag
  if Weihnacht>=0 then
    Feiertag[TagImJahr(Weihnacht)+1]:=8;
  //Karfreitag
  Feiertag[TagImJahr(Ostern)-2]:=9;
  //Ostersonntag
  Feiertag[TagImJahr(Ostern)]:=10;
  //Ostermontag
  Feiertag[TagImJahr(Ostern)+1]:=11;
  //Christi Himmelfahrt
  Feiertag[TagImJahr(Ostern)+39]:=12;
  //Pfingstsonntag
  Feiertag[TagImJahr(Ostern)+49]:=13;
  //Pfingstmontag
  Feiertag[TagImJahr(Ostern)+50]:=14;
  //Fronleichnam
  if (Config.Land<2) or ((Config.Land>=9) and (Config.Land<=12)) or (Config.Land=15) then
    Feiertag[TagImJahr(Ostern)+60]:=15;
  //Heilige 3 K�nige
  if (Config.Land=0) or (Config.Land=1) or (Config.Land=13) then
    Feiertag[6]:=16;
  //Mari� Himmelfahrt
  if (Config.Land=1) or (Config.Land=11) then begin
    Dat:=Encodedate(Y,8,15);
    Feiertag[TagImJahr(Dat)]:=17;
  end;
  //Reformationstag
  if (Config.Land=3) or (Config.Land=7) or (Config.Land=12) or (Config.Land=13) or (Config.Land=15) then begin
    Dat:=Encodedate(Y,10,31);
    Feiertag[TagImJahr(Dat)]:=18;
  end;
end;

Aufruf :

FeiertageBerechnen(Jahr);

Danach enth�lt die Variable Feiertag Werte kleiner 0 f�r Sondertage und Werte gr��er 0 f�r Feiertage.

z.B.:

FT:=Feiertag[TagImJahr(Date)];
if FT>0 then
  Label1.Caption:=Feiertage[FT];
if FT<0 then
  Label1.Caption:=Sondertage[abs(FT)];

Die Variable Config.Land kann folgende Werte annehmen:

0 : Baden-W�rttemberg
1 : Bayern
2 : Berlin
3 : Brandenburg
4 : Bremen
5 : Hanmburg
6 : Hessen
7 : Mecklenburg-Vorpommern
8 : Niedersachsen
9 : Nordrhein-Westfalen
10: Rheinland-Pfalz
11: Saarland
12: Sachsen
13: Sachsen-Anhalt
14: Schleswig-Holstein
15: Th�ringen



}
