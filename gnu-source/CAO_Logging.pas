{

CAO-Faktura f�r Windows Version 1.0
Copyright (C) 2003 Jan Pokrandt / Jan@JP-SOFT.de

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.


******* CAO-Faktura comes with ABSOLUTELY NO WARRANTY ***************
}

{

Programm     : CAO-Faktura
Modul        : CAO_LOGGING
Stand        : 09.04.2004
Version      : 1.2.5.1
Beschreibung :

- Loggt alle SQL-Querys zur Analyse
- die jeweils letzten 300 Zeilen werden gehalten, FIFO-Prinzip
- Darstellung des MySQl-Serverstatus
- Darstellung des MySQl-Server-Proze�liste

}

{ History :

13.01.2003 - Version 1.0.0.48 released Jan Pokrandt
25.02.2004 - Log kann jetzt ein- und ausgeschaltet werden (default=aus)
           - Anzahl der Logeintr�ge kann ge�ndert werden


}

unit CAO_Logging;

{$I CAO32.INC}

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, StdCtrls, ExtCtrls, Grids, DBGrids, Db, ZQuery, ZMySqlQuery,
  JvDBCtrl, CaoDBGrid, Mask, JvMaskEdit, JvSpin;

type
  TLogForm = class(TForm)
    StatusBar1: TStatusBar;
    PC1: TPageControl;
    LogTS: TTabSheet;
    SrvStatusTS: TTabSheet;
    SrvProzListTS: TTabSheet;
    Panel1: TPanel;
    Button1: TButton;
    Memo1: TMemo;
    SrvStatus: TZMySqlQuery;
    SrvStatusDS: TDataSource;
    SrvStatusGrid: TCaoDBGrid;
    SrvProzessGrid: TCaoDBGrid;
    SrvProzList: TZMySqlQuery;
    SrvProzDS: TDataSource;
    LogAktivCB: TCheckBox;
    LogMaxEntryEdit: TJvSpinEdit;
    Label1: TLabel;
    Label2: TLabel;
    SrvVarTS: TTabSheet;
    procedure Button1Click(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure PC1Change(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure LogMaxEntryEditChange(Sender: TObject);
    procedure LogAktivCBClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private-Deklarationen }
  public
    { Public-Deklarationen }

    LogEnabled : Boolean;
    LogMaxEntry : Integer;

    procedure addlog (t : string);
  end;

var
  LogForm: TLogForm;

implementation

{$R *.DFM}

uses CAO_DM;

//------------------------------------------------------------------------------
procedure TLogForm.FormCreate(Sender: TObject);
begin
     LogEnabled  :=True;
     LogMaxEntry :=300;
end;
//------------------------------------------------------------------------------
procedure TLogForm.Button1Click(Sender: TObject);
begin
     memo1.lines.clear;
end;
//------------------------------------------------------------------------------
procedure TLogForm.addlog (t : string);
begin
    if (not assigned(memo1)) or
       (not LogEnabled) then exit;
    try
     memo1.text :=memo1.text + #13#10 +
                  formatdatetime ('dd.mm.yy hh:mm:ss',now)+ #13#10 + t;
     while memo1.lines.count > LogMaxEntry do memo1.lines.Delete(0);
    except end;
end;
//------------------------------------------------------------------------------
procedure TLogForm.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
     SrvStatus.Close;
     SrvProzList.Close;

     DM1.WriteBooleanU ('','SQL_LOG_AKTIV',LogEnabled);
     DM1.WriteIntegerU ('','SQL_LOG_MAX',LogMaxEntry);
end;
//------------------------------------------------------------------------------
procedure TLogForm.PC1Change(Sender: TObject);
begin
     SrvStatus.Active :=False;
     if PC1.ActivePage=SrvStatusTS then
     begin
        SrvStatus.Sql.Text :='SHOW STATUS';
        SrvStatusGrid.Parent :=SrvStatusTS;
     end
        else
     if PC1.ActivePage=SrvVarTS then
     begin
        SrvStatus.Sql.Text :='SHOW VARIABLES';
        SrvStatusGrid.Parent :=SrvVarTS;
     end;

     SrvStatus.Active :=(PC1.ActivePage=SrvStatusTS)or(PC1.ActivePage=SrvVarTS);
     SrvProzList.Active :=PC1.ActivePage=SrvProzListTS;
end;
//------------------------------------------------------------------------------
procedure TLogForm.FormShow(Sender: TObject);
begin
     SrvStatusGrid.RowColor2  :=DM1.C2Color;
     SrvProzessGrid.RowColor2 :=DM1.C2Color;
     LogMaxEntryEdit.Value    :=LogMaxEntry;
     LogAktivCB.Checked       :=LogEnabled;

     PC1.ActivePage :=LogTS;
end;
//------------------------------------------------------------------------------
procedure TLogForm.LogMaxEntryEditChange(Sender: TObject);
begin
     LogMaxEntry :=Round(LogMaxEntryEdit.Value);
end;
//------------------------------------------------------------------------------
procedure TLogForm.LogAktivCBClick(Sender: TObject);
begin
     LogEnabled :=LogAktivCB.Checked;
end;
//------------------------------------------------------------------------------
end.
