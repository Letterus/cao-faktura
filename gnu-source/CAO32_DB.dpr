{CAO-Faktura f�r Windows Version 1.0Copyright (C) 2003 Jan Pokrandt / Jan@JP-SOFT.de

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.


******* CAO-Faktura comes with ABSOLUTELY NO WARRANTY ***************
}

{

Programm     : CAO-Faktura
Modul        : Projektmodul
Stand        : 16.05.2004
Version      : 1.2.5.4

}

{$I CAO32.INC}

{$DESCRIPTION 'freie Warenwirtschaft CAO-Faktura'}

program CAO32_DB;

uses
  Forms,
  Dialogs,
  Controls,
  CAO_MAIN in 'CAO_MAIN.pas' {MainForm},
  CAO_DM in 'CAO_DM.pas' {DM1: TDataModule},
  cao_var_const in 'cao_var_const.pas',
  CAO_Logging in 'CAO_Logging.pas' {LogForm},
  cao_dbgrid_layout in 'cao_dbgrid_layout.pas' {VisibleSpaltenForm},
  CAO_Tool1 in 'CAO_Tool1.pas',
  CAO_About in 'CAO_About.pas' {AboutBox},
  CAO_KUNDE in 'CAO_KUNDE.pas' {AdressForm},
  CAO_ARTIKEL1 in 'CAO_ARTIKEL1.pas' {ArtikelForm},
  CAO_KFZ1 in 'CAO_KFZ1.pas' {KFZForm},
  CAO_Info in 'CAO_Info.pas' {InfoForm},
  CAO_PLZ in 'CAO_PLZ.pas' {PLZForm},
  CAO_BLZ in 'CAO_BLZ.pas' {BLZForm},
  {$IFDEF REPORTBUILDER}
  CAO_PrintRech in 'CAO_PrintRech.pas' {PrintRechForm},
  {$ENDIF}
  CAO_FibuKassenbuch in 'CAO_FibuKassenbuch.pas' {KassenBuchForm},
  CAO_ManKassBuchungDlg in 'CAO_ManKassBuchungDlg.pas' {ManKasBuchForm},
  CAO_ZahlungseingangDlg in 'CAO_ZahlungseingangDlg.pas' {ZEForm},
  CAO_Journal_EKRE in 'CAO_Journal_EKRE.pas' {JournalEKREForm},
  CAO_Journal_VKKASSE in 'CAO_Journal_VKKASSE.pas' {JournalVKKasseForm},
  CAO_Journal_VKAGB in 'CAO_Journal_VKAGB.pas' {JournalVKAGBForm},
  CAO_Journal_VKLIEF in 'CAO_Journal_VKLIEF.pas' {JournalVKLiefForm},
  CAO_Journal_VKRE in 'CAO_Journal_VKRE.pas' {JournalVKREForm},
  CAO_Journal_EKBEST in 'CAO_Journal_EKBEST.pas' {JournalEKBestForm},
  CAO_MakeEKRech in 'CAO_MakeEKRech.pas' {MakeEKReForm},
  CAO_MakeVKRech in 'CAO_MakeVKRech.pas' {MakeVKReForm},
  CAO_MakeEKRech_FertigDlg in 'CAO_MakeEKRech_FertigDlg.pas' {EKRechFertigDlg},
  CAO_MakeEKBest in 'CAO_MakeEKBest.pas' {MakeEKBestForm},
  cao_sn_erfassen in 'cao_sn_erfassen.pas' {SNErfassenForm},
  cao_sn_auswahl in 'cao_sn_auswahl.pas' {SNAuswahlForm},
  CAO_Startup in 'CAO_StartUp.pas',
  CAO_MakeVertragRech in 'CAO_MakeVertragRech.pas' {MakeVertragReForm},
  CAO_Export in 'CAO_Export.pas' {ExportForm},
  CAO_Mitarbeiter in 'CAO_Mitarbeiter.pas' {MitarbeiterForm},
  cao_progress in 'cao_progress.pas' {ProgressForm},
  CAO_ShopImageUpload in 'CAO_ShopImageUpload.pas' {FileUpload},
  cao_teillieferdlg in 'cao_teillieferdlg.pas' {TeilLieferForm},
  CAO_Link in 'CAO_Link.pas' {LinkForm},
  cao_pim in 'cao_pim.pas' {PimForm},
  cao_Vertreter in 'cao_Vertreter.pas' {VertreterForm},
  cao_adressen_lief in 'cao_adressen_lief.pas' {AdressenLiefForm},
  cao_shoptrans in 'cao_Shoptrans.pas' {ShopTransForm};

{$R *.RES}
{$R zipmsgde.RES}


begin
  Application.Initialize;
  Application.Name  :='CAO';
  Application.Title := 'CAO-Faktura 1.2 Stable';
  Screen.Cursor :=crHourGlass;


  SScreen :=tStartBox.Create (nil);
  SScreen.Visible :=True;

  SScreen.PBar.Max :=43;
  SScreen.PBar.Position :=1;
  SScreen.PBar.Visible :=True;
  SScreen.Invalidate;
  SScreen.Show;

  //Allgemein
  Application.CreateForm(TMainForm, MainForm);

  SScreen.PBar.Position :=2;  SScreen.Update;
  Application.CreateForm(TLinkForm, LinkForm);
  SScreen.PBar.Position :=3;  SScreen.Update;
  Application.CreateForm(TProgressForm, ProgressForm);
  SScreen.PBar.Position :=4;  SScreen.Update;
  Application.CreateForm(TDM1, DM1);
  SScreen.PBar.Position :=5;  SScreen.Update;
  
  //

  SScreen.PBar.Position :=7;  SScreen.Update;
  Application.CreateForm(TVisibleSpaltenForm, VisibleSpaltenForm);
  SScreen.PBar.Position :=8;  SScreen.Update;
  Application.CreateForm(TAboutBox, AboutBox);
  SScreen.PBar.Position :=9;  SScreen.Update;
  Application.CreateForm(TExportForm, ExportForm);
  SScreen.PBar.Position :=10;  SScreen.Update;
  Application.CreateForm(TFileUpload, FileUpload);
  SScreen.PBar.Position :=11;  SScreen.Update;

  // Stammdaten
  Application.CreateForm(TAdressForm, AdressForm);
  SScreen.PBar.Position :=12;  SScreen.Update;
  Application.CreateForm(TArtikelForm, ArtikelForm);
  SScreen.PBar.Position :=13;  SScreen.Update;
  Application.CreateForm(TKFZForm, KFZForm);
  SScreen.PBar.Position :=14;  SScreen.Update;
  Application.CreateForm(TInfoForm, InfoForm);
  SScreen.PBar.Position :=15;  SScreen.Update;
  Application.CreateForm(TPLZForm, PLZForm);
  SScreen.PBar.Position :=16;  SScreen.Update;
  Application.CreateForm(TMitarbeiterForm, MitarbeiterForm);
  SScreen.PBar.Position :=17;  SScreen.Update;

  //FIBU
  Application.CreateForm(TKassenBuchForm, KassenBuchForm);
  SScreen.PBar.Position :=18;  SScreen.Update;
  Application.CreateForm(TZEForm, ZEForm);
  SScreen.PBar.Position :=19;  SScreen.Update;

  //Journale
  Application.CreateForm(TJournalEKREForm, JournalEKREForm);
  SScreen.PBar.Position :=20;  SScreen.Update;
  Application.CreateForm(TJournalVKKasseForm, JournalVKKasseForm);
  SScreen.PBar.Position :=21;  SScreen.Update;
  Application.CreateForm(TJournalVKAGBForm, JournalVKAGBForm);
  SScreen.PBar.Position :=22;  SScreen.Update;
  Application.CreateForm(TJournalVKLiefForm, JournalVKLiefForm);
  SScreen.PBar.Position :=23;  SScreen.Update;
  Application.CreateForm(TJournalVKREForm, JournalVKREForm);
  SScreen.PBar.Position :=24;  SScreen.Update;
  Application.CreateForm(TJournalEKBestForm, JournalEKBestForm);
  SScreen.PBar.Position :=24;  SScreen.Update;


  // Belege
  Application.CreateForm(TMakeVKReForm, MakeVKReForm);
  SScreen.PBar.Position :=26;  SScreen.Update;
  Application.CreateForm(TMakeEKReForm, MakeEKReForm);
  SScreen.PBar.Position :=27;  SScreen.Update;
  Application.CreateForm(TSNErfassenForm, SNErfassenForm);
  SScreen.PBar.Position :=28;  SScreen.Update;
  Application.CreateForm(TSNAuswahlForm, SNAuswahlForm);
  SScreen.PBar.Position :=29;  SScreen.Update;
  Application.CreateForm(TMakeVertragReForm, MakeVertragReForm); //NEU
  SScreen.PBar.Position :=31;  SScreen.Update;
  Application.CreateForm(TMakeVKReForm, MakeAGBForm);
  SScreen.PBar.Position :=32;  SScreen.Update;
  Application.CreateForm(TMakeEKBestForm, MakeEKBestForm);
  SScreen.PBar.Position :=33;  SScreen.Update;
  Application.CreateForm(TEKRechFertigDlg, EKRechFertigDlg);
  SScreen.PBar.Position :=34;  SScreen.Update;
  {$IFDEF REPORTBUILDER}
  Application.CreateForm(TPrintRechForm, PrintRechForm);
  SScreen.PBar.Position :=35;  SScreen.Update;
  {$ENDIF}
  Application.CreateForm(TBLZForm, BLZForm);
  SScreen.PBar.Position :=37;  SScreen.Update;

  Application.CreateForm(TVertreterForm, VertreterForm);
  SScreen.PBar.Position :=38;  SScreen.Update;

  Application.CreateForm(TPimForm, PimForm);
  SScreen.PBar.Position :=39;  SScreen.Update;
  Application.CreateForm(TAdressenLiefForm, AdressenLiefForm);
  SScreen.PBar.Position :=41;  SScreen.Update;
  Application.CreateForm(TShopTransForm, ShopTransForm);
  SScreen.PBar.Position :=42;  SScreen.Update;
  Application.CreateForm(TTeilLieferForm, TeilLieferForm);
  SScreen.PBar.Position :=43;  SScreen.Update;
  Application.CreateForm(TLogForm, LogForm);

  Screen.Cursor :=crDefault;

  Application.Run;
end.