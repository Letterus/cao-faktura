object PLZForm: TPLZForm
  Left = 383
  Top = 185
  Width = 456
  Height = 289
  Caption = 'Postleitzahlen und Orte'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnDeactivate = FormDeactivate
  PixelsPerInch = 96
  TextHeight = 13
  object PLZPanel: TPanel
    Left = 0
    Top = 0
    Width = 448
    Height = 262
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    object SB: TStatusBar
      Left = 0
      Top = 243
      Width = 448
      Height = 19
      Panels = <
        item
          Width = 100
        end
        item
          Width = 100
        end
        item
          Width = 50
        end>
      SimplePanel = False
    end
    object ToolBar1: TToolBar
      Left = 0
      Top = 217
      Width = 448
      Height = 26
      Align = alBottom
      AutoSize = True
      BorderWidth = 1
      ButtonWidth = 94
      EdgeBorders = []
      Flat = True
      Images = MainForm.ImageList1
      List = True
      ShowCaptions = True
      TabOrder = 1
      object DBNavigator1: TDBNavigator
        Left = 0
        Top = 0
        Width = 180
        Height = 22
        DataSource = PLZ_DS
        VisibleButtons = [nbFirst, nbPrior, nbNext, nbLast, nbInsert, nbDelete, nbEdit, nbPost, nbCancel]
        Flat = True
        TabOrder = 0
      end
      object ToolButton2: TToolButton
        Left = 180
        Top = 0
        Width = 8
        Caption = 'ToolButton2'
        ImageIndex = 0
        Style = tbsSeparator
      end
      object Label25: TLabel
        Left = 188
        Top = 0
        Width = 57
        Height = 22
        Alignment = taCenter
        AutoSize = False
        Caption = 'Suchbegr.'
        Layout = tlCenter
      end
      object ToolButton1: TToolButton
        Left = 245
        Top = 0
        Width = 8
        Caption = 'ToolButton1'
        Style = tbsSeparator
      end
      object Suchbegr: TEdit
        Left = 253
        Top = 0
        Width = 89
        Height = 22
        TabOrder = 1
        OnChange = SuchbegrChange
      end
      object UebernBtn: TToolButton
        Left = 342
        Top = 0
        Caption = '�ber&nehmen'
        ImageIndex = 22
        Visible = False
        OnClick = PLZGridDblClick
      end
    end
    object PLZGrid: TCaoDBGrid
      Left = 0
      Top = 0
      Width = 448
      Height = 217
      Align = alClient
      DataSource = PLZ_DS
      DefaultDrawing = False
      TabOrder = 2
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      OnDblClick = PLZGridDblClick
      TitleButtons = True
      OnCheckButton = PLZGridCheckButton
      OnTitleBtnClick = PLZGridTitleBtnClick
      RowColor1 = 12255087
      RowColor2 = clWindow
      ShowTitleEllipsis = True
      DefaultRowHeight = 16
      EditColor = clBlack
      Columns = <
        item
          Expanded = False
          FieldName = 'LAND'
          Width = 30
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PLZ'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -11
          Title.Font.Name = 'MS Sans Serif'
          Title.Font.Style = [fsBold]
          Width = 61
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NAME'
          Width = 141
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'VORWAHL'
          Width = 61
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'BUNDESLAND'
          Visible = True
        end>
    end
  end
  object PLZ_DS: TDataSource
    DataSet = PLZQuery
    OnDataChange = PLZ_DSDataChange
    Left = 88
    Top = 80
  end
  object PLZQuery: TZMySqlQuery
    Database = DM1.db1
    Transaction = DM1.Transact1
    CachedUpdates = False
    ShowRecordTypes = [ztModified, ztInserted, ztUnmodified]
    Options = [doHourGlass, doQueryAllRecords, doAutoFillDefs]
    LinkOptions = [loCascadeUpdate, loAlwaysResync]
    Constraints = <>
    OnNewRecord = PLZQueryNewRecord
    ExtraOptions = [moStoreResult]
    Macros = <>
    Sql.Strings = (
      'SELECT LAND, PLZ ,NAME ,VORWAHL , BUNDESLAND'
      'FROM PLZ'
      'ORDER BY LAND, PLZ, NAME')
    RequestLive = True
    Left = 152
    Top = 72
    object PLZQueryLAND: TStringField
      DisplayLabel = 'Land'
      FieldName = 'LAND'
      Required = True
      Size = 3
    end
    object PLZQueryPLZ: TStringField
      FieldName = 'PLZ'
      Size = 10
    end
    object PLZQueryNAME: TStringField
      DisplayLabel = 'Ort'
      DisplayWidth = 45
      FieldName = 'NAME'
      Required = True
      Size = 50
    end
    object PLZQueryVORWAHL: TStringField
      DisplayLabel = 'Vorwahl'
      FieldName = 'VORWAHL'
      Size = 12
    end
    object PLZQueryBUNDESLAND: TStringField
      DisplayLabel = 'Bundesland'
      FieldName = 'BUNDESLAND'
      Size = 3
    end
  end
end
