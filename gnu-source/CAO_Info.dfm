object InfoForm: TInfoForm
  Left = 448
  Top = 187
  Width = 635
  Height = 560
  Caption = 'alle Notizen'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Menu = MainMenu1
  OldCreateOrder = False
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnDeactivate = FormDeactivate
  PixelsPerInch = 96
  TextHeight = 13
  object InfoPanel: TPanel
    Left = 0
    Top = 0
    Width = 627
    Height = 514
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object CenterPan: TPanel
      Left = 0
      Top = 35
      Width = 627
      Height = 449
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object MsgPan: TPanel
        Left = 0
        Top = 286
        Width = 627
        Height = 163
        Align = alBottom
        BevelOuter = bvNone
        TabOrder = 0
        object Panel4: TPanel
          Left = 0
          Top = 0
          Width = 627
          Height = 29
          Align = alTop
          BevelOuter = bvNone
          BorderWidth = 5
          TabOrder = 0
          object Label1: TLabel
            Left = 2
            Top = 8
            Width = 45
            Height = 13
            AutoSize = False
            Caption = 'Kurzinfo'
          end
          object WVCB: TDBCheckBox
            Left = 434
            Top = 6
            Width = 97
            Height = 17
            Anchors = [akTop, akRight]
            Caption = 'Wiedervorlage'
            Ctl3D = False
            DataField = 'WIEDERVORLAGE'
            DataSource = InfoDS
            ParentCtl3D = False
            TabOrder = 0
            ValueChecked = '1'
            ValueUnchecked = '0'
          end
          object WVDate: TJvDBDateEdit
            Left = 530
            Top = 5
            Width = 88
            Height = 19
            DataField = 'WV_DATUM'
            DataSource = InfoDS
            Ctl3D = False
            Anchors = [akTop, akRight]
            NumGlyphs = 2
            ParentCtl3D = False
            TabOrder = 1
            YearDigits = dyFour
          end
          object KurzInfoEdi: TDBEdit
            Left = 48
            Top = 5
            Width = 376
            Height = 19
            Anchors = [akLeft, akTop, akRight]
            Ctl3D = False
            DataField = 'KURZTEXT'
            DataSource = InfoDS
            ParentCtl3D = False
            TabOrder = 2
          end
        end
        object DBRichEdit1: TDBRichEdit
          Left = 0
          Top = 29
          Width = 627
          Height = 134
          Align = alClient
          DataField = 'MEMO'
          DataSource = InfoDS
          ScrollBars = ssVertical
          TabOrder = 1
        end
      end
      object JvxSplitter1: TJvxSplitter
        Left = 0
        Top = 280
        Width = 627
        Height = 6
        ControlFirst = InfoGrid
        ControlSecond = MsgPan
        Align = alBottom
        BevelInner = bvRaised
        BorderStyle = bsSingle
      end
      object InfoGrid: TCaoDBGrid
        Left = 0
        Top = 0
        Width = 627
        Height = 280
        Align = alClient
        DataSource = InfoDS
        DefaultDrawing = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgConfirmDelete]
        ParentFont = False
        ReadOnly = True
        TabOrder = 2
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleButtons = True
        RowColor1 = 12255087
        RowColor2 = clWindow
        ShowTitleEllipsis = True
        DefaultRowHeight = 17
        EditColor = clBlack
        OnApplyCellAttribute = InfoGridApplyCellAttribute
        Columns = <
          item
            Expanded = False
            FieldName = 'LFD_NR'
            Width = 35
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DATUM'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ERLED'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'KURZTEXT'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ERST_VON'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CALC_WV'
            Width = 25
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'WV_DATUM'
            Visible = True
          end>
      end
    end
    object ToolBar1: TToolBar
      Left = 0
      Top = 484
      Width = 627
      Height = 30
      Align = alBottom
      AutoSize = True
      BorderWidth = 2
      ButtonWidth = 68
      Caption = 'ToolBar1'
      EdgeInner = esNone
      EdgeOuter = esNone
      Flat = True
      Images = MainForm.ImageList1
      List = True
      ShowCaptions = True
      TabOrder = 1
      Wrapable = False
      object DBNavigator1: TDBNavigator
        Left = 0
        Top = 0
        Width = 240
        Height = 22
        DataSource = InfoDS
        Flat = True
        TabOrder = 0
      end
      object ToolButton1: TToolButton
        Left = 240
        Top = 0
        Width = 8
        Caption = 'ToolButton1'
        Style = tbsSeparator
      end
      object Label25: TLabel
        Left = 248
        Top = 0
        Width = 73
        Height = 22
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'Suchebegriff : '
        Layout = tlCenter
        Visible = False
      end
      object Suchbeg: TEdit
        Left = 321
        Top = 0
        Width = 121
        Height = 22
        TabOrder = 1
        Visible = False
      end
      object ToolButton2: TToolButton
        Left = 442
        Top = 0
        Width = 8
        Caption = 'ToolButton2'
        ImageIndex = 0
        Style = tbsSeparator
        Visible = False
      end
      object ErledBtn: TToolButton
        Left = 450
        Top = 0
        AutoSize = True
        Caption = 'Erledigt'
        ImageIndex = 12
        OnClick = ErledBtnClick
      end
      object ToolButton3: TToolButton
        Left = 516
        Top = 0
        Width = 8
        Caption = 'ToolButton3'
        ImageIndex = 10
        Style = tbsSeparator
        Visible = False
      end
      object PrintBtn: TToolButton
        Left = 524
        Top = 0
        AutoSize = True
        Caption = 'Drucken'
        Enabled = False
        ImageIndex = 11
        Visible = False
      end
    end
    object TopStatusPan: TPanel
      Left = 0
      Top = 0
      Width = 627
      Height = 35
      Align = alTop
      Alignment = taLeftJustify
      BevelInner = bvLowered
      BevelOuter = bvNone
      BorderWidth = 3
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 2
      object MainLabelMid: TLabel
        Left = 69
        Top = 4
        Width = 512
        Height = 27
        Align = alClient
        Alignment = taRightJustify
        Caption = 'ddfdfdd'
        Color = clBtnShadow
        Font.Charset = ANSI_CHARSET
        Font.Color = clWhite
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        Layout = tlCenter
      end
      object MainLabLeft: TLabel
        Left = 9
        Top = 4
        Width = 57
        Height = 27
        Align = alLeft
        Caption = 'Notizen '
        Color = clBtnShadow
        Font.Charset = ANSI_CHARSET
        Font.Color = clWhite
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        Layout = tlCenter
      end
      object Label2: TLabel
        Left = 4
        Top = 4
        Width = 5
        Height = 27
        Align = alLeft
        Caption = ' '
        Color = clBtnShadow
        ParentColor = False
      end
      object MainLabMid: TLabel
        Left = 66
        Top = 4
        Width = 3
        Height = 27
        Align = alLeft
        Color = clBtnShadow
        Font.Charset = ANSI_CHARSET
        Font.Color = clWhite
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Layout = tlCenter
      end
      object MainPanRight: TPanel
        Left = 581
        Top = 4
        Width = 42
        Height = 27
        Align = alRight
        BevelOuter = bvNone
        Color = clBtnShadow
        TabOrder = 0
        object Image1: TImage
          Left = 0
          Top = 0
          Width = 42
          Height = 27
          Align = alClient
          Center = True
          Picture.Data = {
            07544269746D617042010000424D420100000000000076000000280000001100
            0000110000000100040000000000CC0000000000000000000000100000001000
            0000000000000000BF0000BF000000BFBF00BF000000BF00BF00BFBF0000C0C0
            C000808080000000FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFF
            FF0077777777777777777000000070000000000007777000000070FFFFFFFFFF
            07777000000070FCCFCCCCCF07777000000070FFFFFFFFFF07777000000070FC
            CFCCCCCF07777000000070FFFFFFFFFF07777000000070FFFFFFF0FF07777000
            000070F00FFF0B0F07770000000070F0F0F0B0F000700000000070FF0B0B0F0F
            BF0000000000700000F0F0FBFBF0000000007777770B0FBFBFB0000000007777
            7770FBFBFB000000000077777777000000700000000077777777777777777000
            0000777777777777777770000000}
          Transparent = True
        end
      end
    end
  end
  object InfoDS: TDataSource
    DataSet = InfoTab
    OnDataChange = InfoDSDataChange
    Left = 373
    Top = 155
  end
  object InfoTab: TZMySqlQuery
    Database = DM1.db1
    Transaction = DM1.Transact1
    CachedUpdates = False
    ShowRecordTypes = [ztModified, ztInserted, ztUnmodified]
    Options = [doHourGlass, doQueryAllRecords, doAutoFillDefs]
    LinkOptions = [loAlwaysResync]
    Constraints = <>
    AfterOpen = InfoTabAfterScroll
    AfterInsert = InfoTabAfterInsert
    AfterPost = InfoTabAfterPost
    AfterScroll = InfoTabAfterScroll
    OnCalcFields = InfoTabCalcFields
    OnNewRecord = InfoTabNewRecord
    ExtraOptions = [moStoreResult]
    Macros = <>
    Sql.Strings = (
      'select * from INFO')
    RequestLive = True
    Left = 436
    Top = 153
    object InfoTabLFD_NR: TIntegerField
      DisplayLabel = 'lfd.Nr.'
      DisplayWidth = 4
      FieldName = 'LFD_NR'
      DisplayFormat = '00000'
    end
    object InfoTabERLED: TIntegerField
      DisplayLabel = 'Erledigt'
      DisplayWidth = 2
      FieldName = 'ERLED'
      Visible = False
    end
    object InfoTabQUELLE: TIntegerField
      DisplayLabel = 'Quelle'
      FieldName = 'QUELLE'
      Required = True
      Visible = False
    end
    object InfoTabDATUM: TDateField
      DisplayLabel = 'Datum'
      FieldName = 'DATUM'
      Required = True
      DisplayFormat = 'dd.mm.yyyy'
    end
    object InfoTabKURZTEXT: TStringField
      DisplayLabel = 'Info'
      DisplayWidth = 65
      FieldName = 'KURZTEXT'
      Size = 100
    end
    object InfoTabWIEDERVORLAGE: TIntegerField
      DisplayLabel = 'WV'
      FieldName = 'WIEDERVORLAGE'
      Visible = False
    end
    object InfoTabWV_DATUM: TDateField
      DisplayLabel = 'WV am'
      FieldName = 'WV_DATUM'
      DisplayFormat = 'dd.mm.yyyy'
    end
    object InfoTabERST_VON: TStringField
      DisplayLabel = 'Erstellt von'
      FieldName = 'ERST_VON'
    end
    object InfoTabMEMO: TMemoField
      FieldName = 'MEMO'
      Visible = False
      BlobType = ftMemo
    end
    object InfoTabCALC_WV: TBooleanField
      DisplayLabel = 'WV'
      FieldKind = fkCalculated
      FieldName = 'CALC_WV'
      Calculated = True
    end
  end
  object MainMenu1: TMainMenu
    Images = MainForm.ImageList1
    Left = 308
    Top = 156
    object Bearbeiten1: TMenuItem
      Caption = '&Bearbeiten'
      GroupIndex = 2
      object neuenBelegerstellen1: TMenuItem
        Caption = 'neue Info'
        ImageIndex = 23
        OnClick = neuenBelegerstellen1Click
      end
      object N3: TMenuItem
        Caption = '-'
      end
      object Storno1: TMenuItem
        Caption = 'L�schen'
        ImageIndex = 19
        OnClick = Storno1Click
      end
      object N1: TMenuItem
        Caption = '-'
      end
      object SichtbareSpalten1: TMenuItem
        Caption = 'Sichtbare Spalten'
        ImageIndex = 15
        OnClick = SichtbareSpalten1Click
      end
      object Layoutspeichern1: TMenuItem
        Caption = 'Layout speichern'
        ImageIndex = 16
        OnClick = Layoutspeichern1Click
      end
    end
    object Sortierung1: TMenuItem
      Caption = '&Sortierung'
      GroupIndex = 3
      object Datum1: TMenuItem
        Tag = 1
        Caption = 'Datum'
        Checked = True
        GroupIndex = 1
        RadioItem = True
        OnClick = Erstellt1Click
      end
      object Info1: TMenuItem
        Tag = 2
        Caption = 'Info'
        GroupIndex = 1
        RadioItem = True
        OnClick = Erstellt1Click
      end
      object Erstellt1: TMenuItem
        Tag = 3
        Caption = 'Erstellt'
        GroupIndex = 1
        RadioItem = True
        OnClick = Erstellt1Click
      end
    end
    object Ansicht1: TMenuItem
      Caption = '&Ansicht'
      GroupIndex = 4
      object nurUnerledigt1: TMenuItem
        Caption = 'nur Unerledigt'
        OnClick = nurUnerledigt1Click
      end
      object N2: TMenuItem
        Caption = '-'
      end
      object Aktualisieren1: TMenuItem
        Caption = 'Aktualisieren'
        ImageIndex = 13
        ShortCut = 116
        OnClick = Aktualisieren1Click
      end
    end
  end
end
