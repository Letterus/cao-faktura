

unit CAO_PrintArtikel;

{$I CAO32.INC}

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, JvLookup, Db, ppDB, ppComm, ppRelatv, ppDBPipe,
  ZQuery, ZMySqlQuery, JvSpin, ppBands, ppReport, ppPrnabl, ppClass,
  ppStrtch, ppSubRpt, ppCache, ppProd, ppEndUsr, ppViewr,
  ppDevice, ppPrnDev, JvEdit, Mask, JvMaskEdit;

type
  TPrintArtikelForm = class(TForm)
    Label1: TLabel;
    Label2: TLabel;
    FormularCB: TJvDBLookupCombo;
    ZielCB: TComboBox;
    LayoutBtn: TBitBtn;
    BinNamCB: TComboBox;
    AnzCopy: TJvSpinEdit;
    VorschauBtn: TBitBtn;
    PrintBtn: TBitBtn;
    DruEinrBtn: TBitBtn;
    CloseBtn: TBitBtn;
    ArtDS: TDataSource;
    ArtListPipeline: TppDBPipeline;
    ReportTab: TZMySqlQuery;
    ReportTabMAINKEY: TStringField;
    ReportTabNAME: TStringField;
    ReportTabVAL_BLOB: TMemoField;
    ReportTabVAL_CHAR: TStringField;
    ReportTabVAL_BIN: TBlobField;
    ReportDS: TDataSource;
    RepPipeline: TppDBPipeline;
    ppDesigner1: TppDesigner;
    Report: TppReport;
    ppHeaderBand1: TppHeaderBand;
    ppDetailBand1: TppDetailBand;
    ppFooterBand1: TppFooterBand;
    Label7: TLabel;
    Label8: TLabel;
    ArtStueckListPipeline: TppDBPipeline;
    ArtHistPipeline: TppDBPipeline;
    ppDBPipeline3: TppDBPipeline;
    ArtStueckListDS: TDataSource;
    ArtHistDS: TDataSource;
    DataSource3: TDataSource;


    procedure ReportTabBeforePost(DataSet: TDataSet);
    procedure ReportTabBeforeOpen(DataSet: TDataSet);
    procedure CloseBtnClick(Sender: TObject);
    procedure FormularCBChange(Sender: TObject);
    procedure ZielCBChange(Sender: TObject);
    procedure PrintBtnClick(Sender: TObject);
    procedure LayoutBtnClick(Sender: TObject);
    procedure ReportPreviewFormCreate(Sender: TObject);
    procedure VorschauBtnClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure AnzCopyChange(Sender: TObject);
    procedure ppDesigner1CloseQuery(Sender: TObject;
      var CanClose: Boolean);
    procedure DruEinrBtnClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private-Deklarationen }
    MainKey   : String;
    PrintName : String;
  public
    { Public-Deklarationen }
    procedure ShowArtikelListDlg;
    procedure ShowArtStuecklistDlg;
    procedure ShowArtHistorieDlg;
  end;


implementation

{$R *.DFM}

uses cao_dm, cao_var_const, cao_tool1, ppTypes, ppPrintr, CAO_ARTIKEL1;

//------------------------------------------------------------------------------
procedure TPrintArtikelForm.FormCreate(Sender: TObject);
begin
     clientheight :=155;
end;
//------------------------------------------------------------------------------
procedure TPrintArtikelForm.ShowArtikelListDlg;
var Res : Boolean; i:integer;
begin
     MainKey             :='MAIN\REPORT\ARTIKEL';
     Caption             :='Artikelliste drucken';
     PrintName           :='CAO-Faktura Artikelliste';
     ppDesigner1.Caption :='Layout Artikelliste bearbeiten';
     Report.DataPipeline :=ArtListPipeline;

     ArtDS.DataSet :=ArtikelForm.ASQuery;
     ArtikelForm.ASQuery.DisableControls;
     try
       ReportTab.Close;
       ReportTab.Open;

       Res :=FormularCB.Locate (ReportTabVal_Char,'DEFAULT',True);
       if not Res then
       begin
           ReportTab.First;
           FormularCB.DisplayValue :=ReportTabName.Value;
       end
         else
       begin
            FormularCB.DisplayValue :=ReportTabName.Value;
       end;

       ZielCB.Items.Assign (Report.PrinterSetup.PrinterNames);
       I :=ZielCB.Items.IndexOf ('Screen');
       if I>=0 then ZielCB.Items.Delete (i);

       if ZielCB.Items.Count >=0 then
        for i:=0 to ZielCB.Items.Count-1 do
         if ZielCB.Items[i]=Report.PrinterSetup.PrinterName
           then ZielCB.ItemIndex :=I;

       if ZielCB.ItemIndex=-1 then
        if ZielCB.Items.IndexOf ('Default')>-1
         then ZielCB.ItemIndex := ZielCB.Items.IndexOf ('Default');

       ZielCBChange(Self);

       DM1.FirmaTab.Open;

       ArtListPipeline.Visible    :=True;
       ArtListPipeline.RangeBegin :=rbFirstRecord;
       ArtListPipeline.RangeEnd   :=reLastRecord;

       ShowModal;

       ArtListPipeline.Visible :=False;

     finally
       ArtDS.DataSet :=nil;
       ArtikelForm.ASQuery.EnableControls;
     end;

     ReportTab.Close;
     DM1.FirmaTab.Close;
end;
//------------------------------------------------------------------------------
procedure TPrintArtikelForm.ShowArtStuecklistDlg;
var Res : Boolean; i:integer;
begin
     MainKey             :='MAIN\REPORT\ARTIKEL_STUECKLISTE';
     Caption             :='Artikel-Stückliste drucken';
     PrintName           :='CAO-Faktura Artikel-Stückliste';
     ppDesigner1.Caption :='Layout Artikel-Stückliste bearbeiten';
     Report.DataPipeline :=ArtListPipeline;

     ArtDS.DataSet :=ArtikelForm.ASQuery;
     ArtStueckListDS.Dataset :=ArtikelForm.STListTab;


     ArtikelForm.ASQuery.DisableControls;
     ArtikelForm.STListTab.DisableControls;
     try
       ReportTab.Close;
       ReportTab.Open;

       Res :=FormularCB.Locate (ReportTabVal_Char,'DEFAULT',True);
       if not Res then
       begin
           ReportTab.First;
           FormularCB.DisplayValue :=ReportTabName.Value;
       end
         else
       begin
            FormularCB.DisplayValue :=ReportTabName.Value;
       end;

       ZielCB.Items.Assign (Report.PrinterSetup.PrinterNames);
       I :=ZielCB.Items.IndexOf ('Screen');
       if I>=0 then ZielCB.Items.Delete (i);

       if ZielCB.Items.Count >=0 then
        for i:=0 to ZielCB.Items.Count-1 do
         if ZielCB.Items[i]=Report.PrinterSetup.PrinterName
           then ZielCB.ItemIndex :=I;

       if ZielCB.ItemIndex=-1 then
        if ZielCB.Items.IndexOf ('Default')>-1
         then ZielCB.ItemIndex := ZielCB.Items.IndexOf ('Default');

       ZielCBChange(Self);

       DM1.FirmaTab.Open;

       ArtListPipeline.Visible       :=True;
       ArtListPipeline.RangeBegin    :=rbCurrentRecord;
       ArtListPipeline.RangeEnd      :=reCurrentRecord;
       ArtStueckListPipeline.Visible :=True;

       ShowModal;
     finally
       ArtDS.DataSet :=nil;
       ArtStueckListDS.Dataset :=nil;

       ArtikelForm.ASQuery.EnableControls;
       ArtikelForm.STListTab.EnableControls;
     end;

     ReportTab.Close;
     DM1.FirmaTab.Close;
end;
//------------------------------------------------------------------------------
procedure TPrintArtikelForm.ShowArtHistorieDlg;
var Res : Boolean; i:integer;
begin
     MainKey             :='MAIN\REPORT\ARTIKEL_HISTORIE';
     Caption             :='Artikel-Histore drucken';
     PrintName           :='CAO-Faktura Artikel-Historie';
     ppDesigner1.Caption :='Layout Artikel-Historie bearbeiten';
     Report.DataPipeline :=ArtHistPipeline;

     ArtDS.DataSet :=ArtikelForm.ASQuery;
     ArtHistDS.Dataset :=ArtikelForm.JPosTab;


     ArtikelForm.ASQuery.DisableControls;
     ArtikelForm.JPosTab.DisableControls;
     try
       ReportTab.Close;
       ReportTab.Open;

       Res :=FormularCB.Locate (ReportTabVal_Char,'DEFAULT',True);
       if not Res then
       begin
           ReportTab.First;
           FormularCB.DisplayValue :=ReportTabName.Value;
       end
         else
       begin
            FormularCB.DisplayValue :=ReportTabName.Value;
       end;

       ZielCB.Items.Assign (Report.PrinterSetup.PrinterNames);
       I :=ZielCB.Items.IndexOf ('Screen');
       if I>=0 then ZielCB.Items.Delete (i);

       if ZielCB.Items.Count >=0 then
        for i:=0 to ZielCB.Items.Count-1 do
         if ZielCB.Items[i]=Report.PrinterSetup.PrinterName
           then ZielCB.ItemIndex :=I;

       if ZielCB.ItemIndex=-1 then
        if ZielCB.Items.IndexOf ('Default')>-1
         then ZielCB.ItemIndex := ZielCB.Items.IndexOf ('Default');

       ZielCBChange(Self);

       DM1.FirmaTab.Open;

       ArtListPipeline.Visible       :=True;
       ArtListPipeline.RangeBegin    :=rbCurrentRecord;
       ArtListPipeline.RangeEnd      :=reCurrentRecord;
       ArtHistPipeline.Visible       :=True;

       ShowModal;
     finally
       ArtDS.DataSet :=nil;
       ArtHistDS.Dataset :=nil;

       ArtikelForm.ASQuery.EnableControls;
       ArtikelForm.JPosTab.EnableControls;
     end;

     ReportTab.Close;
     DM1.FirmaTab.Close;
end;
//------------------------------------------------------------------------------
procedure TPrintArtikelForm.FormShow(Sender: TObject);
begin
     PrintBtn.SetFocus;
end;
//------------------------------------------------------------------------------
procedure TPrintArtikelForm.ReportTabBeforePost(DataSet: TDataSet);
begin
     ReportTabMAINKEY.AsString :=MainKey;
end;
//------------------------------------------------------------------------------
procedure TPrintArtikelForm.ReportTabBeforeOpen(DataSet: TDataSet);
begin
     ReportTab.ParamByName('KEY').Value :=MainKey;
end;
//------------------------------------------------------------------------------
procedure TPrintArtikelForm.CloseBtnClick(Sender: TObject);
begin
     Close;
end;
//------------------------------------------------------------------------------
procedure TPrintArtikelForm.FormularCBChange(Sender: TObject);
begin
     Report.Template.DatabaseSettings.Name :=FormularCB.DisplayValue;
     try Report.Template.LoadFromDatabase; except end;
     Report.PrinterSetup.Copies :=trunc(ANzCopy.Value);
     Report.Language :=lgGerman;
     Report.OnPreviewFormCreate :=ReportPreviewFormCreate;
     Report.AllowPrintToFile :=True;
end;
//------------------------------------------------------------------------------
procedure TPrintArtikelForm.ZielCBChange(Sender: TObject);
begin
     {set the printer name for the device}
     Report.PrinterSetup.PrinterName  := ZielCB.Items[ZielCB.ItemIndex];

     Application.ProcessMessages;

     BinNamCB.Items.Assign (Report.PrinterSetup.BinNames);

     if BinNamCB.Items.IndexOf(Report.PrinterSetup.BinName)>-1
      then BinNamCB.ItemIndex :=BinNamCB.Items.IndexOf(Report.PrinterSetup.BinName);

     if BinNamCB.ItemIndex=-1 then BinNamCB.ItemIndex :=BinNamCB.Items.IndexOf('Default');

     AnzCopy.Value :=Report.PrinterSetup.Copies;
end;
//------------------------------------------------------------------------------
procedure TPrintArtikelForm.PrintBtnClick(Sender: TObject);
begin
     Report.PrinterSetup.DocumentName :=PrintName;

     Report.DeviceType :=dtPrinter;
     Report.ShowPrintDialog :=False;
     Report.Print;
end;
//------------------------------------------------------------------------------
procedure TPrintArtikelForm.LayoutBtnClick(Sender: TObject);
begin
     //ArtListPipeline.Visible :=True;

     ppDesigner1.ShowModal;
     if Report.Template.DatabaseSettings.Name<>FormularCB.DisplayValue
      then FormularCB.DisplayValue :=Report.Template.DatabaseSettings.Name;

     //ArtListPipeline.Visible :=False;

     Report.OnPreviewFormCreate :=ReportPreviewFormCreate;
     Report.AllowPrintToFile :=True;
end;
//------------------------------------------------------------------------------
procedure TPrintArtikelForm.ReportPreviewFormCreate(Sender: TObject);
begin
     Report.PreviewForm.WindowState := wsMaximized;
     TppViewer(Report.PreviewForm.Viewer).ZoomSetting := zs100Percent;
end;
//------------------------------------------------------------------------------
procedure TPrintArtikelForm.VorschauBtnClick(Sender: TObject);
begin
     Report.DeviceType :=dtScreen;
     Report.Print;
     Report.DeviceType :=dtPrinter;
end;
//------------------------------------------------------------------------------
procedure TPrintArtikelForm.FormDestroy(Sender: TObject);
begin
end;
//------------------------------------------------------------------------------
procedure TPrintArtikelForm.AnzCopyChange(Sender: TObject);
begin
     Report.PrinterSetup.Copies :=round(ANzCopy.Value);
end;
//------------------------------------------------------------------------------
procedure TPrintArtikelForm.ppDesigner1CloseQuery(Sender: TObject;
  var CanClose: Boolean);

var Res : Integer;
begin
     if Report.Modified then
     begin
        Res :=MessageDlg ('Bericht wurde verändert.'+#13#10+
                          'Wollen Sie die Änderungen speichern ?',
                          mtconfirmation,mbyesnocancel,0);
        if Res=mryes then
        begin
           Report.Template.Save;
           CanClose :=True;
        end else
        if Res=mrNo then
        begin
           Report.Template.Load;
           CanClose :=True;
        end else CanClose :=False;
     end
     else CanClose :=True;
end;
//------------------------------------------------------------------------------
procedure TPrintArtikelForm.DruEinrBtnClick(Sender: TObject);
var myPrinter : TppPrinter;
begin
     myPrinter := TppPrinter.Create;
     try
        myPrinter.PrinterSetup :=Report.PrinterSetup;

        if myPrinter.ShowSetupDialog
         then Report.PrinterSetup := myPrinter.PrinterSetup;

        if ZielCB.Items.IndexOf (Report.PrinterSetup.PrinterName)>0
         then ZielCB.ItemIndex :=ZielCB.Items.IndexOf (Report.Printer.PrinterName);

       ZielCBChange(Sender);
     finally
        myPrinter.Free;
     end;
end;
//------------------------------------------------------------------------------
end.
