object BLZForm: TBLZForm
  Left = 321
  Top = 117
  Width = 456
  Height = 289
  Caption = 'Bankleitzahlen'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnDeactivate = FormDeactivate
  PixelsPerInch = 96
  TextHeight = 13
  object BLZPanel: TPanel
    Left = 0
    Top = 0
    Width = 448
    Height = 262
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    object SB: TStatusBar
      Left = 0
      Top = 243
      Width = 448
      Height = 19
      Panels = <
        item
          Width = 100
        end
        item
          Width = 100
        end
        item
          Width = 50
        end>
      SimplePanel = False
    end
    object ToolBar1: TToolBar
      Left = 0
      Top = 217
      Width = 448
      Height = 26
      Align = alBottom
      AutoSize = True
      BorderWidth = 1
      ButtonWidth = 94
      EdgeBorders = []
      Flat = True
      Images = MainForm.ImageList1
      List = True
      ShowCaptions = True
      TabOrder = 1
      object DBNavigator1: TDBNavigator
        Left = 0
        Top = 0
        Width = 180
        Height = 22
        DataSource = BLZ_DS
        VisibleButtons = [nbFirst, nbPrior, nbNext, nbLast, nbInsert, nbDelete, nbEdit, nbPost, nbCancel]
        Flat = True
        TabOrder = 0
      end
      object ToolButton2: TToolButton
        Left = 180
        Top = 0
        Width = 8
        Caption = 'ToolButton2'
        ImageIndex = 0
        Style = tbsSeparator
      end
      object Label25: TLabel
        Left = 188
        Top = 0
        Width = 57
        Height = 22
        Alignment = taCenter
        AutoSize = False
        Caption = 'Suchbegr.'
        Layout = tlCenter
      end
      object ToolButton1: TToolButton
        Left = 245
        Top = 0
        Width = 8
        Caption = 'ToolButton1'
        Style = tbsSeparator
      end
      object Suchbegr: TEdit
        Left = 253
        Top = 0
        Width = 89
        Height = 22
        TabOrder = 1
        OnChange = SuchbegrChange
      end
      object UebernBtn: TToolButton
        Left = 342
        Top = 0
        Caption = '�ber&nehmen'
        ImageIndex = 22
        Visible = False
        OnClick = BLZGridDblClick
      end
    end
    object BLZGrid: TCaoDBGrid
      Left = 0
      Top = 0
      Width = 448
      Height = 217
      Align = alClient
      DataSource = BLZ_DS
      DefaultDrawing = False
      TabOrder = 2
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      OnDblClick = BLZGridDblClick
      RowColor1 = 12255087
      RowColor2 = clWindow
      ShowTitleEllipsis = True
      DefaultRowHeight = 16
      EditColor = clBlack
      Columns = <
        item
          Expanded = False
          FieldName = 'LAND'
          Width = 30
          Visible = True
        end
        item
          Alignment = taLeftJustify
          Expanded = False
          FieldName = 'BLZ'
          Width = 101
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'BANK_NAME'
          Width = 317
          Visible = True
        end>
    end
  end
  object BLZ_DS: TDataSource
    DataSet = BLZQuery
    OnDataChange = BLZ_DSDataChange
    Left = 88
    Top = 72
  end
  object BLZQuery: TZMySqlQuery
    Database = DM1.db1
    Transaction = DM1.Transact1
    CachedUpdates = False
    ShowRecordTypes = [ztModified, ztInserted, ztUnmodified]
    Options = [doHourGlass, doAutoFillDefs]
    LinkOptions = [loCascadeUpdate, loAlwaysResync]
    Constraints = <>
    OnNewRecord = BLZQueryNewRecord
    ExtraOptions = [moStoreResult]
    Macros = <>
    Sql.Strings = (
      'SELECT LAND, BLZ ,BANK_NAME'
      'FROM BLZ'
      'ORDER BY BLZ, BANK_NAME')
    RequestLive = True
    Left = 152
    Top = 72
    object BLZQueryLAND: TStringField
      DisplayLabel = 'Land'
      FieldName = 'LAND'
      Size = 5
    end
    object BLZQueryBLZ: TIntegerField
      FieldName = 'BLZ'
    end
    object BLZQueryBANK_NAME: TStringField
      DisplayLabel = 'Name der Bank'
      DisplayWidth = 40
      FieldName = 'BANK_NAME'
      Size = 255
    end
  end
end
