object KasseArtikelForm: TKasseArtikelForm
  Left = 368
  Top = 134
  Width = 699
  Height = 628
  Caption = 'Material und Dienstleisungen'
  Color = clBtnFace
  DragKind = dkDock
  DragMode = dmAutomatic
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  Menu = MainMenu1
  OldCreateOrder = False
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnDeactivate = FormDeactivate
  PixelsPerInch = 96
  TextHeight = 13
  object ArtikelPanel: TPanel
    Left = 0
    Top = 0
    Width = 691
    Height = 582
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    OnResize = ArtikelPanelResize
    object Art_PC: TJvPageControl
      Left = 142
      Top = 27
      Width = 549
      Height = 509
      ActivePage = Such_TS
      Style = tsFlatButtons
      TabOrder = 0
      OnChange = Art_PCChange
      ClientBorderWidth = 0
      HideAllTabs = True
      object Allg_TS: TTabSheet
        Tag = 1
        Caption = 'Allgemein'
        object Panel4: TPanel
          Left = 0
          Top = 0
          Width = 274
          Height = 486
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 0
          object SuchGB: TCaoGroupBox
            Left = 0
            Top = 0
            Width = 274
            Height = 111
            Align = alTop
            Caption = 'Suchbegriffe'
            Color = 14680063
            Ctl3D = False
            ParentColor = False
            ParentCtl3D = False
            TabOrder = 0
            FRameColor = clBtnFace
            object Label13: TLabel
              Left = 8
              Top = 88
              Width = 74
              Height = 13
              AutoSize = False
              Caption = 'Artikel-Typ:'
              FocusControl = match
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
            end
            object Label30: TLabel
              Left = 6
              Top = 66
              Width = 74
              Height = 13
              AutoSize = False
              Caption = 'Ersatz-Nr.:'
              FocusControl = match
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
            end
            object Label12: TLabel
              Left = 5
              Top = 45
              Width = 74
              Height = 13
              AutoSize = False
              Caption = 'Artikel-Nr.:'
              FocusControl = Artikelnr
            end
            object Label11: TLabel
              Left = 5
              Top = 24
              Width = 74
              Height = 13
              AutoSize = False
              Caption = 'Suchbegriff:'
              FocusControl = match
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
            end
            object ArtikelTyp: TVolgaDBEdit
              Tag = 1
              Left = 79
              Top = 84
              Width = 187
              Height = 19
              AutoDropDown = True
              ButtonWidth = 17
              ComboProps.ComboItems.Strings = (
                'normaler Artikel'
                'Artikel mit St�ckliste'
                'Lohn'
                'Text / Kommentar')
              ComboProps.ComboValues.Strings = (
                'N'
                'S'
                'L'
                'T')
              DataField = 'ARTIKELTYP'
              DataSource = AS_DS
              DialogStyle = vdsCombo
              Style = vcsDropDownList
              TabOrder = 3
              OnEnter = matchEnter
              OnExit = matchExit
              OnKeyDown = matchKeyDown
              OnKeyPress = matchKeyPress
            end
            object Ersatz_artnr: TDBEdit
              Tag = 1
              Left = 79
              Top = 63
              Width = 187
              Height = 19
              AutoSelect = False
              AutoSize = False
              BiDiMode = bdLeftToRight
              CharCase = ecUpperCase
              DataField = 'ERSATZ_ARTNUM'
              DataSource = AS_DS
              ParentBiDiMode = False
              TabOrder = 2
              OnEnter = matchEnter
              OnExit = matchExit
              OnKeyDown = matchKeyDown
              OnKeyPress = matchKeyPress
            end
            object Artikelnr: TDBEdit
              Tag = 1
              Left = 79
              Top = 42
              Width = 187
              Height = 19
              AutoSelect = False
              AutoSize = False
              CharCase = ecUpperCase
              DataField = 'ARTNUM'
              DataSource = AS_DS
              TabOrder = 1
              OnChange = ArtikelnrChange
              OnEnter = matchEnter
              OnExit = matchExit
              OnKeyDown = matchKeyDown
              OnKeyPress = matchKeyPress
            end
            object match: TDBEdit
              Tag = 1
              Left = 79
              Top = 21
              Width = 187
              Height = 19
              AutoSelect = False
              AutoSize = False
              CharCase = ecUpperCase
              DataField = 'MATCHCODE'
              DataSource = AS_DS
              TabOrder = 0
              OnEnter = matchEnter
              OnExit = matchExit
              OnKeyDown = matchKeyDown
              OnKeyPress = matchKeyPress
            end
          end
          object ZuwGB: TCaoGroupBox
            Left = 0
            Top = 111
            Width = 274
            Height = 108
            Align = alTop
            Caption = 'Zuweisungen'
            Color = 14680063
            Ctl3D = False
            ParentColor = False
            ParentCtl3D = False
            TabOrder = 1
            FRameColor = clBtnFace
            object Warengruppe: TLabel
              Left = 5
              Top = 24
              Width = 74
              Height = 13
              AutoSize = False
              Caption = 'Warengruppe:'
            end
            object Label14: TLabel
              Left = 5
              Top = 45
              Width = 74
              Height = 13
              AutoSize = False
              Caption = 'Herkunftsland:'
            end
            object Label15: TLabel
              Left = 5
              Top = 65
              Width = 74
              Height = 13
              AutoSize = False
              Caption = 'Lager-Ort:'
            end
            object Label16: TLabel
              Left = 5
              Top = 85
              Width = 74
              Height = 13
              AutoSize = False
              Caption = 'Barcode/EAN:'
            end
            object barcode: TDBEdit
              Tag = 1
              Left = 79
              Top = 83
              Width = 187
              Height = 19
              AutoSelect = False
              AutoSize = False
              DataField = 'BARCODE'
              DataSource = AS_DS
              TabOrder = 5
              OnEnter = matchEnter
              OnExit = matchExit
              OnKeyDown = matchKeyDown
              OnKeyPress = matchKeyPress
            end
            object lagerort: TDBEdit
              Tag = 1
              Left = 79
              Top = 63
              Width = 187
              Height = 19
              AutoSelect = False
              AutoSize = False
              DataField = 'LAGERORT'
              DataSource = AS_DS
              TabOrder = 4
              OnEnter = matchEnter
              OnExit = matchExit
              OnKeyDown = matchKeyDown
              OnKeyPress = matchKeyPress
            end
            object Land: TDBEdit
              Left = 79
              Top = 42
              Width = 32
              Height = 19
              AutoSize = False
              CharCase = ecUpperCase
              DataField = 'HERKUNFSLAND'
              DataSource = AS_DS
              TabOrder = 2
              OnEnter = matchEnter
              OnExit = matchExit
              OnKeyDown = matchKeyDown
              OnKeyPress = matchKeyPress
            end
            object Land_CB: TDBLookupComboBox
              Tag = 1
              Left = 110
              Top = 42
              Width = 156
              Height = 19
              Cursor = crHandPoint
              Ctl3D = False
              DataField = 'HERKUNFSLAND'
              DataSource = AS_DS
              KeyField = 'ID'
              ListField = 'NAME'
              ListSource = DM1.LandDS
              ParentCtl3D = False
              TabOrder = 3
              OnEnter = matchEnter
              OnExit = matchExit
              OnKeyDown = matchKeyDown
              OnKeyPress = matchKeyPress
            end
            object Warengr: TDBEdit
              Left = 79
              Top = 21
              Width = 32
              Height = 19
              AutoSize = False
              CharCase = ecUpperCase
              DataField = 'WARENGRUPPE'
              DataSource = AS_DS
              TabOrder = 0
              OnEnter = matchEnter
              OnExit = matchExit
              OnKeyDown = matchKeyDown
              OnKeyPress = matchKeyPress
            end
            object Warengr_CB: TDBLookupComboBox
              Tag = 1
              Left = 110
              Top = 21
              Width = 156
              Height = 19
              Cursor = crHandPoint
              DataField = 'WARENGRUPPE'
              DataSource = AS_DS
              DropDownRows = 15
              KeyField = 'ID'
              ListField = 'NAME'
              ListSource = DM1.WgrDS
              TabOrder = 1
              OnEnter = matchEnter
              OnExit = matchExit
              OnKeyDown = matchKeyDown
              OnKeyPress = matchKeyPress
            end
          end
          object EinheitenGB: TCaoGroupBox
            Left = 0
            Top = 219
            Width = 274
            Height = 151
            Align = alTop
            Caption = 'Einheiten / Konten'
            Color = 14680063
            Ctl3D = False
            ParentColor = False
            ParentCtl3D = False
            TabOrder = 2
            FRameColor = clBtnFace
            object Label20: TLabel
              Left = 157
              Top = 24
              Width = 50
              Height = 13
              AutoSize = False
              Caption = 'Aufw.-Kto:'
            end
            object Label21: TLabel
              Left = 157
              Top = 45
              Width = 49
              Height = 13
              AutoSize = False
              Caption = 'Erl��-Kto:'
            end
            object Label29: TLabel
              Left = 5
              Top = 86
              Width = 74
              Height = 13
              AutoSize = False
              Caption = 'Gewicht Kg:'
            end
            object Label22: TLabel
              Left = 5
              Top = 45
              Width = 74
              Height = 13
              AutoSize = False
              Caption = 'Rabattgruppe:'
            end
            object Label19: TLabel
              Left = 5
              Top = 66
              Width = 74
              Height = 13
              AutoSize = False
              Caption = 'Mwst-Code:'
            end
            object Label17: TLabel
              Left = 5
              Top = 24
              Width = 74
              Height = 13
              AutoSize = False
              Caption = 'Mengeneinh.:'
            end
            object Label53: TLabel
              Left = 5
              Top = 107
              Width = 74
              Height = 13
              AutoSize = False
              Caption = 'Dimension:'
            end
            object Label54: TLabel
              Left = 157
              Top = 107
              Width = 51
              Height = 13
              AutoSize = False
              Caption = 'Gr��e:'
            end
            object Label55: TLabel
              Left = 157
              Top = 86
              Width = 50
              Height = 13
              AutoSize = False
              Caption = 'L�nge:'
            end
            object Label46: TLabel
              Left = 157
              Top = 66
              Width = 50
              Height = 13
              AutoSize = False
              Caption = 'Inv.-Wert:'
            end
            object Label47: TLabel
              Left = 5
              Top = 130
              Width = 74
              Height = 13
              AutoSize = False
              Caption = 'Vertr.-Prov.:'
            end
            object Gewicht: TDBEdit
              Left = 78
              Top = 84
              Width = 70
              Height = 19
              AutoSize = False
              DataField = 'GEWICHT'
              DataSource = AS_DS
              TabOrder = 6
              OnEnter = matchEnter
              OnExit = matchExit
              OnKeyDown = matchKeyDown
              OnKeyPress = matchKeyPress
            end
            object E_KTO: TDBEdit
              Tag = 1
              Left = 211
              Top = 42
              Width = 55
              Height = 19
              AutoSize = False
              DataField = 'ERLOES_KTO'
              DataSource = AS_DS
              TabOrder = 4
              OnEnter = matchEnter
              OnExit = matchExit
              OnKeyDown = matchKeyDown
              OnKeyPress = matchKeyPress
            end
            object A_KTO: TDBEdit
              Tag = 1
              Left = 211
              Top = 21
              Width = 55
              Height = 19
              AutoSize = False
              DataField = 'AUFW_KTO'
              DataSource = AS_DS
              TabOrder = 3
              OnEnter = matchEnter
              OnExit = matchExit
              OnKeyDown = matchKeyDown
              OnKeyPress = matchKeyPress
            end
            object Inventur_wert: TDBEdit
              Tag = 1
              Left = 211
              Top = 63
              Width = 55
              Height = 19
              AutoSize = False
              DataField = 'INVENTUR_WERT'
              DataSource = AS_DS
              TabOrder = 5
              OnEnter = matchEnter
              OnExit = matchExit
              OnKeyDown = matchKeyDown
              OnKeyPress = matchKeyPress
            end
            object MEinheit: TDBEdit
              Left = 78
              Top = 21
              Width = 70
              Height = 19
              AutoSelect = False
              AutoSize = False
              DataField = 'ME_EINHEIT'
              DataSource = AS_DS
              TabOrder = 0
              OnEnter = matchEnter
              OnExit = matchExit
              OnKeyDown = matchKeyDown
              OnKeyPress = matchKeyPress
            end
            object MwstCB: TVolgaDBEdit
              Left = 78
              Top = 63
              Width = 70
              Height = 19
              AutoDropDown = True
              ButtonWidth = 17
              ComboProps.ComboItems.Strings = (
                '0=0,0%'
                '1=7,0%'
                '2=16,0%'
                '3=0,0%')
              ComboProps.ComboValues.Strings = (
                '0'
                '1'
                '2'
                '3')
              Ctl3D = False
              DataField = 'STEUER_CODE'
              DataSource = AS_DS
              DialogStyle = vdsCombo
              ParentCtl3D = False
              Style = vcsDropDownList
              TabOrder = 2
              OnChange = MwstCBChange
              OnEnter = matchEnter
              OnExit = matchExit
              OnKeyDown = matchKeyDown
              OnKeyPress = matchKeyPress
            end
            object dimension: TDBEdit
              Left = 78
              Top = 105
              Width = 70
              Height = 19
              AutoSize = False
              DataField = 'DIMENSION'
              DataSource = AS_DS
              TabOrder = 7
              OnEnter = matchEnter
              OnExit = matchExit
              OnKeyDown = matchKeyDown
              OnKeyPress = matchKeyPress
            end
            object Groesse: TDBEdit
              Tag = 1
              Left = 211
              Top = 105
              Width = 55
              Height = 19
              AutoSize = False
              DataField = 'GROESSE'
              DataSource = AS_DS
              TabOrder = 9
              OnEnter = matchEnter
              OnExit = matchExit
              OnKeyDown = matchKeyDown
              OnKeyPress = matchKeyPress
            end
            object Laenge: TDBEdit
              Tag = 1
              Left = 211
              Top = 84
              Width = 55
              Height = 19
              AutoSize = False
              DataField = 'LAENGE'
              DataSource = AS_DS
              TabOrder = 8
              OnEnter = matchEnter
              OnExit = matchExit
              OnKeyDown = matchKeyDown
              OnKeyPress = matchKeyPress
            end
            object RabGrpLoCB: TDBLookupComboBox
              Left = 78
              Top = 42
              Width = 70
              Height = 19
              Cursor = crHandPoint
              Ctl3D = False
              DataField = 'RABGRP_ID'
              DataSource = AS_DS
              DropDownRows = 15
              DropDownWidth = 500
              KeyField = 'RABGRP_ID'
              ListField = 'RABGRP_ID; BESCHREIBUNG; RABATT1; RABATT2; RABATT3'
              ListSource = DM1.RabGrpDS
              ParentCtl3D = False
              TabOrder = 1
              OnEnter = matchEnter
              OnExit = matchExit
              OnKeyDown = matchKeyDown
              OnKeyPress = matchKeyPress
            end
            object provision: TDBEdit
              Left = 78
              Top = 127
              Width = 70
              Height = 19
              AutoSize = False
              DataField = 'PROVIS_PROZ'
              DataSource = AS_DS
              TabOrder = 10
              OnEnter = matchEnter
              OnExit = matchExit
              OnKeyDown = matchKeyDown
              OnKeyPress = matchKeyPress
            end
          end
          object BewDatGB: TCaoGroupBox
            Left = 0
            Top = 370
            Width = 274
            Height = 116
            Align = alClient
            Caption = 'Menge / Preis'
            Color = 14680063
            Ctl3D = False
            ParentColor = False
            ParentCtl3D = False
            TabOrder = 3
            FRameColor = clBtnFace
            object Label23: TLabel
              Left = 5
              Top = 25
              Width = 74
              Height = 13
              AutoSize = False
              Caption = 'Mind.-Bestand:'
            end
            object Label28: TLabel
              Left = 152
              Top = 25
              Width = 43
              Height = 13
              AutoSize = False
              Caption = 'Bestand:'
            end
            object EKPreisLab: TLabel
              Left = 8
              Top = 67
              Width = 66
              Height = 13
              Caption = 'Einkaufspreis:'
            end
            object ListPreisLab: TLabel
              Left = 8
              Top = 88
              Width = 68
              Height = 13
              Caption = 'Listenpreis (2):'
            end
            object FaktorLab: TLabel
              Left = 152
              Top = 67
              Width = 43
              Height = 13
              AutoSize = False
              Caption = 'Faktor:'
            end
            object BrLiPreisLab: TLabel
              Left = 152
              Top = 88
              Width = 34
              Height = 13
              Caption = 'Brutto :'
            end
            object Label59: TLabel
              Left = 152
              Top = 46
              Width = 43
              Height = 13
              AutoSize = False
              Caption = 'Bestellt:'
            end
            object Label60: TLabel
              Left = 5
              Top = 46
              Width = 74
              Height = 13
              AutoSize = False
              Caption = 'Bestellvorschl.:'
            end
            object MinMenge: TDBEdit
              Left = 78
              Top = 22
              Width = 70
              Height = 19
              AutoSize = False
              DataField = 'MENGE_MIN'
              DataSource = AS_DS
              TabOrder = 0
              OnEnter = matchEnter
              OnExit = matchExit
              OnKeyDown = matchKeyDown
              OnKeyPress = matchKeyPress
            end
            object Menge: TDBEdit
              Tag = 1
              Left = 196
              Top = 22
              Width = 70
              Height = 19
              AutoSize = False
              DataField = 'MENGE_AKT'
              DataSource = AS_DS
              TabOrder = 1
              OnChange = MengeChange
              OnEnter = matchEnter
              OnExit = matchExit
              OnKeyDown = matchKeyDown
              OnKeyPress = matchKeyPress
            end
            object EK_Preis: TDBEdit
              Left = 78
              Top = 64
              Width = 70
              Height = 19
              DataField = 'EK_PREIS'
              DataSource = AS_DS
              TabOrder = 4
              OnChange = EK_PreisChange
              OnEnter = matchEnter
              OnExit = matchExit
              OnKeyDown = matchKeyDown
              OnKeyPress = matchKeyPress
            end
            object ListPreis: TDBEdit
              Left = 78
              Top = 85
              Width = 70
              Height = 19
              DataField = 'VK5'
              DataSource = AS_DS
              TabOrder = 6
              OnChange = EK_PreisChange
              OnEnter = matchEnter
              OnExit = vk1Change
              OnKeyDown = matchKeyDown
              OnKeyPress = matchKeyPress
            end
            object GewinnFaktor: TJvxCurrencyEdit
              Tag = 1
              Left = 196
              Top = 64
              Width = 70
              Height = 19
              AutoSize = False
              DecimalPlaces = 3
              DisplayFormat = ',0.000" ";-,0.000" "'
              FormatOnEditing = True
              TabOrder = 5
              ZeroEmpty = False
              OnChange = GewinnFaktorChange
              OnEnter = matchEnter
              OnExit = matchExit
              OnKeyDown = matchKeyDown
              OnKeyPress = matchKeyPress
            end
            object BruttoPreis: TDBEdit
              Tag = 1
              Left = 196
              Top = 85
              Width = 70
              Height = 19
              AutoSize = False
              DataSource = AS_DS
              TabOrder = 7
              OnEnter = matchEnter
              OnExit = vk1_bruttoChange
              OnKeyDown = matchKeyDown
              OnKeyPress = matchKeyPress
            end
            object MEBestellt: TDBEdit
              Tag = 1
              Left = 196
              Top = 43
              Width = 70
              Height = 19
              AutoSize = False
              DataField = 'MENGE_BESTELLT'
              DataSource = AS_DS
              Enabled = False
              TabOrder = 3
              OnChange = MengeChange
              OnEnter = matchEnter
              OnExit = matchExit
              OnKeyDown = matchKeyDown
              OnKeyPress = matchKeyPress
            end
            object ME_Bestvorschlag: TDBEdit
              Left = 78
              Top = 43
              Width = 70
              Height = 19
              AutoSize = False
              DataField = 'MENGE_BVOR'
              DataSource = AS_DS
              TabOrder = 2
              OnEnter = matchEnter
              OnExit = matchExit
              OnKeyDown = matchKeyDown
              OnKeyPress = matchKeyPress
            end
          end
        end
        object Panel5: TPanel
          Left = 274
          Top = 0
          Width = 275
          Height = 486
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 1
          object KurztextGB: TCaoGroupBox
            Left = 0
            Top = 0
            Width = 275
            Height = 46
            Align = alTop
            Caption = 'Kurztext'
            Color = 14680063
            Ctl3D = False
            ParentColor = False
            ParentCtl3D = False
            TabOrder = 0
            FRameColor = clBtnFace
            object aname1: TDBEdit
              Tag = 1
              Left = 8
              Top = 21
              Width = 259
              Height = 19
              AutoSelect = False
              AutoSize = False
              DataField = 'KURZNAME'
              DataSource = AS_DS
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 0
              OnEnter = matchEnter
              OnExit = aname1Exit
              OnKeyDown = matchKeyDown
              OnKeyPress = matchKeyPress
            end
          end
          object KasTextGB: TCaoGroupBox
            Left = 0
            Top = 46
            Width = 275
            Height = 46
            Align = alTop
            Caption = 'Kasse'
            Color = 14680063
            ParentColor = False
            TabOrder = 1
            FRameColor = clBtnFace
            object kasname: TDBEdit
              Tag = 1
              Left = 8
              Top = 21
              Width = 259
              Height = 19
              AutoSelect = False
              AutoSize = False
              Ctl3D = False
              DataField = 'KAS_NAME'
              DataSource = AS_DS
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 0
              OnEnter = matchEnter
              OnExit = aname1Exit
              OnKeyDown = matchKeyDown
              OnKeyPress = matchKeyPress
            end
          end
          object InfoGB: TCaoGroupBox
            Left = 0
            Top = 323
            Width = 275
            Height = 105
            Align = alBottom
            Caption = 'Info'
            Color = 14680063
            ParentColor = False
            TabOrder = 3
            FRameColor = clBtnFace
            object InfoMemo: TDBMemo
              Tag = 1
              Left = 8
              Top = 21
              Width = 259
              Height = 76
              Hint = 'Zeilenumbruch mit Strg+Enter'
              Align = alClient
              Ctl3D = False
              DataField = 'INFO'
              DataSource = AS_DS
              ParentCtl3D = False
              ScrollBars = ssVertical
              TabOrder = 0
              OnEnter = matchEnter
              OnExit = matchExit
              OnKeyDown = matchKeyDown
              OnKeyPress = matchKeyPress
            end
          end
          object LangtextGB: TCaoGroupBox
            Left = 0
            Top = 92
            Width = 275
            Height = 231
            Align = alClient
            Caption = 'Langtext'
            Color = 14680063
            ParentColor = False
            TabOrder = 2
            FRameColor = clBtnFace
            object LtextMemo: TDBMemo
              Tag = 1
              Left = 8
              Top = 21
              Width = 259
              Height = 202
              Hint = 'Zeilenumbruch mit Strg+Enter'
              Align = alClient
              Ctl3D = False
              DataField = 'LANGNAME'
              DataSource = AS_DS
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Courier New'
              Font.Style = []
              ParentCtl3D = False
              ParentFont = False
              ScrollBars = ssVertical
              TabOrder = 0
              OnEnter = matchEnter
              OnExit = LtextMemoExit
              OnKeyDown = matchKeyDown
              OnKeyPress = matchKeyPress
            end
          end
          object SpezialGB: TCaoGroupBox
            Left = 0
            Top = 428
            Width = 275
            Height = 58
            Align = alBottom
            Caption = 'Spezial'
            Color = 14680063
            ParentColor = False
            TabOrder = 4
            FRameColor = clBtnFace
            object NO_EK: TDBCheckBox
              Left = 102
              Top = 20
              Width = 70
              Height = 17
              Caption = 'EK-Sperre'
              DataField = 'NO_EK_FLAG'
              DataSource = AS_DS
              TabOrder = 2
              ValueChecked = '1'
              ValueUnchecked = '0'
              OnEnter = matchEnter
              OnExit = matchExit
              OnKeyDown = matchKeyDown
              OnKeyPress = matchKeyPress
            end
            object NO_VK: TDBCheckBox
              Left = 102
              Top = 36
              Width = 70
              Height = 17
              Caption = 'VK-Sperre'
              DataField = 'NO_VK_FLAG'
              DataSource = AS_DS
              TabOrder = 3
              ValueChecked = '1'
              ValueUnchecked = '0'
              OnEnter = matchEnter
              OnExit = matchExit
              OnKeyDown = matchKeyDown
              OnKeyPress = matchKeyPress
            end
            object NO_BEZEICHNUNG: TDBCheckBox
              Left = 172
              Top = 20
              Width = 68
              Height = 17
              Caption = 'Bez. fest'
              DataField = 'NO_BEZEDIT_FLAG'
              DataSource = AS_DS
              TabOrder = 4
              ValueChecked = '1'
              ValueUnchecked = '0'
              OnEnter = matchEnter
              OnExit = matchExit
              OnKeyDown = matchKeyDown
              OnKeyPress = matchKeyPress
            end
            object NO_PROV: TDBCheckBox
              Left = 5
              Top = 36
              Width = 96
              Height = 17
              Caption = 'Ohne  Provision'
              DataField = 'NO_PROVISION_FLAG'
              DataSource = AS_DS
              TabOrder = 1
              ValueChecked = '1'
              ValueUnchecked = '0'
              OnEnter = matchEnter
              OnExit = matchExit
              OnKeyDown = matchKeyDown
              OnKeyPress = matchKeyPress
            end
            object NO_RABATT: TDBCheckBox
              Left = 5
              Top = 20
              Width = 96
              Height = 17
              Caption = 'Ohne  Rabatt:'
              Ctl3D = False
              DataField = 'NO_RABATT_FLAG'
              DataSource = AS_DS
              ParentCtl3D = False
              TabOrder = 0
              ValueChecked = '1'
              ValueUnchecked = '0'
              OnEnter = matchEnter
              OnExit = matchExit
              OnKeyDown = matchKeyDown
              OnKeyPress = matchKeyPress
            end
            object SerNoCB: TDBCheckBox
              Left = 172
              Top = 36
              Width = 68
              Height = 17
              Caption = 'SN-Pflicht'
              DataField = 'SN_FLAG'
              DataSource = AS_DS
              TabOrder = 5
              ValueChecked = '1'
              ValueUnchecked = '0'
              OnEnter = matchEnter
              OnExit = matchExit
              OnKeyDown = matchKeyDown
              OnKeyPress = matchKeyPress
            end
          end
        end
      end
      object Preis_TS: TTabSheet
        Tag = 2
        Caption = 'Erweitert'
        ImageIndex = 1
        object CaoGroupBox3: TCaoGroupBox
          Left = 0
          Top = 0
          Width = 549
          Height = 88
          Align = alTop
          Caption = 'Artikel'
          Color = 14680063
          Ctl3D = False
          Enabled = False
          ParentColor = False
          ParentCtl3D = False
          TabOrder = 0
          FRameColor = clBtnFace
          object Label5: TLabel
            Left = 5
            Top = 24
            Width = 74
            Height = 13
            AutoSize = False
            Caption = 'Suchbegriff:'
            FocusControl = DBEdit1
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label6: TLabel
            Left = 293
            Top = 24
            Width = 45
            Height = 13
            AutoSize = False
            Caption = 'Kurztext:'
            FocusControl = DBEdit1
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label7: TLabel
            Left = 293
            Top = 43
            Width = 45
            Height = 13
            AutoSize = False
            Caption = 'Langtext:'
            FocusControl = DBEdit3
          end
          object Label8: TLabel
            Left = 6
            Top = 66
            Width = 74
            Height = 13
            AutoSize = False
            Caption = 'Ersatz-Nr.:'
            FocusControl = DBEdit1
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label9: TLabel
            Left = 5
            Top = 45
            Width = 74
            Height = 13
            AutoSize = False
            Caption = 'Artikel-Nr.:'
            FocusControl = DBEdit3
          end
          object DBEdit1: TDBEdit
            Left = 79
            Top = 21
            Width = 206
            Height = 19
            AutoSize = False
            CharCase = ecUpperCase
            DataField = 'MATCHCODE'
            DataSource = AS_DS
            ReadOnly = True
            TabOrder = 0
            OnKeyDown = matchKeyDown
            OnKeyPress = matchKeyPress
          end
          object DBEdit2: TDBEdit
            Tag = 1
            Left = 339
            Top = 21
            Width = 202
            Height = 19
            AutoSize = False
            DataField = 'KURZNAME'
            DataSource = AS_DS
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            ReadOnly = True
            TabOrder = 1
            OnKeyDown = matchKeyDown
            OnKeyPress = matchKeyPress
          end
          object DBMemo1: TDBMemo
            Tag = 1
            Left = 339
            Top = 42
            Width = 202
            Height = 40
            DataField = 'LANGNAME'
            DataSource = AS_DS
            ReadOnly = True
            ScrollBars = ssVertical
            TabOrder = 2
          end
          object DBEdit3: TDBEdit
            Left = 79
            Top = 42
            Width = 206
            Height = 19
            AutoSize = False
            CharCase = ecUpperCase
            DataField = 'ARTNUM'
            DataSource = AS_DS
            ReadOnly = True
            TabOrder = 3
            OnKeyDown = matchKeyDown
            OnKeyPress = matchKeyPress
          end
          object DBEdit4: TDBEdit
            Left = 79
            Top = 63
            Width = 206
            Height = 19
            AutoSize = False
            BiDiMode = bdLeftToRight
            CharCase = ecUpperCase
            DataField = 'ERSATZ_ARTNUM'
            DataSource = AS_DS
            ParentBiDiMode = False
            ReadOnly = True
            TabOrder = 4
            OnKeyDown = matchKeyDown
            OnKeyPress = matchKeyPress
          end
        end
        object EKPreisGB: TCaoGroupBox
          Left = 0
          Top = 88
          Width = 549
          Height = 48
          Align = alTop
          Caption = 'EK-Preise'
          Color = 14680063
          Ctl3D = False
          ParentColor = False
          ParentCtl3D = False
          TabOrder = 1
          FRameColor = clBtnFace
          object Label3: TLabel
            Left = 5
            Top = 24
            Width = 60
            Height = 15
            AutoSize = False
            Caption = '&EK-Preis :'
            FocusControl = ekpreis
            Layout = tlCenter
          end
          object Label4: TLabel
            Left = 275
            Top = 24
            Width = 58
            Height = 13
            Alignment = taRightJustify
            AutoSize = False
            Caption = 'letzter &EK :'
            FocusControl = lastekpreis
          end
          object ekpreis: TDBEdit
            Left = 79
            Top = 21
            Width = 87
            Height = 19
            Ctl3D = False
            DataField = 'EK_PREIS'
            DataSource = AS_DS
            ParentCtl3D = False
            TabOrder = 0
            OnChange = ekpreisChange
            OnEnter = matchEnter
            OnExit = matchExit
            OnKeyDown = matchKeyDown
            OnKeyPress = matchKeyPress
          end
          object lastekpreis: TDBEdit
            Left = 338
            Top = 21
            Width = 95
            Height = 19
            DataField = 'LAST_EK'
            DataSource = AS_DS
            Enabled = False
            ReadOnly = True
            TabOrder = 1
            OnKeyDown = matchKeyDown
            OnKeyPress = matchKeyPress
          end
        end
        object VKPreisGB: TCaoGroupBox
          Left = 0
          Top = 136
          Width = 549
          Height = 350
          Align = alClient
          Caption = 'VK-Preise'
          Color = 14680063
          Ctl3D = False
          ParentColor = False
          ParentCtl3D = False
          TabOrder = 2
          FRameColor = clBtnFace
          object vk5lab: TLabel
            Left = 5
            Top = 118
            Width = 60
            Height = 19
            AutoSize = False
            Caption = 'VK-Preis &5:'
            FocusControl = vk5
            Layout = tlCenter
          end
          object vk4lab: TLabel
            Left = 5
            Top = 94
            Width = 60
            Height = 19
            AutoSize = False
            Caption = 'VK-Preis &4:'
            FocusControl = vk4
            Layout = tlCenter
          end
          object vk3lab: TLabel
            Left = 5
            Top = 70
            Width = 60
            Height = 19
            AutoSize = False
            Caption = 'VK-Preis &3:'
            FocusControl = vk3
            Layout = tlCenter
          end
          object Label2: TLabel
            Left = 5
            Top = 46
            Width = 60
            Height = 19
            AutoSize = False
            Caption = 'VK-Preis &2:'
            FocusControl = vk2
            Layout = tlCenter
          end
          object Label1: TLabel
            Left = 5
            Top = 22
            Width = 60
            Height = 19
            AutoSize = False
            Caption = 'VK-Preis &1:'
            FocusControl = vk1
            Layout = tlCenter
          end
          object Label35: TLabel
            Left = 364
            Top = 1
            Width = 65
            Height = 13
            Caption = 'Netto-Gewinn'
            Transparent = True
          end
          object Label34: TLabel
            Left = 251
            Top = 1
            Width = 83
            Height = 13
            Caption = 'Brutto (incl.MwSt)'
            Transparent = True
          end
          object MwStLabel: TLabel
            Left = 168
            Top = 1
            Width = 77
            Height = 13
            Alignment = taRightJustify
            AutoSize = False
            Caption = 'MwSt'
            Transparent = True
          end
          object Label32: TLabel
            Left = 135
            Top = 1
            Width = 26
            Height = 13
            Caption = 'Netto'
            Transparent = True
          end
          object vk5_rgw: TJvxCurrencyEdit
            Tag = -5
            Left = 338
            Top = 117
            Width = 95
            Height = 19
            AutoSize = False
            TabOrder = 19
            OnChange = vk1_rgwChange
            OnEnter = matchEnter
            OnExit = matchExit
            OnKeyDown = matchKeyDown
            OnKeyPress = matchKeyPress
          end
          object vk4_rgw: TJvxCurrencyEdit
            Tag = -4
            Left = 338
            Top = 93
            Width = 95
            Height = 19
            AutoSize = False
            TabOrder = 15
            OnChange = vk1_rgwChange
            OnEnter = matchEnter
            OnExit = matchExit
            OnKeyDown = matchKeyDown
            OnKeyPress = matchKeyPress
          end
          object vk3_rgw: TJvxCurrencyEdit
            Tag = -3
            Left = 338
            Top = 69
            Width = 95
            Height = 19
            AutoSize = False
            TabOrder = 11
            OnChange = vk1_rgwChange
            OnEnter = matchEnter
            OnExit = matchExit
            OnKeyDown = matchKeyDown
            OnKeyPress = matchKeyPress
          end
          object vk2_rgw: TJvxCurrencyEdit
            Tag = -2
            Left = 338
            Top = 45
            Width = 95
            Height = 19
            AutoSize = False
            TabOrder = 7
            OnChange = vk1_rgwChange
            OnEnter = matchEnter
            OnExit = matchExit
            OnKeyDown = matchKeyDown
            OnKeyPress = matchKeyPress
          end
          object vk1_rgw: TJvxCurrencyEdit
            Tag = -1
            Left = 338
            Top = 21
            Width = 95
            Height = 19
            AutoSize = False
            TabOrder = 3
            OnChange = vk1_rgwChange
            OnEnter = matchEnter
            OnExit = matchExit
            OnKeyDown = matchKeyDown
            OnKeyPress = matchKeyPress
          end
          object vk1_brutto: TDBEdit
            Tag = -1
            Left = 248
            Top = 21
            Width = 87
            Height = 19
            AutoSize = False
            DataField = 'VK1B'
            DataSource = AS_DS
            TabOrder = 2
            OnEnter = matchEnter
            OnExit = vk1_bruttoChange
            OnKeyDown = matchKeyDown
            OnKeyPress = matchKeyPress
          end
          object vk1_mwst: TJvxCurrencyEdit
            Tag = -1
            Left = 168
            Top = 21
            Width = 78
            Height = 19
            AutoSize = False
            Enabled = False
            ReadOnly = True
            TabOrder = 1
            Value = 99999
            OnEnter = matchEnter
            OnExit = matchExit
            OnKeyDown = matchKeyDown
            OnKeyPress = matchKeyPress
          end
          object vk2_brutto: TDBEdit
            Tag = -2
            Left = 248
            Top = 45
            Width = 87
            Height = 19
            AutoSize = False
            DataField = 'VK2B'
            DataSource = AS_DS
            TabOrder = 6
            OnEnter = matchEnter
            OnExit = vk1_bruttoChange
            OnKeyDown = matchKeyDown
            OnKeyPress = matchKeyPress
          end
          object vk3_brutto: TDBEdit
            Tag = -3
            Left = 248
            Top = 69
            Width = 87
            Height = 19
            AutoSize = False
            DataField = 'VK3B'
            DataSource = AS_DS
            TabOrder = 10
            OnEnter = matchEnter
            OnExit = vk1_bruttoChange
            OnKeyDown = matchKeyDown
            OnKeyPress = matchKeyPress
          end
          object vk4_brutto: TDBEdit
            Tag = -4
            Left = 248
            Top = 93
            Width = 87
            Height = 19
            AutoSize = False
            DataField = 'VK4B'
            DataSource = AS_DS
            TabOrder = 14
            OnEnter = matchEnter
            OnExit = vk1_bruttoChange
            OnKeyDown = matchKeyDown
            OnKeyPress = matchKeyPress
          end
          object vk5_brutto: TDBEdit
            Tag = -5
            Left = 248
            Top = 117
            Width = 87
            Height = 19
            AutoSize = False
            DataField = 'VK5B'
            DataSource = AS_DS
            TabOrder = 18
            OnEnter = matchEnter
            OnExit = vk1_bruttoChange
            OnKeyDown = matchKeyDown
            OnKeyPress = matchKeyPress
          end
          object vk5_mwst: TJvxCurrencyEdit
            Tag = -5
            Left = 168
            Top = 117
            Width = 78
            Height = 19
            AutoSize = False
            Enabled = False
            ReadOnly = True
            TabOrder = 17
            OnEnter = matchEnter
            OnExit = matchExit
            OnKeyDown = matchKeyDown
            OnKeyPress = matchKeyPress
          end
          object vk4_mwst: TJvxCurrencyEdit
            Tag = -4
            Left = 168
            Top = 93
            Width = 78
            Height = 19
            AutoSize = False
            Enabled = False
            ReadOnly = True
            TabOrder = 13
            OnEnter = matchEnter
            OnExit = matchExit
            OnKeyDown = matchKeyDown
            OnKeyPress = matchKeyPress
          end
          object vk3_mwst: TJvxCurrencyEdit
            Tag = -3
            Left = 168
            Top = 69
            Width = 78
            Height = 19
            AutoSize = False
            Enabled = False
            ReadOnly = True
            TabOrder = 9
            OnEnter = matchEnter
            OnExit = matchExit
            OnKeyDown = matchKeyDown
            OnKeyPress = matchKeyPress
          end
          object vk2_mwst: TJvxCurrencyEdit
            Tag = -2
            Left = 168
            Top = 45
            Width = 78
            Height = 19
            AutoSize = False
            Enabled = False
            ReadOnly = True
            TabOrder = 5
            OnEnter = matchEnter
            OnExit = matchExit
            OnKeyDown = matchKeyDown
            OnKeyPress = matchKeyPress
          end
          object vk2: TDBEdit
            Tag = -2
            Left = 79
            Top = 45
            Width = 87
            Height = 19
            DataField = 'VK2'
            DataSource = AS_DS
            TabOrder = 4
            OnEnter = matchEnter
            OnExit = vk1Change
            OnKeyDown = matchKeyDown
            OnKeyPress = matchKeyPress
          end
          object vk1: TDBEdit
            Tag = -1
            Left = 79
            Top = 21
            Width = 87
            Height = 19
            Ctl3D = False
            DataField = 'VK1'
            DataSource = AS_DS
            ParentCtl3D = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnEnter = matchEnter
            OnExit = vk1Change
            OnKeyDown = matchKeyDown
            OnKeyPress = matchKeyPress
          end
          object vk3: TDBEdit
            Tag = -3
            Left = 79
            Top = 69
            Width = 87
            Height = 19
            DataField = 'VK3'
            DataSource = AS_DS
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 8
            OnEnter = matchEnter
            OnExit = vk1Change
            OnKeyDown = matchKeyDown
            OnKeyPress = matchKeyPress
          end
          object vk4: TDBEdit
            Tag = -4
            Left = 79
            Top = 93
            Width = 87
            Height = 19
            DataField = 'VK4'
            DataSource = AS_DS
            TabOrder = 12
            OnEnter = matchEnter
            OnExit = vk1Change
            OnKeyDown = matchKeyDown
            OnKeyPress = matchKeyPress
          end
          object vk5: TDBEdit
            Tag = -5
            Left = 79
            Top = 117
            Width = 87
            Height = 19
            DataField = 'VK5'
            DataSource = AS_DS
            TabOrder = 16
            OnEnter = matchEnter
            OnExit = vk1Change
            OnKeyDown = matchKeyDown
            OnKeyPress = matchKeyPress
          end
        end
      end
      object SerNoTS: TTabSheet
        Tag = 3
        Caption = 'Seriennummern'
        ImageIndex = 5
        object CaoGroupBox1: TCaoGroupBox
          Left = 0
          Top = 0
          Width = 549
          Height = 88
          Align = alTop
          Caption = 'Artikel'
          Color = 14680063
          Ctl3D = False
          Enabled = False
          ParentColor = False
          ParentCtl3D = False
          TabOrder = 0
          FRameColor = clBtnFace
          object Label25: TLabel
            Left = 6
            Top = 66
            Width = 80
            Height = 13
            AutoSize = False
            Caption = 'Ersatz-Nr.:'
            FocusControl = DBEdit7
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label36: TLabel
            Left = 5
            Top = 45
            Width = 82
            Height = 13
            AutoSize = False
            Caption = 'Artikel-Nr.:'
            FocusControl = DBEdit6
          end
          object Label37: TLabel
            Left = 5
            Top = 24
            Width = 82
            Height = 13
            AutoSize = False
            Caption = 'Suchbegriff:'
            FocusControl = DBEdit7
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label38: TLabel
            Left = 293
            Top = 24
            Width = 45
            Height = 13
            AutoSize = False
            Caption = 'Kurztext:'
            FocusControl = DBEdit7
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label39: TLabel
            Left = 293
            Top = 43
            Width = 45
            Height = 13
            AutoSize = False
            Caption = 'Langtext:'
            FocusControl = DBEdit6
          end
          object DBEdit5: TDBEdit
            Left = 79
            Top = 63
            Width = 202
            Height = 19
            AutoSize = False
            BiDiMode = bdLeftToRight
            CharCase = ecUpperCase
            DataField = 'ERSATZ_ARTNUM'
            DataSource = AS_DS
            ParentBiDiMode = False
            ReadOnly = True
            TabOrder = 0
            OnKeyDown = matchKeyDown
            OnKeyPress = matchKeyPress
          end
          object DBEdit6: TDBEdit
            Left = 79
            Top = 42
            Width = 202
            Height = 19
            AutoSize = False
            CharCase = ecUpperCase
            DataField = 'ARTNUM'
            DataSource = AS_DS
            ReadOnly = True
            TabOrder = 1
            OnKeyDown = matchKeyDown
            OnKeyPress = matchKeyPress
          end
          object DBEdit7: TDBEdit
            Left = 79
            Top = 21
            Width = 202
            Height = 19
            AutoSize = False
            CharCase = ecUpperCase
            DataField = 'MATCHCODE'
            DataSource = AS_DS
            ReadOnly = True
            TabOrder = 2
            OnKeyDown = matchKeyDown
            OnKeyPress = matchKeyPress
          end
          object DBEdit8: TDBEdit
            Tag = 1
            Left = 339
            Top = 21
            Width = 202
            Height = 19
            AutoSize = False
            DataField = 'KURZNAME'
            DataSource = AS_DS
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            ReadOnly = True
            TabOrder = 3
            OnKeyDown = matchKeyDown
            OnKeyPress = matchKeyPress
          end
          object DBMemo4: TDBMemo
            Tag = 1
            Left = 339
            Top = 42
            Width = 202
            Height = 40
            DataField = 'LANGNAME'
            DataSource = AS_DS
            ReadOnly = True
            ScrollBars = ssVertical
            TabOrder = 4
          end
        end
        object CaoGroupBox15: TCaoGroupBox
          Left = 0
          Top = 88
          Width = 549
          Height = 328
          Align = alClient
          Caption = 'Liste der Seriennummern'
          Color = 14680063
          Ctl3D = True
          ParentColor = False
          ParentCtl3D = False
          TabOrder = 1
          FRameColor = clBtnFace
          object SerNoGrid: TCaoDBGrid
            Left = 8
            Top = 21
            Width = 533
            Height = 299
            Align = alClient
            BorderStyle = bsNone
            Ctl3D = True
            DataSource = SerNoDS
            DefaultDrawing = False
            Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgConfirmDelete, dgCancelOnExit]
            ParentCtl3D = False
            ReadOnly = True
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            RowColor1 = 12255087
            RowColor2 = clWindow
            ShowTitleEllipsis = True
            DefaultRowHeight = 17
            EditColor = clWindow
            Columns = <
              item
                Expanded = False
                FieldName = 'SERNUMMER'
                Width = 273
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'EINK_NUM'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'LIEF_NUM'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'VERK_NUM'
                Visible = True
              end>
          end
        end
        object CaoGroupBox16: TCaoGroupBox
          Left = 0
          Top = 416
          Width = 549
          Height = 70
          Align = alBottom
          Caption = 'Details'
          Color = 14680063
          Ctl3D = True
          ParentColor = False
          ParentCtl3D = False
          TabOrder = 2
          FRameColor = clBtnFace
        end
      end
      object Such_TS: TTabSheet
        Tag = 6
        Caption = 'Liste'
        ImageIndex = 7
        object ArtSuchGrid: TCaoDBGrid
          Left = 0
          Top = 0
          Width = 549
          Height = 486
          Align = alClient
          BorderStyle = bsNone
          DataSource = AS_DS
          DefaultDrawing = False
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          OnDblClick = ArtSuchGridDblClick
          TitleButtons = True
          OnTitleBtnClick = ArtSuchGridTitleBtnClick
          DisplayMemo = False
          RowColor1 = 14680063
          RowColor2 = clWindow
          ShowTitleEllipsis = True
          DefaultRowHeight = 16
          EditColor = 12255087
          OnApplyCellAttribute = ArtSuchGridApplyCellAttribute
          Columns = <
            item
              Expanded = False
              FieldName = 'WARENGRUPPE'
              Width = 26
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'MATCHCODE'
              Title.Caption = 'Suchbegriff'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ARTNUM'
              Width = 93
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ERSATZ_ARTNUM'
              Visible = False
            end
            item
              Expanded = False
              FieldName = 'BARCODE'
              Visible = False
            end
            item
              Expanded = False
              FieldName = 'KURZNAME'
              Width = 196
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CALC_VK1'
              Width = 63
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CALC_VK1B'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CALC_VK2'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CALC_VK2B'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CALC_VK3'
              Width = 61
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CALC_VK3B'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CALC_VK4'
              Width = 62
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CALC_VK4B'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'VK5'
              Width = 63
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'VK5B'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'STEUER_CODE'
              Width = 12
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ARTIKELTYP'
              ReadOnly = True
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'MENGE_AKT'
              Width = 47
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'MENGE_BESTELLT'
              ReadOnly = True
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ME_EINHEIT'
              Width = 49
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PR_EINHEIT'
              Visible = False
            end
            item
              Expanded = False
              FieldName = 'LAENGE'
              Visible = False
            end
            item
              Expanded = False
              FieldName = 'GROESSE'
              Visible = False
            end
            item
              Expanded = False
              FieldName = 'DIMENSION'
              Visible = False
            end
            item
              Expanded = False
              FieldName = 'GEWICHT'
              Visible = False
            end
            item
              Expanded = False
              FieldName = 'INVENTUR_WERT'
              Visible = False
            end
            item
              Expanded = False
              FieldName = 'CALC_EK'
              Width = 54
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'RABGRP_ID'
              Title.Caption = 'Rab.Gr.'
              Visible = False
            end
            item
              Expanded = False
              FieldName = 'HERKUNFSLAND'
              Width = 86
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'LAGERORT'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ERLOES_KTO'
              Width = 36
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'AUFW_KTO'
              Width = 38
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ERSTELLT'
              Visible = False
            end
            item
              Expanded = False
              FieldName = 'ERST_NAME'
              Visible = False
            end
            item
              Expanded = False
              FieldName = 'GEAEND'
              Visible = False
            end
            item
              Expanded = False
              FieldName = 'GEAEND_NAME'
              Visible = False
            end>
        end
      end
    end
    object ArtPan: TPanel
      Left = 0
      Top = 0
      Width = 691
      Height = 27
      Align = alTop
      BevelOuter = bvNone
      BorderStyle = bsSingle
      Color = clBtnShadow
      TabOrder = 1
      object ArtSuchenBtn: TJvSpeedButton
        Tag = 4
        Left = 608
        Top = 0
        Width = 45
        Height = 22
        Caption = '&Liste'
        Flat = True
        OnClick = ArtAllgemeinBtnClick
        HotTrackFont.Charset = DEFAULT_CHARSET
        HotTrackFont.Color = clWindowText
        HotTrackFont.Height = -11
        HotTrackFont.Name = 'MS Sans Serif'
        HotTrackFont.Style = []
        OnMouseEnter = ArtAllgemeinBtnMouseEnter
        OnMouseLeave = ArtAllgemeinBtnMouseLeave
      end
      object ArtMengeBtn: TJvSpeedButton
        Tag = 2
        Left = 436
        Top = 0
        Width = 61
        Height = 22
        Caption = 'Preise'
        Flat = True
        OnClick = ArtAllgemeinBtnClick
        HotTrackFont.Charset = DEFAULT_CHARSET
        HotTrackFont.Color = clWindowText
        HotTrackFont.Height = -11
        HotTrackFont.Name = 'MS Sans Serif'
        HotTrackFont.Style = []
        OnMouseEnter = ArtAllgemeinBtnMouseEnter
        OnMouseLeave = ArtAllgemeinBtnMouseLeave
      end
      object ArtAllgemeinBtn: TJvSpeedButton
        Tag = 1
        Left = 362
        Top = 0
        Width = 63
        Height = 22
        Caption = 'All&gemein'
        Flat = True
        OnClick = ArtAllgemeinBtnClick
        HotTrackFont.Charset = DEFAULT_CHARSET
        HotTrackFont.Color = clWindowText
        HotTrackFont.Height = -11
        HotTrackFont.Name = 'MS Sans Serif'
        HotTrackFont.Style = []
        OnMouseEnter = ArtAllgemeinBtnMouseEnter
        OnMouseLeave = ArtAllgemeinBtnMouseLeave
      end
      object SpeedButton1: TJvSpeedButton
        Tag = 1
        Left = 3
        Top = 1
        Width = 86
        Height = 22
        Caption = 'Artikel'
        Flat = True
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        OnClick = SpeedButton1Click
        HotTrackFont.Charset = DEFAULT_CHARSET
        HotTrackFont.Color = clWindowText
        HotTrackFont.Height = -11
        HotTrackFont.Name = 'MS Sans Serif'
        HotTrackFont.Style = []
      end
      object ArtSernoBtn: TJvSpeedButton
        Tag = 3
        Left = 504
        Top = 0
        Width = 93
        Height = 22
        Caption = 'S&eriennummern'
        Flat = True
        Visible = False
        OnClick = ArtAllgemeinBtnClick
        HotTrackFont.Charset = DEFAULT_CHARSET
        HotTrackFont.Color = clWindowText
        HotTrackFont.Height = -11
        HotTrackFont.Name = 'MS Sans Serif'
        HotTrackFont.Style = []
        OnMouseEnter = ArtAllgemeinBtnMouseEnter
        OnMouseLeave = ArtAllgemeinBtnMouseLeave
      end
    end
    object ArtikelToolBar: TToolBar
      Left = 0
      Top = 537
      Width = 505
      Height = 0
      Align = alBottom
      Anchors = []
      AutoSize = True
      ButtonWidth = 8
      Caption = 'ArtikelToolBar'
      Constraints.MaxWidth = 505
      Constraints.MinWidth = 505
      EdgeBorders = []
      EdgeInner = esNone
      EdgeOuter = esNone
      Flat = True
      TabOrder = 3
      object ToolButton2: TToolButton
        Left = 0
        Top = 0
        Width = 8
        Caption = 'ToolButton2'
        Style = tbsDivider
      end
      object ToolButton6: TToolButton
        Left = 8
        Top = 0
        Width = 8
        Caption = 'ToolButton6'
        ImageIndex = 0
        Style = tbsDivider
      end
      object ToolButton5: TToolButton
        Left = 16
        Top = 0
        Width = 8
        Caption = 'ToolButton5'
        ImageIndex = 1
        Style = tbsDivider
      end
    end
    object ArtikelToolbar1: TToolBar
      Left = 0
      Top = 537
      Width = 691
      Height = 26
      Align = alBottom
      AutoSize = True
      BorderWidth = 1
      ButtonWidth = 88
      Caption = 'ArtikelToolbar1'
      EdgeBorders = []
      EdgeInner = esNone
      EdgeOuter = esNone
      Flat = True
      Images = KMainForm.ImageList2
      List = True
      ShowCaptions = True
      TabOrder = 4
      object DBNavigator1: TDBNavigator
        Left = 0
        Top = 0
        Width = 176
        Height = 22
        DataSource = AS_DS
        VisibleButtons = [nbFirst, nbPrior, nbNext, nbLast, nbInsert, nbDelete, nbPost, nbCancel]
        Flat = True
        Hints.Strings = (
          'Erster Artikel'
          'Vorheriger Artikel'
          'N�chster Artikel'
          'Letzter Artikel'
          'Artikel einf�gen'
          'Artikel l�schen'
          'Artikel bearbeiten'
          '�bernehmen'
          'Bearbeiten abbrechen'
          'Artikel aktualisieren')
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
      end
      object Label52: TLabel
        Left = 176
        Top = 0
        Width = 50
        Height = 22
        Alignment = taCenter
        AutoSize = False
        Caption = 'Suchfeld'
        Layout = tlCenter
      end
      object SuchFeldCB: TComboBox
        Left = 226
        Top = 0
        Width = 99
        Height = 21
        Style = csDropDownList
        Ctl3D = False
        ItemHeight = 13
        ParentCtl3D = False
        TabOrder = 2
        OnChange = SuchFeldCBChange
        Items.Strings = (
          'Text'
          'Suchbegriff'
          'Artikelnummer'
          'Lieferant-Bestellnr.'
          'Info')
      end
      object Label31: TLabel
        Left = 325
        Top = 0
        Width = 62
        Height = 22
        Alignment = taCenter
        AutoSize = False
        Caption = 'Suchbegriff'
        Layout = tlCenter
      end
      object Suchbeg: TJvEdit
        Left = 387
        Top = 0
        Width = 92
        Height = 22
        Hint = 
          'durch Eingabe des Jokerzeichens % vor dem Suchtext '#13#10'kann auch n' +
          'ach Text im Suchfeld gesucht werden, '#13#10'der nicht am Anfang des F' +
          'eldes steht.'
        GroupIndex = -1
        MaxPixel.Font.Charset = DEFAULT_CHARSET
        MaxPixel.Font.Color = clWindowText
        MaxPixel.Font.Height = -11
        MaxPixel.Font.Name = 'MS Sans Serif'
        MaxPixel.Font.Style = []
        Modified = False
        SelStart = 0
        SelLength = 0
        Ctl3D = True
        ParentCtl3D = False
        ParentShowHint = False
        PasswordChar = #0
        ReadOnly = False
        ShowHint = True
        TabOrder = 1
        OnChange = SuchbegChange
        OnEnter = matchEnter
        OnExit = matchExit
        OnKeyDown = matchKeyDown
        OnKeyPress = SuchbegKeyPress
      end
      object ToolButton9: TToolButton
        Left = 479
        Top = 0
        Width = 8
        Caption = 'ToolButton9'
        ImageIndex = 0
        Style = tbsSeparator
      end
      object AddMengeLab: TLabel
        Left = 487
        Top = 0
        Width = 41
        Height = 22
        Alignment = taCenter
        AutoSize = False
        Caption = 'Menge'
        Layout = tlCenter
      end
      object AddMengeEdi: TJvSpinEdit
        Left = 528
        Top = 0
        Width = 61
        Height = 22
        ValueType = vtFloat
        TabOrder = 3
        OnEnter = matchEnter
        OnExit = matchExit
        OnKeyDown = matchKeyDown
        OnKeyPress = matchKeyPress
      end
      object Label57: TLabel
        Left = 589
        Top = 0
        Width = 6
        Height = 22
        Alignment = taCenter
        AutoSize = False
        Layout = tlCenter
      end
      object UebernahmeBtn: TToolButton
        Left = 595
        Top = 0
        AllowAllUp = True
        AutoSize = True
        Caption = '�bernehmen'
        ImageIndex = 22
        OnClick = UebernahmeBtnClick
      end
    end
    object SB1: TAdvStatusBar
      Left = 0
      Top = 563
      Width = 691
      Height = 19
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      Panels = <
        item
          Alignment = taCenter
          Text = 'Suchzeit'
          Width = 80
        end
        item
          Alignment = taCenter
          Text = 'Satz 1 von 1'
          Width = 120
        end
        item
          Text = 'Sortierung'
          Width = 140
        end
        item
          Text = 'erstellt'
          Width = 120
        end
        item
          Text = 'geaend'
          Width = 120
        end
        item
          Width = 50
        end>
      SimplePanel = False
      UseSystemFont = False
      Visible = False
    end
    object ArtWgrPan: TPanel
      Left = 0
      Top = 27
      Width = 142
      Height = 510
      Align = alLeft
      BevelInner = bvLowered
      BorderWidth = 1
      TabOrder = 2
      Visible = False
      object Panel1: TPanel
        Left = 3
        Top = 3
        Width = 136
        Height = 20
        Align = alTop
        Alignment = taLeftJustify
        BevelOuter = bvNone
        Caption = ' Hierarchie'
        Color = 14680063
        TabOrder = 1
        object ArtHirDockBtn: TSpeedButton
          Left = 111
          Top = 1
          Width = 22
          Height = 18
          Hint = 'anheften'
          Flat = True
          Glyph.Data = {
            66010000424D66010000000000007600000028000000240000000C0000000100
            040000000000F000000000000000000000001000000000000000000000000000
            8000008000000080800080000000800080008080000080808000C0C0C0000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
            DDDDDDDDDDDDDDDDDDDDDDDD0000DDDD0DDDDDDDDDDD0DDDDDDDD0DDDDDDDDDD
            0000DDDD00DDD00DDDDD00DDD00DDD000000DDDD0000DDDD0700070DDDDD0700
            070DDD0077000DDD0000DDDD0B070B0DDDDD0707070DDD07777000DD00000000
            0FBF7F0D00000778780DDD07FB0000DD0000DDDD0BFBBB0DDDDD0787770DDD0F
            B077700D0000DDDD0F000F0DDDDD0800080DDD0BF0BF770D0000DDDD00DDD00D
            DDDD00DDD00DDDD0B0FBF70D0000DDDD0DDDDDDDDDDD0DDDDDDDDDDD00BFB70D
            0000DDDDDDDDDDDDDDDDDDDDDDDDDDDDDD0000DD0000DDDDDDDDDDDDDDDDDDDD
            DDDDDDDDDDDDDDDD0000}
          NumGlyphs = 3
          OnClick = ArtHirDockBtnClick
        end
      end
      object ArtWgrTV: TTreeView
        Left = 3
        Top = 23
        Width = 136
        Height = 484
        Align = alClient
        BorderStyle = bsNone
        Color = 14680063
        HideSelection = False
        Indent = 19
        ReadOnly = True
        TabOrder = 0
        OnChange = ArtWgrTVChange
        OnDblClick = ArtWgrTVDblClick
        OnExit = ArtWgrTVExit
      end
    end
  end
  object MainMenu1: TMainMenu
    Images = KMainForm.ImageList2
    Left = 31
    Top = 102
    object Datei1: TMenuItem
      Caption = '&Datei'
      GroupIndex = 2
      object EKPreiseanzeigen1: TMenuItem
        Caption = 'EK-Preise anzeigen'
        Checked = True
        GroupIndex = 200
        ShortCut = 120
        OnClick = EKPreiseanzeigen1Click
      end
      object N3: TMenuItem
        Caption = '-'
        GroupIndex = 200
      end
      object Schlieen1: TMenuItem
        Caption = 'Schlie�en'
        GroupIndex = 200
        ImageIndex = 44
        ShortCut = 27
        OnClick = Schlieen1Click
      end
    end
    object Adresse1: TMenuItem
      Caption = '&Bearbeiten'
      GroupIndex = 3
      object Neu1: TMenuItem
        Caption = '&Neu'
        GroupIndex = 2
        ImageIndex = 23
        ShortCut = 16462
        OnClick = Neu1Click
      end
      object Kopieren1: TMenuItem
        Caption = 'Kopieren'
        GroupIndex = 2
        ImageIndex = 14
        ShortCut = 16459
        OnClick = Kopieren1Click
      end
      object Lschen1: TMenuItem
        Caption = '&L�schen'
        GroupIndex = 2
        ImageIndex = 6
        ShortCut = 16430
        OnClick = Lschen1Click
      end
      object N1: TMenuItem
        Caption = '-'
        GroupIndex = 2
      end
      object Suchen1: TMenuItem
        Caption = 'Suchen'
        GroupIndex = 2
        ImageIndex = 10
        ShortCut = 114
        OnClick = Suchen1Click
      end
      object N2: TMenuItem
        Caption = '-'
        GroupIndex = 2
      end
      object uebernehmen1: TMenuItem
        Caption = '�bernehmen'
        GroupIndex = 30
        ImageIndex = 22
        ShortCut = 123
        OnClick = UebernahmeBtnClick
      end
      object uebernSchliessen1: TMenuItem
        Caption = '�bern. + Schlie�en'
        GroupIndex = 40
        ImageIndex = 22
        ShortCut = 32891
        OnClick = uebernSchliessen1Click
      end
    end
    object Sortierung1: TMenuItem
      Caption = '&Sortierung'
      GroupIndex = 4
      object Match1: TMenuItem
        Tag = 1
        Caption = '&Suchbegriff'
        Checked = True
        Default = True
        GroupIndex = 3
        RadioItem = True
        OnClick = SortierungClick
      end
      object Name11: TMenuItem
        Tag = 2
        Caption = '&Artikelnummer'
        GroupIndex = 3
        RadioItem = True
        OnClick = SortierungClick
      end
    end
    object Ansicht1: TMenuItem
      Caption = '&Ansicht'
      GroupIndex = 6
      object Allgemein1: TMenuItem
        Tag = 1
        Caption = 'All&gemein'
        Checked = True
        GroupIndex = 4
        RadioItem = True
        ShortCut = 117
        OnClick = ArtAllgemeinBtnClick
      end
      object MengePreise1: TMenuItem
        Tag = 2
        Caption = 'Er&weitert'
        GroupIndex = 4
        RadioItem = True
        OnClick = ArtAllgemeinBtnClick
      end
      object SerNo1: TMenuItem
        Tag = 3
        Caption = 'S&eriennummern'
        GroupIndex = 4
        RadioItem = True
        OnClick = ArtAllgemeinBtnClick
      end
      object Liste1: TMenuItem
        Tag = 4
        Caption = '&Liste'
        GroupIndex = 4
        RadioItem = True
        ShortCut = 118
        OnClick = ArtAllgemeinBtnClick
      end
      object N4: TMenuItem
        Caption = '-'
        GroupIndex = 4
      end
      object Treffer1: TMenuItem
        Caption = 'Treffer'
        GroupIndex = 4
        ImageIndex = 18
        object N101: TMenuItem
          Tag = 10
          Caption = '10 Treffer'
          GroupIndex = 12
          RadioItem = True
          OnClick = alle1Click
        end
        object N501: TMenuItem
          Tag = 50
          Caption = '50 Treffer'
          Checked = True
          Default = True
          GroupIndex = 12
          RadioItem = True
          OnClick = alle1Click
        end
        object N1001: TMenuItem
          Tag = 100
          Caption = '100 Treffer'
          GroupIndex = 12
          RadioItem = True
          OnClick = alle1Click
        end
        object N2001: TMenuItem
          Tag = 200
          Caption = '200 Treffer'
          GroupIndex = 12
          RadioItem = True
          OnClick = alle1Click
        end
        object N5001: TMenuItem
          Tag = 500
          Caption = '500 Treffer'
          GroupIndex = 12
          RadioItem = True
          OnClick = alle1Click
        end
        object alle1: TMenuItem
          Tag = 999999999
          Caption = 'alle Treffer'
          GroupIndex = 12
          RadioItem = True
          OnClick = alle1Click
        end
      end
      object N6: TMenuItem
        Caption = '-'
        GroupIndex = 200
      end
      object SichtbareSpalten1: TMenuItem
        Caption = 'Sichtbare Spalten'
        GroupIndex = 200
        ImageIndex = 15
        OnClick = SichtbareSpalten1Click
      end
      object Layoutspeichern1: TMenuItem
        Caption = 'Layout speichern'
        GroupIndex = 200
        ImageIndex = 16
        OnClick = Layoutspeichern1Click
      end
      object N5: TMenuItem
        Caption = '-'
        GroupIndex = 200
      end
      object Aktualisieren1: TMenuItem
        Caption = 'Aktualisieren'
        GroupIndex = 200
        ImageIndex = 13
        ShortCut = 116
        OnClick = Aktualisieren1Click
      end
    end
  end
  object AS_DS: TDataSource
    DataSet = ASQuery
    OnDataChange = AS_DSDataChange
    Left = 95
    Top = 246
  end
  object ASQuery: TZMySqlQuery
    Database = DM1.db1
    Transaction = DM1.Transact1
    UpdateObject = ArtikelUpdateSql
    CachedUpdates = False
    ShowRecordTypes = [ztModified, ztInserted, ztUnmodified]
    Options = [doHourGlass, doQueryAllRecords, doAutoFillDefs, doQuickOpen, doUseRowId]
    LinkOptions = [loAlwaysResync]
    Constraints = <>
    OnUpdateRecord = ASQueryUpdateRecord
    AfterOpen = ASQueryAfterScroll
    BeforeInsert = ASQueryBeforeInsert
    BeforePost = ASQueryBeforePost
    AfterPost = ASQueryAfterScroll
    AfterCancel = ASQueryAfterScroll
    BeforeDelete = ASQueryBeforeDelete
    AfterDelete = ASQueryAfterScroll
    AfterScroll = ASQueryAfterScroll
    OnCalcFields = ASQueryCalcFields
    OnPostError = ASQueryPostError
    OnNewRecord = ASQueryNewRecord
    ExtraOptions = [moStoreResult]
    AlternativeView = True
    Macros = <>
    Sql.Strings = (
      'select *'
      'from ARTIKEL'
      'LIMIT 0,50')
    RequestLive = True
    Left = 31
    Top = 246
    object ASQueryREC_ID: TIntegerField
      FieldName = 'REC_ID'
    end
    object ASQueryWARENGRUPPE: TIntegerField
      DisplayLabel = 'WG'
      FieldName = 'WARENGRUPPE'
    end
    object ASQueryMATCHCODE: TStringField
      DisplayLabel = 'MATCH'
      DisplayWidth = 20
      FieldName = 'MATCHCODE'
      Size = 200
    end
    object ASQueryARTNUM: TStringField
      DisplayLabel = 'Art.-Nr.'
      FieldName = 'ARTNUM'
    end
    object ASQueryBARCODE: TStringField
      DisplayLabel = 'Barcode'
      FieldName = 'BARCODE'
    end
    object ASQueryKURZNAME: TStringField
      DisplayLabel = 'Kurzname'
      FieldName = 'KURZNAME'
      Size = 80
    end
    object ASQueryLANGNAME: TBlobField
      DisplayLabel = 'Langtext'
      FieldName = 'LANGNAME'
      BlobType = ftBlob
    end
    object ASQueryVK1: TFloatField
      DisplayLabel = 'VK-Preis 1N'
      DisplayWidth = 12
      FieldName = 'VK1'
      DisplayFormat = ',###,##0.00 "DM"'
      EditFormat = '#0.00'
    end
    object ASQueryVK1B: TFloatField
      DisplayLabel = 'VK-Preis 1B'
      FieldName = 'VK1B'
      DisplayFormat = ',###,##0.00 "DM"'
      EditFormat = '#0.00'
    end
    object ASQueryCALC_VK1: TFloatField
      DisplayLabel = 'VK-Preis 1N'
      FieldKind = fkCalculated
      FieldName = 'CALC_VK1'
      DisplayFormat = ',###,##0.00 "DM"'
      Calculated = True
    end
    object ASQueryCALC_VK1B: TFloatField
      DisplayLabel = 'VK-Preis 1B'
      FieldKind = fkCalculated
      FieldName = 'CALC_VK1B'
      DisplayFormat = ',###,##0.00 "DM"'
      Calculated = True
    end
    object ASQueryVK2: TFloatField
      DisplayLabel = 'VK-Preis 2N'
      DisplayWidth = 12
      FieldName = 'VK2'
      DisplayFormat = ',###,##0.00 "DM"'
      EditFormat = '#0.00'
    end
    object ASQueryVK2B: TFloatField
      DisplayLabel = 'VK-Preis 2B'
      FieldName = 'VK2B'
      DisplayFormat = ',###,##0.00 "DM"'
      EditFormat = '#0.00'
    end
    object ASQueryCALC_VK2: TFloatField
      DisplayLabel = 'VK-Preis 2N'
      FieldKind = fkCalculated
      FieldName = 'CALC_VK2'
      DisplayFormat = ',###,##0.00 "DM"'
      Calculated = True
    end
    object ASQueryCALC_VK2B: TFloatField
      DisplayLabel = 'VK-Preis 2B'
      FieldKind = fkCalculated
      FieldName = 'CALC_VK2B'
      DisplayFormat = ',###,##0.00 "DM"'
      Calculated = True
    end
    object ASQueryVK3: TFloatField
      DisplayLabel = 'VK-Preis 3N'
      DisplayWidth = 12
      FieldName = 'VK3'
      DisplayFormat = ',###,##0.00 "DM"'
      EditFormat = '#0.00'
    end
    object ASQueryVK3B: TFloatField
      DisplayLabel = 'VK-Preis 3B'
      FieldName = 'VK3B'
      DisplayFormat = ',###,##0.00 "DM"'
      EditFormat = '#0.00'
    end
    object ASQueryCALC_VK3: TFloatField
      DisplayLabel = 'VK-Preis 3N'
      FieldKind = fkCalculated
      FieldName = 'CALC_VK3'
      DisplayFormat = ',###,##0.00 "DM"'
      Calculated = True
    end
    object ASQueryCALC_VK3B: TFloatField
      DisplayLabel = 'VK-Preis 3B'
      FieldKind = fkCalculated
      FieldName = 'CALC_VK3B'
      DisplayFormat = ',###,##0.00 "DM"'
      Calculated = True
    end
    object ASQueryVK4: TFloatField
      DisplayLabel = 'VK-Preis 4N'
      DisplayWidth = 12
      FieldName = 'VK4'
      DisplayFormat = ',###,##0.00 "DM"'
      EditFormat = '#0.00'
    end
    object ASQueryVK4B: TFloatField
      DisplayLabel = 'VK-Preis 4B'
      FieldName = 'VK4B'
      DisplayFormat = ',###,##0.00 "DM"'
      EditFormat = '#0.00'
    end
    object ASQueryCALC_VK4: TFloatField
      DisplayLabel = 'VK-Preis 4N'
      FieldKind = fkCalculated
      FieldName = 'CALC_VK4'
      DisplayFormat = ',###,##0.00 "DM"'
      Calculated = True
    end
    object ASQueryCALC_VK4B: TFloatField
      FieldKind = fkCalculated
      FieldName = 'CALC_VK4B'
      DisplayFormat = ',###,##0.00 "DM"'
      Calculated = True
    end
    object ASQueryVK5: TFloatField
      DisplayLabel = 'VK-Preis 5N'
      FieldName = 'VK5'
      DisplayFormat = ',###,##0.00 "DM"'
      EditFormat = '#0.00'
    end
    object ASQueryVK5B: TFloatField
      DisplayLabel = 'VK-Preis 5B'
      FieldName = 'VK5B'
      EditFormat = '#0.00'
    end
    object ASQuerySTEUER_CODE: TIntegerField
      DisplayLabel = 'S'
      FieldName = 'STEUER_CODE'
      MaxValue = 3
    end
    object ASQueryARTIKELTYP: TStringField
      DisplayLabel = 'T'
      FieldName = 'ARTIKELTYP'
      Size = 1
    end
    object ASQueryME_EINHEIT: TStringField
      DisplayLabel = 'ME-Einh.'
      FieldName = 'ME_EINHEIT'
      Size = 10
    end
    object ASQueryERLOES_KTO: TIntegerField
      DisplayLabel = 'E-KTO'
      FieldName = 'ERLOES_KTO'
      DisplayFormat = ',#0;-;-'
      MaxValue = 99999
      MinValue = -1
    end
    object ASQueryAUFW_KTO: TIntegerField
      DisplayLabel = 'A-KTO'
      FieldName = 'AUFW_KTO'
      DisplayFormat = ',#0;-;-'
      EditFormat = '0'
      MaxValue = 99999
      MinValue = -1
    end
    object ASQueryMENGE_AKT: TFloatField
      DisplayLabel = 'Menge'
      FieldName = 'MENGE_AKT'
      DisplayFormat = ',#0.00'
      EditFormat = '0'
    end
    object ASQueryERSATZ_ARTNUM: TStringField
      DisplayLabel = 'Ersatz-Art.Nr.'
      FieldName = 'ERSATZ_ARTNUM'
    end
    object ASQueryKAS_NAME: TStringField
      DisplayLabel = 'Kasentext'
      FieldName = 'KAS_NAME'
      Size = 80
    end
    object ASQueryINFO: TBlobField
      DisplayLabel = 'Info'
      FieldName = 'INFO'
      BlobType = ftBlob
    end
    object ASQueryPR_EINHEIT: TFloatField
      DisplayLabel = 'PR-Einh.'
      FieldName = 'PR_EINHEIT'
    end
    object ASQueryLAENGE: TStringField
      DisplayLabel = 'L�nge'
      FieldName = 'LAENGE'
    end
    object ASQueryGROESSE: TStringField
      DisplayLabel = 'Gr��e'
      FieldName = 'GROESSE'
    end
    object ASQueryDIMENSION: TStringField
      DisplayLabel = 'Dimension'
      FieldName = 'DIMENSION'
    end
    object ASQueryGEWICHT: TFloatField
      DisplayLabel = 'Gewicht'
      FieldName = 'GEWICHT'
      DisplayFormat = ',#0.00'
      EditFormat = '0.0'
    end
    object ASQueryINVENTUR_WERT: TFloatField
      DisplayLabel = 'I-Wert'
      FieldName = 'INVENTUR_WERT'
      DisplayFormat = ',#0.0 %'
      EditFormat = '0.0'
      MaxValue = 100
    end
    object ASQueryPROVIS_PROZ: TFloatField
      FieldName = 'PROVIS_PROZ'
      DisplayFormat = ',#0.0 %'
      EditFormat = '0.0'
    end
    object ASQueryEK_PREIS: TFloatField
      DisplayLabel = 'EK-Preis'
      FieldName = 'EK_PREIS'
      DisplayFormat = '#0.000'
      EditFormat = '#0.000'
    end
    object ASQueryCALC_EK: TFloatField
      DisplayLabel = 'EK-Preis'
      FieldKind = fkCalculated
      FieldName = 'CALC_EK'
      DisplayFormat = '#0.000'
      EditFormat = '#0.000'
      Calculated = True
    end
    object ASQueryMENGE_START: TFloatField
      FieldName = 'MENGE_START'
    end
    object ASQueryMENGE_MIN: TFloatField
      FieldName = 'MENGE_MIN'
      DisplayFormat = ',#0.00'
      EditFormat = '0'
    end
    object ASQueryMENGE_BESTELLT: TFloatField
      DisplayLabel = 'Menge Best.'
      FieldName = 'MENGE_BESTELLT'
      DisplayFormat = ',#0.00'
    end
    object ASQueryMENGE_BVOR: TFloatField
      FieldName = 'MENGE_BVOR'
      DisplayFormat = ',#0.00'
      EditFormat = '0'
    end
    object ASQueryLAST_EK: TFloatField
      FieldName = 'LAST_EK'
      EditFormat = '0.00'
    end
    object ASQueryHERKUNFSLAND: TStringField
      DisplayLabel = 'Herk.-Land'
      FieldName = 'HERKUNFSLAND'
      Size = 2
    end
    object ASQueryHERSTELLER_ID: TIntegerField
      FieldName = 'HERSTELLER_ID'
    end
    object ASQueryLAGERORT: TStringField
      DisplayLabel = 'Lagerort'
      FieldName = 'LAGERORT'
    end
    object ASQueryERSTELLT: TDateField
      DisplayLabel = 'erstellt'
      FieldName = 'ERSTELLT'
      DisplayFormat = 'dd.mm.yyyy'
    end
    object ASQueryERST_NAME: TStringField
      DisplayLabel = 'erstellt von'
      FieldName = 'ERST_NAME'
    end
    object ASQueryGEAEND: TDateField
      DisplayLabel = 'le. �nderung'
      FieldName = 'GEAEND'
      DisplayFormat = 'dd.mm.yyyy'
    end
    object ASQueryGEAEND_NAME: TStringField
      DisplayLabel = 'ge�ndert von'
      FieldName = 'GEAEND_NAME'
    end
    object ASQueryRABGRP_ID: TStringField
      FieldName = 'RABGRP_ID'
      Size = 10
    end
    object ASQueryAUTODEL_FLAG: TBooleanField
      FieldName = 'AUTODEL_FLAG'
      Required = True
    end
    object ASQuerySN_FLAG: TBooleanField
      DisplayLabel = 'SN-Pflicht'
      FieldName = 'SN_FLAG'
      Required = True
    end
    object ASQueryNO_RABATT_FLAG: TBooleanField
      FieldName = 'NO_RABATT_FLAG'
      Required = True
    end
    object ASQueryNO_PROVISION_FLAG: TBooleanField
      FieldName = 'NO_PROVISION_FLAG'
      Required = True
    end
    object ASQueryNO_BEZEDIT_FLAG: TBooleanField
      FieldName = 'NO_BEZEDIT_FLAG'
      Required = True
    end
    object ASQueryNO_EK_FLAG: TBooleanField
      FieldName = 'NO_EK_FLAG'
      Required = True
    end
    object ASQueryNO_VK_FLAG: TBooleanField
      FieldName = 'NO_VK_FLAG'
      Required = True
    end
    object ASQueryALTTEIL_FLAG: TBooleanField
      DisplayLabel = 'Alt-Teil'
      FieldName = 'ALTTEIL_FLAG'
      Required = True
    end
    object ASQueryDEFAULT_LIEF_ID: TIntegerField
      FieldName = 'DEFAULT_LIEF_ID'
    end
  end
  object SearchTimer: TTimer
    Enabled = False
    Interval = 100
    OnTimer = SearchTimerTimer
    Left = 29
    Top = 148
  end
  object SerNoDS: TDataSource
    Left = 93
    Top = 295
  end
  object ArtikelUpdateSql: TZUpdateSql
    DeleteSql.Strings = (
      'delete from ARTIKEL where REC_ID=:REC_ID')
    InsertSql.Strings = (
      'INSERT INTO ARTIKEL SET '
      ''
      '  ARTNUM            =: ARTNUM,'
      '  ERSATZ_ARTNUM     =: ERSATZ_ARTNUM,'
      '  MATCHCODE         =: MATCHCODE,'
      '  WARENGRUPPE       =: WARENGRUPPE,'
      '  RABGRP_ID         =: RABGRP_ID,'
      '  BARCODE           =: BARCODE,'
      '  ARTIKELTYP        =: ARTIKELTYP,'
      '  KURZNAME          =: KURZNAME,'
      '  LANGNAME          =: LANGNAME,'
      '  KAS_NAME          =: KAS_NAME,'
      '  MEMO              =: MEMO,'
      '  INFO              =: INFO,'
      '  KAT1              =: KAT1,'
      '  KAT2              =: KAT2,'
      '  ME_EINHEIT        =: ME_EINHEIT,'
      '  PR_EINHEIT        =: PR_EINHEIT,'
      '  LAENGE            =: LAENGE,'
      '  GROESSE           =: GROESSE,'
      '  DIMENSION         =: DIMENSION,'
      '  GEWICHT           =: GEWICHT,'
      '  INVENTUR_WERT     =: INVENTUR_WERT,'
      '  EK_PREIS          =: EK_PREIS,'
      '  VK1               =: VK1,'
      '  VK1B              =: VK1B,'
      '  VK2               =: VK2,'
      '  VK2B              =: VK2B,'
      '  VK3               =: VK3,'
      '  VK3B              =: VK3B,'
      '  VK4               =: VK4,'
      '  VK4B              =: VK4B,'
      '  VK5               =: VK5,'
      '  VK5B              =: VK5B,'
      '  STEUER_CODE       =: STEUER_CODE,'
      '  ALTTEIL_FLAG      =: ALTTEIL_FLAG,'
      '  NO_RABATT_FLAG    =: NO_RABATT_FLAG,'
      '  NO_PROVISION_FLAG =: NO_PROVISION_FLAG,'
      '  NO_BEZEDIT_FLAG   =: NO_BEZEDIT_FLAG,'
      '  NO_EK_FLAG        =: NO_EK_FLAG,'
      '  NO_VK_FLAG        =: NO_VK_FLAG,'
      '  SN_FLAG           =: SN_FLAG,'
      '  AUTODEL_FLAG      =: AUTODEL_FLAG,'
      '  MENGE_START       =: MENGE_START,'
      '  MENGE_AKT         =: MENGE_AKT,'
      '  MENGE_MIN         =: MENGE_MIN,'
      '  MENGE_BESTELLT    =: MENGE_BESTELLT,'
      '  MENGE_BVOR        =: MENGE_BVOR,'
      '  MENGE_EKBEST_EDI  =: MENGE_EKBEST_EDI,'
      '  MENGE_VKRE_EDI    =: MENGE_VKRE_EDI,'
      '  MENGE_EKRE_EDI    =: MENGE_EKRE_EDI,'
      '  LAST_EK           =: LAST_EK,'
      '  LAST_LIEF         =: LAST_LIEF,'
      '  LAST_LIEFDAT      =: LAST_LIEFDAT,'
      '  LAST_VK           =: LAST_VK,'
      '  LAST_KUNDE        =: LAST_KUNDE,'
      '  LAST_VKDAT        =: LAST_VKDAT,'
      '  ERLOES_KTO        =: ERLOES_KTO,'
      '  AUFW_KTO          =: AUFW_KTO,'
      '  HERKUNFSLAND      =: HERKUNFSLAND,'
      '  HERSTELLER_ID     =: HERSTELLER_ID,'
      '  LAGERORT          =: LAGERORT,'
      '  ERSTELLT          =: ERSTELLT,'
      '  ERST_NAME         =: ERST_NAME,'
      '  GEAEND            =: GEAEND,'
      '  GEAEND_NAME       =: GEAEND_NAME,'
      '  SHOP_ID           =: SHOP_ID,'
      '  SHOP_TOP_ID       =: SHOP_TOP_ID,'
      '  SHOP_ARTIKEL_ID   =: SHOP_ARTIKEL_ID,'
      '  SHOP_ART_NR       =: SHOP_ART_NR,'
      '  SHOP_ART_NAME     =: SHOP_ART_NAME,'
      '  SHOP_KURZTEXT     =: SHOP_KURZTEXT,'
      '  SHOP_LANGTEXT     =: SHOP_LANGTEXT,'
      '  SHOP_BESCHREIBUNG =: SHOP_BESCHREIBUNG,'
      '  SHOP_SPEZIFIKATION=: SHOP_SPEZIFIKATION,'
      '  SHOP_ZUBEHOER     =: SHOP_ZUBEHOER,'
      '  SHOP_ZUBEHOER_OPT =: SHOP_ZUBEHOER_OPT,'
      '  SHOP_IMAGE_BIG    =: SHOP_IMAGE_BIG,'
      '  SHOP_IMAGE_SMALL  =: SHOP_IMAGE_SMALL,'
      '  SHOP_DATASHEET_D  =: SHOP_DATASHEET_D,'
      '  SHOP_DATASHEET_E  =: SHOP_DATASHEET_E,'
      '  SHOP_KATALOG_D    =: SHOP_KATALOG_D,'
      '  SHOP_KATALOG_E    =: SHOP_KATALOG_E,'
      '  SHOP_DRAWING      =: SHOP_DRAWING,'
      '  SHOP_HANDBUCH_D   =: SHOP_HANDBUCH_D,'
      '  SHOP_HANDBUCH_E   =: SHOP_HANDBUCH_E,'
      '  AUSSCHREIBUNGSTEXT=: AUSSCHREIBUNGSTEXT,'
      '  SHOP_PREIS_EK     =: SHOP_PREIS_EK,'
      '  SHOP_PREIS_LISTE  =: SHOP_PREIS_LISTE,'
      '  SHOP_PREIS_SPEC   =: SHOP_PREIS_SPEC,'
      '  SHOP_EK_RABATT    =: SHOP_EK_RABATT,'
      '  SHOP_VISIBLE      =: SHOP_VISIBLE,'
      '  SHOP_DATE_NEU     =: SHOP_DATE_NEU,'
      '  SHOP_FAELLT_WEG   =: SHOP_FAELLT_WEG,'
      '  SHOP_FAELLT_WEG_AB=: SHOP_FAELLT_WEG_AB,'
      '  SHOP_CLICK_COUNT  =: SHOP_CLICK_COUNT,'
      '  SHOP_SYNC         =: SHOP_SYNC,'
      '  SHOP_ZUB          =: SHOP_ZUB,'
      '  SHOP_CHANGE_DATE  =: SHOP_CHANGE_DATE,'
      '  SHOP_CHANGE_FLAG  =: SHOP_CHANGE_FLAG,'
      '  SHOP_DEL_FLAG     =: SHOP_DEL_FLAG')
    ModifySql.Strings = (
      'update ARTIKEL SET '
      ''
      '    ARTNUM            =: NEW_ARTNUM,'
      '  ERSATZ_ARTNUM     =: ERSATZ_ARTNUM,'
      '  MATCHCODE         =: MATCHCODE,'
      '  WARENGRUPPE       =: WARENGRUPPE,'
      '  RABGRP_ID         =: RABGRP_ID,'
      '  BARCODE           =: BARCODE,'
      '  ARTIKELTYP        =: ARTIKELTYP,'
      '  KURZNAME          =: KURZNAME,'
      '  LANGNAME          =: LANGNAME,'
      '  KAS_NAME          =: KAS_NAME,'
      '  MEMO              =: MEMO,'
      '  INFO              =: INFO,'
      '  KAT1              =: KAT1,'
      '  KAT2              =: KAT2,'
      '  ME_EINHEIT        =: ME_EINHEIT,'
      '  PR_EINHEIT        =: PR_EINHEIT,'
      '  LAENGE            =: LAENGE,'
      '  GROESSE           =: GROESSE,'
      '  DIMENSION         =: DIMENSION,'
      '  GEWICHT           =: GEWICHT,'
      '  INVENTUR_WERT     =: INVENTUR_WERT,'
      '  EK_PREIS          =: EK_PREIS,'
      '  VK1               =: VK1,'
      '  VK1B              =: VK1B,'
      '  VK2               =: VK2,'
      '  VK2B              =: VK2B,'
      '  VK3               =: VK3,'
      '  VK3B              =: VK3B,'
      '  VK4               =: VK4,'
      '  VK4B              =: VK4B,'
      '  VK5               =: VK5,'
      '  VK5B              =: VK5B,'
      '  STEUER_CODE       =: STEUER_CODE,'
      '  ALTTEIL_FLAG      =: ALTTEIL_FLAG,'
      '  NO_RABATT_FLAG    =: NO_RABATT_FLAG,'
      '  NO_PROVISION_FLAG =: NO_PROVISION_FLAG,'
      '  NO_BEZEDIT_FLAG   =: NO_BEZEDIT_FLAG,'
      '  NO_EK_FLAG        =: NO_EK_FLAG,'
      '  NO_VK_FLAG        =: NO_VK_FLAG,'
      '  SN_FLAG           =: SN_FLAG,'
      '  AUTODEL_FLAG      =: AUTODEL_FLAG,'
      '  MENGE_START       =: MENGE_START,'
      '  MENGE_AKT         =: MENGE_AKT,'
      '  MENGE_MIN         =: MENGE_MIN,'
      '  MENGE_BESTELLT    =: MENGE_BESTELLT,'
      '  MENGE_BVOR        =: MENGE_BVOR,'
      '  MENGE_EKBEST_EDI  =: MENGE_EKBEST_EDI,'
      '  MENGE_VKRE_EDI    =: MENGE_VKRE_EDI,'
      '  MENGE_EKRE_EDI    =: MENGE_EKRE_EDI,'
      '  LAST_EK           =: LAST_EK,'
      '  LAST_LIEF         =: LAST_LIEF,'
      '  LAST_LIEFDAT      =: LAST_LIEFDAT,'
      '  LAST_VK           =: LAST_VK,'
      '  LAST_KUNDE        =: LAST_KUNDE,'
      '  LAST_VKDAT        =: LAST_VKDAT,'
      '  ERLOES_KTO        =: ERLOES_KTO,'
      '  AUFW_KTO          =: AUFW_KTO,'
      '  HERKUNFSLAND      =: HERKUNFSLAND,'
      '  HERSTELLER_ID     =: HERSTELLER_ID,'
      '  LAGERORT          =: LAGERORT,'
      '  ERSTELLT          =: ERSTELLT,'
      '  ERST_NAME         =: ERST_NAME,'
      '  GEAEND            =: GEAEND,'
      '  GEAEND_NAME       =: GEAEND_NAME,'
      '  SHOP_ID           =: SHOP_ID,'
      '  SHOP_TOP_ID       =: SHOP_TOP_ID,'
      '  SHOP_ARTIKEL_ID   =: SHOP_ARTIKEL_ID,'
      '  SHOP_ART_NR       =: SHOP_ART_NR,'
      '  SHOP_ART_NAME     =: SHOP_ART_NAME,'
      '  SHOP_KURZTEXT     =: SHOP_KURZTEXT,'
      '  SHOP_LANGTEXT     =: SHOP_LANGTEXT,'
      '  SHOP_BESCHREIBUNG =: SHOP_BESCHREIBUNG,'
      '  SHOP_SPEZIFIKATION=: SHOP_SPEZIFIKATION,'
      '  SHOP_ZUBEHOER     =: SHOP_ZUBEHOER,'
      '  SHOP_ZUBEHOER_OPT =: SHOP_ZUBEHOER_OPT,'
      '  SHOP_IMAGE_BIG    =: SHOP_IMAGE_BIG,'
      '  SHOP_IMAGE_SMALL  =: SHOP_IMAGE_SMALL,'
      '  SHOP_DATASHEET_D  =: SHOP_DATASHEET_D,'
      '  SHOP_DATASHEET_E  =: SHOP_DATASHEET_E,'
      '  SHOP_KATALOG_D    =: SHOP_KATALOG_D,'
      '  SHOP_KATALOG_E    =: SHOP_KATALOG_E,'
      '  SHOP_DRAWING      =: SHOP_DRAWING,'
      '  SHOP_HANDBUCH_D   =: SHOP_HANDBUCH_D,'
      '  SHOP_HANDBUCH_E   =: SHOP_HANDBUCH_E,'
      '  AUSSCHREIBUNGSTEXT=: AUSSCHREIBUNGSTEXT,'
      '  SHOP_PREIS_EK     =: SHOP_PREIS_EK,'
      '  SHOP_PREIS_LISTE  =: SHOP_PREIS_LISTE,'
      '  SHOP_PREIS_SPEC   =: SHOP_PREIS_SPEC,'
      '  SHOP_EK_RABATT    =: SHOP_EK_RABATT,'
      '  SHOP_VISIBLE      =: SHOP_VISIBLE,'
      '  SHOP_DATE_NEU     =: SHOP_DATE_NEU,'
      '  SHOP_FAELLT_WEG   =: SHOP_FAELLT_WEG,'
      '  SHOP_FAELLT_WEG_AB=: SHOP_FAELLT_WEG_AB,'
      '  SHOP_CLICK_COUNT  =: SHOP_CLICK_COUNT,'
      '  SHOP_SYNC         =: SHOP_SYNC,'
      '  SHOP_ZUB          =: SHOP_ZUB,'
      '  SHOP_CHANGE_DATE  =: SHOP_CHANGE_DATE,'
      '  SHOP_CHANGE_FLAG  =: SHOP_CHANGE_FLAG,'
      '  SHOP_DEL_FLAG     =: SHOP_DEL_FLAG'
      ''
      'where REC_ID = :REC_ID')
    Left = 28
    Top = 346
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'LANGNAME'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'REC_ID'
        ParamType = ptUnknown
      end>
  end
  object SerNoTab: TZMySqlQuery
    Database = DM1.db1
    Transaction = DM1.Transact1
    CachedUpdates = False
    ShowRecordTypes = [ztModified, ztInserted, ztUnmodified]
    Options = [doHourGlass, doAutoFillDefs, doUseRowId]
    LinkOptions = [loAlwaysResync]
    Constraints = <>
    ExtraOptions = [moStoreResult]
    Macros = <>
    Sql.Strings = (
      'select * from ARTIKEL_SERNUM'
      'where ARTIKEL_ID =:ID')
    RequestLive = False
    Left = 29
    Top = 295
    ParamData = <
      item
        DataType = ftInteger
        Name = 'ID'
        ParamType = ptInput
      end>
    object SerNoTabARTIKEL_ID: TIntegerField
      FieldName = 'ARTIKEL_ID'
      Required = True
      Visible = False
    end
    object SerNoTabSERNUMMER: TStringField
      DisplayLabel = 'Seriennummer'
      DisplayWidth = 84
      FieldName = 'SERNUMMER'
      Required = True
      Size = 255
    end
    object SerNoTabEINK_NUM: TIntegerField
      DisplayLabel = 'Einkauf'
      DisplayWidth = 12
      FieldName = 'EINK_NUM'
      Required = True
      DisplayFormat = '0;---;---'
    end
    object SerNoTabLIEF_NUM: TIntegerField
      DisplayLabel = 'Lieferschein'
      DisplayWidth = 12
      FieldName = 'LIEF_NUM'
      Required = True
      DisplayFormat = '0;---;---'
    end
    object SerNoTabVERK_NUM: TIntegerField
      DisplayLabel = 'Rechnung'
      DisplayWidth = 12
      FieldName = 'VERK_NUM'
      Required = True
      DisplayFormat = '0;---;---'
    end
    object SerNoTabSNUM_ID: TIntegerField
      FieldName = 'SNUM_ID'
      Visible = False
    end
  end
  object FormStorage1: TJvFormStorage
    IniFileName = '\SOFTWARE\SBP\CAO-Faktura'
    IniSection = 'KASSE_ARTIKEL'
    MinMaxInfo.MinTrackHeight = 600
    MinMaxInfo.MinTrackWidth = 705
    UseRegistry = True
    Version = 1000
    StoredValues = <>
    Left = 93
    Top = 105
  end
end
