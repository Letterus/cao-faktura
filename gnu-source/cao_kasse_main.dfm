object KMainForm: TKMainForm
  Left = 233
  Top = 164
  Width = 795
  Height = 522
  Caption = 'CAO-Kasse'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Menu = MainMenu1
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 16
  object SB1: TAdvStatusBar
    Left = 0
    Top = 457
    Width = 787
    Height = 19
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    Panels = <
      item
        Alignment = taCenter
        Text = 'Anmeldename'
        Width = 160
      end
      item
        Alignment = taCenter
        Text = 'DM'
        Width = 45
      end
      item
        Alignment = taCenter
        Text = 'Aufl�sung'
        Width = 90
      end
      item
        Alignment = taCenter
        Text = 'alle Preise Brutto'
        Width = 140
      end
      item
        Alignment = taCenter
        Text = 'Datum'
        Width = 220
      end
      item
        Alignment = taCenter
        Text = 'Preisebene : 2'
        Width = 120
      end
      item
        Width = 50
      end>
    SimplePanel = False
    UseSystemFont = False
  end
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 787
    Height = 346
    Align = alClient
    BevelOuter = bvNone
    BorderWidth = 1
    Enabled = False
    TabOrder = 1
    object ExRxDBGrid1: TExRxDBGrid
      Left = 1
      Top = 1
      Width = 785
      Height = 344
      Align = alClient
      BorderStyle = bsNone
      DataSource = PosDS
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -13
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Controls = <>
      ScrollBars = ssHorizontal
      EditColor = clWindow
      DefaultRowHeight = 20
      GridAutoWidth = awProportional
      RowColor1 = 12255087
      RowColor2 = clWindow
      UseRowColors = True
      HighlightColor = clNavy
      ImageHighlightColor = clWindow
      HighlightFontColor = clWhite
      HotTrackColor = clNavy
      LockedCols = 0
      LockedFont.Charset = DEFAULT_CHARSET
      LockedFont.Color = clWindowText
      LockedFont.Height = -11
      LockedFont.Name = 'MS Sans Serif'
      LockedFont.Style = []
      LockedColor = clGray
      ShowTextEllipsis = True
      ShowTitleEllipsis = True
      ExMenuOptions = [exAutoSize, exAutoWidth, exDisplayBoolean, exDisplayImages, exDisplayMemo, exDisplayDateTime, exShowTextEllipsis, exShowTitleEllipsis, exFullSizeMemo, exAllowRowSizing, exCellHints, exMultiLineTitles, exUseRowColors, exFixedColumns, exPrintGrid, exPrintDataSet, exExportGrid, exSelectAll, exUnSelectAll, exQueryByForm, exSortByForm, exMemoInplaceEditors, exCustomize, exSearchMode, exSaveLayout, exLoadLayout]
      MaskedColumnDrag = True
      ValueChecked = 1
      ValueUnChecked = 0
      Columns = <
        item
          Expanded = False
          FieldName = 'POSITION'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -13
          Title.Font.Name = 'MS Sans Serif'
          Title.Font.Style = [fsBold]
          Width = 35
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MENGE'
          Title.Caption = 'Menge'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -13
          Title.Font.Name = 'MS Sans Serif'
          Title.Font.Style = [fsBold]
          Width = 53
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'BARCODE'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -13
          Title.Font.Name = 'MS Sans Serif'
          Title.Font.Style = [fsBold]
          Width = 116
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ARTNUM'
          Title.Caption = 'Artikelnr.'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -13
          Title.Font.Name = 'MS Sans Serif'
          Title.Font.Style = [fsBold]
          Width = 89
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'BEZEICHNUNG'
          Title.Caption = 'Bezeichnung'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -13
          Title.Font.Name = 'MS Sans Serif'
          Title.Font.Style = [fsBold]
          Width = 268
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'EPREIS'
          Title.Alignment = taCenter
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -13
          Title.Font.Name = 'MS Sans Serif'
          Title.Font.Style = [fsBold]
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'BSUMME'
          Title.Alignment = taCenter
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -13
          Title.Font.Name = 'MS Sans Serif'
          Title.Font.Style = [fsBold]
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MwstProz'
          Title.Alignment = taRightJustify
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -13
          Title.Font.Name = 'MS Sans Serif'
          Title.Font.Style = [fsBold]
          Width = 55
          Visible = True
        end>
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 346
    Width = 787
    Height = 111
    Align = alBottom
    AutoSize = True
    BevelOuter = bvNone
    TabOrder = 2
    object ToolBar1: TToolBar
      Left = 0
      Top = 77
      Width = 787
      Height = 34
      Align = alBottom
      AutoSize = True
      BorderWidth = 2
      ButtonHeight = 26
      ButtonWidth = 168
      Caption = 'ToolBar1'
      Color = clBtnFace
      EdgeBorders = []
      EdgeInner = esNone
      EdgeOuter = esNone
      Flat = True
      Images = ImageList1
      List = True
      ParentColor = False
      ShowCaptions = True
      TabOrder = 0
      Wrapable = False
      object StornoBtn: TToolButton
        Left = 0
        Top = 0
        AutoSize = True
        Caption = 'letzte Pos. l�schen (F2)'
        ImageIndex = 1
        OnClick = StornoBtnClick
      end
      object ToolButton3: TToolButton
        Left = 172
        Top = 0
        Width = 8
        Caption = 'ToolButton3'
        ImageIndex = 15
        Style = tbsSeparator
      end
      object AddBtn: TToolButton
        Left = 180
        Top = 0
        AutoSize = True
        Caption = 'Pos. Suchen (F3)'
        ImageIndex = 0
        OnClick = Suchen1Click
      end
      object ToolButton5: TToolButton
        Left = 316
        Top = 0
        Width = 8
        Caption = 'ToolButton5'
        ImageIndex = 4
        Style = tbsSeparator
      end
      object BonPrintBtn: TToolButton
        Left = 324
        Top = 0
        AutoSize = True
        Caption = 'le. Bon drucken (F8)'
        ImageIndex = 13
        OnClick = letztenBondrucken1Click
      end
      object ToolButton2: TToolButton
        Left = 477
        Top = 0
        Width = 8
        Caption = 'ToolButton2'
        ImageIndex = 14
        Style = tbsSeparator
      end
      object BonFertigBtn: TToolButton
        Left = 485
        Top = 0
        AutoSize = True
        Caption = 'Fertigstellen (F11)'
        ImageIndex = 11
        OnClick = BonFertigBtnClick
      end
      object ToolButton1: TToolButton
        Left = 626
        Top = 0
        Width = 8
        Caption = 'ToolButton1'
        ImageIndex = 12
        Style = tbsSeparator
      end
      object ToolButton4: TToolButton
        Left = 634
        Top = 0
        AutoSize = True
        Caption = 'Prog. beenden'
        ImageIndex = 14
        OnClick = Beenden1Click
      end
    end
    object CaoGroupBox1: TCaoGroupBox
      Left = 0
      Top = 28
      Width = 787
      Height = 49
      Align = alBottom
      Caption = 'Ihre Eingabe'
      Color = 14680063
      ParentColor = False
      TabOrder = 1
      FRameColor = clBtnFace
      object Label5: TLabel
        Left = 8
        Top = 24
        Width = 57
        Height = 17
        Align = alLeft
        AutoSize = False
        Caption = 'Menge'
        Layout = tlCenter
      end
      object MengeLab: TLabel
        Left = 65
        Top = 24
        Width = 57
        Height = 17
        Align = alLeft
        Alignment = taRightJustify
        AutoSize = False
        Caption = '1'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        Layout = tlCenter
      end
      object Edit1: TEdit
        Tag = 1
        Left = 136
        Top = 22
        Width = 643
        Height = 21
        BorderStyle = bsNone
        Ctl3D = False
        ParentCtl3D = False
        TabOrder = 0
        OnKeyDown = Edit1KeyDown
        OnKeyPress = Edit1KeyPress
      end
    end
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 787
      Height = 28
      Align = alBottom
      BevelOuter = bvNone
      BorderWidth = 3
      TabOrder = 2
      object Label1: TLabel
        Left = 203
        Top = 3
        Width = 55
        Height = 22
        Align = alRight
        Caption = ' Netto '
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        Layout = tlCenter
      end
      object Label2: TLabel
        Left = 398
        Top = 3
        Width = 55
        Height = 22
        Align = alRight
        Caption = ' MwSt '
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        Layout = tlCenter
      end
      object Label3: TLabel
        Left = 583
        Top = 3
        Width = 61
        Height = 22
        Align = alRight
        Caption = ' Brutto '
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        Layout = tlCenter
      end
      object Label4: TLabel
        Left = 3
        Top = 3
        Width = 76
        Height = 22
        Align = alLeft
        Caption = ' Summen'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        Layout = tlCenter
      end
      object Panel4: TPanel
        Left = 644
        Top = 3
        Width = 140
        Height = 22
        Align = alRight
        BevelInner = bvLowered
        BevelOuter = bvNone
        TabOrder = 0
        object DBText6: TDBText
          Left = 1
          Top = 1
          Width = 138
          Height = 20
          Align = alClient
          Alignment = taRightJustify
          DataField = 'BSUMME'
          DataSource = KopfDS
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
      end
      object Panel5: TPanel
        Left = 453
        Top = 3
        Width = 130
        Height = 22
        Align = alRight
        BevelInner = bvLowered
        BevelOuter = bvNone
        TabOrder = 1
        object DBText4: TDBText
          Left = 1
          Top = 1
          Width = 128
          Height = 20
          Align = alClient
          Alignment = taRightJustify
          DataField = 'MSUMME'
          DataSource = KopfDS
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
      end
      object Panel6: TPanel
        Left = 258
        Top = 3
        Width = 140
        Height = 22
        Align = alRight
        BevelInner = bvLowered
        BevelOuter = bvNone
        TabOrder = 2
        object DBText1: TDBText
          Left = 1
          Top = 1
          Width = 138
          Height = 20
          Align = alClient
          Alignment = taRightJustify
          DataField = 'NSUMME'
          DataSource = KopfDS
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
      end
    end
  end
  object MainMenu1: TMainMenu
    Images = ImageList2
    OwnerDraw = True
    Left = 248
    Top = 120
    object Datei1: TMenuItem
      Caption = '&Datei'
      object Mandant1: TMenuItem
        Caption = '&Mandant'
        ImageIndex = 28
        OnClick = Wechseln1Click
      end
      object N1: TMenuItem
        Caption = '-'
      end
      object Suchmodus1: TMenuItem
        Caption = 'Suchmodus'
        ImageIndex = 10
        object Barcode1: TMenuItem
          Tag = 1
          Caption = 'Barcode'
          Checked = True
          Default = True
          GroupIndex = 2
          RadioItem = True
          OnClick = Artikelnummer1Click
        end
        object Matchcode1: TMenuItem
          Tag = 2
          Caption = 'Matchcode'
          GroupIndex = 2
          RadioItem = True
          OnClick = Artikelnummer1Click
        end
        object Artikelnummer1: TMenuItem
          Tag = 3
          Caption = 'Artikelnummer'
          GroupIndex = 2
          RadioItem = True
          OnClick = Artikelnummer1Click
        end
      end
      object N6: TMenuItem
        Caption = '-'
      end
      object EinstellungenKasse1: TMenuItem
        Caption = 'Einstellungen Kasse'
        ImageIndex = 24
        OnClick = EinstellungenKasse1Click
      end
      object Einstellungen1: TMenuItem
        Caption = 'allgemeine Einstellungen'
        ImageIndex = 24
        OnClick = Einstellungen1Click
      end
      object N2: TMenuItem
        Caption = '-'
      end
      object Beenden1: TMenuItem
        Caption = 'Beenden'
        ImageIndex = 44
        OnClick = Beenden1Click
      end
    end
    object Bearbeiten1: TMenuItem
      Caption = '&Bearbeiten'
      object lePositionlschen1: TMenuItem
        Caption = 'le. Position l�schen'
        ImageIndex = 32
        ShortCut = 113
        OnClick = StornoBtnClick
      end
      object N3: TMenuItem
        Caption = '-'
      end
      object Suchen1: TMenuItem
        Caption = 'Suchen'
        ImageIndex = 15
        ShortCut = 114
        OnClick = Suchen1Click
      end
      object N4: TMenuItem
        Caption = '-'
      end
      object Fertigstellen1: TMenuItem
        Caption = 'Fertigstellen'
        ImageIndex = 12
        ShortCut = 122
        OnClick = BonFertigBtnClick
      end
      object N5: TMenuItem
        Caption = '-'
      end
      object letztenBondrucken1: TMenuItem
        Caption = 'letzten Bon drucken'
        ImageIndex = 11
        ShortCut = 119
        OnClick = letztenBondrucken1Click
      end
      object Schubladeffnen1: TMenuItem
        Caption = 'Schublade �ffnen'
        ImageIndex = 18
        ShortCut = 32887
        OnClick = Schubladeffnen1Click
      end
    end
    object Ansicht1: TMenuItem
      Caption = '&Ansicht'
      object Log1: TMenuItem
        Caption = 'Log'
        GroupIndex = 6
        ImageIndex = 15
        OnClick = Log1Click
      end
      object REgistery1: TMenuItem
        Caption = 'SQL-Registery'
        GroupIndex = 7
        ImageIndex = 25
        OnClick = REgistery1Click
      end
    end
    object Hilfe1: TMenuItem
      Caption = '&Hilfe'
      object Info1: TMenuItem
        Caption = 'Info'
        ImageIndex = 47
      end
    end
  end
  object ZMonitor1: TZMonitor
    Transaction = DM1.Transact1
    OnMonitorEvent = ZMonitor1MonitorEvent
    Left = 203
    Top = 120
  end
  object PosDS: TDataSource
    DataSet = PosTab
    Left = 248
    Top = 176
  end
  object PosTab: TZMySqlQuery
    Database = DM1.db1
    Transaction = DM1.Transact1
    CachedUpdates = False
    ShowRecordTypes = [ztModified, ztInserted, ztUnmodified]
    Options = [doHourGlass, doQueryAllRecords, doAutoFillDefs, doUseRowId]
    LinkOptions = [loAlwaysResync]
    Constraints = <>
    AfterOpen = PosTabAfterScroll
    BeforePost = PosTabBeforePost
    AfterPost = PosTabAfterPost
    BeforeDelete = PosTabBeforeDelete
    AfterDelete = PosTabAfterPost
    AfterScroll = PosTabAfterScroll
    OnCalcFields = PosTabCalcFields
    OnNewRecord = PosTabNewRecord
    ExtraOptions = [moStoreResult]
    AlternativeView = True
    Macros = <>
    Sql.Strings = (
      'select * '
      'from JOURNALPOS'
      'where JOURNAL_ID=:ID')
    RequestLive = True
    Left = 200
    Top = 176
    ParamData = <
      item
        DataType = ftInteger
        Name = 'ID'
        ParamType = ptInput
        Value = 13878
      end>
    object PosTabREC_ID: TIntegerField
      FieldName = 'REC_ID'
    end
    object PosTabQUELLE: TIntegerField
      FieldName = 'QUELLE'
    end
    object PosTabQUELLE_SUB: TIntegerField
      FieldName = 'QUELLE_SUB'
    end
    object PosTabJOURNAL_ID: TIntegerField
      FieldName = 'JOURNAL_ID'
      Required = True
    end
    object PosTabARTIKELTYP: TStringField
      FieldName = 'ARTIKELTYP'
      Size = 1
    end
    object PosTabARTIKEL_ID: TIntegerField
      FieldName = 'ARTIKEL_ID'
      Required = True
    end
    object PosTabADDR_ID: TIntegerField
      FieldName = 'ADDR_ID'
      Required = True
    end
    object PosTabVRENUM: TIntegerField
      FieldName = 'VRENUM'
    end
    object PosTabVLSNUM: TIntegerField
      FieldName = 'VLSNUM'
    end
    object PosTabPOSITION: TIntegerField
      DisplayLabel = 'Pos.'
      FieldName = 'POSITION'
      Required = True
    end
    object PosTabMATCHCODE: TStringField
      FieldName = 'MATCHCODE'
    end
    object PosTabARTNUM: TStringField
      FieldName = 'ARTNUM'
    end
    object PosTabBARCODE: TStringField
      DisplayLabel = 'Barcode / EAN'
      FieldName = 'BARCODE'
    end
    object PosTabMENGE: TFloatField
      FieldName = 'MENGE'
      DisplayFormat = ',###,##0.0'
    end
    object PosTabLAENGE: TStringField
      FieldName = 'LAENGE'
    end
    object PosTabGROESSE: TStringField
      FieldName = 'GROESSE'
    end
    object PosTabDIMENSION: TStringField
      FieldName = 'DIMENSION'
    end
    object PosTabGEWICHT: TFloatField
      FieldName = 'GEWICHT'
    end
    object PosTabME_EINHEIT: TStringField
      FieldName = 'ME_EINHEIT'
      Size = 10
    end
    object PosTabPR_EINHEIT: TFloatField
      FieldName = 'PR_EINHEIT'
    end
    object PosTabEPREIS: TFloatField
      DisplayLabel = 'E-Preis'
      FieldName = 'EPREIS'
      DisplayFormat = ',###,##0.00'
    end
    object PosTabE_RGEWINN: TFloatField
      FieldName = 'E_RGEWINN'
    end
    object PosTabRABATT: TFloatField
      FieldName = 'RABATT'
    end
    object PosTabSTEUER_CODE: TIntegerField
      FieldName = 'STEUER_CODE'
      MaxValue = 3
    end
    object PosTabALTTEIL_PROZ: TFloatField
      FieldName = 'ALTTEIL_PROZ'
    end
    object PosTabALTTEIL_STCODE: TIntegerField
      FieldName = 'ALTTEIL_STCODE'
    end
    object PosTabGEGENKTO: TIntegerField
      FieldName = 'GEGENKTO'
    end
    object PosTabBEZEICHNUNG: TMemoField
      FieldName = 'BEZEICHNUNG'
      BlobType = ftMemo
    end
    object PosTabVIEW_POS: TStringField
      FieldName = 'VIEW_POS'
      Size = 3
    end
    object PosTabATRNUM2: TIntegerField
      DisplayLabel = 'Artikelnummer'
      FieldName = 'ATRNUM'
    end
    object PosTabMwstProz: TFloatField
      DisplayLabel = 'MwSt'
      FieldKind = fkCalculated
      FieldName = 'MwstProz'
      DisplayFormat = '0.0"%"'
      Calculated = True
    end
    object PosTabGEBUCHT: TBooleanField
      FieldName = 'GEBUCHT'
      Required = True
    end
    object PosTabALTTEIL_FLAG: TBooleanField
      FieldName = 'ALTTEIL_FLAG'
      Required = True
    end
    object PosTabBEZ_FEST_FLAG: TBooleanField
      FieldName = 'BEZ_FEST_FLAG'
      Required = True
    end
    object PosTabSN_FLAG: TBooleanField
      FieldName = 'SN_FLAG'
      Required = True
    end
    object PosTabBRUTTO_FLAG: TBooleanField
      FieldName = 'BRUTTO_FLAG'
      Required = True
    end
    object PosTabNSUMME: TFloatField
      FieldKind = fkCalculated
      FieldName = 'NSUMME'
      Calculated = True
    end
    object PosTabMSUMME: TFloatField
      FieldKind = fkCalculated
      FieldName = 'MSUMME'
      Calculated = True
    end
    object PosTabBSUMME: TFloatField
      DisplayLabel = 'G-Preis'
      FieldKind = fkCalculated
      FieldName = 'BSUMME'
      DisplayFormat = ',###,##0.00'
      Calculated = True
    end
    object PosTabPROVIS_PROZ: TFloatField
      FieldName = 'PROVIS_PROZ'
      Required = True
    end
    object PosTabPROVIS_WERT: TFloatField
      FieldName = 'PROVIS_WERT'
      Required = True
    end
    object PosTabVPE: TIntegerField
      FieldName = 'VPE'
    end
  end
  object KopfTab: TZMySqlQuery
    Database = DM1.db1
    Transaction = DM1.Transact1
    CachedUpdates = False
    ShowRecordTypes = [ztModified, ztInserted, ztUnmodified]
    Options = [doHourGlass, doAutoFillDefs, doUseRowId]
    LinkOptions = [loAlwaysResync]
    Constraints = <>
    AfterOpen = KopfTabAfterOpen
    BeforePost = KopfTabAfterOpen
    AfterScroll = KopfTabAfterOpen
    ExtraOptions = [moStoreResult]
    AlternativeView = True
    Macros = <>
    Sql.Strings = (
      'select * '
      'from JOURNAL '
      'where QUELLE=13 and QUELLE_SUB=2 and TERM_ID=:TID'
      'limit 0,1')
    RequestLive = True
    Left = 296
    Top = 176
    ParamData = <
      item
        DataType = ftInteger
        Name = 'TID'
        ParamType = ptInput
      end>
    object KopfTabTERM_ID: TIntegerField
      FieldName = 'TERM_ID'
    end
    object KopfTabQUELLE: TIntegerField
      FieldName = 'QUELLE'
      Required = True
    end
    object KopfTabREC_ID: TIntegerField
      FieldName = 'REC_ID'
    end
    object KopfTabQUELLE_SUB: TIntegerField
      FieldName = 'QUELLE_SUB'
    end
    object KopfTabADDR_ID: TIntegerField
      FieldName = 'ADDR_ID'
      Required = True
    end
    object KopfTabATRNUM: TIntegerField
      FieldName = 'ATRNUM'
    end
    object KopfTabVRENUM: TIntegerField
      FieldName = 'VRENUM'
      Required = True
    end
    object KopfTabVLSNUM: TIntegerField
      FieldName = 'VLSNUM'
    end
    object KopfTabFOLGENR: TIntegerField
      FieldName = 'FOLGENR'
    end
    object KopfTabKM_STAND: TIntegerField
      FieldName = 'KM_STAND'
    end
    object KopfTabKFZ_ID: TIntegerField
      FieldName = 'KFZ_ID'
    end
    object KopfTabVERTRETER_ID: TIntegerField
      FieldName = 'VERTRETER_ID'
    end
    object KopfTabGLOBRABATT: TFloatField
      FieldName = 'GLOBRABATT'
    end
    object KopfTabADATUM: TDateField
      FieldName = 'ADATUM'
    end
    object KopfTabRDATUM: TDateField
      FieldName = 'RDATUM'
      Required = True
    end
    object KopfTabLDATUM: TDateField
      FieldName = 'LDATUM'
    end
    object KopfTabPR_EBENE: TIntegerField
      FieldName = 'PR_EBENE'
    end
    object KopfTabLIEFART: TIntegerField
      FieldName = 'LIEFART'
    end
    object KopfTabZAHLART: TIntegerField
      FieldName = 'ZAHLART'
    end
    object KopfTabKOST_NETTO: TFloatField
      FieldName = 'KOST_NETTO'
    end
    object KopfTabWERT_NETTO: TFloatField
      FieldName = 'WERT_NETTO'
    end
    object KopfTabLOHN: TFloatField
      FieldName = 'LOHN'
    end
    object KopfTabWARE: TFloatField
      FieldName = 'WARE'
    end
    object KopfTabTKOST: TFloatField
      FieldName = 'TKOST'
    end
    object KopfTabMWST_0: TFloatField
      FieldName = 'MWST_0'
    end
    object KopfTabMWST_1: TFloatField
      FieldName = 'MWST_1'
    end
    object KopfTabMWST_2: TFloatField
      FieldName = 'MWST_2'
    end
    object KopfTabMWST_3: TFloatField
      FieldName = 'MWST_3'
    end
    object KopfTabNSUMME: TFloatField
      FieldName = 'NSUMME'
      DisplayFormat = ',###,##0.00'
    end
    object KopfTabMSUMME_0: TFloatField
      FieldName = 'MSUMME_0'
      DisplayFormat = ',###,##0.00'
    end
    object KopfTabMSUMME_1: TFloatField
      FieldName = 'MSUMME_1'
      DisplayFormat = ',###,##0.00'
    end
    object KopfTabMSUMME_2: TFloatField
      FieldName = 'MSUMME_2'
      DisplayFormat = ',###,##0.00'
    end
    object KopfTabMSUMME_3: TFloatField
      FieldName = 'MSUMME_3'
      DisplayFormat = ',###,##0.00'
    end
    object KopfTabMSUMME: TFloatField
      FieldName = 'MSUMME'
      DisplayFormat = ',###,##0.00'
    end
    object KopfTabBSUMME: TFloatField
      FieldName = 'BSUMME'
      DisplayFormat = ',###,##0.00'
    end
    object KopfTabATSUMME: TFloatField
      FieldName = 'ATSUMME'
    end
    object KopfTabATMSUMME: TFloatField
      FieldName = 'ATMSUMME'
    end
    object KopfTabWAEHRUNG: TStringField
      FieldName = 'WAEHRUNG'
      Size = 5
    end
    object KopfTabGEGENKONTO: TIntegerField
      FieldName = 'GEGENKONTO'
    end
    object KopfTabSOLL_STAGE: TIntegerField
      FieldName = 'SOLL_STAGE'
    end
    object KopfTabSOLL_SKONTO: TFloatField
      FieldName = 'SOLL_SKONTO'
    end
    object KopfTabSOLL_NTAGE: TIntegerField
      FieldName = 'SOLL_NTAGE'
    end
    object KopfTabSOLL_RATEN: TIntegerField
      FieldName = 'SOLL_RATEN'
    end
    object KopfTabSOLL_RATBETR: TFloatField
      FieldName = 'SOLL_RATBETR'
    end
    object KopfTabSOLL_RATINTERVALL: TIntegerField
      FieldName = 'SOLL_RATINTERVALL'
    end
    object KopfTabIST_ANZAHLUNG: TFloatField
      FieldName = 'IST_ANZAHLUNG'
    end
    object KopfTabIST_SKONTO: TFloatField
      FieldName = 'IST_SKONTO'
    end
    object KopfTabIST_ZAHLDAT: TDateField
      FieldName = 'IST_ZAHLDAT'
    end
    object KopfTabIST_BETRAG: TFloatField
      FieldName = 'IST_BETRAG'
    end
    object KopfTabMAHNKOSTEN: TFloatField
      FieldName = 'MAHNKOSTEN'
    end
    object KopfTabKONTOAUSZUG: TIntegerField
      FieldName = 'KONTOAUSZUG'
    end
    object KopfTabBANK_ID: TIntegerField
      FieldName = 'BANK_ID'
    end
    object KopfTabSTADIUM: TIntegerField
      FieldName = 'STADIUM'
    end
    object KopfTabERSTELLT: TDateField
      FieldName = 'ERSTELLT'
    end
    object KopfTabERST_NAME: TStringField
      FieldName = 'ERST_NAME'
    end
    object KopfTabKUN_NUM: TStringField
      FieldName = 'KUN_NUM'
    end
    object KopfTabKUN_ANREDE: TStringField
      FieldName = 'KUN_ANREDE'
      Size = 30
    end
    object KopfTabKUN_NAME1: TStringField
      FieldName = 'KUN_NAME1'
      Size = 30
    end
    object KopfTabKUN_NAME2: TStringField
      FieldName = 'KUN_NAME2'
      Size = 30
    end
    object KopfTabKUN_NAME3: TStringField
      FieldName = 'KUN_NAME3'
      Size = 30
    end
    object KopfTabKUN_ABTEILUNG: TStringField
      FieldName = 'KUN_ABTEILUNG'
      Size = 30
    end
    object KopfTabKUN_STRASSE: TStringField
      FieldName = 'KUN_STRASSE'
      Size = 30
    end
    object KopfTabKUN_LAND: TStringField
      FieldName = 'KUN_LAND'
      Size = 2
    end
    object KopfTabKUN_PLZ: TStringField
      FieldName = 'KUN_PLZ'
      Size = 5
    end
    object KopfTabKUN_ORT: TStringField
      FieldName = 'KUN_ORT'
      Size = 30
    end
    object KopfTabUSR1: TStringField
      FieldName = 'USR1'
      Size = 80
    end
    object KopfTabUSR2: TStringField
      FieldName = 'USR2'
      Size = 80
    end
    object KopfTabPROJEKT: TStringField
      FieldName = 'PROJEKT'
      Size = 40
    end
    object KopfTabORGNUM: TStringField
      FieldName = 'ORGNUM'
    end
    object KopfTabBEST_NAME: TStringField
      FieldName = 'BEST_NAME'
      Size = 40
    end
    object KopfTabBEST_CODE: TIntegerField
      FieldName = 'BEST_CODE'
    end
    object KopfTabINFO: TMemoField
      FieldName = 'INFO'
      BlobType = ftMemo
    end
    object KopfTabUW_NUM: TIntegerField
      FieldName = 'UW_NUM'
    end
    object KopfTabBEST_DATUM: TDateField
      FieldName = 'BEST_DATUM'
    end
    object KopfTabFREIGABE1_FLAG: TBooleanField
      FieldName = 'FREIGABE1_FLAG'
      Required = True
    end
    object KopfTabBRUTTO_FLAG: TBooleanField
      FieldName = 'BRUTTO_FLAG'
      Required = True
    end
    object KopfTabPROVIS_WERT: TFloatField
      FieldName = 'PROVIS_WERT'
      Required = True
    end
  end
  object KopfDS: TDataSource
    DataSet = KopfTab
    Left = 344
    Top = 176
  end
  object ImageList1: TImageList
    Height = 20
    Width = 20
    Left = 468
    Top = 177
    Bitmap = {
      494C01010F001300040014001400FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      000000000000360000002800000050000000640000000100100000000000803E
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000018631863186318631863186318631863186318631863186318631863
      1863186318631863186300000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000104210421042104210421042104210421042104210421042FF7F1863FF7F
      1863FF7F18631042104200000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00001042104210421042104210421042104210421042104218631863FF7F1863
      FF7F186310421042104200000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FF7F
      FF7F000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000010001000100010001000100000000000104210421042FF7FFF7FFF7F1000
      1000100010001000100000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000FF7F0000
      0000FF7FFF7F000000000000000000000000000000000000000000000000FF7F
      1863186318631863186318631863186318631863186318631863186318630000
      00000000000000000000000010001F7C1040000000001863FF7FFF7FFF7F1000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FF7F0000FF7F
      FF7F00000000FF7FFF7F0000000000000000000000000000000000000000FF7F
      1863186318631863186318631863186318631863186318631863186318630000
      000000000000000000000000100010401F7C10400000FF7FFF7FFF7FFF7F1000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000FF7F0000FF7F1042
      0000FF7FFF7F00000000FF7FFF7F00000000000000000000000000000000FF7F
      18631863186318631863186318631863000200021863007C007C186318630000
      00000000000000000000000010001F7C10401F7C0000FF7FFF7FFF7FFF7F1000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FF7F10420000
      000010420000FF7FFF7F0000000000000000000000000000000000000000FF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F0000
      000000000000000000000000100010401F7C10400000FF7FFF03FF7FFF031000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000FF7F10420000FF7F
      E07F0000000010420000FF7FFF7F000000000000000000000000000010420000
      0000104210421042104210421042104210421042104210421042000000001042
      00000000000000000000000010001F7C10401F7C0000FF7FFF7FFF7FFF7F1000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000FF7FE07F
      FF7FE07FFF7F0000000010420000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000100010401F7C10400000FF7FFF03FF7FFF031000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FF7FE07FFF7F
      E07FFF7FE07FFF7FE07F00000000000000000000000000000000000000000000
      00000000FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F0000000000000000
      00000000000000000000000010001F7C10401F7C0000FF7FFF7FFF7FFF7F1000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000FF7FE07FFF7FE07F
      FF7FE07FFF7FE07FFF7FE07FFF7F000000000000000000000000000000000000
      00000000FF7F00000000000000000000000000000000FF7F0000000000000000
      000000000000000000000000100010401F7C10400000FF7FFF03FF7FFF031000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000E07FFF7F
      E07FFF7FE07FFF7FE07FFF7F0000000000000000000000000000000000000000
      00000000FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F0000000000000000
      00000000000000000000000010001F7C10401F7C0000FF03FF7FFF03FF7F1000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      FF7FE07FFF7FE07FFF7F00000000000000000000000000000000000000000000
      00000000FF7F00000000000000000000000000000000FF7F0000000000000000
      000000000000000000000000100010401F7C10400000FF7FFF03FF7FFF031000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000E07F0000000000000000000000000000000000000000000000000000
      00000000FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F0000000000000000
      00000000000000000000000010001F7C10401F7C0000FF03FF7FFF03FF7F1000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000FF7F00000000FF7FFF7FFF7FFF7FFF7FFF7FFF7F0000000000000000
      0000000000000000000000001000100010001000100010001000100010001000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F0000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000E003E003E003E003000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000010421042104210421042104210421042
      1042104210421042104210421042104200000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000104210421042104210421042104210421042000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000001042104200000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000104210421042104210421042104210421042104210420000
      0000000000000000000000000000E07FFF7FE07F0000FF7FFF7FFF7FFF7FFF7F
      FF7FFF7F0000E07FFF7FE07F0000104200000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000001700170017001700170017001700170017001700104210420000
      0000000000000000000000000000FF7FE07FFF7F0000FF7F00000000FF7FFF7F
      FF7FFF7F0000FF7FE07FFF7F0000104200000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000001700FF7FE07FE07FFF7F0000000000000000FF7F104210420000
      0000000000000000000000000000E07FFF7FE07F0000FF7F10421042FF7FFF7F
      FF7FFF7F0000E07FFF7FE07F0000104200000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000001700E07FFF7FFF7FE07F0000E05EE05EE05E0000104210420000
      0000000000000000000000000000FF7FE07FFF7F0000FF7FFF7FFF7FFF7FFF7F
      FF7FFF7F0000FF7FE07FFF7F0000104200000000000000000000000000000000
      0000F70200000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000F70200000000000000000000000000000000
      0000000000001700FF7FE07FE07FFF7F0000E05EE07FE05EE05E104210420000
      0000000000000000000000000000E07FFF7FE07FFF7F00000000000000000000
      00000000FF7FE07FFF7FE07F0000104200000000000000000000000000000000
      F702F70200000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000F702F7020000000000000000000000000000
      0000000000001700FF7FE07FE07FFF7F0000FF7FE07FE07FE07FE05E00001042
      0000000000000000000000000000FF7FE07FFF7FE07FFF7FE07FFF7FE07FFF7F
      E07FFF7FE07FFF7FE07FFF7F000010420000000000000000000000000000F702
      F702F702F702F702F702F7020000000000000000000000000000000000000000
      000000000000F702F702F702F702F702F702F702000000000000000000000000
      0000000000001700E07FFF7FFF7FE07FFF7FFF7FFF7FE07FE07FE05EE05E0000
      1042000000000000000000000000E07FFF7FE07FFF7FE07FFF7FE07FFF7FE07F
      FF7FE07FFF7FE07FFF7FE07F00001042000000000000000000000000F702F702
      F702F702F702F702F702F7020000000000000000000000000000000000000000
      000000000000F702F702F702F702F702F702F702F70200000000000000000000
      0000000000001700FF7FE07FE07FFF7FE07F0000FF7FFF7FE07FE07FE05EE05E
      1042104200000000000000000000FF7FE07FFF7FE07FFF7FE07FFF7FE07FFF7F
      E07FFF7FE07FFF7FE07FFF7F000010420000000000000000000000000000F702
      F702F702F702F702F702F7020000000000000000000000000000000000000000
      000000000000F702F702F702F702F702F702F702000000000000000000000000
      0000000000001700E07FFF7FFF7FE07FFF7FFF7F0000FF7FFF7FE07FE07FE05E
      0000104210420000000000000000E07FFF7FE07FFF7FE07FFF7FE07FFF7FE07F
      FF7FE07FFF7FE07FFF7FE07F0000104200000000000000000000000000000000
      F702F70200000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000F702F7020000000000000000000000000000
      0000000000001700E07FFF7FFF7FE07FFF7F1700170017000000FF7FE07FE07F
      0000000000000000000000000000FF7FE07FFF7F000000000000000000000000
      000000000000FF7FE07FFF7F0000104200000000000000000000000000000000
      0000F70200000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000F70200000000000000000000000000000000
      0000000000001700FF7FE07FE07FFF7FE07F1700FF7FE07F1700FF7FFF7FE07F
      E05EE05E00000000000000000000E07FFF7F0000FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7F0000E07FFF7FE07F0000104200000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000001700E07FFF7FFF7FE07FFF7F1700E07F170000000000FF7F0000
      0000E05E00000000000000000000FF7FE07F0000FF7F1F001F001F001F001F00
      1F00FF7F0000FF7FE07FFF7F0000104200000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000001700170017001700170017001700000000000000000000000000
      0000000000000000000000000000E07FFF7F0000FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7F0000E07FFF7FE07F0000104200000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FF7FE07F0000FF7F1F001F001F001F001F00
      1F00FF7F0000FF7F0000FF7F0000104200000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000E07FFF7F0000FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7F0000E07FFF7FE07F0000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000104210421042104210421042
      1042104210421042000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000104210421042104210421042
      1042104210421042000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000001700
      1700170017001700170017001700170017001700170000000000000000000000
      0000000000000000000000000000FF7F0000FF7F0000FF7F0000FF7F00000000
      0000000000000000000000000000000000001700170017001700170017001700
      1700170017001042000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000001700
      1042104210421042104210421042104210421042170000000000000000000000
      0000000000000000000000000000FF7F0000FF7F0000FF7F0000FF7F00000000
      0000000000000000000000000000000000001700E07FFF7FE07FE07FFF7FE07F
      FF7FFF7F17001042000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000001700
      0000000000000000000000000000000000000000170000000000000000000000
      0000000000000000000000000000FF7F0000FF7F0000FF7F0000FF7F00000000
      0000000000000000000000000000000000001700FF7FE07FFF7FFF7FE07FFF7F
      E07FE07F17001042000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000001700
      0000000000000000000000000000000000000000170000000000000000000000
      0000000000000000000000000000FF7F0000FF7F0000FF7F0000FF7F00000000
      0000000000000000000000000000000000001700E07FFF7FE07FE07FFF7FE07F
      FF7FFF7F17001042000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000001700
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FF7F0000FF7F0000FF7F0000FF7F00000000
      0000000000000000000000000000000000001700E07FFF7FE07FE07FFF7FE07F
      FF7FFF7F17001042000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000001700
      000000000000000000000000000000000000E07F104200000000000000000000
      0000000000000000000000000000FF7F0000FF7F0000FF7F0000FF7F00000000
      0000000000000000000000000000000000001700FF7FE07FFF7FFF7FE07FFF7F
      E07FE07F17001042000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000001700
      000000000000000000000000000000000000E07F000000000000000000000000
      0000000000000000000000000000FF7F0000FF7F0000FF7F0000FF7F00000000
      0000000000000000000000000000000000001700E07FFF7FE07FE07FFF7FE07F
      FF7FFF7F17001042000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000001700
      1700170017001700170017001700170017000000E07F10420000000000000000
      0000000000000000000000000000FF7F0000FF7F0000FF7F0000FF7F00000000
      0000000000000000000000000000000000001700FF7FE07FFF7FFF7FE07FFF7F
      E07FE07F17001042000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000001700
      FF7F17001700FF7F17001700FF7F170017000000E07F00000000000000000000
      0000000000000000000000000000FF7F0000FF7F0000FF7F0000FF7F00000000
      0000000000000000000000000000000000001700FF7FE07FFF7FFF7FE07F1700
      1700170017000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000001700
      17001700170017001700170017001700170017000000E07F1042000000000000
      0000000000000000000000000000FF7F0000FF7F0000FF7F0000FF7F00000000
      000000000000000000000000000000000000FF7FE07FFF7FE07FE07FFF7F1700
      FF7F170000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000E07F1042000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FF7F0000FF7FFF7FE07FFF7FFF7FE07F1700
      E07F000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000175C00000000
      000000000000000000000000FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F0000
      0000000000000000000000000000FF7FFF7FFF7FFF7FFF7F1700170017001700
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000175C175C00000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FF7FFF7F000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FF7F0000FF7FFF7F00000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000104210421042104210421042104210421042104210421042104200000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000010421042000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000040007C007C104200000000000000000000004010420000000000000000
      0000104210421042104210421042104210421042104210420000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000FF7FFF7FFF7FFF7FFF7FFF7F000000000000000000000000
      00000040007C007C007C10420000000000000040007C007C1042000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000FF7F1042104210421042FF7F000000000000000000000000
      00000040007C007C007C007C104200000040007C007C007C007C104200000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000E07F0000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000FF7FFF7FFF7FFF7FFF7FFF7F000000000000000000000000
      000000000040007C007C007C007C1042007C007C007C007C007C104200000000
      0000104210421042104210421042104200000000000000000000000000000000
      00000000E07F0000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000FF7F1042104210421042FF7F000000000000000000000000
      0000000000000040007C007C007C007C007C007C007C007C1042000000000000
      00000000000000000000000000000000000000000000000000000000E07F0000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000017001700170017001700
      1700170017000000FF7FFF7FFF7FFF7FFF7FFF7F000000000000000000000000
      00000000000000000040007C007C007C007C007C007C10420000000000000000
      000000000000000000000000000000000000000000001F00000000000000E07F
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000017001042104210421042
      1042104210420000FF7F1042104210421042FF7F000000000000000000000000
      00000000000000000000007C007C007C007C007C104200000000000000000000
      00000000000000000000000000000000000000001F001F000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000017000000000000000000
      0000000000000000FF7FFF7FFF7FFF7FFF7FFF7F000000000000000000000000
      000000000000000000000040007C007C007C007C104200000000000000000000
      0000000000000000000000000000000000001F001F001F000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000017000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000040007C007C007C007C007C104200000000000000000000
      000000000000000000000000000000001F001F001F001F000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000017000000000000000000
      0000000000000000000000001700000000000000000000000000000000000000
      0000000000000040007C007C007C1042007C007C007C10420000000000000000
      0000000000000000000000000000000000001F001F001F000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000017000000000000000000
      0000000000000000000000001700000000000000000000000000000000000000
      000000000040007C007C007C104200000040007C007C007C1042000000000000
      00000000000000000000000000000000000000001F001F000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000017000000000000000000
      0000000000000000000000001700000000001700000000000000000000000000
      000000000040007C007C10420000000000000040007C007C007C104200000000
      000000000000000000000000000000000000000000001F000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000017001700170017001700
      1700170017001700170017001700000017001700170000000000000000000000
      0000000000000040007C000000000000000000000040007C007C007C00000000
      0000104210421042104210421042104200000000000000000000000000000000
      E07F000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000001700FF7F17001700FF7F
      17001700FF7F17001700FF7F1700000000001700000000000000000000000000
      000000000000000000000000000000000000000000000040007C004000000000
      000000000000000000000000000000000000000000000000000000000000E07F
      00000000E07F0000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000017001700170017001700
      1700170017001700170017001700000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000E07F0000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000104210421042104210421042104210421042104210420000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000050000000640000000100010000000000B00400000000000000000000
      000000000000000000000000FFFFFF0000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      FFFFFFFFFF80000000000000FFFFFFFFFF00000000000000FCFFFC7FE3000000
      00000000F83FF8000100000000000000F00FF80001F801F000000000E003F800
      01F801F000000000C101F80001F801F000000000E043F80001F801F000000000
      C011F80001F801F000000000E003FE0007F801F000000000E003FE0007F801F0
      00000000C001FF000FF801F000000000E003FF000FF801F000000000F806FF00
      0FF801F000000000FE2C7F000FF801F000000000FF983F000FF801F000000000
      FFFEFF000FFFFFF000000000FFFEFF000FFE07F000000000FFFFFFFFFFFE07F0
      00000000FFFFFFFFFFFE07F000000000FFFFFFFFFFFFFFFFFFFF0000FFFFFFFF
      FFFFFFFE00010000FFFFFFFFFFE00FFC00010000FFFFFFFFFFE003F800010000
      FFFFFFFFFFC003F800010000FFBFFFFBFFC003F800010000FF3FFFF9FFC003F8
      00010000FE3FFFF8FFC003F800010000FC03FF807FC001F800010000F803FF80
      3FC000F800010000F003FF801FC0007800010000F803FF803FC0003800010000
      FC03FF807FC0003800010000FE3FFFF8FFC0003800010000FF3FFFF9FFC01038
      00010000FFBFFFFBFFC07CF800010000FFFFFFFFFFFFFFF800010000FFFFFFFF
      FFFFFFF800030000FFFFFFFFFFFFFFFC00070000FFFFFFFFFFFFFFFFFFFF0000
      FFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFF
      801F0000FFFFFFFFFFFC07FF801F0000FFFFFC003FF803FF001F0000FFFFFC00
      3FF803FF001F0000FFFFFDFEBFF803FF001F0000FFDFFD243FF803FF001F0000
      FF8FFDFE3FF803FF001F0000FF07FD241FF803FF001F0000FE03FDFE5FF803FF
      001F0000FF8FFC000FF803FF001F0000FF8FFC002FF803FF003F0000FF8FFC00
      07F803FF007F0000FFFFFFFF87F001FD00FF0000FFFFFFFFC3F001FC01FF0000
      FFFFFFFFC3F803FE7FFF0000FFFFFFFFE7FF1FFD3FFF0000FFFFFFFFFFFFFFFF
      FFFF0000FFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFF0007FFFFFF0000FFFFFFFF
      FF0007FFFFFF0000FFFFFF9FFFFFFFFFFFFF0000FFC03F0F9F001FFFFFFF0000
      FFC03F070F001FFFFFFF0000FFC03F0207FFFFBFFFFF0000FFC03F800701FFBF
      F8FF0000FFC03FC00F01FBFFF8FF0000C0003FE01FFFDDFFF8FF0000C0003FF0
      3FFF9FBFE03F0000DFC03FF03FFF1FBFF07F0000D2403FE03FFE1E0FF8FF0000
      DFFBFFC01FFF1FBFFDFF0000D24BFF820FFF9FBFFFFF0000DFFB7F8707FFDFFF
      FFFF0000C0023FCF8701FEFFFFFF0000C0037FFFC701FDBFFFFF0000C003FFFF
      FFFFFFBFFFFF0000FFFFFFFFFF001FFFFFFF0000FFFFFFFFFF001FFFFFFF0000
      00000000000000000000000000000000000000000000}
  end
  object ArtikelTab1: TZMySqlQuery
    Database = DM1.db1
    Transaction = DM1.Transact1
    CachedUpdates = False
    ShowRecordTypes = [ztModified, ztInserted, ztUnmodified]
    Options = [doHourGlass, doAutoFillDefs]
    LinkOptions = [loCascadeUpdate, loAlwaysResync]
    Constraints = <>
    ExtraOptions = []
    AlternativeView = True
    Macros = <>
    Sql.Strings = (
      'select * from ARTIKEL'
      'where REC_ID=:ID'
      'LIMIT 0,10')
    RequestLive = True
    Left = 398
    Top = 176
    ParamData = <
      item
        DataType = ftInteger
        Name = 'ID'
        ParamType = ptInput
      end>
    object ArtikelTab1REC_ID: TIntegerField
      FieldName = 'REC_ID'
    end
    object ArtikelTab1ARTIKELTYP: TStringField
      FieldName = 'ARTIKELTYP'
      Size = 1
    end
    object ArtikelTab1ARTNUM: TStringField
      FieldName = 'ARTNUM'
    end
    object ArtikelTab1MATCHCODE: TStringField
      FieldName = 'MATCHCODE'
    end
    object ArtikelTab1BARCODE: TStringField
      FieldName = 'BARCODE'
    end
    object ArtikelTab1KAS_NAME: TStringField
      FieldName = 'KAS_NAME'
      Size = 80
    end
    object ArtikelTab1LAENGE: TStringField
      FieldName = 'LAENGE'
    end
    object ArtikelTab1GROESSE: TStringField
      FieldName = 'GROESSE'
    end
    object ArtikelTab1DIMENSION: TStringField
      FieldName = 'DIMENSION'
    end
    object ArtikelTab1GEWICHT: TFloatField
      FieldName = 'GEWICHT'
    end
    object ArtikelTab1EK_PREIS: TFloatField
      FieldName = 'EK_PREIS'
    end
    object ArtikelTab1STEUER_CODE: TIntegerField
      FieldName = 'STEUER_CODE'
    end
    object ArtikelTab1ME_EINHEIT: TStringField
      FieldName = 'ME_EINHEIT'
      Size = 10
    end
    object ArtikelTab1PR_EINHEIT: TFloatField
      FieldName = 'PR_EINHEIT'
    end
    object ArtikelTab1WARENGRUPPE: TIntegerField
      FieldName = 'WARENGRUPPE'
    end
    object ArtikelTab1ERLOES_KTO: TIntegerField
      FieldName = 'ERLOES_KTO'
    end
    object ArtikelTab1AUFW_KTO: TIntegerField
      FieldName = 'AUFW_KTO'
    end
    object ArtikelTab1RABGRP_ID: TStringField
      FieldName = 'RABGRP_ID'
      Required = True
      Size = 10
    end
    object ArtikelTab1NO_RABATT_FLAG: TBooleanField
      FieldName = 'NO_RABATT_FLAG'
      Required = True
    end
    object ArtikelTab1NO_BEZEDIT_FLAG: TBooleanField
      FieldName = 'NO_BEZEDIT_FLAG'
      Required = True
    end
    object ArtikelTab1NO_VK_FLAG: TBooleanField
      FieldName = 'NO_VK_FLAG'
      Required = True
    end
    object ArtikelTab1ALTTEIL_FLAG: TBooleanField
      FieldName = 'ALTTEIL_FLAG'
      Required = True
    end
    object ArtikelTab1VK1B: TFloatField
      FieldName = 'VK1B'
      Required = True
    end
    object ArtikelTab1VK2B: TFloatField
      FieldName = 'VK2B'
      Required = True
    end
    object ArtikelTab1VK3B: TFloatField
      FieldName = 'VK3B'
      Required = True
    end
    object ArtikelTab1VK4B: TFloatField
      FieldName = 'VK4B'
      Required = True
    end
    object ArtikelTab1VK5B: TFloatField
      FieldName = 'VK5B'
      Required = True
    end
    object ArtikelTab1VK1: TFloatField
      FieldName = 'VK1'
      Required = True
    end
    object ArtikelTab1VK2: TFloatField
      FieldName = 'VK2'
      Required = True
    end
    object ArtikelTab1VK3: TFloatField
      FieldName = 'VK3'
      Required = True
    end
    object ArtikelTab1VK4: TFloatField
      FieldName = 'VK4'
      Required = True
    end
    object ArtikelTab1VK5: TFloatField
      FieldName = 'VK5'
      Required = True
    end
    object ArtikelTab1SN_FLAG: TBooleanField
      FieldName = 'SN_FLAG'
      Required = True
    end
  end
  object FormPlacement1: TJvFormPlacement
    IniFileName = '\SOFTWARE\SBP\CAO-Faktura'
    IniSection = 'KASSE_MAIN'
    MinMaxInfo.MinTrackHeight = 500
    MinMaxInfo.MinTrackWidth = 796
    UseRegistry = True
    Version = 1002
    Left = 557
    Top = 125
  end
  object ImageList2: TImageList
    Left = 557
    Top = 176
    Bitmap = {
      494C010130003100040010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      000000000000360000002800000040000000D000000001001000000000000068
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000010420000
      0000000000000000104200000000000000000000000000000040004000400040
      0040004000400040004000000000000000000000000000000000000010001000
      1F0010001F000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      FF7FFF7F18631863000000000000000000000000000000000040004000400040
      00400040004000400040000000000000000000000000000010001F0010001000
      10001F0010001000100000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000010420000
      0000000000000000000000000000000000000000000000000040004000400040
      0040004000400040004000000000000000000000000010001F00100000420042
      10001000100010001000100000000000000000000000FF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F00000000000000000000000000000000
      FF7FFF7F18631863000000000000000000000000000000000040004000400040
      004000400040004000400000000000000000000010001F0010001F0000420042
      10001F0010001F001000004200420000000000000000FF7FFF7FFF7FFF7F1700
      1700170017001700FF7FFF7FFF7FFF7F00000000000000000000000010420000
      0000000000000000000000000000000000000000000000000040004000400040
      004000400040004000400000000000000000000010001F001F001F0000420042
      00421000100010001000004200420000000000000000FF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F0000000000000000000000000000FF7F
      104218631042FF7F000000000000000000000000000000000040004000400040
      0040004000400040004000000000000000001F001F001F001F001F0000420042
      00421F001F001F0010001F0010001000000000000000FF7FFF7FFF7FFF7F1700
      17001700170017001700FF7FFF7FFF7F0000000000000000000000001042E07F
      104218631042E07FFF7F00000000000000000000000000000040004000400040
      004000400040E07F0040000000000000000010001F001F000042004200420042
      00421F001F001F001000100010001F00000000000000FF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F000000000000000000000000FF7FFF7F
      104218631042FF7FFF7F10420000000000000000000000000040004000400040
      0040004000400040004000000000000000001F00100000420042004200420042
      00421F0000421F0010001F001F001F00000000000000FF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7F005C005CFF7F00000000000000001042E07FFF7FE07F
      104218631042E07FFF7FE07F1042000000000000000000000040004000400040
      00400040004000400040000000000000000010001F001F000042004200420042
      00420042004200421F0010001F001000000000000000FF7F0000000000000000
      FF7FFF7FFF7FFF7FFF7F005C005CFF7F0000000000001042E07FFF7FFF7F1042
      1863186318631042FF7FFF7FE07F1042000000000000000000400040FF03FF03
      FF03FF03FF030040004000000000000000001F001F001F00004200421F001F00
      10000042004200421F001F0010001F00000000000000FF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F0000000000001042FF7FE07FFF7F1042
      1863186318631042FF7FE07FFF7F1042000000000000000000400040FF03FF03
      FF03FF03FF0300400040000000000000000000001F001F001F001F001F000042
      0042004200420042004210001F00000000000000000000000000000000000000
      000000000000000000000000000000000000000000001042E07FFF7F00001042
      00000000000010420000FF7FE07F1042000000000000000000400040FF03FF03
      FF03FF03FF03004000400000000000000000000010001F000042004200420042
      004200420042004200421F001F00000000000000000000000000000000000000
      000000000000000000000000000000000000000000001042FF7FE07FFF7F1042
      FF7FE07FFF7F1042FF7FE07FFF7F104200000000000000000040004000400040
      0040004000400040004000000000000000000000000000420042004200421F00
      1F0000421F00004200421F000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000001042FF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7F1042000000000000000000000040004000400040
      004000400040004000400000000000000000000000000000004200421F001F00
      10001F0010001F001F0000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000001042FF7FE07F
      FF7FE07FFF7FE07FFF7F10420000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000001F001000
      1F001F001F000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000010421042
      E07FFF7FE07F1042104200000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000FF7FFF7FFF7FFF7FFF7F0000000000000000000000000000
      00000000FF7FFF7FFF7FFF7FFF7FFF7FFF7F0000000000000000000000000000
      1042104200000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FF7FFF7F0000FF7FFF7FFF7F0000000000000000000000000000
      00000000FF7F10421042104210421042FF7F0000000000000000000000001042
      0000000010420000000000000000000000000000000000001042104210421042
      1042104210421042104210421042104200000000000000000000000000000000
      00000000FF7FFF7F0000FF7FFF7FFF7FFF7F0000000000000000000000000000
      00000000FF7F1042FF7FFF7FFF7F1042FF7F0000000000000000000010420000
      0000FF7F00000000104200000000000000000000000000000000000000000000
      0000000000000000000000000000104200000000000000000000000000000000
      0000000000000000FF7FFF7FFF7FFF7FFF7F0000000000000000000000000000
      00000000FF7F10421042104210421042FF7F000000000000000010420000005C
      0000FF7FFF7F000000001042000000000000000000000000FF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7F0000104200000000000000000000000000000000
      000000000000FF7FFF7F0000FF7FFF7FFF7F0000170017001700170017001700
      17000000FF7FFF7FFF7FFF7FFF7FFF7FFF7F00000000000010420000005C005C
      00000000FF7FFF7FFF7F0000000010420000000000000000FF7F170017001700
      17001700FF7FFF03FF03FF7F0000104200000000000000000000000000000000
      0000FF7FFF7FFF7F0000FF7F0000FF7FFF7F0000170010421042104210421042
      10420000FF7F10421042000000000000FF7F0000000010420000005C005C0000
      007C007C00000000FF7FFF7FFF7F00000000000000000000FF7F1700FF7FFF7F
      FF7F1700FF7FFF7FFF7FFF7F000010420000000000000000000000000000FF7F
      FF7FFF7F00000000FF7F0000FF7F0000FF7F0000170000000000000000000000
      00000000FF7FFF7FFF7FFF7F000000000000000000000000005C005C0000007C
      007C007C007C007C00000000FF7F10420000000000000000FF7F1700FF7FFF7F
      FF7F1700FF7FF702F702FF7F00001042000000000000E05EE05E0000FF7FFF7F
      00000000FF7FFF7F0000FF7F0000FF7F00000000170000000000000000000000
      00000000FF7F104210420000000000000000000000000000005C0000007C007C
      007C007C007C007C007C007C000000000000000000000000FF7F170017001700
      17001700FF7FFF03FF03FF7F00001042000000000000FF7FFF7F0000FF7F0000
      E05EE05E00000000FF7F0000FF7F000000000000170000000000000000000000
      00000000FF7FFF7FFF7FFF7F0000000000000000000000000000007C007C007C
      007C007C007C007C007C007C000000000000000000000000FF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7F00001042000000000000FF7F0000000000000000
      0000000000000000000000000000E05E00000000170000000000000000000000
      0000000000000000000000000000000000000000000000000000007C007C007C
      007C007C007C007C007C0000000000000000000000000000FF7FFF03FF03FF03
      FF03FF7FFF03FF7FFF7FFF7F00001042000000000000FF7FFF7FE05EFF7FE05E
      FF7FE05EFF7FE05EFF7FE05EFF7FE05E00000000170000000000000000000000
      000000000000000000001700000000000000000000000000000000000000007C
      007C007C007C007C000000000000000000000000000000001700170017001700
      17001700170017001700170000001042000000000000FF7F0000000000000000
      0000000000000000000000000000E05E00000000170017001700170017001700
      1700170017001700170017000000000000000000000000000000000000000000
      0000007C007C0000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FF7FFF7FE05EFF7FE05E
      FF7FE05EFF7FE05EFF7FE05EFF7F0000000000001700FF7F17001700FF7F1700
      1700FF7F17001700FF7F17000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000170017001700170017001700
      1700170017001700170017000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000F702
      0000000000000000000000000000000000000000000000000000000000000000
      0000F7020000000000000000000000000000000000000000000000000000F702
      F702F70200000000000000000000000000000000000000000000000000000000
      F7020000000000000000000000000000000000000000000000000000F702F702
      0000000000000000000000000000000000000000000000000000000000000000
      0000F702F702000000000000000000000000000000000000000000000000F702
      F702F7020000000000000000000000000000000000000000000000000000F702
      F702F70200000000000000000000000000000000000000000000F702F702F702
      F702F702F702F702000000000000000000000000000000000000F702F702F702
      F702F702F702F70200000000000000000000000000000000000000000000F702
      F702F702000000000000000000000000000000000000000000000000F702F702
      F702F702F702000000000000000000000000000000000000F702F702F702F702
      F702F702F702F702000000000000000000000000000000000000F702F702F702
      F702F702F702F702F7020000000000000000000000000000000000000000F702
      F702F70200000000000000000000000000000000000000000000F702F702F702
      F702F702F702F702000000000000000000000000000000000000F702F702F702
      F702F702F702F702000000000000000000000000000000000000F702F702F702
      F702F702F702F702000000000000000000000000000000000000F702F702F702
      F702F702F702F70200000000000000000000000000000000000000000000F702
      F702F702000000000000000000000000000000000000000000000000F702F702
      0000000000000000000000000000000000000000000000000000000000000000
      0000F702F70200000000000000000000000000000000000000000000F702F702
      F702F702F702000000000000000000000000000000000000000000000000F702
      F702F7020000000000000000000000000000000000000000000000000000F702
      0000000000000000000000000000000000000000000000000000000000000000
      0000F7020000000000000000000000000000000000000000000000000000F702
      F702F7020000000000000000000000000000000000000000000000000000F702
      F702F70200000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      F70200000000000000000000000000000000000000000000000000000000F702
      F702F70200000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000005C000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000005C005C000000000000000000000000000000000000000000000000
      000000000000FF7FFF7FFF7FFF7FFF7FFF7F0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000005C005C005C000000000000005C0000000000000000000000000000
      000000000000FF7F1042104210421042FF7F0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000005C005C00000000005C005C0000000000000000000000000000
      000000000000FF7FFF7FFF7FFF7FFF7FFF7F0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000005C005C175C00000000000000000000000000000000
      000000000000FF7F1042104210421042FF7F0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000175C005C005C000000000000170017001700170017001700
      170017000000FF7FFF7FFF7FFF7FFF7FFF7F0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000005C175C005C005C00000000170010421042104210421042
      104210420000FF7F1042104210421042FF7F0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000005C005C00000000005C005C0000170000000000000000000000
      000000000000FF7FFF7FFF7FFF7FFF7FFF7F0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000017001700170017001700
      1700175C005C17001700170017000000175C0000170000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000001700FF7FFF7FFF7FFF7F
      FF7F005C175CFF7FFF7FFF7F1700000000000000170000000000000000000000
      0000000000000000000017000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000001700FF7F00000000FF7F
      00000000FF7F00000000FF7F1700000000000000170000000000000000000000
      0000000000000000000017000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000001700FF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7F1700000000000000170000000000000000000000
      0000000000000000000017000000000017000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000017001700170017001700
      1700170017001700170017001700000000000000170017001700170017001700
      1700170017001700170017000000170017000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000017000000170017000000
      17001700000017001700000017000000000000001700FF7F17001700FF7F1700
      1700FF7F17001700FF7F17000000000017000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000017001700170017001700
      1700170017001700170017001700000000000000170017001700170017001700
      1700170017001700170017000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000170017001700170017001700
      1700170017001700170017000000000000001042104210421042104210421042
      10421042000000000000000000000000000000000000FF7FFF7FFF7F00000000
      00000000000000000000FF7FFF7FFF7F000000000000E05EE05E000000000000
      0000000000000000E05EE05E0000000000000000170010421042104210421042
      1042104210421042104217000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FF7FFF7FFF7F00000000
      00000000000000000000FF7FFF7FFF7F000000000000E05EE05E000000000000
      0000000000000000E05EE05E0000000000000000170000000000000000000000
      0000000000000000000017000000000000000000000000000000000000000000
      00000000000000000000000000000000E07F0000000000000000000000000000
      00000000000000000000000000000000000000000000E05EE05E000000000000
      0000000000000000E05EE05E0000000000000000170000000000000000000000
      0000000000000000000017000000000000001042104210421042104200000000
      00000000000000000000000000000000E07F0000000000000000000000000000
      0000FF7F000000000000000000000000000000000000E05EE05E000000000000
      0000000000000000E05EE05E0000000000000000170000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000E07F00000000000000000000000000000000000000000000
      FF7FFF7FFF7F00000000000000000000000000000000E05EE05EE05EE05EE05E
      E05EE05EE05EE05EE05EE05E0000000000000000170000000000000000000000
      0000000000000000E07F10420000000000000000000000000000000000000000
      00001F00000000000000E07F000000000000000000000000000000000000FF7F
      FF7FFF7FFF7FFF7F0000000000000000000000000000E05EE05E000000000000
      0000000000000000E05EE05E0000000000000000170000000000000000000000
      0000000000000000E07F00000000000000000000000000000000000000000000
      1F001F0000000000000000000000000000000000000000000000000000000000
      FF7FFF7FFF7F00000000000000000000000000000000E05E0000FF7FFF7FFF7F
      FF7FFF7FFF7FFF7F0000E05E0000000000000000170017001700170017001700
      17001700170017000000E07F1042000000000000000000000000000000001F00
      1F001F0000000000000000000000000000000000000000000000000000000000
      0000FF7F000000000000000000000000000000000000E05E0000FF7FFF7FFF7F
      1F00FF7FFF7FFF7F0000E05E00000000000000001700FF7F17001700FF7F1700
      1700FF7F170017000000E07F000000000000000000000000000000001F001F00
      1F001F0000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000E05E0000FF7FFF7F1F00
      1F001F00FF7FFF7F0000E05E0000000000000000170017001700170017001700
      170017001700170017000000E07F104200000000000000000000000000001F00
      1F001F0000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000E05E0000FF7FFF7FFF7F
      1F001F001F00FF7F000000000000000000000000000000000000000000000000
      000000000000000000000000E07F104200000000000000000000000000000000
      1F001F0000000000000000000000000000000000000000000000000000000000
      FF7FFF7FFF7F00000000000000000000000000000000E05E0000FF7FFF7FFF7F
      FF7F1F001F001F0000001F000000000000000000000000000000000000000000
      00000000000000000000000000000000175C0000000000000000000000000000
      00001F0000000000000000000000000000000000000000000000000000000000
      FF7FFF7FFF7F0000000000000000000000000000000000000000000000000000
      000000001F001F001F001F000000000000000000000000000000000000000000
      0000000000000000000000000000175C175C1042104210421042104200000000
      000000000000000000000000E07F000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000001F001F001F000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000E07F00000000E07F0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000001F001F001F001F000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000E07F000000000000000000000000FF7F
      FF7F000000000000007C0040007C000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000001042104217001700
      1700170017001042104200000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000EF3D000000000000FF7FFF7F
      0000000000000000007C0040007C000000420000000000000000000000000000
      00000000000000000000E07FFF7FE07F00000000000010421700170017001700
      170017001700170017001042F7020000000000000000FF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7F0000000000000000EF3D0000FF7FFF7FFF7FFF7F0000
      0000000000000000007C0040007C004200420000000000000000000000000000
      00000000000000000000FF7FE07FFF7F0000000017001700170017001700E05E
      E07FE05E17001700170017001700F702000000000000FF7FFF7FFF7F0000FF7F
      FF7F0000FF7FFF7FFF7F00000000000000000000FF7F00000000FF7FFF7F0000
      0000000000000000007C0040007C004200420000000000000000000000000000
      0000E07FFF7FE07F0000E07FFF7FE07F0000F70217001700170017001700E07F
      E07FE07F170017001700170017001042000000000000FF7FFF7FFF7F00000000
      00000000FF7FFF7FFF7F000000000000000000000000000000000000FF7F0000
      0000000000000000007C0040007C004200000000000000000000000000000000
      0000FF7FE07FFF7F00000000000000000000170017001700170017001700E05E
      E07FE05E170017001700170017001700104200000000FF7FFF7FFF7F0000FF7F
      FF7F0000FF7FFF7FFF7F00001863186300000000EF3D000000000000FF7F0000
      000000000000007C007C007C007C007C0000000000000000FF7FE07FFF7FE07F
      0000E07FFF7FE07FFF7FE07F0000000000001700170017001700170017001700
      17001700170017001700170017001700104200000000FF7FFF7FFF7FFF7F0000
      0000FF7FFF7F00000000000018631863000000000000EF3D0000FF7F0000EF3D
      000000000000004000400040004000400000000000000000E07FFF7FE07FFF7F
      0000FF7FE07FFF7FE07FFF7F000000000000170017001700170017001700E05E
      E07FE05E170017001700170017001700170000000000FF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7F00000000186318631863000000000000000000000000EF3D0000
      000000000042004200420000000000000000000000000000FF7FE07FFF7FE07F
      0000E07FFF7FE07FFF7FE07F000000000000170017001700170017001700E07F
      E07FE05E17001700170017001700170017000000000000000000000000000000
      0000000000000000186300001863186300000000000000000000000000000000
      000000420042004200000000000000000000000000000000E07FFF7FE07FFF7F
      000000000000000000000000000000000000170017001700170017001700E05E
      E07FE07FE05E1700170017001700170017000000000000000000000000000000
      1863186300000000000000001863186300000000000000000000000000000000
      004200420042000000000000000000000000000000000000FF7FE07FFF7FE07F
      FF7FE07FFF7FE07F000000000000000000001700170017001700170017001700
      E05EE07FE07FE05E170017001700170017000000000000001700000000000000
      1863186300001863186300001863186300000000000000000000000000000042
      004200420000000000000000000000000000000000000000E07FFF7FE07FFF7F
      E07FFF7FE07FFF7F000000000000000000001700170017001700170017001700
      1700E05EE07FE07FE05E17001700170017000000000017001700170000000000
      1863186318630000000018631863186300000000000000000000000000420042
      004200000000000000000000000000000000000000000000FF7FE07FFF7FE07F
      FF7FE07FFF7FE07F00000000000000000000170017001700E05EE07FE05E1700
      17001700E07FE07FE05E17001700170010420000000000001700000000000000
      1863186318631863186318630000000000000000000000000000000000000042
      0000000000000000EF3D0000EF3D00000000000000000000E07FFF7FE07FFF7F
      E07FFF7FE07FFF7F00000000000000000000170017001700E05EE07FE07FE05E
      E05EE05EE07FE07FE05E17001700170010420000000000001700000000000000
      1863186318631863186318630000000000000000000000000000000000000000
      0000000000000000EF3D0000EF3D000000000000000000000000000000000000
      000000000000000000000000000000000000FF7F170017001700E05EE07FE07F
      E07FE07FE07FE05E170017001700104200000000000000001700170000000000
      0000000000000000000000000000000000000000000000420000000000000000
      EF3D000000000000EF3D0000EF3D000000000000000000000000000000000000
      000000000000000000000000000000000000E07F0000170017001700E05EE07F
      E07FE07FE05E1700170017001700000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000420042000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000E07FFF7F1700170017001700
      17001700170017001700F7020000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000010421042104210421042
      1042104210420000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000001042000000000000000000000000000000000000000010001000
      1F0010001F000000000000000000000000000000000000000000000017000000
      000000000000000000000000000000000000000000000000000000000000FF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7F000000000000E07FFF7FFF7FFF7FE07FFF7F
      FF7FFF7F000010421042104210421042104200000000000010001F0010001000
      10001F0010001000100000000000000000000000000000000000000017001700
      00000000005C005C0000005C005C0000005C000000000000000000000000FF7F
      000000000000000000000000FF7F000000000000FF7F10001000100010001000
      1000FF7F00000000000000000000000000000000000010001F00100000420042
      1000100010001000100010000000000000000000170017001700170017001700
      17000000005C005C0000005C005C0000005C000000000000000000000000FF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7F000000000000FF7FFF7FE07FFF7FFF7FFF7F
      E07FFF7F0000FF7FE07FFF7FFF7FFF7F0000000010001F0010001F0000420042
      10001F0010001F00100000420042000000000000000000000000000017001700
      000000000000000000000000000000000000000000000000000000000000FF7F
      000000000000000000000000FF7F000000000000FF7F10001000100010001000
      1000FF7F00001000100010001000FF7F0000000010001F001F001F0000420042
      0042100010001000100000420042000000000000000000000000000017000000
      000000000000000000000000000000000000000000000000000000000000FF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7F000000000000E07FFF7FFF7FFF7FE07FFF7F
      FF7FFF7F0000FF7FFF7FFF7FE07FFF7F00001F001F001F001F001F0000420042
      00421F001F001F0010001F001000100000000000000000000000000000000000
      0000000000000000000000000000000000000000FF7F000000001042FF7FFF7F
      000000000000000000000000FF7F000000000000FF7F10001000100010001000
      1000FF7F00001000100010001000FF7F000010001F001F000042004200420042
      00421F001F001F001000100010001F0000000000000000000000000000000000
      00000000000000000000000000000000000000001042000000001042E07FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7F000000000000FF7FFF7FE07FFF7FFF7FFF7F
      E07FFF7F0000FF7FE07FFF7FFF7FFF7F00001F00100000420042004200420042
      00421F0000421F0010001F001F001F0000000000000000000000FF7FFF7FFF7F
      000000000000000000000000000000000000000000000000FF7F1042FF7F1042
      000000000000FF7F000000000000000000000000FF7F10001000FF7F00000000
      0000000000001000100010421042FF7F000010001F001F000042004200420042
      00420042004200421F0010001F0010000000000000000000000000000000FF7F
      0000000000000000000000000000000000000000104210421042FF7FE07F0000
      FF7FFF7FFF7FFF7F0000FF7F0000000000000000E07FFF7FFF7FFF7F0000FF7F
      FF7FFF7F0000FF7FFF7F00000000000010421F001F001F00004200421F001F00
      10000042004200421F001F0010001F00000000000000FF7FFF7FFF7F0000FF7F
      0000000000000000000000000000000000000000FF7FE07F1042E07FFF7FE07F
      FF7FE07FFF7FE07F000000000000000000000000FF7F10001000FF7F0000FF7F
      E07F00000000FF7F10420000E0030000104200001F001F001F001F001F000042
      0042004200420042004210001F000000000000000000FF7F1042FF7F00000000
      000000000000000000000000000000000000000000001042E07F1042E07F1042
      E07F000000000000000000000000000000000000FF7FFF7FE07FFF7F0000FF7F
      0000FF7FFF7F000000000000E00300000000000010001F000042004200420042
      004200420042004200421F001F000000000000000000FF7F1042FF7F00000000
      00000000000000000000000000000000000000001042E07F00001042FF7F0000
      1042E07F00000000000000000000000000000000000000000000000000000000
      FF7F100010420000E003E003E003E003E0030000000000420042004200421F00
      1F0000421F00004200421F0000000000000000000000FF7FFF7F000000000000
      0000000000000000000000000000000000000000E07F000000001042E07F0000
      00001042FF7F0000000000000000000000000000000000000000000000000000
      FF7FFF7FE07F000000000000E00300000000000000000000004200421F001F00
      10001F0010001F001F0000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000001042FF7F0000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000104210420000E00300000000000000000000000000001F001000
      1F001F001F000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000E07F000000000000000000000000E07F0000000000000000000000000000
      000000000000000000000000000000000000000000000000007C004000401042
      00000000000000000000007C1042000000000000000000000000104200000000
      0000000000000000000010420000000000000000E07FE07F0000EF3DEF3DEF3D
      E07FE07FEF3DEF3DEF3DEF3DE07FE07F00000000000000000000000000000000
      000000000000000000000000000000000000000000000000007C004000400040
      1042000000000000007C004000401042000000000000E05EE05E000010421042
      104210421042000000000000E05E0000000000000000E07F0000000000000000
      000000000000000000000000E07F000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000007C004000400040
      004010420000007C0040004000400040104200000000E05EE05E000010421042
      104210421042000000000000E05E000000000000000000000000FF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7F0000EF3D000000000000000000000000104218630000
      0000000000000000000000000000000000000000000000000000007C00400040
      00400040104200400040004000400040104200000000E05EE05E000010421042
      104210421042000000000000E05E000000000000000000000000FF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7F0000EF3D00000000000000000000E07FE05E00001042
      18630000000000000000000000000000000000000000000000000000007C0040
      00400040004000400040004000401042000000000000E05EE05E000000000000
      000000000000000000000000E05E000000000000000000000000FF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7F0000EF3D000000000000000000000000E07FE07F0000
      000010421863000000000000000000000000000000000000000000000000007C
      00400040004000400040004010420000000000000000E05EE05EE05EE05EE05E
      E05EE05EE05EE05EE05EE05EE05E000000000000000000000000FF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7F0000EF3D0000000000000000000000000000E07FE07F
      E05E000000001042000000000000000000000000000000000000000000000000
      00400040004000400040104200000000000000000000E05E0000000000000000
      00000000000000000000E05EE05E00000000E07FE07FE07F0000FF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7F0000E07FE07F0000000000000000000000000000E07F
      E07FE07F00000000104200000000000000000000000000000000000000000000
      007C0040004000400040104200000000000000000000E05E0000000000000000
      000000000000000000000000E05E000000000000E07FE07F0000FF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7F0000E07FE07FE07F0000000000000000000000000000
      E07FE07FE07F000000001863000000000000000000000000000000000000007C
      00400040004000400040104200000000000000000000E05E0000000000000000
      000000000000000000000000E05E000000000000000000000000FF7FFF7FFF7F
      FF7F00000000000000000000000000000000000000000000000000000000E07F
      E07FE07F000000001863000000000000000000000000000000000000007C0040
      00400040104200400040004010420000000000000000E05E0000000000000000
      000000000000000000000000E05E000000000000000000000000FF7FFF7FFF7F
      FF7F0000FF7FFF7F000000000000000000000000000000000000000000000000
      E07FE07FE07F0000000018630000000000000000000000000000007C00400040
      004010420000007C0040004000401042000000000000E05E0000000000000000
      000000000000000000000000E05E000000000000000000000000FF7FFF7FFF7F
      FF7F0000FF7F0000E07F00000000000000000000000000000000000000000000
      00000000E07FE07F000000001863000000000000000000000000007C00400040
      1042000000000000007C004000400040104200000000E05E0000000000000000
      0000000000000000000000000000000000000000000000000000FF7FFF7FFF7F
      FF7F000000000000E07FE07F0000000000000000000000000000000000000000
      0000000000000000E07F000000001863000000000000000000000000007C0040
      00000000000000000000007C00400040004000000000E05E0000000000000000
      00000000000000000000000000000000000000000000E07F0000000000000000
      00000000000000000000E07FE07F000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000007C0040007C0000000000000000000000000000
      0000000000000000000000000000000000000000E07FE07F0000000000000000
      E07FE07F0000000000000000E07FE07F00000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000E07F000000000000000000000000
      E07F0000000000000000000000000000E07F0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000001700FF7F0000000000000000000000000000000000000000007C007C
      007C007C007C0000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000001863
      0000000000000000000000000000186300000000000000000000000000000000
      0000170017001700FF7F0000000000000000000000000000007C007C007C007C
      007C007C007C007C007C00000000000000000000000000000000000000000000
      0000FF7FFF7FFF7FFF7FFF7FFF7FFF7F00000000000000000000000000000000
      0000104200000000000010420000000000000000000000000000000000000000
      1700170017001700FF7F000000000000000000000000007C007C007C0000EF3D
      0000EF3D0000007C007C007C0000000000000000000000000000000000000000
      0000FF7F1F001F001F001F001F00FF7F00000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000001700
      17001700FF7F17001700FF7F0000000000000000007C007C007C000000000000
      0000000000000000007C007C007C000000000000000000000000000000000000
      0000FF7FFF7FFF7FFF7FFF7FFF7FFF7F00000000170017001700170000001863
      00000000000000001863000000000000000000000000FF7FFF7FFF7F17001700
      1700FF7F0000000017001700FF7F000000000000007C007C000000000000EF3D
      0000EF3D000000000000007C007C0000000000000000FF7FFF7FFF7FFF7FFF7F
      0000FF7F1F001F001F001F001F00FF7F000000001700FF7FFF7FFF7F00001863
      00000000000000001863000000000000000000000000FF7F1042104210421700
      FF7FFF7F0000000000001700FF7F00000000007C007C00000000000000000000
      000000000000000000000000007C007C000000000000FF7F1F001F001F001F00
      0000FF7FFF7FFF7FFF7FFF7FFF7FFF7F000000001700FF7F1042104200000000
      00000000000000000000000000000000000000000000FF7FFF7FFF7FFF7FFF7F
      FF7FFF7F00000000000000001700FF7F0000007C007C00000000000000000000
      000000000000000000000000007C007C000000000000FF7FFF7FFF7FFF7FFF7F
      0000FF7F1F001F00FF7F000000000000000000001700FF7FFF7FFF7FFF7F0000
      00000000000018630000000000000000000000000000FF7F1042104210421042
      1042FF7F000000000000000000001700FF7F007C007C0000000000000000EF3D
      0000EF3D000000000000000000000000000000000000FF7F1F001F001F001F00
      0000FF7FFF7FFF7FFF7F0000FF7F0000000000001700FF7F10421042FF7F1042
      0000000010421042FF7F000000000000000000000000FF7FFF7FFF7FFF7FFF7F
      FF7FFF7F0000000000000000000000001700007C007C00000000000000000040
      00000040000000000000000000000000000000000000FF7FFF7FFF7FFF7FFF7F
      0000FF7FFF7FFF7FFF7F000000000000000000001700FF7FFF7FFF7FFF7FFF7F
      00000000FF7FFF7FFF7F000000000000000000000000FF7F1042104200000000
      0000FF7F0000000000000000000000000000007C007C00000000000000000000
      000000000000007C007C007C007C007C000000000000FF7F1F001F00FF7F0000
      00000000000000000000000000000000000000001700FF7F10421042FF7F1042
      0000000010421042FF7F000000000000000000000000FF7FFF7FFF7FFF7F0000
      0000000000000000000000000000000000000000007C007C0000000000000000
      0000000000000000007C007C007C007C000000000000FF7FFF7FFF7FFF7F0000
      FF7F0000000000000000000000000000000000001700FF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7F170000000000000000000000FF7F1042104200000000
      0000000000000000000000000000000000000000007C007C007C000000000000
      00000000000000000000007C007C007C000000000000FF7FFF7FFF7FFF7F0000
      0000000000000000000000000000000000000000170017001700170017001700
      17001700170017001700170000000000000000000000FF7FFF7FFF7FFF7F0000
      00000000000000000000000000000000000000000000007C007C007C0000EF3D
      0000EF3D0000007C007C007C007C007C00000000000000000000000000000000
      0000000000000000000000000000000000000000170018631700170018631700
      1700186317001700186317000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000007C007C007C007C
      007C007C007C007C007C00000000007C00000000000000000000000000000000
      0000000000000000000000000000000000000000170017001700170017001700
      1700170017001700170017000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000007C007C
      007C007C007C0000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000F75EF75EF75EF75EF75EF75E
      F75EF75EF75EF75EF75EF75EF75EF75E00000000000000000000000000000000
      0000000000000000000000000000104200000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000F75EF75EF75EF75EF75EF75E
      F75EF75EF75EF75EF75EF75EF75EF75E00000000000000000000E07FE07FE07F
      E07FE07FE07FE07FE07FE07FE07F0000000000000000E07F0000000000000000
      00000000000000000000FF7F0000000000000000000000001700000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000E07FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FE07F0000000000000000FF7F0000000000000000
      00000000000000000000E07F0000000000000000000000001700170000000000
      0000E05EE05EE05EE05EE05E0000000000000000FF7FF75EFF7FF75EFF7FF75E
      FF7FF75EFF7FF75EFF7FF75EFF7FF75E0000000000000000FF7FE07FFF7F1863
      18631863186318631863FF7FE07F000000000000000000000000000000000000
      0000000000000000000000000000000000001700170017001700170017000000
      0000000000000000000000000000E05E00000000F75EFF7FF75EFF7FF75EFF7F
      F75EFF7FF75EFF7FF75EFF7F007CFF7F0000000000000000170017001863FF7F
      FF7FFF7FFF7FFF7FFF7FFF7FE07F00000000000000000000E07F000000000000
      000000000000E07F000000000000000000000000000000001700170000000000
      0000FF7F0000FF7F0000FF7F0000000000000000FF7FF75EFF7FF75EFF7FF75E
      FF7FF75EFF7FF75EFF7FF75EFF7FF75E000000001700170017001700FF7F1863
      FF7F1863186318631863FF7FE07F00000000000000000000FF7F000000000000
      000000000000FF7F000000000000000000000000000000001700000000000000
      0000000000000000000000000000104200000000000000000000000000000000
      0000000000000000000000000000000000000000170017001700170017001700
      1863FF7FFF7FFF7FFF7FFF7FE07F00000000000000000000E07F000000000000
      000000000000E07F000000000000000000000000000000000000000000000000
      00000000FF7FFF7FFF7FFF7F0000000000000000000000000000FF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7F0000000000000000000017001700170017001700FF7F
      FF7FFF7F186318631863FF7FE07F000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000FF7F10421042FF7F0000000000000000000000000000FF7F00000000
      00000000FF7F0000FF7F0000000000000000000000000000170017001863FF7F
      FF7FFF7FFF7FFF7FFF7FFF7FE07F000000000000000000000000E07F00000000
      000000000000E07F00000000000000000000000000000000FF7FFF7FFF7F0000
      000000000000FF7FFF7FFF7FFF7F000000000000000000000000FF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7F00000000000000000000000000001700FF7FFF7FFF7F
      18631863186318631863FF7FE07F000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000FF7F0000
      0000000000000000000000000000000000000000000000000000FF7F00000000
      FF7F000000000000000000000000000000000000000000000000E07FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FE07F000000000000000000000000000000000000
      0000000000000000000000000000000000000000FF7FFF7FFF7F0000FF7F0000
      0000000000000000000000000000000000000000000000000000FF7FFF7FFF7F
      FF7F0000FF7FFF7F000000000000000000000000000000000000E07FFF7FFF7F
      0000000000000000FF7FFF7FE07F0000000000000000000000000000E07F0000
      0000000000000000E07F00000000000000000000FF7F1042FF7F000000000000
      0000000000000000000000000000000000000000000000000000FF7F0000F75E
      FF7F0000FF7F0000000000000000000000000000000000000000E07FE07FE07F
      0000186318630000E07FE07FE07F000000000000000000000000000000000000
      0000000000000000000000000000000000000000FF7F1042FF7F000000000000
      0000000000000000000000000000000000000000000000000000FF7FFF7FFF7F
      FF7F000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000FF7FFF7F0000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FF7FEF3DEF3D
      EF3DEF3DEF3DEF3D000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F00000000000000000000FF7F0000F75E
      0000F75E0000EF3D000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000FF7F00000000FF7F00000000
      FF7F00000000FF7F000000000000FF7F00000000000000000000FF7F0000F75E
      0000EF3D0000EF3D000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000001F7C1F7C
      1F7C1F7C1F7C0000000000000000000000000000FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F00000000000000000000FF7F0000F75E
      0000F75E0000EF3D000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000001F7C1F7C1F7C
      1F7C1F7C1F7C1F7C000000000000000000000000FF7F00000000FF7F00000000
      FF7F00000000FF7F000000000000FF7F00000000000000000000FF7F0000F75E
      0000EF3D0000EF3D000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000001F7C1F7C1F7C1F7C
      1F7C1F7C1F7C1F7C1F7C00000000000000000000FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F00000000000000000000FF7F0000F75E
      0000F75E0000EF3D000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000001F7C1F7C1F7C1F7C
      1F7C1F7C1F7C1F7C1F7C00000000000000000000FF7F00000000FF7F00000000
      FF7F00000000FF7F000000000000FF7F00000000000000000000FF7F0000F75E
      0000EF3D0000EF3D000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000001F7C1F7C1F7C1F7C
      1F7C1F7C1F7C1F7C1F7C00000000000000000000FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F00000000000000000000FF7F0000F75E
      0000F75E0000EF3D000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000FF7FFF7F1F7C1F7C
      1F7C1F7C1F7C1F7C1F7C00000000000000000000FF7F00000000FF7F00000000
      FF7F00000000FF7F000000000000FF7F00000000000000000000FF7F0000F75E
      0000EF3D0000EF3D000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FF7FFF7F1F7C
      1F7C1F7C1F7C1F7C000000000000000000000000FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F00000000000000000000EF3D0000EF3D
      0000EF3D0000EF3D000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000FF7F1F7C
      1F7C1F7C1F7C00000000000000000000000000001F001F001F001F001F001F00
      1F001F001F001F001F001F001F001F0000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000F75EF75E1F001F001F001F00
      1F001F001F001F001F001F00F75EF75E0000000000000000FF7FF75EF75EF75E
      EF3DEF3DEF3DEF3DEF3D00000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000EF3D
      EF3DEF3D00000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000FF03FF03
      FF03FF03FF0300000000000000000000000000000000000000000000007C007C
      007C007C007C00000000000000000000000000000000000000000000E003E003
      E003E003E00300000000000000000000000000000000000000000000E07FE07F
      E07FE07FE07F0000000000000000000000000000000000000000FF03FF03FF03
      FF03FF03FF03FF03000000000000000000000000000000000000007C007C007C
      007C007C007C007C000000000000000000000000000000000000E003E003E003
      E003E003E003E003000000000000000000000000000000000000E07FE07FE07F
      E07FE07FE07FE07F00000000000000000000000000000000FF03FF03FF03FF03
      FF03FF03FF03FF03FF030000000000000000000000000000007C007C007C007C
      007C007C007C007C007C0000000000000000000000000000E003E003E003E003
      E003E003E003E003E0030000000000000000000000000000E07FE07FE07FE07F
      E07FE07FE07FE07FE07F0000000000000000000000000000FF03FF03FF03FF03
      FF03FF03FF03FF03FF030000000000000000000000000000007C007C007C007C
      007C007C007C007C007C0000000000000000000000000000E003E003E003E003
      E003E003E003E003E0030000000000000000000000000000E07FE07FE07FE07F
      E07FE07FE07FE07FE07F0000000000000000000000000000FF03FF03FF03FF03
      FF03FF03FF03FF03FF030000000000000000000000000000007C007C007C007C
      007C007C007C007C007C0000000000000000000000000000E003E003E003E003
      E003E003E003E003E0030000000000000000000000000000E07FE07FE07FE07F
      E07FE07FE07FE07FE07F0000000000000000000000000000FF7FFF7FFF03FF03
      FF03FF03FF03FF03FF030000000000000000000000000000FF7FFF7F007C007C
      007C007C007C007C007C0000000000000000000000000000FF7FFF7FE003E003
      E003E003E003E003E0030000000000000000000000000000FF7FFF7FE07FE07F
      E07FE07FE07FE07FE07F00000000000000000000000000000000FF7FFF7FFF03
      FF03FF03FF03FF03000000000000000000000000000000000000FF7FFF7F007C
      007C007C007C007C000000000000000000000000000000000000FF7FFF7FE003
      E003E003E003E003000000000000000000000000000000000000FF7FFF7FE07F
      E07FE07FE07FE07F0000000000000000000000000000000000000000FF7FFF03
      FF03FF03FF0300000000000000000000000000000000000000000000FF7F007C
      007C007C007C00000000000000000000000000000000000000000000FF7FE003
      E003E003E00300000000000000000000000000000000000000000000FF7FE07F
      E07FE07FE07F0000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000D00000000100010000000000800600000000000000000000
      000000000000000000000000FFFFFF0000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000C007FFFFFFFFF80FC007F83FFFFFF80F
      C007E00F8000F80FC007C0078000F80FC00780038000F80FC00780038000F80F
      C00700018000F807C00700018000F007C00700018000E003C00700018000C001
      C00700018000C001C00780038000C001C0078003FFFFC001C007C007FFFFE003
      C007E00FFFFFF007C007F83FFFFFF80FFFFFFF00FFFFFFFFFFC0FF00FE7FFFFF
      FF80FF00FC3FE001FF00FF00F80FC001FF00FF00F007C001FE408000E001C001
      FD00800EC001C001C000BF00C001C0018000A40AC001C0018000BF01C003C001
      8000A403C007C0018000BFF7F00FC00180008007FC1FC00380028007FF3FFFFF
      C0018007FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFEFFFEFFFFFFFFFFFCFFFE7FF83FFEFFF8FFFE3FF83FFC7F
      F00FE01FF83FF83FE00FE00FF83FF01FC00FE007C007E00FE00FE00FE00FC007
      F00FE01FF01FF83FF8FFFE3FF83FF83FFCFFFE7FFC7FF83FFEFFFEFFFEFFF83F
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFFF80FFFFFFFFFF9FFF80FFFFFFFF
      FF8EFF80FFFFFFFFFFCCFF80FFFFFFFF8001FF80FFFFFE3FBFE38000FF7FFE3F
      A4A18000FE3FFE3FBFCCBF80FC1FF80F8002A480F80FFC1FC003BFF7FE3FFE3F
      C003A497FE3FFF7FC003BFF6FE3FFFFFC0038004FFFFFFFFD24B8006FFFFFFFF
      C0038007FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF83E0C0038007007F
      83E086038007007F83E08603BFD7FFFE83608603A48707FEEE3B8003BFC707EF
      EC1B8003A483FF77E0038003BFCBFE7EFC1F80038001FC7EFE3F80038005F878
      FF7F80038000FC7EFC1F801FFFF0FE7EFC1F8017FFF8FF7FFC1F8187FFF807FB
      FC1FFFC7FFFC07F6FFFFFF87FFFFFFFEF862FFE0E00F800780E0FFE0C0038007
      01E0FE008001800701E0FE000001800031E1C0000000800031C1C00300008000
      C181C00300008000C307C00300008000FE17C0030000FC00CC37C00F0000EC00
      A877C00F0000C40040F7C00F0000EC0001E3C00F0000EC01C1E3C00F0001E403
      C0E3FFFF4003FFFFC83FFFFF8007FFFFC03FFFFFFFFFF801001FF83FFBFFF801
      0000E00FF992F8010000C0078092F80100008003F9FFF80100008003FBFFF801
      00000001FFFFB00100000001E0FFB00100000001E0FFE0010000000180FF8203
      0000000180FF80070000800380FFC00F0000800381FF927F0000C00783FFB33F
      FC00E00F87FFF3FFFC01F83FFFFFFFFFFFFFFF7EFFFFE1F3C0019001FFFFE0E1
      8031C003FFFFE0408031E003C3FFF0008031E003C0FFF8018001E003E03FFC03
      8001E003F01FFE0780010001F80FFE078FF18000F807FC078FF1E007F00FF803
      8FF1E00FF807F0418FF1E00FFE03F0E08FF1E027FF81F9F08FF5C073FFE3FFF8
      80019E79FFFFFFFFFFFF7EFEFFFFFFFFFFFFFFFFFFFFF8F8FF9FF83FFE00F8F8
      FF0FE00FFE00F870FE0FC447FE00F80080078C638000800080239C7380008000
      80333FF98000800080393EF980008001803C3C7F80018003803E3C7F80038003
      873F3C4180078003803F9C61807F8007857F8C7180FF800780FFC44181FF8007
      81FFE00DFFFF8007FFFFF83FFFFFFFFF8001FFFFFFFFFFFF0000F00183E0FF03
      0000E00183E0EE010000E00183E0E6000000E001808002000000E0018000E654
      000080018100EE04000080018100FF01E0078001C001C103E007E001E083C181
      E007E001E08301C1E007E001F1C701FFE00FE001F1C701FFE01FE001F1C703FF
      E03FF003FFFF07FFE07FFE1FFFFF0FFFFFFFFFFFE00FFFFFFFFF0000E00FFFFF
      F83F0000E00FC007E00F0000E00FE7E7C0070000E00FF3F7C0070000E00FF9F7
      80030000E00FFCFF80030000A00BFE7F80030000C007FF3F80030000E00FFE7F
      C0070000E00FFCFFC0070000C007F9F7E00F0000C007F3F7F83F0000C007E7E7
      FFFFFFFFF83FC007FFFFFFFFF83FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      F83FF83FF83FF83FE00FE00FE00FE00FC007C007C007C007C007C007C007C007
      8003800380038003800380038003800380038003800380038003800380038003
      C007C007C007C007C007C007C007C007E00FE00FE00FE00FF83FF83FF83FF83F
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00000000000000000000000000000000
      000000000000}
  end
  object SNTab: TZMySqlQuery
    Database = DM1.db1
    Transaction = DM1.Transact1
    CachedUpdates = False
    ShowRecordTypes = [ztModified, ztInserted, ztUnmodified]
    Options = [doHourGlass, doAutoFillDefs, doUseRowId]
    LinkOptions = [loAlwaysResync]
    Constraints = <>
    ExtraOptions = [moStoreResult]
    Macros = <>
    Sql.Strings = (
      'select * from ARTIKEL_SERNUM')
    RequestLive = True
    Left = 340
    Top = 119
    object SNTabARTIKEL_ID: TIntegerField
      FieldName = 'ARTIKEL_ID'
      Required = True
    end
    object SNTabSERNUMMER: TStringField
      FieldName = 'SERNUMMER'
      Required = True
      Size = 255
    end
    object SNTabVERK_NUM: TIntegerField
      FieldName = 'VERK_NUM'
    end
    object SNTabVK_JOURNAL_ID: TIntegerField
      FieldName = 'VK_JOURNAL_ID'
    end
    object SNTabVK_JOURNALPOS_ID: TIntegerField
      FieldName = 'VK_JOURNALPOS_ID'
    end
  end
  object SNDS: TDataSource
    DataSet = SNTab
    Left = 396
    Top = 119
  end
  object XPMenu1: TXPMenu
    DimLevel = 30
    GrayLevel = 10
    Font.Charset = ANSI_CHARSET
    Font.Color = clMenuText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    Color = clBtnFace
    DrawMenuBar = False
    IconBackColor = clBtnFace
    MenuBarColor = clBtnFace
    SelectColor = clHighlight
    SelectBorderColor = clHighlight
    SelectFontColor = clMenuText
    DisabledColor = clInactiveCaption
    SeparatorColor = clBtnFace
    CheckedColor = clHighlight
    IconWidth = 24
    DrawSelect = True
    UseSystemColors = True
    UseDimColor = False
    OverrideOwnerDraw = False
    Gradient = False
    FlatMenu = True
    AutoDetect = False
    XPControls = [xcMainMenu, xcPopupMenu, xcControlbar, xcCheckBox, xcRadioButton]
    Active = True
    Left = 463
    Top = 120
  end
end
