{

CAO-Faktura f�r Windows Version 1.0
Copyright (C) 2003 Jan Pokrandt / Jan@JP-SOFT.de

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

******* CAO-Faktura comes with ABSOLUTELY NO WARRANTY ***************

Programm     : CAO-Faktura / Kasse
Modul        : CAO_KASSE_SETUP
Stand        : 28.02.2004
Version      : 1.2.4.1
Beschreibung : Kasse Setup / Einstellungen

History :

17.11.2003 - JP: Unit erstellt
28.02.2004 - JP: Setup f�r BON-Templates erweitert

}
unit CAO_KASSE_SETUP;

{$I CAO32.INC}

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, StdCtrls, Buttons, ExtCtrls, jpeg, Grids;

type
  TKasseSetupForm = class(TForm)
    BottomPan: TPanel;
    UebernehmenBtn: TBitBtn;
    AbortBtn: TBitBtn;
    PC1: TPageControl;
    AllgemeinTS: TTabSheet;
    DruckenTS: TTabSheet;
    TabSheet3: TTabSheet;
    Label1: TLabel;
    Label2: TLabel;
    PreisEbeneLB: TComboBox;
    UseBondruckerCB: TCheckBox;
    DruckerPortLab: TLabel;
    DruckerPort: TEdit;
    Label4: TLabel;
    DruckerLB: TComboBox;
    DruckDialogLab: TLabel;
    DruckdialogCB: TCheckBox;
    SofortDruckLab: TLabel;
    SofortDruckCB: TCheckBox;
    FormularLab: TLabel;
    FormularLB: TComboBox;
    BonTS: TTabSheet;
    BonUsrLab1: TLabel;
    BonUsrLab2: TLabel;
    BonUsrLab3: TLabel;
    BonUsr2: TEdit;
    BonUsr1: TEdit;
    BonUsr3: TEdit;
    Panel2: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    Image2: TImage;
    Panel3: TPanel;
    Panel4: TPanel;
    Image3: TImage;
    Image4: TImage;
    Label3: TLabel;
    OpenSchubladeCB: TCheckBox;
    BonLayoutTab: TTabSheet;
    BonTemplate: TMemo;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    BonPosZeile1: TMemo;
    BonPosZeileN1: TMemo;
    BonGrid: TStringGrid;
    procedure FormShow(Sender: TObject);
    procedure UseBondruckerCBClick(Sender: TObject);
    procedure UebernehmenBtnClick(Sender: TObject);
  private
    { Private-Deklarationen }
    procedure UpdateDruckerSettings;
  public
    { Public-Deklarationen }
  end;

var
  KasseSetupForm: TKasseSetupForm;

implementation

{$R *.DFM}

uses CAO_DM, PRINTERS, cao_kasse_main;

//------------------------------------------------------------------------------
procedure TKasseSetupForm.UpdateDruckerSettings;
var I : Integer;
begin
     if UseBondruckerCB.Checked then
     begin
       DruckerPort.Enabled :=True;  DruckerPortLab.Enabled :=True;
       DruckerLB.Items.Clear;
       Druckerport.Text :=DM1.ReadString ('KASSE','DRUCKERPORT','LPT2');
       DM1.UniQuery.SQL.Text :='select VAL_CHAR as NAME from REGISTERY where '+
                               'POSITION("TREIBER\\BONDRUCKER" IN MAINKEY)=1 '+
                               'and LENGTH(MAINKEY)>19 and NAME="DEFAULT"';
       DM1.UniQuery.Open;
       while not DM1.UniQuery.Eof do
       begin
            DruckerLB.Items.Add (DM1.UniQuery.FieldByName('NAME').AsString);
            DM1.UniQuery.Next;
       end;
       DM1.UniQuery.Close;
       DruckerLB.ItemIndex :=DruckerLB.Items.IndexOf
                              (DM1.ReadString ('KASSE','DRUCKERTREIBER',''));
       FormularLB.Items.Clear;
       FormularLB.Enabled    :=False;  FormularLab.Enabled :=False;
       DruckdialogCB.Enabled :=False; DruckDialogLab.Enabled :=False;
       DruckdialogCB.Checked :=False;

       BonUsr1.Enabled :=True;
       BonUsr2.Enabled :=True;
       BonUsr3.Enabled :=True;
       BonUsrLab1.Enabled :=True;
       BonUsrLab2.Enabled :=True;
       BonUsrLab3.Enabled :=True;
     end
        else
     begin
       DruckerPort.Enabled :=False; DruckerPortLab.Enabled :=False;
       DruckerLB.Items.Clear;

       if Printer.Printers.Count>0 then
         for i:=0 to Printer.Printers.Count-1
          do DruckerLB.Items.Add (Printer.Printers[i]);

       DruckerLB.ItemIndex :=DruckerLB.Items.IndexOF(DM1.ReadString('KASSE','DEFAULT_DRUCKER',''));

       FormularLB.Items.Clear;
       DM1.UniQuery.SQL.Text :='select NAME from REGISTERY where '+
                               'MAINKEY="MAIN\\REPORT\\KASSENBON" '+
                               'and NAME !="DEFAULT"';
       DM1.UniQuery.Open;
       while not DM1.UniQuery.Eof do
       begin
            FormularLB.Items.Add (DM1.UniQuery.FieldByName('NAME').AsString);
            DM1.UniQuery.Next;
       end;
       DM1.UniQuery.Close;

       FormularLB.ItemIndex :=FormularLB.Items.IndexOf(DM1.ReadString('KASSE','DEFAULT_FORMULAR',''));


       FormularLB.Enabled    :=True; FormularLab.Enabled :=True;
       DruckdialogCB.Enabled :=True; DruckDialogLab.Enabled :=True;

       BonUsr1.Enabled :=False;
       BonUsr2.Enabled :=False;
       BonUsr3.Enabled :=False;
       BonUsrLab1.Enabled :=False;
       BonUsrLab2.Enabled :=False;
       BonUsrLab3.Enabled :=False;
     end;
end;
//------------------------------------------------------------------------------
procedure TKasseSetupForm.FormShow(Sender: TObject);
var i : integer;
begin
     BonGrid.Cells[0, 0] :='Feld'; BonGrid.Cells[1,0] :='Wert';
     BonGrid.Cells[0, 1] :='�berschrift 1';
     BonGrid.Cells[0, 2] :='�berschrift 2';
     BonGrid.Cells[0, 3] :='�berschrift 3';
     BonGrid.Cells[0, 4] :='Unterschrift 1';
     BonGrid.Cells[0, 5] :='Unterschrift 2';
     BonGrid.Cells[0, 6] :='Unterschrift 3';
     BonGrid.Cells[0, 7] :='Benutzerfeld 1';
     BonGrid.Cells[0, 8] :='Benutzerfeld 2';
     BonGrid.Cells[0, 9] :='Benutzerfeld 3';
     BonGrid.Cells[0,10] :='Benutzerfeld 4';
     BonGrid.Cells[0,11] :='Benutzerfeld 5';

     PC1.ActivePage :=PC1.Pages[0];

     PreisEbeneLB.Items.Clear;
     For i:=1 to DM1.AnzPreis do PreisEbeneLB.Items.Add ('VK'+Inttostr(I));
     PreisEbeneLB.ItemIndex :=DM1.ReadInteger ('KASSE','PREISEBENE',DM1.AnzPreis)-1;
     UseBondruckerCB.Checked :=DM1.ReadBoolean ('KASSE','BONDRUCKER',False);
     OpenSchubladeCB.Checked :=DM1.ReadBoolean ('KASSE','SCHUBLADE_OEFFNEN',True);

     UpdateDruckerSettings;

     DruckdialogCB.Checked :=DM1.ReadBoolean ('KASSE','DRUCKDIALOG',False);
     SofortDruckCB.Checked :=DM1.ReadBoolean ('KASSE','SOFORTDRUCK',False);

     {
     BonUsr1.Text :=dm1.readstring ('KASSE','BON_UEBERSCHRIFT1','   UEBERSCHRIFT');
     BonUsr2.Text :=dm1.readstring ('KASSE','BON_UEBERSCHRIFT2','      ------');
     BonUsr3.Text :=dm1.readstring ('KASSE','BON_UEBERSCHRIFT3','         Telefon : -----/------         ');}

     BonGrid.Cells[1, 1] :=KMainForm.BON_HDR1;
     BonGrid.Cells[1, 2] :=KMainForm.BON_HDR2;
     BonGrid.Cells[1, 3] :=KMainForm.BON_HDR3;

     BonGrid.Cells[1, 4] :=KMainForm.BON_FTR1;
     BonGrid.Cells[1, 5] :=KMainForm.BON_FTR2;
     BonGrid.Cells[1, 6] :=KMainForm.BON_FTR3;

     BonGrid.Cells[1, 7] :=KMainForm.BON_USR1;
     BonGrid.Cells[1, 8] :=KMainForm.BON_USR2;
     BonGrid.Cells[1, 9] :=KMainForm.BON_USR3;
     BonGrid.Cells[1,10] :=KMainForm.BON_USR4;
     BonGrid.Cells[1,11] :=KMainForm.BON_USR5;

     BonTemplate.Text   :=KMainForm.BonTemplate;
     BonPosZeile1.Text  :=KMainForm.PosTemplateMenge1;
     BonPosZeileN1.Text :=KMainForm.PosTemplateMengeN1;
end;
//------------------------------------------------------------------------------
procedure TKasseSetupForm.UseBondruckerCBClick(Sender: TObject);
begin
     UpdateDruckerSettings;
     BonLayoutTab.TabVisible :=UseBondruckerCB.Checked;
end;
//------------------------------------------------------------------------------
procedure TKasseSetupForm.UebernehmenBtnClick(Sender: TObject);
begin
     DM1.WriteInteger ('KASSE','PREISEBENE',PreisEbeneLB.ItemIndex+1);
     DM1.WriteBoolean ('KASSE','DRUCKDIALOG',DruckdialogCB.Checked);
     DM1.WriteBoolean ('KASSE','SOFORTDRUCK',SofortDruckCB.Checked);
     DM1.WriteBoolean ('KASSE','BONDRUCKER',UseBondruckerCB.Checked);
     DM1.WriteBoolean ('KASSE','SCHUBLADE_OEFFNEN',OpenSchubladeCB.Checked);
     if UseBondruckerCB.Checked then
     begin
        DM1.WriteString ('KASSE','DRUCKERTREIBER',DruckerLB.Items[DruckerLB.ItemIndex]);
        DM1.WriteString ('KASSE','DRUCKERPORT',DruckerPort.Text);
     end
        else
     begin
        DM1.WriteString ('KASSE','DEFAULT_DRUCKER',DruckerLB.Items[DruckerLB.ItemIndex]);
        DM1.WriteString ('KASSE','DEFAULT_FORMULAR',FormularLB.Items[FormularLB.ItemIndex]);
     end;
     {
     dm1.writestring ('KASSE','BON_UEBERSCHRIFT1',BonUsr1.Text);
     dm1.writestring ('KASSE','BON_UEBERSCHRIFT2',BonUsr2.Text);
     dm1.writestring ('KASSE','BON_UEBERSCHRIFT3',BonUsr3.Text);
     }

     DM1.WriteString ('KASSE','BON_UEBERSCHRIFT1',BonGrid.Cells[1,1]);
     DM1.WriteString ('KASSE','BON_UEBERSCHRIFT2',BonGrid.Cells[1,2]);
     DM1.WriteString ('KASSE','BON_UEBERSCHRIFT3',BonGrid.Cells[1,3]);

     DM1.WriteString ('KASSE','BON_UNTERSCHRIFT1','Unterschrift 1');
     DM1.WriteString ('KASSE','BON_UNTERSCHRIFT2','Unterschrift 2');
     DM1.WriteString ('KASSE','BON_UNTERSCHRIFT3','Unterschrift 3');

     DM1.WriteString ('KASSE','BON_USERFELD_1'   ,'Benutzerfeld 1');
     DM1.WriteString ('KASSE','BON_USERFELD_2'   ,'Benutzerfeld 2');
     DM1.WriteString ('KASSE','BON_USERFELD_3'   ,'Benutzerfeld 3');
     DM1.WriteString ('KASSE','BON_USERFELD_4'   ,'Benutzerfeld 4');
     DM1.WriteString ('KASSE','BON_USERFELD_5'   ,'Benutzerfeld 5');


     DM1.WriteLongString('KASSE','BON_TEMPLATE',BonTemplate.Text);
     DM1.WriteLongString('KASSE','BON_POSZEILE_M1',BonPosZeile1.Text);
     DM1.WriteLongString('KASSE','BON_POSZEILE_MX',BonPosZeileN1.Text);

     KMainForm.FormShow(Sender);
end;
//------------------------------------------------------------------------------
end.
