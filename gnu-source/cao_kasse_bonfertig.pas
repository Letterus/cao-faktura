{

CAO-Faktura f�r Windows Version 1.0
Copyright (C) 2003 Jan Pokrandt / Jan@JP-SOFT.de

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

******* CAO-Faktura comes with ABSOLUTELY NO WARRANTY ***************

Programm     : CAO-Faktura / Kasse
Modul        : CAO_KASSE_BONFERTIG
Stand        : 23.11.2003
Version      : 1.2.4.1
Beschreibung : Kasse BON-FERTIG Dialog

History :

31.05.2003 - JP: akt. Version zum CVS hinzugef�gt
21.08.2003 - DP: Kundendisplay eingebunden
23.11.2003 - JP: Dialog v�llig �berarbeitet
}

unit cao_kasse_bonfertig;

{$I CAO32.INC}

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, Mask, JvToolEdit, JvCurrEdit, JvComponent,
  JvSpeedButton;

type
  TBonFertigForm = class(TForm)
    BuchenBtn: TBitBtn;
    AbortBtn: TBitBtn;
    Label1: TLabel;
    Panel1: TPanel;
    Panel2: TPanel;
    zurueck: TLabel;
    Panel3: TPanel;
    Gegeben: TJvxCurrencyEdit;
    BarBtn: TJvSpeedButton;
    ScheckBtn: TJvSpeedButton;
    ECKarteBtn: TJvSpeedButton;
    ZahlBetragLab: TLabel;
    ZahlartLab: TLabel;
    Label5: TLabel;
    Label3: TLabel;
    Label2: TLabel;
    SpeedButton1: TSpeedButton;
    SpeedButton2: TSpeedButton;
    SpeedButton3: TSpeedButton;
    SpeedButton4: TSpeedButton;
    SpeedButton5: TSpeedButton;
    SpeedButton6: TSpeedButton;
    SpeedButton7: TSpeedButton;
    SpeedButton8: TSpeedButton;
    SpeedButton9: TSpeedButton;
    NumPadBtn: TSpeedButton;
    SpeedButton11: TSpeedButton;
    BackBtn: TSpeedButton;
    procedure FormShow(Sender: TObject);
    procedure zahlartKeyPress(Sender: TObject; var Key: Char);
    procedure BuchenBtnClick(Sender: TObject);
    procedure GegebenChange(Sender: TObject);
    procedure GegebenKeyPress(Sender: TObject; var Key: Char);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure GegebenExit(Sender: TObject);
    procedure zahlartClick(Sender: TObject);
    procedure NumPadBtnClick(Sender: TObject);
    procedure BackBtnClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private-Deklarationen }
    za: integer;
    diff: double;
    LastBild : Integer;
    UseKeyPad : Boolean;
  public
    { Public-Deklarationen }
  end;

var
  BonFertigForm: TBonFertigForm;

implementation

uses CAO_DM, cao_kasse_main;

{$R *.DFM}

//******************************************************************************
procedure TBonFertigForm.FormCreate(Sender: TObject);
begin
     UseKeyPad :=False;
end;
//******************************************************************************
procedure TBonFertigForm.FormShow(Sender: TObject);
begin
  if UseKeyPad
   then Width :=661
   else Width :=516;

  ZahlBetragLab.Caption :=FormatFloat(',#0.00 "' + DM1.Leitwaehrung + '"',
                                       KMainForm.KopfTabBSumme.Value);
  diff := KMainForm.KopfTabBSumme.Value;
  Gegeben.DisplayFormat := ',0.00 ' + DM1.Leitwaehrung + ';-,0.00 ' +
    DM1.Leitwaehrung;
  Gegeben.Value := 0;
  Zurueck.Caption := FormatFloat(',#0.00 "' + DM1.Leitwaehrung + '"', 0);
  BuchenBtn.Enabled := False;
  GegebenChange (Self);

  AbortBtn.Kind        :=bkAbort;
  AbortBtn.ModalResult :=mrCancel;
  AbortBtn.Caption     :='Abbrechen [ESC]';
  AbortBtn.Spacing     :=29;

  BarBtn.Enabled     :=True;
  ScheckBtn.Enabled  :=True;
  ECKarteBtn.Enabled :=True;

  //ZahlartClick(BarBtn);
  za := 1; // BAR
  gegeben.value := 0;
  gegeben.enabled := true;
  ZahlartLab.Caption :='BAR';


  try gegeben.setfocus; except end;


  KMainForm.ZeigeBild('BILD20',
                      [KMainForm.KopfTabNSumme.Value,
                       KMainForm.KopfTabBSUMME.Value,
                       KMainForm.waehrung],
                      'TBonFertigForm.FormShow');
  LastBild :=20;
end;

//******************************************************************************
procedure TBonFertigForm.zahlartKeyPress(Sender: TObject; var Key: Char);
begin
  if key = #13 then
  begin
    key := #0;
    try
      if gegeben.enabled then gegeben.setfocus
      else buchenbtn.setfocus;
    except
    end;
  end;
end;

//******************************************************************************
procedure TBonFertigForm.BuchenBtnClick(Sender: TObject);
begin
     BuchenBtn.Enabled  :=False;
     gegeben.enabled    :=False;
     BarBtn.Enabled     :=False;
     ScheckBtn.Enabled  :=False;
     ECKarteBtn.Enabled :=False;

     KMainForm.ZeigeBild('BILD35',
                         [KMainForm.KopfTabNSumme.Value,
                          KMainForm.KopfTabBSUMME.Value,
                          {Zahlart.Items[Zahlart.ItemIndex]}ZahlartLab.Caption,
                          gegeben.value,
                          diff,
                          KMainForm.waehrung],
                         'TBonFertigForm.BuchenBtnClick');

     // jetzt buchen !!!
     KMainForm.BucheBon(za, gegeben.value, diff);
     AbortBtn.Kind    := bkClose;
     AbortBtn.Tag     := 2;
     AbortBtn.Caption :='Schlie�en [ESC]';
     AbortBtn.Spacing :=34;

     try ABortBtn.SetFocus; except end;

     if KMainForm.OpenSchublade then
     begin
        try
           KMainForm.Schubladeffnen1Click(Sender);
        except end;
     end;
end;

//******************************************************************************
procedure TBonFertigForm.GegebenChange(Sender: TObject);
begin
  diff := Gegeben.Value;
  diff := diff - round(KMainForm.KopfTabBSumme.Value*100)/100;
  Zurueck.Caption := FormatFloat(',###,##0.00 "' + DM1.Leitwaehrung + '"',
    diff);
  if diff < 0 then Zurueck.font.color := clRed
  else Zurueck.Font.color := clBlack;
  BuchenBtn.Enabled := (diff >= 0) or (za = 5) or (za = 6);


  if LastBild=20 then
  begin
    LastBild :=25;
    if not AbortBtn.Focused then
    KMainForm.ZeigeBild('BILD25',
                        [KMainForm.KopfTabNSumme.Value,
                         KMainForm.KopfTabbSumme.Value,
                         ZahlartLab.Caption,
                         KMainForm.waehrung],
                        'TBonFertigForm.GegebenChange');
  end;
end;

//******************************************************************************
procedure TBonFertigForm.GegebenKeyPress(Sender: TObject; var Key: Char);
begin
  if (key = #13) then
  begin
    if BuchenBtn.Enabled then
    begin
      try
        BuchenBtn.SetFocus;
      except end;
    end
    else
    begin
      MessageBeep(0);
      if Gegeben.Value > 0 then
      begin
        if MessageDlg('Der Betrag ist zu niedrig,' + #13#10 +
          'Wollen Sie alle Preise neu berechnen ?', mtconfirmation, [mbNo,
          mbYes], 0) = mrYes then
        begin
          KMainForm.RecalcPreise(Gegeben.Value / KMainForm.KopfTabBSumme.Value);
          ZahlBetragLab.Caption :=FormatFloat(',#0.00 "'+DM1.Leitwaehrung+'"',
                                              KMainForm.KopfTabBSumme.Value);

          GegebenChange(Sender);
          gegeben.SelectAll;
        end;
      end;
    end;
    key := #0;
  end;
end;

//******************************************************************************
procedure TBonFertigForm.ZahlartClick(Sender: TObject);
begin
  case TBitBtn(Sender).Tag of
    0: begin
        za := 1;
        gegeben.value := 0;
        gegeben.enabled := true;
        ZahlartLab.Caption :='BAR';
      end;
    1: begin
        za := 5;
        gegeben.value := KMainForm.KopfTabBSumme.Value;
        gegeben.enabled := false;
        ZahlartLab.Caption :='SCHECK';
      end;
    2: begin
        za := 6;
        gegeben.value := KMainForm.KopfTabBSumme.Value;
        gegeben.enabled := false;
        ZahlartLab.Caption :='EC-Karte';
      end;
  end;
  if not AbortBtn.Focused then
    KMainForm.ZeigeBild('BILD25',
                        [KMainForm.KopfTabNSumme.Value,
                         KMainForm.KopfTabbSumme.Value,
                         ZahlartLab.Caption{Zahlart.Items[Zahlart.ItemIndex]},
                         KMainForm.waehrung],
                        'TBonFertigForm.ZahlartExit');
end;
//******************************************************************************
procedure TBonFertigForm.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_ESCAPE then
  begin
    key := 0;
    Close;
  end
  else
  if (Key = VK_F8)and(Shift=[]) then
  begin
    key := 0;
    if KMainForm.Gebucht then KMainForm.letztenBondrucken1Click(Sender);
  end else
  if (Key = VK_F2)and(Shift=[]) then
  begin
     key :=0;
     zahlartClick(BarBtn);
     Gegeben.SetFocus;
  end else
  if (Key = VK_F3)and(Shift=[]) then
  begin
     key :=0;
     zahlartClick(ScheckBtn);
     BuchenBtn.SetFocus;
  end else
  if (Key = VK_F4)and(Shift=[]) then
  begin
     key :=0;
     zahlartClick(ECKarteBtn);
     BuchenBtn.SetFocus;
  end;
end;
//******************************************************************************
procedure TBonFertigForm.GegebenExit(Sender: TObject);
begin
  if not AbortBtn.Focused then
    KMainForm.ZeigeBild('BILD30',
                        [KMainForm.KopfTabNSumme.Value,
                         KMainForm.KopfTabBSumme.Value,
                         ZahlartLab.Caption{Zahlart.Items[Zahlart.ItemIndex]},
                         Gegeben.Value,
                         diff,
                         KMainForm.waehrung],
                        'TBonFertigForm.GegebenExit');
end;
//******************************************************************************
procedure TBonFertigForm.NumPadBtnClick(Sender: TObject);
var C : Char;
begin
     C :=tSpeedButton(Sender).Caption[1];
     PostMessage(Gegeben.Handle, WM_CHAR, Word(Ord(C)), 0);
end;
//******************************************************************************
procedure TBonFertigForm.BackBtnClick(Sender: TObject);
begin
     PostMessage(Gegeben.Handle, WM_CHAR, Word(8), 0);
end;
//******************************************************************************
end.

