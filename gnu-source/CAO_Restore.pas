{******************************************************************************}
{ PROJEKT      : CAO-FAKTURA                                                   }
{ DATEI        : CAO_RESTORE.PAS / DFM                                         }
{ BESCHREIBUNG : Dialog zur R�cksicherung (SQL)                                }
{ STAND        : 03.05.2004                                                    }
{ VERSION      : 1.2.5.3                                                       }
{ � 2004 Jan Pokrandt / Jan@JP-Soft.de                                         }
{                                                                              }
{ Diese Unit geh�rt zum Projekt CAO-Faktura und wird unter der                 }
{ GNU General Public License Version 2.0 freigegeben                           }
{                                                                              }
{******************************************************************************}
{                                                                              }
{ This program is free software; you can redistribute it and/or                }
{ modify it under the terms of the GNU General Public License                  }
{ as published by the Free Software Foundation; either version 2               }
{ of the License, or any later version.                                        }
{                                                                              }
{ This program is distributed in the hope that it will be useful,              }
{ but WITHOUT ANY WARRANTY; without even the implied warranty of               }
{ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                }
{ GNU General Public License for more details.                                 }
{                                                                              }
{ You should have received a copy of the GNU General Public License            }
{ along with this program; if not, write to the Free Software                  }
{ Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.  }
{                                                                              }
{    ******* CAO-Faktura comes with ABSOLUTELY NO WARRANTY ***************     }
{                                                                              }
{******************************************************************************}
{                                                                              }
{ Historie :                                                                   }
{ 03.05.2003 - Unit erstellt Jan Pokrandt                                      }
{                                                                              }
{                                                                              }
{ Todo : Dialog und Funktion fertigstellen                                     }
{                                                                              }
{******************************************************************************}


unit CAO_RESTORE;

interface

{$I CAO32.INC}
{$O-}

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ComCtrls, ZipMstr, Buttons, Mask, JvToolEdit;

type
  TRestoreForm = class(TForm)
    StartRestoreBtn: TBitBtn;
    pb1: TProgressBar;
    Zip: TZipMaster;
    Label1: TLabel;
    TabNameLab: TLabel;
    PB3: TProgressBar;
    Label3: TLabel;
    ZipSizeLab: TLabel;
    CloseBtn: TBitBtn;
    JvFilenameEdit1: TJvFilenameEdit;
    Label2: TLabel;
    procedure StartRestoreBtnClick(Sender: TObject);
    procedure ZipProgress(Sender: TObject; ProgrType: ProgressType;
      Filename: String; FileSize: Integer);
    procedure ZipMessage(Sender: TObject; ErrCode: Integer;
      Message: String);
    procedure FormShow(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure CloseBtnClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure JvFilenameEdit1AfterDialog(Sender: TObject; var Name: String;
      var Action: Boolean);
  private
    { Private-Deklarationen }
    ZipError : Boolean;

    ZIPTotalSize1,
    ZIPTotalProgress1,
    ZIPTotalSize2,
    ZIPTotalProgress2 : Int64;

    Busy              : Boolean;
  public
    { Public-Deklarationen }
  end;

implementation

uses CAO_DM, DB, ZExtra, FileCtrl, CAO_Logging;

{$R *.DFM}

//------------------------------------------------------------------------------
procedure TRestoreForm.StartRestoreBtnClick(Sender: TObject);
var RPfad   : string;
    RP, DN  : String;
    DSTab   : Array of Boolean;
    I,SQLCount,Error : Integer;
    SQL,
    SQL2, S : String;
    ZDE     : ZipDirEntry;
    NoSqlFile : Boolean;
    TF        : TextFile;

begin
     rpfad :=dm1.maindir+'Restore\';

     rp :=DM1.Readstring ('MAIN\SICHERUNG','RESTORE_PFAD','@@@');
     if rp = '@@@' then DM1.Writestring ('MAIN\SICHERUNG','RESTORE_PFAD','')
                   else if rp <> '' then rpfad :=rp;

     ForceDirectories (RPfad);

     ZipError :=False;

     zip.ZipFilename :=JvFilenameEdit1.FileName;

     // Pr�fen ob im Zip eine CAO-Datensicherung ist

     NoSqlFile :=False;

     if (Zip.Count=1) then
     begin
        ZDE :=ZIP.DirEntry [0];
        DN  :=uppercase(ZDE.FileName);

        if (Pos('CAO_BACKUP_',DN)<>1) or
           (ExtractFileExt(DN)<>'.SQL') then NoSqlFile :=True;

     end else NoSqlFile :=True;

     if NoSqlFile then
     begin
        MessageDlg ('Die angegebene Datei enth�lt keine CAO-Datensicherung.',
                    mterror,[mbok],0);
        exit;
     end;

     // Pfad hinzuf�gen
     DN :=RPfad+DN;

     if fileexists (DN) then
     begin
        if MessageDlg ('Die Datei "'+DN+'" existiert bereits, '+#13#10+
                       'soll sie �berschrieben werden ?',mtconfirmation,
                       [mbyes, mbcancel],0)<>mryes then exit;

        if not DeleteFile (DN) then exit; // Abbruch, wenn Datei nicht gel�scht werden kann
     end;


     Zip.ExtrBaseDir :=RPfad;
     zip.ExtrOptions :=[ExtrOverWrite];
     zip.FSpecArgs.Clear;
     zip.FSpecArgs.Add ('CAO_backup_*.sql');

     if rpfad[length(rpfad)]<>'\' then rpfad :=rpfad+'\';

     try
        Zip.Extract;
     except
        ZipError :=True;

     end;

     if ZipError then
     begin
        MessageDlg ('Beim Entpacken der Datensicherung ist ein Fehler aufgetreten.'+#13#10+
                    'Aus Sicherheitsgr�nden wurde die R�cksicherung abgebrochen !',mterror,[mbok],0);
        Exit;
     end;


     AssignFile (TF, DN);
     try
        Reset (TF);

        SQLCount   :=0;
        SQL :='';
        try
           while not Eof(TF) do
           begin
              Readln (TF,S);

              SQL :=SQL+S;
              if (length(SQL)>0)and(S[length(S)]=';') then
              begin
                 // SQL ausf�hren

                 inc(SQLCount); Label2.Caption :=IntToStr(SQLCount);
                 SQL :='';

                 Application.ProcessMessages;
              end;
           end;

           if length(SQL)>0 then
           begin
             // le. SQL ausf�hren
             inc(SQLCount);  Label2.Caption :=IntToStr(SQLCount);
           end;
        finally
           CloseFile (TF);
        end;
     except
        //Fehler beim �ffnen
     end;


     //exit;

     // Datenbank schlie�en

     // offene Datasets merken
     setlength(DSTab,DM1.DB1.DatasetCount);
     for i:=0 to DM1.DB1.DatasetCount-1 do
     begin
          DSTab[i] :=tDataset(DM1.DB1.Datasets[i]).Active;
          tDataset(DM1.DB1.Datasets[i]).Close;
     end;

     // Tabellen umbenennen
     DM1.UniQuery.Close;
     DM1.UniQuery.Sql.Clear;
     DM1.UniQuery.Sql.Add ('FLUSH TABLES');
     DM1.UniQuery.ExecSql;

     //evt. alte Backups l�schen
     DM1.UniQuery.Close;
     DM1.UniQuery.Sql.Text :='show Tables from '+DM1.DB1.Database;
     DM1.UniQuery.Open;

     while not DM1.UniQuery.Eof do
     begin
        S :=DM1.UniQuery.FieldByName ('Tables_In_'+DM1.DB1.Database).AsString;
        if Pos('BACK_',UpperCase(S))=1 then
        begin
           if length(SQL)>0 then SQL :=SQL+',';
           SQL :=SQL+S;
        end;
        DM1.UniQuery.Next;
     end;

     // alte Backups l�schen
     if length(SQL)>0 then
     begin
       SQL :='DROP TABLE IF EXISTS '+SQL;
       DM1.UniQuery.Close;
       DM1.UniQuery.Sql.Text :=SQL;
       DM1.UniQuery.ExecSql;
     end;

     // alle Tabellen in BACK_.... unbenennen
     DM1.UniQuery.Close;
     DM1.UniQuery.Sql.Text :='show Tables from '+DM1.DB1.Database;
     DM1.UniQuery.Open;

     Sql :=''; SQL2 :='';

     while not DM1.UniQuery.Eof do
     begin
        S :=DM1.UniQuery.FieldByName ('Tables_In_'+DM1.DB1.Database).AsString;
        //if (Pos('BACK_',Uppercase(S))=0)and(fileexists (rpfad+S+'.MYD')) then
        //begin
           if length(SQL)>0 then SQL :=SQL+',';
           SQL :=SQL+S+' TO BACK_'+S;

           if length(SQL2)>0 then SQL2 :=SQL2+',';
           SQL2 :=SQL2+S;
        //end;
        DM1.UniQuery.Next;
     end;

     if length(SQL)>0 then
     begin
        SQL :='RENAME TABLE '+SQL;
        DM1.UniQuery.Close;
        DM1.UniQuery.Sql.Text :=SQL;
        DM1.UniQuery.ExecSql;
     end;


     pb1.Max      :=SQLCount;
     pb1.Position :=0;
     Error        :=0;

     AssignFile (TF, DN);
     try
        Reset (TF); I:=0;
        SQL :='';
        try
           while not Eof(TF) do
           begin
              Readln (TF,S);
              SQL :=SQL+S;
              if (length(SQL)>0)and(S[length(S)]=';') then
              begin
                 inc(I); PB1.Position :=I;

                 // SQL ausf�hren
                 DM1.UniQuery.Close;
                 DM1.UniQuery.Sql.Text :=SQL;
                 try
                    DM1.UniQuery.ExecSql;
                 except
                    inc(Error);
                 end;


                 SQL :='';
                 Application.ProcessMessages;
              end;
           end;

           if length(SQL)>0 then
           begin
             // le. SQL ausf�hren
             DM1.UniQuery.Close;
             DM1.UniQuery.Sql.Text :=SQL;
             try
                DM1.UniQuery.ExecSql;
             except
                inc(Error);
             end;
             inc(I);  PB1.Position :=I;
           end;
        finally
           CloseFile (TF);
        end;
     except
        //Fehler beim �ffnen
     end;








     DM1.OpenMandant (DM1.AktMandant,Application.Name,True);

     // offene Datasets erneut �ffnen
     for i:=0 to DM1.DB1.DatasetCount-1 do
     begin
          if DSTab[i] then tDataset(DM1.DB1.Datasets[i]).Open;
     end;
     MessageDlg ('R�cksicherung abgeschlossen !!!',mtinformation,[mbok],0);
end;
//------------------------------------------------------------------------------
procedure TRestoreForm.ZipMessage(Sender: TObject; ErrCode: Integer;
  Message: String);

var p : integer;
begin
    try
       P :=Pos('ADDING:',Uppercase(Message));
       if P>0 then delete (Message,P,8); Insert('zu ZIP hinzuf�gen:',Message,P);

       if assigned(LogForm)
         then logform.addlog ('ZIP:ERR-CODE:'+Inttostr(ErrCode)+#13#10+
                              'MLD:'+Message+#13#10);

       if ErrCode <> 0 then ZipError :=True;
    except
    end;
end;
//------------------------------------------------------------------------------
procedure TRestoreForm.FormShow(Sender: TObject);
var D : String;
begin
     Zip.DLLDirectory        :=ExtractFilePath(ParamStr(0));
     Busy                    :=False;
     StartRestoreBtn.Enabled :=False;
     ZipSizeLab.Caption      :='';
     PB3.Position            :=0;
     //ZipFileName.Caption     :='';

     d :=dm1.BackupDir+dm1.AktMandant+'\';
     if not DirectoryExists (D) then D :=ExtractFilePath(ParamStr(0));

     JvFilenameEdit1.FileName   :=D;

end;
//------------------------------------------------------------------------------
procedure TRestoreForm.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
     CanClose := not Busy;
end;
//------------------------------------------------------------------------------
procedure TRestoreForm.ZipProgress(Sender: TObject; ProgrType: ProgressType;
  Filename: String; FileSize: Integer);
var
   Step: Integer;
begin
   case ProgrType of
      TotalSize2Process:
         begin
            ZIPTotalSize2     := FileSize;
            ZIPTotalProgress2 := 0;
         end;
      NewFile:
         begin
            PB3.Position      := 1;  // Current position of bar.
            ZIPTotalSize1     := FileSize;
            ZIPTotalProgress1 := 0;
         end;
      ProgressUpdate:
         begin
            // FileSize gives now the bytes processed since the last call.
            ZIPTotalProgress1 := ZIPTotalProgress1 + FileSize;
            ZIPTotalProgress2 := ZIPTotalProgress2 + FileSize;
            if ZIPTotalSize1 <> 0 then
            begin
               Step := Integer( Int64(ZIPTotalProgress1) *
                                Int64(10000) div Int64(ZipTotalSize1)
                              );

               PB3.Position := 1 + Step;
            end else
               PB3.Position := 10001;
         end;
   end; // EOF Case
end;
//------------------------------------------------------------------------------
procedure TRestoreForm.CloseBtnClick(Sender: TObject);
begin
     Close;
end;
//------------------------------------------------------------------------------
procedure TRestoreForm.FormCreate(Sender: TObject);
begin
     if Screen.PixelsPerInch <> 96 then
     begin
       Self.ScaleBy (96, Screen.PixelsPerInch);
       Refresh;
     end;
end;
//------------------------------------------------------------------------------
procedure TRestoreForm.JvFilenameEdit1AfterDialog(Sender: TObject;
  var Name: String; var Action: Boolean);
begin
     StartRestoreBtn.Enabled :=FileExists (Name);
end;

end.


{

// Restore


procedure TDM1.RestoreMandant (ZipFileName : String);
var RPfad   : string;
    RP      : String;
    DSTab   : Array of Boolean;
    I       : Integer;
    SQL,
    SQL2, S : String;
begin
     rpfad :=dm1.maindir+'Restore\';

     rp :=readstring ('MAIN\SICHERUNG','RESTORE_PFAD','@@@');
     if rp = '@@@' then writestring ('MAIN\SICHERUNG','RESTORE_PFAD','')
                   else if rp <> '' then rpfad :=rp;



     //rpfad :='C:\BACKUP\';
     ForceDirectories (RPfad);

     ZipError :=False;

     zip.ZipFilename :=zipfilename;
     Zip.ExtrBaseDir :=RPfad;
     zip.ExtrOptions :=[ExtrOverWrite];

     if rpfad[length(rpfad)]<>'\' then rpfad :=rpfad+'\';

     try
        Zip.Extract;
     except
        ZipError :=True;

     end;

     if ZipError then
     begin
        MessageDlg ('Beim Entpacken der Datensicherung ist ein Fehler aufgetreten.'+#13#10+
                    'Aus Sicherheitsgr�nden wurde die R�cksicherung abgebrochen !',mterror,[mbok],0);
        Exit;
     end;

     //exit;

     // Datenbank schlie�en

     // offene Datasets merken
     setlength(DSTab,DB1.DatasetCount);
     for i:=0 to DB1.DatasetCount-1 do
     begin
          DSTab[i] :=tDataset(DB1.Datasets[i]).Active;
          tDataset(DB1.Datasets[i]).Close;
     end;

     // Tabellen umbenennen
     UniQuery.Close;
     UniQuery.Sql.Clear;
     UniQuery.Sql.Add ('FLUSH TABLES');
     UniQuery.ExecSql;

     //evt. alte Backups l�schen
     UniQuery.Close;
     UniQuery.Sql.Text :='show Tables from '+DB1.Database;
     UniQuery.Open;

     while not UniQuery.Eof do
     begin
        S :=UniQuery.FieldByName ('Tables_In_'+DB1.Database).AsString;
        if Pos('BACK_',UpperCase(S))=1 then
        begin
           if length(SQL)>0 then SQL :=SQL+',';
           SQL :=SQL+S;
        end;
        UniQuery.Next;
     end;

     // alte Backups l�schen
     if length(SQL)>0 then
     begin
       SQL :='DROP TABLE IF EXISTS '+SQL;
       UniQuery.Close;
       UniQuery.Sql.Text :=SQL;
       UniQuery.ExecSql;
     end;

     // Tabellen, die im Backup existieren, in BACK_.... unbenennen
     UniQuery.Close;
     UniQuery.Sql.Text :='show Tables from '+DB1.Database;
     UniQuery.Open;

     Sql :=''; SQL2 :='';

     while not UniQuery.Eof do
     begin
        S :=UniQuery.FieldByName ('Tables_In_'+DB1.Database).AsString;
        if (Pos('BACK_',Uppercase(S))=0)and(fileexists (rpfad+S+'.MYD')) then
        begin
           if length(SQL)>0 then SQL :=SQL+',';
           SQL :=SQL+S+' TO BACK_'+S;

           if length(SQL2)>0 then SQL2 :=SQL2+',';
           SQL2 :=SQL2+S;
        end;
        UniQuery.Next;
     end;

     if length(SQL)>0 then
     begin
        SQL :='RENAME TABLE '+SQL;
        UniQuery.Close;
        UniQuery.Sql.Text :=SQL;
        UniQuery.ExecSql;
     end;


     // MYSQL-Restore ausf�hren
     UniQuery.Close;
     UniQuery.Sql.Text :='RESTORE TABLE '+SQL2;

     while pos('\',rpfad)>0 do rpfad[pos('\',rpfad)] :='/';
     UniQuery.Sql.Add ('FROM "'+rpfad+'"');

     UniQuery.ExecSql;


     OpenMandant (AktMandant,Application.Name,True);

     // offene Datasets erneut �ffnen
     for i:=0 to DB1.DatasetCount-1 do
     begin
          if DSTab[i] then tDataset(DB1.Datasets[i]).Open;
     end;
     MessageDlg ('R�cksicherung abgeschlossen !!!',mtinformation,[mbok],0);
end;


}
