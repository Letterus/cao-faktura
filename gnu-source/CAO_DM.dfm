object DM1: TDM1
  OldCreateOrder = False
  OnCreate = DM1Create
  OnDestroy = DataModuleDestroy
  Left = 269
  Top = 105
  Height = 696
  Width = 814
  object LandDS: TDataSource
    DataSet = LandTab
    Left = 120
    Top = 16
  end
  object WhrungDS: TDataSource
    DataSet = WhrungTab
    Left = 120
    Top = 64
  end
  object WgrDS: TDataSource
    DataSet = WgrTab
    Left = 120
    Top = 120
  end
  object LiefArtDS: TDataSource
    DataSet = LiefArtTab
    Left = 120
    Top = 176
  end
  object ZahlArtDS: TDataSource
    DataSet = ZahlartTab
    Left = 120
    Top = 232
  end
  object VertreterDS: TDataSource
    DataSet = VertreterTab
    Left = 120
    Top = 288
  end
  object db1: TZMySqlDatabase
    Host = '192.168.2.100'
    Port = '3306'
    Database = 'sbp_faktura'
    Encoding = etNone
    Login = 'jan'
    Password = 'jan'
    LoginPrompt = False
    Connected = True
    Left = 26
    Top = 16
  end
  object Transact1: TZMySqlTransact
    Options = [toHourGlass]
    AutoCommit = True
    OnAfterBatchExec = Transact1AfterBatchExec
    Database = db1
    TransactSafe = False
    Left = 26
    Top = 64
  end
  object JourTab: TZMySqlQuery
    Database = db1
    Transaction = Transact1
    CachedUpdates = False
    ShowRecordTypes = [ztModified, ztInserted, ztUnmodified]
    Options = [doHourGlass, doAutoFillDefs]
    LinkOptions = [loCascadeUpdate, loAlwaysResync]
    Constraints = <>
    OnCalcFields = JourTabCalcFields
    ExtraOptions = []
    AlternativeView = True
    Macros = <>
    Sql.Strings = (
      'select * from JOURNAL '
      'where REC_ID =:id')
    RequestLive = True
    Left = 253
    Top = 120
    ParamData = <
      item
        DataType = ftInteger
        Name = 'id'
        ParamType = ptInput
        Value = 2591
      end>
    object JourTabREC_ID: TIntegerField
      FieldName = 'REC_ID'
    end
    object JourTabQUELLE: TIntegerField
      FieldName = 'QUELLE'
    end
    object JourTabVRENUM: TIntegerField
      FieldName = 'VRENUM'
    end
    object JourTabRDATUM: TDateField
      FieldName = 'RDATUM'
    end
    object JourTabKUN_NAME1: TStringField
      FieldName = 'KUN_NAME1'
      Size = 30
    end
    object JourTabADDR_ID: TIntegerField
      FieldName = 'ADDR_ID'
    end
    object JourTabLIEF_ADDR_ID: TIntegerField
      FieldName = 'LIEF_ADDR_ID'
    end
    object JourTabKFZ_ID: TIntegerField
      FieldName = 'KFZ_ID'
    end
    object JourTabKM_STAND: TIntegerField
      FieldName = 'KM_STAND'
    end
    object JourTabNSUMME: TFloatField
      FieldName = 'NSUMME'
    end
    object JourTabMSUMME: TFloatField
      FieldName = 'MSUMME'
    end
    object JourTabBSUMME: TFloatField
      FieldName = 'BSUMME'
    end
    object JourTabSTADIUM: TIntegerField
      FieldName = 'STADIUM'
    end
    object JourTabPROJEKT: TStringField
      FieldName = 'PROJEKT'
      Size = 80
    end
    object JourTabORGNUM: TStringField
      FieldName = 'ORGNUM'
    end
    object JourTabWAEHRUNG: TStringField
      FieldName = 'WAEHRUNG'
      Size = 5
    end
    object JourTabZAHLART: TIntegerField
      FieldName = 'ZAHLART'
    end
    object JourTabSOLL_STAGE: TIntegerField
      FieldName = 'SOLL_STAGE'
    end
    object JourTabSOLL_SKONTO: TFloatField
      FieldName = 'SOLL_SKONTO'
    end
    object JourTabSOLL_NTAGE: TIntegerField
      FieldName = 'SOLL_NTAGE'
    end
    object JourTabSOLL_RATEN: TIntegerField
      FieldName = 'SOLL_RATEN'
    end
    object JourTabSOLL_RATBETR: TFloatField
      FieldName = 'SOLL_RATBETR'
    end
    object JourTabSOLL_RATINTERVALL: TIntegerField
      FieldName = 'SOLL_RATINTERVALL'
    end
    object JourTabIST_ANZAHLUNG: TFloatField
      FieldName = 'IST_ANZAHLUNG'
    end
    object JourTabIST_SKONTO: TFloatField
      FieldName = 'IST_SKONTO'
      DisplayFormat = '0.0 %'
    end
    object JourTabIST_ZAHLDAT: TDateField
      FieldName = 'IST_ZAHLDAT'
      DisplayFormat = 'dd.mm.yyyy'
    end
    object JourTabIST_BETRAG: TFloatField
      FieldName = 'IST_BETRAG'
      DisplayFormat = ',###,##0.00'
    end
    object JourTabKONTOAUSZUG: TIntegerField
      FieldName = 'KONTOAUSZUG'
    end
    object JourTabBANK_ID: TIntegerField
      FieldName = 'BANK_ID'
    end
    object JourTabUW_NUM: TIntegerField
      FieldName = 'UW_NUM'
    end
    object JourTabGEGENKONTO: TIntegerField
      FieldName = 'GEGENKONTO'
    end
    object JourTabKUN_NUM: TStringField
      FieldName = 'KUN_NUM'
    end
    object JourTabKUN_ANREDE: TStringField
      FieldName = 'KUN_ANREDE'
      Size = 40
    end
    object JourTabKUN_NAME2: TStringField
      FieldName = 'KUN_NAME2'
      Size = 40
    end
    object JourTabKUN_NAME3: TStringField
      FieldName = 'KUN_NAME3'
      Size = 40
    end
    object JourTabKUN_ABTEILUNG: TStringField
      FieldName = 'KUN_ABTEILUNG'
      Size = 40
    end
    object JourTabKUN_STRASSE: TStringField
      FieldName = 'KUN_STRASSE'
      Size = 40
    end
    object JourTabKUN_LAND: TStringField
      FieldName = 'KUN_LAND'
      Size = 5
    end
    object JourTabKUN_PLZ: TStringField
      FieldName = 'KUN_PLZ'
      Size = 10
    end
    object JourTabKUN_ORT: TStringField
      FieldName = 'KUN_ORT'
      Size = 40
    end
    object JourTabIST_SKONTO_BETR: TFloatField
      FieldKind = fkCalculated
      FieldName = 'IST_SKONTO_BETR'
      DisplayFormat = ',###,##0.00'
      Calculated = True
    end
    object JourTabVLSNUM: TIntegerField
      FieldName = 'VLSNUM'
    end
    object JourTabLDATUM: TDateField
      FieldName = 'LDATUM'
    end
    object JourTabLIEFART: TIntegerField
      FieldName = 'LIEFART'
    end
    object JourTabKOST_NETTO: TFloatField
      FieldName = 'KOST_NETTO'
    end
    object JourTabWERT_NETTO: TFloatField
      FieldName = 'WERT_NETTO'
    end
    object JourTabQUELLE_SUB: TIntegerField
      FieldName = 'QUELLE_SUB'
    end
    object JourTabMAHNKOSTEN: TFloatField
      FieldName = 'MAHNKOSTEN'
    end
    object JourTabFREIGABE1_FLAG: TBooleanField
      FieldName = 'FREIGABE1_FLAG'
      Required = True
    end
    object JourTabBRUTTO_FLAG: TBooleanField
      FieldName = 'BRUTTO_FLAG'
      Required = True
    end
    object JourTabMWST_FREI_FLAG: TBooleanField
      FieldName = 'MWST_FREI_FLAG'
      Required = True
    end
    object JourTabMWST_0: TFloatField
      FieldName = 'MWST_0'
      Required = True
    end
    object JourTabMWST_1: TFloatField
      FieldName = 'MWST_1'
      Required = True
    end
    object JourTabMWST_2: TFloatField
      FieldName = 'MWST_2'
      Required = True
    end
    object JourTabMWST_3: TFloatField
      FieldName = 'MWST_3'
      Required = True
    end
    object JourTabMSUMME_0: TFloatField
      FieldName = 'MSUMME_0'
      Required = True
    end
    object JourTabMSUMME_1: TFloatField
      FieldName = 'MSUMME_1'
      Required = True
    end
    object JourTabMSUMME_2: TFloatField
      FieldName = 'MSUMME_2'
      Required = True
    end
    object JourTabMSUMME_3: TFloatField
      FieldName = 'MSUMME_3'
      Required = True
    end
    object JourTabSHOP_ID: TIntegerField
      FieldName = 'SHOP_ID'
    end
    object JourTabSHOP_ORDERID: TIntegerField
      FieldName = 'SHOP_ORDERID'
    end
  end
  object JPosTab: TZMySqlQuery
    Database = db1
    Transaction = Transact1
    CachedUpdates = False
    ShowRecordTypes = [ztModified, ztInserted, ztUnmodified]
    Options = [doHourGlass, doAutoFillDefs]
    LinkOptions = [loCascadeUpdate, loAlwaysResync]
    Constraints = <>
    ExtraOptions = [moStoreResult]
    AlternativeView = True
    Macros = <>
    Sql.Strings = (
      'select * from JOURNALPOS'
      'where JOURNAL_ID =:ID')
    RequestLive = True
    Left = 325
    Top = 120
    ParamData = <
      item
        DataType = ftInteger
        Name = 'ID'
        ParamType = ptInput
      end>
    object JPosTabREC_ID: TIntegerField
      FieldName = 'REC_ID'
    end
    object JPosTabQUELLE: TIntegerField
      FieldName = 'QUELLE'
    end
    object JPosTabQUELLE_SUB: TIntegerField
      FieldName = 'QUELLE_SUB'
    end
    object JPosTabQUELLE_SRC: TIntegerField
      FieldName = 'QUELLE_SRC'
    end
    object JPosTabJOURNAL_ID: TIntegerField
      FieldName = 'JOURNAL_ID'
      Required = True
    end
    object JPosTabARTIKELTYP: TStringField
      FieldName = 'ARTIKELTYP'
      Size = 1
    end
    object JPosTabARTIKEL_ID: TIntegerField
      FieldName = 'ARTIKEL_ID'
      Required = True
    end
    object JPosTabADDR_ID: TIntegerField
      FieldName = 'ADDR_ID'
      Required = True
    end
    object JPosTabATRNUM: TIntegerField
      FieldName = 'ATRNUM'
    end
    object JPosTabVRENUM: TIntegerField
      FieldName = 'VRENUM'
    end
    object JPosTabVLSNUM: TIntegerField
      FieldName = 'VLSNUM'
    end
    object JPosTabPOSITION: TIntegerField
      FieldName = 'POSITION'
      Required = True
    end
    object JPosTabMATCHCODE: TStringField
      FieldName = 'MATCHCODE'
    end
    object JPosTabARTNUM: TStringField
      FieldName = 'ARTNUM'
    end
    object JPosTabBARCODE: TStringField
      FieldName = 'BARCODE'
    end
    object JPosTabMENGE: TFloatField
      FieldName = 'MENGE'
    end
    object JPosTabLAENGE: TStringField
      FieldName = 'LAENGE'
    end
    object JPosTabGROESSE: TStringField
      FieldName = 'GROESSE'
    end
    object JPosTabDIMENSION: TStringField
      FieldName = 'DIMENSION'
    end
    object JPosTabGEWICHT: TFloatField
      FieldName = 'GEWICHT'
    end
    object JPosTabME_EINHEIT: TStringField
      FieldName = 'ME_EINHEIT'
      Size = 10
    end
    object JPosTabPR_EINHEIT: TFloatField
      FieldName = 'PR_EINHEIT'
    end
    object JPosTabEPREIS: TFloatField
      FieldName = 'EPREIS'
    end
    object JPosTabE_RGEWINN: TFloatField
      FieldName = 'E_RGEWINN'
    end
    object JPosTabRABATT: TFloatField
      FieldName = 'RABATT'
    end
    object JPosTabRABATT2: TFloatField
      FieldName = 'RABATT2'
      Required = True
    end
    object JPosTabRABATT3: TFloatField
      FieldName = 'RABATT3'
      Required = True
    end
    object JPosTabSTEUER_CODE: TIntegerField
      FieldName = 'STEUER_CODE'
    end
    object JPosTabALTTEIL_PROZ: TFloatField
      FieldName = 'ALTTEIL_PROZ'
    end
    object JPosTabALTTEIL_STCODE: TIntegerField
      FieldName = 'ALTTEIL_STCODE'
    end
    object JPosTabGEGENKTO: TIntegerField
      FieldName = 'GEGENKTO'
    end
    object JPosTabBEZEICHNUNG: TMemoField
      FieldName = 'BEZEICHNUNG'
      BlobType = ftMemo
    end
    object JPosTabVIEW_POS: TStringField
      FieldName = 'VIEW_POS'
      Size = 3
    end
    object JPosTabGEBUCHT: TBooleanField
      FieldName = 'GEBUCHT'
      Required = True
    end
    object JPosTabSN_FLAG: TBooleanField
      FieldName = 'SN_FLAG'
      Required = True
    end
    object JPosTabALTTEIL_FLAG: TBooleanField
      FieldName = 'ALTTEIL_FLAG'
      Required = True
    end
    object JPosTabBEZ_FEST_FLAG: TBooleanField
      FieldName = 'BEZ_FEST_FLAG'
      Required = True
    end
    object JPosTabBRUTTO_FLAG: TBooleanField
      FieldName = 'BRUTTO_FLAG'
      Required = True
    end
  end
  object ZahlartTab: TZMySqlQuery
    Database = db1
    Transaction = Transact1
    CachedUpdates = False
    ShowRecordTypes = [ztModified, ztInserted, ztUnmodified]
    Options = [doHourGlass, doQueryAllRecords, doAutoFillDefs]
    LinkOptions = [loAlwaysResync]
    Constraints = <>
    BeforeDelete = NummerTabNewRecord
    OnNewRecord = NummerTabNewRecord
    ExtraOptions = [moStoreResult]
    Macros = <>
    Sql.Strings = (
      'select TRUNCATE(VAL_CHAR,0) as ZAHL_ID, '
      'NAME as LANGBEZ, '
      'VAL_DOUBLE as SKONTO_PROZ, '
      'VAL_INT2 as NETTO_TAGE, '
      'VAL_INT as SKONTO_TAGE,'
      'VAL_BLOB as TEXT'
      'from REGISTERY '
      'where MAINKEY='#39'MAIN\\ZAHLART'#39
      'order by ZAHL_ID'
      ''
      '')
    RequestLive = True
    Left = 184
    Top = 232
    object ZahlartTabZAHL_ID: TFloatField
      FieldName = 'ZAHL_ID'
    end
    object ZahlartTabSKONTO_PROZ: TFloatField
      FieldName = 'SKONTO_PROZ'
      DisplayFormat = ',#0.00'
      EditFormat = '0.00'
    end
    object ZahlartTabSKONTO_TAGE: TIntegerField
      FieldName = 'SKONTO_TAGE'
      EditFormat = '0'
    end
    object ZahlartTabNETTO_TAGE: TLargeintField
      FieldName = 'NETTO_TAGE'
      EditFormat = '0'
    end
    object ZahlartTabLANGBEZ: TStringField
      DisplayWidth = 100
      FieldName = 'LANGBEZ'
      Size = 255
    end
    object ZahlartTabTEXT: TMemoField
      FieldName = 'TEXT'
      BlobType = ftMemo
    end
  end
  object VertreterTab: TZMySqlQuery
    Database = db1
    Transaction = Transact1
    CachedUpdates = False
    ShowRecordTypes = [ztModified, ztInserted, ztUnmodified]
    Options = [doHourGlass, doQueryAllRecords, doAutoFillDefs]
    LinkOptions = [loCascadeUpdate, loAlwaysResync]
    Constraints = <>
    ExtraOptions = [moStoreResult]
    Macros = <>
    Sql.Strings = (
      'select * from VERTRETER')
    RequestLive = False
    Left = 184
    Top = 288
  end
  object LandTab: TZMySqlQuery
    Database = db1
    Transaction = Transact1
    CachedUpdates = False
    ShowRecordTypes = [ztModified, ztInserted, ztUnmodified]
    Options = [doHourGlass, doQueryAllRecords, doAutoFillDefs]
    LinkOptions = [loCascadeUpdate, loAlwaysResync]
    Constraints = <>
    OnCalcFields = LandTabCalcFields
    ExtraOptions = [moStoreResult]
    Macros = <>
    Sql.Strings = (
      'select * from LAND')
    RequestLive = False
    Left = 184
    Top = 16
    object LandTabID: TStringField
      FieldName = 'ID'
      Size = 2
    end
    object LandTabNAME: TStringField
      FieldName = 'NAME'
      Size = 100
    end
    object LandTabISO_CODE_3: TStringField
      FieldName = 'ISO_CODE_3'
      Size = 3
    end
    object LandTabFORMAT: TIntegerField
      FieldName = 'FORMAT'
    end
    object LandTabVORWAHL: TStringField
      FieldName = 'VORWAHL'
      Size = 10
    end
    object LandTabWAEHRUNG: TStringField
      FieldName = 'WAEHRUNG'
      Size = 5
    end
    object LandTabSPRACHE: TStringField
      FieldName = 'SPRACHE'
      Size = 3
    end
    object LandTabPOST_CODE: TStringField
      FieldKind = fkCalculated
      FieldName = 'POST_CODE'
      Calculated = True
    end
  end
  object WhrungTab: TZMySqlQuery
    Tag = 999999999
    Database = db1
    Transaction = Transact1
    CachedUpdates = False
    ShowRecordTypes = [ztModified, ztInserted, ztUnmodified]
    Options = [doHourGlass, doQueryAllRecords, doAutoFillDefs]
    LinkOptions = [loCascadeUpdate, loAlwaysResync]
    Constraints = <>
    ExtraOptions = [moStoreResult]
    Macros = <>
    Sql.Strings = (
      
        'select NAME as WAEHRUNG, VAL_CHAR as LANGBEZ, VAL_DOUBLE as FAKT' +
        'OR'
      'from REGISTERY'
      'where MAINKEY='#39'MAIN\\WAEHRUNG'#39
      'order by WAEHRUNG')
    RequestLive = False
    Left = 184
    Top = 64
    object WhrungTabWAEHRUNG: TStringField
      FieldName = 'WAEHRUNG'
      Size = 5
    end
    object WhrungTabLANGBEZ: TStringField
      FieldName = 'LANGBEZ'
      Size = 30
    end
    object WhrungTabFAKTOR: TFloatField
      FieldName = 'FAKTOR'
    end
  end
  object WgrTab: TZMySqlQuery
    Database = db1
    Transaction = Transact1
    CachedUpdates = False
    ShowRecordTypes = [ztModified, ztInserted, ztUnmodified]
    Options = [doHourGlass, doQueryAllRecords, doAutoFillDefs]
    LinkOptions = [loCascadeUpdate, loAlwaysResync]
    Constraints = <>
    BeforePost = WgrTabBeforePost
    ExtraOptions = [moStoreResult]
    Macros = <>
    Sql.Strings = (
      'select * from WARENGRUPPEN order by ID')
    RequestLive = True
    Left = 184
    Top = 120
    object WgrTabID: TIntegerField
      FieldName = 'ID'
    end
    object WgrTabTOP_ID: TIntegerField
      FieldName = 'TOP_ID'
      DisplayFormat = '0;-;-'
    end
    object WgrTabNAME: TStringField
      FieldName = 'NAME'
      Size = 250
    end
    object WgrTabBESCHREIBUNG: TMemoField
      FieldName = 'BESCHREIBUNG'
      BlobType = ftMemo
    end
    object WgrTabDEF_EKTO: TIntegerField
      DisplayWidth = 6
      FieldName = 'DEF_EKTO'
      DisplayFormat = '00000;-;-'
      EditFormat = '0'
    end
    object WgrTabDEF_AKTO: TIntegerField
      DisplayWidth = 6
      FieldName = 'DEF_AKTO'
      DisplayFormat = '00000;-;-'
      EditFormat = '0'
    end
    object WgrTabVORGABEN: TMemoField
      FieldName = 'VORGABEN'
      BlobType = ftMemo
    end
    object WgrTabSTEUER_CODE: TIntegerField
      FieldName = 'STEUER_CODE'
      MaxValue = 3
    end
    object WgrTabVK1_FAKTOR: TFloatField
      FieldName = 'VK1_FAKTOR'
      DisplayFormat = ',#0.00000'
      EditFormat = '0.00000'
      MaxValue = 99
    end
    object WgrTabVK2_FAKTOR: TFloatField
      FieldName = 'VK2_FAKTOR'
      DisplayFormat = ',#0.00000'
      EditFormat = '0.00000'
      MaxValue = 99
    end
    object WgrTabVK3_FAKTOR: TFloatField
      FieldName = 'VK3_FAKTOR'
      DisplayFormat = ',#0.00000'
      EditFormat = '0.00000'
      MaxValue = 99
    end
    object WgrTabVK4_FAKTOR: TFloatField
      FieldName = 'VK4_FAKTOR'
      DisplayFormat = ',#0.00000'
      EditFormat = '0.00000'
      MaxValue = 99
    end
    object WgrTabVK5_FAKTOR: TFloatField
      FieldName = 'VK5_FAKTOR'
      DisplayFormat = ',#0.00000'
      EditFormat = '0.00000'
      MaxValue = 99
    end
  end
  object NummerTab: TZMySqlQuery
    Database = db1
    Transaction = Transact1
    CachedUpdates = False
    ShowRecordTypes = [ztModified, ztInserted, ztUnmodified]
    Options = [doHourGlass, doQueryAllRecords, doAutoFillDefs]
    LinkOptions = [loCascadeUpdate, loAlwaysResync]
    Constraints = <>
    BeforeDelete = NummerTabNewRecord
    OnNewRecord = NummerTabNewRecord
    ExtraOptions = [moStoreResult]
    Macros = <>
    Sql.Strings = (
      'select '
      'VAL_INT as QUELLE, '
      'VAL_CHAR as FORMAT, '
      'VAL_INT2 as NEXT_NUM, '
      'VAL_INT3 as MAXLEN, '
      'MAINKEY, NAME'
      'from REGISTERY '
      'where MAINKEY='#39'MAIN\\NUMBERS'#39' '
      'order by QUELLE')
    RequestLive = True
    Left = 24
    Top = 344
    object NummerTabQUELLE: TIntegerField
      FieldName = 'QUELLE'
      Required = True
    end
    object NummerTabFORMAT: TStringField
      FieldName = 'FORMAT'
    end
    object NummerTabMAINKEY: TStringField
      FieldName = 'MAINKEY'
      Required = True
      Size = 255
    end
    object NummerTabNAME: TStringField
      FieldName = 'NAME'
      Required = True
      Size = 100
    end
    object NummerTabNEXT_NUM: TLargeintField
      FieldName = 'NEXT_NUM'
      MaxValue = 999999999
      MinValue = 1
    end
    object NummerTabMAXLEN: TLargeintField
      DisplayLabel = 'max. L�nge'
      FieldName = 'MAXLEN'
      DisplayFormat = '0 "Stellen"'
      EditFormat = '0'
      MaxValue = 9
      MinValue = 2
    end
  end
  object KGRTab: TZMySqlQuery
    Database = db1
    Transaction = Transact1
    CachedUpdates = False
    ShowRecordTypes = [ztModified, ztInserted, ztUnmodified]
    Options = [doHourGlass, doQueryAllRecords, doAutoFillDefs]
    LinkOptions = [loCascadeUpdate, loAlwaysResync]
    Constraints = <>
    BeforePost = KGRTabBeforePost
    ExtraOptions = [moStoreResult]
    Macros = <>
    Sql.Strings = (
      
        'select VAL_INT as GR, NAME as LANGBEZ, VAL_BLOB as SQL_STATEMENT' +
        ', MAINKEY'
      'from REGISTERY'
      'where MAINKEY='#39'MAIN\\ADDR_HIR'#39
      'order by GR')
    RequestLive = True
    Left = 184
    Top = 344
    object KGRTabGR: TIntegerField
      FieldName = 'GR'
    end
    object KGRTabLANGBEZ: TStringField
      FieldName = 'LANGBEZ'
      Size = 100
    end
    object KGRTabSQL_STATEMENT: TMemoField
      FieldName = 'SQL_STATEMENT'
      BlobType = ftMemo
    end
    object KGRTabMAINKEY: TStringField
      FieldName = 'MAINKEY'
      Required = True
      Size = 255
    end
  end
  object KgrDS: TDataSource
    DataSet = KGRTab
    Left = 120
    Top = 344
  end
  object LiefArtTab: TZMySqlQuery
    Database = db1
    Transaction = Transact1
    CachedUpdates = False
    ShowRecordTypes = [ztModified, ztInserted, ztUnmodified]
    Options = [doHourGlass, doAutoFillDefs, doUseRowId]
    LinkOptions = [loAlwaysResync]
    Constraints = <>
    BeforeDelete = NummerTabNewRecord
    OnNewRecord = NummerTabNewRecord
    ExtraOptions = [moStoreResult]
    Macros = <>
    Sql.Strings = (
      'select VAL_INT as LIEF_ID, NAME as LANGBEZ, VAL_BLOB as TEXT'
      'from REGISTERY '
      'where MAINKEY='#39'MAIN\\LIEFART'#39
      'order by LIEF_ID')
    RequestLive = True
    Left = 184
    Top = 176
  end
  object RegTab: TZMySqlQuery
    Database = db1
    Transaction = Transact1
    CachedUpdates = False
    ShowRecordTypes = [ztModified, ztInserted, ztUnmodified]
    Options = [doHourGlass, doAutoFillDefs, doQuickOpen, doUseRowId]
    LinkOptions = []
    Constraints = <>
    ExtraOptions = [moStoreResult]
    Macros = <>
    Sql.Strings = (
      'select * from REGISTERY'
      'where MAINKEY=:KEY and NAME=:NAME')
    RequestLive = True
    Left = 24
    Top = 288
    ParamData = <
      item
        DataType = ftString
        Name = 'KEY'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'NAME'
        ParamType = ptInput
      end>
    object RegTabMAINKEY: TStringField
      FieldName = 'MAINKEY'
      Size = 255
    end
    object RegTabNAME: TStringField
      FieldName = 'NAME'
      Size = 100
    end
    object RegTabVAL_CHAR: TStringField
      FieldName = 'VAL_CHAR'
      Size = 255
    end
    object RegTabVAL_DATE: TDateTimeField
      FieldName = 'VAL_DATE'
    end
    object RegTabVAL_INT: TIntegerField
      FieldName = 'VAL_INT'
    end
    object RegTabVAL_INT2: TLargeintField
      FieldName = 'VAL_INT2'
    end
    object RegTabVAL_INT3: TLargeintField
      FieldName = 'VAL_INT3'
    end
    object RegTabVAL_DOUBLE: TFloatField
      FieldName = 'VAL_DOUBLE'
    end
    object RegTabVAL_BLOB: TBlobField
      FieldName = 'VAL_BLOB'
      BlobType = ftBlob
    end
    object RegTabVAL_BIN: TBlobField
      FieldName = 'VAL_BIN'
      BlobType = ftBlob
    end
    object RegTabVAL_TYP: TIntegerField
      FieldName = 'VAL_TYP'
    end
  end
  object ArtMengeTab: TZMySqlQuery
    Database = db1
    Transaction = Transact1
    CachedUpdates = False
    ShowRecordTypes = [ztModified, ztInserted, ztUnmodified]
    Options = [doHourGlass, doAutoFillDefs, doUseRowId]
    LinkOptions = [loAlwaysResync]
    Constraints = <>
    ExtraOptions = [moStoreResult]
    Macros = <>
    Sql.Strings = (
      'update ARTIKEL '
      'set '
      'MENGE_AKT=MENGE_AKT - :SUBMENGE, '
      'MENGE_BESTELLT=MENGE_BESTELLT - :BMENGE,'
      'SHOP_CHANGE_FLAG=1'
      'where REC_ID = :ID')
    RequestLive = True
    Left = 533
    Top = 136
    ParamData = <
      item
        DataType = ftFloat
        Name = 'submenge'
        ParamType = ptInput
      end
      item
        DataType = ftFloat
        Name = 'BMENGE'
        ParamType = ptInput
      end
      item
        DataType = ftUnknown
        Name = 'ID'
        ParamType = ptUnknown
      end>
  end
  object CpySrcKopfTab: TZMySqlQuery
    Database = db1
    Transaction = Transact1
    CachedUpdates = False
    ShowRecordTypes = [ztModified, ztInserted, ztUnmodified]
    Options = [doHourGlass, doAutoFillDefs, doUseRowId]
    LinkOptions = [loAlwaysResync]
    Constraints = <>
    ExtraOptions = [moStoreResult]
    AlternativeView = True
    Macros = <>
    Sql.Strings = (
      'select * from JOURNAL'
      'where REC_ID=:ID')
    RequestLive = False
    Left = 456
    Top = 16
    ParamData = <
      item
        DataType = ftInteger
        Name = 'ID'
        ParamType = ptInput
      end>
    object CpySrcKopfTabQUELLE: TIntegerField
      FieldName = 'QUELLE'
      Required = True
    end
    object CpySrcKopfTabREC_ID: TIntegerField
      FieldName = 'REC_ID'
    end
    object CpySrcKopfTabQUELLE_SUB: TIntegerField
      FieldName = 'QUELLE_SUB'
    end
    object CpySrcKopfTabADDR_ID: TIntegerField
      FieldName = 'ADDR_ID'
      Required = True
    end
    object CpySrcKopfTabATRNUM: TIntegerField
      FieldName = 'ATRNUM'
    end
    object CpySrcKopfTabVRENUM: TIntegerField
      FieldName = 'VRENUM'
      Required = True
    end
    object CpySrcKopfTabVLSNUM: TIntegerField
      FieldName = 'VLSNUM'
    end
    object CpySrcKopfTabFOLGENR: TIntegerField
      FieldName = 'FOLGENR'
    end
    object CpySrcKopfTabKM_STAND: TIntegerField
      FieldName = 'KM_STAND'
    end
    object CpySrcKopfTabKFZ_ID: TIntegerField
      FieldName = 'KFZ_ID'
    end
    object CpySrcKopfTabVERTRETER_ID: TIntegerField
      FieldName = 'VERTRETER_ID'
    end
    object CpySrcKopfTabGLOBRABATT: TFloatField
      FieldName = 'GLOBRABATT'
    end
    object CpySrcKopfTabADATUM: TDateField
      FieldName = 'ADATUM'
    end
    object CpySrcKopfTabRDATUM: TDateField
      FieldName = 'RDATUM'
      Required = True
    end
    object CpySrcKopfTabLDATUM: TDateField
      FieldName = 'LDATUM'
    end
    object CpySrcKopfTabTermin: TDateField
      FieldName = 'TERMIN'
    end
    object CpySrcKopfTabPR_EBENE: TIntegerField
      FieldName = 'PR_EBENE'
    end
    object CpySrcKopfTabLIEFART: TIntegerField
      FieldName = 'LIEFART'
    end
    object CpySrcKopfTabZAHLART: TIntegerField
      FieldName = 'ZAHLART'
    end
    object CpySrcKopfTabKOST_NETTO: TFloatField
      FieldName = 'KOST_NETTO'
    end
    object CpySrcKopfTabWERT_NETTO: TFloatField
      FieldName = 'WERT_NETTO'
    end
    object CpySrcKopfTabLOHN: TFloatField
      FieldName = 'LOHN'
    end
    object CpySrcKopfTabWARE: TFloatField
      FieldName = 'WARE'
    end
    object CpySrcKopfTabTKOST: TFloatField
      FieldName = 'TKOST'
    end
    object CpySrcKopfTabMWST_0: TFloatField
      FieldName = 'MWST_0'
    end
    object CpySrcKopfTabMWST_1: TFloatField
      FieldName = 'MWST_1'
    end
    object CpySrcKopfTabMWST_2: TFloatField
      FieldName = 'MWST_2'
    end
    object CpySrcKopfTabMWST_3: TFloatField
      FieldName = 'MWST_3'
    end
    object CpySrcKopfTabNSUMME: TFloatField
      FieldName = 'NSUMME'
    end
    object CpySrcKopfTabMSUMME_0: TFloatField
      FieldName = 'MSUMME_0'
    end
    object CpySrcKopfTabMSUMME_1: TFloatField
      FieldName = 'MSUMME_1'
    end
    object CpySrcKopfTabMSUMME_2: TFloatField
      FieldName = 'MSUMME_2'
    end
    object CpySrcKopfTabMSUMME_3: TFloatField
      FieldName = 'MSUMME_3'
    end
    object CpySrcKopfTabMSUMME: TFloatField
      FieldName = 'MSUMME'
    end
    object CpySrcKopfTabBSUMME: TFloatField
      FieldName = 'BSUMME'
    end
    object CpySrcKopfTabATSUMME: TFloatField
      FieldName = 'ATSUMME'
    end
    object CpySrcKopfTabATMSUMME: TFloatField
      FieldName = 'ATMSUMME'
    end
    object CpySrcKopfTabWAEHRUNG: TStringField
      FieldName = 'WAEHRUNG'
      Size = 5
    end
    object CpySrcKopfTabGEGENKONTO: TIntegerField
      FieldName = 'GEGENKONTO'
    end
    object CpySrcKopfTabSOLL_STAGE: TIntegerField
      FieldName = 'SOLL_STAGE'
    end
    object CpySrcKopfTabSOLL_SKONTO: TFloatField
      FieldName = 'SOLL_SKONTO'
    end
    object CpySrcKopfTabSOLL_NTAGE: TIntegerField
      FieldName = 'SOLL_NTAGE'
    end
    object CpySrcKopfTabSOLL_RATEN: TIntegerField
      FieldName = 'SOLL_RATEN'
    end
    object CpySrcKopfTabSOLL_RATBETR: TFloatField
      FieldName = 'SOLL_RATBETR'
    end
    object CpySrcKopfTabSOLL_RATINTERVALL: TIntegerField
      FieldName = 'SOLL_RATINTERVALL'
    end
    object CpySrcKopfTabIST_ANZAHLUNG: TFloatField
      FieldName = 'IST_ANZAHLUNG'
    end
    object CpySrcKopfTabIST_SKONTO: TFloatField
      FieldName = 'IST_SKONTO'
    end
    object CpySrcKopfTabIST_ZAHLDAT: TDateField
      FieldName = 'IST_ZAHLDAT'
    end
    object CpySrcKopfTabIST_BETRAG: TFloatField
      FieldName = 'IST_BETRAG'
    end
    object CpySrcKopfTabMAHNKOSTEN: TFloatField
      FieldName = 'MAHNKOSTEN'
    end
    object CpySrcKopfTabKONTOAUSZUG: TIntegerField
      FieldName = 'KONTOAUSZUG'
    end
    object CpySrcKopfTabBANK_ID: TIntegerField
      FieldName = 'BANK_ID'
    end
    object CpySrcKopfTabSTADIUM: TIntegerField
      FieldName = 'STADIUM'
    end
    object CpySrcKopfTabERSTELLT: TDateField
      FieldName = 'ERSTELLT'
    end
    object CpySrcKopfTabERST_NAME: TStringField
      FieldName = 'ERST_NAME'
    end
    object CpySrcKopfTabKUN_ANREDE: TStringField
      FieldName = 'KUN_ANREDE'
      Size = 40
    end
    object CpySrcKopfTabKUN_NAME1: TStringField
      FieldName = 'KUN_NAME1'
      Size = 40
    end
    object CpySrcKopfTabKUN_NAME2: TStringField
      FieldName = 'KUN_NAME2'
      Size = 40
    end
    object CpySrcKopfTabKUN_NAME3: TStringField
      FieldName = 'KUN_NAME3'
      Size = 40
    end
    object CpySrcKopfTabKUN_ABTEILUNG: TStringField
      FieldName = 'KUN_ABTEILUNG'
      Size = 40
    end
    object CpySrcKopfTabKUN_STRASSE: TStringField
      FieldName = 'KUN_STRASSE'
      Size = 40
    end
    object CpySrcKopfTabKUN_LAND: TStringField
      FieldName = 'KUN_LAND'
      Size = 5
    end
    object CpySrcKopfTabKUN_PLZ: TStringField
      FieldName = 'KUN_PLZ'
      Size = 10
    end
    object CpySrcKopfTabKUN_ORT: TStringField
      FieldName = 'KUN_ORT'
      Size = 40
    end
    object CpySrcKopfTabUSR1: TStringField
      FieldName = 'USR1'
      Size = 80
    end
    object CpySrcKopfTabUSR2: TStringField
      FieldName = 'USR2'
      Size = 80
    end
    object CpySrcKopfTabPROJEKT: TStringField
      FieldName = 'PROJEKT'
      Size = 80
    end
    object CpySrcKopfTabORGNUM: TStringField
      FieldName = 'ORGNUM'
    end
    object CpySrcKopfTabBEST_NAME: TStringField
      FieldName = 'BEST_NAME'
      Size = 40
    end
    object CpySrcKopfTabBEST_CODE: TIntegerField
      FieldName = 'BEST_CODE'
    end
    object CpySrcKopfTabINFO: TMemoField
      FieldName = 'INFO'
      BlobType = ftMemo
    end
    object CpySrcKopfTabUW_NUM: TIntegerField
      FieldName = 'UW_NUM'
    end
    object CpySrcKopfTabKUN_NUM: TStringField
      FieldName = 'KUN_NUM'
    end
    object CpySrcKopfTabBEST_DATUM: TDateField
      FieldName = 'BEST_DATUM'
    end
    object CpySrcKopfTabLIEF_ADDR_ID: TIntegerField
      FieldName = 'LIEF_ADDR_ID'
    end
    object CpySrcKopfTabMAHNSTUFE: TIntegerField
      FieldName = 'MAHNSTUFE'
    end
    object CpySrcKopfTabMAHNDATUM: TDateField
      FieldName = 'MAHNDATUM'
    end
    object CpySrcKopfTabMAHNPRINT: TIntegerField
      FieldName = 'MAHNPRINT'
    end
    object CpySrcKopfTabFREIGABE1_FLAG: TBooleanField
      FieldName = 'FREIGABE1_FLAG'
      Required = True
    end
    object CpySrcKopfTabBRUTTO_FLAG: TBooleanField
      FieldName = 'BRUTTO_FLAG'
      Required = True
    end
    object CpySrcKopfTabMWST_FREI_FLAG: TBooleanField
      FieldName = 'MWST_FREI_FLAG'
      Required = True
    end
    object CpySrcKopfTabPROVIS_WERT: TFloatField
      FieldName = 'PROVIS_WERT'
    end
    object CpySrcKopfTabGEWICHT: TFloatField
      FieldName = 'GEWICHT'
      Required = True
    end
    object CpySrcKopfTabROHGEWINN: TFloatField
      FieldName = 'ROHGEWINN'
      Required = True
    end
  end
  object CpyDstKopfTab: TZMySqlQuery
    Database = db1
    Transaction = Transact1
    CachedUpdates = False
    ShowRecordTypes = [ztModified, ztInserted, ztUnmodified]
    Options = [doHourGlass, doAutoFillDefs, doUseRowId]
    LinkOptions = [loAlwaysResync]
    Constraints = <>
    ExtraOptions = [moStoreResult]
    AlternativeView = True
    Macros = <>
    Sql.Strings = (
      'select * from JOURNAL'
      'limit 0,1')
    RequestLive = True
    Left = 538
    Top = 16
    object CpyDstKopfTabQUELLE: TIntegerField
      FieldName = 'QUELLE'
      Required = True
    end
    object CpyDstKopfTabREC_ID: TIntegerField
      FieldName = 'REC_ID'
    end
    object CpyDstKopfTabQUELLE_SUB: TIntegerField
      FieldName = 'QUELLE_SUB'
    end
    object CpyDstKopfTabADDR_ID: TIntegerField
      FieldName = 'ADDR_ID'
      Required = True
    end
    object CpyDstKopfTabATRNUM: TIntegerField
      FieldName = 'ATRNUM'
    end
    object CpyDstKopfTabVRENUM: TIntegerField
      FieldName = 'VRENUM'
      Required = True
    end
    object CpyDstKopfTabVLSNUM: TIntegerField
      FieldName = 'VLSNUM'
    end
    object CpyDstKopfTabFOLGENR: TIntegerField
      FieldName = 'FOLGENR'
    end
    object CpyDstKopfTabKM_STAND: TIntegerField
      FieldName = 'KM_STAND'
    end
    object CpyDstKopfTabKFZ_ID: TIntegerField
      FieldName = 'KFZ_ID'
    end
    object CpyDstKopfTabVERTRETER_ID: TIntegerField
      FieldName = 'VERTRETER_ID'
    end
    object CpyDstKopfTabGLOBRABATT: TFloatField
      FieldName = 'GLOBRABATT'
    end
    object CpyDstKopfTabADATUM: TDateField
      FieldName = 'ADATUM'
    end
    object CpyDstKopfTabRDATUM: TDateField
      FieldName = 'RDATUM'
      Required = True
    end
    object CpyDstKopfTabLDATUM: TDateField
      FieldName = 'LDATUM'
    end
    object CpyDstKopfTabTermin: TDateField
      FieldName = 'TERMIN'
    end
    object CpyDstKopfTabPR_EBENE: TIntegerField
      FieldName = 'PR_EBENE'
    end
    object CpyDstKopfTabLIEFART: TIntegerField
      FieldName = 'LIEFART'
    end
    object CpyDstKopfTabZAHLART: TIntegerField
      FieldName = 'ZAHLART'
    end
    object CpyDstKopfTabKOST_NETTO: TFloatField
      FieldName = 'KOST_NETTO'
    end
    object CpyDstKopfTabWERT_NETTO: TFloatField
      FieldName = 'WERT_NETTO'
    end
    object CpyDstKopfTabLOHN: TFloatField
      FieldName = 'LOHN'
    end
    object CpyDstKopfTabWARE: TFloatField
      FieldName = 'WARE'
    end
    object CpyDstKopfTabTKOST: TFloatField
      FieldName = 'TKOST'
    end
    object CpyDstKopfTabMWST_0: TFloatField
      FieldName = 'MWST_0'
    end
    object CpyDstKopfTabMWST_1: TFloatField
      FieldName = 'MWST_1'
    end
    object CpyDstKopfTabMWST_2: TFloatField
      FieldName = 'MWST_2'
    end
    object CpyDstKopfTabMWST_3: TFloatField
      FieldName = 'MWST_3'
    end
    object CpyDstKopfTabNSUMME: TFloatField
      FieldName = 'NSUMME'
    end
    object CpyDstKopfTabMSUMME_0: TFloatField
      FieldName = 'MSUMME_0'
    end
    object CpyDstKopfTabMSUMME_1: TFloatField
      FieldName = 'MSUMME_1'
    end
    object CpyDstKopfTabMSUMME_2: TFloatField
      FieldName = 'MSUMME_2'
    end
    object CpyDstKopfTabMSUMME_3: TFloatField
      FieldName = 'MSUMME_3'
    end
    object CpyDstKopfTabMSUMME: TFloatField
      FieldName = 'MSUMME'
    end
    object CpyDstKopfTabBSUMME: TFloatField
      FieldName = 'BSUMME'
    end
    object CpyDstKopfTabATSUMME: TFloatField
      FieldName = 'ATSUMME'
    end
    object CpyDstKopfTabATMSUMME: TFloatField
      FieldName = 'ATMSUMME'
    end
    object CpyDstKopfTabWAEHRUNG: TStringField
      FieldName = 'WAEHRUNG'
      Size = 5
    end
    object CpyDstKopfTabGEGENKONTO: TIntegerField
      FieldName = 'GEGENKONTO'
    end
    object CpyDstKopfTabSOLL_STAGE: TIntegerField
      FieldName = 'SOLL_STAGE'
    end
    object CpyDstKopfTabSOLL_SKONTO: TFloatField
      FieldName = 'SOLL_SKONTO'
    end
    object CpyDstKopfTabSOLL_NTAGE: TIntegerField
      FieldName = 'SOLL_NTAGE'
    end
    object CpyDstKopfTabSOLL_RATEN: TIntegerField
      FieldName = 'SOLL_RATEN'
    end
    object CpyDstKopfTabSOLL_RATBETR: TFloatField
      FieldName = 'SOLL_RATBETR'
    end
    object CpyDstKopfTabSOLL_RATINTERVALL: TIntegerField
      FieldName = 'SOLL_RATINTERVALL'
    end
    object CpyDstKopfTabIST_ANZAHLUNG: TFloatField
      FieldName = 'IST_ANZAHLUNG'
    end
    object CpyDstKopfTabIST_SKONTO: TFloatField
      FieldName = 'IST_SKONTO'
    end
    object CpyDstKopfTabIST_ZAHLDAT: TDateField
      FieldName = 'IST_ZAHLDAT'
    end
    object CpyDstKopfTabIST_BETRAG: TFloatField
      FieldName = 'IST_BETRAG'
    end
    object CpyDstKopfTabMAHNKOSTEN: TFloatField
      FieldName = 'MAHNKOSTEN'
    end
    object CpyDstKopfTabKONTOAUSZUG: TIntegerField
      FieldName = 'KONTOAUSZUG'
    end
    object CpyDstKopfTabBANK_ID: TIntegerField
      FieldName = 'BANK_ID'
    end
    object CpyDstKopfTabSTADIUM: TIntegerField
      FieldName = 'STADIUM'
    end
    object CpyDstKopfTabERSTELLT: TDateField
      FieldName = 'ERSTELLT'
    end
    object CpyDstKopfTabERST_NAME: TStringField
      FieldName = 'ERST_NAME'
    end
    object CpyDstKopfTabKUN_ANREDE: TStringField
      FieldName = 'KUN_ANREDE'
      Size = 40
    end
    object CpyDstKopfTabKUN_NAME1: TStringField
      FieldName = 'KUN_NAME1'
      Size = 40
    end
    object CpyDstKopfTabKUN_NAME2: TStringField
      FieldName = 'KUN_NAME2'
      Size = 40
    end
    object CpyDstKopfTabKUN_NAME3: TStringField
      FieldName = 'KUN_NAME3'
      Size = 40
    end
    object CpyDstKopfTabKUN_ABTEILUNG: TStringField
      FieldName = 'KUN_ABTEILUNG'
      Size = 40
    end
    object CpyDstKopfTabKUN_STRASSE: TStringField
      FieldName = 'KUN_STRASSE'
      Size = 40
    end
    object CpyDstKopfTabKUN_LAND: TStringField
      FieldName = 'KUN_LAND'
      Size = 5
    end
    object CpyDstKopfTabKUN_PLZ: TStringField
      FieldName = 'KUN_PLZ'
      Size = 10
    end
    object CpyDstKopfTabKUN_ORT: TStringField
      FieldName = 'KUN_ORT'
      Size = 40
    end
    object CpyDstKopfTabUSR1: TStringField
      FieldName = 'USR1'
      Size = 80
    end
    object CpyDstKopfTabUSR2: TStringField
      FieldName = 'USR2'
      Size = 80
    end
    object CpyDstKopfTabPROJEKT: TStringField
      FieldName = 'PROJEKT'
      Size = 80
    end
    object CpyDstKopfTabORGNUM: TStringField
      FieldName = 'ORGNUM'
    end
    object CpyDstKopfTabBEST_NAME: TStringField
      FieldName = 'BEST_NAME'
      Size = 40
    end
    object CpyDstKopfTabBEST_CODE: TIntegerField
      FieldName = 'BEST_CODE'
    end
    object CpyDstKopfTabINFO: TMemoField
      FieldName = 'INFO'
      BlobType = ftMemo
    end
    object CpyDstKopfTabUW_NUM: TIntegerField
      FieldName = 'UW_NUM'
    end
    object CpyDstKopfTabKUN_NUM: TStringField
      FieldName = 'KUN_NUM'
    end
    object CpyDstKopfTabBEST_DATUM: TDateField
      FieldName = 'BEST_DATUM'
    end
    object CpyDstKopfTabLIEF_ADDR_ID: TIntegerField
      FieldName = 'LIEF_ADDR_ID'
    end
    object CpyDstKopfTabMAHNSTUFE: TIntegerField
      FieldName = 'MAHNSTUFE'
    end
    object CpyDstKopfTabMAHNDATUM: TDateField
      FieldName = 'MAHNDATUM'
    end
    object CpyDstKopfTabMAHNPRINT: TIntegerField
      FieldName = 'MAHNPRINT'
    end
    object CpyDstKopfTabFREIGABE1_FLAG: TBooleanField
      FieldName = 'FREIGABE1_FLAG'
      Required = True
    end
    object CpyDstKopfTabBRUTTO_FLAG: TBooleanField
      FieldName = 'BRUTTO_FLAG'
      Required = True
    end
    object CpyDstKopfTabMWST_FREI_FLAG: TBooleanField
      FieldName = 'MWST_FREI_FLAG'
      Required = True
    end
    object CpyDstKopfTabPROVIS_WERT: TFloatField
      FieldName = 'PROVIS_WERT'
    end
    object CpyDstKopfTabGEWICHT: TFloatField
      FieldName = 'GEWICHT'
    end
    object CpyDstKopfTabROHGEWINN: TFloatField
      FieldName = 'ROHGEWINN'
    end
  end
  object CpySrcPosTab: TZMySqlQuery
    Database = db1
    Transaction = Transact1
    CachedUpdates = False
    ShowRecordTypes = [ztModified, ztInserted, ztUnmodified]
    Options = [doHourGlass, doAutoFillDefs, doUseRowId]
    LinkOptions = [loAlwaysResync]
    Constraints = <>
    ExtraOptions = [moStoreResult]
    AlternativeView = True
    Macros = <>
    Sql.Strings = (
      'select * from JOURNALPOS'
      'where JOURNAL_ID=:ID and ARTIKELTYP !="X"')
    RequestLive = False
    Left = 458
    Top = 64
    ParamData = <
      item
        DataType = ftInteger
        Name = 'ID'
        ParamType = ptInput
      end>
  end
  object CpyDstPosTab: TZMySqlQuery
    Database = db1
    Transaction = Transact1
    CachedUpdates = False
    ShowRecordTypes = [ztModified, ztInserted, ztUnmodified]
    Options = [doHourGlass, doAutoFillDefs, doUseRowId]
    LinkOptions = [loAlwaysResync]
    Constraints = <>
    ExtraOptions = [moStoreResult]
    AlternativeView = True
    Macros = <>
    Sql.Strings = (
      'select * from JOURNALPOS'
      'limit 0,1')
    RequestLive = True
    Left = 538
    Top = 64
  end
  object KasBuch: TZMySqlQuery
    Database = db1
    Transaction = Transact1
    CachedUpdates = False
    ShowRecordTypes = [ztModified, ztInserted, ztUnmodified]
    Options = [doHourGlass, doAutoFillDefs, doUseRowId]
    LinkOptions = [loAlwaysResync]
    Constraints = <>
    ExtraOptions = [moStoreResult]
    Macros = <>
    Sql.Strings = (
      'select * from FIBU_KASSE'
      'where JAHR=:JAHR'
      'limit 0,1')
    RequestLive = True
    Left = 18
    Top = 512
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'JAHR'
        ParamType = ptUnknown
      end>
    object KasBuchJAHR: TIntegerField
      FieldName = 'JAHR'
      Required = True
    end
    object KasBuchBDATUM: TDateField
      FieldName = 'BDATUM'
      Required = True
    end
    object KasBuchQUELLE: TIntegerField
      FieldName = 'QUELLE'
      Required = True
    end
    object KasBuchJOURNAL_ID: TIntegerField
      FieldName = 'JOURNAL_ID'
    end
    object KasBuchZU_ABGANG: TFloatField
      FieldName = 'ZU_ABGANG'
      Required = True
    end
    object KasBuchBTXT: TMemoField
      FieldName = 'BTXT'
      BlobType = ftMemo
    end
    object KasBuchBELEGNUM: TStringField
      FieldName = 'BELEGNUM'
      Size = 10
    end
    object KasBuchGKONTO: TIntegerField
      FieldName = 'GKONTO'
    end
    object KasBuchSKONTO: TFloatField
      FieldName = 'SKONTO'
    end
  end
  object STListTab: TZMySqlQuery
    Database = db1
    Transaction = Transact1
    CachedUpdates = False
    ShowRecordTypes = [ztModified, ztInserted, ztUnmodified]
    Options = [doHourGlass, doAutoFillDefs, doUseRowId]
    LinkOptions = [loAlwaysResync]
    Constraints = <>
    ExtraOptions = [moStoreResult]
    Macros = <>
    Sql.Strings = (
      'select * from ARTIKEL_STUECKLIST'
      'where REC_ID=:ID')
    RequestLive = False
    Left = 327
    Top = 232
    ParamData = <
      item
        DataType = ftInteger
        Name = 'ID'
        ParamType = ptInput
      end>
    object STListTabREC_ID: TIntegerField
      FieldName = 'REC_ID'
      Visible = False
    end
    object STListTabART_ID: TIntegerField
      FieldName = 'ART_ID'
      Required = True
      Visible = False
    end
    object STListTabMENGE: TFloatField
      DisplayLabel = 'Menge'
      FieldName = 'MENGE'
      DisplayFormat = '0.00'
    end
    object STListTabName: TStringField
      DisplayLabel = 'Artikelname'
      FieldKind = fkCalculated
      FieldName = 'Name'
      LookupCache = True
      Size = 50
      Calculated = True
    end
    object STListTabERSTELLT: TDateField
      FieldName = 'ERSTELLT'
      Visible = False
    end
    object STListTabERST_NAME: TStringField
      FieldName = 'ERST_NAME'
      Visible = False
    end
    object STListTabGEAEND: TDateField
      FieldName = 'GEAEND'
      Visible = False
    end
    object STListTabGEAEND_NAME: TStringField
      FieldName = 'GEAEND_NAME'
      Visible = False
    end
  end
  object KunTab: TZMySqlQuery
    Database = db1
    Transaction = Transact1
    CachedUpdates = False
    ShowRecordTypes = [ztModified, ztInserted, ztUnmodified]
    Options = [doHourGlass, doAutoFillDefs, doUseRowId]
    LinkOptions = [loAlwaysResync]
    Constraints = <>
    ExtraOptions = [moStoreResult]
    AlternativeView = True
    Macros = <>
    Sql.Strings = (
      'select * from ADRESSEN'
      'where REC_ID = :ID'
      'limit 0,10')
    RequestLive = True
    Left = 325
    Top = 176
    ParamData = <
      item
        DataType = ftInteger
        Name = 'ID'
        ParamType = ptInput
      end>
    object KunTabREC_ID: TIntegerField
      FieldName = 'REC_ID'
    end
    object KunTabKUNNUM1: TStringField
      FieldName = 'KUNNUM1'
    end
    object KunTabKUNNUM2: TStringField
      FieldName = 'KUNNUM2'
    end
    object KunTabDEB_NUM: TIntegerField
      FieldName = 'DEB_NUM'
    end
    object KunTabKRD_NUM: TIntegerField
      FieldName = 'KRD_NUM'
    end
    object KunTabSTATUS: TIntegerField
      FieldName = 'STATUS'
    end
    object KunTabKUN_LIEFART: TIntegerField
      FieldName = 'KUN_LIEFART'
    end
    object KunTabKUN_ZAHLART: TIntegerField
      FieldName = 'KUN_ZAHLART'
    end
    object KunTabLIEF_LIEFART: TIntegerField
      FieldName = 'LIEF_LIEFART'
    end
    object KunTabLIEF_ZAHLART: TIntegerField
      FieldName = 'LIEF_ZAHLART'
    end
    object KunTabPR_EBENE: TIntegerField
      FieldName = 'PR_EBENE'
    end
    object KunTabLAND: TStringField
      FieldName = 'LAND'
      Size = 5
    end
    object KunTabNAME1: TStringField
      FieldName = 'NAME1'
      Size = 40
    end
    object KunTabPLZ: TStringField
      FieldName = 'PLZ'
      Size = 10
    end
    object KunTabORT: TStringField
      FieldName = 'ORT'
      Size = 40
    end
    object KunTabNAME2: TStringField
      FieldName = 'NAME2'
      Size = 40
    end
    object KunTabNAME3: TStringField
      FieldName = 'NAME3'
      Size = 40
    end
    object KunTabANREDE: TStringField
      FieldName = 'ANREDE'
      Size = 40
    end
    object KunTabSTRASSE: TStringField
      FieldName = 'STRASSE'
      Size = 40
    end
    object KunTabBLZ: TStringField
      FieldName = 'BLZ'
      Size = 8
    end
    object KunTabKTO: TStringField
      FieldName = 'KTO'
      Size = 10
    end
    object KunTabBANK: TStringField
      FieldName = 'BANK'
      Size = 40
    end
    object KunTabEMAIL: TStringField
      FieldName = 'EMAIL'
      Size = 50
    end
    object KunTabBRUTTO_FLAG: TBooleanField
      FieldName = 'BRUTTO_FLAG'
      Required = True
    end
    object KunTabMWST_FREI_FLAG: TBooleanField
      FieldName = 'MWST_FREI_FLAG'
      Required = True
    end
    object KunTabDEFAULT_LIEFANSCHRIFT_ID: TIntegerField
      FieldName = 'DEFAULT_LIEFANSCHRIFT_ID'
    end
    object KunTabUST_NUM: TStringField
      FieldName = 'UST_NUM'
      Size = 25
    end
    object KunTabFAX: TStringField
      FieldName = 'FAX'
      Size = 100
    end
    object KunTabBRIEFANREDE: TStringField
      FieldName = 'BRIEFANREDE'
      Size = 100
    end
  end
  object CreateMandantStr: TJvStrHolder
    Capacity = 1695
    Macros = <>
    Duplicates = dupAccept
    Left = 320
    Top = 400
    InternalVer = 1
    StrData = (
      ''
      
        '2f2a20546162656c6c656e20667565722043414f2d46616b74757261204d5953' +
        '514c205374616e642030352e31312e323030332044422d56657273696f6e2031' +
        '2e3039202a2f'
      ''
      '435245415445205441424c4520414452455353454e2028'
      
        '20205245435f494420696e7428313129204e4f54204e554c4c206175746f5f69' +
        '6e6372656d656e742c'
      
        '20204d41544348434f444520766172636861722832353529204e4f54204e554c' +
        '4c2064656661756c742027272c'
      
        '20204b554e44454e47525550504520696e7428313129204e4f54204e554c4c20' +
        '64656661756c74202730272c'
      
        '20205350524143485f494420696e7428313129204e4f54204e554c4c20646566' +
        '61756c74202732272c'
      
        '20204b554e4e554d312076617263686172283230292064656661756c74204e55' +
        '4c4c2c'
      
        '20204b554e4e554d322076617263686172283230292064656661756c74204e55' +
        '4c4c2c'
      
        '20204e414d45312076617263686172283430292064656661756c74204e554c4c' +
        '2c'
      '2020504c5a2076617263686172283130292064656661756c74204e554c4c2c'
      '20204f52542076617263686172283430292064656661756c74204e554c4c2c'
      '20204c414e4420766172636861722835292064656661756c74204e554c4c2c'
      
        '20204e414d45322076617263686172283430292064656661756c74204e554c4c' +
        '2c'
      
        '20204e414d45332076617263686172283430292064656661756c74204e554c4c' +
        '2c'
      
        '202041425445494c554e472076617263686172283430292064656661756c7420' +
        '4e554c4c2c'
      
        '2020414e524544452076617263686172283430292064656661756c74204e554c' +
        '4c2c'
      
        '2020535452415353452076617263686172283430292064656661756c74204e55' +
        '4c4c2c'
      
        '2020504f5354464143482076617263686172283430292064656661756c74204e' +
        '554c4c2c'
      
        '202050465f504c5a2076617263686172283130292064656661756c74204e554c' +
        '4c2c'
      
        '202044454641554c545f4c494546414e534348524946545f494420696e742831' +
        '3129204e4f54204e554c4c2064656661756c7420272d31272c'
      
        '202047525550504520766172636861722834292064656661756c74204e554c4c' +
        '2c'
      
        '202054454c4531207661726368617228313030292064656661756c74204e554c' +
        '4c2c'
      
        '202054454c4532207661726368617228313030292064656661756c74204e554c' +
        '4c2c'
      '2020464158207661726368617228313030292064656661756c74204e554c4c2c'
      
        '202046554e4b207661726368617228313030292064656661756c74204e554c4c' +
        '2c'
      
        '2020454d41494c207661726368617228313030292064656661756c74204e554c' +
        '4c2c'
      
        '2020454d41494c32207661726368617228313030292064656661756c74204e55' +
        '4c4c2c'
      
        '2020494e5445524e4554207661726368617228313030292064656661756c7420' +
        '4e554c4c2c'
      
        '20204449564552534553207661726368617228313030292064656661756c7420' +
        '4e554c4c2c'
      
        '20204252494546414e5245444520766172636861722831303029206465666175' +
        '6c74204e554c4c2c'
      '2020424c5a2076617263686172283230292064656661756c74204e554c4c2c'
      '20204b544f2076617263686172283230292064656661756c74204e554c4c2c'
      '202042414e4b2076617263686172283430292064656661756c74204e554c4c2c'
      
        '20204942414e207661726368617228313030292064656661756c74204e554c4c' +
        '2c'
      
        '20205357494654207661726368617228313030292064656661756c74204e554c' +
        '4c2c'
      
        '20204b544f5f494e48414245522076617263686172283430292064656661756c' +
        '74204e554c4c2c'
      '20204445425f4e554d20696e74283131292064656661756c74204e554c4c2c'
      '20204b52445f4e554d20696e74283131292064656661756c74204e554c4c2c'
      '202053544154555320696e74283131292064656661756c74204e554c4c2c'
      
        '20204e45545f534b4f4e544f20666c6f617428352c32292064656661756c7420' +
        '4e554c4c2c'
      
        '20204e45545f544147452074696e79696e742834292064656661756c74204e55' +
        '4c4c2c'
      
        '20204252545f544147452074696e79696e742834292064656661756c74204e55' +
        '4c4c2c'
      
        '202057414552554e4720766172636861722835292064656661756c74204e554c' +
        '4c2c'
      
        '20205553545f4e554d2076617263686172283235292064656661756c74204e55' +
        '4c4c2c'
      
        '20205645525452455445525f494420696e742831312920756e7369676e656420' +
        '4e4f54204e554c4c2064656661756c74202730272c'
      
        '202050524f5649535f50524f5a20666c6f617428352c3229204e4f54204e554c' +
        '4c2064656661756c742027302e3030272c'
      '2020494e464f20746578742c'
      
        '20204752414241545420666c6f617428352c32292064656661756c74204e554c' +
        '4c2c'
      
        '20204b554e5f4b52444c494d495420666c6f61742831302c3229206465666175' +
        '6c74204e554c4c2c'
      
        '20204b554e5f4c49454641525420696e7428313129204e4f54204e554c4c2064' +
        '656661756c7420272d31272c'
      
        '20204b554e5f5a41484c41525420696e7428313129204e4f54204e554c4c2064' +
        '656661756c7420272d31272c'
      
        '20204b554e5f50524c4953544520656e756d28274e272c27592729204e4f5420' +
        '4e554c4c2064656661756c7420274e272c'
      
        '20204c4945465f4c49454641525420696e7428313129204e4f54204e554c4c20' +
        '64656661756c7420272d31272c'
      
        '20204c4945465f5a41484c41525420696e7428313129204e4f54204e554c4c20' +
        '64656661756c7420272d31272c'
      
        '20204c4945465f50524c4953544520656e756d28274e272c27592729204e4f54' +
        '204e554c4c2064656661756c7420274e272c'
      
        '202050525f4542454e452074696e79696e742831292064656661756c74202735' +
        '272c'
      
        '202042525554544f5f464c414720656e756d28274e272c27592729204e4f5420' +
        '4e554c4c2064656661756c7420274e272c'
      
        '20204d5753545f465245495f464c414720656e756d28274e272c27592729204e' +
        '4f54204e554c4c2064656661756c7420274e272c'
      '20204b554e5f5345495420646174652064656661756c74204e554c4c2c'
      
        '20204b554e5f474542444154554d20646174652064656661756c74204e554c4c' +
        '2c'
      
        '2020454e544645524e554e4720696e742831302920756e7369676e6564206465' +
        '6661756c74204e554c4c2c'
      '202045525354454c4c5420646174652064656661756c74204e554c4c2c'
      
        '2020455253545f4e414d452076617263686172283230292064656661756c7420' +
        '4e554c4c2c'
      '2020474541454e4420646174652064656661756c74204e554c4c2c'
      
        '2020474541454e445f4e414d452076617263686172283230292064656661756c' +
        '74204e554c4c2c'
      
        '202053484f505f4b554e444520656e756d28274e272c27592729204e4f54204e' +
        '554c4c2064656661756c7420274e272c'
      
        '202053484f505f49442074696e79696e74283329204e4f54204e554c4c206465' +
        '6661756c7420272d31272c'
      
        '202053484f505f4b554e44455f494420696e7428313129204e4f54204e554c4c' +
        '2064656661756c7420272d31272c'
      
        '202053484f505f4348414e47455f464c41472054494e59494e54283129202055' +
        '4e5349474e45442044454641554c5420223022204e4f54204e554c4c2c'
      
        '202053484f505f44454c5f464c414720454e554d28224e222c22592229202044' +
        '454641554c5420224e22204e4f54204e554c4c2c'
      '202053484f505f50415353574f52442056415243484152283230292c'
      
        '20205553455246454c445f303120766172636861722832353529206465666175' +
        '6c74204e554c4c2c'
      
        '20205553455246454c445f303220766172636861722832353529206465666175' +
        '6c74204e554c4c2c'
      
        '20205553455246454c445f303320766172636861722832353529206465666175' +
        '6c74204e554c4c2c'
      
        '20205553455246454c445f303420766172636861722832353529206465666175' +
        '6c74204e554c4c2c'
      
        '20205553455246454c445f303520766172636861722832353529206465666175' +
        '6c74204e554c4c2c'
      
        '20205553455246454c445f303620766172636861722832353529206465666175' +
        '6c74204e554c4c2c'
      
        '20205553455246454c445f303720766172636861722832353529206465666175' +
        '6c74204e554c4c2c'
      
        '20205553455246454c445f303820766172636861722832353529206465666175' +
        '6c74204e554c4c2c'
      
        '20205553455246454c445f303920766172636861722832353529206465666175' +
        '6c74204e554c4c2c'
      
        '20205553455246454c445f313020766172636861722832353529206465666175' +
        '6c74204e554c4c2c'
      '20205052494d415259204b45592020285245435f494429'
      '293b'
      ''
      '435245415445205441424c4520414452455353454e5f4c4945462028'
      
        '20205245435f494420696e7428313129204e4f54204e554c4c206175746f5f69' +
        '6e6372656d656e742c'
      
        '2020414444525f494420696e7428313129204e4f54204e554c4c206465666175' +
        '6c74202730272c'
      
        '2020414e524544452076617263686172283430292064656661756c74204e554c' +
        '4c2c'
      
        '20204e414d4531207661726368617228343029204e4f54204e554c4c20646566' +
        '61756c742027272c'
      
        '20204e414d45322076617263686172283430292064656661756c74204e554c4c' +
        '2c'
      
        '20204e414d45332076617263686172283430292064656661756c74204e554c4c' +
        '2c'
      
        '202041425445494c554e472076617263686172283430292064656661756c7420' +
        '4e554c4c2c'
      
        '202053545241535345207661726368617228343029204e4f54204e554c4c2064' +
        '656661756c742027272c'
      '20204c414e4420766172636861722835292064656661756c74204e554c4c2c'
      
        '2020504c5a207661726368617228313029204e4f54204e554c4c206465666175' +
        '6c742027272c'
      
        '20204f5254207661726368617228343029204e4f54204e554c4c206465666175' +
        '6c742027272c'
      '2020494e464f20746578742c'
      '20205052494d415259204b45592020285245435f49442c414444525f494429'
      '293b'
      ''
      '435245415445205441424c4520414452455353454e5f4d45524b2028'
      
        '20204d45524b4d414c5f494420696e7428313129204e4f54204e554c4c206175' +
        '746f5f696e6372656d656e742c'
      
        '20204e414d4520766172636861722831303029204e4f54204e554c4c20646566' +
        '61756c742027272c'
      '20205052494d415259204b45592020284d45524b4d414c5f494429'
      '293b'
      ''
      '435245415445205441424c4520414452455353454e5f544f5f4d45524b2028'
      
        '20204d45524b4d414c5f494420696e742831312920756e7369676e6564204e4f' +
        '54204e554c4c2064656661756c74202730272c'
      
        '2020414444525f494420696e742831312920756e7369676e6564204e4f54204e' +
        '554c4c2064656661756c74202730272c'
      
        '20205052494d415259204b45592020284d45524b4d414c5f49442c414444525f' +
        '494429'
      '293b'
      ''
      '435245415445205441424c452041504152544e45522028'
      
        '2020414444525f494420696e7428313129204e4f54204e554c4c206465666175' +
        '6c74202730272c'
      
        '20205245435f494420696e7428313129204e4f54204e554c4c206175746f5f69' +
        '6e6372656d656e742c'
      
        '20204e414d45207661726368617228343029204e4f54204e554c4c2064656661' +
        '756c742027272c'
      
        '2020564f524e414d45207661726368617228343029204e4f54204e554c4c2064' +
        '656661756c742027272c'
      
        '2020414e524544452076617263686172283430292064656661756c74204e554c' +
        '4c2c'
      
        '2020535452415353452076617263686172283430292064656661756c74204e55' +
        '4c4c2c'
      '20204c414e4420766172636861722835292064656661756c74202744272c'
      '2020504c5a2076617263686172283130292064656661756c74204e554c4c2c'
      '20204f52542076617263686172283430292064656661756c74204e554c4c2c'
      
        '202046554e4b54494f4e2076617263686172283430292064656661756c74204e' +
        '554c4c2c'
      
        '202054454c45464f4e207661726368617228313030292064656661756c74204e' +
        '554c4c2c'
      
        '202054454c45464158207661726368617228313030292064656661756c74204e' +
        '554c4c2c'
      
        '20204d4f42494c46554e4b207661726368617228313030292064656661756c74' +
        '204e554c4c2c'
      
        '202054454c45505249564154207661726368617228313030292064656661756c' +
        '74204e554c4c2c'
      
        '2020454d41494c207661726368617228313030292064656661756c74204e554c' +
        '4c2c'
      
        '2020454d41494c3220766172636861722831303029204e4f54204e554c4c2064' +
        '656661756c742027272c'
      '2020494e464f20746578742c'
      '2020474542444154554d20646174652064656661756c74204e554c4c2c'
      
        '20205553455246454c445f303120766172636861722832353529206465666175' +
        '6c74204e554c4c2c'
      
        '20205553455246454c445f303220766172636861722832353529206465666175' +
        '6c74204e554c4c2c'
      
        '20205553455246454c445f303320766172636861722832353529206465666175' +
        '6c74204e554c4c2c'
      
        '20205553455246454c445f303420766172636861722832353529206465666175' +
        '6c74204e554c4c2c'
      
        '20205553455246454c445f303520766172636861722832353529206465666175' +
        '6c74204e554c4c2c'
      
        '20205553455246454c445f303620766172636861722832353529206465666175' +
        '6c74204e554c4c2c'
      
        '20205553455246454c445f303720766172636861722832353529206465666175' +
        '6c74204e554c4c2c'
      
        '20205553455246454c445f303820766172636861722832353529206465666175' +
        '6c74204e554c4c2c'
      
        '20205553455246454c445f303920766172636861722832353529206465666175' +
        '6c74204e554c4c2c'
      
        '20205553455246454c445f313020766172636861722832353529206465666175' +
        '6c74204e554c4c2c'
      '20205052494d415259204b45592020285245435f4944292c'
      
        '20204b4559204e414d452028414444525f49442c4e414d452c564f524e414d45' +
        '29'
      '293b'
      ''
      '435245415445205441424c4520415254494b454c2028'
      
        '20205245435f494420696e7428313129204e4f54204e554c4c206175746f5f69' +
        '6e6372656d656e742c'
      
        '20204152544e554d2076617263686172283230292064656661756c74204e554c' +
        '4c2c'
      
        '202045525341545a5f4152544e554d2076617263686172283230292064656661' +
        '756c74204e554c4c2c'
      
        '20204d41544348434f4445207661726368617228323535292064656661756c74' +
        '204e554c4c2c'
      
        '2020574152454e47525550504520696e7428313129204e4f54204e554c4c2064' +
        '656661756c74202730272c'
      
        '20205241424752505f4944207661726368617228313029204e4f54204e554c4c' +
        '2064656661756c7420272d272c'
      
        '2020424152434f44452076617263686172283230292064656661756c74204e55' +
        '4c4c2c'
      
        '2020424152434f4445322076617263686172283230292064656661756c74204e' +
        '554c4c2c'
      
        '2020424152434f4445332076617263686172283230292064656661756c74204e' +
        '554c4c2c'
      
        '2020415254494b454c5459502063686172283129204e4f54204e554c4c206465' +
        '6661756c7420274e272c'
      
        '20204b55525a4e414d452076617263686172283830292064656661756c74204e' +
        '554c4c2c'
      '20204c414e474e414d4520746578742c'
      
        '20204b41535f4e414d452076617263686172283830292064656661756c74204e' +
        '554c4c2c'
      '2020494e464f20746578742c'
      
        '20204d455f45494e484549542076617263686172283130292064656661756c74' +
        '20275374fc636b272c'
      
        '202056504520696e7428313129204e4f54204e554c4c2064656661756c742027' +
        '31272c'
      
        '202050525f45494e4845495420666c6f61742831302c3229204e4f54204e554c' +
        '4c2064656661756c742027312e3030272c'
      
        '20204c41454e47452076617263686172283230292064656661756c74204e554c' +
        '4c2c'
      
        '202047524f455353452076617263686172283230292064656661756c74204e55' +
        '4c4c2c'
      
        '202044494d454e53494f4e2076617263686172283230292064656661756c7420' +
        '4e554c4c2c'
      
        '20204745574943485420666c6f61742831302c3329204e4f54204e554c4c2064' +
        '656661756c742027302e3030272c'
      
        '2020494e56454e5455525f5745525420666c6f61742831302c3229204e4f5420' +
        '4e554c4c2064656661756c7420273130302e3030272c'
      
        '2020454b5f505245495320666c6f61742831302c3329204e4f54204e554c4c20' +
        '64656661756c742027302e303030272c'
      
        '2020564b3120666c6f61742831302c3229204e4f54204e554c4c206465666175' +
        '6c742027302e3030272c'
      
        '2020564b314220666c6f61742831302c3229204e4f54204e554c4c2064656661' +
        '756c742027302e3030272c'
      
        '2020564b3220666c6f61742831302c3229204e4f54204e554c4c206465666175' +
        '6c742027302e3030272c'
      
        '2020564b324220666c6f61742831302c3229204e4f54204e554c4c2064656661' +
        '756c742027302e3030272c'
      
        '2020564b3320666c6f61742831302c3229204e4f54204e554c4c206465666175' +
        '6c742027302e3030272c'
      
        '2020564b334220666c6f61742831302c3229204e4f54204e554c4c2064656661' +
        '756c742027302e3030272c'
      
        '2020564b3420666c6f61742831302c3229204e4f54204e554c4c206465666175' +
        '6c742027302e3030272c'
      
        '2020564b344220666c6f61742831302c3229204e4f54204e554c4c2064656661' +
        '756c742027302e3030272c'
      
        '2020564b3520666c6f61742831302c3229204e4f54204e554c4c206465666175' +
        '6c742027302e3030272c'
      
        '2020564b354220666c6f61742831302c3229204e4f54204e554c4c2064656661' +
        '756c742027302e3030272c'
      
        '20204d415852414241545420464c4f415428332c3229204e4f54204e554c4c20' +
        '44454641554c542027302e3030272c'
      
        '202050524f5649535f50524f5a20666c6f617428352c3229204e4f54204e554c' +
        '4c2064656661756c742027302e3030272c'
      
        '20205354455545525f434f44452074696e79696e74283429204e4f54204e554c' +
        '4c2064656661756c74202732272c'
      
        '2020414c545445494c5f464c414720656e756d28274e272c27592729204e4f54' +
        '204e554c4c2064656661756c7420274e272c'
      
        '20204e4f5f5241424154545f464c414720656e756d28274e272c27592729204e' +
        '4f54204e554c4c2064656661756c7420274e272c'
      
        '20204e4f5f50524f564953494f4e5f464c414720656e756d28274e272c275927' +
        '29204e4f54204e554c4c2064656661756c7420274e272c'
      
        '20204e4f5f42455a454449545f464c414720656e756d28274e272c2759272920' +
        '4e4f54204e554c4c2064656661756c7420274e272c'
      
        '20204e4f5f454b5f464c414720656e756d28274e272c27592729204e4f54204e' +
        '554c4c2064656661756c7420274e272c'
      
        '20204e4f5f564b5f464c414720656e756d28274e272c27592729204e4f54204e' +
        '554c4c2064656661756c7420274e272c'
      
        '2020534e5f464c414720656e756d28274e272c27592729204e4f54204e554c4c' +
        '2064656661756c7420274e272c'
      
        '20204155544f44454c5f464c414720656e756d28274e272c27592729204e4f54' +
        '204e554c4c2064656661756c7420274e272c'
      
        '20204b4f4d4d4953494f4e5f464c414720454e554d28274e272c275927292044' +
        '454641554c5420274e272c'
      
        '20204d454e47455f535441525420666c6f61742831302c3229204e4f54204e55' +
        '4c4c2064656661756c742027302e3030272c'
      
        '20204d454e47455f414b5420666c6f61742831302c3229204e4f54204e554c4c' +
        '2064656661756c742027302e3030272c'
      
        '20204d454e47455f4d494e20666c6f61742831302c3229204e4f54204e554c4c' +
        '2064656661756c742027302e3030272c'
      
        '20204d454e47455f42455354454c4c5420666c6f61742831302c3229204e4f54' +
        '204e554c4c2064656661756c742027302e3030272c'
      
        '20204d454e47455f42564f5220666c6f61742831302c3229204e4f54204e554c' +
        '4c2064656661756c742027302e3030272c'
      
        '20204d454e47455f454b424553545f45444920666c6f61742831302c3229204e' +
        '4f54204e554c4c2064656661756c742027302e3030272c'
      
        '20204d454e47455f564b52455f45444920666c6f61742831302c3229204e4f54' +
        '204e554c4c2064656661756c742027302e3030272c'
      
        '20204d454e47455f454b52455f45444920666c6f61742831302c3229204e4f54' +
        '204e554c4c2064656661756c742027302e3030272c'
      
        '20204c4153545f454b20666c6f61742831302c3329204e4f54204e554c4c2064' +
        '656661756c742027302e303030272c'
      
        '20204c4153545f4c49454620696e7428313129204e4f54204e554c4c20646566' +
        '61756c7420272d31272c'
      
        '20204c4153545f4c49454644415420646174652064656661756c74204e554c4c' +
        '2c'
      
        '20204c4153545f564b20666c6f61742831302c3229204e4f54204e554c4c2064' +
        '656661756c742027302e3030272c'
      
        '20204c4153545f4b554e444520696e7428313129204e4f54204e554c4c206465' +
        '6661756c7420272d31272c'
      '20204c4153545f564b44415420646174652064656661756c74204e554c4c2c'
      
        '202044454641554c545f4c4945465f494420696e7428313129204e4f54204e55' +
        '4c4c2064656661756c7420272d31272c'
      
        '202045524c4f45535f4b544f20696e74283131292064656661756c74204e554c' +
        '4c2c'
      '2020415546575f4b544f20696e74283131292064656661756c74204e554c4c2c'
      
        '20204845524b554e46534c414e4420636861722833292064656661756c74204e' +
        '554c4c2c'
      
        '20204845525354454c4c45525f494420696e7428313129204e4f54204e554c4c' +
        '2064656661756c7420272d31272c'
      
        '202048455253545f4152544e554d207661726368617228313030292064656661' +
        '756c74204e554c4c2c'
      
        '20204c414745524f52542076617263686172283230292064656661756c74204e' +
        '554c4c2c'
      '202045525354454c4c5420646174652064656661756c74204e554c4c2c'
      
        '2020455253545f4e414d452076617263686172283230292064656661756c7420' +
        '4e554c4c2c'
      '2020474541454e4420646174652064656661756c74204e554c4c2c'
      
        '2020474541454e445f4e414d452076617263686172283230292064656661756c' +
        '74204e554c4c2c'
      
        '202053484f505f49442074696e79696e74283429204e4f54204e554c4c206465' +
        '6661756c7420272d31272c'
      
        '202053484f505f415254494b454c5f494420696e7428313129204e4f54204e55' +
        '4c4c2064656661756c7420272d31272c'
      '202053484f505f4b55525a5445585420746578742c'
      '202053484f505f4c414e475445585420746578742c'
      
        '202053484f505f494d414745207661726368617228313030292064656661756c' +
        '74204e554c4c2c'
      
        '202053484f505f494d4147455f4d454420564152434841522831303029206465' +
        '6661756c74204e554c4c2c'
      
        '202053484f505f494d4147455f4c415247452076617263686172283130302920' +
        '64656661756c74204e554c4c2c'
      
        '202053484f505f444154454e424c415454207661726368617228313030292064' +
        '656661756c74204e554c4c2c'
      
        '202053484f505f4b4154414c4f47207661726368617228313030292064656661' +
        '756c74204e554c4c2c'
      
        '202053484f505f5a454943484e554e4720766172636861722831303029206465' +
        '6661756c74204e554c4c2c'
      
        '202053484f505f48414e44425543482076617263686172283130302920646566' +
        '61756c74204e554c4c2c'
      
        '202041555353434852454942554e475354455854207661726368617228313030' +
        '292064656661756c74204e554c4c2c'
      
        '202053484f505f50524549535f4c4953544520666c6f61742831322c38292064' +
        '656661756c742027302e3030303030303030272c'
      
        '202053484f505f56495349424c4520696e742831292064656661756c74202731' +
        '272c'
      
        '202053484f505f444154455f4e455520646174652064656661756c74204e554c' +
        '4c2c'
      
        '202053484f505f4641454c4c545f5745475f414220646174652064656661756c' +
        '74204e554c4c2c'
      
        '202053484f505f434c49434b5f434f554e5420696e7428313129206465666175' +
        '6c74202730272c'
      
        '202053484f505f53594e432074696e79696e7428312920756e7369676e656420' +
        '64656661756c74202730272c'
      
        '202053484f505f5a55422074696e79696e7428312920756e7369676e65642064' +
        '656661756c74202730272c'
      
        '202053484f505f4348414e47455f44415445206461746574696d652064656661' +
        '756c74204e554c4c2c'
      
        '202053484f505f4348414e47455f464c41472074696e79696e7428312920756e' +
        '7369676e6564204e4f54204e554c4c2064656661756c74202730272c'
      
        '202053484f505f44454c5f464c414720656e756d28274e272c27592729204e4f' +
        '54204e554c4c2064656661756c7420274e272c'
      '202053484f505f50415353574f52442056415243484152283230292c'
      
        '20205553455246454c445f303120766172636861722832353529206465666175' +
        '6c74204e554c4c2c'
      
        '20205553455246454c445f303220766172636861722832353529206465666175' +
        '6c74204e554c4c2c'
      
        '20205553455246454c445f303320766172636861722832353529206465666175' +
        '6c74204e554c4c2c'
      
        '20205553455246454c445f303420766172636861722832353529206465666175' +
        '6c74204e554c4c2c'
      
        '20205553455246454c445f303520766172636861722832353529206465666175' +
        '6c74204e554c4c2c'
      
        '20205553455246454c445f303620766172636861722832353529206465666175' +
        '6c74204e554c4c2c'
      
        '20205553455246454c445f303720766172636861722832353529206465666175' +
        '6c74204e554c4c2c'
      
        '20205553455246454c445f303820766172636861722832353529206465666175' +
        '6c74204e554c4c2c'
      
        '20205553455246454c445f303920766172636861722832353529206465666175' +
        '6c74204e554c4c2c'
      
        '20205553455246454c445f313020766172636861722832353529206465666175' +
        '6c74204e554c4c2c'
      '20205052494d415259204b45592020285245435f4944292c'
      '20204b4559204944585f5747522028574152454e475255505045292c'
      '20204b4559204944585f4d41544348434f444520284d41544348434f4445292c'
      '20204b4559204944585f424152434f44452028424152434f4445292c'
      '20204b4559204944585f4152544e554d20284152544e554d292c'
      
        '20204b4559204944585f48455253545f4152544e554d202848455253545f4152' +
        '544e554d29'
      '293b'
      ''
      '435245415445205441424c4520415254494b454c5f494e56454e5455522028'
      
        '2020494e56454e5455525f494420696e7428352920756e7369676e6564204e4f' +
        '54204e554c4c2064656661756c74202730272c'
      
        '2020415254494b454c5f494420696e742831312920756e7369676e6564204e4f' +
        '54204e554c4c2064656661756c74202730272c'
      
        '2020574152454e47525550504520696e742831312920756e7369676e65642064' +
        '656661756c74202730272c'
      
        '20204152544e554d207661726368617228323530292064656661756c74204e55' +
        '4c4c2c'
      
        '20204d41544348434f4445207661726368617228323530292064656661756c74' +
        '204e554c4c2c'
      
        '2020424152434f4445207661726368617228323530292064656661756c74204e' +
        '554c4c2c'
      
        '20204b55525a54455854207661726368617228323530292064656661756c7420' +
        '4e554c4c2c'
      
        '20204d454e47455f49535420666c6f61742831302c3329204e4f54204e554c4c' +
        '2064656661756c742027302e303030272c'
      
        '20204d454e47455f534f4c4c20666c6f61742831302c3329204e4f54204e554c' +
        '4c2064656661756c742027302e303030272c'
      
        '20204d454e47455f4449464620666c6f61742831302c3329204e4f54204e554c' +
        '4c2064656661756c742027302e303030272c'
      
        '2020494e56454e5455525f5745525420666c6f61742831302c32292064656661' +
        '756c742027302e3030272c'
      
        '2020454b5f505245495320666c6f61742831302c33292064656661756c742027' +
        '302e303030272c'
      
        '20205354415455532074696e79696e7428312920756e7369676e6564204e4f54' +
        '204e554c4c2064656661756c74202730272c'
      
        '2020424541524245495445544552207661726368617228353029206465666175' +
        '6c74204e554c4c2c'
      
        '20205052494d415259204b4559202028494e56454e5455525f49442c41525449' +
        '4b454c5f494429'
      
        '2920434f4d4d454e543d27417274696b656c646174656e2066fc7220496e7665' +
        '6e747572273b'
      ''
      '435245415445205441424c4520415254494b454c5f4b41542028'
      
        '202053484f505f49442074696e79696e74283329204e4f54204e554c4c206465' +
        '6661756c7420272d31272c'
      
        '2020494420696e7428313129204e4f54204e554c4c206175746f5f696e637265' +
        '6d656e742c'
      
        '2020544f505f494420696e7428313129204e4f54204e554c4c2064656661756c' +
        '7420272d31272c'
      
        '2020534f52545f4e554d20696e7428332920756e7369676e6564204e4f54204e' +
        '554c4c2064656661756c74202730272c'
      
        '20204e414d4520766172636861722832353029204e4f54204e554c4c20646566' +
        '61756c742027272c'
      '202055524c207661726368617228323530292064656661756c74204e554c4c2c'
      '2020424553434852454942554e4720746578742c'
      
        '2020494d414745207661726368617228323530292064656661756c74204e554c' +
        '4c2c'
      
        '202056495349424c455f464c414720656e756d28274e272c27592729204e4f54' +
        '204e554c4c2064656661756c74202759272c'
      '20204c4945465f494420696e74283133292064656661756c7420272d31272c'
      
        '2020415254494b454c5f54454d504c4154452076617263686172283235302920' +
        '64656661756c7420276e6f726d616c2e74706c272c'
      
        '20204752555050454e5f54454d504c4154452076617263686172283235302920' +
        '64656661756c7420276772705f6e6f726d616c2e74706c272c'
      
        '2020434c49434b5f434f554e5420696e74283131292064656661756c74202730' +
        '272c'
      
        '20204c4153545f49502076617263686172283136292064656661756c74204e55' +
        '4c4c2c'
      
        '202053594e435f464c414720656e756d28274e272c27592729204e4f54204e55' +
        '4c4c2064656661756c7420274e272c'
      
        '20204348414e47455f464c414720656e756d28274e272c27592729204e4f5420' +
        '4e554c4c2064656661756c74202759272c'
      
        '202044454c5f464c414720656e756d28274e272c27592729204e4f54204e554c' +
        '4c2064656661756c7420274e272c'
      '20205052494d415259204b455920202853484f505f49442c494429'
      '293b'
      ''
      '435245415445205441424c4520415254494b454c5f4c544558542028'
      
        '2020415254494b454c5f494420696e7428313129204e4f54204e554c4c206465' +
        '6661756c74202730272c'
      
        '2020535052414348455f494420696e7428313129204e4f54204e554c4c206465' +
        '6661756c74202730272c'
      
        '20204b55525a4e414d452076617263686172283830292064656661756c74204e' +
        '554c4c2c'
      '20204c414e474e414d4520746578742c'
      
        '20204b41535f4e414d452076617263686172283830292064656661756c74204e' +
        '554c4c2c'
      '202053484f505f4b55525a5445585420746578742c'
      '202053484f505f4c414e475445585420746578742c'
      
        '202053484f505f444154454e424c415454207661726368617228313030292064' +
        '656661756c74204e554c4c2c'
      
        '202053484f505f4b4154414c4f47207661726368617228313030292064656661' +
        '756c74204e554c4c2c'
      
        '202053484f505f5a454943484e554e4720766172636861722831303029206465' +
        '6661756c74204e554c4c2c'
      
        '202053484f505f48414e44425543482076617263686172283130302920646566' +
        '61756c74204e554c4c2c'
      
        '20204348414e47455f464c41472074696e79696e7428312920756e7369676e65' +
        '64204e4f54204e554c4c2064656661756c74202730272c'
      
        '20205052494d415259204b4559202028415254494b454c5f49442c5350524143' +
        '48455f4944292c'
      
        '20204b4559204944585f415254494b454c5f49442028415254494b454c5f4944' +
        '292c'
      
        '20204b4559204944585f535052414348455f49442028535052414348455f4944' +
        '292c'
      
        '20204b4559204944585f415254494b454c5f535052414348452028415254494b' +
        '454c5f49442c535052414348455f494429'
      '293b'
      ''
      ''
      '435245415445205441424c4520415254494b454c5f4d45524b2028'
      
        '20204d45524b4d414c5f494420696e7428313129204e4f54204e554c4c206175' +
        '746f5f696e6372656d656e742c'
      
        '20204e414d4520766172636861722831303029204e4f54204e554c4c20646566' +
        '61756c742027272c'
      '20205052494d415259204b45592020284d45524b4d414c5f494429'
      '293b'
      ''
      '435245415445205441424c4520415254494b454c5f50524549532028'
      
        '2020415254494b454c5f494420696e7428313129204e4f54204e554c4c206465' +
        '6661756c7420272d31272c'
      
        '20204144524553535f494420696e7428313129204e4f54204e554c4c20646566' +
        '61756c7420272d31272c'
      
        '202050524549535f5459502074696e79696e7428322920756e7369676e656420' +
        '4e4f54204e554c4c2064656661756c74202730272c'
      
        '2020424553544e554d2076617263686172283530292064656661756c74204e55' +
        '4c4c2c'
      
        '2020505245495320666c6f61742831302c3329204e4f54204e554c4c20646566' +
        '61756c742027302e303030272c'
      
        '20204d454e47453220696e7428362920756e7369676e6564204e4f54204e554c' +
        '4c2064656661756c74202730272c'
      
        '202050524549533220666c6f61742831302c3329204e4f54204e554c4c206465' +
        '6661756c742027302e303030272c'
      
        '20204d454e47453320696e7428362920756e7369676e6564204e4f54204e554c' +
        '4c2064656661756c74202730272c'
      
        '202050524549533320666c6f61742831302c3329204e4f54204e554c4c206465' +
        '6661756c742027302e303030272c'
      
        '20204d454e47453420696e7428362920756e7369676e6564204e4f54204e554c' +
        '4c2064656661756c74202730272c'
      
        '202050524549533420666c6f61742831302c3329204e4f54204e554c4c206465' +
        '6661756c742027302e303030272c'
      
        '20204d454e47453520696e7428362920756e7369676e6564204e4f54204e554c' +
        '4c2064656661756c74202730272c'
      
        '202050524549533520666c6f61742831302c3329204e4f54204e554c4c206465' +
        '6661756c742027302e303030272c'
      '20204755454c5449475f564f4e20646174652064656661756c74204e554c4c2c'
      '20204755454c5449475f42495320646174652064656661756c74204e554c4c2c'
      '2020494e464f20746578742c'
      '2020474541454e442074696d657374616d7028313429204e4f54204e554c4c2c'
      
        '2020474541454e445f4e414d452076617263686172283230292064656661756c' +
        '74204e554c4c2c'
      
        '20205052494d415259204b4559202028415254494b454c5f49442c4144524553' +
        '535f49442c50524549535f54595029'
      '293b'
      ''
      ''
      '435245415445205441424c4520415254494b454c5f5345524e554d2028'
      
        '2020415254494b454c5f494420696e7428313129204e4f54204e554c4c206465' +
        '6661756c74202730272c'
      
        '20205345524e554d4d455220766172636861722832353529204e4f54204e554c' +
        '4c2064656661756c742027272c'
      
        '202045494e4b5f4e554d20696e7428313129204e4f54204e554c4c2064656661' +
        '756c7420272d31272c'
      
        '20204c4945465f4e554d20696e7428313129204e4f54204e554c4c2064656661' +
        '756c7420272d31272c'
      
        '20205645524b5f4e554d20696e7428313129204e4f54204e554c4c2064656661' +
        '756c7420272d31272c'
      
        '2020534e554d5f494420696e7428313129204e4f54204e554c4c206175746f5f' +
        '696e6372656d656e742c'
      
        '2020454b5f4a4f55524e414c5f494420696e7428313029204e4f54204e554c4c' +
        '2064656661756c7420272d31272c'
      
        '2020564b5f4a4f55524e414c5f494420696e7428313029204e4f54204e554c4c' +
        '2064656661756c7420272d31272c'
      
        '20204c535f4a4f55524e414c5f494420696e7428313029204e4f54204e554c4c' +
        '2064656661756c7420272d31272c'
      
        '2020454b5f4a4f55524e414c504f535f494420696e7428313029204e4f54204e' +
        '554c4c2064656661756c7420272d31272c'
      
        '2020564b5f4a4f55524e414c504f535f494420696e7428313029204e4f54204e' +
        '554c4c2064656661756c7420272d31272c'
      
        '20204c535f4a4f55524e414c504f535f494420696e7428313029204e4f54204e' +
        '554c4c2064656661756c7420272d31272c'
      '20205052494d415259204b4559202028534e554d5f4944292c'
      
        '2020554e49515545204b4559205345524e554d4d455220285345524e554d4d45' +
        '522c415254494b454c5f494429'
      '293b'
      ''
      ''
      '435245415445205441424c4520415254494b454c5f53484f502028'
      
        '2020494420696e7428313129204e4f54204e554c4c206175746f5f696e637265' +
        '6d656e742c'
      
        '2020544f505f494420696e7428313129204e4f54204e554c4c2064656661756c' +
        '7420273939393939272c'
      
        '20204152545f4e522076617263686172283530292064656661756c74204e554c' +
        '4c2c'
      
        '20204152545f4e414d452076617263686172283530292064656661756c74204e' +
        '554c4c2c'
      '20204b55525a5445585420746578742c'
      '20204c414e475445585420746578742c'
      '2020424553434852454942554e4720746578742c'
      '20205350455a4946494b4154494f4e20746578742c'
      '20205a554245484f455220746578742c'
      '20205a554245484f45525f4f505420746578742c'
      
        '2020494d4147455f424947207661726368617228313030292064656661756c74' +
        '204e554c4c2c'
      
        '2020494d4147455f534d414c4c20766172636861722831303029206465666175' +
        '6c74204e554c4c2c'
      
        '20204441544153484545545f4420766172636861722831303029206465666175' +
        '6c74204e554c4c2c'
      
        '20204441544153484545545f4520766172636861722831303029206465666175' +
        '6c74204e554c4c2c'
      
        '20204b4154414c4f475f44207661726368617228313030292064656661756c74' +
        '204e554c4c2c'
      
        '20204b4154414c4f475f45207661726368617228313030292064656661756c74' +
        '204e554c4c2c'
      
        '202044524157494e47207661726368617228313030292064656661756c74204e' +
        '554c4c2c'
      
        '202048414e44425543485f44207661726368617228313030292064656661756c' +
        '74204e554c4c2c'
      
        '202048414e44425543485f45207661726368617228313030292064656661756c' +
        '74204e554c4c2c'
      
        '202041555353434852454942554e475354455854207661726368617228313030' +
        '292064656661756c74204e554c4c2c'
      
        '202050524549535f454b20666c6f61742831322c32292064656661756c74204e' +
        '554c4c2c'
      
        '202050524549535f4c4953544520666c6f61742831322c32292064656661756c' +
        '74204e554c4c2c'
      
        '202050524549535f5350454320666c6f61742831322c32292064656661756c74' +
        '204e554c4c2c'
      
        '2020454b5f52414241545420666c6f617428352c32292064656661756c74204e' +
        '554c4c2c'
      '202056495349424c4520696e742831292064656661756c74202731272c'
      '2020444154455f4e455520646174652064656661756c74204e554c4c2c'
      '20204641454c4c545f57454720696e742831292064656661756c74202730272c'
      
        '20204641454c4c545f574552475f414220646174652064656661756c74204e55' +
        '4c4c2c'
      
        '2020434c49434b5f434f554e5420696e74283131292064656661756c74202730' +
        '272c'
      
        '202053594e432074696e79696e7428312920756e7369676e6564206465666175' +
        '6c74202730272c'
      
        '20204c4153545f49502076617263686172283136292064656661756c74204e55' +
        '4c4c2c'
      
        '20205a55422074696e79696e7428312920756e7369676e65642064656661756c' +
        '74202730272c'
      
        '20204348414e47455f44415445206461746574696d652064656661756c74204e' +
        '554c4c2c'
      
        '20204348414e47455f464c41472074696e79696e7428312920756e7369676e65' +
        '64204e4f54204e554c4c2064656661756c74202730272c'
      '20204c4945465f494420696e74283133292064656661756c7420272d31272c'
      
        '20205241424752505f49442076617263686172283130292064656661756c7420' +
        '4e554c4c2c'
      '20205052494d415259204b4559202028494429'
      '293b'
      ''
      '435245415445205441424c4520415254494b454c5f53484f505f4944582028'
      
        '2020494420696e7428313129204e4f54204e554c4c206175746f5f696e637265' +
        '6d656e742c'
      
        '2020544f505f494420696e7428313129204e4f54204e554c4c2064656661756c' +
        '7420273939393939272c'
      
        '20204e414d45207661726368617228323530292064656661756c74204e554c4c' +
        '2c'
      '202055524c207661726368617228323530292064656661756c74204e554c4c2c'
      
        '2020415254494b454c5f54454d504c4154452076617263686172283235302920' +
        '64656661756c7420276e6f726d616c2e74706c272c'
      
        '2020434c49434b5f434f554e5420696e74283131292064656661756c74202730' +
        '272c'
      
        '202056495349424c452074696e79696e7428312920756e7369676e6564206465' +
        '6661756c74202731272c'
      
        '20204752555050454e5f54454d504c4154452076617263686172283235302920' +
        '64656661756c7420276772705f6e6f726d616c2e74706c272c'
      '2020424553434852454942554e4720746578742c'
      
        '2020494d414745207661726368617228323530292064656661756c74204e554c' +
        '4c2c'
      
        '20204c4153545f49502076617263686172283136292064656661756c74204e55' +
        '4c4c2c'
      
        '202053594e432074696e79696e7428312920756e7369676e6564206465666175' +
        '6c74202730272c'
      '20204c4945465f494420696e74283133292064656661756c7420272d31272c'
      '20205052494d415259204b4559202028494429'
      '293b'
      ''
      
        '435245415445205441424c4520415254494b454c5f53545545434b4c49535420' +
        '28'
      
        '20205245435f494420696e7428313129204e4f54204e554c4c2064656661756c' +
        '74202730272c'
      
        '20204152545f494420696e7428313129204e4f54204e554c4c2064656661756c' +
        '74202730272c'
      
        '2020415254494b454c5f41525420454e554d282253544c222c22534554222c22' +
        '5a5542222c2245525322292044454641554c54202253544c22204e4f54204e55' +
        '4c4c2c'
      '20204d454e474520666c6f617428352c32292064656661756c74204e554c4c2c'
      '202045525354454c4c5420646174652064656661756c74204e554c4c2c'
      
        '2020455253545f4e414d452076617263686172283230292064656661756c7420' +
        '4e554c4c2c'
      '2020474541454e4420646174652064656661756c74204e554c4c2c'
      
        '2020474541454e445f4e414d452076617263686172283230292064656661756c' +
        '74204e554c4c2c'
      '20205052494d415259204b45592020285245435f49442c4152545f494429'
      '293b'
      ''
      '435245415445205441424c4520415254494b454c5f544f5f4b41542028'
      
        '202053484f505f49442074696e79696e74283329204e4f54204e554c4c206465' +
        '6661756c7420272d31272c'
      
        '2020415254494b454c5f494420696e7428313329204e4f54204e554c4c206465' +
        '6661756c7420272d31272c'
      
        '20204b41545f494420696e7428313329204e4f54204e554c4c2064656661756c' +
        '7420272d31272c'
      
        '20204348414e47455f464c414720656e756d28274e272c27592729204e4f5420' +
        '4e554c4c2064656661756c74202759272c'
      
        '202044454c5f464c414720656e756d28274e272c27592729204e4f54204e554c' +
        '4c2064656661756c7420274e272c'
      
        '20205052494d415259204b455920202853484f505f49442c415254494b454c5f' +
        '49442c4b41545f4944292c'
      
        '20204b4559204b41545f544f5f415254494b454c202853484f505f49442c4b41' +
        '545f49442c415254494b454c5f494429'
      '293b'
      ''
      '435245415445205441424c4520415254494b454c5f544f5f4d45524b2028'
      
        '20204d45524b4d414c5f494420696e742831312920756e7369676e6564204e4f' +
        '54204e554c4c2064656661756c74202730272c'
      
        '2020415254494b454c5f494420696e742831312920756e7369676e6564204e4f' +
        '54204e554c4c2064656661756c74202730272c'
      
        '20205052494d415259204b45592020284d45524b4d414c5f49442c415254494b' +
        '454c5f494429'
      '293b'
      ''
      '435245415445205441424c4520424c5a2028'
      
        '20204c414e442056415243484152283529202044454641554c54202244452220' +
        '4e4f54204e554c4c2c'
      
        '2020424c5a20696e7428313029204e4f54204e554c4c2064656661756c742027' +
        '30272c'
      
        '202042414e4b5f4e414d4520766172636861722832353529204e4f54204e554c' +
        '4c2064656661756c742027272c'
      
        '202050525a2063686172283229204e4f54204e554c4c2064656661756c742027' +
        '272c'
      '20205052494d415259204b45592020284c414e442c424c5a292c'
      
        '20204b4559204944585f4e414d4520284c414e442c42414e4b5f4e414d452c42' +
        '4c5a29'
      '293b'
      ''
      '435245415445205441424c45204558504f52542028'
      
        '2020494420696e7428313129204e4f54204e554c4c206175746f5f696e637265' +
        '6d656e742c'
      
        '20204b55525a42455a20766172636861722832353529204e4f54204e554c4c20' +
        '64656661756c742027272c'
      '2020494e464f2074657874204e4f54204e554c4c2c'
      '202051554552592074657874204e4f54204e554c4c2c'
      '202046454c44455220746578742c'
      '2020464f524d554c415220626c6f622c'
      '2020464f524d415420636861722833292064656661756c74204e554c4c2c'
      
        '202046494c454e414d45207661726368617228323535292064656661756c7420' +
        '4e554c4c2c'
      
        '20204c4153545f4348414e47452074696d657374616d7028313429204e4f5420' +
        '4e554c4c2c'
      
        '20204348414e47455f4e414d4520766172636861722831303029206465666175' +
        '6c74204e554c4c2c'
      '20205052494d415259204b4559202028494429'
      '293b'
      ''
      '435245415445205441424c4520464942555f42554348554e47454e2028'
      
        '20204c46445f4e5220696e7428313129204e4f54204e554c4c206175746f5f69' +
        '6e6372656d656e742c'
      
        '20204a524e5f4e554d20696e7428313129204e4f54204e554c4c206465666175' +
        '6c74202730272c'
      
        '20204b544f5f4e5220696e7428313129204e4f54204e554c4c2064656661756c' +
        '74202730272c'
      
        '20204745475f4b544f20696e7428313129204e4f54204e554c4c206465666175' +
        '6c74202730272c'
      
        '2020444154554d2064617465204e4f54204e554c4c2064656661756c74202730' +
        '3030302d30302d3030272c'
      
        '202042455452414720646f75626c65204e4f54204e554c4c2064656661756c74' +
        '202730272c'
      
        '20204d54474c5f4e5220696e7428313129204e4f54204e554c4c206465666175' +
        '6c74202730272c'
      
        '202042454c4e554d207661726368617228323029204e4f54204e554c4c206465' +
        '6661756c742027272c'
      
        '20205741454852554e472074696e79696e742834292064656661756c74204e55' +
        '4c4c2c'
      
        '20204255434854455854207661726368617228313030292064656661756c7420' +
        '4e554c4c2c'
      
        '202055534552207661726368617228323529204e4f54204e554c4c2064656661' +
        '756c742027272c'
      
        '20205052494d415259204b45592020284c46445f4e522c4a524e5f4e554d2c4b' +
        '544f5f4e522c4745475f4b544f2c444154554d2c4245545241472c4d54474c5f' +
        '4e522c42454c4e554d292c'
      '20204b4559204a524e5f4e554d20284a524e5f4e554d292c'
      '20204b455920444154554d2028444154554d292c'
      '20204b45592055534552202855534552292c'
      '20204b4559204b4f4e544f20284b544f5f4e52292c'
      '20204b45592042454c4e554d202842454c4e554d29'
      '293b'
      ''
      '435245415445205441424c4520464942555f4a4f55524e414c2028'
      
        '20204c46445f4e5220696e7428313129204e4f54204e554c4c206175746f5f69' +
        '6e6372656d656e742c'
      
        '2020444154554d2064617465204e4f54204e554c4c2064656661756c74202730' +
        '3030302d30302d3030272c'
      
        '20204255444154554d2064617465204e4f54204e554c4c2064656661756c7420' +
        '27303030302d30302d3030272c'
      
        '202042454c4e554d207661726368617228323029204e4f54204e554c4c206465' +
        '6661756c742027272c'
      
        '2020534b544f20696e7428313129204e4f54204e554c4c2064656661756c7420' +
        '2730272c'
      
        '2020484b544f20696e7428313129204e4f54204e554c4c2064656661756c7420' +
        '2730272c'
      
        '202042455452414720666c6f61742831302c32292064656661756c74204e554c' +
        '4c2c'
      
        '20205741454852554e472074696e79696e742834292064656661756c74204e55' +
        '4c4c2c'
      '2020545854207661726368617228323030292064656661756c74204e554c4c2c'
      
        '202055534552207661726368617228323029204e4f54204e554c4c2064656661' +
        '756c742027272c'
      
        '20205052494d415259204b45592020284c46445f4e522c444154554d2c425544' +
        '4154554d2c42454c4e554d2c534b544f2c484b544f292c'
      '20204b45592055534552202855534552292c'
      '20204b4559204255444154554d20284255444154554d292c'
      '20204b455920444154554d2028444154554d29'
      '293b'
      ''
      '435245415445205441424c4520464942555f4b415353452028'
      
        '20205245435f494420696e7428313129204e4f54204e554c4c206175746f5f69' +
        '6e6372656d656e742c'
      
        '20204a41485220696e7428313129204e4f54204e554c4c2064656661756c7420' +
        '2730272c'
      
        '202042444154554d2064617465204e4f54204e554c4c2064656661756c742027' +
        '303030302d30302d3030272c'
      
        '202042454c45474e554d2076617263686172283130292064656661756c74204e' +
        '554c4c2c'
      
        '20205155454c4c4520696e7428313129204e4f54204e554c4c2064656661756c' +
        '74202730272c'
      
        '20204a4f55524e414c5f494420696e74283131292064656661756c74204e554c' +
        '4c2c'
      '2020474b4f4e544f20696e74283131292064656661756c7420272d31272c'
      
        '2020534b4f4e544f20666c6f617428352c33292064656661756c742027302e30' +
        '3030272c'
      
        '20205a555f414247414e4720666c6f61742831322c3229204e4f54204e554c4c' +
        '2064656661756c742027302e3030272c'
      '20204254585420746578742c'
      
        '20205052494d415259204b45592020284a4148522c42444154554d2c5245435f' +
        '494429'
      '293b'
      ''
      '435245415445205441424c4520464942555f4b4f4e54454e2028'
      
        '20204b544f5f4e5220696e7428313129204e4f54204e554c4c2064656661756c' +
        '74202730272c'
      
        '20204b544f5f4e414d4520766172636861722831303029204e4f54204e554c4c' +
        '2064656661756c742027272c'
      
        '20204d54474c5f4e5220696e7428313129204e4f54204e554c4c206465666175' +
        '6c74202730272c'
      
        '20204b544f5f4b4c4153534520696e74283131292064656661756c74204e554c' +
        '4c2c'
      
        '20205a455f564945572074696e79696e742834292064656661756c74204e554c' +
        '4c2c'
      
        '20205a415f564945572074696e79696e742834292064656661756c74204e554c' +
        '4c2c'
      
        '2020454b5f564945572074696e79696e742834292064656661756c74204e554c' +
        '4c2c'
      
        '2020564b5f564945572074696e79696e742834292064656661756c74204e554c' +
        '4c2c'
      
        '202046495f564945572074696e79696e742834292064656661756c74204e554c' +
        '4c2c'
      
        '202053414c444f20666c6f61742831302c32292064656661756c74204e554c4c' +
        '2c'
      
        '20205052494d415259204b45592020284b544f5f4e522c4b544f5f4e414d452c' +
        '4d54474c5f4e52292c'
      '20204b4559204b544f20284b544f5f4e52292c'
      '20204b4559204e414d4520284b544f5f4e414d4529'
      '293b'
      ''
      '435245415445205441424c4520464942555f4f504f532028'
      
        '20204c46445f4e5220696e7428313129204e4f54204e554c4c206175746f5f69' +
        '6e6372656d656e742c'
      
        '20204d54474c5f4e5220696e7428313129204e4f54204e554c4c206465666175' +
        '6c74202730272c'
      
        '202053444154554d2064617465204e4f54204e554c4c2064656661756c742027' +
        '303030302d30302d3030272c'
      
        '202048444154554d2064617465204e4f54204e554c4c2064656661756c742027' +
        '303030302d30302d3030272c'
      
        '20205a41484c4152542074696e79696e74283429204e4f54204e554c4c206465' +
        '6661756c74202730272c'
      
        '20205342455452414720666c6f61742831302c3229204e4f54204e554c4c2064' +
        '656661756c742027302e3030272c'
      
        '20204842455452414720666c6f61742831302c3229204e4f54204e554c4c2064' +
        '656661756c742027302e3030272c'
      '202045524c45442074696e79696e742834292064656661756c74204e554c4c2c'
      
        '20205741454852554e472074696e79696e742834292064656661756c74204e55' +
        '4c4c2c'
      '2020545854207661726368617228313030292064656661756c74204e554c4c2c'
      
        '2020555345524e414d452076617263686172283230292064656661756c74204e' +
        '554c4c2c'
      
        '20205052494d415259204b45592020284c46445f4e522c4d54474c5f4e522c53' +
        '444154554d2c48444154554d2c5a41484c4152542c534245545241472c484245' +
        '54524147292c'
      '20204b4559204d54474c5f4e5220284d54474c5f4e5229'
      '293b'
      ''
      '435245415445205441424c45204649524d412028'
      
        '2020414e52454445207661726368617228323530292064656661756c74204e55' +
        '4c4c2c'
      
        '20204e414d4531207661726368617228323530292064656661756c74204e554c' +
        '4c2c'
      
        '20204e414d4532207661726368617228323530292064656661756c74204e554c' +
        '4c2c'
      
        '20204e414d4533207661726368617228323530292064656661756c74204e554c' +
        '4c2c'
      
        '202053545241535345207661726368617228323530292064656661756c74204e' +
        '554c4c2c'
      '20204c414e442076617263686172283130292064656661756c74204e554c4c2c'
      '2020504c5a2076617263686172283130292064656661756c74204e554c4c2c'
      '20204f5254207661726368617228323530292064656661756c74204e554c4c2c'
      
        '2020564f525741484c207661726368617228323530292064656661756c74204e' +
        '554c4c2c'
      
        '202054454c45464f4e31207661726368617228323530292064656661756c7420' +
        '4e554c4c2c'
      
        '202054454c45464f4e32207661726368617228323530292064656661756c7420' +
        '4e554c4c2c'
      
        '20204d4f42494c46554e4b207661726368617228323530292064656661756c74' +
        '204e554c4c2c'
      '2020464158207661726368617228323530292064656661756c74204e554c4c2c'
      
        '2020454d41494c207661726368617228323530292064656661756c74204e554c' +
        '4c2c'
      
        '20205745425345495445207661726368617228323530292064656661756c7420' +
        '4e554c4c2c'
      
        '202042414e4b315f424c5a20766172636861722838292064656661756c74204e' +
        '554c4c2c'
      
        '202042414e4b315f4b4f4e544f4e522076617263686172283132292064656661' +
        '756c74204e554c4c2c'
      
        '202042414e4b315f4e414d45207661726368617228323530292064656661756c' +
        '74204e554c4c2c'
      
        '202042414e4b315f4942414e207661726368617228313030292064656661756c' +
        '74204e554c4c2c'
      
        '202042414e4b315f535749465420766172636861722831303029206465666175' +
        '6c74204e554c4c2c'
      
        '202042414e4b325f424c5a20766172636861722838292064656661756c74204e' +
        '554c4c2c'
      
        '202042414e4b325f4b4f4e544f4e522076617263686172283132292064656661' +
        '756c74204e554c4c2c'
      
        '202042414e4b325f4e414d45207661726368617228323530292064656661756c' +
        '74204e554c4c2c'
      
        '202042414e4b325f4942414e207661726368617228313030292064656661756c' +
        '74204e554c4c2c'
      
        '202042414e4b325f535749465420766172636861722831303029206465666175' +
        '6c74204e554c4c2c'
      '20204b4f50465445585420746578742c'
      '2020465553535445585420746578742c'
      
        '2020414253454e444552207661726368617228323530292064656661756c7420' +
        '4e554c4c2c'
      
        '20205354455545524e554d4d4552207661726368617228323529206465666175' +
        '6c74204e554c4c2c'
      
        '20205553545f49442076617263686172283235292064656661756c74204e554c' +
        '4c2c'
      '2020494d4147453120626c6f622c'
      '2020494d4147453220626c6f622c'
      '2020494d4147453320626c6f62'
      
        '2920434f4d4d454e543d27416c6c67656d65696e65204669726d656e64617465' +
        '6e2066fc7220466f726d756c617265273b'
      ''
      '435245415445205441424c45204845525354454c4c45522028'
      
        '202053484f505f49442074696e79696e74283329204e4f54204e554c4c206465' +
        '6661756c7420272d31272c'
      
        '20204845525354454c4c45525f494420696e7428313129204e4f54204e554c4c' +
        '206175746f5f696e6372656d656e742c'
      
        '2020414444525f494420696e7428313129204e4f54204e554c4c206465666175' +
        '6c7420272d31272c'
      
        '20204845525354454c4c45525f4e414d45207661726368617228333229204e4f' +
        '54204e554c4c2064656661756c742027272c'
      
        '20204845525354454c4c45525f494d4147452076617263686172283634292064' +
        '656661756c74204e554c4c2c'
      
        '20204c4153545f4348414e4745206461746574696d652064656661756c74204e' +
        '554c4c2c'
      
        '202053484f505f444154455f4144444544206461746574696d65206465666175' +
        '6c74204e554c4c2c'
      
        '202053484f505f444154455f4348414e4745206461746574696d652064656661' +
        '756c74204e554c4c2c'
      
        '202053594e435f464c414720656e756d28274e272c27592729204e4f54204e55' +
        '4c4c2064656661756c7420274e272c'
      
        '20204348414e47455f464c414720656e756d28274e272c27592729204e4f5420' +
        '4e554c4c2064656661756c7420274e272c'
      
        '202044454c5f464c414720656e756d28274e272c27592729204e4f54204e554c' +
        '4c2064656661756c7420274e272c'
      
        '20205052494d415259204b455920202853484f505f49442c4845525354454c4c' +
        '45525f4944292c'
      
        '20204b4559204944585f4845525354454c4c45525f4e414d4520284845525354' +
        '454c4c45525f4e414d4529'
      '293b'
      ''
      '435245415445205441424c45204845525354454c4c45525f494e464f2028'
      
        '202053484f505f49442074696e79696e74283329204e4f54204e554c4c206465' +
        '6661756c7420272d31272c'
      
        '20204845525354454c4c45525f494420696e7428313129204e4f54204e554c4c' +
        '2064656661756c74202730272c'
      
        '2020535052414348455f494420696e7428313129204e4f54204e554c4c206465' +
        '6661756c74202730272c'
      
        '20204845525354454c4c45525f55524c20766172636861722832353529204e4f' +
        '54204e554c4c2064656661756c742027272c'
      
        '202055524c5f434c49434b454420696e74283529204e4f54204e554c4c206465' +
        '6661756c74202730272c'
      
        '2020444154455f4c4153545f434c49434b206461746574696d65206465666175' +
        '6c74204e554c4c2c'
      
        '20205052494d415259204b455920202853484f505f49442c4845525354454c4c' +
        '45525f49442c535052414348455f494429'
      '293b'
      ''
      '435245415445205441424c4520494e464f2028'
      
        '20204c46445f4e5220696e7428313129204e4f54204e554c4c206175746f5f69' +
        '6e6372656d656e742c'
      
        '20205155454c4c452074696e79696e74283429204e4f54204e554c4c20646566' +
        '61756c74202730272c'
      
        '20205155454c4c5f494420696e7428313129204e4f54204e554c4c2064656661' +
        '756c7420272d31272c'
      
        '2020444154554d2064617465204e4f54204e554c4c2064656661756c74202730' +
        '3030302d30302d3030272c'
      
        '2020574945444552564f524c4147452074696e79696e74283429206465666175' +
        '6c74204e554c4c2c'
      '202057565f444154554d20646174652064656661756c74204e554c4c2c'
      '202045524c45442074696e79696e742834292064656661756c74204e554c4c2c'
      
        '2020455253545f564f4e2076617263686172283230292064656661756c74204e' +
        '554c4c2c'
      
        '20204b55525a54455854207661726368617228313030292064656661756c7420' +
        '4e554c4c2c'
      '20204d454d4f20746578742c'
      
        '20205052494d415259204b45592020284c46445f4e522c5155454c4c452c4441' +
        '54554d29'
      '293b'
      ''
      '435245415445205441424c4520494e56454e5455522028'
      
        '2020494420696e74283529204e4f54204e554c4c206175746f5f696e6372656d' +
        '656e742c'
      
        '2020444154554d2064617465204e4f54204e554c4c2064656661756c74202730' +
        '3030302d30302d3030272c'
      
        '2020424553434852454942554e47207661726368617228323530292064656661' +
        '756c74204e554c4c2c'
      '2020494e464f20746578742c'
      
        '20205354415455532074696e79696e7428332920756e7369676e6564204e4f54' +
        '204e554c4c2064656661756c74202730272c'
      '20205052494d415259204b4559202028494429'
      
        '2920434f4d4d454e543d274b6f7066646174656e2066fc7220496e76656e7475' +
        '72656e273b'
      ''
      '435245415445205441424c45204a4f55524e414c2028'
      
        '20205445524d5f494420696e742831312920756e7369676e6564204e4f54204e' +
        '554c4c2064656661756c74202731272c'
      
        '20205155454c4c452074696e79696e74283429204e4f54204e554c4c20646566' +
        '61756c74202730272c'
      
        '20205245435f494420696e7428313129204e4f54204e554c4c206175746f5f69' +
        '6e6372656d656e742c'
      
        '20205155454c4c455f5355422074696e79696e742834292064656661756c7420' +
        '4e554c4c2c'
      
        '2020414444525f494420696e7428313129204e4f54204e554c4c206465666175' +
        '6c7420272d31272c'
      
        '20204c4945465f414444525f494420696e7428313129204e4f54204e554c4c20' +
        '64656661756c7420272d31272c'
      
        '20204154524e554d20696e7428313129204e4f54204e554c4c2064656661756c' +
        '7420272d31272c'
      
        '20205652454e554d20696e7428313129204e4f54204e554c4c2064656661756c' +
        '7420272d31272c'
      
        '2020564c534e554d20696e7428313129204e4f54204e554c4c2064656661756c' +
        '7420272d31272c'
      
        '2020464f4c47454e5220696e7428313129204e4f54204e554c4c206465666175' +
        '6c7420272d31272c'
      '20204b4d5f5354414e4420696e74283131292064656661756c74204e554c4c2c'
      
        '20204b465a5f494420696e7428313129204e4f54204e554c4c2064656661756c' +
        '7420272d31272c'
      
        '20205645525452455445525f494420696e7428313129204e4f54204e554c4c20' +
        '64656661756c7420272d31272c'
      
        '2020474c4f4252414241545420666c6f61742831302c3229204e4f54204e554c' +
        '4c2064656661756c742027302e3030272c'
      '202041444154554d20646174652064656661756c74204e554c4c2c'
      
        '202052444154554d2064617465204e4f54204e554c4c2064656661756c742027' +
        '303030302d30302d3030272c'
      '20204c444154554d20646174652064656661756c74204e554c4c2c'
      '20205445524d494e20646174652064656661756c74204e554c4c2c'
      
        '202050525f4542454e452074696e79696e742834292064656661756c74204e55' +
        '4c4c2c'
      
        '20204c4945464152542074696e79696e74283229204e4f54204e554c4c206465' +
        '6661756c7420272d31272c'
      
        '20205a41484c4152542074696e79696e74283229204e4f54204e554c4c206465' +
        '6661756c7420272d31272c'
      
        '20204745574943485420464c4f41542831302c3329204e4f54204e554c4c2044' +
        '454641554c54202730272c'
      
        '20204b4f53545f4e4554544f20666c6f61742831302c3229204e4f54204e554c' +
        '4c2064656661756c742027302e3030272c'
      
        '2020574552545f4e4554544f20666c6f61742831302c3229204e4f54204e554c' +
        '4c2064656661756c742027302e3030272c'
      
        '20204c4f484e20666c6f61742831302c3229204e4f54204e554c4c2064656661' +
        '756c742027302e3030272c'
      
        '20205741524520666c6f61742831302c3229204e4f54204e554c4c2064656661' +
        '756c742027302e3030272c'
      
        '2020544b4f535420666c6f61742831302c3229204e4f54204e554c4c20646566' +
        '61756c742027302e3030272c'
      
        '2020524f48474557494e4e20464c4f41542831302c3229204e4f54204e554c4c' +
        '2044454641554c54202730272c'
      
        '20204d5753545f3020666c6f61742831302c3229204e4f54204e554c4c206465' +
        '6661756c742027302e3030272c'
      
        '20204d5753545f3120666c6f61742831302c3229204e4f54204e554c4c206465' +
        '6661756c742027302e3030272c'
      
        '20204d5753545f3220666c6f61742831302c3229204e4f54204e554c4c206465' +
        '6661756c742027302e3030272c'
      
        '20204d5753545f3320666c6f61742831302c3229204e4f54204e554c4c206465' +
        '6661756c742027302e3030272c'
      
        '20204e53554d4d4520666c6f61742831302c3229204e4f54204e554c4c206465' +
        '6661756c742027302e3030272c'
      
        '20204d53554d4d455f3020666c6f61742831302c3229204e4f54204e554c4c20' +
        '64656661756c742027302e3030272c'
      
        '20204d53554d4d455f3120666c6f61742831302c3229204e4f54204e554c4c20' +
        '64656661756c742027302e3030272c'
      
        '20204d53554d4d455f3220666c6f61742831302c3229204e4f54204e554c4c20' +
        '64656661756c742027302e3030272c'
      
        '20204d53554d4d455f3320666c6f61742831302c3229204e4f54204e554c4c20' +
        '64656661756c742027302e3030272c'
      
        '20204d53554d4d4520666c6f61742831302c3229204e4f54204e554c4c206465' +
        '6661756c742027302e3030272c'
      
        '20204253554d4d4520666c6f61742831302c3229204e4f54204e554c4c206465' +
        '6661756c742027302e3030272c'
      
        '2020415453554d4d4520666c6f61742831302c3229204e4f54204e554c4c2064' +
        '656661756c742027302e3030272c'
      
        '202041544d53554d4d4520666c6f61742831302c3229204e4f54204e554c4c20' +
        '64656661756c742027302e3030272c'
      
        '202050524f5649535f5745525420666c6f61742831302c3229204e4f54204e55' +
        '4c4c2064656661756c742027302e3030272c'
      
        '20205741454852554e472076617263686172283529204e4f54204e554c4c2064' +
        '656661756c742027272c'
      
        '2020474547454e4b4f4e544f20696e7428313129204e4f54204e554c4c206465' +
        '6661756c7420272d31272c'
      
        '2020534f4c4c5f53544147452074696e79696e74283429204e4f54204e554c4c' +
        '2064656661756c74202730272c'
      
        '2020534f4c4c5f534b4f4e544f20666c6f617428352c3229204e4f54204e554c' +
        '4c2064656661756c742027302e3030272c'
      
        '2020534f4c4c5f4e544147452074696e79696e74283429204e4f54204e554c4c' +
        '2064656661756c74202730272c'
      
        '2020534f4c4c5f524154454e2074696e79696e74283429204e4f54204e554c4c' +
        '2064656661756c74202731272c'
      
        '2020534f4c4c5f5241544245545220666c6f61742831302c3229204e4f54204e' +
        '554c4c2064656661756c742027302e3030272c'
      
        '2020534f4c4c5f524154494e54455256414c4c20696e7428313129204e4f5420' +
        '4e554c4c2064656661756c74202730272c'
      
        '20204953545f414e5a41484c554e4720666c6f61742831302c32292064656661' +
        '756c74204e554c4c2c'
      
        '20204953545f534b4f4e544f20666c6f61742831302c32292064656661756c74' +
        '204e554c4c2c'
      '20204953545f5a41484c44415420646174652064656661756c74204e554c4c2c'
      
        '20204953545f42455452414720666c6f61742831302c3229204e4f54204e554c' +
        '4c2064656661756c742027302e3030272c'
      
        '20204d41484e4b4f5354454e20666c6f61742831302c3229204e4f54204e554c' +
        '4c2064656661756c742027302e3030272c'
      
        '20204b4f4e544f4155535a554720696e74283131292064656661756c74204e55' +
        '4c4c2c'
      
        '202042414e4b5f494420696e7428313129204e4f54204e554c4c206465666175' +
        '6c7420272d31272c'
      
        '20205354414449554d2074696e79696e74283429204e4f54204e554c4c206465' +
        '6661756c74202730272c'
      '202045525354454c4c5420646174652064656661756c74204e554c4c2c'
      
        '2020455253545f4e414d452076617263686172283230292064656661756c7420' +
        '4e554c4c2c'
      
        '20204b554e5f4e554d2076617263686172283230292064656661756c74204e55' +
        '4c4c2c'
      
        '20204b554e5f414e524544452076617263686172283430292064656661756c74' +
        '204e554c4c2c'
      
        '20204b554e5f4e414d45312076617263686172283430292064656661756c7420' +
        '4e554c4c2c'
      
        '20204b554e5f4e414d45322076617263686172283430292064656661756c7420' +
        '4e554c4c2c'
      
        '20204b554e5f4e414d45332076617263686172283430292064656661756c7420' +
        '4e554c4c2c'
      
        '20204b554e5f41425445494c554e472076617263686172283430292064656661' +
        '756c74204e554c4c2c'
      
        '20204b554e5f535452415353452076617263686172283430292064656661756c' +
        '74204e554c4c2c'
      
        '20204b554e5f4c414e4420766172636861722835292064656661756c74204e55' +
        '4c4c2c'
      
        '20204b554e5f504c5a2076617263686172283130292064656661756c74204e55' +
        '4c4c2c'
      
        '20204b554e5f4f52542076617263686172283430292064656661756c74204e55' +
        '4c4c2c'
      '2020555352312076617263686172283830292064656661756c74204e554c4c2c'
      '2020555352322076617263686172283830292064656661756c74204e554c4c2c'
      
        '202050524f4a454b542076617263686172283830292064656661756c74204e55' +
        '4c4c2c'
      
        '20204f52474e554d2076617263686172283230292064656661756c74204e554c' +
        '4c2c'
      
        '2020424553545f4e414d452076617263686172283430292064656661756c7420' +
        '4e554c4c2c'
      
        '2020424553545f434f44452074696e79696e742834292064656661756c74204e' +
        '554c4c2c'
      '2020424553545f444154554d20646174652064656661756c74204e554c4c2c'
      '2020494e464f20746578742c'
      
        '202055575f4e554d20696e7428313129204e4f54204e554c4c2064656661756c' +
        '7420272d31272c'
      
        '20204d41484e53545546452074696e79696e74283129204e4f54204e554c4c20' +
        '64656661756c74202730272c'
      '20204d41484e444154554d20646174652064656661756c74204e554c4c2c'
      
        '20204d41484e5052494e542074696e79696e74283129204e4f54204e554c4c20' +
        '64656661756c74202730272c'
      
        '20204652454947414245315f464c414720656e756d28274e272c27592729204e' +
        '4f54204e554c4c2064656661756c7420274e272c'
      
        '20205052494e545f464c414720656e756d28274e272c27592729204e4f54204e' +
        '554c4c2064656661756c7420274e272c'
      
        '202042525554544f5f464c414720656e756d28274e272c27592729204e4f5420' +
        '4e554c4c2064656661756c7420274e272c'
      
        '20204d5753545f465245495f464c414720656e756d28274e272c27592729204e' +
        '4f54204e554c4c2064656661756c7420274e272c'
      
        '202050524f5649535f4245524543484e455420656e756d28274e272c27592729' +
        '204e4f54204e554c4c2064656661756c7420274e272c'
      
        '202053484f505f49442074696e79696e74283329204e4f54204e554c4c206465' +
        '6661756c7420272d31272c'
      
        '202053484f505f4f52444552494420696e7428313129204e4f54204e554c4c20' +
        '64656661756c7420272d31272c'
      
        '202053484f505f5354415455532074696e79696e7428332920756e7369676e65' +
        '64204e4f54204e554c4c2064656661756c74202730272c'
      
        '202053484f505f4348414e47455f464c414720656e756d28274e272c27592729' +
        '204e4f54204e554c4c2064656661756c7420274e272c'
      '20205052494d415259204b45592020285245435f4944292c'
      '20204b455920524a5f52454e554d20285155454c4c452c5652454e554d292c'
      '20204b455920414444525f49442028414444525f49442c52444154554d29'
      '293b'
      ''
      '435245415445205441424c45204a4f55524e414c504f532028'
      
        '20205245435f494420696e7428313129204e4f54204e554c4c206175746f5f69' +
        '6e6372656d656e742c'
      
        '20205155454c4c452074696e79696e74283429204e4f54204e554c4c20646566' +
        '61756c74202730272c'
      
        '20205155454c4c455f5355422074696e79696e74283429204e4f54204e554c4c' +
        '2064656661756c74202730272c'
      
        '20205155454c4c455f53524320696e7428313129204e4f54204e554c4c206465' +
        '6661756c7420272d31272c'
      
        '20204a4f55524e414c5f494420696e7428313129204e4f54204e554c4c206465' +
        '6661756c74202730272c'
      
        '2020415254494b454c5459502063686172283129204e4f54204e554c4c206465' +
        '6661756c742027272c'
      
        '2020415254494b454c5f494420696e7428313129204e4f54204e554c4c206465' +
        '6661756c7420272d31272c'
      
        '2020544f505f504f535f494420696e7428313129204e4f54204e554c4c206465' +
        '6661756c7420272d31272c'
      
        '2020414444525f494420696e7428313129204e4f54204e554c4c206465666175' +
        '6c7420272d31272c'
      
        '20204154524e554d20696e7428313129204e4f54204e554c4c2064656661756c' +
        '7420272d31272c'
      
        '20205652454e554d20696e7428313129204e4f54204e554c4c2064656661756c' +
        '7420272d31272c'
      
        '2020564c534e554d20696e7428313129204e4f54204e554c4c2064656661756c' +
        '7420272d31272c'
      
        '2020504f534954494f4e20696e7428313129204e4f54204e554c4c2064656661' +
        '756c74202730272c'
      '2020564945575f504f5320636861722833292064656661756c74204e554c4c2c'
      
        '20204d41544348434f44452076617263686172283230292064656661756c7420' +
        '4e554c4c2c'
      
        '20204152544e554d2076617263686172283230292064656661756c74204e554c' +
        '4c2c'
      
        '2020424152434f44452076617263686172283230292064656661756c74204e55' +
        '4c4c2c'
      
        '20204d454e474520666c6f61742831302c3329204e4f54204e554c4c20646566' +
        '61756c742027302e303030272c'
      
        '20204c41454e47452076617263686172283230292064656661756c74204e554c' +
        '4c2c'
      
        '202047524f455353452076617263686172283230292064656661756c74204e55' +
        '4c4c2c'
      
        '202044494d454e53494f4e2076617263686172283230292064656661756c7420' +
        '4e554c4c2c'
      
        '20204745574943485420666c6f61742831302c3329204e4f54204e554c4c2064' +
        '656661756c742027302e303030272c'
      
        '20204d455f45494e484549542076617263686172283130292064656661756c74' +
        '204e554c4c2c'
      
        '202050525f45494e4845495420666c6f61742831302c3329204e4f54204e554c' +
        '4c2064656661756c742027312e303030272c'
      
        '202056504520696e742831312920756e7369676e6564204e4f54204e554c4c20' +
        '64656661756c74202731272c'
      
        '2020454b5f505245495320464c4f41542831302c3329204e4f54204e554c4c20' +
        '44454641554c542027302e303030272c'
      
        '202043414c435f46414b544f5220464c4f415428322c3529204e4f54204e554c' +
        '4c2044454641554c542027302e3030303030272c'
      
        '202045505245495320666c6f61742831302c3329204e4f54204e554c4c206465' +
        '6661756c742027302e303030272c'
      
        '2020455f52474557494e4e20666c6f61742831302c3229204e4f54204e554c4c' +
        '2064656661756c742027302e3030272c'
      
        '202052414241545420666c6f61742831302c3229204e4f54204e554c4c206465' +
        '6661756c742027302e3030272c'
      
        '20205241424154543220666c6f61742831302c3229204e4f54204e554c4c2064' +
        '656661756c742027302e3030272c'
      
        '20205241424154543320666c6f61742831302c3229204e4f54204e554c4c2064' +
        '656661756c742027302e3030272c'
      
        '20205354455545525f434f44452074696e79696e74283429204e4f54204e554c' +
        '4c2064656661756c74202730272c'
      
        '2020414c545445494c5f50524f5a20666c6f61742831302c3229204e4f54204e' +
        '554c4c2064656661756c742027302e3130272c'
      
        '2020414c545445494c5f5354434f44452074696e79696e74283429204e4f5420' +
        '4e554c4c2064656661756c74202730272c'
      
        '202050524f5649535f50524f5a20666c6f617428352c3229204e4f54204e554c' +
        '4c2064656661756c742027302e3030272c'
      
        '202050524f5649535f5745525420666c6f61742831302c3229204e4f54204e55' +
        '4c4c2064656661756c742027302e3030272c'
      
        '20204745425543485420656e756d28274e272c27592729204e4f54204e554c4c' +
        '2064656661756c7420274e272c'
      
        '2020474547454e4b544f20696e7428313129204e4f54204e554c4c2064656661' +
        '756c7420272d31272c'
      '202042455a454943484e554e4720746578742c'
      
        '2020534e5f464c414720656e756d28274e272c27592729204e4f54204e554c4c' +
        '2064656661756c7420274e272c'
      
        '2020414c545445494c5f464c414720656e756d28274e272c27592729204e4f54' +
        '204e554c4c2064656661756c7420274e272c'
      
        '202042455a5f464553545f464c414720656e756d28274e272c27592729204e4f' +
        '54204e554c4c2064656661756c7420274e272c'
      
        '202042525554544f5f464c414720656e756d28274e272c27592729204e4f5420' +
        '4e554c4c2064656661756c7420274e272c'
      
        '20204e4f5f5241424154545f464c414720656e756d28274e272c27592729204e' +
        '4f54204e554c4c2064656661756c7420274e272c'
      '20205052494d415259204b45592020285245435f4944292c'
      '20204b455920415254494b454c5f49442028415254494b454c5f4944292c'
      '20204b455920414444525f49442028414444525f4944292c'
      
        '20204b4559204a4f55524e414c5f494420284a4f55524e414c5f49442c504f53' +
        '4954494f4e292c'
      '20204b4559205155454c4c455f53524320285155454c4c455f53524329'
      '293b'
      ''
      '435245415445205441424c45204b465a2028'
      
        '20204b465a5f494420696e7428313129204e4f54204e554c4c206175746f5f69' +
        '6e6372656d656e742c'
      
        '2020414444525f494420696e7428313129204e4f54204e554c4c206465666175' +
        '6c74202730272c'
      
        '2020464753545f4e554d207661726368617228323029204e4f54204e554c4c20' +
        '64656661756c742027272c'
      
        '20204b465a5f4752555050452074696e79696e742834292064656661756c7420' +
        '4e554c4c2c'
      
        '2020504f4c5f4b454e4e5a207661726368617228313029204e4f54204e554c4c' +
        '2064656661756c742027272c'
      
        '20205343484c5f5a555f322076617263686172283230292064656661756c7420' +
        '4e554c4c2c'
      
        '20205343484c5f5a555f332076617263686172283230292064656661756c7420' +
        '4e554c4c2c'
      '20205459505f494420696e74283131292064656661756c74204e554c4c2c'
      '20205459502076617263686172283130292064656661756c74204e554c4c2c'
      
        '2020415553465545522076617263686172283130292064656661756c74204e55' +
        '4c4c2c'
      '20204152545f494420696e74283131292064656661756c74204e554c4c2c'
      
        '202046414252494b41545f494420696e74283131292064656661756c74204e55' +
        '4c4c2c'
      
        '20204b5241465453544f46465f494420696e74283131292064656661756c7420' +
        '4e554c4c2c'
      '202047525550504520696e74283131292064656661756c74204e554c4c2c'
      
        '20205343484c5545535f4e522076617263686172283130292064656661756c74' +
        '204e554c4c2c'
      
        '20205a5343484c5f4e522076617263686172283130292064656661756c74204e' +
        '554c4c2c'
      
        '20204d4f544f525f4e522076617263686172283230292064656661756c74204e' +
        '554c4c2c'
      
        '20204b465a4252495f4e522076617263686172283135292064656661756c7420' +
        '4e554c4c2c'
      
        '20204d4f544f522076617263686172283135292064656661756c74204e554c4c' +
        '2c'
      
        '202047455452494542452076617263686172283130292064656661756c74204e' +
        '554c4c2c'
      '20204b5720696e74283131292064656661756c74204e554c4c2c'
      '2020505320696e74283131292064656661756c74204e554c4c2c'
      '20204b4d5f5354414e4420696e74283131292064656661756c74204e554c4c2c'
      '20204855425241554d20696e74283131292064656661756c74204e554c4c2c'
      
        '202052454946454e2076617263686172283130292064656661756c74204e554c' +
        '4c2c'
      
        '2020524549465f47522076617263686172283130292064656661756c74204e55' +
        '4c4c2c'
      
        '202046415242452076617263686172283130292064656661756c74204e554c4c' +
        '2c'
      
        '2020504f4c535445522076617263686172283130292064656661756c74204e55' +
        '4c4c2c'
      '20205a554c415353554e4720646174652064656661756c74204e554c4c2c'
      '20204845525354454c4c554e4720646174652064656661756c74204e554c4c2c'
      '20204b415546444154554d20646174652064656661756c74204e554c4c2c'
      '20204c455f42455355434820646174652064656661756c74204e554c4c2c'
      '20204e41455f5455455620646174652064656661756c74204e554c4c2c'
      '20204e41455f415520646174652064656661756c74204e554c4c2c'
      '20204e41455f535020646174652064656661756c74204e554c4c2c'
      '20204e41455f545020646174652064656661756c74204e554c4c2c'
      '2020454b5f505245495320646f75626c652064656661756c74204e554c4c2c'
      '202052554553544b20646f75626c652064656661756c74204e554c4c2c'
      '2020564b5f4e4554544f20646f75626c652064656661756c74204e554c4c2c'
      '20204d5753545f50524f5a20646f75626c652064656661756c74204e554c4c2c'
      
        '2020554d5341545a5f47455320646f75626c652064656661756c74204e554c4c' +
        '2c'
      
        '2020554d5341545a5f47415220646f75626c652064656661756c74204e554c4c' +
        '2c'
      
        '20205645525452455445525f494420696e7428313129204e4f54204e554c4c20' +
        '64656661756c7420272d31272c'
      '2020494e464f20746578742c'
      '2020574b53545f494e464f20746578742c'
      
        '20205553455246454c445f303120766172636861722832353529206465666175' +
        '6c74204e554c4c2c'
      
        '20205553455246454c445f303220766172636861722832353529206465666175' +
        '6c74204e554c4c2c'
      
        '20205553455246454c445f303320766172636861722832353529206465666175' +
        '6c74204e554c4c2c'
      
        '20205553455246454c445f303420766172636861722832353529206465666175' +
        '6c74204e554c4c2c'
      
        '20205553455246454c445f303520766172636861722832353529206465666175' +
        '6c74204e554c4c2c'
      
        '20205553455246454c445f303620766172636861722832353529206465666175' +
        '6c74204e554c4c2c'
      
        '20205553455246454c445f303720766172636861722832353529206465666175' +
        '6c74204e554c4c2c'
      
        '20205553455246454c445f303820766172636861722832353529206465666175' +
        '6c74204e554c4c2c'
      
        '20205553455246454c445f303920766172636861722832353529206465666175' +
        '6c74204e554c4c2c'
      
        '20205553455246454c445f313020766172636861722832353529206465666175' +
        '6c74204e554c4c2c'
      '20205052494d415259204b45592020284b465a5f4944292c'
      '20204b4559204b554e4e554d2028414444525f4944292c'
      '20204b4559204b454e4e5a2028504f4c5f4b454e4e5a292c'
      '20204b455920464753545f4e522028464753545f4e554d29'
      '293b'
      ''
      '435245415445205441424c45204c414e442028'
      
        '202049442063686172283229204e4f54204e554c4c2064656661756c74202727' +
        '2c'
      
        '20204e414d4520766172636861722831303029204e4f54204e554c4c20646566' +
        '61756c742027272c'
      
        '202049534f5f434f44455f332063686172283329204e4f54204e554c4c206465' +
        '6661756c742027272c'
      
        '2020464f524d41542074696e79696e74283329204e4f54204e554c4c20646566' +
        '61756c74202731272c'
      
        '2020564f525741484c2076617263686172283130292064656661756c74204e55' +
        '4c4c2c'
      
        '20205741454852554e4720766172636861722835292064656661756c74204e55' +
        '4c4c2c'
      '20205350524143484520636861722833292064656661756c74204e554c4c2c'
      '20205052494d415259204b45592020284944292c'
      '2020554e49515545204b4559204944585f4e414d4520284e414d4529'
      '293b'
      ''
      '435245415445205441424c45204c494e4b2028'
      
        '20204d4f44554c5f49442074696e79696e74283329204e4f54204e554c4c2064' +
        '656661756c7420272d31272c'
      
        '20205245435f494420696e7428313129204e4f54204e554c4c2064656661756c' +
        '7420272d31272c'
      
        '20205046414420766172636861722832353529204e4f54204e554c4c20646566' +
        '61756c742027272c'
      
        '2020444154454920766172636861722832303029204e4f54204e554c4c206465' +
        '6661756c742027272c'
      '202042454d45524b554e4720746578742c'
      
        '20204c4153545f4348414e47452074696d657374616d7028313429204e4f5420' +
        '4e554c4c2c'
      
        '20204c4153545f4348414e47455f55534552207661726368617228353029204e' +
        '4f54204e554c4c2064656661756c742027272c'
      
        '20204f50454e5f464c414720656e756d28274e272c27592729204e4f54204e55' +
        '4c4c2064656661756c7420274e272c'
      
        '20204f50454e5f55534552207661726368617228353029204e4f54204e554c4c' +
        '2064656661756c742027272c'
      
        '20204f50454e5f54494d452074696d657374616d7028313429204e4f54204e55' +
        '4c4c2c'
      
        '20205052494d415259204b45592020284d4f44554c5f49442c5245435f49442c' +
        '504641442c444154454929'
      '293b'
      ''
      '435245415445205441424c45204d495441524245495445522028'
      
        '20204d415f494420696e742831312920756e7369676e6564204e4f54204e554c' +
        '4c206175746f5f696e6372656d656e742c'
      
        '20204d415f4e554d4d45522076617263686172283130292064656661756c7420' +
        '4e554c4c2c'
      
        '20204e414d45207661726368617228313030292064656661756c74204e554c4c' +
        '2c'
      
        '2020564e414d45207661726368617228313030292064656661756c74204e554c' +
        '4c2c'
      
        '20204c4f47494e5f4e414d45207661726368617228353029204e4f54204e554c' +
        '4c2064656661756c742027272c'
      
        '2020414e5a454947455f4e414d45207661726368617228353029204e4f54204e' +
        '554c4c2064656661756c742027272c'
      
        '2020414e524544452076617263686172283135292064656661756c74204e554c' +
        '4c2c'
      
        '2020544954454c2076617263686172283135292064656661756c74204e554c4c' +
        '2c'
      
        '20205a555341545a2076617263686172283430292064656661756c74204e554c' +
        '4c2c'
      
        '20205a555341545a322076617263686172283430292064656661756c74204e55' +
        '4c4c2c'
      
        '20205a554841454e44454e2076617263686172283430292064656661756c7420' +
        '4e554c4c2c'
      
        '2020535452415353452076617263686172283430292064656661756c74204e55' +
        '4c4c2c'
      '20204c414e4420766172636861722835292064656661756c74204e554c4c2c'
      '2020504c5a2076617263686172283130292064656661756c74204e554c4c2c'
      '20204f52542076617263686172283430292064656661756c74204e554c4c2c'
      
        '202054454c45464f4e207661726368617228313030292064656661756c74204e' +
        '554c4c2c'
      '2020464158207661726368617228313030292064656661756c74204e554c4c2c'
      
        '202046554e4b207661726368617228313030292064656661756c74204e554c4c' +
        '2c'
      
        '2020454d41494c207661726368617228313030292064656661756c74204e554c' +
        '4c2c'
      
        '2020494e5445524e4554207661726368617228313030292064656661756c7420' +
        '4e554c4c2c'
      
        '20205350524143485f494420736d616c6c696e742836292064656661756c7420' +
        '2732272c'
      
        '20204245534348414546544947554e475341525420736d616c6c696e74283629' +
        '2064656661756c74204e554c4c2c'
      
        '20204245534348414546544947554e47534752414420736d616c6c696e742836' +
        '292064656661756c74204e554c4c2c'
      
        '20204a414852455355524c41554220666c6f61742064656661756c74204e554c' +
        '4c2c'
      
        '20204755454c5449475f564f4e206461746574696d652064656661756c74204e' +
        '554c4c2c'
      
        '20204755454c5449475f424953206461746574696d652064656661756c74204e' +
        '554c4c2c'
      
        '2020474542444154554d206461746574696d652064656661756c74204e554c4c' +
        '2c'
      
        '202047455343484c4543485420656e756d28274d272c27572729204e4f54204e' +
        '554c4c2064656661756c7420274d272c'
      
        '202046414d5354414e4420736d616c6c696e742836292064656661756c74204e' +
        '554c4c2c'
      '202042414e4b2076617263686172283430292064656661756c74204e554c4c2c'
      '2020424c5a2076617263686172283130292064656661756c74204e554c4c2c'
      '20204b544f2076617263686172283230292064656661756c74204e554c4c2c'
      '202042454d45524b554e4720746578742c'
      
        '202045525354454c4c54206461746574696d652064656661756c74204e554c4c' +
        '2c'
      
        '202045525354454c4c545f4e414d452076617263686172283230292064656661' +
        '756c74204e554c4c2c'
      '2020474541454e44206461746574696d652064656661756c74204e554c4c2c'
      
        '2020474541454e445f4e414d452076617263686172283230292064656661756c' +
        '74204e554c4c2c'
      
        '20205553455246454c445f303120766172636861722832353529206465666175' +
        '6c74204e554c4c2c'
      
        '20205553455246454c445f303220766172636861722832353529206465666175' +
        '6c74204e554c4c2c'
      
        '20205553455246454c445f303320766172636861722832353529206465666175' +
        '6c74204e554c4c2c'
      
        '20205553455246454c445f303420766172636861722832353529206465666175' +
        '6c74204e554c4c2c'
      
        '20205553455246454c445f303520766172636861722832353529206465666175' +
        '6c74204e554c4c2c'
      
        '20205553455246454c445f303620766172636861722832353529206465666175' +
        '6c74204e554c4c2c'
      
        '20205553455246454c445f303720766172636861722832353529206465666175' +
        '6c74204e554c4c2c'
      
        '20205553455246454c445f303820766172636861722832353529206465666175' +
        '6c74204e554c4c2c'
      
        '20205553455246454c445f303920766172636861722832353529206465666175' +
        '6c74204e554c4c2c'
      
        '20205553455246454c445f313020766172636861722832353529206465666175' +
        '6c74204e554c4c2c'
      '20205052494d415259204b45592020284d415f4944292c'
      
        '2020554e49515545204b4559204944585f4d415f4e554d20284d415f4e554d4d' +
        '4552292c'
      
        '2020554e49515545204b4559204944585f4c4f47494e4e414d4520284c4f4749' +
        '4e5f4e414d4529'
      '293b'
      ''
      '435245415445205441424c452050494d5f415546474142454e2028'
      
        '20205245434f5244494420696e7428313129204e4f54204e554c4c206175746f' +
        '5f696e6372656d656e742c'
      
        '20205245534f55524345494420696e7428313129204e4f54204e554c4c206465' +
        '6661756c74202730272c'
      
        '2020434f4d504c45544520656e756d28274e272c275927292064656661756c74' +
        '20274e272c'
      
        '20204445534352495054494f4e206368617228323535292064656661756c7420' +
        '4e554c4c2c'
      
        '202044455441494c53206368617228323535292064656661756c74204e554c4c' +
        '2c'
      '2020435245415445444f4e204441544554494d452c'
      '20205052494f5249545920696e74283131292064656661756c74204e554c4c2c'
      '202043415445474f525920696e74283131292064656661756c74204e554c4c2c'
      '2020434f4d504c455445444f4e204441544554494d452c'
      '202044554544415445204441544554494d452c'
      
        '2020555345524649454c4430206368617228313030292064656661756c74204e' +
        '554c4c2c'
      
        '2020555345524649454c4431206368617228313030292064656661756c74204e' +
        '554c4c2c'
      
        '2020555345524649454c4432206368617228313030292064656661756c74204e' +
        '554c4c2c'
      
        '2020555345524649454c4433206368617228313030292064656661756c74204e' +
        '554c4c2c'
      
        '2020555345524649454c4434206368617228313030292064656661756c74204e' +
        '554c4c2c'
      
        '2020555345524649454c4435206368617228313030292064656661756c74204e' +
        '554c4c2c'
      
        '2020555345524649454c4436206368617228313030292064656661756c74204e' +
        '554c4c2c'
      
        '2020555345524649454c4437206368617228313030292064656661756c74204e' +
        '554c4c2c'
      
        '2020555345524649454c4438206368617228313030292064656661756c74204e' +
        '554c4c2c'
      
        '2020555345524649454c4439206368617228313030292064656661756c74204e' +
        '554c4c2c'
      '20205052494d415259204b45592020285245434f52444944292c'
      '20204b45592052657349445f6e647820285245534f555243454944292c'
      '20204b45592044756544617465202844554544415445292c'
      '20204b455920436f6d706c657465644f6e2028434f4d504c455445444f4e29'
      '293b'
      ''
      '435245415445205441424c452050494d5f4b4f4e54414b54452028'
      
        '20205245434f5244494420696e7428313129204e4f54204e554c4c206175746f' +
        '5f696e6372656d656e742c'
      
        '20205245534f55524345494420696e7428313129204e4f54204e554c4c206465' +
        '6661756c74202730272c'
      
        '202046495253544e414d452063686172283530292064656661756c74204e554c' +
        '4c2c'
      
        '20204c4153544e414d45206368617228353029204e4f54204e554c4c20646566' +
        '61756c742027272c'
      '202042495254484441544520646174652064656661756c74204e554c4c2c'
      '2020414e4e495645525341525920646174652064656661756c74204e554c4c2c'
      '20205449544c452063686172283530292064656661756c74204e554c4c2c'
      
        '2020434f4d50414e59206368617228353029204e4f54204e554c4c2064656661' +
        '756c742027272c'
      
        '20204a4f425f504f534954494f4e2063686172283330292064656661756c7420' +
        '4e554c4c2c'
      
        '202041444452455353206368617228313030292064656661756c74204e554c4c' +
        '2c'
      '2020434954592063686172283530292064656661756c74204e554c4c2c'
      '202053544154452063686172283235292064656661756c74204e554c4c2c'
      '20205a49502063686172283130292064656661756c74204e554c4c2c'
      '2020434f554e5452592063686172283235292064656661756c74204e554c4c2c'
      '20204e4f5445206368617228323535292064656661756c74204e554c4c2c'
      '202050484f4e45312063686172283235292064656661756c74204e554c4c2c'
      '202050484f4e45322063686172283235292064656661756c74204e554c4c2c'
      '202050484f4e45332063686172283235292064656661756c74204e554c4c2c'
      '202050484f4e45342063686172283235292064656661756c74204e554c4c2c'
      '202050484f4e45352063686172283235292064656661756c74204e554c4c2c'
      
        '202050484f4e45545950453120696e74283131292064656661756c74204e554c' +
        '4c2c'
      
        '202050484f4e45545950453220696e74283131292064656661756c74204e554c' +
        '4c2c'
      
        '202050484f4e45545950453320696e74283131292064656661756c74204e554c' +
        '4c2c'
      
        '202050484f4e45545950453420696e74283131292064656661756c74204e554c' +
        '4c2c'
      
        '202050484f4e45545950453520696e74283131292064656661756c74204e554c' +
        '4c2c'
      '202043415445474f525920696e74283131292064656661756c74204e554c4c2c'
      '2020454d41494c206368617228313030292064656661756c74204e554c4c2c'
      
        '2020435553544f4d31206368617228313030292064656661756c74204e554c4c' +
        '2c'
      
        '2020435553544f4d32206368617228313030292064656661756c74204e554c4c' +
        '2c'
      
        '2020435553544f4d33206368617228313030292064656661756c74204e554c4c' +
        '2c'
      
        '2020435553544f4d34206368617228313030292064656661756c74204e554c4c' +
        '2c'
      
        '2020555345524649454c4430206368617228313030292064656661756c74204e' +
        '554c4c2c'
      
        '2020555345524649454c4431206368617228313030292064656661756c74204e' +
        '554c4c2c'
      
        '2020555345524649454c4432206368617228313030292064656661756c74204e' +
        '554c4c2c'
      
        '2020555345524649454c4433206368617228313030292064656661756c74204e' +
        '554c4c2c'
      
        '2020555345524649454c4434206368617228313030292064656661756c74204e' +
        '554c4c2c'
      
        '2020555345524649454c4435206368617228313030292064656661756c74204e' +
        '554c4c2c'
      
        '2020555345524649454c4436206368617228313030292064656661756c74204e' +
        '554c4c2c'
      
        '2020555345524649454c4437206368617228313030292064656661756c74204e' +
        '554c4c2c'
      
        '2020555345524649454c4438206368617228313030292064656661756c74204e' +
        '554c4c2c'
      
        '2020555345524649454c4439206368617228313030292064656661756c74204e' +
        '554c4c2c'
      '20205052494d415259204b45592020285245434f52444944292c'
      '20204b45592052657349445f6e647820285245534f555243454944292c'
      '20204b4559204c4e616d655f6e647820284c4153544e414d45292c'
      '20204b455920436f6d70616e795f6e64782028434f4d50414e5929'
      '293b'
      ''
      '435245415445205441424c452050494d5f5445524d494e452028'
      
        '20205245434f5244494420696e7428313129204e4f54204e554c4c206175746f' +
        '5f696e6372656d656e742c'
      '2020535441525454494d45204441544554494d452c'
      '2020454e4454494d45204441544554494d452c'
      
        '20205245534f55524345494420696e7428313129204e4f54204e554c4c206465' +
        '6661756c74202730272c'
      
        '20204445534352495054494f4e206368617228323535292064656661756c7420' +
        '4e554c4c2c'
      '20204e4f544553206368617228323535292064656661756c74204e554c4c2c'
      '202043415445474f525920696e74283131292064656661756c74204e554c4c2c'
      
        '2020414c4c4441594556454e5420656e756d28274e272c27592729204e4f5420' +
        '4e554c4c2064656661756c7420274e272c'
      
        '202044494e4750415448206368617228323535292064656661756c74204e554c' +
        '4c2c'
      
        '2020414c41524d53455420656e756d28274e272c27592729204e4f54204e554c' +
        '4c2064656661756c7420274e272c'
      
        '2020414c41524d414456414e434520696e74283131292064656661756c74204e' +
        '554c4c2c'
      
        '2020414c41524d414456414e43455459504520696e7428313129206465666175' +
        '6c74204e554c4c2c'
      '2020534e4f4f5a4554494d45204441544554494d452c'
      
        '2020524550454154434f444520696e74283131292064656661756c74204e554c' +
        '4c2c'
      '202052455045415452414e4745454e44204441544554494d452c'
      
        '2020435553544f4d494e54455256414c20696e74283131292064656661756c74' +
        '204e554c4c2c'
      
        '2020555345524649454c4430206368617228313030292064656661756c74204e' +
        '554c4c2c'
      
        '2020555345524649454c4431206368617228313030292064656661756c74204e' +
        '554c4c2c'
      
        '2020555345524649454c4432206368617228313030292064656661756c74204e' +
        '554c4c2c'
      
        '2020555345524649454c4433206368617228313030292064656661756c74204e' +
        '554c4c2c'
      
        '2020555345524649454c4434206368617228313030292064656661756c74204e' +
        '554c4c2c'
      
        '2020555345524649454c4435206368617228313030292064656661756c74204e' +
        '554c4c2c'
      
        '2020555345524649454c4436206368617228313030292064656661756c74204e' +
        '554c4c2c'
      
        '2020555345524649454c4437206368617228313030292064656661756c74204e' +
        '554c4c2c'
      
        '2020555345524649454c4438206368617228313030292064656661756c74204e' +
        '554c4c2c'
      
        '2020555345524649454c4439206368617228313030292064656661756c74204e' +
        '554c4c2c'
      '20205052494d415259204b45592020285245434f52444944292c'
      
        '20204b4559207269645f73745f6e647820285245534f5552434549442c535441' +
        '525454494d45292c'
      '20204b45592073745f6e64782028535441525454494d45292c'
      '20204b45592065745f6e64782028454e4454494d45292c'
      '20204b45592052657349445f6e647820285245534f55524345494429'
      '293b'
      ''
      '435245415445205441424c4520504c5a2028'
      
        '20204c414e442063686172283329204e4f54204e554c4c2064656661756c7420' +
        '2744272c'
      
        '2020504c5a207661726368617228313129204e4f54204e554c4c206465666175' +
        '6c742027272c'
      
        '20204e414d45207661726368617228353029204e4f54204e554c4c2064656661' +
        '756c742027272c'
      
        '2020564f525741484c2076617263686172283132292064656661756c74204e55' +
        '4c4c2c'
      
        '202042554e4445534c414e4420636861722833292064656661756c74204e554c' +
        '4c2c'
      '20205052494d415259204b45592020284c414e442c504c5a2c4e414d45292c'
      '20204b4559204944585f4e414d4520284c414e442c4e414d45292c'
      
        '20204b4559204944585f564f525741484c20284c414e442c564f525741484c29' +
        '2c'
      '20204b4559204944585f4c414e4420284c414e4429'
      '293b'
      ''
      '435245415445205441424c45205241424154544752555050454e2028'
      
        '20205241424752505f4944207661726368617228313029204e4f54204e554c4c' +
        '2064656661756c742027272c'
      
        '20205241424752505f5459502074696e79696e74283329204e4f54204e554c4c' +
        '2064656661756c74202730272c'
      
        '20204d494e5f4d454e474520696e7428313129204e4f54204e554c4c20646566' +
        '61756c74202731272c'
      
        '20204c4945465f52414247525020696e7428313029204e4f54204e554c4c2064' +
        '656661756c74202730272c'
      
        '20205241424154543120666c6f617428362c3229204e4f54204e554c4c206465' +
        '6661756c742027302e3030272c'
      
        '20205241424154543220666c6f617428362c3229204e4f54204e554c4c206465' +
        '6661756c742027302e3030272c'
      
        '20205241424154543320666c6f617428362c3229204e4f54204e554c4c206465' +
        '6661756c742027302e3030272c'
      
        '2020414444525f494420696e7428313129204e4f54204e554c4c206465666175' +
        '6c7420272d31272c'
      
        '2020424553434852454942554e47207661726368617228323530292064656661' +
        '756c74204e554c4c2c'
      
        '20205052494d415259204b45592020285241424752505f49442c524142475250' +
        '5f5459502c4c4945465f5241424752502c4d494e5f4d454e474529'
      '293b'
      ''
      '435245415445205441424c45205245474953544552592028'
      
        '20204d41494e4b455920766172636861722832353529204e4f54204e554c4c20' +
        '64656661756c742027272c'
      
        '20204e414d4520766172636861722831303029204e4f54204e554c4c20646566' +
        '61756c742027272c'
      
        '202056414c5f43484152207661726368617228323535292064656661756c7420' +
        '4e554c4c2c'
      
        '202056414c5f44415445206461746574696d652064656661756c74204e554c4c' +
        '2c'
      '202056414c5f494e5420696e74283131292064656661756c74204e554c4c2c'
      
        '202056414c5f494e543220626967696e74283230292064656661756c74204e55' +
        '4c4c2c'
      
        '202056414c5f494e543320626967696e74283230292064656661756c74204e55' +
        '4c4c2c'
      
        '202056414c5f444f55424c4520646f75626c652064656661756c74204e554c4c' +
        '2c'
      '202056414c5f424c4f42206c6f6e67746578742c'
      '202056414c5f42494e206c6f6e67626c6f622c'
      
        '202056414c5f54595020736d616c6c696e742836292064656661756c74204e55' +
        '4c4c2c'
      
        '20204341434841424c4520656e756d28274e272c27592729204e4f54204e554c' +
        '4c2064656661756c7420274e272c'
      
        '2020524541444f4e4c5920656e756d28274e272c27592729204e4f54204e554c' +
        '4c2064656661756c7420274e272c'
      
        '20204c4153545f4348414e47452074696d657374616d7028313429204e4f5420' +
        '4e554c4c2c'
      '20205052494d415259204b45592020284d41494e4b45592c4e414d4529'
      '293b'
      ''
      '435245415445205441424c4520534348524946545645524b4548522028'
      
        '2020494420696e7428313129204e4f54204e554c4c206175746f5f696e637265' +
        '6d656e742c'
      '2020414444525f494420696e74283131292064656661756c7420272d31272c'
      
        '2020424553434852454942554e47207661726368617228323530292064656661' +
        '756c74204e554c4c2c'
      '20204c414e475f5445585420746578742c'
      
        '20204348414e47455f4c4153542074696d657374616d7028313429204e4f5420' +
        '4e554c4c2c'
      
        '20204348414e47455f5553455220766172636861722831303029206465666175' +
        '6c74204e554c4c2c'
      '20205052494d415259204b4559202028494429'
      '293b'
      ''
      '435245415445205441424c4520535052414348454e2028'
      
        '20205350524143485f494420696e7428313129204e4f54204e554c4c20617574' +
        '6f5f696e6372656d656e742c'
      
        '20204e414d45207661726368617228333229204e4f54204e554c4c2064656661' +
        '756c742027272c'
      
        '2020434f44452063686172283229204e4f54204e554c4c2064656661756c7420' +
        '27272c'
      '2020534f525420696e742833292064656661756c74204e554c4c2c'
      
        '202044454641554c545f464c414720656e756d28274e272c27592729204e4f54' +
        '204e554c4c2064656661756c7420274e272c'
      '20205052494d415259204b45592020285350524143485f4944292c'
      '20204b4559204944585f4e414d4520284e414d4529'
      '293b'
      ''
      '435245415445205441424c4520554542455257454953554e47454e2028'
      
        '2020494420696e7428313129204e4f54204e554c4c206175746f5f696e637265' +
        '6d656e742c'
      
        '202055574e554d20696e7428313129204e4f54204e554c4c2064656661756c74' +
        '202730272c'
      
        '202041525420656e756d282755272c274c2729204e4f54204e554c4c20646566' +
        '61756c74202755272c'
      '20204645525449472074696e79696e742831292064656661756c74202730272c'
      
        '20204a4f55524e414c5f494420696e7428313129204e4f54204e554c4c206465' +
        '6661756c74202730272c'
      '202055575f444154554d20646174652064656661756c74204e554c4c2c'
      
        '202042455452414720666c6f61742831302c32292064656661756c742027302e' +
        '3030272c'
      '20204b544f2076617263686172283230292064656661756c74204e554c4c2c'
      '2020424c5a20766172636861722838292064656661756c74204e554c4c2c'
      
        '202042494e48414245522076617263686172283530292064656661756c74204e' +
        '554c4c2c'
      
        '202055575f54455854207661726368617228323530292064656661756c74204e' +
        '554c4c2c'
      '20205052494d415259204b455920202849442c55574e554d29'
      '293b'
      ''
      '435245415445205441424c4520564552545241472028'
      
        '20205245435f494420696e7428313129204e4f54204e554c4c206175746f5f69' +
        '6e6372656d656e742c'
      
        '2020414444525f494420696e7428313129204e4f54204e554c4c206465666175' +
        '6c7420272d31272c'
      
        '20205656544e554d20696e7428313129204e4f54204e554c4c2064656661756c' +
        '7420272d31272c'
      
        '20205645525452455445525f494420696e7428313129204e4f54204e554c4c20' +
        '64656661756c7420272d31272c'
      
        '2020474c4f4252414241545420666c6f61742831302c3229204e4f54204e554c' +
        '4c2064656661756c742027302e3030272c'
      
        '2020444154554d5f53544152542064617465204e4f54204e554c4c2064656661' +
        '756c742027303030302d30302d3030272c'
      
        '2020444154554d5f454e44452064617465204e4f54204e554c4c206465666175' +
        '6c742027303030302d30302d3030272c'
      
        '2020444154554d5f4e4558542064617465204e4f54204e554c4c206465666175' +
        '6c742027303030302d30302d3030272c'
      
        '2020494e54455256414c4c2063686172283129204e4f54204e554c4c20646566' +
        '61756c7420274d272c'
      
        '2020494e54455256414c4c5f4e554d2074696e79696e7428332920756e736967' +
        '6e6564204e4f54204e554c4c2064656661756c74202731272c'
      
        '2020414b5449565f464c414720656e756d28274e272c27592729204e4f54204e' +
        '554c4c2064656661756c7420274e272c'
      
        '20205052494e545f464c414720656e756d28274e272c27592729204e4f54204e' +
        '554c4c2064656661756c7420274e272c'
      
        '202050525f4542454e452074696e79696e742834292064656661756c74204e55' +
        '4c4c2c'
      
        '20204c4945464152542074696e79696e742832292064656661756c74204e554c' +
        '4c2c'
      
        '20205a41484c4152542074696e79696e742832292064656661756c74204e554c' +
        '4c2c'
      
        '20204b4f53545f4e4554544f20666c6f61742831302c3229204e4f54204e554c' +
        '4c2064656661756c742027302e3030272c'
      
        '2020574552545f4e4554544f20666c6f61742831302c3229204e4f54204e554c' +
        '4c2064656661756c742027302e3030272c'
      
        '20204c4f484e20666c6f61742831302c3229204e4f54204e554c4c2064656661' +
        '756c742027302e3030272c'
      
        '20205741524520666c6f61742831302c3229204e4f54204e554c4c2064656661' +
        '756c742027302e3030272c'
      
        '2020544b4f535420666c6f61742831302c3229204e4f54204e554c4c20646566' +
        '61756c742027302e3030272c'
      
        '20204d5753545f3020666c6f61742831302c3229204e4f54204e554c4c206465' +
        '6661756c742027302e3030272c'
      
        '20204d5753545f3120666c6f61742831302c3229204e4f54204e554c4c206465' +
        '6661756c742027302e3030272c'
      
        '20204d5753545f3220666c6f61742831302c3229204e4f54204e554c4c206465' +
        '6661756c742027302e3030272c'
      
        '20204d5753545f3320666c6f61742831302c3229204e4f54204e554c4c206465' +
        '6661756c742027302e3030272c'
      
        '20204e53554d4d4520666c6f61742831302c3229204e4f54204e554c4c206465' +
        '6661756c742027302e3030272c'
      
        '20204d53554d4d455f3020666c6f61742831302c3229204e4f54204e554c4c20' +
        '64656661756c742027302e3030272c'
      
        '20204d53554d4d455f3120666c6f61742831302c3229204e4f54204e554c4c20' +
        '64656661756c742027302e3030272c'
      
        '20204d53554d4d455f3220666c6f61742831302c3229204e4f54204e554c4c20' +
        '64656661756c742027302e3030272c'
      
        '20204d53554d4d455f3320666c6f61742831302c3229204e4f54204e554c4c20' +
        '64656661756c742027302e3030272c'
      
        '20204d53554d4d4520666c6f61742831302c3229204e4f54204e554c4c206465' +
        '6661756c742027302e3030272c'
      
        '20204253554d4d4520666c6f61742831302c3229204e4f54204e554c4c206465' +
        '6661756c742027302e3030272c'
      
        '2020415453554d4d4520666c6f61742831302c3229204e4f54204e554c4c2064' +
        '656661756c742027302e3030272c'
      
        '202041544d53554d4d4520666c6f61742831302c3229204e4f54204e554c4c20' +
        '64656661756c742027302e3030272c'
      
        '20205741454852554e472076617263686172283529204e4f54204e554c4c2064' +
        '656661756c742027272c'
      
        '2020474547454e4b4f4e544f20696e7428313129204e4f54204e554c4c206465' +
        '6661756c7420272d31272c'
      
        '2020534f4c4c5f53544147452074696e79696e74283429204e4f54204e554c4c' +
        '2064656661756c74202730272c'
      
        '2020534f4c4c5f534b4f4e544f20666c6f617428352c3229204e4f54204e554c' +
        '4c2064656661756c742027302e3030272c'
      
        '2020534f4c4c5f4e544147452074696e79696e74283429204e4f54204e554c4c' +
        '2064656661756c74202730272c'
      
        '2020534f4c4c5f524154454e2074696e79696e74283429204e4f54204e554c4c' +
        '2064656661756c74202731272c'
      
        '2020534f4c4c5f5241544245545220666c6f61742831302c3229204e4f54204e' +
        '554c4c2064656661756c742027302e3030272c'
      
        '2020534f4c4c5f524154494e54455256414c4c20696e7428313129204e4f5420' +
        '4e554c4c2064656661756c74202730272c'
      
        '20205354414449554d2074696e79696e74283429204e4f54204e554c4c206465' +
        '6661756c74202730272c'
      '202045525354454c4c5420646174652064656661756c74204e554c4c2c'
      
        '2020455253545f4e414d452076617263686172283230292064656661756c7420' +
        '4e554c4c2c'
      
        '20204b554e5f4e554d2076617263686172283230292064656661756c74204e55' +
        '4c4c2c'
      
        '20204b554e5f414e524544452076617263686172283430292064656661756c74' +
        '204e554c4c2c'
      
        '20204b554e5f4e414d45312076617263686172283430292064656661756c7420' +
        '4e554c4c2c'
      
        '20204b554e5f4e414d45322076617263686172283430292064656661756c7420' +
        '4e554c4c2c'
      
        '20204b554e5f4e414d45332076617263686172283430292064656661756c7420' +
        '4e554c4c2c'
      
        '20204b554e5f41425445494c554e472076617263686172283430292064656661' +
        '756c74204e554c4c2c'
      
        '20204b554e5f535452415353452076617263686172283430292064656661756c' +
        '74204e554c4c2c'
      
        '20204b554e5f4c414e4420766172636861722835292064656661756c74204e55' +
        '4c4c2c'
      
        '20204b554e5f504c5a2076617263686172283130292064656661756c74204e55' +
        '4c4c2c'
      
        '20204b554e5f4f52542076617263686172283430292064656661756c74204e55' +
        '4c4c2c'
      '2020555352312076617263686172283830292064656661756c74204e554c4c2c'
      '2020555352322076617263686172283830292064656661756c74204e554c4c2c'
      
        '202050524f4a454b542076617263686172283830292064656661756c74204e55' +
        '4c4c2c'
      
        '20204f52474e554d2076617263686172283230292064656661756c74204e554c' +
        '4c2c'
      
        '2020424553545f4e414d452076617263686172283430292064656661756c7420' +
        '4e554c4c2c'
      
        '2020424553545f434f44452074696e79696e742834292064656661756c74204e' +
        '554c4c2c'
      '2020424553545f444154554d20646174652064656661756c74204e554c4c2c'
      '2020494e464f20746578742c'
      
        '202042525554544f5f464c414720656e756d28274e272c27592729204e4f5420' +
        '4e554c4c2064656661756c7420274e272c'
      
        '20204d5753545f465245495f464c414720656e756d28274e272c27592729204e' +
        '4f54204e554c4c2064656661756c7420274e272c'
      '20205052494d415259204b45592020285245435f4944292c'
      '20204b455920414444525f49442028414444525f494429'
      '293b'
      ''
      '435245415445205441424c452056455254524147504f532028'
      
        '20205245435f494420696e7428313129204e4f54204e554c4c206175746f5f69' +
        '6e6372656d656e742c'
      
        '20204a4f55524e414c5f494420696e7428313129204e4f54204e554c4c206465' +
        '6661756c74202730272c'
      
        '2020415254494b454c5459502063686172283129204e4f54204e554c4c206465' +
        '6661756c742027272c'
      
        '2020415254494b454c5f494420696e7428313129204e4f54204e554c4c206465' +
        '6661756c7420272d31272c'
      
        '2020414444525f494420696e7428313129204e4f54204e554c4c206465666175' +
        '6c7420272d31272c'
      
        '20205656544e554d20696e7428313129204e4f54204e554c4c2064656661756c' +
        '7420272d31272c'
      
        '2020504f534954494f4e20696e7428313129204e4f54204e554c4c2064656661' +
        '756c74202730272c'
      '2020564945575f504f5320636861722833292064656661756c74204e554c4c2c'
      
        '20204d41544348434f44452076617263686172283230292064656661756c7420' +
        '4e554c4c2c'
      
        '20204152544e554d2076617263686172283230292064656661756c74204e554c' +
        '4c2c'
      
        '2020424152434f44452076617263686172283230292064656661756c74204e55' +
        '4c4c2c'
      
        '20204d454e474520666c6f61742831302c3229204e4f54204e554c4c20646566' +
        '61756c742027302e3030272c'
      
        '20204c41454e47452076617263686172283230292064656661756c74204e554c' +
        '4c2c'
      
        '202047524f455353452076617263686172283230292064656661756c74204e55' +
        '4c4c2c'
      
        '202044494d454e53494f4e2076617263686172283230292064656661756c7420' +
        '4e554c4c2c'
      
        '20204745574943485420666c6f61742831302c3329204e4f54204e554c4c2064' +
        '656661756c742027302e303030272c'
      
        '20204d455f45494e484549542076617263686172283130292064656661756c74' +
        '204e554c4c2c'
      
        '202050525f45494e4845495420666c6f61742831302c3329204e4f54204e554c' +
        '4c2064656661756c742027312e303030272c'
      
        '202045505245495320666c6f61742831302c3329204e4f54204e554c4c206465' +
        '6661756c742027302e303030272c'
      
        '2020455f52474557494e4e20666c6f61742831302c3229204e4f54204e554c4c' +
        '2064656661756c742027302e3030272c'
      
        '202052414241545420666c6f61742831302c3229204e4f54204e554c4c206465' +
        '6661756c742027302e3030272c'
      
        '20205241424154543220666c6f61742831302c3229204e4f54204e554c4c2064' +
        '656661756c742027302e3030272c'
      
        '20205241424154543320666c6f61742831302c3229204e4f54204e554c4c2064' +
        '656661756c742027302e3030272c'
      
        '20205354455545525f434f44452074696e79696e74283429204e4f54204e554c' +
        '4c2064656661756c74202730272c'
      
        '2020414c545445494c5f50524f5a20666c6f61742831302c3229204e4f54204e' +
        '554c4c2064656661756c742027302e3130272c'
      
        '2020414c545445494c5f5354434f44452074696e79696e74283429204e4f5420' +
        '4e554c4c2064656661756c74202730272c'
      
        '2020474547454e4b544f20696e7428313129204e4f54204e554c4c2064656661' +
        '756c7420272d31272c'
      '202042455a454943484e554e4720746578742c'
      
        '2020414c545445494c5f464c414720656e756d28274e272c27592729204e4f54' +
        '204e554c4c2064656661756c7420274e272c'
      
        '202042455a5f464553545f464c414720656e756d28274e272c27592729204e4f' +
        '54204e554c4c2064656661756c7420274e272c'
      
        '202042525554544f5f464c414720656e756d28274e272c27592729204e4f5420' +
        '4e554c4c2064656661756c7420274e272c'
      '20205052494d415259204b45592020285245435f4944292c'
      '20204b455920415254494b454c5f49442028415254494b454c5f4944292c'
      '20204b455920414444525f49442028414444525f4944292c'
      
        '20204b4559204a4f55524e414c5f494420284a4f55524e414c5f49442c504f53' +
        '4954494f4e29'
      '293b'
      ''
      '435245415445205441424c45205645525452455445522028'
      
        '20205645525452455445525f494420696e7428313129204e4f54204e554c4c20' +
        '6175746f5f696e6372656d656e742c'
      
        '202056455254525f4e554d4d4552207661726368617228313029206465666175' +
        '6c74204e554c4c2c'
      
        '2020564e414d452076617263686172283330292064656661756c74204e554c4c' +
        '2c'
      '20204e414d452076617263686172283330292064656661756c74204e554c4c2c'
      
        '2020446174756d4e6575206461746574696d652064656661756c74204e554c4c' +
        '2c'
      
        '2020446174756d4265617262656974656e206461746574696d65206465666175' +
        '6c74204e554c4c2c'
      
        '202042656e75747a65724e65752076617263686172283530292064656661756c' +
        '74204e554c4c2c'
      
        '202042656e75747a65724265617262656974656e207661726368617228353029' +
        '2064656661756c74204e554c4c2c'
      
        '2020414e524544452076617263686172283135292064656661756c74204e554c' +
        '4c2c'
      
        '2020544954454c2076617263686172283135292064656661756c74204e554c4c' +
        '2c'
      
        '20205a555341545a2076617263686172283430292064656661756c74204e554c' +
        '4c2c'
      
        '20205a555341545a322076617263686172283430292064656661756c74204e55' +
        '4c4c2c'
      
        '20205a554841454e44454e2076617263686172283430292064656661756c7420' +
        '4e554c4c2c'
      
        '2020535452415353452076617263686172283430292064656661756c74204e55' +
        '4c4c2c'
      '20204c414e4420766172636861722835292064656661756c74204e554c4c2c'
      '2020504c5a2076617263686172283130292064656661756c74204e554c4c2c'
      '20204f52542076617263686172283430292064656661756c74204e554c4c2c'
      
        '202054454c45464f4e207661726368617228313030292064656661756c74204e' +
        '554c4c2c'
      '2020464158207661726368617228313030292064656661756c74204e554c4c2c'
      
        '202046554e4b207661726368617228313030292064656661756c74204e554c4c' +
        '2c'
      
        '2020454d41494c207661726368617228313030292064656661756c74204e554c' +
        '4c2c'
      
        '2020494e5445524e4554207661726368617228313030292064656661756c7420' +
        '4e554c4c2c'
      
        '20205350524143485f494420736d616c6c696e742836292064656661756c7420' +
        '2732272c'
      
        '202050524f564953494f4e5341525420636861722831292064656661756c7420' +
        '4e554c4c2c'
      
        '20204142524543484e554e47535a45495450554e4b5420656e756d282752272c' +
        '275a27292064656661756c7420275a272c'
      
        '202050524f564953494f4e4d49545452414e53504f525420656e756d28275927' +
        '2c274e27292064656661756c7420274e272c'
      
        '202050524f564953494f4e5341545a20666c6f617428352c3229206465666175' +
        '6c74204e554c4c2c'
      
        '20204c45545a54454142524543484e554e4720646174652064656661756c7420' +
        '4e554c4c2c'
      
        '2020554d5341545a20666c6f61742831322c32292064656661756c74204e554c' +
        '4c2c'
      
        '202050524f564953494f4e20666c6f61742831302c32292064656661756c7420' +
        '4e554c4c2c'
      
        '20204245534348414546544947554e475341525420736d616c6c696e74283629' +
        '2064656661756c74204e554c4c2c'
      
        '20204245534348414546544947554e47534752414420736d616c6c696e742836' +
        '292064656661756c74204e554c4c2c'
      
        '20204755454c5449475f564f4e206461746574696d652064656661756c74204e' +
        '554c4c2c'
      
        '20204755454c5449475f424953206461746574696d652064656661756c74204e' +
        '554c4c2c'
      
        '2020474542444154554d206461746574696d652064656661756c74204e554c4c' +
        '2c'
      
        '202047455343484c4543485420656e756d28274d272c27572729204e4f54204e' +
        '554c4c2064656661756c7420274d272c'
      
        '202046414d5354414e4420736d616c6c696e742836292064656661756c74204e' +
        '554c4c2c'
      '202042414e4b2076617263686172283430292064656661756c74204e554c4c2c'
      
        '20204b544f5f494e48414245522076617263686172283430292064656661756c' +
        '74204e554c4c2c'
      '2020424c5a2076617263686172283130292064656661756c74204e554c4c2c'
      '20204b544f2076617263686172283230292064656661756c74204e554c4c2c'
      '202042454d45524b554e4720746578742c'
      
        '202045525354454c4c54206461746574696d652064656661756c74204e554c4c' +
        '2c'
      
        '202045525354454c4c545f4e414d452076617263686172283230292064656661' +
        '756c74204e554c4c2c'
      '2020474541454e44206461746574696d652064656661756c74204e554c4c2c'
      
        '2020474541454e445f4e414d452076617263686172283230292064656661756c' +
        '74204e554c4c2c'
      
        '20205553455246454c445f303120766172636861722832353529206465666175' +
        '6c74204e554c4c2c'
      
        '20205553455246454c445f303220766172636861722832353529206465666175' +
        '6c74204e554c4c2c'
      
        '20205553455246454c445f303320766172636861722832353529206465666175' +
        '6c74204e554c4c2c'
      
        '20205553455246454c445f303420766172636861722832353529206465666175' +
        '6c74204e554c4c2c'
      
        '20205553455246454c445f303520766172636861722832353529206465666175' +
        '6c74204e554c4c2c'
      
        '20205553455246454c445f303620766172636861722832353529206465666175' +
        '6c74204e554c4c2c'
      
        '20205553455246454c445f303720766172636861722832353529206465666175' +
        '6c74204e554c4c2c'
      
        '20205553455246454c445f303820766172636861722832353529206465666175' +
        '6c74204e554c4c2c'
      
        '20205553455246454c445f303920766172636861722832353529206465666175' +
        '6c74204e554c4c2c'
      
        '20205553455246454c445f313020766172636861722832353529206465666175' +
        '6c74204e554c4c2c'
      '20205052494d415259204b45592020285645525452455445525f4944292c'
      
        '2020554e49515545204b4559204944585f56455f4e554d202856455254525f4e' +
        '554d4d455229'
      '293b'
      ''
      '435245415445205441424c4520574152454e4752555050454e2028'
      
        '2020494420696e7428313129204e4f54204e554c4c206175746f5f696e637265' +
        '6d656e742c'
      
        '2020544f505f494420696e7428313129204e4f54204e554c4c2064656661756c' +
        '7420272d31272c'
      
        '20204e414d4520766172636861722832353029204e4f54204e554c4c20646566' +
        '61756c742027272c'
      '2020424553434852454942554e4720746578742c'
      '20204445465f454b544f20696e74283131292064656661756c7420272d31272c'
      '20204445465f414b544f20696e74283131292064656661756c7420272d31272c'
      
        '20205354455545525f434f44452074696e79696e7428342920756e7369676e65' +
        '64204e4f54204e554c4c2064656661756c74202732272c'
      
        '2020564b315f46414b544f5220666c6f617428362c3529204e4f54204e554c4c' +
        '2064656661756c742027302e3030303030272c'
      
        '2020564b325f46414b544f5220666c6f617428362c3529204e4f54204e554c4c' +
        '2064656661756c742027302e3030303030272c'
      
        '2020564b335f46414b544f5220666c6f617428362c3529204e4f54204e554c4c' +
        '2064656661756c742027302e3030303030272c'
      
        '2020564b345f46414b544f5220666c6f617428362c3529204e4f54204e554c4c' +
        '2064656661756c742027302e3030303030272c'
      
        '2020564b355f46414b544f5220666c6f617428362c3529204e4f54204e554c4c' +
        '2064656661756c742027302e3030303030272c'
      '2020564f52474142454e20746578742c'
      '20205052494d415259204b4559202028494429'
      '293b'
      ''
      '2f2a2042415349532d444154454e202a2f'
      ''
      
        '494e5345525420494e544f20574152454e4752555050454e2056414c55455328' +
        '2231222c20222d31222c2022417274696b656c2031222c204e554c4c2c20222d' +
        '31222c20222d31222c202232222c2022302e3030303030222c2022302e303030' +
        '3030222c2022302e3030303030222c2022302e3030303030222c2022302e3030' +
        '303030222c204e554c4c293b'
      
        '494e5345525420494e544f20574152454e4752555050454e2056414c55455328' +
        '2232222c20222d31222c2022417274696b656c2032222c204e554c4c2c20222d' +
        '31222c20222d31222c202232222c2022302e3030303030222c2022302e303030' +
        '3030222c2022302e3030303030222c2022302e3030303030222c2022302e3030' +
        '303030222c204e554c4c293b'
      
        '494e5345525420494e544f20574152454e4752555050454e2056414c55455328' +
        '2233222c20222d31222c2022417274696b656c2033222c204e554c4c2c20222d' +
        '31222c20222d31222c202232222c2022302e3030303030222c2022302e303030' +
        '3030222c2022302e3030303030222c2022302e3030303030222c2022302e3030' +
        '303030222c204e554c4c293b'
      
        '494e5345525420494e544f20574152454e4752555050454e2056414c55455328' +
        '2234222c20222d31222c2022417274696b656c2034222c204e554c4c2c20222d' +
        '31222c20222d31222c202232222c2022302e3030303030222c2022302e303030' +
        '3030222c2022302e3030303030222c2022302e3030303030222c2022302e3030' +
        '303030222c204e554c4c293b'
      
        '494e5345525420494e544f20574152454e4752555050454e2056414c55455328' +
        '2235222c20222d31222c2022417274696b656c2035222c204e554c4c2c20222d' +
        '31222c20222d31222c202232222c2022302e3030303030222c2022302e303030' +
        '3030222c2022302e3030303030222c2022302e3030303030222c2022302e3030' +
        '303030222c204e554c4c293b'
      
        '494e5345525420494e544f20574152454e4752555050454e2056414c55455328' +
        '2236222c20222d31222c2022417274696b656c2036222c204e554c4c2c20222d' +
        '31222c20222d31222c202232222c2022302e3030303030222c2022302e303030' +
        '3030222c2022302e3030303030222c2022302e3030303030222c2022302e3030' +
        '303030222c204e554c4c293b'
      
        '494e5345525420494e544f20574152454e4752555050454e2056414c55455328' +
        '2237222c20222d31222c2022417274696b656c2037222c204e554c4c2c20222d' +
        '31222c20222d31222c202232222c2022302e3030303030222c2022302e303030' +
        '3030222c2022302e3030303030222c2022302e3030303030222c2022302e3030' +
        '303030222c204e554c4c293b'
      
        '494e5345525420494e544f20574152454e4752555050454e2056414c55455328' +
        '2238222c20222d31222c2022417274696b656c2038222c204e554c4c2c20222d' +
        '31222c20222d31222c202232222c2022302e3030303030222c2022302e303030' +
        '3030222c2022302e3030303030222c2022302e3030303030222c2022302e3030' +
        '303030222c204e554c4c293b'
      
        '494e5345525420494e544f20574152454e4752555050454e2056414c55455328' +
        '2239222c20222d31222c2022417274696b656c2039222c204e554c4c2c20222d' +
        '31222c20222d31222c202232222c2022302e3030303030222c2022302e303030' +
        '3030222c2022302e3030303030222c2022302e3030303030222c2022302e3030' +
        '303030222c204e554c4c293b'
      
        '494e5345525420494e544f20574152454e4752555050454e2056414c55455328' +
        '223130222c20222d31222c2022417274696b656c203130222c204e554c4c2c20' +
        '222d31222c20222d31222c202232222c2022302e3030303030222c2022302e30' +
        '30303030222c2022302e3030303030222c2022302e3030303030222c2022302e' +
        '3030303030222c204e554c4c293b'
      
        '494e5345525420494e544f20574152454e4752555050454e2056414c55455328' +
        '223131222c20222d31222c2022417274696b656c203131222c204e554c4c2c20' +
        '222d31222c20222d31222c202232222c2022302e3030303030222c2022302e30' +
        '30303030222c2022302e3030303030222c2022302e3030303030222c2022302e' +
        '3030303030222c204e554c4c293b'
      
        '494e5345525420494e544f20574152454e4752555050454e2056414c55455328' +
        '223132222c20222d31222c2022417274696b656c203132222c204e554c4c2c20' +
        '222d31222c20222d31222c202232222c2022302e3030303030222c2022302e30' +
        '30303030222c2022302e3030303030222c2022302e3030303030222c2022302e' +
        '3030303030222c204e554c4c293b'
      
        '494e5345525420494e544f20574152454e4752555050454e2056414c55455328' +
        '223133222c20222d31222c2022417274696b656c203133222c204e554c4c2c20' +
        '222d31222c20222d31222c202232222c2022302e3030303030222c2022302e30' +
        '30303030222c2022302e3030303030222c2022302e3030303030222c2022302e' +
        '3030303030222c204e554c4c293b'
      
        '494e5345525420494e544f20574152454e4752555050454e2056414c55455328' +
        '223134222c20222d31222c2022417274696b656c203134222c204e554c4c2c20' +
        '222d31222c20222d31222c202232222c2022302e3030303030222c2022302e30' +
        '30303030222c2022302e3030303030222c2022302e3030303030222c2022302e' +
        '3030303030222c204e554c4c293b'
      
        '494e5345525420494e544f20574152454e4752555050454e2056414c55455328' +
        '223135222c20222d31222c2022417274696b656c203135222c204e554c4c2c20' +
        '222d31222c20222d31222c202232222c2022302e3030303030222c2022302e30' +
        '30303030222c2022302e3030303030222c2022302e3030303030222c2022302e' +
        '3030303030222c204e554c4c293b'
      
        '494e5345525420494e544f20574152454e4752555050454e2056414c55455328' +
        '223136222c20222d31222c2022417274696b656c203136222c204e554c4c2c20' +
        '222d31222c20222d31222c202232222c2022302e3030303030222c2022302e30' +
        '30303030222c2022302e3030303030222c2022302e3030303030222c2022302e' +
        '3030303030222c204e554c4c293b'
      
        '494e5345525420494e544f20574152454e4752555050454e2056414c55455328' +
        '223137222c20222d31222c2022417274696b656c203137222c204e554c4c2c20' +
        '222d31222c20222d31222c202232222c2022302e3030303030222c2022302e30' +
        '30303030222c2022302e3030303030222c2022302e3030303030222c2022302e' +
        '3030303030222c204e554c4c293b'
      
        '494e5345525420494e544f20574152454e4752555050454e2056414c55455328' +
        '223138222c20222d31222c2022417274696b656c203138222c204e554c4c2c20' +
        '222d31222c20222d31222c202232222c2022302e3030303030222c2022302e30' +
        '30303030222c2022302e3030303030222c2022302e3030303030222c2022302e' +
        '3030303030222c204e554c4c293b'
      
        '494e5345525420494e544f20574152454e4752555050454e2056414c55455328' +
        '223139222c20222d31222c2022417274696b656c203139222c204e554c4c2c20' +
        '222d31222c20222d31222c202232222c2022302e3030303030222c2022302e30' +
        '30303030222c2022302e3030303030222c2022302e3030303030222c2022302e' +
        '3030303030222c204e554c4c293b'
      
        '494e5345525420494e544f20574152454e4752555050454e2056414c55455328' +
        '223230222c20222d31222c2022417274696b656c203230222c204e554c4c2c20' +
        '222d31222c20222d31222c202232222c2022302e3030303030222c2022302e30' +
        '30303030222c2022302e3030303030222c2022302e3030303030222c2022302e' +
        '3030303030222c204e554c4c293b'
      ''
      '435245415445205441424c452057415254554e472028'
      
        '20205245435f494420696e7428313129204e4f54204e554c4c206175746f5f69' +
        '6e6372656d656e742c'
      
        '2020414444525f494420696e7428313129204e4f54204e554c4c206465666175' +
        '6c74202730272c'
      
        '2020424553434852454942554e47207661726368617228323535292064656661' +
        '756c74204e554c4c2c'
      '202057415254554e4720646174652064656661756c74204e554c4c2c'
      '2020494e54455256414c4c20696e74283131292064656661756c74202730272c'
      
        '202057564552545241472076617263686172283230292064656661756c74204e' +
        '554c4c2c'
      '202042454d45524b554e4720746578742c'
      '20204c4542454e534c41554620746578742c'
      '20204c4953544520746578742c'
      
        '202057415254554e475f5459502076617263686172283230292064656661756c' +
        '74204e554c4c2c'
      
        '202057564552545241475f4e5220736d616c6c696e742836292064656661756c' +
        '74202730272c'
      
        '2020534f4c4c5a4549545f4b5720666c6f617428332c31292064656661756c74' +
        '204e554c4c2c'
      
        '2020534f4c4c5a4549545f475720666c6f617428332c31292064656661756c74' +
        '204e554c4c2c'
      
        '2020454e544645524e554e4720696e742831312920756e7369676e6564206465' +
        '6661756c74204e554c4c2c'
      
        '202042454d5f4f5054312074696e79696e7428312920756e7369676e65642064' +
        '656661756c74202730272c'
      
        '202042454d5f4f5054322074696e79696e7428312920756e7369676e65642064' +
        '656661756c74202730272c'
      
        '202042454d5f4f5054332074696e79696e7428312920756e7369676e65642064' +
        '656661756c74202730272c'
      
        '202042454d5f4f5054342074696e79696e7428312920756e7369676e65642064' +
        '656661756c74202730272c'
      
        '202042454d5f4f5054352074696e79696e7428312920756e7369676e65642064' +
        '656661756c74202730272c'
      
        '202042454d5f4f5054362074696e79696e7428312920756e7369676e65642064' +
        '656661756c74202730272c'
      
        '202042454d5f4f5054372074696e79696e7428312920756e7369676e65642064' +
        '656661756c74202730272c'
      
        '202042454d5f4f5054382074696e79696e7428312920756e7369676e65642064' +
        '656661756c74202730272c'
      
        '202042454d5f4f5054392074696e79696e7428312920756e7369676e65642064' +
        '656661756c74202730272c'
      '202045525354454c4c5420646174652064656661756c74204e554c4c2c'
      
        '2020455253545f4e414d452076617263686172283230292064656661756c7420' +
        '4e554c4c2c'
      '2020474541454e4445525420646174652064656661756c74204e554c4c2c'
      
        '2020474541454e445f4e414d452076617263686172283230292064656661756c' +
        '74204e554c4c2c'
      '20205052494d415259204b45592020285245435f494429'
      '293b'
      ''
      ''
      
        '494e5345525420494e544f20464942555f4b4f4e54454e2056414c5545532822' +
        '31303030222c20224b61737365222c202230222c202230222c202230222c2022' +
        '30222c202230222c202230222c202230222c2022302e303022293b'
      
        '494e5345525420494e544f20464942555f4b4f4e54454e2056414c5545532822' +
        '31323030222c202242616e6b222c202230222c202230222c202230222c202230' +
        '222c202230222c202230222c202230222c2022302e303022293b'
      
        '494e5345525420494e544f20464942555f4b4f4e54454e2056414c5545532822' +
        '33343030222c202241756677656e64756e67656e2066fc7220576172656e222c' +
        '202230222c202230222c202230222c202230222c202230222c202230222c2022' +
        '30222c2022302e303022293b'
      
        '494e5345525420494e544f20464942555f4b4f4e54454e2056414c5545532822' +
        '38343030222c2022556d7361747a65726cf6736520576172656e222c20223022' +
        '2c202230222c202230222c202230222c202230222c202230222c202230222c20' +
        '22302e303022293b'
      ''
      
        '494e5345525420494e544f204c414e442056414c55455328224445222c202244' +
        '6575747363686c616e64222c2022444555222c202235222c202230303439222c' +
        '202280222c20224422293b'
      
        '494e5345525420494e544f204c414e442056414c55455328224154222c2022d6' +
        '737465727265696368222c2022415554222c202235222c204e554c4c2c202241' +
        '5453222c20224422293b'
      
        '494e5345525420494e544f204c414e442056414c55455328224348222c202253' +
        '63687765697a222c2022434845222c202231222c204e554c4c2c202243484622' +
        '2c20224422293b'
      
        '494e5345525420494e544f204c414e442056414c55455328224652222c202246' +
        '72616e6b7265696368222c2022465241222c202231222c204e554c4c2c202246' +
        '4652222c20224622293b'
      
        '494e5345525420494e544f204c414e442056414c55455328224742222c202245' +
        '6e676c616e64222c2022474252222c202231222c204e554c4c2c204e554c4c2c' +
        '20224522293b'
      ''
      
        '494e5345525420494e544f205241424154544752555050454e2056414c554553' +
        '28222d222c202235222c202231222c202230222c2022302e3030222c2022302e' +
        '3030222c2022302e3030222c20222d31222c20226b65696e6520526162617474' +
        '67727570706522293b'
      ''
      
        '494e5345525420494e544f205245474953544552592056414c55455328224d41' +
        '494e222c202244454641554c54222c2022616c6c672e2050726f6772616d6d65' +
        '696e7374656c6c756e67656e222c204e554c4c2c204e554c4c2c204e554c4c2c' +
        '204e554c4c2c204e554c4c2c204e554c4c2c204e554c4c2c202231222c20224e' +
        '222c20224e222c2022323030333039313431393233343722293b'
      
        '494e5345525420494e544f205245474953544552592056414c55455328225553' +
        '455253455454494e4753222c202244454641554c54222c202262656e75747a65' +
        '72646566696e69657274652045696e7374656c6c756e67656e222c204e554c4c' +
        '2c204e554c4c2c204e554c4c2c204e554c4c2c204e554c4c2c204e554c4c2c20' +
        '4e554c4c2c202231222c20224e222c20224e222c202232303033303931343139' +
        '3233313822293b'
      
        '494e5345525420494e544f205245474953544552592056414c55455328224d41' +
        '494e5c5c4c41594f5554222c202244454641554c54222c20225370616c74656e' +
        '6272656974656e2c204e414d456e206574632e222c204e554c4c2c204e554c4c' +
        '2c204e554c4c2c204e554c4c2c204e554c4c2c204e554c4c2c204e554c4c2c20' +
        '2231222c20224e222c20224e222c202232303033303931343139323431352229' +
        '3b'
      
        '494e5345525420494e544f205245474953544552592056414c55455328224d41' +
        '494e5c5c5245504f5254222c202244454641554c54222c20224c61796f757420' +
        '6465722042656c656765222c204e554c4c2c204e554c4c2c204e554c4c2c204e' +
        '554c4c2c204e554c4c2c204e554c4c2c204e554c4c2c202231222c20224e222c' +
        '20224e222c2022323030333039313431393234323522293b'
      
        '494e5345525420494e544f205245474953544552592056414c55455328224d41' +
        '494e5c5c5741454852554e47222c2022444d222c20224465757473636865204d' +
        '61726b222c204e554c4c2c204e554c4c2c204e554c4c2c204e554c4c2c202231' +
        '222c204e554c4c2c204e554c4c2c202231222c20224e222c20224e222c202232' +
        '3030333039313431393235303922293b'
      
        '494e5345525420494e544f205245474953544552592056414c55455328224d41' +
        '494e5c5c5741454852554e47222c202280222c20224575726f222c204e554c4c' +
        '2c204e554c4c2c204e554c4c2c204e554c4c2c2022312e3935353833222c204e' +
        '554c4c2c204e554c4c2c202231222c20224e222c20224e222c20223230303330' +
        '39313431393235313022293b'
      
        '494e5345525420494e544f205245474953544552592056414c55455328224d41' +
        '494e5c5c5741454852554e47222c202224222c2022555320446f6c6c6172222c' +
        '204e554c4c2c204e554c4c2c204e554c4c2c204e554c4c2c2022322e32222c20' +
        '4e554c4c2c204e554c4c2c202231222c20224e222c20224e222c202232303033' +
        '3039313431393235313022293b'
      
        '494e5345525420494e544f205245474953544552592056414c55455328224d41' +
        '494e222c20224c4549545741454852554e47222c202280222c204e554c4c2c20' +
        '4e554c4c2c204e554c4c2c204e554c4c2c204e554c4c2c204e554c4c2c204e55' +
        '4c4c2c202231222c20224e222c20224e222c2022323030333039313431393233' +
        '343622293b'
      
        '494e5345525420494e544f205245474953544552592056414c55455328224d41' +
        '494e5c5c4c494546415254222c2022445044222c204e554c4c2c204e554c4c2c' +
        '202231222c204e554c4c2c204e554c4c2c204e554c4c2c204e554c4c2c204e55' +
        '4c4c2c204e554c4c2c20224e222c20224e222c20223230303330393134313932' +
        '34313622293b'
      
        '494e5345525420494e544f205245474953544552592056414c55455328224d41' +
        '494e5c5c4c494546415254222c2022667265692048617573222c204e554c4c2c' +
        '204e554c4c2c202232222c204e554c4c2c204e554c4c2c204e554c4c2c204e55' +
        '4c4c2c204e554c4c2c204e554c4c2c20224e222c20224e222c20223230303330' +
        '39313431393234313622293b'
      
        '494e5345525420494e544f205245474953544552592056414c55455328224d41' +
        '494e5c5c4c494546415254222c2022506f73742d4e6163686e61686d65222c20' +
        '4e554c4c2c204e554c4c2c202233222c204e554c4c2c204e554c4c2c204e554c' +
        '4c2c204e554c4c2c204e554c4c2c204e554c4c2c20224e222c20224e222c2022' +
        '323030333039313431393234313622293b'
      
        '494e5345525420494e544f205245474953544552592056414c55455328224d41' +
        '494e5c5c4c494546415254222c2022506f7374222c204e554c4c2c204e554c4c' +
        '2c202234222c204e554c4c2c204e554c4c2c204e554c4c2c204e554c4c2c204e' +
        '554c4c2c204e554c4c2c20224e222c20224e222c202232303033303931343139' +
        '3234313622293b'
      
        '494e5345525420494e544f205245474953544552592056414c55455328224d41' +
        '494e5c5c4c494546415254222c2022555053222c204e554c4c2c204e554c4c2c' +
        '202235222c204e554c4c2c204e554c4c2c204e554c4c2c204e554c4c2c204e55' +
        '4c4c2c204e554c4c2c20224e222c20224e222c20223230303330393134313932' +
        '34313722293b'
      
        '494e5345525420494e544f205245474953544552592056414c55455328224d41' +
        '494e5c5c4c494546415254222c20225550532d4e6163686e61686d65222c204e' +
        '554c4c2c204e554c4c2c202236222c204e554c4c2c204e554c4c2c204e554c4c' +
        '2c204e554c4c2c204e554c4c2c204e554c4c2c20224e222c20224e222c202232' +
        '3030333039313431393234313722293b'
      
        '494e5345525420494e544f205245474953544552592056414c55455328224d41' +
        '494e5c5c4c494546415254222c202253656c6273746162686f6c756e67222c20' +
        '4e554c4c2c204e554c4c2c202237222c204e554c4c2c204e554c4c2c204e554c' +
        '4c2c204e554c4c2c204e554c4c2c204e554c4c2c20224e222c20224e222c2022' +
        '323030333039313431393234313722293b'
      
        '494e5345525420494e544f205245474953544552592056414c55455328224d41' +
        '494e5c5c414444525f484952222c2022616c6c6520416472657373656e222c20' +
        '4e554c4c2c204e554c4c2c202230222c204e554c4c2c204e554c4c2c204e554c' +
        '4c2c204e554c4c2c204e554c4c2c204e554c4c2c20224e222c20224e222c2022' +
        '323030333039313431393234313222293b'
      
        '494e5345525420494e544f205245474953544552592056414c55455328224d41' +
        '494e5c5c414444525f484952222c20224b756e64656e2031222c204e554c4c2c' +
        '204e554c4c2c202231222c204e554c4c2c204e554c4c2c204e554c4c2c20224b' +
        '554e44454e4752555050453d31222c204e554c4c2c204e554c4c2c20224e222c' +
        '20224e222c2022323030333039313431393234313222293b'
      
        '494e5345525420494e544f205245474953544552592056414c55455328224d41' +
        '494e5c5c414444525f484952222c20224b756e64656e2032222c204e554c4c2c' +
        '204e554c4c2c202232222c204e554c4c2c204e554c4c2c204e554c4c2c20224b' +
        '554e44454e4752555050453d32222c204e554c4c2c204e554c4c2c20224e222c' +
        '20224e222c2022323030333039313431393234313222293b'
      
        '494e5345525420494e544f205245474953544552592056414c55455328224d41' +
        '494e5c5c414444525f484952222c20224b756e64656e2033222c204e554c4c2c' +
        '204e554c4c2c202233222c204e554c4c2c204e554c4c2c204e554c4c2c20224b' +
        '554e44454e4752555050453d33222c204e554c4c2c204e554c4c2c20224e222c' +
        '20224e222c2022323030333039313431393234313322293b'
      
        '494e5345525420494e544f205245474953544552592056414c55455328224d41' +
        '494e5c5c414444525f484952222c20224b756e64656e2034222c204e554c4c2c' +
        '204e554c4c2c202234222c204e554c4c2c204e554c4c2c204e554c4c2c20224b' +
        '554e44454e4752555050453d34222c204e554c4c2c204e554c4c2c20224e222c' +
        '20224e222c2022323030333039313431393234313322293b'
      
        '494e5345525420494e544f205245474953544552592056414c55455328224d41' +
        '494e5c5c414444525f484952222c20224b756e64656e2035222c204e554c4c2c' +
        '204e554c4c2c202235222c204e554c4c2c204e554c4c2c204e554c4c2c20224b' +
        '554e44454e4752555050453d35222c204e554c4c2c204e554c4c2c20224e222c' +
        '20224e222c2022323030333039313431393234313322293b'
      
        '494e5345525420494e544f205245474953544552592056414c55455328224d41' +
        '494e5c5c414444525f484952222c20224b756e64656e2036222c204e554c4c2c' +
        '204e554c4c2c202236222c204e554c4c2c204e554c4c2c204e554c4c2c20224b' +
        '554e44454e4752555050453d36222c204e554c4c2c204e554c4c2c20224e222c' +
        '20224e222c2022323030333039313431393234313322293b'
      
        '494e5345525420494e544f205245474953544552592056414c55455328224d41' +
        '494e5c5c414444525f484952222c20224b756e64656e2037222c204e554c4c2c' +
        '204e554c4c2c202237222c204e554c4c2c204e554c4c2c204e554c4c2c20224b' +
        '554e44454e4752555050453d37222c204e554c4c2c204e554c4c2c20224e222c' +
        '20224e222c2022323030333039313431393234313422293b'
      
        '494e5345525420494e544f205245474953544552592056414c55455328224d41' +
        '494e5c5c414444525f484952222c20224b756e64656e2038222c204e554c4c2c' +
        '204e554c4c2c202238222c204e554c4c2c204e554c4c2c204e554c4c2c20224b' +
        '554e44454e4752555050453d38222c204e554c4c2c204e554c4c2c20224e222c' +
        '20224e222c2022323030333039313431393234313422293b'
      
        '494e5345525420494e544f205245474953544552592056414c55455328224d41' +
        '494e5c5c414444525f484952222c20224b756e64656e2039222c204e554c4c2c' +
        '204e554c4c2c202239222c204e554c4c2c204e554c4c2c204e554c4c2c20224b' +
        '554e44454e4752555050453d39222c204e554c4c2c204e554c4c2c20224e222c' +
        '20224e222c2022323030333039313431393234313422293b'
      
        '494e5345525420494e544f205245474953544552592056414c55455328224d41' +
        '494e5c5c414444525f484952222c20224b756e64656e203130222c204e554c4c' +
        '2c204e554c4c2c20223130222c204e554c4c2c204e554c4c2c204e554c4c2c20' +
        '224b554e44454e4752555050453d3130222c204e554c4c2c204e554c4c2c2022' +
        '4e222c20224e222c2022323030333039313431393234313422293b'
      
        '494e5345525420494e544f205245474953544552592056414c55455328224d41' +
        '494e5c5c414444525f484952222c20224c6965666572616e74656e222c204e55' +
        '4c4c2c204e554c4c2c20223131222c204e554c4c2c204e554c4c2c204e554c4c' +
        '2c20224b554e44454e4752555050453d3131222c204e554c4c2c204e554c4c2c' +
        '20224e222c20224e222c2022323030333039313431393234313522293b'
      
        '494e5345525420494e544f205245474953544552592056414c55455328224d41' +
        '494e5c5c5a41484c415254222c2022566f726b61737365222c202230222c204e' +
        '554c4c2c202230222c20223330222c204e554c4c2c202230222c204e554c4c2c' +
        '20224265747261672070657220566f726b6173736520657268616c74656e222c' +
        '204e554c4c2c20224e222c20224e222c20223230303330393134313932333339' +
        '22293b'
      
        '494e5345525420494e544f205245474953544552592056414c55455328224d41' +
        '494e5c5c5a41484c415254222c2022426172222c202231222c204e554c4c2c20' +
        '2231222c20223330222c204e554c4c2c202233222c204e554c4c2c2022426574' +
        '7261672042415220657268616c74656e222c204e554c4c2c20224e222c20224e' +
        '222c2022323030333039313431393233333822293b'
      
        '494e5345525420494e544f205245474953544552592056414c55455328224d41' +
        '494e5c5c5a41484c415254222c2022dc62657277656973756e672042616e6b22' +
        '2c202232222c204e554c4c2c20223134222c20223330222c204e554c4c2c2022' +
        '32222c204e554c4c2c2022dc62657277656973756e672042616e6b222c204e55' +
        '4c4c2c20224e222c20224e222c2022323030333039313431393233333822293b'
      
        '494e5345525420494e544f205245474953544552592056414c55455328224d41' +
        '494e5c5c5a41484c415254222c20224e6163686e61686d65222c202233222c20' +
        '4e554c4c2c20223134222c20223330222c204e554c4c2c202232222c204e554c' +
        '4c2c20224265747261672070657220506f73742d4e6163686e61686d65206572' +
        '68616c74656e222c204e554c4c2c20224e222c20224e222c2022323030333039' +
        '313431393233333722293b'
      
        '494e5345525420494e544f205245474953544552592056414c55455328224d41' +
        '494e5c5c5a41484c415254222c2022555053204e6163686e61686d65222c2022' +
        '34222c204e554c4c2c20223134222c20223330222c204e554c4c2c202232222c' +
        '204e554c4c2c202242657472616720706572205550532d4e6163686e61686d65' +
        '20657268616c74656e222c204e554c4c2c20224e222c20224e222c2022323030' +
        '333039313431393233333722293b'
      
        '494e5345525420494e544f205245474953544552592056414c55455328224d41' +
        '494e5c5c5a41484c415254222c202253636865636b222c202235222c204e554c' +
        '4c2c20223134222c20223330222c204e554c4c2c202232222c204e554c4c2c20' +
        '22426574726167207065722053636865636b20657268616c74656e222c204e55' +
        '4c4c2c20224e222c20224e222c2022323030333039313431393233333622293b'
      
        '494e5345525420494e544f205245474953544552592056414c55455328224d41' +
        '494e5c5c5a41484c415254222c202245432d4b41525445222c202236222c204e' +
        '554c4c2c20223134222c20223330222c204e554c4c2c202232222c204e554c4c' +
        '2c2022426574726167207065722045432d4b415254452065696e67657a6f6765' +
        '6e222c204e554c4c2c20224e222c20224e222c20223230303330393134313932' +
        '33333622293b'
      
        '494e5345525420494e544f205245474953544552592056414c55455328224d41' +
        '494e5c5c5a41484c415254222c20224b72656469746b61727465222c20223722' +
        '2c204e554c4c2c20223134222c20223330222c204e554c4c2c202232222c204e' +
        '554c4c2c2022426574726167207769726420706572204b72656469746b617274' +
        '652065696e67657a6f67656e222c204e554c4c2c20224e222c20224e222c2022' +
        '323030333039313431393233333522293b'
      
        '494e5345525420494e544f205245474953544552592056414c55455328224d41' +
        '494e5c5c5a41484c415254222c202250617950616c222c202238222c204e554c' +
        '4c2c202230222c202237222c204e554c4c2c202230222c204e554c4c2c202242' +
        '6574726167207065722050617950616c20657268616c74656e222c204e554c4c' +
        '2c20224e222c20224e222c2022323030333039313431393233333522293b'
      
        '494e5345525420494e544f205245474953544552592056414c55455328224d41' +
        '494e5c5c5a41484c415254222c20224c61737473636872696674222c20223922' +
        '2c204e554c4c2c20223134222c20223330222c204e554c4c2c202232222c204e' +
        '554c4c2c20225769722062756368656e2064656e2042657472616720766f6e20' +
        '696872656d204b6f6e746f20254b544f4e522520424c5a2025424c5a25206162' +
        '2e222c204e554c4c2c20224e222c20224e222c20223230303330393134313932' +
        '38343222293b'
      
        '494e5345525420494e544f205245474953544552592056414c55455328224d41' +
        '494e5c5c4e554d42455253222c2022564b2d415546222c202230303030303022' +
        '2c204e554c4c2c202238222c2022323330303030222c202236222c204e554c4c' +
        '2c204e554c4c2c204e554c4c2c202233222c20224e222c20224e222c20223230' +
        '30333039313431393330303422293b'
      
        '494e5345525420494e544f205245474953544552592056414c55455328224d41' +
        '494e5c5c4e554d42455253222c2022564b2d414742222c202230303030303022' +
        '2c204e554c4c2c202231222c2022323330303030222c202236222c204e554c4c' +
        '2c204e554c4c2c204e554c4c2c202233222c20224e222c20224e222c20223230' +
        '30333039313431393330303622293b'
      
        '494e5345525420494e544f205245474953544552592056414c55455328224d41' +
        '494e5c5c4e554d42455253222c2022564b2d4c494546222c2022303030303030' +
        '222c204e554c4c2c202232222c2022323330303030222c202236222c204e554c' +
        '4c2c204e554c4c2c204e554c4c2c202233222c20224e222c20224e222c202232' +
        '3030333039313431393330303722293b'
      
        '494e5345525420494e544f205245474953544552592056414c55455328224d41' +
        '494e5c5c4e554d42455253222c2022564b2d52454348222c2022303030303030' +
        '222c204e554c4c2c202233222c2022323330303030222c202236222c204e554c' +
        '4c2c204e554c4c2c204e554c4c2c202233222c20224e222c20224e222c202232' +
        '3030333039313431393330303722293b'
      
        '494e5345525420494e544f205245474953544552592056414c55455328224d41' +
        '494e5c5c4e554d42455253222c2022454b2d52454348222c2022303030303030' +
        '222c204e554c4c2c202235222c2022323330303030222c202236222c204e554c' +
        '4c2c204e554c4c2c204e554c4c2c202233222c20224e222c20224e222c202232' +
        '3030333039313431393330303822293b'
      
        '494e5345525420494e544f205245474953544552592056414c55455328224d41' +
        '494e5c5c4e554d42455253222c202245444954222c2022303030303030222c20' +
        '4e554c4c2c20223130222c202231303030222c202236222c204e554c4c2c204e' +
        '554c4c2c204e554c4c2c202233222c20224e222c20224e222c20223230303330' +
        '39313431393330313522293b'
      
        '494e5345525420494e544f205245474953544552592056414c55455328224d41' +
        '494e5c5c4e554d42455253222c20224b554e4e554d222c202230303030303022' +
        '2c204e554c4c2c20223939222c202235303030222c202236222c204e554c4c2c' +
        '204e554c4c2c204e554c4c2c202233222c20224e222c20224e222c2022323030' +
        '333039313431393234323022293b'
      
        '494e5345525420494e544f205245474953544552592056414c55455328224d41' +
        '494e5c5c4e554d42455253222c2022564b2d4b41535345222c20223030303022' +
        '2c204e554c4c2c20223232222c202231303030222c202234222c204e554c4c2c' +
        '204e554c4c2c204e554c4c2c202233222c20224e222c20224e222c2022323030' +
        '333039313431393234323122293b'
      
        '494e5345525420494e544f205245474953544552592056414c55455328224d41' +
        '494e5c5c4e554d42455253222c2022454b2d42455354222c202230303030222c' +
        '204e554c4c2c202236222c202231303030222c202234222c204e554c4c2c204e' +
        '554c4c2c204e554c4c2c202233222c20224e222c20224e222c20223230303330' +
        '39313431393234323122293b'
      
        '494e5345525420494e544f205245474953544552592056414c55455328224d41' +
        '494e5c5c4e554d42455253222c2022554542455257454953554e47222c202230' +
        '303030222c204e554c4c2c2022313130222c202231303030222c202234222c20' +
        '4e554c4c2c204e554c4c2c204e554c4c2c202233222c20224e222c20224e222c' +
        '2022323030333039313431393234323422293b'
      
        '494e5345525420494e544f205245474953544552592056414c55455328224d41' +
        '494e5c5c4e554d42455253222c20224b52442d4e554d222c2022303030303030' +
        '222c204e554c4c2c20223330222c20223735303030222c202236222c204e554c' +
        '4c2c204e554c4c2c204e554c4c2c202233222c20224e222c20224e222c202232' +
        '3030333039313431393234323422293b'
      
        '494e5345525420494e544f205245474953544552592056414c55455328224d41' +
        '494e5c5c4e554d42455253222c20224445422d4e554d222c2022303030303030' +
        '222c204e554c4c2c20223331222c20223135303030222c202236222c204e554c' +
        '4c2c204e554c4c2c204e554c4c2c202233222c20224e222c20224e222c202232' +
        '3030333039313431393234323422293b'
      
        '494e5345525420494e544f205245474953544552592056414c55455328224d41' +
        '494e5c5c4e554d42455253222c202256455254524147222c2022303030303030' +
        '222c204e554c4c2c20223233222c202231303030222c202236222c204e554c4c' +
        '2c204e554c4c2c204e554c4c2c202233222c20224e222c20224e222c20223230' +
        '30333039313431393234323522293b'
      
        '494e5345525420494e544f205245474953544552592056414c55455328224d41' +
        '494e5c5c4e554d42455253222c2022415254494b454c4e554d4d4552222c2022' +
        '303030303030222c204e554c4c2c20223938222c202231303030222c20223130' +
        '222c204e554c4c2c204e554c4c2c204e554c4c2c202233222c20224e222c2022' +
        '4e222c2022323030333039313832313536343422293b'
      
        '494e5345525420494e544f205245474953544552592056414c55455328224d41' +
        '494e5c5c4d575354222c202230222c20226f686e65204d775374222c204e554c' +
        '4c2c204e554c4c2c204e554c4c2c204e554c4c2c202230222c204e554c4c2c20' +
        '4e554c4c2c204e554c4c2c20224e222c20224e222c2022323030333039313431' +
        '393234313722293b'
      
        '494e5345525420494e544f205245474953544552592056414c55455328224d41' +
        '494e5c5c4d575354222c202231222c202265726d2e204d7753742d5361747a22' +
        '2c204e554c4c2c204e554c4c2c204e554c4c2c204e554c4c2c202237222c204e' +
        '554c4c2c204e554c4c2c204e554c4c2c20224e222c20224e222c202232303033' +
        '3039313431393234313822293b'
      
        '494e5345525420494e544f205245474953544552592056414c55455328224d41' +
        '494e5c5c4d575354222c202232222c2022766f6c6c6572204d7753742d536174' +
        '7a222c204e554c4c2c204e554c4c2c204e554c4c2c204e554c4c2c2022313622' +
        '2c204e554c4c2c204e554c4c2c204e554c4c2c20224e222c20224e222c202232' +
        '3030333039313431393234313822293b'
      
        '494e5345525420494e544f205245474953544552592056414c55455328224d41' +
        '494e5c5c4d575354222c202233222c202252657365727665222c204e554c4c2c' +
        '204e554c4c2c204e554c4c2c204e554c4c2c202230222c204e554c4c2c204e55' +
        '4c4c2c204e554c4c2c20224e222c20224e222c20223230303330393134313932' +
        '34313822293b'
      
        '494e5345525420494e544f205245474953544552592056414c55455328224d41' +
        '494e5c5c5245504f5254222c2022564b5f4147425f4d41494c5f5355424a4543' +
        '54222c204e554c4c2c204e554c4c2c204e554c4c2c204e554c4c2c204e554c4c' +
        '2c204e554c4c2c204e554c4c2c204e554c4c2c202231222c20224e222c20224e' +
        '222c2022323030333039313431393132353722293b'
      
        '494e5345525420494e544f205245474953544552592056414c55455328224d41' +
        '494e5c5c5245504f5254222c2022564b5f524543485f4d41494c5f5355424a45' +
        '4354222c204e554c4c2c204e554c4c2c204e554c4c2c204e554c4c2c204e554c' +
        '4c2c204e554c4c2c204e554c4c2c204e554c4c2c202231222c20224e222c2022' +
        '4e222c2022323030333039313431393132353722293b'
      
        '494e5345525420494e544f205245474953544552592056414c55455328224d41' +
        '494e5c5c5245504f5254222c2022564b5f4c4945465f4d41494c5f5355424a45' +
        '4354222c204e554c4c2c204e554c4c2c204e554c4c2c204e554c4c2c204e554c' +
        '4c2c204e554c4c2c204e554c4c2c204e554c4c2c202231222c20224e222c2022' +
        '4e222c2022323030333039313431393132353722293b'
      
        '494e5345525420494e544f205245474953544552592056414c55455328224d41' +
        '494e5c5c5245504f5254222c2022454b5f424553545f4d41494c5f5355424a45' +
        '4354222c204e554c4c2c204e554c4c2c204e554c4c2c204e554c4c2c204e554c' +
        '4c2c204e554c4c2c204e554c4c2c204e554c4c2c202231222c20224e222c2022' +
        '4e222c2022323030333039313431393132353722293b'
      
        '494e5345525420494e544f2052454749535445525920284d41494e4b45592c20' +
        '4e414d452c2056414c5f494e542c2056414c5f545950292056616c7565732028' +
        '274d41494e5c5c50494d272c2027474c4f425f555345524944272c202d312c20' +
        '273327293b'
      
        '494e5345525420494e544f205245474953544552592056414c55455328224b41' +
        '535345222c2022424f4e5f55454245525343485249465431222c204e554c4c2c' +
        '204e554c4c2c204e554c4c2c204e554c4c2c204e554c4c2c204e554c4c2c204e' +
        '554c4c2c204e554c4c2c202231222c20224e222c20224e222c20223230303330' +
        '39313431393132353722293b'
      
        '494e5345525420494e544f205245474953544552592056414c55455328224b41' +
        '535345222c2022424f4e5f55454245525343485249465432222c204e554c4c2c' +
        '204e554c4c2c204e554c4c2c204e554c4c2c204e554c4c2c204e554c4c2c204e' +
        '554c4c2c204e554c4c2c202231222c20224e222c20224e222c20223230303330' +
        '39313431393132353722293b'
      
        '494e5345525420494e544f205245474953544552592056414c55455328224b41' +
        '535345222c2022424f4e5f55454245525343485249465433222c204e554c4c2c' +
        '204e554c4c2c204e554c4c2c204e554c4c2c204e554c4c2c204e554c4c2c204e' +
        '554c4c2c204e554c4c2c202231222c20224e222c20224e222c20223230303330' +
        '39313431393132353722293b'
      
        '494e5345525420494e544f205245474953544552592056414c55455328224b41' +
        '535345222c2022445255434b4552504f5254222c20224c505432222c204e554c' +
        '4c2c204e554c4c2c204e554c4c2c204e554c4c2c204e554c4c2c204e554c4c2c' +
        '204e554c4c2c202231222c20224e222c20224e222c2022323030333039313431' +
        '393132353722293b'
      
        '494e5345525420494e544f205245474953544552592056414c55455328224d41' +
        '494e5c5c414452455353454e222c20224b544f5f4c454e222c204e554c4c2c20' +
        '4e554c4c2c20223130222c204e554c4c2c204e554c4c2c204e554c4c2c204e55' +
        '4c4c2c204e554c4c2c202233222c20224e222c20224e222c2022323030333039' +
        '313431393132353722293b'
      
        '494e5345525420494e544f205245474953544552592056414c55455328224d41' +
        '494e5c5c414452455353454e222c2022424c5a5f4c454e222c204e554c4c2c20' +
        '4e554c4c2c202238222c204e554c4c2c204e554c4c2c204e554c4c2c204e554c' +
        '4c2c204e554c4c2c202233222c20224e222c20224e222c202232303033303931' +
        '3431393132353722293b'
      
        '494e5345525420494e544f205245474953544552592056414c55455328224d41' +
        '494e5c5c5245504f5254222c20225a41484c554e47535a49454c5f534f464f52' +
        '545f54455854222c2022536f666f7274222c204e554c4c2c204e554c4c2c204e' +
        '554c4c2c204e554c4c2c204e554c4c2c204e554c4c2c204e554c4c2c20223122' +
        '2c20224e222c20224e222c2022323030333039313431393132353722293b'
      
        '494e5345525420494e544f205245474953544552592056414c55455328225348' +
        '4f505c5c4c4945464152545f4d4150222c202244454641554c54222c20224d61' +
        '7070696e672066fc72204c6965666572617274656e222c204e554c4c2c204e55' +
        '4c4c2c204e554c4c2c204e554c4c2c204e554c4c2c204e554c4c2c204e554c4c' +
        '2c202231222c20224e222c20224e222c20223230303330353133323134323432' +
        '22293b'
      
        '494e5345525420494e544f205245474953544552592056414c55455328225348' +
        '4f505c5c5a41484c4152545f4d4150222c202244454641554c54222c20224d61' +
        '7070696e672066fc72205a61686c756e6773617274656e222c204e554c4c2c20' +
        '4e554c4c2c204e554c4c2c204e554c4c2c204e554c4c2c204e554c4c2c204e55' +
        '4c4c2c202231222c20224e222c20224e222c2022323030333035313332313432' +
        '343222293b'
      
        '494e5345525420494e544f205245474953544552592056414c55455328225348' +
        '4f50222c202244454641554c54222c2022616c6c672e2053686f7065696e7374' +
        '656c6c756e67656e222c204e554c4c2c204e554c4c2c204e554c4c2c204e554c' +
        '4c2c204e554c4c2c204e554c4c2c204e554c4c2c202231222c20224e222c2022' +
        '4e222c2022323030333039313431393132353722293b'
      
        '494e5345525420494e544f205245474953544552592056414c55455328224d41' +
        '494e222c202244425f56455253494f4e222c2022312e3039222c204e554c4c2c' +
        '204e554c4c2c204e554c4c2c204e554c4c2c204e554c4c2c204e554c4c2c204e' +
        '554c4c2c202231222c20224e222c20224e222c20223230303330393138323135' +
        '36343422293b'
      
        '494e5345525420494e544f205245474953544552592056414c55455328225348' +
        '4f50222c202244454641554c545f4445424e554d222c204e554c4c2c204e554c' +
        '4c2c202230222c204e554c4c2c204e554c4c2c204e554c4c2c204e554c4c2c20' +
        '4e554c4c2c202233222c20224e222c20224e222c202232303033303931343139' +
        '3133313322293b'
      
        '494e5345525420494e544f205245474953544552592056414c55455328224b41' +
        '535345222c2022424f4e445255434b4552222c204e554c4c2c204e554c4c2c20' +
        '2231222c204e554c4c2c204e554c4c2c204e554c4c2c204e554c4c2c204e554c' +
        '4c2c202233222c20224e222c20224e222c202232303033303931343139313331' +
        '3322293b'
      
        '494e5345525420494e544f205245474953544552592056414c55455328224b41' +
        '535345222c2022445255434b4449414c4f47222c204e554c4c2c204e554c4c2c' +
        '202231222c204e554c4c2c204e554c4c2c204e554c4c2c204e554c4c2c204e55' +
        '4c4c2c202233222c20224e222c20224e222c2022323030333039313431393133' +
        '313322293b'
      
        '494e5345525420494e544f205245474953544552592056414c55455328224b41' +
        '535345222c2022534f464f5254445255434b222c204e554c4c2c204e554c4c2c' +
        '202230222c204e554c4c2c204e554c4c2c204e554c4c2c204e554c4c2c204e55' +
        '4c4c2c202233222c20224e222c20224e222c2022323030333039313431393133' +
        '313322293b'
      
        '494e5345525420494e544f205245474953544552592056414c55455328224b41' +
        '535345222c202244454641554c545f464f524d554c4152222c2022222c204e55' +
        '4c4c2c204e554c4c2c204e554c4c2c204e554c4c2c204e554c4c2c204e554c4c' +
        '2c204e554c4c2c202231222c20224e222c20224e222c20223230303330393134' +
        '31393133313322293b'
      
        '494e5345525420494e544f205245474953544552592056414c55455328224b41' +
        '535345222c202244454641554c545f445255434b4552222c202244656661756c' +
        '74222c204e554c4c2c204e554c4c2c204e554c4c2c204e554c4c2c204e554c4c' +
        '2c204e554c4c2c204e554c4c2c202231222c20224e222c20224e222c20223230' +
        '30333039313431393133313322293b'
      
        '494e5345525420494e544f205245474953544552592056414c55455328225348' +
        '4f50222c20225553455f53484f50222c204e554c4c2c204e554c4c2c20223122' +
        '2c204e554c4c2c204e554c4c2c204e554c4c2c204e554c4c2c204e554c4c2c20' +
        '2233222c20224e222c20224e222c202232303033303732303135353134372229' +
        '3b'
      
        '494e5345525420494e544f205245474953544552592056414c55455328225348' +
        '4f50222c20225553455f53484f505f4f524445524944222c204e554c4c2c204e' +
        '554c4c2c202231222c204e554c4c2c204e554c4c2c204e554c4c2c204e554c4c' +
        '2c204e554c4c2c202233222c20224e222c20224e222c20223230303330373239' +
        '31303532323222293b'
      
        '494e5345525420494e544f205245474953544552592056414c55455328225348' +
        '4f50222c202242525554544f5f53484f50222c204e554c4c2c204e554c4c2c20' +
        '2231222c204e554c4c2c204e554c4c2c204e554c4c2c204e554c4c2c204e554c' +
        '4c2c202233222c20224e222c20224e222c202232303033303732373136343934' +
        '3022293b'
      
        '494e5345525420494e544f205245474953544552592056414c55455328225348' +
        '4f50222c202253484f505452414e535f55534552222c2022222c204e554c4c2c' +
        '204e554c4c2c204e554c4c2c204e554c4c2c204e554c4c2c204e554c4c2c204e' +
        '554c4c2c202231222c20224e222c20224e222c20223230303330373232323335' +
        '35353522293b'
      
        '494e5345525420494e544f205245474953544552592056414c55455328225348' +
        '4f50222c202253484f505452414e535f534543524554222c2022222c204e554c' +
        '4c2c204e554c4c2c204e554c4c2c204e554c4c2c204e554c4c2c204e554c4c2c' +
        '204e554c4c2c202231222c20224e222c20224e222c2022323030333037323232' +
        '333535353522293b'
      
        '494e5345525420494e544f205245474953544552592056414c55455328225348' +
        '4f50222c2022494d504f52545f55524c222c2022222c204e554c4c2c204e554c' +
        '4c2c204e554c4c2c204e554c4c2c204e554c4c2c204e554c4c2c204e554c4c2c' +
        '202231222c20224e222c20224e222c2022323030333037323532303439313522' +
        '293b'
      
        '494e5345525420494e544f205245474953544552592056414c55455328225348' +
        '4f50222c20225550444154455f55524c222c2022222c204e554c4c2c204e554c' +
        '4c2c204e554c4c2c204e554c4c2c204e554c4c2c204e554c4c2c204e554c4c2c' +
        '202231222c20224e222c20224e222c2022323030333037323031353531343722' +
        '293b'
      
        '494e5345525420494e544f205245474953544552592056414c55455328225348' +
        '4f50222c20225550444154454f524445525354415455535f53454e444d41494c' +
        '222c204e554c4c2c204e554c4c2c202230222c204e554c4c2c204e554c4c2c20' +
        '4e554c4c2c204e554c4c2c204e554c4c2c202233222c20224e222c20224e222c' +
        '2022323030333037323031353531343722293b'
      
        '494e5345525420494e544f205245474953544552592056414c55455328225348' +
        '4f505c5c4f52444552535441545553222c20224f6666656e222c204e554c4c2c' +
        '204e554c4c2c202231222c204e554c4c2c204e554c4c2c204e554c4c2c204e55' +
        '4c4c2c204e554c4c2c202233222c20224e222c20224e222c2022323030333037' +
        '323132333031353622293b'
      
        '494e5345525420494e544f205245474953544552592056414c55455328225348' +
        '4f505c5c4f52444552535441545553222c2022496e204265617262656974756e' +
        '67222c204e554c4c2c204e554c4c2c202232222c204e554c4c2c204e554c4c2c' +
        '204e554c4c2c204e554c4c2c204e554c4c2c202233222c20224e222c20224e22' +
        '2c2022323030333039313431393338343022293b'
      
        '494e5345525420494e544f205245474953544552592056414c55455328225348' +
        '4f505c5c4f52444552535441545553222c202256657273656e646574222c204e' +
        '554c4c2c204e554c4c2c202233222c204e554c4c2c204e554c4c2c204e554c4c' +
        '2c204e554c4c2c204e554c4c2c202233222c20224e222c20224e222c20223230' +
        '30333039313431393339313022293b'
      
        '494e5345525420494e544f205245474953544552592056414c55455328225348' +
        '4f505c5c4c4945464152545f4d4150222c20226470222c204e554c4c2c204e55' +
        '4c4c2c202234222c204e554c4c2c204e554c4c2c204e554c4c2c204e554c4c2c' +
        '204e554c4c2c202233222c20224e222c20224e222c2022323030333037323230' +
        '313235343622293b'
      
        '494e5345525420494e544f205245474953544552592056414c55455328225348' +
        '4f505c5c4c4945464152545f4d4150222c2022757073222c204e554c4c2c204e' +
        '554c4c2c202235222c204e554c4c2c204e554c4c2c204e554c4c2c204e554c4c' +
        '2c204e554c4c2c202233222c20224e222c20224e222c20223230303330373232' +
        '32313132303222293b'
      
        '494e5345525420494e544f205245474953544552592056414c55455328225348' +
        '4f505c5c4c4945464152545f4d4150222c2022647064222c204e554c4c2c204e' +
        '554c4c2c202231222c204e554c4c2c204e554c4c2c204e554c4c2c204e554c4c' +
        '2c204e554c4c2c202233222c20224e222c20224e222c20223230303330373232' +
        '32313131353822293b'
      
        '494e5345525420494e544f205245474953544552592056414c55455328225348' +
        '4f505c5c5a41484c4152545f4d4150222c20226d6f6e65796f72646572222c20' +
        '4e554c4c2c204e554c4c2c202230222c204e554c4c2c204e554c4c2c204e554c' +
        '4c2c204e554c4c2c204e554c4c2c202233222c20224e222c20224e222c202232' +
        '3030333038303132333532333922293b'
      
        '494e5345525420494e544f205245474953544552592056414c55455328225348' +
        '4f505c5c5a41484c4152545f4d4150222c2022636f64222c204e554c4c2c204e' +
        '554c4c2c202233222c204e554c4c2c204e554c4c2c204e554c4c2c204e554c4c' +
        '2c204e554c4c2c202233222c20224e222c20224e222c20223230303330373232' +
        '32313131343122293b'
      
        '494e5345525420494e544f205245474953544552592056414c55455328225348' +
        '4f505c5c5a41484c4152545f4d4150222c202270617970616c222c204e554c4c' +
        '2c204e554c4c2c202238222c204e554c4c2c204e554c4c2c204e554c4c2c204e' +
        '554c4c2c204e554c4c2c202233222c20224e222c20224e222c20223230303330' +
        '39313431393235333922293b'
      
        '494e5345525420494e544f205245474953544552592056414c55455328225348' +
        '4f505c5c5a41484c4152545f4d4150222c202262616e6b7472616e7366657222' +
        '2c204e554c4c2c204e554c4c2c202239222c204e554c4c2c204e554c4c2c204e' +
        '554c4c2c204e554c4c2c204e554c4c2c202233222c20224e222c20224e222c20' +
        '22323030333039313431393235343022293b'
      
        '494e5345525420494e544f205245474953544552592056414c55455328225348' +
        '4f505c5c5a41484c4152545f4d4150222c20226363222c204e554c4c2c204e55' +
        '4c4c2c202237222c204e554c4c2c204e554c4c2c204e554c4c2c204e554c4c2c' +
        '204e554c4c2c202233222c20224e222c20224e222c2022323030333039313431' +
        '393235343122293b'
      ''
      
        '494e5345525420494e544f20535052414348454e2056414c554553282231222c' +
        '2022456e676c697368222c2022656e222c202232222c20224e22293b'
      
        '494e5345525420494e544f20535052414348454e2056414c554553282232222c' +
        '202244657574736368222c20226465222c202231222c20225922293b'
      
        '494e5345525420494e544f20535052414348454e2056414c554553282233222c' +
        '202245737061f16f6c222c20226573222c202233222c20224e22293b')
  end
  object FirBankTab: TZMySqlQuery
    Database = db1
    Transaction = Transact1
    CachedUpdates = False
    ShowRecordTypes = [ztModified, ztInserted, ztUnmodified]
    Options = [doHourGlass, doAutoFillDefs, doUseRowId]
    LinkOptions = [loAlwaysResync]
    Constraints = <>
    BeforePost = FirBankTabBeforePost
    ExtraOptions = [moStoreResult]
    Macros = <>
    Sql.Strings = (
      'select'
      
        'NAME as KURZBEZ, VAL_CHAR as INHABER, VAL_INT as BLZ, VAL_INT2 a' +
        's KTONR, VAL_INT3 as FIBU_KTO, MAINKEY'
      'from REGISTERY'
      'where MAINKEY='#39'MAIN\\FIRMENKONTEN'#39
      'order by VAL_INT3, KURZBEZ')
    RequestLive = True
    Left = 24
    Top = 400
    object FirBankTabkurzbez: TStringField
      DisplayLabel = 'Kurzbezeichnung'
      DisplayWidth = 20
      FieldName = 'KURZBEZ'
      Required = True
      Size = 100
    end
    object FirBankTabinhaber: TStringField
      DisplayLabel = 'Konto-Inhaber'
      FieldName = 'INHABER'
      Size = 255
    end
    object FirBankTabblz: TIntegerField
      DisplayLabel = 'Bankleitzahl'
      FieldName = 'BLZ'
    end
    object FirBankTabmainkey: TStringField
      FieldName = 'MAINKEY'
      Required = True
      Size = 255
    end
    object FirBankTabKTONR: TLargeintField
      DisplayLabel = 'Kontonummer'
      FieldName = 'KTONR'
    end
    object FirBankTabFIBU_KTO: TLargeintField
      DisplayLabel = 'Fibu-Konto'
      DisplayWidth = 6
      FieldName = 'FIBU_KTO'
      MaxValue = 999999
    end
  end
  object DbUpdTo1_0: TJvStrHolder
    Capacity = 60
    Macros = <>
    Left = 320
    Top = 456
    InternalVer = 1
    StrData = (
      ''
      '616c746572207461626c65204a4f55524e414c'
      '64726f70204e53554d4d455f302c'
      '64726f70204e53554d4d455f312c'
      '64726f70204e53554d4d455f322c'
      '64726f70204e53554d4d455f332c'
      '64726f70204253554d4d455f302c'
      '64726f70204253554d4d455f312c'
      '64726f70204253554d4d455f322c'
      '64726f70204253554d4d455f332c'
      
        '414444204d41484e53545546452054494e59494e542831292044454641554c54' +
        '202230222c'
      '414444204d41484e444154554d20444154452c'
      
        '414444204d41484e5052494e542054494e59494e542831292044454641554c54' +
        '202230223b'
      ''
      '616c746572207461626c6520524547495354455259'
      
        '6164642056414c5f494e543220696e74656765722061667465722056414c5f49' +
        '4e543b'
      ''
      '616c746572207461626c6520524547495354455259'
      
        '6164642056414c5f494e543320696e74656765722061667465722056414c5f49' +
        '4e54323b'
      ''
      '757064617465204a4f55524e414c'
      '736574204953545f4245545241473d27302e303027'
      '77686572652049534e554c4c284953545f424554524147293b'
      ''
      '757064617465204a4f55524e414c'
      '736574204953545f414e5a41484c554e473d27302e303027'
      '77686572652049534e554c4c284953545f414e5a41484c554e47293b'
      ''
      '757064617465204b465a'
      '736574204c455f4245535543483d6e756c6c'
      '7768657265204c455f4245535543483d27313930302d30312d3031273b'
      ''
      '757064617465204b465a'
      '736574204e41455f545545463d6e756c6c'
      '7768657265204e41455f545545463d27313930302d30312d3031273b'
      ''
      '757064617465204b465a'
      '736574204e45415f41553d6e756c6c'
      '7768657265204e45415f41553d27313930302d30312d3031273b'
      ''
      '757064617465204b465a'
      '736574204845525354454c4c554e473d6e756c6c'
      '7768657265204845525354454c4c554e473d27313930302d30312d3031273b'
      ''
      '757064617465204b465a'
      '736574204b415546444154554d3d6e756c6c'
      '7768657265204b415546444154554d3d27313930302d30312d3031273b'
      ''
      '757064617465204b465a'
      '736574205a554c415353554e473d6e756c6c'
      '7768657265205a554c415353554e473d27313930302d30312d3031273b'
      ''
      '757064617465204a4f55524e414c'
      '7365742055575f4e554d3d30'
      
        '7768657265205155454c4c453d3320616e64205155454c4c455f5355423d3220' +
        '616e64204a4148523e3020616e64204a4148523c3230303220616e6420524441' +
        '54554d203c2027323030322d30312d3031273b'
      ''
      
        '7265706c61636520696e746f2052454749535445525920284d41494e4b45592c' +
        '204e414d452c2056414c5f43484152292056616c7565732028274d41494e272c' +
        '202744425f56455253494f4e272c2027312e303027293b')
  end
  object UpdateArtTab: TZMySqlQuery
    Database = db1
    Transaction = Transact1
    CachedUpdates = False
    ShowRecordTypes = [ztModified, ztInserted, ztUnmodified]
    Options = [doHourGlass, doAutoFillDefs, doUseRowId]
    LinkOptions = [loAlwaysResync]
    Constraints = <>
    ExtraOptions = [moStoreResult]
    Macros = <>
    Sql.Strings = (
      'select '
      'REC_ID, WARENGRUPPE,'
      
        'EK_PREIS, VK1, VK2, VK3, VK4, VK5, VK1B, VK2B, VK3B, VK4B, VK5B,' +
        ' STEUER_CODE,'
      'MENGE_AKT, '
      'LAST_EK, LAST_LIEF, LAST_LIEFDAT, '
      'LAST_VK, LAST_KUNDE, LAST_VKDAT,'
      'ERLOES_KTO, AUFW_KTO, RABGRP_ID, MENGE_BESTELLT'
      'from ARTIKEL'
      'where REC_ID = :ID')
    RequestLive = True
    Left = 456
    Top = 136
    ParamData = <
      item
        DataType = ftInteger
        Name = 'ID'
        ParamType = ptInput
        Value = 1
      end>
  end
  object DBUpdTo1_01: TJvStrHolder
    Capacity = 60
    Macros = <>
    Left = 320
    Top = 512
    InternalVer = 1
    StrData = (
      ''
      
        '435245415445205441424c45204946204e4f5420455849535453205545424552' +
        '57454953554e47454e2028'
      
        '2020494420494e5445474552204e4f54204e554c4c2044454641554c54202730' +
        '27204155544f5f494e4352454d454e542c'
      '202055574e554d20494e5445474552204e4f54204e554c4c2c'
      '20204645525449472054494e59494e542831292044454641554c54202730272c'
      '20204a4f55524e414c5f494420494e5445474552204e4f54204e554c4c2c'
      '202055575f444154554d20444154452c'
      
        '202042455452414720464c4f41542831302c32292044454641554c5420273027' +
        '2c'
      '20204b544f2056415243484152283230292c'
      '2020424c5a20564152434841522838292c'
      '202042494e48414245522056415243484152283530292c'
      '202055575f54455854205641524348415228323530292c'
      '20205052494d415259204b4559202849442c2055574e554d29'
      '293b'
      ''
      ''
      
        '435245415445205441424c45204946204e4f5420455849535453205343485249' +
        '46545645524b4548522028'
      
        '2020494420494e5445474552204e4f54204e554c4c2044454641554c54202730' +
        '27204155544f5f494e4352454d454e542c'
      '2020414444525f494420494e54454745522044454641554c5420272d31272c'
      '2020424553434852454942554e47205641524348415228323530292c'
      '20204c414e475f5445585420544558542c'
      '20204348414e47455f4c4153542054494d455354414d502c'
      '20204348414e47455f55534552205641524348415228313030292c'
      '20205052494d415259204b45592028494429'
      '293b'
      ''
      
        '435245415445205441424c45204946204e4f5420455849535453205241424154' +
        '544752555050454e2028'
      
        '20205241424752505f4944204348415228313029204e4f54204e554c4c204445' +
        '4641554c542027272c'
      
        '20205241424752505f5459502054494e59494e54283329204e4f54204e554c4c' +
        '2044454641554c54202730272c'
      
        '20204d494e5f4d454e474520494e5445474552204e4f54204e554c4c20444546' +
        '41554c54202731272c'
      
        '20205241424154543120464c4f415428362c3229204e4f54204e554c4c204445' +
        '4641554c54202730272c'
      
        '20205241424154543220464c4f415428362c3229204e4f54204e554c4c204445' +
        '4641554c54202730272c'
      
        '20205241424154543320464c4f415428362c3229204e4f54204e554c4c204445' +
        '4641554c54202730272c'
      
        '2020414444525f494420494e5445474552204e4f54204e554c4c204445464155' +
        '4c5420272d31272c'
      '2020424553434852454942554e47205641524348415228323530292c'
      
        '20205052494d415259204b455920285241424752505f49442c20524142475250' +
        '5f5459502c204d494e5f4d454e474529'
      '293b'
      ''
      
        '5245504c41434520494e544f205245474953544552592056414c55455328274d' +
        '41494e5c5c415254494b454c272c20274445465f45524c4f45534b544f272c20' +
        '4e554c4c2c204e554c4c2c20383430302c204e554c4c2c204e554c4c2c204e55' +
        '4c4c2c204e554c4c2c204e554c4c2c204e554c4c2c204e554c4c293b'
      
        '5245504c41434520494e544f205245474953544552592056414c55455328274d' +
        '41494e5c5c415254494b454c272c20274445465f41554657414e44534b544f27' +
        '2c204e554c4c2c204e554c4c2c20333430302c204e554c4c2c204e554c4c2c20' +
        '4e554c4c2c204e554c4c2c204e554c4c2c204e554c4c2c204e554c4c293b'
      
        '5245504c41434520494e544f205245474953544552592056414c55455328274d' +
        '41494e5c5c415254494b454c272c20274445465f4c4f484e5f45524c4f45534b' +
        '544f272c204e554c4c2c204e554c4c2c20383430312c204e554c4c2c204e554c' +
        '4c2c204e554c4c2c204e554c4c2c204e554c4c2c204e554c4c2c204e554c4c29' +
        '3b'
      
        '5245504c41434520494e544f205245474953544552592056414c55455328274d' +
        '41494e5c5c415254494b454c272c20274445465f4c4f484e5f41554657414e44' +
        '534b544f272c204e554c4c2c204e554c4c2c20333430312c204e554c4c2c204e' +
        '554c4c2c204e554c4c2c204e554c4c2c204e554c4c2c204e554c4c2c204e554c' +
        '4c293b'
      ''
      
        '5245504c41434520494e544f205245474953544552592056414c55455328274d' +
        '41494e5c5c4e554d42455253272c2027454b2d42455354272c20273030303027' +
        '2c204e554c4c2c202736272c204e554c4c2c204e554c4c2c202731303030272c' +
        '204e554c4c2c204e554c4c2c204e554c4c2c204e554c4c293b'
      
        '5245504c41434520494e544f205245474953544552592056414c55455328274d' +
        '41494e5c5c4e554d42455253272c2027554542455257454953554e47272c2027' +
        '30303030272c204e554c4c2c2027313130272c204e554c4c2c204e554c4c2c20' +
        '2731303030272c204e554c4c2c204e554c4c2c204e554c4c2c204e554c4c293b'
      ''
      
        '414c544552205441424c45204a4f55524e414c2044524f50204b554e5f494458' +
        '5f4f4c443b'
      
        '414c544552205441424c45204a4f55524e414c504f532044524f50204f4c445f' +
        '4c41474e554d3b'
      
        '414c544552205441424c4520414452455353454e2044524f50204944585f4f4c' +
        '443b'
      
        '414c544552205441424c4520415254494b454c2044524f50204f4c445f494e55' +
        '4d3b'
      
        '414c544552205441424c45204b465a2044524f5020414444525f49445f4f4c44' +
        '3b'
      
        '414c544552205441424c45204a4f55524e414c504f5320414444205241424154' +
        '543220464c4f41542831302c3229204146544552205241424154543b'
      
        '414c544552205441424c45204a4f55524e414c504f5320414444205241424154' +
        '543320464c4f41542831302c322920414654455220524142415454323b'
      
        '414c544552205441424c45204a4f55524e414c20414444204c4945465f414444' +
        '525f494420496e74656765722044454641554c5420272d312720414654455220' +
        '414444525f49443b'
      
        '414c544552205441424c4520504c5a204348414e4745204e414d45204e414d45' +
        '20564152434841522834352920204e4f54204e554c4c3b'
      
        '414c544552205441424c4520424c5a204348414e47452042414e4b5f4e414d45' +
        '2042414e4b5f4e414d4520564152434841522833322920204e4f54204e554c4c' +
        '3b'
      ''
      
        '7265706c61636520696e746f2052454749535445525920284d41494e4b45592c' +
        '204e414d452c2056414c5f43484152292056616c7565732028274d41494e272c' +
        '202744425f56455253494f4e272c2027312e303127293b')
  end
  object ReKFZTab: TZMySqlQuery
    Database = db1
    Transaction = Transact1
    CachedUpdates = False
    ShowRecordTypes = [ztModified, ztInserted, ztUnmodified]
    Options = [doHourGlass, doAutoFillDefs, doUseRowId]
    LinkOptions = [loAlwaysResync]
    Constraints = <>
    ExtraOptions = [moStoreResult]
    Macros = <>
    Sql.Strings = (
      'select * from KFZ'
      'where KFZ_ID=:KID '
      'LIMIT 0,2')
    RequestLive = True
    Left = 250
    Top = 174
    ParamData = <
      item
        DataType = ftInteger
        Name = 'KID'
        ParamType = ptInput
      end>
    object ReKFZTabKFZ_ID: TIntegerField
      FieldName = 'KFZ_ID'
    end
    object ReKFZTabADDR_ID: TIntegerField
      FieldName = 'ADDR_ID'
      Required = True
    end
    object ReKFZTabFGST_NUM: TStringField
      FieldName = 'FGST_NUM'
      Required = True
    end
    object ReKFZTabPOL_KENNZ: TStringField
      FieldName = 'POL_KENNZ'
      Required = True
      Size = 10
    end
    object ReKFZTabSCHL_ZU_2: TStringField
      FieldName = 'SCHL_ZU_2'
    end
    object ReKFZTabSCHL_ZU_3: TStringField
      FieldName = 'SCHL_ZU_3'
    end
    object ReKFZTabKM_STAND: TIntegerField
      FieldName = 'KM_STAND'
    end
    object ReKFZTabZULASSUNG: TDateField
      FieldName = 'ZULASSUNG'
    end
    object ReKFZTabLE_BESUCH: TDateField
      FieldName = 'LE_BESUCH'
    end
    object ReKFZTabNAE_TUEV: TDateField
      FieldName = 'NAE_TUEV'
    end
    object ReKFZTabNAE_AU: TDateField
      FieldName = 'NAE_AU'
    end
  end
  object LiefRabGrp: TZMySqlQuery
    Database = db1
    Transaction = Transact1
    CachedUpdates = False
    ShowRecordTypes = [ztModified, ztInserted, ztUnmodified]
    Options = [doHourGlass, doAutoFillDefs, doUseRowId]
    LinkOptions = [loAlwaysResync]
    Constraints = <>
    ExtraOptions = [moStoreResult]
    Macros = <>
    Sql.Strings = (
      'select * from RABATTGRUPPEN'
      'where RABGRP_TYP=5'
      'order by RABGRP_ID')
    RequestLive = False
    Left = 188
    Top = 405
    object LiefRabGrpRABGRP_ID: TStringField
      DisplayWidth = 10
      FieldName = 'RABGRP_ID'
      Required = True
      Size = 10
    end
    object LiefRabGrpRABGRP_TYP: TIntegerField
      FieldName = 'RABGRP_TYP'
      Required = True
    end
    object LiefRabGrpMIN_MENGE: TIntegerField
      FieldName = 'MIN_MENGE'
      Required = True
      DisplayFormat = ',#0.0'
    end
    object LiefRabGrpLIEF_RABGRP: TIntegerField
      FieldName = 'LIEF_RABGRP'
      Required = True
      DisplayFormat = '00'
    end
    object LiefRabGrpRABATT1: TFloatField
      DisplayWidth = 5
      FieldName = 'RABATT1'
      Required = True
      DisplayFormat = '0"%"'
    end
    object LiefRabGrpRABATT2: TFloatField
      DisplayWidth = 5
      FieldName = 'RABATT2'
      Required = True
      DisplayFormat = '0"%"'
    end
    object LiefRabGrpRABATT3: TFloatField
      DisplayWidth = 5
      FieldName = 'RABATT3'
      Required = True
      DisplayFormat = '0"%"'
    end
    object LiefRabGrpADDR_ID: TIntegerField
      FieldName = 'ADDR_ID'
      Required = True
    end
    object LiefRabGrpBESCHREIBUNG: TStringField
      DisplayWidth = 52
      FieldName = 'BESCHREIBUNG'
      Size = 100
    end
  end
  object RabGrpDS: TDataSource
    DataSet = LiefRabGrp
    Left = 120
    Top = 408
  end
  object DBUpdTo1_02: TJvStrHolder
    Capacity = 147
    Macros = <>
    Left = 392
    Top = 400
    InternalVer = 1
    StrData = (
      ''
      
        '2f2a205461626c652073747275637475726520666f72207461626c6520276172' +
        '74696b656c5f73686f7027202a2f'
      ''
      
        '435245415445205441424c45204946204e4f542045584953545320415254494b' +
        '454c5f53484f502028'
      
        '2020494420696e7428313129204e4f54204e554c4c206175746f5f696e637265' +
        '6d656e742c'
      
        '2020544f505f494420696e7428313129204e4f54204e554c4c2044454641554c' +
        '542027393939393927202c'
      '20204152545f4e52207661726368617228353029202c'
      '20204152545f4e414d45207661726368617228353029202c'
      '20204b55525a544558542074657874202c'
      '20204c414e47544558542074657874202c'
      '2020424553434852454942554e472074657874202c'
      '20205350455a4946494b4154494f4e2074657874202c'
      '20205a554245484f45522074657874202c'
      '20205a554245484f45525f4f50542074657874202c'
      '2020494d4147455f42494720766172636861722831303029202c'
      '2020494d4147455f534d414c4c20766172636861722831303029202c'
      '20204441544153484545545f4420766172636861722831303029202c'
      '20204441544153484545545f4520766172636861722831303029202c'
      '20204b4154414c4f475f4420766172636861722831303029202c'
      '20204b4154414c4f475f4520766172636861722831303029202c'
      '202044524157494e4720766172636861722831303029202c'
      '202048414e44425543485f4420766172636861722831303029202c'
      '202048414e44425543485f4520766172636861722831303029202c'
      
        '202041555353434852454942554e475354455854207661726368617228313030' +
        '29202c'
      '202050524549535f454b20666c6f61742831322c3229202c'
      '202050524549535f4c4953544520666c6f61742831322c3229202c'
      '202050524549535f5350454320666c6f61742831322c3229202c'
      '2020454b5f52414241545420666c6f617428352c3229202c'
      '202056495349424c4520696e742831292044454641554c5420273127202c'
      '2020444154455f4e45552064617465202c'
      
        '20204641454c4c545f57454720696e742831292044454641554c542027302720' +
        '2c'
      '20204641454c4c545f574552475f41422064617465202c'
      
        '2020434c49434b5f434f554e5420696e74283131292044454641554c54202730' +
        '27202c'
      
        '202053594e432074696e79696e7428312920756e7369676e6564204445464155' +
        '4c5420273027202c'
      '20204c4153545f4950207661726368617228313629202c'
      
        '20205a55422074696e79696e7428312920756e7369676e65642044454641554c' +
        '5420273027202c'
      '20204348414e47455f44415445206461746574696d65202c'
      
        '20204348414e47455f464c41472074696e79696e7428312920756e7369676e65' +
        '64204e4f54204e554c4c2044454641554c5420273027202c'
      '20204c4945465f494420696e74283133292044454641554c5420272d3127202c'
      '20205241424752505f4944207661726368617228313029202c'
      '20205052494d415259204b45592028494429'
      '293b'
      ''
      
        '2f2a205461626c652073747275637475726520666f72207461626c6520276172' +
        '74696b656c5f73686f705f69647827202a2f'
      ''
      
        '435245415445205441424c45204946204e4f542045584953545320415254494b' +
        '454c5f53484f505f4944582028'
      
        '2020494420696e7428313129204e4f54204e554c4c206175746f5f696e637265' +
        '6d656e742c'
      
        '2020544f505f494420696e7428313129204e4f54204e554c4c2044454641554c' +
        '542027393939393927202c'
      '20204e414d4520766172636861722832353029202c'
      '202055524c20766172636861722832353029202c'
      
        '2020415254494b454c5f54454d504c4154452076617263686172283235302920' +
        '44454641554c5420276e6f726d616c2e74706c27202c'
      
        '2020434c49434b5f434f554e5420696e74283131292044454641554c54202730' +
        '27202c'
      
        '202056495349424c452074696e79696e7428312920756e7369676e6564204445' +
        '4641554c5420273127202c'
      
        '20204752555050454e5f54454d504c4154452076617263686172283235302920' +
        '44454641554c5420276772705f6e6f726d616c2e74706c27202c'
      '2020424553434852454942554e472074657874202c'
      '2020494d41474520766172636861722832353029202c'
      '20204c4153545f4950207661726368617228313629202c'
      
        '202053594e432074696e79696e7428312920756e7369676e6564204445464155' +
        '4c5420273027202c'
      '20204c4945465f494420696e74283133292044454641554c5420272d3127202c'
      '20205052494d415259204b45592028494429'
      '293b'
      ''
      
        '435245415445205441424c45204946204e4f5420455849535453205241424154' +
        '544752555050454e2028'
      
        '20205241424752505f4944207661726368617228313029204e4f54204e554c4c' +
        '2044454641554c54202727202c'
      
        '20205241424752505f5459502074696e79696e74283329204e4f54204e554c4c' +
        '2044454641554c5420273027202c'
      
        '20204d494e5f4d454e474520696e7428313129204e4f54204e554c4c20444546' +
        '41554c5420273127202c'
      
        '20204c4945465f52414247525020696e7428313029204e4f54204e554c4c2044' +
        '454641554c5420273027202c'
      
        '20205241424154543120666c6f617428362c3229204e4f54204e554c4c204445' +
        '4641554c542027302e303027202c'
      
        '20205241424154543220666c6f617428362c3229204e4f54204e554c4c204445' +
        '4641554c542027302e303027202c'
      
        '20205241424154543320666c6f617428362c3229204e4f54204e554c4c204445' +
        '4641554c542027302e303027202c'
      
        '2020414444525f494420696e7428313129204e4f54204e554c4c204445464155' +
        '4c5420272d3127202c'
      '2020424553434852454942554e4720766172636861722832353029202c'
      
        '20205052494d415259204b455920285241424752505f49442c5241424752505f' +
        '5459502c4d494e5f4d454e474529'
      '293b'
      ''
      '414c544552205441424c45205241424154544752555050454e20'
      
        '20414444204c4945465f52414247525020696e7428313029204e4f54204e554c' +
        '4c2044454641554c5420273027204146544552204d494e5f4d454e47453b'
      ''
      '414c544552205441424c4520415254494b454c2020'
      
        '20414444205241424752505f4944205641524348415228313029204445464155' +
        '4c5420272d2720414654455220574152454e4752555050452c'
      
        '204144442053484f505f415254494b454c2074696e79696e7428312920756e73' +
        '69676e6564204e4f54204e554c4c2044454641554c54202730272c'
      
        '204144442053484f505f494420696e7428313129204e4f54204e554c4c206465' +
        '6661756c7420272d31272c'
      
        '204144442053484f505f544f505f494420696e7428313129204e4f54204e554c' +
        '4c2044454641554c5420273939393939272c'
      '204144442053484f505f4152545f4e52207661726368617228353029202c'
      '204144442053484f505f4152545f4e414d45207661726368617228353029202c'
      '204144442053484f505f4b55525a544558542074657874202c'
      '204144442053484f505f4c414e47544558542074657874202c'
      '204144442053484f505f424553434852454942554e472074657874202c'
      '204144442053484f505f5350455a4946494b4154494f4e2074657874202c'
      '204144442053484f505f5a554245484f45522074657874202c'
      '204144442053484f505f5a554245484f45525f4f50542074657874202c'
      
        '204144442053484f505f494d4147455f42494720766172636861722831303029' +
        '202c'
      
        '204144442053484f505f494d4147455f534d414c4c2076617263686172283130' +
        '3029202c'
      
        '204144442053484f505f4441544153484545545f442076617263686172283130' +
        '3029202c'
      
        '204144442053484f505f4441544153484545545f452076617263686172283130' +
        '3029202c'
      
        '204144442053484f505f4b4154414c4f475f4420766172636861722831303029' +
        '202c'
      
        '204144442053484f505f4b4154414c4f475f4520766172636861722831303029' +
        '202c'
      '204144442053484f505f44524157494e4720766172636861722831303029202c'
      
        '204144442053484f505f48414e44425543485f44207661726368617228313030' +
        '29202c'
      
        '204144442053484f505f48414e44425543485f45207661726368617228313030' +
        '29202c'
      
        '204144442041555353434852454942554e475354455854207661726368617228' +
        '31303029202c'
      
        '204144442053484f505f50524549535f454b20666c6f61742831322c32292020' +
        '44454641554c54202730272c'
      
        '204144442053484f505f50524549535f4c4953544520666c6f61742831322c32' +
        '29202044454641554c54202730272c'
      
        '204144442053484f505f50524549535f5350454320666c6f61742831322c3229' +
        '202044454641554c54202730272c'
      
        '204144442053484f505f454b5f52414241545420666c6f617428352c32292020' +
        '44454641554c54202730272c'
      
        '204144442053484f505f56495349424c4520696e742831292044454641554c54' +
        '20273127202c'
      '204144442053484f505f444154455f4e45552064617465202c'
      
        '204144442053484f505f4641454c4c545f57454720696e742831292044454641' +
        '554c5420273027202c'
      '204144442053484f505f4641454c4c545f5745475f41422064617465202c'
      
        '204144442053484f505f434c49434b5f434f554e5420696e7428313129204445' +
        '4641554c5420273027202c'
      
        '204144442053484f505f53594e432074696e79696e7428312920756e7369676e' +
        '65642044454641554c5420273027202c'
      
        '204144442053484f505f5a55422074696e79696e7428312920756e7369676e65' +
        '642044454641554c5420273027202c'
      '204144442053484f505f4348414e47455f44415445206461746574696d65202c'
      
        '204144442053484f505f4348414e47455f464c41472074696e79696e74283129' +
        '20756e7369676e6564204e4f54204e554c4c2044454641554c54202730272c'
      
        '204348414e4745204b415432204b4154322054494e59494e5428342920204445' +
        '4641554c54202230222c'
      
        '204348414e4745204b415431204b4154312054494e59494e5428342920204445' +
        '4641554c54202230222c'
      
        '204348414e474520454b5f505245495320454b5f505245495320464c4f415428' +
        '31302c3329202044454641554c54202230222c'
      
        '204348414e474520564b3120564b3120464c4f41542831302c32292020444546' +
        '41554c54202230222c'
      
        '204348414e474520564b3220564b3220464c4f41542831302c32292020444546' +
        '41554c54202230222c'
      
        '204348414e474520564b3320564b3320464c4f41542831302c32292020444546' +
        '41554c54202230222c'
      
        '204348414e474520564b3420564b3420464c4f41542831302c32292020444546' +
        '41554c54202230222c'
      
        '204348414e474520564b3520564b3520464c4f41542831302c32292020444546' +
        '41554c54202230222c'
      
        '204348414e4745204d454e47455f5354415254204d454e47455f535441525420' +
        '464c4f41542831302c3229202044454641554c54202230222c'
      
        '204348414e4745204d454e47455f414b54204d454e47455f414b5420464c4f41' +
        '542831302c3229202044454641554c54202230222c'
      
        '204348414e4745204d454e47455f4d494e204d454e47455f4d494e20464c4f41' +
        '542831302c3229202044454641554c54202230222c'
      
        '204348414e4745204d454e47455f42455354454c4c54204d454e47455f424553' +
        '54454c4c5420464c4f41542831302c32292044454641554c54202230222c'
      
        '204348414e4745204d454e47455f42564f52204d454e47455f42564f5220464c' +
        '4f41542831302c3229202044454641554c54202230222c'
      
        '204348414e4745204c4153545f454b204c4153545f454b20464c4f4154283130' +
        '2c3229202044454641554c54202230222c'
      
        '204348414e4745204c4153545f564b204c4153545f564b20464c4f4154283130' +
        '2c3229202044454641554c54202230222c'
      
        '204348414e47452050525f45494e484549542050525f45494e4845495420464c' +
        '4f41542831302c3229202044454641554c54202231223b'
      ''
      ''
      '494e5345525420494e544f205241424154544752555050454e20'
      
        '20534554205241424752505f49443d272d272c20205241424752505f5459503d' +
        '2735272c20204d494e5f4d454e47453d2731272c20204c4945465f5241424752' +
        '503d2730272c2020524142415454313d27302e3030272c202052414241545432' +
        '3d27302e3030272c2020524142415454333d27302e3030272c2020414444525f' +
        '49443d272d31272c2020424553434852454942554e473d276b65696e65205261' +
        '62617474677275707065273b'
      
        '55504441544520415254494b454c20534554204c494546313d2d312c204c4945' +
        '46323d2d312c204c494546315f424e554d3d27272c204c494546325f424e554d' +
        '3d27272c204c494546315f50524549533d302c204c494546325f50524549533d' +
        '303b'
      
        '55504441544520415254494b454c20534554205241424752505f49443d272d27' +
        '2077686572652049534e554c4c285241424752505f4944293b'
      
        '555044415445204a4f55524e414c20534554204652454947414245313d273027' +
        '205748455245205155454c4c453d333b'
      ''
      
        '7265706c61636520696e746f2052454749535445525920284d41494e4b45592c' +
        '204e414d452c2056414c5f43484152292056616c7565732028274d41494e272c' +
        '202744425f56455253494f4e272c2027312e303227293b')
  end
  object ZBatchSql1: TZBatchSql
    Transaction = Transact1
    OnBeforeExecute = ZBatchSql1BeforeExecute
    OnAfterExecute = ZBatchSql1AfterExecute
    Left = 23
    Top = 126
  end
  object DBUpdTo1_03: TJvStrHolder
    Capacity = 44
    Macros = <>
    Duplicates = dupAccept
    Left = 392
    Top = 456
    InternalVer = 1
    StrData = (
      ''
      
        '435245415445205441424c45204946204e6f74204558495354532060494e5645' +
        '4e545552602028'
      
        '6049446020494e54202835292044454641554c5420273027204e4f54204e554c' +
        '4c204155544f5f494e4352454d454e542c20'
      '60444154554d602044415445204e4f54204e554c4c2c20'
      '60424553434852454942554e476020564152434841522028323530292c20'
      '60494e464f6020544558542c20'
      
        '60535441545553602054494e59494e542833292020554e5349474e4544204445' +
        '4641554c5420223022204e4f54204e554c4c2c'
      '5052494d415259204b4559286049446029'
      
        '292020434f4d4d454e54203d20224b6f7066646174656e2066fc7220496e7665' +
        '6e747572656e223b'
      ''
      ''
      
        '435245415445205441424c45204946204e4f5420455849535453206041525449' +
        '4b454c5f494e56454e545552602028'
      
        '60494e56454e5455525f49446020494e542028352920554e5349474e45442044' +
        '454641554c5420273027204e4f54204e554c4c2c20'
      
        '60415254494b454c5f49446020494e54202831312920554e5349474e45442044' +
        '454641554c5420273027204e4f54204e554c4c2c20'
      
        '60574152454e4752555050456020494e54202831312920554e5349474e454420' +
        '44454641554c54202730272c20'
      '604152544e554d6020564152434841522028323530292c'
      '604d41544348434f44456020564152434841522028323530292c'
      '60424152434f44456020564152434841522028323530292c'
      '604b55525a544558546020564152434841522028323530292c20'
      
        '604d454e47455f4953546020464c4f4154202831302c33292044454641554c54' +
        '20273027204e4f54204e554c4c2c20'
      
        '604d454e47455f534f4c4c6020464c4f4154202831302c33292044454641554c' +
        '5420273027204e4f54204e554c4c2c20'
      
        '604d454e47455f444946466020464c4f4154202831302c33292044454641554c' +
        '5420273027204e4f54204e554c4c2c20'
      
        '60494e56454e5455525f574552546020464c4f4154202831302c322920444546' +
        '41554c54202730272c20'
      
        '60454b5f50524549536020464c4f4154202831302c33292044454641554c5420' +
        '2730272c20'
      
        '60535441545553602054494e59494e542028312920554e5349474e4544204445' +
        '4641554c5420273027204e4f54204e554c4c2c20'
      '6042454152424549544554455260205641524348415220283530292c20'
      
        '5052494d415259204b45592860494e56454e5455525f4944602c60415254494b' +
        '454c5f49446029'
      
        '292020434f4d4d454e54203d2022417274696b656c646174656e2066fc722049' +
        '6e76656e747572223b'
      ''
      
        '7265706c61636520696e746f2052454749535445525920284d41494e4b45592c' +
        '204e414d452c2056414c5f43484152292056616c7565732028274d41494e272c' +
        '202744425f56455253494f4e272c2027312e303327293b')
  end
  object FirmaTab: TZMySqlQuery
    Database = db1
    Transaction = Transact1
    CachedUpdates = False
    ShowRecordTypes = [ztModified, ztInserted, ztUnmodified]
    Options = [doHourGlass, doAutoFillDefs, doUseRowId]
    LinkOptions = [loAlwaysResync]
    Constraints = <>
    OnCalcFields = FirmaTabCalcFields
    ExtraOptions = [moStoreResult]
    Macros = <>
    Sql.Strings = (
      'select * from FIRMA limit 0,1')
    RequestLive = True
    Left = 320
    Top = 16
    object FirmaTabANREDE: TStringField
      FieldName = 'ANREDE'
      Size = 250
    end
    object FirmaTabNAME1: TStringField
      FieldName = 'NAME1'
      Size = 250
    end
    object FirmaTabNAME2: TStringField
      FieldName = 'NAME2'
      Size = 250
    end
    object FirmaTabNAME3: TStringField
      FieldName = 'NAME3'
      Size = 250
    end
    object FirmaTabSTRASSE: TStringField
      FieldName = 'STRASSE'
      Size = 250
    end
    object FirmaTabLAND: TStringField
      FieldName = 'LAND'
      Size = 10
    end
    object FirmaTabPLZ: TStringField
      FieldName = 'PLZ'
      Size = 10
    end
    object FirmaTabORT: TStringField
      FieldName = 'ORT'
      Size = 250
    end
    object FirmaTabVORWAHL: TStringField
      FieldName = 'VORWAHL'
      Size = 250
    end
    object FirmaTabTELEFON1: TStringField
      FieldName = 'TELEFON1'
      Size = 250
    end
    object FirmaTabTELEFON2: TStringField
      FieldName = 'TELEFON2'
      Size = 250
    end
    object FirmaTabMOBILFUNK: TStringField
      FieldName = 'MOBILFUNK'
      Size = 250
    end
    object FirmaTabFAX: TStringField
      FieldName = 'FAX'
      Size = 250
    end
    object FirmaTabEMAIL: TStringField
      FieldName = 'EMAIL'
      Size = 250
    end
    object FirmaTabWEBSEITE: TStringField
      FieldName = 'WEBSEITE'
      Size = 250
    end
    object FirmaTabBANK1_BLZ: TStringField
      FieldName = 'BANK1_BLZ'
      Size = 8
    end
    object FirmaTabBANK1_KONTONR: TStringField
      FieldName = 'BANK1_KONTONR'
      Size = 12
    end
    object FirmaTabBANK1_NAME: TStringField
      FieldName = 'BANK1_NAME'
      Size = 250
    end
    object FirmaTabBANK1_IBAN: TStringField
      FieldName = 'BANK1_IBAN'
      Size = 100
    end
    object FirmaTabBANK1_SWIFT: TStringField
      FieldName = 'BANK1_SWIFT'
      Size = 100
    end
    object FirmaTabBANK2_BLZ: TStringField
      FieldName = 'BANK2_BLZ'
      Size = 8
    end
    object FirmaTabBANK2_KONTONR: TStringField
      FieldName = 'BANK2_KONTONR'
      Size = 12
    end
    object FirmaTabBANK2_NAME: TStringField
      FieldName = 'BANK2_NAME'
      Size = 250
    end
    object FirmaTabBANK2_IBAN: TStringField
      FieldName = 'BANK2_IBAN'
      Size = 100
    end
    object FirmaTabBANK2_SWIFT: TStringField
      FieldName = 'BANK2_SWIFT'
      Size = 100
    end
    object FirmaTabKOPFTEXT: TMemoField
      FieldName = 'KOPFTEXT'
      BlobType = ftMemo
    end
    object FirmaTabFUSSTEXT: TMemoField
      FieldName = 'FUSSTEXT'
      BlobType = ftMemo
    end
    object FirmaTabABSENDER: TStringField
      FieldName = 'ABSENDER'
      Size = 250
    end
    object FirmaTabSTEUERNUMMER: TStringField
      FieldName = 'STEUERNUMMER'
      Size = 25
    end
    object FirmaTabUST_ID: TStringField
      FieldName = 'UST_ID'
      Size = 25
    end
    object FirmaTabIMAGE1: TBlobField
      FieldName = 'IMAGE1'
      BlobType = ftBlob
    end
    object FirmaTabIMAGE2: TBlobField
      FieldName = 'IMAGE2'
      BlobType = ftBlob
    end
    object FirmaTabIMAGE3: TBlobField
      FieldName = 'IMAGE3'
      BlobType = ftBlob
    end
    object FirmaTabUSER_AKT: TStringField
      FieldKind = fkCalculated
      FieldName = 'USER_AKT'
      Size = 100
      Calculated = True
    end
    object FirmaTabLEITWAEHRUNG: TStringField
      FieldKind = fkCalculated
      FieldName = 'LEITWAEHRUNG'
      Size = 10
      Calculated = True
    end
    object FirmaTabMANDANT_NAME: TStringField
      FieldKind = fkCalculated
      FieldName = 'MANDANT_NAME'
      Size = 200
      Calculated = True
    end
  end
  object FirmaDS: TDataSource
    DataSet = FirmaTab
    Left = 256
    Top = 16
  end
  object DBUpdTo1_04: TJvStrHolder
    Capacity = 356
    Macros = <>
    Left = 400
    Top = 512
    InternalVer = 1
    StrData = (
      ''
      
        '2f2a2055706461746520746f2044422056657273696f6e20312e303420537461' +
        '6e642031312e30352e32303033202a2f'
      ''
      
        '414c544552205441424c45204a4f55524e414c504f532041444420534e20534d' +
        '414c4c494e542831292020554e5349474e45442044454641554c542022302220' +
        '4e4f54204e554c4c3b'
      ''
      '2f2a2055706461746520417274696b656c746162656c6c65202a2f'
      
        '414c544552205441424c4520415254494b454c204348414e4745204c4153545f' +
        '454b204c4153545f454b20464c4f41542831302c3329202044454641554c5420' +
        '22302e3030223b'
      ''
      ''
      
        '2f2a205570646174652053657269656e6e756d6d65726e746162656c6c65202a' +
        '2f'
      
        '414c544552205441424c4520415254494b454c5f5345524e554d204144442045' +
        '4b5f4a4f55524e414c5f494420494e5428313029202044454641554c5420222d' +
        '3122204e4f54204e554c4c3b'
      
        '414c544552205441424c4520415254494b454c5f5345524e554d204144442056' +
        '4b5f4a4f55524e414c5f494420494e5428313029202044454641554c5420222d' +
        '3122204e4f54204e554c4c3b'
      
        '414c544552205441424c4520415254494b454c5f5345524e554d20414444204c' +
        '535f4a4f55524e414c5f494420494e5428313029202044454641554c5420222d' +
        '3122204e4f54204e554c4c3b'
      
        '414c544552205441424c4520415254494b454c5f5345524e554d204144442045' +
        '4b5f4a4f55524e414c504f535f494420494e5428313029202044454641554c54' +
        '20222d3122204e4f54204e554c4c3b'
      
        '414c544552205441424c4520415254494b454c5f5345524e554d204144442056' +
        '4b5f4a4f55524e414c504f535f494420494e5428313029202044454641554c54' +
        '20222d3122204e4f54204e554c4c3b'
      
        '414c544552205441424c4520415254494b454c5f5345524e554d20414444204c' +
        '535f4a4f55524e414c504f535f494420494e5428313029202044454641554c54' +
        '20222d3122204e4f54204e554c4c3b'
      ''
      
        '2f2a20546162656c6c652066fc7220616c6c67656d65696e65204669726d656e' +
        '646174656e202a2f'
      
        '435245415445205441424c45204946204e4f5420455849535453204649524d41' +
        '2028'
      '414e5245444520564152434841522028323530292c'
      '4e414d453120564152434841522028323530292c'
      '4e414d453220564152434841522028323530292c'
      '4e414d453320564152434841522028323530292c'
      '5354524153534520564152434841522028323530292c'
      '4c414e44205641524348415220283130292c'
      '504c5a205641524348415220283130292c'
      '4f525420564152434841522028323530292c'
      '564f525741484c20564152434841522028323530292c'
      '54454c45464f4e3120564152434841522028323530292c'
      '54454c45464f4e3220564152434841522028323530292c'
      '4d4f42494c46554e4b20564152434841522028323530292c'
      '46415820564152434841522028323530292c'
      '454d41494c20564152434841522028323530292c'
      '574542534549544520564152434841522028323530292c'
      '42414e4b315f424c5a2056415243484152202838292c'
      '42414e4b315f4b4f4e544f4e52205641524348415220283132292c'
      '42414e4b315f4e414d4520564152434841522028323530292c'
      '42414e4b325f424c5a2056415243484152202838292c'
      '42414e4b325f4b4f4e544f4e52205641524348415220283132292c'
      '42414e4b325f4e414d4520564152434841522028323530292c'
      '4b4f50465445585420544558542c'
      '465553535445585420544558542c'
      '414253454e44455220564152434841522028323530292c'
      '5354455545524e554d4d45522056415243484152283235292c'
      '5553545f4944205641524348415228323529'
      
        '292020434f4d4d454e54203d2022416c6c67656d65696e65204669726d656e64' +
        '6174656e2066fc7220466f726d756c617265223b'
      ''
      ''
      '2f2a2055706461746520504c5a2d546162656c6c65202a2f'
      
        '414c544552205441424c4520504c5a20414444204c414e442056415243484152' +
        '283329202044454641554c5420224422204e4f54204e554c4c2046495253543b'
      
        '414c544552205441424c4520504c5a204144442042554e4445534c414e442056' +
        '4152434841522833293b'
      
        '414c544552205441424c4520504c5a204348414e474520564f525741484c2056' +
        '4f525741484c2056415243484152283132293b'
      
        '414c544552205441424c4520504c5a204348414e4745204e414d45204e414d45' +
        '20564152434841522835302920204e4f54204e554c4c3b'
      
        '414c544552205441424c4520504c5a204348414e474520504c5a20504c5a2056' +
        '41524348415228313129204e4f54204e554c4c3b'
      
        '414c544552205441424c4520504c5a2041444420544d50312056415243484152' +
        '28313129204e4f54204e554c4c204146544552204c414e443b'
      '55504441544520504c5a2053455420544d50313d504c5a3b'
      '414c544552205441424c4520504c5a2044524f5020504c5a3b'
      
        '414c544552205441424c4520504c5a204348414e474520544d503120504c5a20' +
        '5641524348415228313129204e4f54204e554c4c3b'
      
        '414c544552205441424c4520504c5a2044524f5020494e444558204944585f4e' +
        '414d453b'
      
        '414c544552205441424c4520504c5a2044524f5020494e444558204944585f56' +
        '4f525741484c3b'
      
        '414c544552205441424c4520504c5a2044524f50205052494d415259204b4559' +
        '2c20414444205052494d415259204b455920284c414e442c504c5a2c4e414d45' +
        '293b'
      
        '414c544552205441424c4520504c5a2041444420494e444558204944585f4e41' +
        '4d4520284c414e442c4e414d45293b'
      
        '414c544552205441424c4520504c5a2041444420494e444558204944585f564f' +
        '525741484c20284c414e442c564f525741484c293b'
      ''
      ''
      
        '2f2a206675657220442e507573742c20456e746665726e756e677366656c6420' +
        '696d204164726573732d5374616d6d202a2f'
      
        '414c544552205441424c4520414452455353454e2041444420454e544645524e' +
        '554e4720494e5420554e5349474e4544204146544552204b554e5f4745424441' +
        '54554d3b'
      ''
      ''
      '2f2a20557064617465204a6f75726e616c506f73202a2f'
      ''
      
        '414c544552205441424c45204a4f55524e414c504f5320414444205155454c4c' +
        '455f53524320494e54283131292044454641554c5420222d3122204e4f54204e' +
        '554c4c204146544552205155454c4c455f5355423b'
      ''
      
        '2f2a20557064617465205265676973746572792c20566f72626572656974756e' +
        '67206675657220676563616368746520446174656e202a2f'
      
        '414c544552205441424c45205245474953544552592041444420434143484142' +
        '4c452054494e59494e542831292020554e5349474e45442044454641554c5420' +
        '223022204e4f54204e554c4c3b'
      
        '414c544552205441424c45205245474953544552592041444420524541444f4e' +
        '4c592054494e59494e542831292020554e5349474e45442044454641554c5420' +
        '223022204e4f54204e554c4c3b'
      
        '414c544552205441424c452052454749535445525920414444204c4153545f43' +
        '48414e47452054494d455354414d50204e4f54204e554c4c3b'
      ''
      ''
      '2f2a205570646174652056657273696f6e736e756d6d6572202a2f'
      
        '44454c4554452046524f4d20524547495354455259207768657265204d41494e' +
        '4b45593d224d41494e2220616e64204e414d453d2244425f56455253494f4e22' +
        '3b'
      
        '494e5345525420494e544f2052454749535445525920284d41494e4b45592c20' +
        '4e414d452c2056414c5f43484152292056616c7565732028274d41494e272c20' +
        '2744425f56455253494f4e272c2027312e303427293b'
      ''
      
        '2f2a202d2d2d2d2d2d2d2d2d2d2d2d2d20312e302e302e353442202d2d2d2d2d' +
        '2d2d2d2d2d2a2f'
      ''
      
        '414c544552205441424c45204a4f55524e414c504f5320414444204745425543' +
        '4854312020454e554d28224e222c22592229202044454641554c5420224e2220' +
        '4e4f54204e554c4c3b'
      
        '555044415445204a4f55524e414c504f53205345542047454255434854313d22' +
        '592220776865726520474542554348543d313b'
      
        '555044415445204a4f55524e414c504f53205345542047454255434854313d22' +
        '4e222077686572652047454255434854213d313b'
      
        '414c544552205441424c45204a4f55524e414c504f532044524f502047454255' +
        '4348543b'
      
        '414c544552205441424c45204a4f55524e414c504f53204348414e4745204745' +
        '425543485431204745425543485420454e554d28274e272c2759272920204445' +
        '4641554c5420224e22204e4f54204e554c4c3b'
      ''
      
        '414c544552205441424c45204a4f55524e414c504f532041444420534e5f464c' +
        '414720454e554d28224e222c22592229202044454641554c5420224e22204e4f' +
        '54204e554c4c3b'
      
        '555044415445204a4f55524e414c504f532053455420534e5f464c41473d2259' +
        '2220776865726520534e3d313b'
      
        '555044415445204a4f55524e414c504f532053455420534e5f464c41473d224e' +
        '2220776865726520534e213d313b'
      '414c544552205441424c45204a4f55524e414c504f532044524f5020534e3b'
      ''
      
        '414c544552205441424c45204a4f55524e414c504f532044524f5020414c5454' +
        '45494c3b'
      
        '414c544552205441424c45204a4f55524e414c504f532041444420414c545445' +
        '494c5f464c414720454e554d28224e222c22592229202044454641554c542022' +
        '4e22204e4f54204e554c4c3b'
      ''
      
        '414c544552205441424c45204a4f55524e414c504f532044524f502042455a5f' +
        '464553543b'
      
        '414c544552205441424c45204a4f55524e414c504f53204144442042455a5f46' +
        '4553545f464c414720454e554d28224e222c22592229202044454641554c5420' +
        '224e22204e4f54204e554c4c3b'
      ''
      
        '414c544552205441424c45204a4f55524e414c504f53204348414e474520455f' +
        '52474557494e4e20455f52474557494e4e20464c4f41542831302c3229202044' +
        '454641554c5420223022204e4f54204e554c4c3b'
      ''
      
        '414c544552205441424c45204a4f55524e414c504f53204348414e4745204154' +
        '524e554d204154524e554d20494e5428313129202044454641554c5420222d31' +
        '22204e4f54204e554c4c3b'
      
        '414c544552205441424c45204a4f55524e414c504f53204348414e4745205652' +
        '454e554d205652454e554d20494e5428313129202044454641554c5420222d31' +
        '22204e4f54204e554c4c3b'
      
        '414c544552205441424c45204a4f55524e414c504f53204348414e474520564c' +
        '534e554d20564c534e554d20494e5428313129202044454641554c5420222d31' +
        '22204e4f54204e554c4c3b'
      
        '414c544552205441424c45204a4f55524e414c504f53204348414e4745204144' +
        '44525f494420414444525f494420494e5428313129202044454641554c542022' +
        '2d3122204e4f54204e554c4c3b'
      
        '414c544552205441424c45204a4f55524e414c504f53204348414e4745204152' +
        '54494b454c5f494420415254494b454c5f494420494e54283131292020444546' +
        '41554c5420222d3122204e4f54204e554c4c3b'
      ''
      
        '414c544552205441424c45204a4f55524e414c504f53204348414e4745205155' +
        '454c4c45205155454c4c452054494e59494e5428342920204e4f54204e554c4c' +
        '3b'
      
        '414c544552205441424c45204a4f55524e414c504f53204348414e4745205155' +
        '454c4c455f535542205155454c4c455f5355422054494e59494e542834292020' +
        '44454641554c5420223022204e4f54204e554c4c3b'
      
        '414c544552205441424c45204a4f55524e414c504f53204348414e4745204152' +
        '54494b454c54595020415254494b454c545950204348415228312920204e4f54' +
        '204e554c4c3b'
      
        '414c544552205441424c45204a4f55524e414c504f53204348414e4745204d45' +
        '4e4745204d454e474520464c4f41542831302c3229202044454641554c542022' +
        '3022204e4f54204e554c4c3b'
      
        '414c544552205441424c45204a4f55524e414c504f53204348414e4745204745' +
        '5749434854204745574943485420464c4f41542831302c332920204445464155' +
        '4c5420223022204e4f54204e554c4c3b'
      
        '414c544552205441424c45204a4f55524e414c504f53204348414e4745205052' +
        '5f45494e484549542050525f45494e4845495420464c4f41542831302c332920' +
        '2044454641554c5420223122204e4f54204e554c4c3b'
      
        '414c544552205441424c45204a4f55524e414c504f53204348414e4745204550' +
        '524549532045505245495320464c4f41542831302c3329202044454641554c54' +
        '20223022204e4f54204e554c4c3b'
      
        '414c544552205441424c45204a4f55524e414c504f53204348414e4745205241' +
        '424154542052414241545420464c4f41542831302c3229202044454641554c54' +
        '20223022204e4f54204e554c4c3b'
      
        '414c544552205441424c45204a4f55524e414c504f53204348414e4745205241' +
        '4241545432205241424154543220464c4f41542831302c322920204445464155' +
        '4c5420223022204e4f54204e554c4c3b'
      
        '414c544552205441424c45204a4f55524e414c504f53204348414e4745205241' +
        '4241545433205241424154543320464c4f41542831302c322920204445464155' +
        '4c5420223022204e4f54204e554c4c3b'
      
        '414c544552205441424c45204a4f55524e414c504f53204348414e4745205354' +
        '455545525f434f4445205354455545525f434f44452054494e59494e54283429' +
        '202044454641554c5420223022204e4f54204e554c4c3b'
      
        '414c544552205441424c45204a4f55524e414c504f53204348414e474520414c' +
        '545445494c5f50524f5a20414c545445494c5f50524f5a20464c4f4154283130' +
        '2c3229202044454641554c542022302e3122204e4f54204e554c4c3b'
      
        '414c544552205441424c45204a4f55524e414c504f53204348414e474520414c' +
        '545445494c5f5354434f444520414c545445494c5f5354434f44452054494e59' +
        '494e54283429202044454641554c5420223022204e4f54204e554c4c3b'
      
        '414c544552205441424c45204a4f55524e414c504f53204348414e4745204745' +
        '47454e4b544f20474547454e4b544f20494e5428313129202044454641554c54' +
        '20222d3122204e4f54204e554c4c3b'
      ''
      
        '414c544552205441424c4520415254494b454c5f5345524e554d2044524f5020' +
        '494e444558205345524e554d4d45522c2041444420554e49515545205345524e' +
        '554d4d455220285345524e554d4d45522c415254494b454c5f4944293b'
      ''
      ''
      
        '414c544552205441424c4520604a4f55524e414c60204348414e474520604144' +
        '44525f4944602060414444525f49446020494e5428313129202044454641554c' +
        '5420222d3122204e4f54204e554c4c3b'
      
        '414c544552205441424c4520604a4f55524e414c60204348414e474520604c49' +
        '45465f414444525f49446020604c4945465f414444525f49446020494e542831' +
        '3129202044454641554c5420222d3122204e4f54204e554c4c3b'
      
        '414c544552205441424c4520604a4f55524e414c60204348414e474520604154' +
        '524e554d6020604154524e554d6020494e5428313129202044454641554c5420' +
        '222d3122204e4f54204e554c4c3b'
      
        '414c544552205441424c4520604a4f55524e414c60204348414e474520605652' +
        '454e554d6020605652454e554d6020494e5428313129202044454641554c5420' +
        '222d3122204e4f54204e554c4c3b'
      
        '414c544552205441424c4520604a4f55524e414c60204348414e47452060564c' +
        '534e554d602060564c534e554d6020494e5428313129202044454641554c5420' +
        '222d3122204e4f54204e554c4c3b'
      
        '414c544552205441424c4520604a4f55524e414c60204348414e47452060464f' +
        '4c47454e52602060464f4c47454e526020494e5428313129202044454641554c' +
        '5420222d3122204e4f54204e554c4c3b'
      
        '414c544552205441424c4520604a4f55524e414c60204348414e474520604b46' +
        '5a5f49446020604b465a5f49446020494e5428313129202044454641554c5420' +
        '222d3122204e4f54204e554c4c3b'
      
        '414c544552205441424c4520604a4f55524e414c60204348414e474520605645' +
        '525452455445525f49446020605645525452455445525f49446020494e542831' +
        '3129202044454641554c5420222d3122204e4f54204e554c4c3b'
      
        '414c544552205441424c4520604a4f55524e414c60204348414e47452060474c' +
        '4f42524142415454602060474c4f425241424154546020464c4f41542831302c' +
        '3229202044454641554c5420223022204e4f54204e554c4c3b'
      
        '414c544552205441424c4520604a4f55524e414c60204348414e474520604b4f' +
        '53545f4e4554544f6020604b4f53545f4e4554544f6020464c4f41542831302c' +
        '3229202044454641554c5420223022204e4f54204e554c4c3b'
      
        '414c544552205441424c4520604a4f55524e414c60204348414e474520605745' +
        '52545f4e4554544f602060574552545f4e4554544f6020464c4f41542831302c' +
        '3229202044454641554c5420223022204e4f54204e554c4c3b'
      
        '414c544552205441424c4520604a4f55524e414c60204348414e474520604c4f' +
        '484e6020604c4f484e6020464c4f41542831302c3229202044454641554c5420' +
        '223022204e4f54204e554c4c3b'
      
        '414c544552205441424c4520604a4f55524e414c60204348414e474520605741' +
        '5245602060574152456020464c4f41542831302c3229202044454641554c5420' +
        '223022204e4f54204e554c4c3b'
      
        '414c544552205441424c4520604a4f55524e414c60204348414e47452060544b' +
        '4f5354602060544b4f53546020464c4f41542831302c3229202044454641554c' +
        '5420223022204e4f54204e554c4c3b'
      
        '414c544552205441424c4520604a4f55524e414c60204348414e474520604d57' +
        '53545f306020604d5753545f306020464c4f41542831302c3229202044454641' +
        '554c5420223022204e4f54204e554c4c3b'
      
        '414c544552205441424c4520604a4f55524e414c60204348414e474520604d57' +
        '53545f316020604d5753545f316020464c4f41542831302c3229202044454641' +
        '554c5420223022204e4f54204e554c4c3b'
      
        '414c544552205441424c4520604a4f55524e414c60204348414e474520604d57' +
        '53545f326020604d5753545f326020464c4f41542831302c3229202044454641' +
        '554c5420223022204e4f54204e554c4c3b'
      
        '414c544552205441424c4520604a4f55524e414c60204348414e474520604d57' +
        '53545f336020604d5753545f336020464c4f41542831302c3229202044454641' +
        '554c5420223022204e4f54204e554c4c3b'
      
        '414c544552205441424c4520604a4f55524e414c60204348414e474520604d53' +
        '554d4d455f306020604d53554d4d455f306020464c4f41542831302c32292020' +
        '44454641554c5420223022204e4f54204e554c4c3b'
      
        '414c544552205441424c4520604a4f55524e414c60204348414e474520604d53' +
        '554d4d455f316020604d53554d4d455f316020464c4f41542831302c32292020' +
        '44454641554c5420223022204e4f54204e554c4c3b'
      
        '414c544552205441424c4520604a4f55524e414c60204348414e474520604d53' +
        '554d4d455f326020604d53554d4d455f326020464c4f41542831302c32292020' +
        '44454641554c5420223022204e4f54204e554c4c3b'
      
        '414c544552205441424c4520604a4f55524e414c60204348414e474520604d53' +
        '554d4d455f336020604d53554d4d455f336020464c4f41542831302c32292020' +
        '44454641554c5420223022204e4f54204e554c4c3b'
      
        '414c544552205441424c4520604a4f55524e414c60204348414e474520604e53' +
        '554d4d456020604e53554d4d456020464c4f41542831302c3229202044454641' +
        '554c5420223022204e4f54204e554c4c3b'
      
        '414c544552205441424c4520604a4f55524e414c60204348414e474520604d53' +
        '554d4d456020604d53554d4d456020464c4f41542831302c3229202044454641' +
        '554c5420223022204e4f54204e554c4c3b'
      
        '414c544552205441424c4520604a4f55524e414c60204348414e474520604253' +
        '554d4d456020604253554d4d456020464c4f41542831302c3229202044454641' +
        '554c5420223022204e4f54204e554c4c3b'
      
        '414c544552205441424c4520604a4f55524e414c60204348414e474520604154' +
        '53554d4d45602060415453554d4d456020464c4f41542831302c322920204445' +
        '4641554c5420223022204e4f54204e554c4c3b'
      
        '414c544552205441424c4520604a4f55524e414c60204348414e474520604154' +
        '4d53554d4d4560206041544d53554d4d456020464c4f41542831302c32292020' +
        '44454641554c5420223022204e4f54204e554c4c3b'
      
        '414c544552205441424c4520604a4f55524e414c60204348414e474520605741' +
        '454852554e476020605741454852554e4760205641524348415228352920204e' +
        '4f54204e554c4c3b'
      
        '414c544552205441424c4520604a4f55524e414c60204348414e474520604745' +
        '47454e4b4f4e544f602060474547454e4b4f4e544f6020494e54283131292020' +
        '44454641554c5420222d3122204e4f54204e554c4c3b'
      
        '414c544552205441424c4520604a4f55524e414c60204348414e474520604d41' +
        '484e4b4f5354454e6020604d41484e4b4f5354454e6020464c4f41542831302c' +
        '3229202044454641554c5420223022204e4f54204e554c4c3b'
      
        '414c544552205441424c4520604a4f55524e414c60204348414e474520604241' +
        '4e4b5f494460206042414e4b5f49446020494e5428313129202044454641554c' +
        '5420222d3122204e4f54204e554c4c3b'
      
        '414c544552205441424c4520604a4f55524e414c60204348414e474520605354' +
        '414449554d6020605354414449554d602054494e59494e542834292020444546' +
        '41554c5420223022204e4f54204e554c4c3b'
      
        '414c544552205441424c4520604a4f55524e414c60204348414e474520605557' +
        '5f4e554d60206055575f4e554d6020494e5428313129202044454641554c5420' +
        '222d3122204e4f54204e554c4c3b'
      ''
      
        '414c544552205441424c4520604a4f55524e414c60204348414e47452060534f' +
        '4c4c5f5354414745602060534f4c4c5f5354414745602054494e59494e542834' +
        '29202044454641554c5420223022204e4f54204e554c4c3b'
      
        '414c544552205441424c4520604a4f55524e414c60204348414e47452060534f' +
        '4c4c5f534b4f4e544f602060534f4c4c5f534b4f4e544f6020464c4f41542835' +
        '2c3229202044454641554c5420223022204e4f54204e554c4c3b'
      
        '414c544552205441424c4520604a4f55524e414c60204348414e47452060534f' +
        '4c4c5f4e54414745602060534f4c4c5f4e54414745602054494e59494e542834' +
        '29202044454641554c5420223022204e4f54204e554c4c3b'
      
        '414c544552205441424c4520604a4f55524e414c60204348414e47452060534f' +
        '4c4c5f524154454e602060534f4c4c5f524154454e602054494e59494e542834' +
        '29202044454641554c5420223122204e4f54204e554c4c3b'
      
        '414c544552205441424c4520604a4f55524e414c60204348414e47452060534f' +
        '4c4c5f52415442455452602060534f4c4c5f524154424554526020464c4f4154' +
        '2831302c3229202044454641554c5420223022204e4f54204e554c4c3b'
      
        '414c544552205441424c4520604a4f55524e414c60204348414e47452060534f' +
        '4c4c5f524154494e54455256414c4c602060534f4c4c5f524154494e54455256' +
        '414c4c6020494e5428313129202044454641554c5420223022204e4f54204e55' +
        '4c4c3b'
      
        '414c544552205441424c4520604a4f55524e414c60204348414e474520604953' +
        '545f4245545241476020604953545f4245545241476020464c4f41542831302c' +
        '3229202044454641554c5420223022204e4f54204e554c4c3b'
      
        '414c544552205441424c4520604a4f55524e414c60204348414e474520604d41' +
        '484e53545546456020604d41484e5354554645602054494e59494e5428312920' +
        '2044454641554c5420223022204e4f54204e554c4c3b'
      
        '414c544552205441424c4520604a4f55524e414c60204348414e474520604d41' +
        '484e5052494e546020604d41484e5052494e54602054494e59494e5428312920' +
        '2044454641554c5420223022204e4f54204e554c4c3b'
      ''
      
        '414c544552205441424c4520604a4f55524e414c602041444420604652454947' +
        '414245315f464c41476020454e554d28224e222c22592229202044454641554c' +
        '5420224e22204e4f54204e554c4c3b'
      
        '414c544552205441424c4520604a4f55524e414c602041444420604652454947' +
        '414245325f464c41476020454e554d28224e222c22592229202044454641554c' +
        '5420224e22204e4f54204e554c4c3b'
      
        '55504441544520604a4f55524e414c602053455420604652454947414245315f' +
        '464c414760203d20225922207768657265204652454947414245313d313b'
      
        '55504441544520604a4f55524e414c602053455420604652454947414245315f' +
        '464c414760203d20224e2220776865726520465245494741424531213d313b'
      
        '414c544552205441424c4520604a4f55524e414c602044524f50206046524549' +
        '4741424531603b'
      ''
      ''
      ''
      
        '414c544552205441424c452060415254494b454c60204348414e474520605741' +
        '52454e475255505045602060574152454e4752555050456020494e5428313129' +
        '20204e4f54204e554c4c3b'
      
        '414c544552205441424c452060415254494b454c60204348414e474520605241' +
        '424752505f49446020605241424752505f494460205641524348415228313029' +
        '202044454641554c5420222d22204e4f54204e554c4c3b'
      
        '414c544552205441424c452060415254494b454c60204348414e474520604152' +
        '54494b454c545950602060415254494b454c5459506020434841522831292020' +
        '44454641554c5420224e22204e4f54204e554c4c3b'
      
        '414c544552205441424c452060415254494b454c60204348414e474520604845' +
        '524b554e46534c414e446020604845524b554e46534c414e4460205641524348' +
        '41522833293b'
      
        '414c544552205441424c452060415254494b454c60204348414e474520604745' +
        '5749434854602060474557494348546020464c4f41542831302c322920204445' +
        '4641554c5420223022204e4f54204e554c4c3b'
      
        '414c544552205441424c452060415254494b454c60204348414e47452060494e' +
        '56454e5455525f57455254602060494e56454e5455525f574552546020464c4f' +
        '41542831302c3229202044454641554c5420223130302e303022204e4f54204e' +
        '554c4c3b'
      
        '414c544552205441424c452060415254494b454c60204348414e47452060454b' +
        '5f5052454953602060454b5f50524549536020464c4f41542831302c33292020' +
        '44454641554c542022302e30303022204e4f54204e554c4c3b'
      
        '414c544552205441424c452060415254494b454c60204348414e47452060564b' +
        '31602060564b316020464c4f41542831302c3229202044454641554c54202230' +
        '2e303022204e4f54204e554c4c3b'
      
        '414c544552205441424c452060415254494b454c60204348414e47452060564b' +
        '32602060564b326020464c4f41542831302c3229202044454641554c54202230' +
        '2e303022204e4f54204e554c4c3b'
      
        '414c544552205441424c452060415254494b454c60204348414e47452060564b' +
        '33602060564b336020464c4f41542831302c3229202044454641554c54202230' +
        '2e303022204e4f54204e554c4c3b'
      
        '414c544552205441424c452060415254494b454c60204348414e47452060564b' +
        '34602060564b346020464c4f41542831302c3229202044454641554c54202230' +
        '2e303022204e4f54204e554c4c3b'
      
        '414c544552205441424c452060415254494b454c60204348414e47452060564b' +
        '35602060564b356020464c4f41542831302c3229202044454641554c54202230' +
        '2e303022204e4f54204e554c4c3b'
      
        '414c544552205441424c452060415254494b454c60204348414e474520605354' +
        '455545525f434f44456020605354455545525f434f4445602054494e59494e54' +
        '283429202044454641554c5420223222204e4f54204e554c4c3b'
      
        '414c544552205441424c452060415254494b454c60204348414e474520604d45' +
        '4e47455f53544152546020604d454e47455f53544152546020464c4f41542831' +
        '302c3229202044454641554c542022302e303022204e4f54204e554c4c3b'
      
        '414c544552205441424c452060415254494b454c60204348414e474520604d45' +
        '4e47455f4d494e6020604d454e47455f4d494e6020464c4f41542831302c3229' +
        '202044454641554c542022302e303022204e4f54204e554c4c3b'
      
        '414c544552205441424c452060415254494b454c60204348414e474520604d45' +
        '4e47455f414b546020604d454e47455f414b546020464c4f41542831302c3229' +
        '202044454641554c542022302e303022204e4f54204e554c4c3b'
      
        '414c544552205441424c452060415254494b454c60204348414e474520604d45' +
        '4e47455f42455354454c4c546020604d454e47455f42455354454c4c54602046' +
        '4c4f41542831302c3229202044454641554c542022302e303022204e4f54204e' +
        '554c4c3b'
      
        '414c544552205441424c452060415254494b454c60204348414e474520604d45' +
        '4e47455f42564f526020604d454e47455f42564f526020464c4f41542831302c' +
        '3229202044454641554c542022302e303022204e4f54204e554c4c3b'
      
        '414c544552205441424c452060415254494b454c60204348414e474520604c41' +
        '53545f4c4945466020604c4153545f4c4945466020494e542831312920204445' +
        '4641554c5420222d3122204e4f54204e554c4c3b'
      
        '414c544552205441424c452060415254494b454c60204348414e474520604c41' +
        '53545f4b554e44456020604c4153545f4b554e44456020494e54283131292020' +
        '44454641554c5420222d3122204e4f54204e554c4c3b'
      
        '414c544552205441424c452060415254494b454c60204348414e474520604c49' +
        '4546316020604c494546316020494e5428313129202044454641554c5420222d' +
        '3122204e4f54204e554c4c3b'
      
        '414c544552205441424c452060415254494b454c60204348414e474520604c49' +
        '4546326020604c494546326020494e5428313129202044454641554c5420222d' +
        '3122204e4f54204e554c4c3b'
      
        '414c544552205441424c452060415254494b454c60204348414e474520605052' +
        '5f45494e4845495460206050525f45494e484549546020464c4f41542831302c' +
        '3229202044454641554c542022312e303022204e4f54204e554c4c3b'
      
        '414c544552205441424c452060415254494b454c60204348414e474520604c41' +
        '53545f564b6020604c4153545f564b6020464c4f41542831302c322920204445' +
        '4641554c542022302e303022204e4f54204e554c4c3b'
      
        '414c544552205441424c452060415254494b454c60204348414e474520604c41' +
        '53545f454b6020604c4153545f454b6020464c4f41542831302c332920204445' +
        '4641554c542022302e30303022204e4f54204e554c4c3b'
      
        '414c544552205441424c452060415254494b454c60204348414e474520604c49' +
        '4546315f50524549536020604c494546315f50524549536020464c4f41542831' +
        '302c3329202044454641554c542022302e30303022204e4f54204e554c4c3b'
      
        '414c544552205441424c452060415254494b454c60204348414e474520604c49' +
        '4546325f50524549536020604c494546325f50524549536020464c4f41542831' +
        '302c3329202044454641554c542022302e30303022204e4f54204e554c4c3b'
      ''
      
        '55504441544520415254494b454c20534554204d454e47455f42455354454c4c' +
        '543d303b'
      ''
      
        '414c544552205441424c452060415254494b454c60204144442060414c545445' +
        '494c5f464c41476020454e554d28224e222c22592229202044454641554c5420' +
        '224e22204e4f54204e554c4c2041465445522060414c545f5445494c603b'
      
        '55504441544520415254494b454c2053455420414c545445494c5f464c41473d' +
        '27592720776865726520414c545f5445494c3d313b'
      
        '55504441544520415254494b454c2053455420414c545445494c5f464c41473d' +
        '274e2720776865726520414c545f5445494c213d313b'
      
        '414c544552205441424c4520415254494b454c2044524f5020414c545f544549' +
        '4c3b'
      ''
      
        '414c544552205441424c452060415254494b454c602041444420604e4f5f5241' +
        '424154545f464c41476020454e554d28224e222c22592229202044454641554c' +
        '5420224e22204e4f54204e554c4c2041465445522060414c545445494c5f464c' +
        '4147603b'
      
        '55504441544520415254494b454c20534554204e4f5f5241424154545f464c41' +
        '473d275927207768657265204e4f5f5241424154543d313b'
      
        '55504441544520415254494b454c20534554204e4f5f5241424154545f464c41' +
        '473d274e27207768657265204e4f5f524142415454213d313b'
      
        '414c544552205441424c4520415254494b454c2044524f50204e4f5f52414241' +
        '54543b'
      ''
      
        '414c544552205441424c452060415254494b454c602041444420604e4f5f5052' +
        '4f564953494f4e5f464c41476020454e554d28224e222c225922292020444546' +
        '41554c5420224e22204e4f54204e554c4c20414654455220604e4f5f52414241' +
        '54545f464c4147603b'
      
        '55504441544520415254494b454c20534554204e4f5f50524f564953494f4e5f' +
        '464c41473d275927207768657265204e4f5f50524f564953494f4e3d313b'
      
        '55504441544520415254494b454c20534554204e4f5f50524f564953494f4e5f' +
        '464c41473d274e27207768657265204e4f5f50524f564953494f4e213d313b'
      
        '414c544552205441424c4520415254494b454c2044524f50204e4f5f50524f56' +
        '4953494f4e3b'
      ''
      
        '414c544552205441424c452060415254494b454c602041444420604e4f5f4245' +
        '5a454449545f464c41476020454e554d28224e222c2259222920204445464155' +
        '4c5420224e22204e4f54204e554c4c20414654455220604e4f5f50524f564953' +
        '494f4e5f464c4147603b'
      
        '55504441544520415254494b454c20534554204e4f5f42455a454449545f464c' +
        '41473d275927207768657265204e4f5f42455a454449543d313b'
      
        '55504441544520415254494b454c20534554204e4f5f42455a454449545f464c' +
        '41473d274e27207768657265204e4f5f42455a45444954213d313b'
      
        '414c544552205441424c4520415254494b454c2044524f50204e4f5f42455a45' +
        '4449543b'
      ''
      ''
      
        '414c544552205441424c452060415254494b454c602041444420604e4f5f454b' +
        '5f464c41476020454e554d28224e222c22592229202044454641554c5420224e' +
        '22204e4f54204e554c4c20414654455220604e4f5f42455a454449545f464c41' +
        '47603b'
      
        '55504441544520415254494b454c20534554204e4f5f454b5f464c41473d2759' +
        '27207768657265204e4f5f454b3d313b'
      
        '55504441544520415254494b454c20534554204e4f5f454b5f464c41473d274e' +
        '27207768657265204e4f5f454b213d313b'
      '414c544552205441424c4520415254494b454c2044524f50204e4f5f454b3b'
      ''
      
        '414c544552205441424c452060415254494b454c602041444420604e4f5f564b' +
        '5f464c41476020454e554d28224e222c22592229202044454641554c5420224e' +
        '22204e4f54204e554c4c20414654455220604e4f5f454b5f464c4147603b'
      
        '55504441544520415254494b454c20534554204e4f5f564b5f464c41473d2759' +
        '27207768657265204e4f5f564b3d313b'
      
        '55504441544520415254494b454c20534554204e4f5f564b5f464c41473d274e' +
        '27207768657265204e4f5f564b213d313b'
      '414c544552205441424c4520415254494b454c2044524f50204e4f5f564b3b'
      ''
      ''
      
        '414c544552205441424c452060415254494b454c60204144442060534e5f464c' +
        '41476020454e554d28224e222c22592229202044454641554c5420224e22204e' +
        '4f54204e554c4c20414654455220604e4f5f564b5f464c4147603b'
      
        '55504441544520415254494b454c2053455420534e5f464c41473d2759272077' +
        '68657265205345524e4f5f50464c494348543d313b'
      
        '55504441544520415254494b454c2053455420534e5f464c41473d274e272077' +
        '68657265205345524e4f5f50464c49434854213d313b'
      
        '414c544552205441424c4520415254494b454c2044524f50205345524e4f5f50' +
        '464c494348543b'
      ''
      
        '414c544552205441424c452060415254494b454c602041444420604155544f44' +
        '454c5f464c41476020454e554d28224e222c22592229202044454641554c5420' +
        '224e22204e4f54204e554c4c2041465445522060534e5f464c4147603b'
      
        '55504441544520415254494b454c20534554204155544f44454c5f464c41473d' +
        '275927207768657265204155544f44454c3d313b'
      
        '55504441544520415254494b454c20534554204155544f44454c5f464c41473d' +
        '274e27207768657265204155544f44454c213d313b'
      
        '414c544552205441424c4520415254494b454c2044524f50204155544f44454c' +
        '3b'
      ''
      
        '414c544552205441424c452060415254494b454c60204348414e474520605348' +
        '4f505f415254494b454c60206053484f505f415254494b454c6020454e554d28' +
        '224e222c22592229202044454641554c5420224e22204e4f54204e554c4c3b'
      
        '55504441544520415254494b454c205345542053484f505f415254494b454c3d' +
        '274e273b'
      ''
      ''
      
        '414c544552205441424c45206052454749535445525960204348414e47452060' +
        '4341434841424c456020604341434841424c456020454e554d28224e222c2259' +
        '2229202044454641554c5420224e22204e4f54204e554c4c3b'
      
        '414c544552205441424c45206052454749535445525960204348414e47452060' +
        '524541444f4e4c59602060524541444f4e4c596020454e554d28224e222c2259' +
        '2229202044454641554c5420224e22204e4f54204e554c4c3b'
      
        '5550444154452052454749535445525920534554204341434841424c453d274e' +
        '272c20524541444f4e4c593d274e273b'
      ''
      '2f2a204e45552032322e30352e3230303320444220312e30342043202a2f'
      ''
      '414c544552205441424c4520604a4f55524e414c504f536020'
      
        '414444206042525554544f5f464c41476020454e554d28224e222c2259222920' +
        '2044454641554c5420224e22204e4f54204e554c4c3b'
      ''
      '414c544552205441424c4520604a4f55524e414c6020'
      
        '414444206042525554544f5f464c41476020454e554d28224e222c2259222920' +
        '2044454641554c5420224e22204e4f54204e554c4c2c'
      
        '41444420604d5753545f465245495f464c41476020454e554d28224e222c2259' +
        '2229202044454641554c5420224e22204e4f54204e554c4c3b'
      ''
      '414c544552205441424c452060414452455353454e6020'
      
        '414444206042525554544f5f464c41476020454e554d28224e222c2259222920' +
        '2044454641554c5420224e22204e4f54204e554c4c204146544552206050525f' +
        '4542454e45602c'
      
        '41444420604d5753545f465245495f464c41476020454e554d28224e222c2259' +
        '2229202044454641554c5420224e22204e4f54204e554c4c2041465445522060' +
        '42525554544f5f464c4147603b'
      ''
      '2f2a204e45552033302e30352e3230303320444220312e303444202a2f'
      ''
      
        '414c544552205441424c452060415254494b454c602041444420604d454e4745' +
        '5f454b424553545f4544496020464c4f41542831302c3229202044454641554c' +
        '5420223022204e4f54204e554c4c20414654455220604d454e47455f42564f52' +
        '603b'
      
        '414c544552205441424c452060415254494b454c602041444420604d454e4745' +
        '5f564b52455f4544496020464c4f41542831302c3229202044454641554c5420' +
        '223022204e4f54204e554c4c20414654455220604d454e47455f454b42455354' +
        '5f454449603b'
      
        '414c544552205441424c452060415254494b454c602041444420604d454e4745' +
        '5f454b52455f4544496020464c4f41542831302c3229202044454641554c5420' +
        '223022204e4f54204e554c4c20414654455220604d454e47455f564b52455f45' +
        '4449603b'
      ''
      
        '414c544552205441424c452060414452455353454e602041444420605553545f' +
        '4e554d6020564152434841522832352920204146544552206057414552554e47' +
        '603b'
      
        '414c544552205441424c452060414452455353454e6020414444206056455254' +
        '52455445525f49446020494e54283131292020554e5349474e45442044454641' +
        '554c5420222d3122204e4f54204e554c4c20414654455220605553545f4e554d' +
        '603b'
      ''
      '2f2a204e45552032352e30362e323030332053686f7066656c646572202a2f'
      ''
      '435245415445205441424c452060415254494b454c5f4b415460'
      '28'
      
        '20206053484f505f4944602074696e79696e74283329204e4f54204e554c4c20' +
        '64656661756c7420272d31272c'
      
        '20206049446020696e7428313129204e4f54204e554c4c206175746f5f696e63' +
        '72656d656e742c'
      
        '202060544f505f49446020696e7428313129204e4f54204e554c4c2064656661' +
        '756c7420272d31272c'
      
        '2020604e414d456020766172636861722832353029204e4f54204e554c4c2064' +
        '656661756c742027272c'
      
        '20206055524c60207661726368617228323530292064656661756c74204e554c' +
        '4c2c'
      '202060424553434852454942554e476020746578742c'
      
        '202060494d41474560207661726368617228323530292064656661756c74204e' +
        '554c4c2c'
      
        '20206056495349424c455f464c41476020656e756d28274e272c27592729204e' +
        '4f54204e554c4c2064656661756c74202759272c'
      
        '2020604c4945465f49446020696e74283133292064656661756c7420272d3127' +
        '2c'
      
        '202060415254494b454c5f54454d504c41544560207661726368617228323530' +
        '292064656661756c7420276e6f726d616c2e74706c272c'
      
        '2020604752555050454e5f54454d504c41544560207661726368617228323530' +
        '292064656661756c7420276772705f6e6f726d616c2e74706c272c'
      
        '202060434c49434b5f434f554e546020696e74283131292064656661756c7420' +
        '2730272c'
      
        '2020604c4153545f4950602076617263686172283136292064656661756c7420' +
        '4e554c4c2c'
      
        '20206053594e435f464c41476020656e756d28274e272c27592729204e4f5420' +
        '4e554c4c2064656661756c7420274e272c'
      
        '2020604348414e47455f464c41476020656e756d28274e272c27592729204e4f' +
        '54204e554c4c2064656661756c74202759272c'
      
        '20206044454c5f464c41476020454e554d28274e272c27592729202044454641' +
        '554c5420224e22204e4f54204e554c4c2c'
      '20205052494d415259204b45592020286053484f505f4944602c6049446029'
      '293b'
      ''
      '435245415445205441424c452060415254494b454c5f544f5f4b415460'
      '28'
      
        '20206053484f505f4944602074696e79696e74283329204e4f54204e554c4c20' +
        '64656661756c7420272d31272c'
      
        '202060415254494b454c5f49446020696e7428313329204e4f54204e554c4c20' +
        '64656661756c7420272d31272c'
      
        '2020604b41545f49446020696e7428313329204e4f54204e554c4c2064656661' +
        '756c7420272d31272c'
      
        '2020604348414e47455f464c41476020656e756d28274e272c27592729204e4f' +
        '54204e554c4c2064656661756c74202759272c'
      
        '20206044454c5f464c41476020454e554d28274e272c27592729202044454641' +
        '554c5420224e22204e4f54204e554c4c2c'
      
        '20205052494d415259204b45592020286053484f505f4944602c60415254494b' +
        '454c5f4944602c604b41545f494460292c'
      
        '20204b455920604b41545f544f5f415254494b454c6020286053484f505f4944' +
        '602c604b41545f4944602c60415254494b454c5f49446029'
      '293b'
      ''
      '414c544552205441424c45206041504152544e455260'
      
        '20204144442060414e5245444560205641524348415228343029202041465445' +
        '522060564f524e414d45602c'
      
        '2020414444206053545241535345602056415243484152283430292020414654' +
        '45522060414e52454445602c'
      
        '202041444420604c414e44602056415243484152283529202044454641554c54' +
        '20224422204146544552206053545241535345602c'
      
        '20204144442060504c5a60205641524348415228313029202041465445522060' +
        '4c414e44602c'
      
        '202041444420604f525460205641524348415228343029202041465445522060' +
        '504c5a603b'
      ''
      '414c544552205441424c452060414452455353454e60'
      
        '2020414444206053484f505f4b554e44456020454e554d28274e272c27592729' +
        '202044454641554c5420224e22204e4f54204e554c4c2c'
      
        '2020414444206053484f505f4b554e44455f49446020494e5428313129202044' +
        '454641554c5420222d3122204e4f54204e554c4c2c'
      
        '2020414444206053484f505f4944602054494e59494e54283329202044454641' +
        '554c5420222d3122204e4f54204e554c4c204146544552206053484f505f4b55' +
        '4e4445603b'
      ''
      
        '75706461746520414452455353454e207365742053484f505f49443d31207768' +
        '6572652053484f505f4b554e44453d2759273b'
      ''
      
        '414c544552205441424c452060414452455353454e602044524f50206053484f' +
        '505f4b554e4445603b'
      ''
      '414c544552205441424c4520604a4f55524e414c60'
      
        '2020414444206053484f505f4944602054494e59494e54283329202044454641' +
        '554c5420222d3122204e4f54204e554c4c2c'
      
        '2020414444206053484f505f4f5244455249446020494e542831312920204445' +
        '4641554c5420222d3122204e4f54204e554c4c2c'
      
        '2020414444206053484f505f535441545553602054494e59494e542833292020' +
        '554e5349474e45442044454641554c5420223022204e4f54204e554c4c2c'
      
        '2020414444206053484f505f4348414e47455f464c41476020454e554d28274e' +
        '272c27592729202044454641554c5420224e22204e4f54204e554c4c3b'
      ''
      ''
      '414c544552205441424c452060415254494b454c6020'
      
        '20204348414e4745206053484f505f494460206053484f505f415254494b454c' +
        '5f49446020494e5428313129202044454641554c5420222d3122204e4f54204e' +
        '554c4c2c'
      
        '2020414444206053484f505f4944602054494e59494e542044454641554c5420' +
        '222d3122204146544552206053484f505f415254494b454c603b'
      ''
      
        '75706461746520415254494b454c207365742053484f505f49443d3120776865' +
        '72652053484f505f415254494b454c3d2759273b'
      ''
      '414c544552205441424c452060415254494b454c6020'
      '202044524f50206053484f505f415254494b454c602c'
      
        '2020414444206053484f505f44454c5f464c41476020454e554d28274e272c27' +
        '592729202044454641554c5420224e22204e4f54204e554c4c3b'
      ''
      ''
      '414c544552205441424c452060415254494b454c5f4c544558546020'
      
        '2020414444206046454c445f4944602054494e59494e54283329202044454641' +
        '554c5420222d3122204e4f54204e554c4c204146544552206053505241434845' +
        '5f4944602c'
      '202044524f50205052494d415259204b45592c20'
      
        '2020414444205052494d415259204b45592028415254494b454c5f49442c5350' +
        '52414348455f49442c46454c445f4944293b'
      ''
      ''
      
        '494e5345525420494e544f2052454749535445525920284d41494e4b45592c20' +
        '4e414d452c2056414c5f43484152292056616c75657320282753484f50272c20' +
        '2744454641554c54272c2027616c6c672e2053686f7065696e7374656c6c756e' +
        '67656e27293b')
  end
  object KunRabGrp: TZMySqlQuery
    Database = db1
    Transaction = Transact1
    CachedUpdates = False
    ShowRecordTypes = [ztModified, ztInserted, ztUnmodified]
    Options = [doHourGlass, doAutoFillDefs, doUseRowId]
    LinkOptions = [loAlwaysResync]
    Constraints = <>
    ExtraOptions = [moStoreResult]
    Macros = <>
    Sql.Strings = (
      'select * from RABATTGRUPPEN'
      'where RABGRP_TYP=3'
      'order by RABGRP_ID')
    RequestLive = False
    Left = 20
    Top = 461
    object KunRabGrpRABGRP_ID: TStringField
      FieldName = 'RABGRP_ID'
      Required = True
      Size = 10
    end
    object KunRabGrpRABGRP_TYP: TIntegerField
      FieldName = 'RABGRP_TYP'
      Required = True
    end
    object KunRabGrpMIN_MENGE: TIntegerField
      FieldName = 'MIN_MENGE'
      Required = True
    end
    object KunRabGrpLIEF_RABGRP: TIntegerField
      FieldName = 'LIEF_RABGRP'
      Required = True
    end
    object KunRabGrpRABATT1: TFloatField
      FieldName = 'RABATT1'
      Required = True
    end
    object KunRabGrpRABATT2: TFloatField
      FieldName = 'RABATT2'
      Required = True
    end
    object KunRabGrpRABATT3: TFloatField
      FieldName = 'RABATT3'
      Required = True
    end
    object KunRabGrpADDR_ID: TIntegerField
      FieldName = 'ADDR_ID'
      Required = True
    end
  end
  object UniQuery: TZMySqlQuery
    Database = db1
    Transaction = Transact1
    CachedUpdates = False
    ShowRecordTypes = [ztModified, ztInserted, ztUnmodified]
    Options = [doHourGlass, doAutoFillDefs, doUseRowId]
    LinkOptions = [loAlwaysResync]
    Constraints = <>
    ExtraOptions = [moStoreResult]
    Macros = <>
    RequestLive = False
    Left = 24
    Top = 184
  end
  object UniQuery2: TZMySqlQuery
    Database = db1
    Transaction = Transact1
    CachedUpdates = False
    ShowRecordTypes = [ztModified, ztInserted, ztUnmodified]
    Options = [doHourGlass, doAutoFillDefs, doUseRowId]
    LinkOptions = [loAlwaysResync]
    Constraints = <>
    ExtraOptions = [moStoreResult]
    Macros = <>
    RequestLive = False
    Left = 24
    Top = 240
  end
  object ShopOrderStatusTab: TZMySqlQuery
    Database = db1
    Transaction = Transact1
    CachedUpdates = False
    ShowRecordTypes = [ztModified, ztInserted, ztUnmodified]
    Options = [doHourGlass, doAutoFillDefs, doUseRowId]
    LinkOptions = [loAlwaysResync]
    Constraints = <>
    BeforePost = ShopOrderStatusTabBeforePost
    ExtraOptions = [moStoreResult]
    Macros = <>
    Sql.Strings = (
      'select VAL_INT as ORDERSTATUS_ID, '
      'NAME as LANGBEZ,  VAL_BLOB as TEXT, MAINKEY'
      'from REGISTERY '
      'where MAINKEY='#39'SHOP\\ORDERSTATUS'#39
      'order by ORDERSTATUS_ID')
    RequestLive = True
    Left = 188
    Top = 461
    object ShopOrderStatusTabLANGBEZ: TStringField
      FieldName = 'LANGBEZ'
      Required = True
      Size = 100
    end
    object ShopOrderStatusTabTEXT: TMemoField
      FieldName = 'TEXT'
      BlobType = ftMemo
    end
    object ShopOrderStatusTabMAINKEY: TStringField
      FieldName = 'MAINKEY'
      Size = 255
    end
    object ShopOrderStatusTabORDERSTATUS_ID: TIntegerField
      FieldName = 'ORDERSTATUS_ID'
    end
  end
  object ShopOSDS: TDataSource
    DataSet = ShopOrderStatusTab
    Left = 116
    Top = 459
  end
  object DBUpdTo1_05: TJvStrHolder
    Capacity = 183
    Macros = <>
    Left = 472
    Top = 400
    InternalVer = 1
    StrData = (
      ''
      
        '414c544552205441424c4520604a4f55524e414c504f53602041444420494e44' +
        '4558205155454c4c455f53524320285155454c4c455f535243293b'
      ''
      
        '414c544552205441424c452060415254494b454c60204348414e474520604d41' +
        '544348434f44456020604d41544348434f444560205641524348415228323535' +
        '293b'
      ''
      
        '414c544552205441424c452060494e464f602041444420605155454c4c5f4944' +
        '6020494e5428313129202044454641554c5420222d3122204e4f54204e554c4c' +
        '20414654455220605155454c4c45603b'
      ''
      '414c544552205441424c452060414452455353454e60'
      '204348414e474520604b544f6020604b544f602056415243484152283230292c'
      '204348414e47452060424c5a602060424c5a602056415243484152283230292c'
      
        '204348414e474520604d41544348434f44456020604d41544348434f44456020' +
        '5641524348415228323535293b'
      ''
      ''
      
        '494e5345525420494e544f2052454749535445525920284e414d452c2056414c' +
        '5f5459502c204d41494e4b45592c2056414c5f43484152292056414c55455320' +
        '2827564b5f4147425f4d41494c5f5355424a454354272c20312c20274d41494e' +
        '5c5c5245504f5254272c4e554c4c293b'
      
        '494e5345525420494e544f2052454749535445525920284e414d452c2056414c' +
        '5f5459502c204d41494e4b45592c2056414c5f43484152292056414c55455320' +
        '2827564b5f524543485f4d41494c5f5355424a454354272c20312c20274d4149' +
        '4e5c5c5245504f5254272c4e554c4c293b'
      
        '494e5345525420494e544f2052454749535445525920284e414d452c2056414c' +
        '5f5459502c204d41494e4b45592c2056414c5f43484152292056414c55455320' +
        '2827564b5f4c4945465f4d41494c5f5355424a454354272c20312c20274d4149' +
        '4e5c5c5245504f5254272c4e554c4c293b'
      
        '494e5345525420494e544f2052454749535445525920284e414d452c2056414c' +
        '5f5459502c204d41494e4b45592c2056414c5f43484152292056414c55455320' +
        '2827454b5f424553545f4d41494c5f5355424a454354272c20312c20274d4149' +
        '4e5c5c5245504f5254272c4e554c4c293b'
      ''
      
        '494e5345525420494e544f2052454749535445525920284e414d452c2056414c' +
        '5f5459502c204d41494e4b45592c2056414c5f43484152292056414c55455320' +
        '28275a41484c554e47535a49454c5f534f464f52545f54455854272c20312c20' +
        '274d41494e5c5c5245504f5254272c27536f666f727427293b'
      ''
      '2f2a30362e30372e323030332a2f'
      ''
      
        '494e5345525420494e544f2052454749535445525920284e414d452c2056414c' +
        '5f5459502c204d41494e4b45592c2056414c5f43484152292056414c55455320' +
        '2827424f4e5f55454245525343485249465431272c20312c20274b4153534527' +
        '2c204e554c4c293b'
      
        '494e5345525420494e544f2052454749535445525920284e414d452c2056414c' +
        '5f5459502c204d41494e4b45592c2056414c5f43484152292056414c55455320' +
        '2827424f4e5f55454245525343485249465432272c20312c20274b4153534527' +
        '2c204e554c4c293b'
      
        '494e5345525420494e544f2052454749535445525920284e414d452c2056414c' +
        '5f5459502c204d41494e4b45592c2056414c5f43484152292056414c55455320' +
        '2827424f4e5f55454245525343485249465433272c20312c20274b4153534527' +
        '2c204e554c4c293b'
      ''
      
        '494e5345525420494e544f2052454749535445525920284d41494e4b45592c20' +
        '56414c5f5459502c204e414d452c2056414c5f494e54292056414c5545532028' +
        '274d41494e5c5c414452455353454e272c20332c20274b544f5f4c454e272c20' +
        '3130293b'
      
        '494e5345525420494e544f2052454749535445525920284d41494e4b45592c20' +
        '56414c5f5459502c204e414d452c2056414c5f494e54292056414c5545532028' +
        '274d41494e5c5c414452455353454e272c20332c2027424c5a5f4c454e272c20' +
        '38293b'
      ''
      
        '435245415445205441424c45204946204e4f5420455849535453202056455254' +
        '5241472028'
      
        '20205245435f494420696e7428313129204e4f54204e554c4c206175746f5f69' +
        '6e6372656d656e742c'
      
        '2020414444525f494420696e7428313129204e4f54204e554c4c206465666175' +
        '6c7420272d31272c'
      
        '20205656544e554d20696e7428313129204e4f54204e554c4c2064656661756c' +
        '7420272d31272c'
      
        '20205645525452455445525f494420696e7428313129204e4f54204e554c4c20' +
        '64656661756c7420272d31272c'
      
        '2020474c4f4252414241545420666c6f61742831302c3229204e4f54204e554c' +
        '4c2064656661756c742027302e3030272c'
      
        '2020444154554d5f53544152542064617465204e4f54204e554c4c2064656661' +
        '756c742027303030302d30302d3030272c'
      
        '2020444154554d5f454e44452064617465204e4f54204e554c4c206465666175' +
        '6c742027303030302d30302d3030272c'
      
        '20204441554d5f4e4558542064617465204e4f54204e554c4c2064656661756c' +
        '742027303030302d30302d3030272c'
      
        '2020494e54455256414c4c2063686172283129204e4f54204e554c4c20646566' +
        '61756c7420274d272c'
      
        '2020494e54455256414c4c5f4e554d2074696e79696e7428332920756e736967' +
        '6e6564204e4f54204e554c4c2064656661756c74202731272c'
      
        '2020414b5449565f464c414720656e756d28274e272c27592729204e4f54204e' +
        '554c4c2064656661756c7420274e272c'
      
        '20205052494e545f464c414720656e756d28274e272c27592729204e4f54204e' +
        '554c4c2064656661756c7420274e272c'
      
        '202050525f4542454e452074696e79696e742834292064656661756c74204e55' +
        '4c4c2c'
      
        '20204c4945464152542074696e79696e742832292064656661756c74204e554c' +
        '4c2c'
      
        '20205a41484c4152542074696e79696e742832292064656661756c74204e554c' +
        '4c2c'
      
        '20204b4f53545f4e4554544f20666c6f61742831302c3229204e4f54204e554c' +
        '4c2064656661756c742027302e3030272c'
      
        '2020574552545f4e4554544f20666c6f61742831302c3229204e4f54204e554c' +
        '4c2064656661756c742027302e3030272c'
      
        '20204c4f484e20666c6f61742831302c3229204e4f54204e554c4c2064656661' +
        '756c742027302e3030272c'
      
        '20205741524520666c6f61742831302c3229204e4f54204e554c4c2064656661' +
        '756c742027302e3030272c'
      
        '2020544b4f535420666c6f61742831302c3229204e4f54204e554c4c20646566' +
        '61756c742027302e3030272c'
      
        '20204d5753545f3020666c6f61742831302c3229204e4f54204e554c4c206465' +
        '6661756c742027302e3030272c'
      
        '20204d5753545f3120666c6f61742831302c3229204e4f54204e554c4c206465' +
        '6661756c742027302e3030272c'
      
        '20204d5753545f3220666c6f61742831302c3229204e4f54204e554c4c206465' +
        '6661756c742027302e3030272c'
      
        '20204d5753545f3320666c6f61742831302c3229204e4f54204e554c4c206465' +
        '6661756c742027302e3030272c'
      
        '20204e53554d4d4520666c6f61742831302c3229204e4f54204e554c4c206465' +
        '6661756c742027302e3030272c'
      
        '20204d53554d4d455f3020666c6f61742831302c3229204e4f54204e554c4c20' +
        '64656661756c742027302e3030272c'
      
        '20204d53554d4d455f3120666c6f61742831302c3229204e4f54204e554c4c20' +
        '64656661756c742027302e3030272c'
      
        '20204d53554d4d455f3220666c6f61742831302c3229204e4f54204e554c4c20' +
        '64656661756c742027302e3030272c'
      
        '20204d53554d4d455f3320666c6f61742831302c3229204e4f54204e554c4c20' +
        '64656661756c742027302e3030272c'
      
        '20204d53554d4d4520666c6f61742831302c3229204e4f54204e554c4c206465' +
        '6661756c742027302e3030272c'
      
        '20204253554d4d4520666c6f61742831302c3229204e4f54204e554c4c206465' +
        '6661756c742027302e3030272c'
      
        '2020415453554d4d4520666c6f61742831302c3229204e4f54204e554c4c2064' +
        '656661756c742027302e3030272c'
      
        '202041544d53554d4d4520666c6f61742831302c3229204e4f54204e554c4c20' +
        '64656661756c742027302e3030272c'
      
        '20205741454852554e472076617263686172283529204e4f54204e554c4c2064' +
        '656661756c742027272c'
      
        '2020474547454e4b4f4e544f20696e7428313129204e4f54204e554c4c206465' +
        '6661756c7420272d31272c'
      
        '2020534f4c4c5f53544147452074696e79696e74283429204e4f54204e554c4c' +
        '2064656661756c74202730272c'
      
        '2020534f4c4c5f534b4f4e544f20666c6f617428352c3229204e4f54204e554c' +
        '4c2064656661756c742027302e3030272c'
      
        '2020534f4c4c5f4e544147452074696e79696e74283429204e4f54204e554c4c' +
        '2064656661756c74202730272c'
      
        '2020534f4c4c5f524154454e2074696e79696e74283429204e4f54204e554c4c' +
        '2064656661756c74202731272c'
      
        '2020534f4c4c5f5241544245545220666c6f61742831302c3229204e4f54204e' +
        '554c4c2064656661756c742027302e3030272c'
      
        '2020534f4c4c5f524154494e54455256414c4c20696e7428313129204e4f5420' +
        '4e554c4c2064656661756c74202730272c'
      
        '20205354414449554d2074696e79696e74283429204e4f54204e554c4c206465' +
        '6661756c74202730272c'
      '202045525354454c4c5420646174652064656661756c74204e554c4c2c'
      
        '2020455253545f4e414d452076617263686172283230292064656661756c7420' +
        '4e554c4c2c'
      
        '20204b554e5f4e554d2076617263686172283230292064656661756c74204e55' +
        '4c4c2c'
      
        '20204b554e5f414e524544452076617263686172283330292064656661756c74' +
        '204e554c4c2c'
      
        '20204b554e5f4e414d45312076617263686172283330292064656661756c7420' +
        '4e554c4c2c'
      
        '20204b554e5f4e414d45322076617263686172283330292064656661756c7420' +
        '4e554c4c2c'
      
        '20204b554e5f4e414d45332076617263686172283330292064656661756c7420' +
        '4e554c4c2c'
      
        '20204b554e5f41425445494c554e472076617263686172283330292064656661' +
        '756c74204e554c4c2c'
      
        '20204b554e5f535452415353452076617263686172283330292064656661756c' +
        '74204e554c4c2c'
      '20204b554e5f4c414e4420636861722832292064656661756c74204e554c4c2c'
      
        '20204b554e5f504c5a20766172636861722835292064656661756c74204e554c' +
        '4c2c'
      
        '20204b554e5f4f52542076617263686172283330292064656661756c74204e55' +
        '4c4c2c'
      '2020555352312076617263686172283830292064656661756c74204e554c4c2c'
      '2020555352322076617263686172283830292064656661756c74204e554c4c2c'
      
        '202050524f4a454b542076617263686172283830292064656661756c74204e55' +
        '4c4c2c'
      
        '20204f52474e554d2076617263686172283230292064656661756c74204e554c' +
        '4c2c'
      
        '2020424553545f4e414d452076617263686172283430292064656661756c7420' +
        '4e554c4c2c'
      
        '2020424553545f434f44452074696e79696e742834292064656661756c74204e' +
        '554c4c2c'
      '2020424553545f444154554d20646174652064656661756c74204e554c4c2c'
      '2020494e464f20746578742c'
      
        '202042525554544f5f464c414720656e756d28274e272c27592729204e4f5420' +
        '4e554c4c2064656661756c7420274e272c'
      
        '20204d5753545f465245495f464c414720656e756d28274e272c27592729204e' +
        '4f54204e554c4c2064656661756c7420274e272c'
      '20205052494d415259204b45592020285245435f4944292c'
      '20204b455920414444525f49442028414444525f494429'
      '293b'
      ''
      ''
      
        '435245415445205441424c45204946204e4f5420455849535453205645525452' +
        '4147504f532028'
      
        '20205245435f494420696e7428313129204e4f54204e554c4c206175746f5f69' +
        '6e6372656d656e742c'
      
        '20204a4f55524e414c5f494420696e7428313129204e4f54204e554c4c206465' +
        '6661756c74202730272c'
      
        '2020415254494b454c5459502063686172283129204e4f54204e554c4c206465' +
        '6661756c742027272c'
      
        '2020415254494b454c5f494420696e7428313129204e4f54204e554c4c206465' +
        '6661756c7420272d31272c'
      
        '2020414444525f494420696e7428313129204e4f54204e554c4c206465666175' +
        '6c7420272d31272c'
      
        '20205656544e554d20696e7428313129204e4f54204e554c4c2064656661756c' +
        '7420272d31272c'
      
        '2020504f534954494f4e20696e7428313129204e4f54204e554c4c2064656661' +
        '756c74202730272c'
      '2020564945575f504f5320636861722833292064656661756c74204e554c4c2c'
      
        '20204d41544348434f44452076617263686172283230292064656661756c7420' +
        '4e554c4c2c'
      
        '20204152544e554d2076617263686172283230292064656661756c74204e554c' +
        '4c2c'
      
        '2020424152434f44452076617263686172283230292064656661756c74204e55' +
        '4c4c2c'
      
        '20204d454e474520666c6f61742831302c3229204e4f54204e554c4c20646566' +
        '61756c742027302e3030272c'
      
        '20204c41454e47452076617263686172283230292064656661756c74204e554c' +
        '4c2c'
      
        '202047524f455353452076617263686172283230292064656661756c74204e55' +
        '4c4c2c'
      
        '202044494d454e53494f4e2076617263686172283230292064656661756c7420' +
        '4e554c4c2c'
      
        '20204745574943485420666c6f61742831302c3329204e4f54204e554c4c2064' +
        '656661756c742027302e303030272c'
      
        '20204d455f45494e484549542076617263686172283130292064656661756c74' +
        '204e554c4c2c'
      
        '202050525f45494e4845495420666c6f61742831302c3329204e4f54204e554c' +
        '4c2064656661756c742027312e303030272c'
      
        '202045505245495320666c6f61742831302c3329204e4f54204e554c4c206465' +
        '6661756c742027302e303030272c'
      
        '2020455f52474557494e4e20666c6f61742831302c3229204e4f54204e554c4c' +
        '2064656661756c742027302e3030272c'
      
        '202052414241545420666c6f61742831302c3229204e4f54204e554c4c206465' +
        '6661756c742027302e3030272c'
      
        '20205241424154543220666c6f61742831302c3229204e4f54204e554c4c2064' +
        '656661756c742027302e3030272c'
      
        '20205241424154543320666c6f61742831302c3229204e4f54204e554c4c2064' +
        '656661756c742027302e3030272c'
      
        '20205354455545525f434f44452074696e79696e74283429204e4f54204e554c' +
        '4c2064656661756c74202730272c'
      
        '2020414c545445494c5f50524f5a20666c6f61742831302c3229204e4f54204e' +
        '554c4c2064656661756c742027302e3130272c'
      
        '2020414c545445494c5f5354434f44452074696e79696e74283429204e4f5420' +
        '4e554c4c2064656661756c74202730272c'
      
        '2020474547454e4b544f20696e7428313129204e4f54204e554c4c2064656661' +
        '756c7420272d31272c'
      '202042455a454943484e554e4720746578742c'
      
        '2020414c545445494c5f464c414720656e756d28274e272c27592729204e4f54' +
        '204e554c4c2064656661756c7420274e272c'
      
        '202042455a5f464553545f464c414720656e756d28274e272c27592729204e4f' +
        '54204e554c4c2064656661756c7420274e272c'
      
        '202042525554544f5f464c414720656e756d28274e272c27592729204e4f5420' +
        '4e554c4c2064656661756c7420274e272c'
      '20205052494d415259204b45592020285245435f4944292c'
      '20204b455920415254494b454c5f49442028415254494b454c5f4944292c'
      '20204b455920414444525f49442028414444525f4944292c'
      
        '20204b4559204a4f55524e414c5f494420284a4f55524e414c5f49442c504f53' +
        '4954494f4e29'
      '293b'
      ''
      
        '494e5345525420494e544f205245474953544552592056414c55455328224d41' +
        '494e5c5c4e554d42455253222c202256455254524147222c2022303030303030' +
        '222c204e554c4c2c20223233222c204e554c4c2c204e554c4c2c202231303030' +
        '2e3030222c204e554c4c2c204e554c4c2c204e554c4c2c204e554c4c2c202230' +
        '222c202230222c2022323030333035313332313432343222293b'
      ''
      '44524f50205441424c4520494620455849535453204c414e443b'
      '435245415445205441424c45204c414e442028'
      
        '202049442063686172283229204e4f54204e554c4c2064656661756c74202727' +
        '2c'
      
        '20204e414d4520766172636861722831303029204e4f54204e554c4c20646566' +
        '61756c742027272c'
      
        '202049534f5f434f44455f332063686172283329204e4f54204e554c4c206465' +
        '6661756c742027272c'
      
        '2020464f524d41542074696e79696e74283329204e4f54204e554c4c20646566' +
        '61756c74202731272c'
      
        '2020564f525741484c2076617263686172283130292064656661756c74204e55' +
        '4c4c2c'
      
        '20205741454852554e4720766172636861722835292064656661756c74204e55' +
        '4c4c2c'
      '20205350524143484520636861722833292064656661756c74204e554c4c2c'
      '20205052494d415259204b45592020284944292c'
      '2020554e49515545204b4559204944585f4e414d4520284e414d4529'
      '293b'
      ''
      '44524f50205441424c452049462045584953545320424c5a3b'
      '435245415445205441424c4520424c5a2028'
      
        '2020424c5a20696e7428313029204e4f54204e554c4c2064656661756c742027' +
        '30272c'
      
        '202042414e4b5f4e414d4520766172636861722832353529204e4f54204e554c' +
        '4c2064656661756c742027272c'
      
        '202050525a2063686172283229204e4f54204e554c4c2064656661756c742027' +
        '272c'
      '20205052494d415259204b4559202028424c5a292c'
      '20204b4559204944585f4e414d45202842414e4b5f4e414d452c424c5a29'
      '293b'
      ''
      ''
      '2f2a205570646174652056657273696f6e736e756d6d6572202a2f'
      
        '44454c4554452046524f4d20524547495354455259207768657265204d41494e' +
        '4b45593d224d41494e2220616e64204e414d453d2244425f56455253494f4e22' +
        '3b'
      
        '494e5345525420494e544f2052454749535445525920284d41494e4b45592c20' +
        '4e414d452c2056414c5f43484152292056616c7565732028274d41494e272c20' +
        '2744425f56455253494f4e272c2027312e303527293b')
  end
  object DBUpdTo1_06: TJvStrHolder
    Capacity = 60
    Macros = <>
    Left = 472
    Top = 464
    InternalVer = 1
    StrData = (
      ''
      
        '414c544552205441424c452060464942555f4b4153534560204348414e474520' +
        '604a4f5554524e414c5f49446020604a4f55524e414c5f49446020494e542831' +
        '31293b'
      ''
      
        '414c544552205441424c452060414452455353454e60204348414e474520604b' +
        '554e4e554d316020604b554e4e554d31602056415243484152283230293b'
      ''
      
        '414c544552205441424c4520605645525452414760204348414e474520604441' +
        '554d5f4e455854602060444154554d5f4e455854602044415445204445464155' +
        '4c542022303030302d30302d303022204e4f54204e554c4c3b'
      ''
      
        '414c544552205441424c452060415254494b454c60204348414e474520604b55' +
        '525a4e414d456020604b55525a4e414d45602056415243484152283830293b'
      
        '414c544552205441424c452060415254494b454c60204348414e474520604b41' +
        '535f4e414d456020604b41535f4e414d45602056415243484152283830293b'
      
        '414c544552205441424c452060415254494b454c60204348414e474520605348' +
        '4f505f50524549535f4c4953544560206053484f505f50524549535f4c495354' +
        '456020464c4f41542831322c3829202044454641554c542022302e3030223b'
      
        '414c544552205441424c452060415254494b454c60204348414e474520605348' +
        '4f505f50524549535f5350454360206053484f505f50524549535f5350454360' +
        '20464c4f41542831322c3829202044454641554c542022302e3030223b'
      ''
      
        '414c544552205441424c4520604a4f55524e414c60204348414e474520604b55' +
        '4e5f414e524544456020604b554e5f414e524544456020564152434841522834' +
        '30293b'
      
        '414c544552205441424c4520604a4f55524e414c60204348414e474520604b55' +
        '4e5f4e414d45316020604b554e5f4e414d453160205641524348415228343029' +
        '3b'
      
        '414c544552205441424c4520604a4f55524e414c60204348414e474520604b55' +
        '4e5f4e414d45326020604b554e5f4e414d453260205641524348415228343029' +
        '3b'
      
        '414c544552205441424c4520604a4f55524e414c60204348414e474520604b55' +
        '4e5f4e414d45336020604b554e5f4e414d453360205641524348415228343029' +
        '3b'
      
        '414c544552205441424c4520604a4f55524e414c60204348414e474520604b55' +
        '4e5f41425445494c554e476020604b554e5f41425445494c554e476020564152' +
        '43484152283430293b'
      
        '414c544552205441424c4520604a4f55524e414c60204348414e474520604b55' +
        '4e5f535452415353456020604b554e5f53545241535345602056415243484152' +
        '283430293b'
      
        '414c544552205441424c4520604a4f55524e414c60204348414e474520604b55' +
        '4e5f4c414e446020604b554e5f4c414e446020434841522835293b'
      
        '414c544552205441424c4520604a4f55524e414c60204348414e474520604b55' +
        '4e5f504c5a6020604b554e5f504c5a602056415243484152283130293b'
      
        '414c544552205441424c4520604a4f55524e414c60204348414e474520604b55' +
        '4e5f4f52546020604b554e5f4f5254602056415243484152283430293b'
      ''
      ''
      
        '414c544552205441424c4520605645525452414760204348414e474520604b55' +
        '4e5f414e524544456020604b554e5f414e524544456020564152434841522834' +
        '30293b'
      
        '414c544552205441424c4520605645525452414760204348414e474520604b55' +
        '4e5f4e414d45316020604b554e5f4e414d453160205641524348415228343029' +
        '3b'
      
        '414c544552205441424c4520605645525452414760204348414e474520604b55' +
        '4e5f4e414d45326020604b554e5f4e414d453260205641524348415228343029' +
        '3b'
      
        '414c544552205441424c4520605645525452414760204348414e474520604b55' +
        '4e5f4e414d45336020604b554e5f4e414d453360205641524348415228343029' +
        '3b'
      
        '414c544552205441424c4520605645525452414760204348414e474520604b55' +
        '4e5f41425445494c554e476020604b554e5f41425445494c554e476020564152' +
        '43484152283430293b'
      
        '414c544552205441424c4520605645525452414760204348414e474520604b55' +
        '4e5f535452415353456020604b554e5f53545241535345602056415243484152' +
        '283430293b'
      
        '414c544552205441424c4520605645525452414760204348414e474520604b55' +
        '4e5f4c414e446020604b554e5f4c414e446020434841522835293b'
      
        '414c544552205441424c4520605645525452414760204348414e474520604b55' +
        '4e5f504c5a6020604b554e5f504c5a602056415243484152283130293b'
      
        '414c544552205441424c4520605645525452414760204348414e474520604b55' +
        '4e5f4f52546020604b554e5f4f5254602056415243484152283430293b'
      ''
      
        '435245415445205441424c45204946204e4f5420455849535453204558504f52' +
        '542028'
      
        '2020494420696e7428313129204e4f54204e554c4c206175746f5f696e637265' +
        '6d656e742c'
      
        '20204b55525a42455a20766172636861722832353529204e4f54204e554c4c20' +
        '64656661756c742027272c'
      '2020494e464f2074657874204e4f54204e554c4c2c'
      '202051554552592074657874204e4f54204e554c4c2c'
      '202046454c44455220746578742c'
      '2020464f524d554c415220626c6f622c'
      '2020464f524d415420636861722833292064656661756c74204e554c4c2c'
      
        '202046494c454e414d45207661726368617228323535292064656661756c7420' +
        '4e554c4c2c'
      
        '20204c4153545f4348414e47452074696d657374616d7028313429204e4f5420' +
        '4e554c4c2c'
      
        '20204348414e47455f4e414d4520766172636861722831303029206465666175' +
        '6c74204e554c4c2c'
      '20205052494d415259204b4559202028494429'
      '293b'
      ''
      '2f2a205570646174652056657273696f6e736e756d6d6572202a2f'
      
        '44454c4554452046524f4d20524547495354455259207768657265204d41494e' +
        '4b45593d224d41494e2220616e64204e414d453d2244425f56455253494f4e22' +
        '3b'
      
        '494e5345525420494e544f2052454749535445525920284d41494e4b45592c20' +
        '4e414d452c2056414c5f43484152292056616c7565732028274d41494e272c20' +
        '2744425f56455253494f4e272c2027312e303627293b')
  end
  object DBUpdTo1_07: TJvStrHolder
    Capacity = 228
    Macros = <>
    Left = 472
    Top = 512
    InternalVer = 1
    StrData = (
      ''
      
        '494e5345525420494e544f205245474953544552592056414c55455328224d41' +
        '494e5c5c4e554d42455253222c2022415254494b454c4e554d4d4552222c2022' +
        '5c2241525c22303030303030222c204e554c4c2c20223938222c204e554c4c2c' +
        '204e554c4c2c2022313030302e3030222c204e554c4c2c204e554c4c2c204e55' +
        '4c4c2c204e554c4c2c202230222c202230222c20223230303330353133323134' +
        '32343222293b'
      
        '494e5345525420494e544f205245474953544552592056414c55455328225348' +
        '4f505c5c4c4945464152545f4d4150222c202244454641554c54222c20224d61' +
        '7070696e672066fc72204c6965666572617274656e222c204e554c4c2c204e55' +
        '4c4c2c204e554c4c2c204e554c4c2c204e554c4c2c204e554c4c2c204e554c4c' +
        '2c204e554c4c2c20312c20224e222c20224e222c202232303033303531333231' +
        '3432343222293b'
      
        '494e5345525420494e544f205245474953544552592056414c55455328225348' +
        '4f505c5c5a41484c4152545f4d4150222c202244454641554c54222c20224d61' +
        '7070696e672066fc72205a61686c756e6773617274656e222c204e554c4c2c20' +
        '4e554c4c2c204e554c4c2c204e554c4c2c204e554c4c2c204e554c4c2c204e55' +
        '4c4c2c204e554c4c2c20312c20224e222c20224e222c20223230303330353133' +
        '32313432343222293b'
      
        '494e5345525420494e544f205245474953544552592056414c55455328224d41' +
        '494e5c5c494d504f5254222c202244454641554c54222c202250726f66696c65' +
        '2066fc722064656e20496d706f7274222c204e554c4c2c204e554c4c2c204e55' +
        '4c4c2c204e554c4c2c204e554c4c2c204e554c4c2c204e554c4c2c204e554c4c' +
        '2c20312c20224e222c20224e222c202232303033303531333231343234322229' +
        '3b'
      
        '494e5345525420494e544f205245474953544552592056414c55455328224d41' +
        '494e5c5c494d504f52545c5c415254494b454c222c202244454641554c54222c' +
        '202250726f66696c652066fc722064656e20496d706f727420766f6e20417274' +
        '696b656c6e222c204e554c4c2c204e554c4c2c204e554c4c2c204e554c4c2c20' +
        '4e554c4c2c204e554c4c2c204e554c4c2c204e554c4c2c20312c20224e222c20' +
        '224e222c2022323030333035313332313432343222293b'
      
        '494e5345525420494e544f205245474953544552592056414c55455328224d41' +
        '494e5c5c494d504f52545c5c414452455353454e222c202244454641554c5422' +
        '2c202250726f66696c652066fc722064656e20496d706f727420766f6e204164' +
        '72657373656e222c204e554c4c2c204e554c4c2c204e554c4c2c204e554c4c2c' +
        '204e554c4c2c204e554c4c2c204e554c4c2c204e554c4c2c20312c20224e222c' +
        '20224e222c2022323030333035313332313432343222293b'
      ''
      
        '494e5345525420494e544f205245474953544552592056414c55455328224d41' +
        '494e5c5c415254494b454c222c2022564b315f43414c435f46414b544f52222c' +
        '204e554c4c202c204e554c4c2c204e554c4c2c204e554c4c2c204e554c4c2c20' +
        '4e554c4c2c202230222c204e554c4c2c204e554c4c2c20352c202230222c2022' +
        '30222c2022323030333035313332313432343222293b'
      
        '494e5345525420494e544f205245474953544552592056414c55455328224d41' +
        '494e5c5c415254494b454c222c2022564b325f43414c435f46414b544f52222c' +
        '204e554c4c202c204e554c4c2c204e554c4c2c204e554c4c2c204e554c4c2c20' +
        '4e554c4c2c202230222c204e554c4c2c204e554c4c2c20352c202230222c2022' +
        '30222c2022323030333035313332313432343222293b'
      
        '494e5345525420494e544f205245474953544552592056414c55455328224d41' +
        '494e5c5c415254494b454c222c2022564b335f43414c435f46414b544f52222c' +
        '204e554c4c202c204e554c4c2c204e554c4c2c204e554c4c2c204e554c4c2c20' +
        '4e554c4c2c202230222c204e554c4c2c204e554c4c2c20352c202230222c2022' +
        '30222c2022323030333035313332313432343222293b'
      
        '494e5345525420494e544f205245474953544552592056414c55455328224d41' +
        '494e5c5c415254494b454c222c2022564b345f43414c435f46414b544f52222c' +
        '204e554c4c202c204e554c4c2c204e554c4c2c204e554c4c2c204e554c4c2c20' +
        '4e554c4c2c202230222c204e554c4c2c204e554c4c2c20352c202230222c2022' +
        '30222c2022323030333035313332313432343222293b'
      
        '494e5345525420494e544f205245474953544552592056414c55455328224d41' +
        '494e5c5c415254494b454c222c2022564b355f43414c435f46414b544f52222c' +
        '204e554c4c202c204e554c4c2c204e554c4c2c204e554c4c2c204e554c4c2c20' +
        '4e554c4c2c202230222c204e554c4c2c204e554c4c2c20352c202230222c2022' +
        '30222c2022323030333035313332313432343222293b'
      
        '494e5345525420494e544f205245474953544552592056414c55455328224d41' +
        '494e5c5c415254494b454c222c202253484f505f43414c435f46414b544f5222' +
        '2c204e554c4c202c204e554c4c2c204e554c4c2c204e554c4c2c204e554c4c2c' +
        '204e554c4c2c202230222c204e554c4c2c204e554c4c2c20352c202230222c20' +
        '2230222c2022323030333035313332313432343222293b'
      ''
      
        '494e5345525420494e544f205245474953544552592056414c55455328224d41' +
        '494e5c5c414452455353454e222c20224b554e4e554d315f454449222c204e55' +
        '4c4c202c204e554c4c2c202730272c204e554c4c2c204e554c4c2c204e554c4c' +
        '2c204e554c4c2c204e554c4c2c204e554c4c2c20332c202230222c202230222c' +
        '2022323030333035313332313432343222293b'
      ''
      '2f2a20425547464958202a2f'
      
        '414c544552205441424c45205241424154544752555050454e2044524f502050' +
        '52494d415259204b45592c20414444205052494d415259204b45592028524142' +
        '4752505f49442c5241424752505f5459502c4c4945465f5241424752502c4d49' +
        '4e5f4d454e4745293b'
      ''
      '2f2a2042525554544f5052454953452066fc7220616c6c6520564b202a2f'
      '414c544552205441424c4520415254494b454c'
      
        '2041444420564b314220464c4f41542831302c3229202044454641554c542022' +
        '302e303022204e4f54204e554c4c20414654455220564b312c'
      
        '2041444420564b324220464c4f41542831302c3229202044454641554c542022' +
        '302e303022204e4f54204e554c4c20414654455220564b322c'
      
        '2041444420564b334220464c4f41542831302c3229202044454641554c542022' +
        '302e303022204e4f54204e554c4c20414654455220564b332c'
      
        '2041444420564b344220464c4f41542831302c3229202044454641554c542022' +
        '302e303022204e4f54204e554c4c20414654455220564b342c'
      
        '2041444420564b354220464c4f41542831302c3229202044454641554c542022' +
        '302e303022204e4f54204e554c4c20414654455220564b352c'
      
        '20414444204845525354454c4c45525f494420494e5428313129202044454641' +
        '554c5420222d3122204e4f54204e554c4c204146544552204845524b554e4653' +
        '4c414e443b'
      ''
      '2f2a2046656c646572207665726ce46e6765726e202a2f'
      '414c544552205441424c4520414452455353454e'
      
        '204348414e47452054454c45312054454c453120564152434841522831303029' +
        '2c'
      
        '204348414e47452054454c45322054454c453220564152434841522831303029' +
        '2c'
      '204348414e47452046415820464158205641524348415228313030292c'
      '204348414e47452046554e4b2046554e4b205641524348415228313030292c'
      
        '204348414e474520454d41494c20454d41494c20564152434841522831303029' +
        '2c'
      
        '204348414e474520494e5445524e455420494e5445524e455420564152434841' +
        '5228313030292c'
      
        '204348414e474520444956455253455320444956455253455320564152434841' +
        '5228313030293b'
      ''
      '414c544552205441424c452041504152544e4552'
      
        '204348414e47452054454c45464f4e2054454c45464f4e205641524348415228' +
        '313030292c'
      
        '204348414e47452054454c454641582054454c45464158205641524348415228' +
        '313030292c'
      
        '204348414e4745204d4f42494c46554e4b204d4f42494c46554e4b2056415243' +
        '48415228313030292c'
      
        '204348414e47452054454c455052495641542054454c45505249564154205641' +
        '524348415228313030293b'
      ''
      
        '435245415445205441424c45204946204e4f5420455849535453204845525354' +
        '454c4c45522028'
      
        '202053484f505f49442054494e59494e54283329204e4f54204e554c4c206465' +
        '6661756c7420272d31272c'
      
        '20204845525354454c4c45525f494420696e7428313129204e4f54204e554c4c' +
        '206175746f5f696e6372656d656e742c'
      
        '2020414444525f494420696e7428313129204e4f54204e554c4c206465666175' +
        '6c7420272d31272c'
      
        '20204845525354454c4c45525f4e414d45207661726368617228333229204e4f' +
        '54204e554c4c2064656661756c742027272c'
      
        '20204845525354454c4c45525f494d4147452076617263686172283634292064' +
        '656661756c74204e554c4c2c'
      
        '20204c4153545f4348414e4745206461746574696d652064656661756c74204e' +
        '554c4c2c'
      
        '202053484f505f444154455f4144444544206461746574696d65206465666175' +
        '6c74204e554c4c2c'
      
        '202053484f505f444154455f4348414e4745206461746574696d652064656661' +
        '756c74204e554c4c2c'
      
        '202053594e435f464c414720454e554d28274e272c2759272920204445464155' +
        '4c5420224e22204e4f54204e554c4c2c'
      
        '20204348414e47455f464c414720454e554d28274e272c275927292020444546' +
        '41554c5420224e22204e4f54204e554c4c2c'
      
        '202044454c5f464c414720454e554d28274e272c27592729202044454641554c' +
        '5420224e22204e4f54204e554c4c2c'
      
        '20205052494d415259204b455920202853484f505f49442c204845525354454c' +
        '4c45525f4944292c'
      
        '20204b4559204944585f4845525354454c4c45525f4e414d4520284845525354' +
        '454c4c45525f4e414d4529'
      '293b'
      ''
      
        '435245415445205441424c45204946204e4f5420455849535453204845525354' +
        '454c4c45525f494e464f2028'
      
        '202053484f505f49442054494e59494e54283329204e4f54204e554c4c206465' +
        '6661756c7420272d31272c'
      
        '20204845525354454c4c45525f494420696e7428313129204e4f54204e554c4c' +
        '2064656661756c74202730272c'
      
        '2020535052414348455f494420696e7428313129204e4f54204e554c4c206465' +
        '6661756c74202730272c'
      
        '20204845525354454c4c45525f55524c20766172636861722832353529204e4f' +
        '54204e554c4c2064656661756c742027272c'
      
        '202055524c5f434c49434b454420696e74283529204e4f54204e554c4c206465' +
        '6661756c74202730272c'
      
        '2020444154455f4c4153545f434c49434b206461746574696d65206465666175' +
        '6c74204e554c4c2c'
      
        '20204c4153545f4348414e4745206461746574696d652064656661756c74204e' +
        '554c4c2c'
      
        '202053484f505f444154455f4144444544206461746574696d65206465666175' +
        '6c74204e554c4c2c'
      
        '202053484f505f444154455f4348414e4745206461746574696d652064656661' +
        '756c74204e554c4c2c'
      
        '202053594e435f464c414720454e554d28274e272c2759272920204445464155' +
        '4c5420224e22204e4f54204e554c4c2c'
      
        '20204348414e47455f464c414720454e554d28274e272c275927292020444546' +
        '41554c5420224e22204e4f54204e554c4c2c'
      
        '202044454c5f464c414720454e554d28274e272c27592729202044454641554c' +
        '5420224e22204e4f54204e554c4c2c'
      
        '20205052494d415259204b455920202853484f505f49442c204845525354454c' +
        '4c45525f49442c20535052414348455f494429'
      '293b'
      ''
      
        '435245415445205441424c45204946204e4f5420455849535453204c494e4b20' +
        '28'
      
        '20204d4f44554c5f49442054494e59494e54283329204e4f54204e554c4c2044' +
        '454641554c5420272d31272c'
      
        '20205245435f494420494e5428313129204e4f54204e554c4c2044454641554c' +
        '5420272d31272c'
      
        '20205046414420564152434841522832353529204e4f54204e554c4c20646566' +
        '61756c742027272c'
      
        '2020444154454920564152434841522832303029204e4f54204e554c4c206465' +
        '6661756c742027272c'
      '202042454d45524b554e4720544558542c'
      
        '20204c4153545f4348414e47452054494d455354414d502064656661756c7420' +
        '4e554c4c2c'
      
        '20204c4153545f4348414e47455f55534552205641524348415228353029204e' +
        '4f54204e554c4c2c'
      
        '20204f50454e5f464c414720454e554d28274e272c2759272920204445464155' +
        '4c5420224e22204e4f54204e554c4c2c'
      
        '20204f50454e5f5553455220564152434841522835302920204e4f54204e554c' +
        '4c2c'
      '20204f50454e5f54494d452054494d455354414d50204e4f54204e554c4c2c'
      
        '20205052494d415259204b45592020284d4f44554c5f49442c205245435f4944' +
        '2c20504641442c20444154454929'
      '293b20'
      ''
      
        '2f2a20546162656c6c652066fc72204c6965666572616e74656e2d20756e6420' +
        '4b756e64656e707265697365202a2f'
      
        '435245415445205441424c45204946204e4f542045584953545320415254494b' +
        '454c5f50524549532028'
      
        '2020415254494b454c5f494420696e7428313129204e4f54204e554c4c206465' +
        '6661756c7420272d31272c'
      
        '20204144524553535f494420696e7428313129204e4f54204e554c4c20646566' +
        '61756c7420272d31272c'
      
        '202050524549535f5459502074696e79696e7428322920756e7369676e656420' +
        '4e4f54204e554c4c2064656661756c74202730272c'
      
        '2020424553544e554d2076617263686172283530292064656661756c74204e55' +
        '4c4c2c'
      
        '2020505245495320666c6f61742831302c3329204e4f54204e554c4c20646566' +
        '61756c742027302e303030272c'
      '2020494e464f20746578742c'
      '2020474541454e442074696d657374616d7028313429204e4f54204e554c4c2c'
      
        '2020474541454e445f4e414d452076617263686172283230292064656661756c' +
        '74204e554c4c2c'
      
        '20205052494d415259204b4559202028415254494b454c5f49442c2041445245' +
        '53535f49442c2050524549535f54595029'
      '293b'
      ''
      
        '2f2a20446174656e206175732064656e20616b7475656c6c656e20417274696b' +
        '656c66656c6465726e20fc6265726e65686d656e202a2f'
      '696e7365727420494e544f20415254494b454c5f5052454953'
      '2073656c65637420'
      '205245435f494420617320415254494b454c5f49442c'
      '204c49454631206173204144524553535f49442c'
      '20352061732050524549535459502c'
      '204c494546315f424e554d20617320424553544e554d2c'
      '204c494546315f50524549532061732050524549532c'
      '20272720617320494e464f2c'
      '204e4f5728292c'
      '2027494d504f52542720'
      
        '2066726f6d20415254494b454c207768657265204c494546313e3020616e6420' +
        '4c494546315f5052454953213d303b'
      ''
      '696e7365727420494e544f20415254494b454c5f5052454953'
      '2073656c656374'
      '205245435f494420617320415254494b454c5f49442c'
      '204c49454632206173204144524553535f49442c'
      '20352061732050524549535459502c'
      '204c494546325f424e554d20617320424553544e554d2c'
      '204c494546325f50524549532061732050524549532c'
      '20272720617320494e464f2c'
      '204e4f5728292c'
      '2027494d504f52542720'
      
        '2066726f6d20415254494b454c207768657265204c494546323e3020616e6420' +
        '4c494546325f5052454953213d303b'
      ''
      '2f2a2057415254554e472066fc722044616e69656c2a2f'
      
        '435245415445205441424c45204946204e4f5420455849535453205741525455' +
        '4e472028'
      
        '20205245435f494420696e7428313129204e4f54204e554c4c206175746f5f69' +
        '6e6372656d656e742c'
      
        '2020414444525f494420696e7428313129204e4f54204e554c4c206465666175' +
        '6c74202730272c'
      
        '2020424553434852454942554e47207661726368617228323535292064656661' +
        '756c74204e554c4c2c'
      '202057415254554e4720646174652064656661756c74204e554c4c2c'
      '2020494e54455256414c4c20696e74283131292064656661756c74202730272c'
      
        '202057564552545241472076617263686172283230292064656661756c74204e' +
        '554c4c2c'
      '202042454d45524b554e4720746578742c'
      '20204c4542454e534c41554620746578742c'
      '20204c4953544520746578742c'
      
        '202057415254554e475f5459502076617263686172283230292064656661756c' +
        '74204e554c4c2c'
      
        '202057564552545241475f4e5220736d616c6c696e742836292064656661756c' +
        '74202730272c'
      
        '202042454d5f4f5054312074696e79696e7428312920756e7369676e65642064' +
        '656661756c74202730272c'
      
        '202042454d5f4f5054322074696e79696e7428312920756e7369676e65642064' +
        '656661756c74202730272c'
      
        '202042454d5f4f5054332074696e79696e7428312920756e7369676e65642064' +
        '656661756c74202730272c'
      
        '202042454d5f4f5054342074696e79696e7428312920756e7369676e65642064' +
        '656661756c74202730272c'
      
        '202042454d5f4f5054352074696e79696e7428312920756e7369676e65642064' +
        '656661756c74202730272c'
      
        '202042454d5f4f5054362074696e79696e7428312920756e7369676e65642064' +
        '656661756c74202730272c'
      
        '202042454d5f4f5054372074696e79696e7428312920756e7369676e65642064' +
        '656661756c74202730272c'
      
        '202042454d5f4f5054382074696e79696e7428312920756e7369676e65642064' +
        '656661756c74202730272c'
      
        '202042454d5f4f5054392074696e79696e7428312920756e7369676e65642064' +
        '656661756c74202730272c'
      '202045525354454c4c5420646174652064656661756c74204e554c4c2c'
      
        '2020455253545f4e414d452076617263686172283230292064656661756c7420' +
        '4e554c4c2c'
      '2020474541454e4445525420646174652064656661756c74204e554c4c2c'
      
        '2020474541454e445f4e414d452076617263686172283230292064656661756c' +
        '74204e554c4c2c'
      '20205052494d415259204b45592020285245435f494429'
      '293b'
      ''
      ''
      
        '435245415445205441424c45204946204e4f542045584953545320574152454e' +
        '4752555050454e2028'
      
        '2020494420696e7428313129204e4f54204e554c4c206175746f5f696e637265' +
        '6d656e742c'
      
        '2020544f505f494420696e7428313129204e4f54204e554c4c2064656661756c' +
        '7420272d31272c'
      
        '20204e414d4520766172636861722832353029204e4f54204e554c4c20646566' +
        '61756c742027272c'
      '2020424553434852454942554e4720746578742c'
      '20204445465f454b544f20696e74283131292064656661756c7420272d31272c'
      '20204445465f414b544f20696e74283131292064656661756c7420272d31272c'
      '2020564f52474142454e20746578742c'
      '20205052494d415259204b45592028494429'
      '293b'
      ''
      '494e5345525420494e544f20574152454e4752555050454e'
      
        '2073656c6563742056414c5f494e542061732049442c202d3120617320544f50' +
        '5f49442c204e414d452c204e554c4c20415320424553434852454942554e472c' +
        '2056414c5f494e5433206173204445465f454b544f2c2056414c5f494e543220' +
        '6173204445465f414b544f2c204e554c4c20617320564f52474142454e2020'
      '2066726f6d2052454749535445525920'
      
        '207768657265204d41494e4b45593d274d41494e5c5c4152545f484952272061' +
        '6e642056414c5f494e543e3020616e642056414c5f424c4f42204c494b452022' +
        '574152454e4752555050453d25223b'
      ''
      '64656c6574652046524f4d2052454749535445525920'
      
        '207768657265204d41494e4b45593d274d41494e5c5c4152545f484952272061' +
        '6e642056414c5f424c4f42204c494b452022574152454e4752555050453d2522' +
        '3b'
      ''
      
        '494e5345525420494e544f205245474953544552592056414c55455328224d41' +
        '494e5c5c5a41484c415254222c2022566f726b61737365222c202230222c204e' +
        '554c4c2c202230222c204e554c4c2c204e554c4c2c2022302e3030222c202233' +
        '30222c204e554c4c2c20224265747261672070657220566f726b617373652065' +
        '7268616c74656e222c204e554c4c2c202230222c202230222c20223230303330' +
        '35313332313432343222293b'
      
        '494e5345525420494e544f205245474953544552592056414c55455328224d41' +
        '494e5c5c5a41484c415254222c20224b72656469746b61727465222c20223722' +
        '2c204e554c4c2c20223134222c204e554c4c2c204e554c4c2c2022322e303022' +
        '2c20223330222c204e554c4c2c2022426574726167207769726420706572204b' +
        '72656469746b617274652065696e67657a6f67656e222c204e554c4c2c202230' +
        '222c202230222c2022323030333035313332313432343222293b'
      
        '494e5345525420494e544f205245474953544552592056414c55455328224d41' +
        '494e5c5c5a41484c415254222c202250617950616c222c202238222c204e554c' +
        '4c2c202230222c204e554c4c2c204e554c4c2c2022302e3030222c202237222c' +
        '204e554c4c2c2022426574726167207065722050617950616c20657268616c74' +
        '656e222c204e554c4c2c202230222c202230222c202232303033303531333231' +
        '3432343222293b'
      ''
      ''
      ''
      ''
      '2f2a205570646174652056657273696f6e736e756d6d6572202a2f'
      
        '44454c4554452046524f4d20524547495354455259207768657265204d41494e' +
        '4b45593d224d41494e2220616e64204e414d453d2244425f56455253494f4e22' +
        '3b'
      
        '494e5345525420494e544f2052454749535445525920284d41494e4b45592c20' +
        '4e414d452c2056414c5f434841522c2056414c5f545950292056616c75657320' +
        '28274d41494e272c202744425f56455253494f4e272c2027312e3037272c2027' +
        '3127293b')
  end
  object HerstellerTab: TZMySqlQuery
    Database = db1
    Transaction = Transact1
    CachedUpdates = False
    ShowRecordTypes = [ztModified, ztInserted, ztUnmodified]
    Options = [doHourGlass, doAutoFillDefs, doUseRowId]
    LinkOptions = [loAlwaysResync]
    Constraints = <>
    ExtraOptions = [moStoreResult]
    Macros = <>
    Sql.Strings = (
      'select * from HERSTELLER'
      'where SHOP_ID=:SID'
      'order by HERSTELLER_NAME')
    RequestLive = False
    Left = 188
    Top = 512
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'SID'
        ParamType = ptUnknown
      end>
  end
  object HerstellerDS: TDataSource
    DataSet = HerstellerTab
    Left = 120
    Top = 512
  end
  object SprachTab: TZMySqlQuery
    Database = db1
    Transaction = Transact1
    CachedUpdates = False
    ShowRecordTypes = [ztModified, ztInserted, ztUnmodified]
    Options = [doHourGlass, doAutoFillDefs, doUseRowId]
    LinkOptions = []
    Constraints = <>
    ExtraOptions = [moStoreResult]
    Macros = <>
    Sql.Strings = (
      'select * from SPRACHEN '
      'order by DEFAULT_FLAG DESC, SORT, SPRACH_ID')
    RequestLive = False
    Left = 320
    Top = 64
  end
  object SprachDS: TDataSource
    DataSet = SprachTab
    Left = 256
    Top = 64
  end
  object ArtInfoTab: TZMySqlQuery
    Database = db1
    Transaction = Transact1
    CachedUpdates = False
    ShowRecordTypes = [ztModified, ztInserted, ztUnmodified]
    Options = [doHourGlass, doAutoFillDefs, doUseRowId]
    LinkOptions = [loAlwaysResync]
    Constraints = <>
    ExtraOptions = [moStoreResult]
    AlternativeView = True
    Macros = <>
    Sql.Strings = (
      'select '
      'A.REC_ID,  A.EK_PREIS,  '
      
        'A.VK1,  A.VK2,  A.VK3,  A.VK4,  A.VK5,  A.VK1B, A.VK2B, A.VK3B, ' +
        'A.VK4B, A.VK5B, '
      
        'AP.PREIS,AP.MENGE2, AP.PREIS2, AP.MENGE3, AP.PREIS3, AP.MENGE4, ' +
        'AP.PREIS4, AP.MENGE5, AP.PREIS5,'
      'A.MENGE_AKT, A.MENGE_BESTELLT, '
      'A.RABGRP_ID,  A.MENGE_VKRE_EDI as MENGE_RESERVIERT, '
      
        'A.ALTTEIL_FLAG, A.NO_RABATT_FLAG, A.NO_PROVISION_FLAG, A.NO_BEZE' +
        'DIT_FLAG, A.NO_VK_FLAG, A.SN_FLAG, '
      'A.PROVIS_PROZ, A.STEUER_CODE, A.ERLOES_KTO, A.AUFW_KTO,'
      
        'A.ARTNUM, A.ERSATZ_ARTNUM, A.MATCHCODE, A.WARENGRUPPE, A.BARCODE' +
        ', A.ARTIKELTYP, A.KAS_NAME, A.ME_EINHEIT, A.PR_EINHEIT,'
      
        'A.LAENGE, A.GROESSE, A.DIMENSION, A.GEWICHT, KURZNAME, LANGNAME,' +
        'AP.PREIS_TYP, AP.ADRESS_ID, A.VPE,AP.BESTNUM'
      ',0.00 as MENGE_LIEF, 0.00 as MENGE_SOLL, JP1.REC_ID as JID '
      'from JOURNALPOS JP1, ARTIKEL A'
      
        'left outer join ARTIKEL_PREIS AP on AP.ARTIKEL_ID = A.REC_ID and' +
        ' AP.PREIS_TYP=3 and AP.ADRESS_ID = - 99'
      
        'where (JP1.JOURNAL_ID=:JID) and (JP1.QUELLE=:QUELLE) and ((JP1.A' +
        'RTIKEL_ID = A.REC_ID) or (A.REC_ID=:AID))'
      'group by A.REC_ID')
    RequestLive = False
    Left = 460
    Top = 183
    ParamData = <
      item
        DataType = ftInteger
        Name = 'JID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'QUELLE'
        ParamType = ptInput
        Value = '13'
      end
      item
        DataType = ftInteger
        Name = 'AID'
        ParamType = ptUnknown
        Value = '-1'
      end>
    object ArtInfoTabREC_ID: TIntegerField
      FieldName = 'REC_ID'
    end
    object ArtInfoTabADRESS_ID: TIntegerField
      FieldName = 'ADRESS_ID'
      Required = True
    end
    object ArtInfoTabEK_PREIS: TFloatField
      FieldName = 'EK_PREIS'
    end
    object ArtInfoTabVK1: TFloatField
      FieldName = 'VK1'
    end
    object ArtInfoTabVK2: TFloatField
      FieldName = 'VK2'
    end
    object ArtInfoTabVK3: TFloatField
      FieldName = 'VK3'
    end
    object ArtInfoTabVK4: TFloatField
      FieldName = 'VK4'
    end
    object ArtInfoTabVK5: TFloatField
      FieldName = 'VK5'
    end
    object ArtInfoTabVK1B: TFloatField
      FieldName = 'VK1B'
      Required = True
    end
    object ArtInfoTabVK2B: TFloatField
      FieldName = 'VK2B'
      Required = True
    end
    object ArtInfoTabVK3B: TFloatField
      FieldName = 'VK3B'
      Required = True
    end
    object ArtInfoTabVK4B: TFloatField
      FieldName = 'VK4B'
      Required = True
    end
    object ArtInfoTabVK5B: TFloatField
      FieldName = 'VK5B'
      Required = True
    end
    object ArtInfoTabPREIS_TYP: TIntegerField
      FieldName = 'PREIS_TYP'
      Required = True
    end
    object ArtInfoTabPREIS: TFloatField
      FieldName = 'PREIS'
      Required = True
    end
    object ArtInfoTabMENGE2: TIntegerField
      FieldName = 'MENGE2'
      Required = True
    end
    object ArtInfoTabPREIS2: TFloatField
      FieldName = 'PREIS2'
      Required = True
    end
    object ArtInfoTabMENGE3: TIntegerField
      FieldName = 'MENGE3'
      Required = True
    end
    object ArtInfoTabPREIS3: TFloatField
      FieldName = 'PREIS3'
      Required = True
    end
    object ArtInfoTabMENGE4: TIntegerField
      FieldName = 'MENGE4'
      Required = True
    end
    object ArtInfoTabPREIS4: TFloatField
      FieldName = 'PREIS4'
      Required = True
    end
    object ArtInfoTabMENGE5: TIntegerField
      FieldName = 'MENGE5'
      Required = True
    end
    object ArtInfoTabPREIS5: TFloatField
      FieldName = 'PREIS5'
      Required = True
    end
    object ArtInfoTabMENGE_AKT: TFloatField
      FieldName = 'MENGE_AKT'
    end
    object ArtInfoTabRABGRP_ID: TStringField
      FieldName = 'RABGRP_ID'
      Size = 10
    end
    object ArtInfoTabMENGE_BESTELLT: TFloatField
      FieldName = 'MENGE_BESTELLT'
    end
    object ArtInfoTabMENGE_RESERVIERT: TFloatField
      FieldName = 'MENGE_RESERVIERT'
    end
    object ArtInfoTabPROVIS_PROZ: TFloatField
      FieldName = 'PROVIS_PROZ'
      Required = True
    end
    object ArtInfoTabSTEUER_CODE: TIntegerField
      FieldName = 'STEUER_CODE'
      Required = True
    end
    object ArtInfoTabERLOES_KTO: TIntegerField
      FieldName = 'ERLOES_KTO'
    end
    object ArtInfoTabAUFW_KTO: TIntegerField
      FieldName = 'AUFW_KTO'
    end
    object ArtInfoTabARTNUM: TStringField
      FieldName = 'ARTNUM'
    end
    object ArtInfoTabERSATZ_ARTNUM: TStringField
      FieldName = 'ERSATZ_ARTNUM'
    end
    object ArtInfoTabMATCHCODE: TStringField
      FieldName = 'MATCHCODE'
      Size = 255
    end
    object ArtInfoTabWARENGRUPPE: TIntegerField
      FieldName = 'WARENGRUPPE'
      Required = True
    end
    object ArtInfoTabBARCODE: TStringField
      FieldName = 'BARCODE'
    end
    object ArtInfoTabARTIKELTYP: TStringField
      FieldName = 'ARTIKELTYP'
      Required = True
      Size = 1
    end
    object ArtInfoTabKAS_NAME: TStringField
      FieldName = 'KAS_NAME'
      Size = 80
    end
    object ArtInfoTabME_EINHEIT: TStringField
      FieldName = 'ME_EINHEIT'
      Size = 10
    end
    object ArtInfoTabPR_EINHEIT: TFloatField
      FieldName = 'PR_EINHEIT'
      Required = True
    end
    object ArtInfoTabLAENGE: TStringField
      FieldName = 'LAENGE'
    end
    object ArtInfoTabGROESSE: TStringField
      FieldName = 'GROESSE'
    end
    object ArtInfoTabDIMENSION: TStringField
      FieldName = 'DIMENSION'
    end
    object ArtInfoTabGEWICHT: TFloatField
      FieldName = 'GEWICHT'
      Required = True
    end
    object ArtInfoTabKURZNAME: TStringField
      FieldName = 'KURZNAME'
      Size = 80
    end
    object ArtInfoTabLANGNAME: TMemoField
      FieldName = 'LANGNAME'
      BlobType = ftMemo
    end
    object ArtInfoTabALTTEIL_FLAG: TBooleanField
      FieldName = 'ALTTEIL_FLAG'
      Required = True
    end
    object ArtInfoTabNO_RABATT_FLAG: TBooleanField
      FieldName = 'NO_RABATT_FLAG'
      Required = True
    end
    object ArtInfoTabNO_PROVISION_FLAG: TBooleanField
      FieldName = 'NO_PROVISION_FLAG'
      Required = True
    end
    object ArtInfoTabNO_BEZEDIT_FLAG: TBooleanField
      FieldName = 'NO_BEZEDIT_FLAG'
      Required = True
    end
    object ArtInfoTabNO_VK_FLAG: TBooleanField
      FieldName = 'NO_VK_FLAG'
      Required = True
    end
    object ArtInfoTabSN_FLAG: TBooleanField
      FieldName = 'SN_FLAG'
      Required = True
    end
    object ArtInfoTabVPE: TIntegerField
      FieldName = 'VPE'
    end
    object ArtInfoTabMENGE_LIEF: TFloatField
      FieldName = 'MENGE_LIEF'
    end
    object ArtInfoTabMENGE_SOLL: TFloatField
      FieldName = 'MENGE_SOLL'
    end
    object ArtInfoTabJID: TIntegerField
      FieldName = 'JID'
    end
    object ArtInfoTabBESTNUM: TStringField
      FieldName = 'BESTNUM'
      Size = 50
    end
  end
  object ReKunTab: TZMySqlQuery
    Database = db1
    Transaction = Transact1
    CachedUpdates = False
    ShowRecordTypes = [ztModified, ztInserted, ztUnmodified]
    Options = [doHourGlass, doAutoFillDefs, doUseRowId]
    LinkOptions = [loAlwaysResync]
    Constraints = <>
    ExtraOptions = [moStoreResult]
    Macros = <>
    Sql.Strings = (
      'select * from ADRESSEN'
      'where REC_ID = :ID')
    RequestLive = True
    Left = 532
    Top = 183
    ParamData = <
      item
        DataType = ftInteger
        Name = 'ID'
        ParamType = ptInput
      end>
  end
  object DBUpdTo1_08: TJvStrHolder
    Capacity = 556
    Macros = <>
    Left = 552
    Top = 400
    InternalVer = 1
    StrData = (
      ''
      '2f2a2053544152542031312e30382e32303033202a2f'
      ''
      '414c544552205441424c45204649524d41'
      
        '20204144442042414e4b315f4942414e20564152434841522831303029202041' +
        '465445522042414e4b315f4e414d452c'
      
        '20204144442042414e4b315f5357494654205641524348415228313030292020' +
        '41465445522042414e4b315f4942414e2c'
      
        '20204144442042414e4b325f4942414e20564152434841522831303029202041' +
        '465445522042414e4b325f4e414d452c'
      
        '20204144442042414e4b325f5357494654205641524348415228313030292020' +
        '41465445522042414e4b325f4942414e2c'
      '202041444420494d4147453120424c4f422c'
      '202041444420494d4147453220424c4f422c'
      '202041444420494d4147453320424c4f423b'
      ''
      '414c544552205441424c4520554542455257454953554e47454e'
      
        '204144442041525420454e554d282255222c224c2229202044454641554c5420' +
        '225522204e4f54204e554c4c2041465445522055574e554d3b'
      ''
      
        '435245415445205441424c45204946204e4f5420455849535453204144524553' +
        '53454e5f4c4945462028'
      
        '20205245435f494420494e5445474552204e4f54204e554c4c204175746f5f49' +
        '6e6372656d656e742c'
      '2020414444525f494420494e5445474552204e4f54204e554c4c2c'
      '2020414e524544452056415243484152283430292c'
      '20204e414d4531205641524348415228343029204e4f54204e554c4c2c'
      '20204e414d45322056415243484152283430292c'
      '20204e414d45332056415243484152283430292c'
      '202041425445494c554e472056415243484152283430292c'
      '202053545241535345205641524348415228343029204e4f54204e554c4c2c'
      '20204c414e4420564152434841522835292c'
      '2020504c5a205641524348415228313029204e4f54204e554c4c2c'
      '20204f5254205641524348415228343029204e4f54204e554c4c2c'
      '2020494e464f20544558542c'
      '20205052494d415259204b455920285245435f49442c20414444525f494429'
      '293b'
      ''
      '696e7365727420696e746f20414452455353454e5f4c494546'
      
        '2073656c656374204e554c4c2c205245435f49442c204c5f414e524544452c20' +
        '4c5f4e414d45312c204c5f4e414d45322c204c5f4e414d45332c'
      
        '204c5f41425445494c2c204c5f535452415353452c204c414e442c204c5f504c' +
        '5a2c204c5f4f52542c204e554c4c'
      '2066726f6d20414452455353454e'
      
        '207768657265204c5f4e414d4531213d272720616e6420284c5f4e414d453120' +
        '213d204e414d4531206f72204c5f4e414d453220213d204e414d4532206f7220' +
        '4c5f4e414d453320213d204e414d4533'
      
        '206f72205354524153534520213d204c5f53545241535345206f72204f525420' +
        '213d4c5f4f5254206f72204c5f504c5a20213d20504c5a293b'
      ''
      ''
      '414c544552205441424c4520414452455353454e'
      
        '20204348414e4745204b554e5f4c494546415254204b554e5f4c494546415254' +
        '20494e54283131292044454641554c5420222d3122204e4f54204e554c4c2c'
      
        '20204348414e4745204b554e5f5a41484c415254204b554e5f5a41484c415254' +
        '20494e54283131292044454641554c5420222d3122204e4f54204e554c4c2c'
      
        '20204348414e4745204c4945465f4c494546415254204c4945465f4c49454641' +
        '525420494e54283131292044454641554c5420222d3122204e4f54204e554c4c' +
        '2c'
      
        '20204348414e4745204c4945465f5a41484c415254204c4945465f5a41484c41' +
        '525420494e54283131292044454641554c5420222d3122204e4f54204e554c4c' +
        '2c'
      
        '20204144442044454641554c545f4c494546414e534348524946545f49442049' +
        '4e5428313129202044454641554c5420222d3122204e4f54204e554c4c204146' +
        '5445522050465f504c5a2c'
      
        '2020414444205350524143485f494420494e5428313129202044454641554c54' +
        '20223222204e4f54204e554c4c204146544552204b554e44454e475255505045' +
        '2c'
      
        '2020414444204b544f5f494e4841424552205641524348415228343029204146' +
        '5445522042414e4b2c'
      
        '20204144442050524f5649535f50524f5a20464c4f415428352c322920444546' +
        '41554c5420223022204e4f54204e554c4c204146544552205645525452455445' +
        '525f49442c'
      '202044524f50204841545f4c494546414e53522c'
      '202044524f50204c5f414e524544452c'
      '202044524f50204c5f4e414d45312c'
      '202044524f50204c5f4e414d45322c'
      '202044524f50204c5f4e414d45332c'
      '202044524f50204c5f41425445494c2c'
      '202044524f50204c5f535452415353452c'
      '202044524f50204c5f504c5a2c'
      '202044524f50204c5f4f52542c'
      '202044524f50204c5f504f5354464143482c'
      '202044524f50204c5f50465f504c5a2c'
      '2020414444205553455246454c445f3031205641524348415228323535292c'
      '2020414444205553455246454c445f3032205641524348415228323535292c'
      '2020414444205553455246454c445f3033205641524348415228323535292c'
      '2020414444205553455246454c445f3034205641524348415228323535292c'
      '2020414444205553455246454c445f3035205641524348415228323535292c'
      '2020414444205553455246454c445f3036205641524348415228323535292c'
      '2020414444205553455246454c445f3037205641524348415228323535292c'
      '2020414444205553455246454c445f3038205641524348415228323535292c'
      '2020414444205553455246454c445f3039205641524348415228323535292c'
      '2020414444205553455246454c445f3130205641524348415228323535292c'
      
        '202041444420454d41494c322056415243484152283130302920414654455220' +
        '454d41494c2c'
      
        '2020414444204942414e20564152434841522831303029204146544552204241' +
        '4e4b2c'
      
        '2020414444205357494654205641524348415228313030292041465445522049' +
        '42414e2c'
      
        '20204348414e4745204b554e5f50524c49535445204b554e5f50524c49535445' +
        '20454e554d28224e222c225922292044454641554c5420224e22204e4f54204e' +
        '554c4c2c'
      
        '20204348414e4745204c4945465f50524c49535445204c4945465f50524c4953' +
        '544520454e554d28224e222c225922292044454641554c5420224e22204e4f54' +
        '204e554c4c3b'
      ''
      ''
      '414c544552205441424c452041504152544e4552'
      
        '2041444420454d41494c3220564152434841522831303029204e4f54204e554c' +
        '4c20414654455220454d41494c2c'
      '20414444205553455246454c445f3031205641524348415228323535292c'
      '20414444205553455246454c445f3032205641524348415228323535292c'
      '20414444205553455246454c445f3033205641524348415228323535292c'
      '20414444205553455246454c445f3034205641524348415228323535292c'
      '20414444205553455246454c445f3035205641524348415228323535292c'
      '20414444205553455246454c445f3036205641524348415228323535292c'
      '20414444205553455246454c445f3037205641524348415228323535292c'
      '20414444205553455246454c445f3038205641524348415228323535292c'
      '20414444205553455246454c445f3039205641524348415228323535292c'
      '20414444205553455246454c445f3130205641524348415228323535293b'
      ''
      ''
      '414c544552205441424c4520415254494b454c'
      '202044524f50204c494546312c'
      '202044524f50204c494546315f424e554d2c'
      '202044524f50204c494546315f50524549532c'
      '202044524f50204c494546322c'
      '202044524f50204c494546325f424e554d2c'
      '202044524f50204c494546325f50524549532c'
      '202044524f50204b4154312c'
      '202044524f50204b4154322c'
      '202044524f50204d454d4f2c'
      '202044524f502053484f505f4441544153484545545f452c'
      '202044524f502053484f505f4b4154414c4f475f452c'
      '202044524f502053484f505f48414e44425543485f452c'
      '202044524f502053484f505f50524549535f454b2c'
      '202044524f502053484f505f50524549535f535045432c'
      '202044524f502053484f505f454b5f5241424154542c'
      '202044524f502053484f505f4641454c4c545f5745472c'
      '202044524f502053484f505f424553434852454942554e472c'
      '202044524f502053484f505f5350455a4946494b4154494f4e2c'
      '202044524f502053484f505f5a554245484f45522c'
      '202044524f502053484f505f5a554245484f45525f4f50542c'
      '202044524f502053484f505f544f505f49442c'
      '202044524f502053484f505f4152545f4e522c'
      '202044524f502053484f505f4152545f4e414d452c'
      ''
      
        '20204144442048455253545f4152544e554d2056415243484152283130302920' +
        '4146544552204845525354454c4c45525f49442c'
      
        '202041444420424152434f444532205641524348415228323029202041465445' +
        '5220424152434f44452c'
      
        '202041444420424152434f444533205641524348415228323029202041465445' +
        '5220424152434f4445322c'
      ''
      
        '20204144442044454641554c545f4c4945465f494420494e5428313129202044' +
        '454641554c5420222d3122204e4f54204e554c4c204146544552204c4153545f' +
        '564b4441542c'
      
        '20204144442056504520494e5428313129202044454641554c5420223122204e' +
        '4f54204e554c4c204146544552204d455f45494e484549542c'
      
        '20204144442050524f5649535f50524f5a20464c4f415428352c322920204445' +
        '4641554c5420223022204e4f54204e554c4c20414654455220564b35422c'
      ''
      '2020414444205553455246454c445f3031205641524348415228323535292c'
      '2020414444205553455246454c445f3032205641524348415228323535292c'
      '2020414444205553455246454c445f3033205641524348415228323535292c'
      '2020414444205553455246454c445f3034205641524348415228323535292c'
      '2020414444205553455246454c445f3035205641524348415228323535292c'
      '2020414444205553455246454c445f3036205641524348415228323535292c'
      '2020414444205553455246454c445f3037205641524348415228323535292c'
      '2020414444205553455246454c445f3038205641524348415228323535292c'
      '2020414444205553455246454c445f3039205641524348415228323535292c'
      '2020414444205553455246454c445f3130205641524348415228323535292c'
      ''
      
        '20204348414e47452053484f505f4441544153484545545f442053484f505f44' +
        '4154454e424c415454205641524348415228313030292c'
      
        '20204348414e47452053484f505f48414e44425543485f442053484f505f4841' +
        '4e4442554348205641524348415228313030292c'
      
        '20204348414e47452053484f505f44524157494e472053484f505f5a45494348' +
        '4e554e47205641524348415228313030292c'
      
        '20204348414e47452053484f505f4b4154414c4f475f442053484f505f4b4154' +
        '414c4f47205641524348415228313030292c'
      ''
      
        '202041444420494e444558204944585f5747522028574152454e475255505045' +
        '292c'
      
        '202041444420494e444558204944585f4d41544348434f444520284d41544348' +
        '434f4445292c'
      
        '202041444420494e444558204944585f424152434f44452028424152434f4445' +
        '292c'
      '202041444420494e444558204944585f4152544e554d20284152544e554d292c'
      
        '202041444420494e444558204944585f48455253545f4152544e554d20284845' +
        '5253545f4152544e554d293b'
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      '414c544552205441424c4520415254494b454c5f4c54455854'
      '2044524f502046454c445f49442c'
      '2044524f50204c544558542c'
      
        '20414444204b55525a4e414d452076617263686172283830292064656661756c' +
        '74204e554c4c2c'
      '20414444204c414e474e414d4520746578742c'
      
        '20414444204b41535f4e414d452076617263686172283830292064656661756c' +
        '74204e554c4c2c'
      '204144442053484f505f4b55525a5445585420746578742c'
      '204144442053484f505f4c414e475445585420746578742c'
      
        '204144442053484f505f444154454e424c415454207661726368617228313030' +
        '292064656661756c74204e554c4c2c'
      
        '204144442053484f505f4b4154414c4f47207661726368617228313030292064' +
        '656661756c74204e554c4c2c'
      
        '204144442053484f505f5a454943484e554e4720766172636861722831303029' +
        '2064656661756c74204e554c4c2c'
      
        '204144442053484f505f48414e44425543482076617263686172283130302920' +
        '64656661756c74204e554c4c2c'
      
        '20414444204348414e47455f464c41472074696e79696e7428312920756e7369' +
        '676e6564204e4f54204e554c4c2064656661756c74202730273b'
      ''
      '414c544552205441424c45204b465a'
      
        '204348414e474520564552545245544552205645525452455445525f49442069' +
        '6e7428313129204e4f54204e554c4c2064656661756c7420272d31272c'
      '204348414e4745204e45415f4155204e41455f415520444154452c'
      '20414444204e41455f53502044415445204146544552204e41455f41552c'
      '20414444204e41455f54502044415445204146544552204e41455f53502c'
      '20'
      '20414444205553455246454c445f3031205641524348415228323535292c'
      '20414444205553455246454c445f3032205641524348415228323535292c'
      '20414444205553455246454c445f3033205641524348415228323535292c'
      '20414444205553455246454c445f3034205641524348415228323535292c'
      '20414444205553455246454c445f3035205641524348415228323535292c'
      '20414444205553455246454c445f3036205641524348415228323535292c'
      '20414444205553455246454c445f3037205641524348415228323535292c'
      '20414444205553455246454c445f3038205641524348415228323535292c'
      '20414444205553455246454c445f3039205641524348415228323535292c'
      '20414444205553455246454c445f3130205641524348415228323535293b'
      ''
      ''
      '414c544552205441424c4520415254494b454c5f4b4154'
      
        '202041444420534f52545f4e554d20494e542833292020554e5349474e454420' +
        '44454641554c5420223022204e4f54204e554c4c20414654455220544f505f49' +
        '443b'
      ''
      '414c544552205441424c4520574152454e4752555050454e'
      
        '20414444205354455545525f434f44452054494e59494e5428342920554e5349' +
        '474e45442044454641554c5420223222204e4f54204e554c4c20414654455220' +
        '4445465f414b544f2c'
      
        '2041444420564b315f46414b544f5220464c4f415428322c3529202044454641' +
        '554c5420223022204e4f54204e554c4c204146544552205354455545525f434f' +
        '44452c'
      
        '2041444420564b325f46414b544f5220464c4f415428322c3529202044454641' +
        '554c5420223022204e4f54204e554c4c20414654455220564b315f46414b544f' +
        '522c'
      
        '2041444420564b335f46414b544f5220464c4f415428322c3529202044454641' +
        '554c5420223022204e4f54204e554c4c20414654455220564b325f46414b544f' +
        '522c'
      
        '2041444420564b345f46414b544f5220464c4f415428322c3529202044454641' +
        '554c5420223022204e4f54204e554c4c20414654455220564b335f46414b544f' +
        '522c'
      
        '2041444420564b355f46414b544f5220464c4f415428322c3529202044454641' +
        '554c5420223022204e4f54204e554c4c20414654455220564b345f46414b544f' +
        '523b'
      ''
      '414c544552205441424c452057415254554e47'
      
        '2041444420534f4c4c5a4549545f4b5720464c4f415428332c31292041465445' +
        '522057564552545241475f4e522c'
      
        '2041444420534f4c4c5a4549545f475720464c4f415428332c31292041465445' +
        '5220534f4c4c5a4549545f4b572c'
      
        '2041444420454e544645524e554e4720494e542831312920554e5349474e4544' +
        '20414654455220534f4c4c5a4549545f47573b'
      ''
      
        '55504441544520414452455353454e20534554204b554e5f4c49454641525420' +
        '3d20272d3127207768657265204b554e5f4c4945464152543d303b'
      
        '55504441544520414452455353454e20534554204b554e5f5a41484c41525420' +
        '3d20272d3127207768657265204b554e5f5a41484c4152543d303b'
      
        '55504441544520414452455353454e20534554204c4945465f4c494546415254' +
        '203d20272d3127207768657265204c4945465f4c4945464152543d303b'
      
        '55504441544520414452455353454e20534554204c4945465f5a41484c415254' +
        '203d20272d3127207768657265204c4945465f5a41484c4152543d303b'
      ''
      
        '435245415445205441424c45204946204e4f5420455849535453205350524143' +
        '48454e2028'
      
        '20205350524143485f494420696e7428313129204e4f54204e554c4c20617574' +
        '6f5f696e6372656d656e742c'
      
        '20204e414d45207661726368617228333229204e4f54204e554c4c2064656661' +
        '756c742027272c'
      
        '2020434f44452063686172283229204e4f54204e554c4c2064656661756c7420' +
        '27272c'
      '2020534f525420696e742833292064656661756c74204e554c4c2c'
      
        '202044454641554c545f464c414720656e756d28274e272c27592729204e4f54' +
        '204e554c4c2064656661756c7420274e272c'
      '20205052494d415259204b45592020285350524143485f4944292c'
      '20204b4559204944585f4e414d4520284e414d4529'
      '293b'
      ''
      
        '494e5345525420494e544f20535052414348454e2056414c554553282231222c' +
        '2022456e676c697368222c2022656e222c202232222c20224e22293b'
      
        '494e5345525420494e544f20535052414348454e2056414c554553282232222c' +
        '202244657574736368222c20226465222c202231222c20225922293b'
      
        '494e5345525420494e544f20535052414348454e2056414c554553282233222c' +
        '202245737061f16f6c222c20226573222c202233222c20224e22293b'
      ''
      
        '414c544552205441424c4520504c5a2041444420494e444558204944585f4c41' +
        '4e4420284c414e44293b'
      ''
      '414c544552205441424c45204845525354454c4c45525f494e464f'
      '202044524f50204c4153545f4348414e47452c'
      '202044524f502053484f505f444154455f41444445442c'
      '202044524f502053484f505f444154455f4348414e47452c'
      '202044524f502053594e435f464c41472c'
      '202044524f50204348414e47455f464c41472c'
      '202044524f502044454c5f464c41473b'
      ''
      '414c544552205441424c45204a4f55524e414c'
      
        '20204348414e4745204c494546415254204c4945464152542054494e59494e54' +
        '283229202044454641554c5420222d3122204e4f54204e554c4c2c'
      
        '20204348414e4745205a41484c415254205a41484c4152542054494e59494e54' +
        '283229202044454641554c5420222d3122204e4f54204e554c4c2c'
      
        '20204144442050524f5649535f5745525420464c4f41542831302c3229202044' +
        '454641554c5420223022204e4f54204e554c4c2041465445522041544d53554d' +
        '4d452c'
      
        '20204144442050524f5649535f4245524543484e455420454e554d28274e272c' +
        '27592729202044454641554c5420224e22204e4f54204e554c4c204146544552' +
        '204d5753545f465245495f464c41472c'
      
        '2020414444205445524d5f494420494e542831312920554e5349474e45442044' +
        '454641554c5420223122204e4f54204e554c4c2046495253543b'
      ''
      '414c544552205441424c45204a4f55524e414c504f53'
      
        '202041444420544f505f504f535f494420494e54283131292044454641554c54' +
        '20222d3122204e4f54204e554c4c20414654455220415254494b454c5f49442c'
      
        '20204144442050524f5649535f50524f5a20464c4f415428352c322920204445' +
        '4641554c5420223022204e4f54204e554c4c20414654455220414c545445494c' +
        '5f5354434f44452c'
      
        '20204144442050524f5649535f5745525420464c4f41542831302c3229202044' +
        '454641554c5420223022204e4f54204e554c4c2041465445522050524f564953' +
        '5f50524f5a2c'
      
        '204144442056504520494e542831312920554e5349474e45442044454641554c' +
        '5420223122204e4f54204e554c4c2041465445522050525f45494e484549543b'
      ''
      '414c544552205441424c4520415254494b454c5f5052454953'
      
        '2020414444204d454e47453220494e5428362920554e5349474e454420444546' +
        '41554c5420223022204e4f54204e554c4c2041465445522050524549532c'
      
        '20204144442050524549533220464c4f41542831302c33292044454641554c54' +
        '20223022204e4f54204e554c4c204146544552204d454e4745322c'
      
        '2020414444204d454e47453320494e5428362920554e5349474e454420444546' +
        '41554c5420223022204e4f54204e554c4c204146544552205052454953322c'
      
        '20204144442050524549533320464c4f41542831302c33292044454641554c54' +
        '20223022204e4f54204e554c4c204146544552204d454e4745332c'
      
        '2020414444204d454e47453420494e5428362920554e5349474e454420444546' +
        '41554c5420223022204e4f54204e554c4c204146544552205052454953332c'
      
        '20204144442050524549533420464c4f41542831302c33292044454641554c54' +
        '20223022204e4f54204e554c4c204146544552204d454e4745342c'
      
        '2020414444204d454e47453520494e5428362920554e5349474e454420444546' +
        '41554c5420223022204e4f54204e554c4c204146544552205052454953342c'
      
        '20204144442050524549533520464c4f41542831302c33292044454641554c54' +
        '20223022204e4f54204e554c4c204146544552204d454e4745352c'
      
        '2020414444204755454c5449475f564f4e204441544520414654455220505245' +
        '4953352c'
      
        '2020414444204755454c5449475f424953204441544520414654455220475545' +
        '4c5449475f564f4e3b'
      ''
      ''
      
        '435245415445205441424c45204946204e4f5420455849535453204d49544152' +
        '4245495445522028'
      
        '20204d415f494420696e742831312920756e7369676e6564204e4f54204e554c' +
        '4c206175746f5f696e6372656d656e742c'
      
        '20204d415f4e554d4d45522076617263686172283130292064656661756c7420' +
        '4e554c4c2c'
      
        '20204e414d45207661726368617228313030292064656661756c74204e554c4c' +
        '2c'
      
        '2020564e414d45207661726368617228313030292064656661756c74204e554c' +
        '4c2c'
      
        '20204c4f47494e5f4e414d45207661726368617228353029204e4f54204e554c' +
        '4c2064656661756c742027272c'
      
        '2020414e5a454947455f4e414d45207661726368617228353029204e4f54204e' +
        '554c4c2064656661756c742027272c'
      
        '2020414e524544452076617263686172283135292064656661756c74204e554c' +
        '4c2c'
      
        '2020544954454c2076617263686172283135292064656661756c74204e554c4c' +
        '2c'
      
        '20205a555341545a2076617263686172283430292064656661756c74204e554c' +
        '4c2c'
      
        '20205a555341545a322076617263686172283430292064656661756c74204e55' +
        '4c4c2c'
      
        '20205a554841454e44454e2076617263686172283430292064656661756c7420' +
        '4e554c4c2c'
      
        '2020535452415353452076617263686172283430292064656661756c74204e55' +
        '4c4c2c'
      '20204c414e4420766172636861722835292064656661756c74204e554c4c2c'
      '2020504c5a2076617263686172283130292064656661756c74204e554c4c2c'
      '20204f52542076617263686172283430292064656661756c74204e554c4c2c'
      
        '202054454c45464f4e207661726368617228313030292064656661756c74204e' +
        '554c4c2c'
      '2020464158207661726368617228313030292064656661756c74204e554c4c2c'
      
        '202046554e4b207661726368617228313030292064656661756c74204e554c4c' +
        '2c'
      
        '2020454d41494c207661726368617228313030292064656661756c74204e554c' +
        '4c2c'
      
        '2020494e5445524e4554207661726368617228313030292064656661756c7420' +
        '4e554c4c2c'
      
        '20205350524143485f494420736d616c6c696e742836292064656661756c7420' +
        '2732272c'
      
        '20204245534348414546544947554e475341525420736d616c6c696e74283629' +
        '2064656661756c74204e554c4c2c'
      
        '20204245534348414546544947554e47534752414420736d616c6c696e742836' +
        '292064656661756c74204e554c4c2c'
      
        '20204a414852455355524c41554220666c6f61742064656661756c74204e554c' +
        '4c2c'
      
        '20204755454c5449475f564f4e206461746574696d652064656661756c74204e' +
        '554c4c2c'
      
        '20204755454c5449475f424953206461746574696d652064656661756c74204e' +
        '554c4c2c'
      
        '2020474542444154554d206461746574696d652064656661756c74204e554c4c' +
        '2c'
      
        '202047455343484c4543485420656e756d28274d272c27572729204e4f54204e' +
        '554c4c2064656661756c7420274d272c'
      
        '202046414d5354414e4420736d616c6c696e742836292064656661756c74204e' +
        '554c4c2c'
      '202042414e4b2076617263686172283430292064656661756c74204e554c4c2c'
      '2020424c5a2076617263686172283130292064656661756c74204e554c4c2c'
      '20204b544f2076617263686172283230292064656661756c74204e554c4c2c'
      '202042454d45524b554e4720746578742c'
      '20205553455246454c445f3031205641524348415228323535292c'
      '20205553455246454c445f3032205641524348415228323535292c'
      '20205553455246454c445f3033205641524348415228323535292c'
      '20205553455246454c445f3034205641524348415228323535292c'
      '20205553455246454c445f3035205641524348415228323535292c'
      '20205553455246454c445f3036205641524348415228323535292c'
      '20205553455246454c445f3037205641524348415228323535292c'
      '20205553455246454c445f3038205641524348415228323535292c'
      '20205553455246454c445f3039205641524348415228323535292c'
      '20205553455246454c445f3130205641524348415228323535292c'
      
        '202045525354454c4c54206461746574696d652064656661756c74204e554c4c' +
        '2c'
      
        '202045525354454c4c545f4e414d452076617263686172283230292064656661' +
        '756c74204e554c4c2c'
      '2020474541454e44206461746574696d652064656661756c74204e554c4c2c'
      
        '2020474541454e445f4e414d452076617263686172283230292064656661756c' +
        '74204e554c4c2c'
      '20205052494d415259204b45592020284d415f4944292c'
      
        '2020554e49515545204b4559204944585f4d415f4e554d20284d415f4e554d4d' +
        '4552292c'
      
        '2020554e49515545204b4559204944585f4c4f47494e4e414d4520284c4f4749' +
        '4e5f4e414d4529'
      '293b'
      ''
      
        '435245415445205441424c45204946204e4f54204558495354532050494d5f41' +
        '5546474142454e2028'
      
        '20205245434f5244494420696e7428313129204e4f54204e554c4c206175746f' +
        '5f696e6372656d656e742c'
      
        '20205245534f55524345494420696e7428313129204e4f54204e554c4c206465' +
        '6661756c74202730272c'
      
        '2020434f4d504c45544520656e756d28274e272c275927292064656661756c74' +
        '20274e272c'
      
        '20204445534352495054494f4e206368617228323535292064656661756c7420' +
        '4e554c4c2c'
      
        '202044455441494c53206368617228323535292064656661756c74204e554c4c' +
        '2c'
      '2020435245415445444f4e204441544554494d452c'
      '20205052494f5249545920696e74283131292064656661756c74204e554c4c2c'
      '202043415445474f525920696e74283131292064656661756c74204e554c4c2c'
      '2020434f4d504c455445444f4e204441544554494d452c'
      '202044554544415445204441544554494d452c'
      
        '2020555345524649454c4430206368617228313030292064656661756c74204e' +
        '554c4c2c'
      
        '2020555345524649454c4431206368617228313030292064656661756c74204e' +
        '554c4c2c'
      
        '2020555345524649454c4432206368617228313030292064656661756c74204e' +
        '554c4c2c'
      
        '2020555345524649454c4433206368617228313030292064656661756c74204e' +
        '554c4c2c'
      
        '2020555345524649454c4434206368617228313030292064656661756c74204e' +
        '554c4c2c'
      
        '2020555345524649454c4435206368617228313030292064656661756c74204e' +
        '554c4c2c'
      
        '2020555345524649454c4436206368617228313030292064656661756c74204e' +
        '554c4c2c'
      
        '2020555345524649454c4437206368617228313030292064656661756c74204e' +
        '554c4c2c'
      
        '2020555345524649454c4438206368617228313030292064656661756c74204e' +
        '554c4c2c'
      
        '2020555345524649454c4439206368617228313030292064656661756c74204e' +
        '554c4c2c'
      '20205052494d415259204b455920285245434f52444944292c'
      '20204b45592052657349445f6e6478285245534f555243454944292c'
      '20204b455920447565446174652844554544415445292c'
      '20204b455920436f6d706c657465644f6e28434f4d504c455445444f4e29'
      '293b'
      ''
      
        '435245415445205441424c45204946204e4f54204558495354532050494d5f4b' +
        '4f4e54414b54452028'
      
        '20205245434f5244494420696e7428313129204e4f54204e554c4c206175746f' +
        '5f696e6372656d656e742c'
      
        '20205245534f55524345494420696e7428313129204e4f54204e554c4c206465' +
        '6661756c74202730272c'
      
        '202046495253544e414d452063686172283530292064656661756c74204e554c' +
        '4c2c'
      
        '20204c4153544e414d45206368617228353029204e4f54204e554c4c20646566' +
        '61756c742027272c'
      '202042495254484441544520646174652064656661756c74204e554c4c2c'
      '2020414e4e495645525341525920646174652064656661756c74204e554c4c2c'
      '20205449544c452063686172283530292064656661756c74204e554c4c2c'
      
        '2020434f4d50414e59206368617228353029204e4f54204e554c4c2064656661' +
        '756c742027272c'
      
        '20204a4f425f504f534954494f4e2063686172283330292064656661756c7420' +
        '4e554c4c2c'
      
        '202041444452455353206368617228313030292064656661756c74204e554c4c' +
        '2c'
      '2020434954592063686172283530292064656661756c74204e554c4c2c'
      '202053544154452063686172283235292064656661756c74204e554c4c2c'
      '20205a49502063686172283130292064656661756c74204e554c4c2c'
      '2020434f554e5452592063686172283235292064656661756c74204e554c4c2c'
      '20204e4f5445206368617228323535292064656661756c74204e554c4c2c'
      '202050484f4e45312063686172283235292064656661756c74204e554c4c2c'
      '202050484f4e45322063686172283235292064656661756c74204e554c4c2c'
      '202050484f4e45332063686172283235292064656661756c74204e554c4c2c'
      '202050484f4e45342063686172283235292064656661756c74204e554c4c2c'
      '202050484f4e45352063686172283235292064656661756c74204e554c4c2c'
      
        '202050484f4e45545950453120696e74283131292064656661756c74204e554c' +
        '4c2c'
      
        '202050484f4e45545950453220696e74283131292064656661756c74204e554c' +
        '4c2c'
      
        '202050484f4e45545950453320696e74283131292064656661756c74204e554c' +
        '4c2c'
      
        '202050484f4e45545950453420696e74283131292064656661756c74204e554c' +
        '4c2c'
      
        '202050484f4e45545950453520696e74283131292064656661756c74204e554c' +
        '4c2c'
      '202043415445474f525920696e74283131292064656661756c74204e554c4c2c'
      '2020454d41494c206368617228313030292064656661756c74204e554c4c2c'
      
        '2020435553544f4d31206368617228313030292064656661756c74204e554c4c' +
        '2c'
      
        '2020435553544f4d32206368617228313030292064656661756c74204e554c4c' +
        '2c'
      
        '2020435553544f4d33206368617228313030292064656661756c74204e554c4c' +
        '2c'
      
        '2020435553544f4d34206368617228313030292064656661756c74204e554c4c' +
        '2c'
      
        '2020555345524649454c4430206368617228313030292064656661756c74204e' +
        '554c4c2c'
      
        '2020555345524649454c4431206368617228313030292064656661756c74204e' +
        '554c4c2c'
      
        '2020555345524649454c4432206368617228313030292064656661756c74204e' +
        '554c4c2c'
      
        '2020555345524649454c4433206368617228313030292064656661756c74204e' +
        '554c4c2c'
      
        '2020555345524649454c4434206368617228313030292064656661756c74204e' +
        '554c4c2c'
      
        '2020555345524649454c4435206368617228313030292064656661756c74204e' +
        '554c4c2c'
      
        '2020555345524649454c4436206368617228313030292064656661756c74204e' +
        '554c4c2c'
      
        '2020555345524649454c4437206368617228313030292064656661756c74204e' +
        '554c4c2c'
      
        '2020555345524649454c4438206368617228313030292064656661756c74204e' +
        '554c4c2c'
      
        '2020555345524649454c4439206368617228313030292064656661756c74204e' +
        '554c4c2c'
      '20205052494d415259204b455920285245434f52444944292c'
      '20204b45592052657349445f6e6478285245534f555243454944292c'
      '20204b4559204c4e616d655f6e6478284c4153544e414d45292c'
      '20204b455920436f6d70616e795f6e647828434f4d50414e5929'
      '293b'
      ''
      
        '435245415445205441424c45204946204e4f54204558495354532050494d5f54' +
        '45524d494e452028'
      
        '20205245434f5244494420696e7428313129204e4f54204e554c4c206175746f' +
        '5f696e6372656d656e742c'
      '2020535441525454494d45204441544554494d452c'
      '2020454e4454494d45204441544554494d452c'
      
        '20205245534f55524345494420696e7428313129204e4f54204e554c4c206465' +
        '6661756c74202730272c'
      
        '20204445534352495054494f4e206368617228323535292064656661756c7420' +
        '4e554c4c2c'
      '20204e4f544553206368617228323535292064656661756c74204e554c4c2c'
      '202043415445474f525920696e74283131292064656661756c74204e554c4c2c'
      
        '2020414c4c4441594556454e5420656e756d28274e272c27592729204e4f5420' +
        '4e554c4c2064656661756c7420274e272c'
      
        '202044494e4750415448206368617228323535292064656661756c74204e554c' +
        '4c2c'
      
        '2020414c41524d53455420656e756d28274e272c27592729204e4f54204e554c' +
        '4c2064656661756c7420274e272c'
      
        '2020414c41524d414456414e434520696e74283131292064656661756c74204e' +
        '554c4c2c'
      
        '2020414c41524d414456414e43455459504520696e7428313129206465666175' +
        '6c74204e554c4c2c'
      '2020534e4f4f5a4554494d45204441544554494d452c'
      
        '2020524550454154434f444520696e74283131292064656661756c74204e554c' +
        '4c2c'
      '202052455045415452414e4745454e44204441544554494d452c'
      
        '2020435553544f4d494e54455256414c20696e74283131292064656661756c74' +
        '204e554c4c2c'
      
        '2020555345524649454c4430206368617228313030292064656661756c74204e' +
        '554c4c2c'
      
        '2020555345524649454c4431206368617228313030292064656661756c74204e' +
        '554c4c2c'
      
        '2020555345524649454c4432206368617228313030292064656661756c74204e' +
        '554c4c2c'
      
        '2020555345524649454c4433206368617228313030292064656661756c74204e' +
        '554c4c2c'
      
        '2020555345524649454c4434206368617228313030292064656661756c74204e' +
        '554c4c2c'
      
        '2020555345524649454c4435206368617228313030292064656661756c74204e' +
        '554c4c2c'
      
        '2020555345524649454c4436206368617228313030292064656661756c74204e' +
        '554c4c2c'
      
        '2020555345524649454c4437206368617228313030292064656661756c74204e' +
        '554c4c2c'
      
        '2020555345524649454c4438206368617228313030292064656661756c74204e' +
        '554c4c2c'
      
        '2020555345524649454c4439206368617228313030292064656661756c74204e' +
        '554c4c2c'
      '20205052494d415259204b455920285245434f52444944292c'
      
        '20204b4559207269645f73745f6e6478285245534f5552434549442c53544152' +
        '5454494d45292c'
      '20204b45592073745f6e647828535441525454494d45292c'
      '20204b45592065745f6e647828454e4454494d45292c'
      '20204b45592052657349445f6e6478285245534f55524345494429'
      '293b'
      ''
      ''
      '414c544552205441424c4520564552545245544552'
      
        '204348414e47452056455254525f564e414d4520564e414d4520564152434841' +
        '52283330292c'
      
        '204348414e47452056455254525f4e414d45204e414d45205641524348415228' +
        '3330292c'
      
        '204144442056455254525f4e554d4d4552207661726368617228313029206465' +
        '6661756c74204e554c4c204146544552205645525452455445525f49442c'
      
        '2041444420414e524544452076617263686172283135292064656661756c7420' +
        '4e554c4c2c'
      
        '2041444420544954454c2076617263686172283135292064656661756c74204e' +
        '554c4c2c'
      
        '20414444205a555341545a2076617263686172283430292064656661756c7420' +
        '4e554c4c2c'
      
        '20414444205a555341545a322076617263686172283430292064656661756c74' +
        '204e554c4c2c'
      
        '20414444205a554841454e44454e207661726368617228343029206465666175' +
        '6c74204e554c4c2c'
      
        '2041444420535452415353452076617263686172283430292064656661756c74' +
        '204e554c4c2c'
      
        '20414444204c414e4420766172636861722835292064656661756c74204e554c' +
        '4c2c'
      
        '2041444420504c5a2076617263686172283130292064656661756c74204e554c' +
        '4c2c'
      
        '20414444204f52542076617263686172283430292064656661756c74204e554c' +
        '4c2c'
      
        '204144442054454c45464f4e207661726368617228313030292064656661756c' +
        '74204e554c4c2c'
      
        '2041444420464158207661726368617228313030292064656661756c74204e55' +
        '4c4c2c'
      
        '204144442046554e4b207661726368617228313030292064656661756c74204e' +
        '554c4c2c'
      
        '2041444420454d41494c207661726368617228313030292064656661756c7420' +
        '4e554c4c2c'
      
        '2041444420494e5445524e455420766172636861722831303029206465666175' +
        '6c74204e554c4c2c'
      
        '20414444205350524143485f494420736d616c6c696e74283629206465666175' +
        '6c74202732272c'
      
        '204144442050524f564953494f4e534152542063686172283129206465666175' +
        '6c74204e554c4c2c'
      
        '20414444204142524543484e554e47535a45495450554e4b5420656e756d2827' +
        '52272c275a27292064656661756c7420275a272c'
      
        '204144442050524f564953494f4e4d49545452414e53504f525420656e756d28' +
        '2759272c274e27292064656661756c7420274e272c'
      
        '204144442050524f564953494f4e5341545a20666c6f617428352c3229206465' +
        '6661756c74204e554c4c2c'
      
        '20414444204c45545a54454142524543484e554e472064617465206465666175' +
        '6c74204e554c4c2c'
      
        '2041444420554d5341545a20666c6f61742831322c32292064656661756c7420' +
        '4e554c4c2c'
      
        '204144442050524f564953494f4e20666c6f61742831302c3229206465666175' +
        '6c74204e554c4c2c'
      
        '20414444204245534348414546544947554e475341525420736d616c6c696e74' +
        '2836292064656661756c74204e554c4c2c'
      
        '20414444204245534348414546544947554e47534752414420736d616c6c696e' +
        '742836292064656661756c74204e554c4c2c'
      
        '20414444204755454c5449475f564f4e206461746574696d652064656661756c' +
        '74204e554c4c2c'
      
        '20414444204755454c5449475f424953206461746574696d652064656661756c' +
        '74204e554c4c2c'
      
        '2041444420474542444154554d206461746574696d652064656661756c74204e' +
        '554c4c2c'
      
        '204144442047455343484c4543485420656e756d28274d272c27572729204e4f' +
        '54204e554c4c2064656661756c7420274d272c'
      
        '204144442046414d5354414e4420736d616c6c696e742836292064656661756c' +
        '74204e554c4c2c'
      
        '204144442042414e4b2076617263686172283430292064656661756c74204e55' +
        '4c4c2c'
      
        '20414444204b544f5f494e484142455220564152434841522834302920646566' +
        '61756c74204e554c4c2c'
      
        '2041444420424c5a2076617263686172283130292064656661756c74204e554c' +
        '4c2c'
      
        '20414444204b544f2076617263686172283230292064656661756c74204e554c' +
        '4c2c'
      '204144442042454d45524b554e4720746578742c'
      '20414444205553455246454c445f3031205641524348415228323535292c'
      '20414444205553455246454c445f3032205641524348415228323535292c'
      '20414444205553455246454c445f3033205641524348415228323535292c'
      '20414444205553455246454c445f3034205641524348415228323535292c'
      '20414444205553455246454c445f3035205641524348415228323535292c'
      '20414444205553455246454c445f3036205641524348415228323535292c'
      '20414444205553455246454c445f3037205641524348415228323535292c'
      '20414444205553455246454c445f3038205641524348415228323535292c'
      '20414444205553455246454c445f3039205641524348415228323535292c'
      '20414444205553455246454c445f3130205641524348415228323535292c'
      
        '204144442045525354454c4c54206461746574696d652064656661756c74204e' +
        '554c4c2c'
      
        '204144442045525354454c4c545f4e414d452076617263686172283230292064' +
        '656661756c74204e554c4c2c'
      
        '2041444420474541454e44206461746574696d652064656661756c74204e554c' +
        '4c2c'
      
        '2041444420474541454e445f4e414d4520766172636861722832302920646566' +
        '61756c74204e554c4c2c'
      
        '2041444420554e49515545204b4559204944585f56455f4e554d202856455254' +
        '525f4e554d4d4552293b'
      ''
      
        '435245415445205441424c45204946204e4f5420455849535453204144524553' +
        '53454e5f4d45524b2028'
      
        '204d45524b4d414c5f494420696e7428313129204e4f54204e554c4c20617574' +
        '6f5f696e6372656d656e742c'
      '204e414d4520766172636861722831303029206e6f74204e554c4c2c'
      '205052494d415259204b45592020284d45524b4d414c5f494429'
      '293b'
      ''
      
        '435245415445205441424c45204946204e4f542045584953545320415254494b' +
        '454c5f4d45524b2028'
      
        '204d45524b4d414c5f494420696e7428313129204e4f54204e554c4c20617574' +
        '6f5f696e6372656d656e742c'
      '204e414d4520766172636861722831303029206e6f74204e554c4c2c'
      '205052494d415259204b45592020284d45524b4d414c5f494429'
      '293b'
      ''
      
        '435245415445205441424c45204946204e4f5420455849535453204144524553' +
        '53454e5f544f5f4d45524b2028'
      
        '204d45524b4d414c5f494420494e54202831312920554e5349474e4544204e4f' +
        '54204e554c4c2c'
      
        '20414444525f494420494e54202831312920554e5349474e4544204e4f54204e' +
        '554c4c2c'
      '205052494d415259204b4559284d45524b4d414c5f49442c414444525f494429'
      '293b'
      ''
      
        '435245415445205441424c45204946204e4f542045584953545320415254494b' +
        '454c5f544f5f4d45524b2028'
      
        '204d45524b4d414c5f494420494e54202831312920554e5349474e4544204e4f' +
        '54204e554c4c2c'
      
        '20415254494b454c5f494420494e54202831312920554e5349474e4544204e4f' +
        '54204e554c4c2c'
      
        '205052494d415259204b4559284d45524b4d414c5f49442c415254494b454c5f' +
        '494429'
      '293b'
      ''
      ''
      
        '494e5345525420494e544f2052454749535445525920284d41494e4b45592c20' +
        '4e414d452c2056414c5f494e542c2056414c5f5459502c20524541444f4e4c59' +
        '292056414c554553282253484f50222c202244454641554c545f4445424e554d' +
        '222c20302c20332c20274e27293b'
      ''
      
        '494e5345525420494e544f2052454749535445525920284d41494e4b45592c20' +
        '4e414d452c2056414c5f494e542c2056414c5f5459502c20524541444f4e4c59' +
        '292056414c5545532028274b41535345272c27424f4e445255434b4552272c31' +
        '2c332c274e27293b'
      
        '494e5345525420494e544f2052454749535445525920284d41494e4b45592c20' +
        '4e414d452c2056414c5f494e542c2056414c5f5459502c20524541444f4e4c59' +
        '292056414c5545532028274b41535345272c27445255434b4449414c4f47272c' +
        '312c332c274e27293b'
      
        '494e5345525420494e544f2052454749535445525920284d41494e4b45592c20' +
        '4e414d452c2056414c5f494e542c2056414c5f5459502c20524541444f4e4c59' +
        '292056414c5545532028274b41535345272c27534f464f5254445255434b272c' +
        '302c332c274e27293b'
      
        '494e5345525420494e544f2052454749535445525920284d41494e4b45592c20' +
        '4e414d452c2056414c5f434841522c2056414c5f5459502c20524541444f4e4c' +
        '59292056414c5545532028274b41535345272c2744454641554c545f464f524d' +
        '554c4152272c27272c312c274e27293b'
      
        '494e5345525420494e544f2052454749535445525920284d41494e4b45592c20' +
        '4e414d452c2056414c5f434841522c2056414c5f5459502c20524541444f4e4c' +
        '59292056414c5545532028274b41535345272c2744454641554c545f44525543' +
        '4b4552272c2744656661756c74272c312c274e27293b'
      ''
      ''
      ''
      '75706461746520524547495354455259'
      '205345542056414c5f494e5432203d2056414c5f43555252'
      '205748455245204d41494e4b45593d274d41494e5c5c4e554d42455253273b'
      ''
      '75706461746520524547495354455259'
      
        '205345542056414c5f494e5432203d2056414c5f444f55424c452c2056414c5f' +
        '444f55424c45203d2056414c5f43555252'
      '205748455245204d41494e4b45593d274d41494e5c5c5a41484c415254273b'
      ''
      '414c544552205441424c4520524547495354455259'
      '2044524f502056414c5f435552522c'
      
        '204348414e47452056414c5f494e54322056414c5f494e543220424947494e54' +
        '2c'
      
        '204348414e47452056414c5f494e54332056414c5f494e543320424947494e54' +
        '3b'
      ''
      
        '75706461746520524547495354455259205345542056414c5f494e54333d4c45' +
        '4e4754482856414c5f43484152292c2056414c5f5459503d33'
      '207768657265204d41494e4b45593d274d41494e5c5c4e554d42455253273b'
      ''
      
        '75706461746520415254494b454c20534554205650453d302057484552452041' +
        '5254494b454c5459503d274c27206f7220415254494b454c5459503d2754273b'
      ''
      '2f2a205570646174652056657273696f6e736e756d6d6572202a2f'
      
        '44454c4554452046524f4d20524547495354455259207768657265204d41494e' +
        '4b45593d224d41494e2220616e64204e414d453d2244425f56455253494f4e22' +
        '3b'
      
        '494e5345525420494e544f2052454749535445525920284d41494e4b45592c20' +
        '4e414d452c2056414c5f434841522c2056414c5f545950292056616c75657320' +
        '28274d41494e272c202744425f56455253494f4e272c2027312e3038272c2027' +
        '3127293b')
  end
  object DBUpDTo1_09: TJvStrHolder
    Capacity = 60
    Macros = <>
    Left = 552
    Top = 456
    InternalVer = 1
    StrData = (
      ''
      
        '2f2a2053544152542031352e31302e32303033204a502044422d557064617465' +
        '206175662056657273696f6e20312e3039202a2f'
      ''
      
        '414c544552205441424c4520424c5a20414444204c414e442056415243484152' +
        '283529202044454641554c542022444522204e4f54204e554c4c204649525354' +
        '3b'
      
        '414c544552205441424c4520424c5a2044524f50205052494d415259204b4559' +
        '2c20414444205052494d415259204b455920284c414e442c424c5a293b'
      
        '414c544552205441424c4520424c5a2044524f5020494e444558204944585f4e' +
        '414d452c2041444420494e444558204944585f4e414d4520284c414e442c4241' +
        '4e4b5f4e414d452c424c5a293b'
      ''
      '414c544552205441424c4520414452455353454e'
      
        '204144442053484f505f4348414e47455f464c41472054494e59494e54283129' +
        '2020554e5349474e45442044454641554c5420223022204e4f54204e554c4c20' +
        '41465445522053484f505f4b554e44455f49442c'
      
        '204144442053484f505f44454c5f464c414720454e554d28224e222c22592229' +
        '202044454641554c5420224e22204e4f54204e554c4c2041465445522053484f' +
        '505f4348414e47455f464c41473b'
      ''
      '414c544552205441424c4520415254494b454c'
      
        '204144442053484f505f494d4147455f4d454420564152434841522831303029' +
        '202041465445522053484f505f494d4147455f4249473b'
      ''
      '414c544552205441424c4520415254494b454c5f53545545434b4c495354'
      
        '2041444420415254494b454c5f41525420454e554d282253544c222c22534554' +
        '222c225a5542222c224552532229202044454641554c54202253544c22204e4f' +
        '54204e554c4c204146544552204152545f49443b'
      ''
      '2f2a2032382e31302e32303033202a2f'
      '414c544552205441424c4520414452455353454e'
      
        '204144442053484f505f50415353574f52442056415243484152283230292020' +
        '41465445522053484f505f44454c5f464c41473b'
      ''
      
        '75706461746520414452455353454e20534554204c414e443d27444527207768' +
        '657265205550504552284c414e44293d2744273b'
      
        '75706461746520414452455353454e205345542054454c45313d434f4e434154' +
        '5f57532827272c565741484c2c54454c4531293b'
      
        '75706461746520414452455353454e205345542054454c45323d434f4e434154' +
        '5f57532827272c565741484c2c54454c4532293b'
      
        '75706461746520414452455353454e20534554204641583d434f4e4341545f57' +
        '532827272c565741484c2c464158293b'
      '414c544552205441424c4520414452455353454e2044524f5020565741484c3b'
      ''
      
        '494e5345525420494e544f2052454749535445525920284d41494e4b45592c20' +
        '4e414d452c2056414c5f494e542c2056414c5f54595029'
      
        '202056616c7565732028274d41494e5c5c50494d272c2027474c4f425f555345' +
        '524944272c202d312c20273327293b'
      ''
      '414c544552205441424c4520415254494b454c'
      
        '20414444204b4f4d4d4953494f4e5f464c414720454e554d28274e272c275927' +
        '29202044454641554c5420224e22204146544552204155544f44454c5f464c41' +
        '472c'
      
        '20414444204d415852414241545420464c4f415428332c322920204445464155' +
        '4c5420223022204e4f54204e554c4c20414654455220564b35422c'
      
        '204348414e47452053484f505f494d4147455f4249472053484f505f494d4147' +
        '45205641524348415228313030292c'
      
        '204348414e47452053484f505f494d4147455f534d414c4c2053484f505f494d' +
        '4147455f4c41524745205641524348415228313030292c'
      
        '204348414e47452047455749434854204745574943485420464c4f4154283130' +
        '2c3329202044454641554c542022302e30303022204e4f54204e554c4c3b'
      ''
      '414c544552205441424c45204a4f55524e414c'
      
        '20414444204745574943485420464c4f41542831302c3329202044454641554c' +
        '5420223022204e4f54204e554c4c204146544552205a41484c4152542c'
      '2044524f502057564f524c4147452c'
      '204348414e47452057565f444154554d205445524d494e20444154452c'
      
        '2041444420524f48474557494e4e20464c4f41542831302c3229202044454641' +
        '554c5420223022204e4f54204e554c4c20414654455220574152452c'
      
        '204348414e4745204652454947414245325f464c4147205052494e545f464c41' +
        '4720454e554d28274e272c275927292044454641554c5420224e22204e4f5420' +
        '4e554c4c3b'
      ''
      '414c544552205441424c45204a4f55524e414c504f53'
      
        '20414444204e4f5f5241424154545f464c414720454e554d28274e272c275927' +
        '292044454641554c5420224e22204e4f54204e554c4c20414654455220425255' +
        '54544f5f464c41472c'
      
        '2041444420454b5f505245495320464c4f41542831302c33292044454641554c' +
        '5420223022204e4f54204e554c4c204146544552205650452c'
      
        '204144442043414c435f46414b544f5220464c4f415428322c35292044454641' +
        '554c5420223022204e4f54204e554c4c20414654455220454b5f50524549533b'
      ''
      
        '202f2a205374616469756d2066fc7220416e6765626f7465207a7572fc636b73' +
        '65747a656e202a2f'
      
        '555044415445204a4f55524e414c20534554205354414449554d3d3020574845' +
        '5245205155454c4c453d313b'
      ''
      
        '202f2a205374616469756d2066fc72204544492d42656c656765207a7572fc63' +
        '6b7365747a656e202a2f'
      
        '555044415445204a4f55524e414c20534554205354414449554d3d3020574845' +
        '5245205155454c4c45203e3d31303b'
      ''
      '414c544552205441424c45204a4f55524e414c2044524f50204a4148523b'
      
        '414c544552205441424c45204a4f55524e414c504f532044524f50204a414852' +
        '3b'
      ''
      '2f2a205570646174652056657273696f6e736e756d6d6572202a2f'
      
        '44454c4554452046524f4d20524547495354455259207768657265204d41494e' +
        '4b45593d224d41494e2220616e64204e414d453d2244425f56455253494f4e22' +
        '3b'
      
        '494e5345525420494e544f2052454749535445525920284d41494e4b45592c20' +
        '4e414d452c2056414c5f434841522c2056414c5f545950292056616c75657320' +
        '28274d41494e272c202744425f56455253494f4e272c2027312e3039272c2027' +
        '3127293b')
  end
  object Cipher: TJvVigenereCipher
    Key = '9487jsd987aHGjg76'
    Encoded = '���'
    Decoded = 'Jan'
    Left = 256
    Top = 232
  end
  object CaoSecurity: tCaoSecurity
    Left = 320
    Top = 328
  end
end
