object SerNumSuchForm: TSerNumSuchForm
  Left = 318
  Top = 244
  BorderStyle = bsDialog
  Caption = 'Seriennummern suchen'
  ClientHeight = 279
  ClientWidth = 567
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 15
    Width = 137
    Height = 13
    Caption = 'zu suchende Seriennummer :'
  end
  object NotFoundPan: TPanel
    Left = 8
    Top = 40
    Width = 433
    Height = 233
    BevelInner = bvLowered
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 5
    object NotFoundLab: TLabel
      Left = 88
      Top = 80
      Width = 270
      Height = 105
      Alignment = taCenter
      AutoSize = False
      Caption = 'Zu Ihrem Suchbegriff '#13#10'wurden keine Seiennummern gefunden.'
      Layout = tlCenter
      WordWrap = True
    end
  end
  object SNEdi: TEdit
    Left = 152
    Top = 12
    Width = 289
    Height = 21
    TabOrder = 0
    OnChange = SNEdiChange
    OnKeyPress = SNEdiKeyPress
  end
  object PC1: TPageControl
    Left = 8
    Top = 40
    Width = 433
    Height = 233
    ActivePage = SerNumTS
    TabOrder = 4
    Visible = False
    object ArtikelTS: TTabSheet
      Caption = 'Artikel'
      object CaoDBGrid1: TCaoDBGrid
        Left = 0
        Top = 0
        Width = 425
        Height = 205
        Align = alClient
        DataSource = SNSucheArtDS
        DefaultDrawing = False
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgConfirmDelete, dgCancelOnExit]
        ReadOnly = True
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnDblClick = CaoDBGrid1DblClick
        RowColor1 = clWindow
        RowColor2 = 14869218
        DefaultRowHeight = 17
        EditColor = clWindow
        Columns = <
          item
            Expanded = False
            FieldName = 'MATCHCODE'
            Title.Caption = 'Artikel-Suchbegr.'
            Width = 90
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ARTNUM'
            Title.Caption = 'Artikelnummer'
            Width = 90
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'KURZNAME'
            Title.Caption = 'Artikel-Bezeichnung'
            Width = 208
            Visible = True
          end>
      end
    end
    object SerNumTS: TTabSheet
      Caption = 'Seriennummern'
      ImageIndex = 1
      object Label2: TLabel
        Left = 8
        Top = 8
        Width = 84
        Height = 13
        Caption = 'Artikel-Suchbegr.:'
      end
      object DBText1: TDBText
        Left = 104
        Top = 8
        Width = 129
        Height = 17
        DataField = 'MATCHCODE'
        DataSource = SNSucheArtDS
      end
      object DBText2: TDBText
        Left = 104
        Top = 29
        Width = 313
        Height = 27
        DataField = 'KURZNAME'
        DataSource = SNSucheArtDS
        WordWrap = True
      end
      object DBText3: TDBText
        Left = 320
        Top = 8
        Width = 105
        Height = 17
        DataField = 'ARTNUM'
        DataSource = SNSucheArtDS
      end
      object Label3: TLabel
        Left = 248
        Top = 8
        Width = 69
        Height = 13
        Caption = 'Artikelnummer:'
      end
      object Label4: TLabel
        Left = 8
        Top = 28
        Width = 56
        Height = 13
        Caption = 'Artikel-Bez.:'
      end
      object CaoDBGrid2: TCaoDBGrid
        Left = 0
        Top = 48
        Width = 425
        Height = 157
        DataSource = SNSucheDS
        DefaultDrawing = False
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgConfirmDelete, dgCancelOnExit]
        ReadOnly = True
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnDblClick = CaoDBGrid2DblClick
        RowColor1 = clWindow
        RowColor2 = 14869218
        DefaultRowHeight = 17
        EditColor = clWindow
        Columns = <
          item
            Expanded = False
            FieldName = 'SERNUMMER'
            Title.Caption = 'Seriennummer(n)'
            Width = 390
            Visible = True
          end>
      end
    end
    object SerNumDetailsTS: TTabSheet
      Caption = 'Details'
      ImageIndex = 2
      object Label5: TLabel
        Left = 8
        Top = 8
        Width = 84
        Height = 13
        Caption = 'Artikel-Suchbegr.:'
      end
      object Label6: TLabel
        Left = 8
        Top = 28
        Width = 56
        Height = 13
        Caption = 'Artikel-Bez.:'
      end
      object DBText4: TDBText
        Left = 104
        Top = 29
        Width = 313
        Height = 27
        DataField = 'KURZNAME'
        DataSource = SNSucheArtDS
        WordWrap = True
      end
      object DBText5: TDBText
        Left = 104
        Top = 8
        Width = 129
        Height = 17
        DataField = 'MATCHCODE'
        DataSource = SNSucheArtDS
      end
      object Label7: TLabel
        Left = 248
        Top = 8
        Width = 69
        Height = 13
        Caption = 'Artikelnummer:'
      end
      object DBText6: TDBText
        Left = 320
        Top = 8
        Width = 97
        Height = 17
        DataField = 'ARTNUM'
        DataSource = SNSucheArtDS
      end
      object Bevel1: TBevel
        Left = 8
        Top = 59
        Width = 409
        Height = 3
        Shape = bsTopLine
      end
      object Label8: TLabel
        Left = 8
        Top = 68
        Width = 76
        Height = 13
        Caption = 'Seriennnummer:'
      end
      object DBText7: TDBText
        Left = 104
        Top = 68
        Width = 313
        Height = 17
        DataField = 'SERNUMMER'
        DataSource = SNSucheDS
      end
      object Bevel2: TBevel
        Left = 8
        Top = 90
        Width = 409
        Height = 3
        Shape = bsTopLine
      end
      object Label9: TLabel
        Left = 8
        Top = 132
        Width = 39
        Height = 13
        Caption = 'Einkauf:'
      end
      object Label10: TLabel
        Left = 8
        Top = 156
        Width = 60
        Height = 13
        Caption = 'Lieferschein:'
      end
      object Label11: TLabel
        Left = 8
        Top = 180
        Width = 53
        Height = 13
        Caption = 'Rechnung:'
      end
      object Label12: TLabel
        Left = 104
        Top = 108
        Width = 44
        Height = 13
        Caption = 'Beleg-Nr.'
      end
      object Label13: TLabel
        Left = 168
        Top = 108
        Width = 31
        Height = 13
        Caption = 'Datum'
      end
      object Label14: TLabel
        Left = 224
        Top = 108
        Width = 83
        Height = 13
        Caption = 'Lieferant / Kunde'
      end
      object DBText8: TDBText
        Left = 104
        Top = 132
        Width = 49
        Height = 17
        DataField = 'EINK_NUM'
        DataSource = SNSucheDS
      end
      object DBText9: TDBText
        Left = 160
        Top = 132
        Width = 57
        Height = 17
        Alignment = taCenter
        DataField = 'EK_DATUM'
        DataSource = SNSucheDS
      end
      object DBText10: TDBText
        Left = 224
        Top = 132
        Width = 193
        Height = 17
        DataField = 'LIEFERANT'
        DataSource = SNSucheDS
      end
      object DBText11: TDBText
        Left = 104
        Top = 156
        Width = 49
        Height = 17
        DataField = 'LIEF_NUM'
        DataSource = SNSucheDS
      end
      object DBText12: TDBText
        Left = 160
        Top = 156
        Width = 57
        Height = 17
        Alignment = taCenter
        DataField = 'LS_DATUM'
        DataSource = SNSucheDS
      end
      object DBText13: TDBText
        Left = 224
        Top = 156
        Width = 193
        Height = 17
        DataField = 'LS_KUNDE'
        DataSource = SNSucheDS
      end
      object DBText14: TDBText
        Left = 104
        Top = 180
        Width = 49
        Height = 17
        DataField = 'VERK_NUM'
        DataSource = SNSucheDS
      end
      object DBText15: TDBText
        Left = 160
        Top = 180
        Width = 57
        Height = 17
        Alignment = taCenter
        DataField = 'VK_DATUM'
        DataSource = SNSucheDS
      end
      object DBText16: TDBText
        Left = 224
        Top = 180
        Width = 193
        Height = 17
        DataField = 'VK_KUNDE'
        DataSource = SNSucheDS
      end
    end
  end
  object CloseBtn: TBitBtn
    Left = 448
    Top = 248
    Width = 113
    Height = 25
    Cancel = True
    Caption = 'S&chlie�en'
    ModalResult = 1
    TabOrder = 2
    Glyph.Data = {
      DE010000424DDE01000000000000760000002800000024000000120000000100
      0400000000006801000000000000000000001000000000000000000000000000
      80000080000000808000800000008000800080800000C0C0C000808080000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00388888888877
      F7F787F8888888888333333F00004444400888FFF444448888888888F333FF8F
      000033334D5007FFF4333388888888883338888F0000333345D50FFFF4333333
      338F888F3338F33F000033334D5D0FFFF43333333388788F3338F33F00003333
      45D50FEFE4333333338F878F3338F33F000033334D5D0FFFF43333333388788F
      3338F33F0000333345D50FEFE4333333338F878F3338F33F000033334D5D0FFF
      F43333333388788F3338F33F0000333345D50FEFE4333333338F878F3338F33F
      000033334D5D0EFEF43333333388788F3338F33F0000333345D50FEFE4333333
      338F878F3338F33F000033334D5D0EFEF43333333388788F3338F33F00003333
      4444444444333333338F8F8FFFF8F33F00003333333333333333333333888888
      8888333F00003333330000003333333333333FFFFFF3333F00003333330AAAA0
      333333333333888888F3333F00003333330000003333333333338FFFF8F3333F
      0000}
    NumGlyphs = 2
  end
  object HelpBtn: TBitBtn
    Left = 448
    Top = 208
    Width = 113
    Height = 25
    Enabled = False
    TabOrder = 3
    Kind = bkHelp
  end
  object SuchBtn: TBitBtn
    Left = 448
    Top = 9
    Width = 113
    Height = 25
    Caption = '&Suchen'
    Default = True
    Enabled = False
    TabOrder = 1
    OnClick = SuchBtnClick
    Glyph.Data = {
      4E010000424D4E01000000000000760000002800000012000000120000000100
      040000000000D800000000000000000000001000000010000000000000000000
      BF0000BF000000BFBF00BF000000BF00BF00BFBF0000C0C0C000808080000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
      7777770000007777777777777777770000007777770007777700070000007777
      770F0777770F0700000077777700087778000700000077777700000000000700
      00007777770F0000F000070000007777770F0000F00007000000780000000000
      00000700000070FBFB00000800007700000070BFBFBF00B070077700000070FB
      FBFB00F070077700000070BFBFBF00B070077700000070FBFBFBFBF077777700
      000070BFBFBFBFB077777700000070FBF0000008777777000000770007777777
      777777000000777777777777777777000000}
  end
  object SNSucheArtTab: TZMySqlQuery
    Database = DM1.db1
    Transaction = DM1.Transact1
    CachedUpdates = False
    ShowRecordTypes = [ztModified, ztInserted, ztUnmodified]
    Options = [doHourGlass, doAutoFillDefs, doUseRowId]
    LinkOptions = [loAlwaysResync]
    Constraints = <>
    AfterOpen = SNSucheArtTabAfterScroll
    AfterScroll = SNSucheArtTabAfterScroll
    ExtraOptions = [moStoreResult]
    Macros = <>
    Sql.Strings = (
      'select A.REC_ID, A.MATCHCODE, A.ARTNUM, A.KURZNAME'
      'from ARTIKEL A, ARTIKEL_SERNUM SN'
      'where SN.ARTIKEL_ID=A.REC_ID and SN.SERNUMMER like :SN'
      'group by A.REC_ID')
    RequestLive = False
    Left = 464
    Top = 88
    ParamData = <
      item
        DataType = ftString
        Name = 'SN'
        ParamType = ptInput
        Value = '%12%'
      end>
    object SNSucheArtTabREC_ID: TIntegerField
      FieldName = 'REC_ID'
    end
    object SNSucheArtTabMATCHCODE: TStringField
      FieldName = 'MATCHCODE'
    end
    object SNSucheArtTabARTNUM: TStringField
      FieldName = 'ARTNUM'
    end
    object SNSucheArtTabKURZNAME: TStringField
      FieldName = 'KURZNAME'
      Size = 80
    end
  end
  object SNSucheArtDS: TDataSource
    DataSet = SNSucheArtTab
    Left = 528
    Top = 88
  end
  object SNSucheDS: TDataSource
    DataSet = SNSucheTab
    Left = 528
    Top = 144
  end
  object SNSucheTab: TZMySqlQuery
    Database = DM1.db1
    Transaction = DM1.Transact1
    CachedUpdates = False
    ShowRecordTypes = [ztModified, ztInserted, ztUnmodified]
    Options = [doHourGlass, doAutoFillDefs, doUseRowId]
    LinkOptions = [loAlwaysResync]
    Constraints = <>
    ExtraOptions = [moStoreResult]
    Macros = <>
    Sql.Strings = (
      'select SN.*, '
      'J1.RDATUM as EK_DATUM, J1.KUN_NAME1 as LIEFERANT,'
      'J2.LDATUM as LS_DATUM, J2.KUN_NAME1 as LS_KUNDE,'
      'J3.RDATUM as VK_DATUM, J3.KUN_NAME1 as VK_KUNDE'
      'from ARTIKEL A, ARTIKEL_SERNUM SN'
      'left outer JOIN JOURNAL J1 on J1.REC_ID=SN.EK_JOURNAL_ID'
      'left outer JOIN JOURNAL J2 on J2.REC_ID=SN.LS_JOURNAL_ID'
      'left outer JOIN JOURNAL J3 on J3.REC_ID=SN.VK_JOURNAL_ID'
      'where SN.ARTIKEL_ID=A.REC_ID and SN.SERNUMMER like :SN'
      'and A.REC_ID=:AID')
    RequestLive = False
    Left = 464
    Top = 144
    ParamData = <
      item
        DataType = ftString
        Name = 'SN'
        ParamType = ptInput
        Value = '%12%'
      end
      item
        DataType = ftInteger
        Name = 'AID'
        ParamType = ptInput
        Value = '6788'
      end>
    object SNSucheTabSERNUMMER: TStringField
      FieldName = 'SERNUMMER'
      Size = 255
    end
    object SNSucheTabEINK_NUM: TIntegerField
      FieldName = 'EINK_NUM'
      DisplayFormat = '0;-;-'
    end
    object SNSucheTabLIEF_NUM: TIntegerField
      FieldName = 'LIEF_NUM'
      DisplayFormat = '0;-;-'
    end
    object SNSucheTabVERK_NUM: TIntegerField
      FieldName = 'VERK_NUM'
      DisplayFormat = '0;-;-'
    end
    object SNSucheTabSNUM_ID: TIntegerField
      FieldName = 'SNUM_ID'
    end
    object SNSucheTabEK_JOURNAL_ID: TIntegerField
      FieldName = 'EK_JOURNAL_ID'
    end
    object SNSucheTabVK_JOURNAL_ID: TIntegerField
      FieldName = 'VK_JOURNAL_ID'
    end
    object SNSucheTabLS_JOURNAL_ID: TIntegerField
      FieldName = 'LS_JOURNAL_ID'
    end
    object SNSucheTabEK_JOURNALPOS_ID: TIntegerField
      FieldName = 'EK_JOURNALPOS_ID'
    end
    object SNSucheTabVK_JOURNALPOS_ID: TIntegerField
      FieldName = 'VK_JOURNALPOS_ID'
    end
    object SNSucheTabLS_JOURNALPOS_ID: TIntegerField
      FieldName = 'LS_JOURNALPOS_ID'
    end
    object SNSucheTabEK_DATUM: TDateField
      FieldName = 'EK_DATUM'
      DisplayFormat = 'dd.mm.yyyy'
    end
    object SNSucheTabLIEFERANT: TStringField
      FieldName = 'LIEFERANT'
      Size = 40
    end
    object SNSucheTabLS_DATUM: TDateField
      FieldName = 'LS_DATUM'
      DisplayFormat = 'dd.mm.yyyy'
    end
    object SNSucheTabLS_KUNDE: TStringField
      FieldName = 'LS_KUNDE'
      Size = 40
    end
    object SNSucheTabVK_DATUM: TDateField
      FieldName = 'VK_DATUM'
      DisplayFormat = 'dd.mm.yyyy'
    end
    object SNSucheTabVK_KUNDE: TStringField
      FieldName = 'VK_KUNDE'
      Size = 40
    end
  end
end
