{
Wartungsdaten f�r CAO-Faktura / Windows Version 1.0
Copyright (C) 2003 Daniel Pust / daniel@pust.de

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.


******* WaDa for CAO-Faktura comes with ABSOLUTELY NO WARRANTY ***************


Programm     : CAO-Faktura
Modul        : CAO_Wartung_Druck
Stand        : 29.07.2003
Version      : 1.0.0.0
Beschreibung : Druckmodul / Wartungsdatenverwaltung f�r CAO-Faktura

History :

29.07.2003 - Version 1.0.0.0 released Daniel Pust
30.07.2003 - JP Pipelines werden jetzt erst vor dem Designen Visible geschaltet
}
unit CAO_Wartung_Druck;

{$I CAO32.INC}

interface

uses
  Windows, Messages, SysUtils,
  {$IFDEF COMPILER_D6_UP}Variants,{$ENDIF}
  ppDB, ppDBPipe, ppCtrls, ppVar, myChkBox, ppBands, ppMemo,
  ppStrtch, ppRegion, ppPrnabl, ppClass, ppCache, ppRelatv, ppProd, ppReport,
  ppComm, ppEndUsr, ppDevice, ppPrnDev, ppViewr, ppTypes, ppPrintr, ppSubRpt,
  Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, DB, ZQuery, ZMySqlQuery, jpeg, Mask, JvMaskEdit,
  JvSpin, JvLookup;


type
  TFWartung_Druck = class(TForm)
    ppDesigner1: TppDesigner;
    ppReport1: TppReport;
    ppRepPipeline: TppDBPipeline;
    DS_ReportQuery: TDataSource;
    Wartungsliste: TppDBPipeline;
    Anlagen: TppDBPipeline;
    Ansprechpartner: TppDBPipeline;
    Kundendaten: TppDBPipeline;
    Label1: TLabel;
    Label2: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    FormularCB: TJvDBLookupCombo;
    ZielCB: TComboBox;
    LayoutBtn: TBitBtn;
    BinNamCB: TComboBox;
    AnzCopy: TJvSpinEdit;
    VorschauBtn: TBitBtn;
    PrintBtn: TBitBtn;
    DruEinrBtn: TBitBtn;
    CloseBtn: TBitBtn;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    ppTitleBand2: TppTitleBand;
    ppDetailBand2: TppDetailBand;
    ppFooterBand2: TppFooterBand;
    ReportQuery: TZMySqlQuery;
    procedure FormularCBChange(Sender: TObject);
    procedure DruEinrBtnClick(Sender: TObject);
    procedure CloseBtnClick(Sender: TObject);
    procedure LayoutBtnClick(Sender: TObject);
    procedure PrintBtnClick(Sender: TObject);
    procedure VorschauBtnClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure ZielCBChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormHide(Sender: TObject);
    procedure ReportQueryBeforePost(DataSet: TDataSet);
  private
    { Private-Deklarationen }
  public
    { Public-Deklarationen }
  end;

//var FWartung_Druck: TFWartung_Druck;

implementation

uses CAO_Wartung, CAO_DM;

{$R *.dfm}

//******************************************************************************
procedure TFWartung_Druck.FormCreate(Sender: TObject);
begin
  Height := 187;
  Wartungsliste.Visible   :=False;
  Anlagen.Visible         :=False;
  Ansprechpartner.Visible :=False;
  Kundendaten.Visible     :=False;
end;
//******************************************************************************
procedure TFWartung_Druck.FormShow(Sender: TObject);
var
  i: integer;
begin
  ReportQuery.Active := true;
  if not WartungsForm.Ansprechpartner.Active then WartungsForm.Ansprechpartner.Open;
  if not WartungsForm.Kundenliste.Active then WartungsForm.Kundenliste.Open;
  if not WartungsForm.Anlagen.Active then WartungsForm.Anlagen.Open;
  WartungsForm.Ansprechpartner.Refresh;
  WartungsForm.BitBtn2Click(@Self);
  WartungsForm.KundenListe.Refresh;
  WartungsForm.Anlagen.Refresh;

  ZielCB.Items.Assign(ppReport1.PrinterSetup.PrinterNames);
  I := ZielCB.Items.IndexOf('Screen');
  if I >= 0 then ZielCB.Items.Delete(i);
  if ZielCB.Items.Count >= 0 then
    for i := 0 to ZielCB.Items.Count - 1 do
      if ZielCB.Items[i] = ppReport1.PrinterSetup.PrinterName then
        ZielCB.ItemIndex := I;
  if ZielCB.ItemIndex = -1 then
    if ZielCB.Items.IndexOf('Default') > -1 then
      ZielCB.ItemIndex := ZielCB.Items.IndexOf('Default');
  ZielCBChange(Self);
  PrintBtn.SetFocus;
end;
//******************************************************************************
procedure TFWartung_Druck.FormHide(Sender: TObject);
begin
  ReportQuery.Active := false;
end;
//******************************************************************************
procedure TFWartung_Druck.FormularCBChange(Sender: TObject);
begin
  with ppReport1 do
  begin
    Template.DatabaseSettings.Name := FormularCB.DisplayValue;
    try
    Template.LoadFromDatabase; except
    end;
    PrinterSetup.Copies := 1;
    Language := lgGerman;
    AllowPrintToFile := True;
  end;
end;
//******************************************************************************
procedure TFWartung_Druck.DruEinrBtnClick(Sender: TObject);
var
  myPrinter: TppPrinter;
begin
  myPrinter := TppPrinter.Create;
  try
    myPrinter.PrinterSetup := ppReport1.PrinterSetup;
    if myPrinter.ShowSetupDialog then
      ppReport1.PrinterSetup := myPrinter.PrinterSetup;
    if ZielCB.Items.IndexOf(ppReport1.PrinterSetup.PrinterName) > 0 then
      ZielCB.ItemIndex := ZielCB.Items.IndexOf(ppReport1.Printer.PrinterName);
    ZielCBChange(Sender);
  finally
    myPrinter.Free;
  end;
end;
//******************************************************************************
procedure TFWartung_Druck.CloseBtnClick(Sender: TObject);
begin
  Close;
end;
//******************************************************************************
procedure TFWartung_Druck.LayoutBtnClick(Sender: TObject);
begin
  Wartungsliste.Visible   :=True;
  Anlagen.Visible         :=True;
  Ansprechpartner.Visible :=True;
  Kundendaten.Visible     :=True;
  try
    ppDesigner1.ShowModal;
    if ppReport1.Template.DatabaseSettings.Name <> FormularCB.DisplayValue then
      FormularCB.DisplayValue := ppReport1.Template.DatabaseSettings.Name;
  finally
    Wartungsliste.Visible   :=False;
    Anlagen.Visible         :=False;
    Ansprechpartner.Visible :=False;
    Kundendaten.Visible     :=False;
  end;
end;
//******************************************************************************
procedure TFWartung_Druck.PrintBtnClick(Sender: TObject);
begin
  with ppReport1 do
  begin
    PrinterSetup.DocumentName := 'Druck...';
    DeviceType := dtPrinter;
    ShowPrintDialog := False;
    PrinterSetup.Copies := round(AnzCopy.Value);
    Print;
  end;
end;
//******************************************************************************
procedure TFWartung_Druck.VorschauBtnClick(Sender: TObject);
begin
  with ppReport1 do
  begin
    DeviceType := dtScreen;
    Print;
    DeviceType := dtPrinter;
  end;
end;
//******************************************************************************
procedure TFWartung_Druck.ZielCBChange(Sender: TObject);
begin
  ppReport1.PrinterSetup.PrinterName := ZielCB.Items[ZielCB.ItemIndex];
  Application.ProcessMessages;
  BinNamCB.Items.Assign(ppReport1.PrinterSetup.BinNames);
  if BinNamCB.Items.IndexOf(ppReport1.PrinterSetup.BinName) > -1 then
    BinNamCB.ItemIndex :=
      BinNamCB.Items.IndexOf(ppReport1.PrinterSetup.BinName);
  if BinNamCB.ItemIndex = -1 then
    BinNamCB.ItemIndex := BinNamCB.Items.IndexOf('Default');
  AnzCopy.Value := ppReport1.PrinterSetup.Copies;
end;
//******************************************************************************
procedure TFWartung_Druck.ReportQueryBeforePost(DataSet: TDataSet);
begin
  ReportQuery.FieldByName('MAINKEY').AsString := 'MAIN\REPORT\WARTUNG';
  ReportQuery.FieldByName('VAL_TYP').Value := 8; // Blob-Bin�r (Formular)
end;
//******************************************************************************
end.

