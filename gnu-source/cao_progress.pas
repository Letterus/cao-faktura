unit cao_progress;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ComCtrls;

type
  TProgressForm = class(TForm)
    PB1: TProgressBar;
    Label1: TLabel;
    Label2: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
  private
    { Private-Deklarationen }
    Step  : Integer;
    Step2 : Integer;
    LStep : Integer;
    Run   : Boolean;
  public
    { Public-Deklarationen }
    procedure Init (Caption : String);
    procedure Start;
    procedure Stop;
    procedure UpdateScreen;
  end;

var
  ProgressForm: TProgressForm;

implementation

{$R *.DFM}

procedure TProgressForm.FormCreate(Sender: TObject);
begin
     Run   :=False;
     Step  :=0;
     Step2 :=0;
end;

procedure TProgressForm.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
     CanClose :=not Run;
end;

procedure TProgressForm.Init (Caption : String);
begin
     Run   :=False;
     Step  :=0;
     Step  :=0;;
     PB1.Position :=0;
     Self.Caption :=Caption;
end;

procedure TProgressForm.Start;
begin
     Step  :=0;
     Step2 :=0;
     LStep :=0;
     Run   :=True;
     PB1.Position :=0;
     //Show;
end;

procedure TProgressForm.Stop;
begin
     Run   :=False;
     Step  :=0;
     Step2 :=0;
     Hide;
end;

procedure TProgressForm.UpdateScreen;
begin
     inc(Step);
     inc(Step2);
     if Step>PB1.Max then Step :=PB1.Min;
     PB1.Position :=Step;

     if (Step2 > 10) and (not Visible) then Show;

     if Step2 > LStep+100 then
     begin
        LStep :=LStep+100;
        Label1.Caption :=IntToStr(Step2);
     end;

     Application.ProcessMessages;
end;

end.
