{

CAO-Faktura f�r Windows Version 1.0
Copyright (C) 2003 Jan Pokrandt / Jan@JP-SOFT.de

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.


******* CAO-Faktura comes with ABSOLUTELY NO WARRANTY ***************

Programm     : CAO-Faktura
Modul        : CAO_KASSE_ARTIKEL
Stand        : 25.01.2004
Version      : 1.2 RC3
Beschreibung : Dialog Artikelstammdaten f�r Kasse

History :

13.01.2003 - Version 1.0.0.48 released Jan Pokrandt
25.01.2004 - diverse Bugfixes

}


unit CAO_KASSE_ARTIKEL;

{$I CAO32.INC}

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Menus, StdCtrls, Buttons,  ComCtrls, ToolWin, SortGrid,
  Mask, ExtCtrls, JvCurrEdit, ZQuery,  ZMySqlQuery, CAO_DM, Db, DBCtrls,
  DBGrids, CaoGroupBox, ImgList, ExRxDBGrid, cao_var_const, ZUpdateSql,
  DBTables, JvComCtrls, AdvStatusBar, JvEdit, JvSpeedButton, Grids,
  JvDBCtrl, JvToolEdit, VolDBEdit, JvMaskEdit, JvSpin, CaoDBGrid,
  JvPlacemnt;

type
TAddArtikelEvent = procedure (ID : Integer; Menge : Double) of object; //


type
  TKasseArtikelForm = class(TForm)
    MainMenu1: TMainMenu;
    Adresse1: TMenuItem;
    Neu1: TMenuItem;
    Lschen1: TMenuItem;
    Sortierung1: TMenuItem;
    Match1: TMenuItem;
    Name11: TMenuItem;
    N1: TMenuItem;
    Suchen1: TMenuItem;
    ArtikelPanel: TPanel;
    Art_PC: TJvPageControl;
    Allg_TS: TTabSheet;
    Preis_TS: TTabSheet;
    Such_TS: TTabSheet;
    ArtSuchGrid: TCaoDBGrid;
    AS_DS: TDataSource;
    ArtPan: TPanel;
    ASQuery: TZMySqlQuery;
    ASQueryWARENGRUPPE: TIntegerField;
    ASQueryMATCHCODE: TStringField;
    ASQueryARTNUM: TStringField;
    ASQueryBARCODE: TStringField;
    ASQueryVK1: TFloatField;
    ASQueryVK2: TFloatField;
    ASQueryVK3: TFloatField;
    ASQueryVK4: TFloatField;
    ASQuerySTEUER_CODE: TIntegerField;
    ASQueryARTIKELTYP: TStringField;
    ASQueryME_EINHEIT: TStringField;
    ASQueryERLOES_KTO: TIntegerField;
    ASQueryAUFW_KTO: TIntegerField;
    ASQueryMENGE_AKT: TFloatField;
    ASQueryREC_ID: TIntegerField;
    ASQueryVK5: TFloatField;
    ASQueryERSATZ_ARTNUM: TStringField;
    ASQueryKAS_NAME: TStringField;
    ASQueryINFO: TBlobField;
    ASQueryPR_EINHEIT: TFloatField;
    ASQueryLAENGE: TStringField;
    ASQueryGROESSE: TStringField;
    ASQueryDIMENSION: TStringField;
    ASQueryGEWICHT: TFloatField;
    ASQueryINVENTUR_WERT: TFloatField;
    ASQueryEK_PREIS: TFloatField;
    ASQueryMENGE_START: TFloatField;
    ASQueryMENGE_MIN: TFloatField;
    ASQueryMENGE_BESTELLT: TFloatField;
    ASQueryMENGE_BVOR: TFloatField;
    ASQueryLAST_EK: TFloatField;
    ASQueryHERKUNFSLAND: TStringField;
    ASQueryLAGERORT: TStringField;
    ASQueryERSTELLT: TDateField;
    ASQueryERST_NAME: TStringField;
    ASQueryGEAEND: TDateField;
    ASQueryGEAEND_NAME: TStringField;
    ArtSuchenBtn: TJvSpeedButton;
    ArtMengeBtn: TJvSpeedButton;
    ArtAllgemeinBtn: TJvSpeedButton;
    SpeedButton1: TJvSpeedButton;
    ArtWgrPan: TPanel;
    ArtWgrTV: TTreeView;
    Panel1: TPanel;
    ArtHirDockBtn: TSpeedButton;
    Ansicht1: TMenuItem;
    Allgemein1: TMenuItem;
    MengePreise1: TMenuItem;
    Liste1: TMenuItem;
    ArtikelToolBar: TToolBar;
    ToolButton5: TToolButton;
    ToolButton2: TToolButton;
    ToolButton6: TToolButton;
    ArtikelToolbar1: TToolBar;
    DBNavigator1: TDBNavigator;
    Label31: TLabel;
    Suchbeg: TJvEdit;
    ToolButton9: TToolButton;
    SearchTimer: TTimer;
    SerNoTS: TTabSheet;
    ArtSernoBtn: TJvSpeedButton;
    SerNo1: TMenuItem;
    ASQueryKURZNAME: TStringField;
    ASQueryLANGNAME: TBlobField;
    Panel4: TPanel;
    SuchGB: TCaoGroupBox;
    ArtikelTyp: TVolgaDBEdit;
    Label13: TLabel;
    Label30: TLabel;
    Ersatz_artnr: TDBEdit;
    Artikelnr: TDBEdit;
    Label12: TLabel;
    match: TDBEdit;
    Label11: TLabel;
    ZuwGB: TCaoGroupBox;
    barcode: TDBEdit;
    lagerort: TDBEdit;
    Land: TDBEdit;
    Land_CB: TDBLookupComboBox;
    Warengr: TDBEdit;
    Warengr_CB: TDBLookupComboBox;
    Warengruppe: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    EinheitenGB: TCaoGroupBox;
    Gewicht: TDBEdit;
    E_KTO: TDBEdit;
    A_KTO: TDBEdit;
    Label20: TLabel;
    Label21: TLabel;
    Label29: TLabel;
    Inventur_wert: TDBEdit;
    Label22: TLabel;
    Label19: TLabel;
    Label17: TLabel;
    MEinheit: TDBEdit;
    BewDatGB: TCaoGroupBox;
    Label23: TLabel;
    MinMenge: TDBEdit;
    Label28: TLabel;
    Menge: TDBEdit;
    Panel5: TPanel;
    KurztextGB: TCaoGroupBox;
    KasTextGB: TCaoGroupBox;
    InfoGB: TCaoGroupBox;
    LangtextGB: TCaoGroupBox;
    InfoMemo: TDBMemo;
    LtextMemo: TDBMemo;
    kasname: TDBEdit;
    aname1: TDBEdit;
    CaoGroupBox1: TCaoGroupBox;
    Label25: TLabel;
    Label36: TLabel;
    Label37: TLabel;
    Label38: TLabel;
    Label39: TLabel;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    DBMemo4: TDBMemo;
    CaoGroupBox15: TCaoGroupBox;
    CaoGroupBox16: TCaoGroupBox;
    SerNoGrid: TCaoDBGrid;
    SerNoDS: TDataSource;
    Layoutspeichern1: TMenuItem;
    SuchFeldCB: TComboBox;
    Label52: TLabel;
    UebernahmeBtn: TToolButton;
    N3: TMenuItem;
    Aktualisieren1: TMenuItem;
    SichtbareSpalten1: TMenuItem;
    SpezialGB: TCaoGroupBox;
    NO_EK: TDBCheckBox;
    NO_VK: TDBCheckBox;
    NO_BEZEICHNUNG: TDBCheckBox;
    NO_PROV: TDBCheckBox;
    NO_RABATT: TDBCheckBox;
    SerNoCB: TDBCheckBox;
    MwstCB: TVolgaDBEdit;
    Label53: TLabel;
    Label54: TLabel;
    dimension: TDBEdit;
    Groesse: TDBEdit;
    Label55: TLabel;
    Laenge: TDBEdit;
    N4: TMenuItem;
    Treffer1: TMenuItem;
    N101: TMenuItem;
    N501: TMenuItem;
    N1001: TMenuItem;
    N2001: TMenuItem;
    N5001: TMenuItem;
    alle1: TMenuItem;
    EK_Preis: TDBEdit;
    ListPreis: TDBEdit;
    EKPreisLab: TLabel;
    ListPreisLab: TLabel;
    CaoGroupBox3: TCaoGroupBox;
    Kopieren1: TMenuItem;
    Label5: TLabel;
    DBEdit1: TDBEdit;
    Label6: TLabel;
    DBEdit2: TDBEdit;
    DBMemo1: TDBMemo;
    Label7: TLabel;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    Label8: TLabel;
    Label9: TLabel;
    Datei1: TMenuItem;
    EKPreiseanzeigen1: TMenuItem;
    SB1: TAdvStatusBar;
    Schlieen1: TMenuItem;
    FaktorLab: TLabel;
    GewinnFaktor: TJvxCurrencyEdit;
    N6: TMenuItem;
    Label46: TLabel;
    BrLiPreisLab: TLabel;
    BruttoPreis: TDBEdit;
    ASQueryRABGRP_ID: TStringField;
    RabGrpLoCB: TDBLookupComboBox;
    ASQueryCALC_EK: TFloatField;
    ASQueryAUTODEL_FLAG: TBooleanField;
    ASQuerySN_FLAG: TBooleanField;
    ASQueryNO_RABATT_FLAG: TBooleanField;
    ASQueryNO_PROVISION_FLAG: TBooleanField;
    ASQueryNO_BEZEDIT_FLAG: TBooleanField;
    ASQueryNO_EK_FLAG: TBooleanField;
    ASQueryNO_VK_FLAG: TBooleanField;
    ASQueryALTTEIL_FLAG: TBooleanField;
    MEBestellt: TDBEdit;
    Label59: TLabel;
    Label60: TLabel;
    ME_Bestvorschlag: TDBEdit;
    ASQueryCALC_VK1: TFloatField;
    ASQueryCALC_VK2: TFloatField;
    ASQueryCALC_VK3: TFloatField;
    ASQueryCALC_VK4: TFloatField;
    ASQueryVK1B: TFloatField;
    ASQueryVK2B: TFloatField;
    ASQueryVK3B: TFloatField;
    ASQueryVK4B: TFloatField;
    ASQueryVK5B: TFloatField;
    ASQueryHERSTELLER_ID: TIntegerField;
    ArtikelUpdateSql: TZUpdateSql;
    ASQueryDEFAULT_LIEF_ID: TIntegerField;
    Label47: TLabel;
    provision: TDBEdit;
    ASQueryPROVIS_PROZ: TFloatField;
    ASQueryCALC_VK1B: TFloatField;
    ASQueryCALC_VK2B: TFloatField;
    ASQueryCALC_VK3B: TFloatField;
    ASQueryCALC_VK4B: TFloatField;
    SerNoTab: TZMySqlQuery;
    SerNoTabARTIKEL_ID: TIntegerField;
    SerNoTabSERNUMMER: TStringField;
    SerNoTabEINK_NUM: TIntegerField;
    SerNoTabLIEF_NUM: TIntegerField;
    SerNoTabVERK_NUM: TIntegerField;
    SerNoTabSNUM_ID: TIntegerField;
    N2: TMenuItem;
    uebernehmen1: TMenuItem;
    N5: TMenuItem;
    uebernSchliessen1: TMenuItem;
    AddMengeLab: TLabel;
    AddMengeEdi: TJvSpinEdit;
    Label57: TLabel;
    EKPreisGB: TCaoGroupBox;
    Label3: TLabel;
    Label4: TLabel;
    ekpreis: TDBEdit;
    lastekpreis: TDBEdit;
    VKPreisGB: TCaoGroupBox;
    vk5lab: TLabel;
    vk4lab: TLabel;
    vk3lab: TLabel;
    Label2: TLabel;
    Label1: TLabel;
    Label35: TLabel;
    Label34: TLabel;
    MwStLabel: TLabel;
    Label32: TLabel;
    vk5_rgw: TJvxCurrencyEdit;
    vk4_rgw: TJvxCurrencyEdit;
    vk3_rgw: TJvxCurrencyEdit;
    vk2_rgw: TJvxCurrencyEdit;
    vk1_rgw: TJvxCurrencyEdit;
    vk1_brutto: TDBEdit;
    vk1_mwst: TJvxCurrencyEdit;
    vk2_brutto: TDBEdit;
    vk3_brutto: TDBEdit;
    vk4_brutto: TDBEdit;
    vk5_brutto: TDBEdit;
    vk5_mwst: TJvxCurrencyEdit;
    vk4_mwst: TJvxCurrencyEdit;
    vk3_mwst: TJvxCurrencyEdit;
    vk2_mwst: TJvxCurrencyEdit;
    vk2: TDBEdit;
    vk1: TDBEdit;
    vk3: TDBEdit;
    vk4: TDBEdit;
    vk5: TDBEdit;
    FormStorage1: TJvFormStorage;
    procedure Art_PCChange(Sender: TObject);
    procedure SortierungClick(Sender: TObject);
    procedure ARTIKEL_GRDTitleClick(Column: TColumn);
    procedure UebernahmeBtnClick(Sender: TObject);
    procedure ArtSuchGridDblClick(Sender: TObject);
    procedure ASQueryAfterScroll(DataSet: TDataSet);
    procedure matchKeyPress(Sender: TObject; var Key: Char);
    procedure matchKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure vk1Change(Sender: TObject);
    procedure vk1_bruttoChange(Sender: TObject);
    procedure vk1_rgwChange(Sender: TObject);
    procedure ekpreisChange(Sender: TObject);
    procedure MengeChange(Sender: TObject);
    procedure ArtSuchGridTitleBtnClick(Sender: TObject; ACol: Integer;
      Field: TField);
    procedure ASQueryBeforePost(DataSet: TDataSet);
    procedure AS_DSDataChange(Sender: TObject; Field: TField);
    procedure ArtAllgemeinBtnClick(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure ArtWgrTVDblClick(Sender: TObject);
    procedure ArtWgrTVExit(Sender: TObject);
    procedure ArtHirDockBtnClick(Sender: TObject);
    procedure ArtikelPanelResize(Sender: TObject);
    procedure ArtWgrTVChange(Sender: TObject; Node: TTreeNode);
    procedure SuchbegChange(Sender: TObject);
    procedure SearchTimerTimer(Sender: TObject);
    procedure Layoutspeichern1Click(Sender: TObject);
    procedure SuchFeldCBChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Aktualisieren1Click(Sender: TObject);
    procedure SichtbareSpalten1Click(Sender: TObject);
    procedure Neu1Click(Sender: TObject);
    procedure Lschen1Click(Sender: TObject);
    procedure aname1Exit(Sender: TObject);
    procedure ASQueryBeforeDelete(DataSet: TDataSet);
    procedure LtextMemoExit(Sender: TObject);
    procedure Suchen1Click(Sender: TObject);
    procedure alle1Click(Sender: TObject);
    procedure SuchbegKeyPress(Sender: TObject; var Key: Char);
    procedure Kopieren1Click(Sender: TObject);
    procedure matchEnter(Sender: TObject);
    procedure matchExit(Sender: TObject);
    procedure EKPreiseanzeigen1Click(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormDeactivate(Sender: TObject);
    procedure ASQueryNewRecord(DataSet: TDataSet);
    procedure ASQueryBeforeInsert(DataSet: TDataSet);
    procedure ArtSuchGridApplyCellAttribute(Sender: TObject; Field: TField;
      Canvas: TCanvas; State: TGridDrawState);
    procedure Schlieen1Click(Sender: TObject);
    procedure Artikelliste1Click(Sender: TObject);
    procedure EK_PreisChange(Sender: TObject);
    procedure GewinnFaktorChange(Sender: TObject);
    procedure ArtAllgemeinBtnMouseEnter(Sender: TObject);
    procedure ArtAllgemeinBtnMouseLeave(Sender: TObject);
    procedure ASQueryCalcFields(DataSet: TDataSet);
    procedure ArtikelnrChange(Sender: TObject);
    procedure ASQueryUpdateRecord(DataSet: TDataSet;
      UpdateKind: TUpdateKind; var UpdateAction: TUpdateAction);
    procedure SpeedButton2Click(Sender: TObject);
    procedure ASQueryPostError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure MwstCBChange(Sender: TObject);
    procedure uebernSchliessen1Click(Sender: TObject);
  private
    { Private-Deklarationen }

    searchtime  : Integer;
    QueryTime   : Integer;
    Limit       : Integer;

    sortname    : string;
    SortField   : String;
    SuchBegr    : String;
    WGR         : Integer;
    aktsort     : integer;

    Vk1_faktor,
    Vk2_faktor,
    Vk3_faktor,
    Vk4_faktor,
    Vk5_faktor : Double;

    BN_CALC_SCHRANKE : Double;

    Update      : Boolean;
    ArtWgrDok   : Boolean;
    LastF9      : Boolean;

    K2L         : Boolean; // Kurztext in Langtext �bernehmen

    Artikel_SQL : String;

    LastArtikelTyp : String;

    ME_EDIT        : Boolean;
    ME_EKBEST_EDIT : Boolean;

    SearchAuto     : Boolean;

    LastID         : Integer;

    AnzVKPreis     : Integer;

    InFaktorUpdate : Boolean;

    LastFocus      : HWND;

    procedure SetSort (Sort : Integer);
    procedure UpdateQuery;
    procedure UpdateStatus;
    procedure UpdateGewinnFaktor;

    procedure WMGetMinMaxInfo(var Msg: TWMGetMinMaxInfo);
               message WM_GETMINMAXINFO;
  public
    { Public-Deklarationen }
    First          : Boolean;
    OnlyOneArtikel : Boolean;  // wenn True, wird das Fenser nach der Artikel�bernahme geschlossen;

    ArtnumAuto     : Boolean;

    OnAddArtikel : TAddArtikelEvent; //

    OnUpdateStatusBar : TOnUpdateStatusBar;

    procedure F9Change (Ein : Boolean);
  end;

var
  KasseArtikelForm: TKasseArtikelForm;

implementation

uses cao_progress, cao_dbgrid_layout, cao_kasse_main, cao_tool1;

{$R *.DFM}


procedure TKasseArtikelForm.FormCreate(Sender: TObject);
begin
     OnlyOneArtikel    :=False;
     InFaktorUpdate    :=False;
     Limit             :=50;
     OnUpdateStatusBar :=nil;
     Scaled            :=TRUE;
     LastID            :=-1;
     ME_EDIT           :=False;
     ME_EKBEST_EDIT    :=False;
     Artikel_SQL       :='';
     Update            :=False;
     LastArtikelTyp    :='';
     First             :=True;
     WGr               :=0;
     K2L               :=True;
     ArtnumAuto        :=False;

     Vk1_faktor        :=0;
     Vk2_faktor        :=0;
     Vk3_faktor        :=0;
     Vk4_faktor        :=0;
     Vk5_faktor        :=0;

     BN_CALC_SCHRANKE  :=0;

     SetSOrt (1);
     SuchFeldCB.ItemIndex :=0;

     SpeedButton1.Align          :=alLeft;

     ArtAllgemeinBtn.Align       :=alRight;
     ArtMengeBtn.Align           :=alRight;
     ArtSernoBtn.Align           :=alRight;
     ArtSuchenBtn.Align          :=alRight;

     ArtAllgemeinBtn.Font.Color  :=clwhite;
     ArtMengeBtn.Font.Color      :=clwhite;
     ArtSuchenBtn.Font.Color     :=clwhite;
     ArtSernoBtn.Font.Color      :=clwhite;
     ArtAllgemeinBtn.Font.Style  :=[fsBold];

     LastFocus                   :=0;
end;
//------------------------------------------------------------------------------
procedure TKasseArtikelForm.FormActivate(Sender: TObject);

function FindParent (TopNode, Node : tTreeNode; ID : Integer) : Boolean;
var j : integer;
begin
     Result :=False;

     for j:=0 to TopNode.Count-1 do
     begin
        if (assigned(TopNode.Item[j]))and
           (Integer(TopNode.Item[j].Data)=ID) then
        begin
           Node.MoveTo (TopNode.Item[j],naAddChild);
           //Change :=True;
           Result :=True;
           break;
        end
           else
        begin
           if TopNode[j].HasChildren then
           begin
             Result :=FindParent (TopNode[j], Node, ID);
             if Result then break;
           end;
        end;
     end;
end;


var me, i, id    : integer;
    tn, wtn      : ttreenode;
    res, change  : boolean;
begin
     id :=LastID;
     if First then
     begin
          id :=-1;
          LastID :=-1;

          //SichtbareSpalten1.Visible :=True;
          //Layoutspeichern1.Visible  :=True;

          Allgemein1.Checked          :=True;
          ArtWgrDok                   :=False;
          ArtWgrPan.Visible           :=False;
          Art_PC.Align                :=alNone;
          ArtikelPanelResize(Sender);


          // Warengruppenauswahl f�llen
          try
            ArtWgrTV.Items.BeginUpdate;
            ArtWgrTV.Items.Clear;
            DM1.WgrTab.Close;
            DM1.WgrTab.Open;
            DM1.WgrTab.DisableControls;
            DM1.WgrTab.First;

            WTN :=ArtWgrTV.Items.Add(nil,'alle Warengruppen');
            WTN.Data    :=Pointer(-1);

            WTN.Selected :=True;
            try
              while not DM1.WgrTab.EOF do
              begin
                tn :=ArtWgrTV.Items.AddChild (WTN, DM1.WgrTabNAME.AsString);
                tn.Data :=Pointer(DM1.WgrTabID.Value);
                DM1.WgrTab.Next;
              end;
            finally
               DM1.WgrTab.EnableControls;
            end;

            // jetzt einsorieren
            if WTN.Count>0 then
            begin
              repeat
                Change :=False;
                for i:=0 to WTN.Count-1 do
                begin
                   if (DM1.WgrTab.Locate ('ID',Integer(WTN.Item[i].Data),[])) and
                      (DM1.WgrTabID.AsInteger=Integer(WTN.Item[i].Data))and
                      (DM1.WgrTabTOP_ID.AsInteger>0)and
                      (DM1.WgrTabID.AsInteger<>DM1.WgrTabTOP_ID.AsInteger) then
                   begin
                      if FindParent (WTN,
                                     WTN.Item[i],
                                     DM1.WgrTabTOP_ID.AsInteger) = True then
                      begin
                         Change :=True;
                         break;
                      end;
                   end;
                end;
              until Change=False;
            end;

            WTN.Selected :=True;
            WTN.Expand (True);
          except
          end;

          // Spezialauswahl hinzuf�gen
          DM1.Uniquery.Close;
          DM1.UniQuery.Sql.Text :='select VAL_INT AS ID, NAME, VAL_BLOB from REGISTERY '+
                                  'where MAINKEY="MAIN\\ART_HIR"';
          DM1.UniQuery.Open;
          if DM1.UniQuery.RecordCount>0 then
          begin
             WTN :=ArtWgrTV.Items.Add(nil,'Spezial');
             WTN.Data    :=Pointer(-1);

             while not DM1.UniQuery.Eof do
             begin
               tn :=ArtWgrTV.Items.AddChild (WTN,DM1.UniQuery.FieldByName ('NAME').AsString);
               tn.Data :=Pointer (DM1.UniQuery.FieldByName ('ID').AsInteger);


               DM1.UniQuery.Next;
             end;
          end;
          DM1.UniQuery.Close;


          ArtWgrTV.Items.EndUpdate;

          // Ende Warengruppen Tree-View

          // Kalkulationsfaktoren laden
          Vk1_faktor  :=DM1.ReadDouble ('MAIN\ARTIKEL','VK1_CALC_FAKTOR',0);
          Vk2_faktor  :=DM1.ReadDouble ('MAIN\ARTIKEL','VK2_CALC_FAKTOR',0);
          Vk3_faktor  :=DM1.ReadDouble ('MAIN\ARTIKEL','VK3_CALC_FAKTOR',0);
          Vk4_faktor  :=DM1.ReadDouble ('MAIN\ARTIKEL','VK4_CALC_FAKTOR',0);
          Vk5_faktor  :=DM1.ReadDouble ('MAIN\ARTIKEL','VK5_CALC_FAKTOR',0);

          BN_CALC_SCHRANKE :=DM1.ReadDouble  ('MAIN\ARTIKEL','BN_CALC_SCHRANKE',0);

          Artikel_SQL :='';

          First :=False;
          ART_PC.ActivePage :=Allg_TS;

          ASQueryVK1.DisplayFormat         :=',#0.00 "'+DM1.LeitWaehrung+'"';
          ASQueryVK1B.DisplayFormat        :=',#0.00 "'+DM1.LeitWaehrung+'"'; //NEU
          ASQueryVK2.DisplayFormat         :=',#0.00 "'+DM1.LeitWaehrung+'"';
          ASQueryVK2B.DisplayFormat        :=',#0.00 "'+DM1.LeitWaehrung+'"'; //NEU
          ASQueryVK3.DisplayFormat         :=',#0.00 "'+DM1.LeitWaehrung+'"';
          ASQueryVK3B.DisplayFormat        :=',#0.00 "'+DM1.LeitWaehrung+'"'; //NEU
          ASQueryVK4.DisplayFormat         :=',#0.00 "'+DM1.LeitWaehrung+'"';
          ASQueryVK4B.DisplayFormat        :=',#0.00 "'+DM1.LeitWaehrung+'"'; //NEU
          ASQueryVK5.DisplayFormat         :=',#0.00 "'+DM1.LeitWaehrung+'"';
          ASQueryVK5B.DisplayFormat        :=',#0.00 "'+DM1.LeitWaehrung+'"'; //NEU
          ASQueryEK_PREIS.DisplayFormat    :=',#0.000 "'+DM1.LeitWaehrung+'"';
          ASQueryCALC_EK.DisplayFormat     :=',#0.000 "'+DM1.LeitWaehrung+'"';

          ASQueryCALC_VK1.DisplayFormat    :=',#0.00 "'+DM1.LeitWaehrung+'"';
          ASQueryCALC_VK2.DisplayFormat    :=',#0.00 "'+DM1.LeitWaehrung+'"';
          ASQueryCALC_VK3.DisplayFormat    :=',#0.00 "'+DM1.LeitWaehrung+'"';
          ASQueryCALC_VK4.DisplayFormat    :=',#0.00 "'+DM1.LeitWaehrung+'"';

          ASQueryCALC_VK1B.DisplayFormat   :=',#0.00 "'+DM1.LeitWaehrung+'"';
          ASQueryCALC_VK2B.DisplayFormat   :=',#0.00 "'+DM1.LeitWaehrung+'"';
          ASQueryCALC_VK3B.DisplayFormat   :=',#0.00 "'+DM1.LeitWaehrung+'"';
          ASQueryCALC_VK4B.DisplayFormat   :=',#0.00 "'+DM1.LeitWaehrung+'"';

          ASQueryLast_EK.DisplayFormat     :=',#0.00 "'+DM1.LeitWaehrung+'"';

          vk1_mwst.DisplayFormat           :=',#0.00 "'+DM1.LeitWaehrung+'"';
          vk2_mwst.DisplayFormat           :=',#0.00 "'+DM1.LeitWaehrung+'"';
          vk3_mwst.DisplayFormat           :=',#0.00 "'+DM1.LeitWaehrung+'"';
          vk4_mwst.DisplayFormat           :=',#0.00 "'+DM1.LeitWaehrung+'"';
          vk5_mwst.DisplayFormat           :=',#0.00 "'+DM1.LeitWaehrung+'"';

          vk1_rgw.DisplayFormat            :=',#0.00 "'+DM1.LeitWaehrung+'"';
          vk2_rgw.DisplayFormat            :=',#0.00 "'+DM1.LeitWaehrung+'"';
          vk3_rgw.DisplayFormat            :=',#0.00 "'+DM1.LeitWaehrung+'"';
          vk4_rgw.DisplayFormat            :=',#0.00 "'+DM1.LeitWaehrung+'"';
          vk5_rgw.DisplayFormat            :=',#0.00 "'+DM1.LeitWaehrung+'"';

          ArtSuchGrid.RowColor1  :=DM1.C2Color;
          SerNoGrid.RowColor1    :=DM1.C2Color;


          if DM1.ReadInteger ('MAIN\ARTIKEL','KURZTEXT_TO_LANGTEXT',-1)=-1
           then DM1.WriteBoolean ('MAIN\ARTIKEL','KURZTEXT_TO_LANGTEXT',True);

          K2L :=DM1.ReadBoolean ('MAIN\ARTIKEL','KURZTEXT_TO_LANGTEXT',True);

          AnzVKPreis :=DM1.ReadInteger ('MAIN\ARTIKEL','ANZPREIS',-1);
          if AnzVKPreis=-1 then
          begin
             DM1.WriteInteger ('MAIN\ARTIKEL','ANZPREIS',5);
             AnzVKPreis :=5;
          end;

          if DM1.ReadInteger ('MAIN\ARTIKEL','ARTNUM_AUTO',-1)=-1
           then DM1.WriteBoolean ('MAIN\ARTIKEL','ARTNUM_AUTO',False);

          ArtnumAuto :=DM1.ReadBoolean ('MAIN\ARTIKEL','ARTNUM_AUTO',False);

          vk3.visible        :=AnzVKPreis>=3;
          vk3_mwst.visible   :=AnzVKPreis>=3;
          vk3_rgw.visible    :=AnzVKPreis>=3;
          vk3_brutto.visible :=AnzVKPreis>=3;
          vk3lab.visible     :=AnzVKPreis>=3;

          for i:=0 to ArtSuchGrid.Columns.Count-1 do
           if ArtSuchGrid.Columns[i].FieldName='VK3'
             then ArtSuchGrid.Columns[i].Visible :=AnzVKPreis>=3;

          vk4.visible        :=AnzVKPreis>=4;
          vk4_mwst.visible   :=AnzVKPreis>=4;
          vk4_rgw.visible    :=AnzVKPreis>=4;
          vk4_brutto.visible :=AnzVKPreis>=4;
          vk4lab.visible     :=AnzVKPreis>=4;

          for i:=0 to ArtSuchGrid.Columns.Count-1 do
           if ArtSuchGrid.Columns[i].FieldName='VK4'
            then ArtSuchGrid.Columns[i].Visible :=AnzVKPreis>=4;

          vk5.visible        :=AnzVKPreis>=5;
          vk5_mwst.visible   :=AnzVKPreis>=5;
          vk5_rgw.visible    :=AnzVKPreis>=5;
          vk5_brutto.visible :=AnzVKPreis>=5;
          vk5lab.visible     :=AnzVKPreis>=5;

          for i:=0 to ArtSuchGrid.Columns.Count-1 do
           if ArtSuchGrid.Columns[i].FieldName='VK5'
            then ArtSuchGrid.Columns[i].Visible :=AnzVKPreis>=5;

          case AnzVKPreis of
               2: VKPreisGB.Height :=vk2.top+vk2.height+10;
               3: VKPreisGB.Height :=vk3.top+vk3.height+10;
               4: VKPreisGB.Height :=vk4.top+vk4.height+10;
               5: VKPreisGB.Height :=vk5.top+vk5.height+10;
          end;

          ListPreis.Datafield   :='VK'+Inttostr(KMainForm.PEbene);
          ListPreis.Tag         :=KMainForm.PEbene*-1;

          BruttoPreis.Datafield :='VK'+Inttostr(KMainForm.PEbene)+'B';
          BruttoPreis.Tag       :=KMainForm.PEbene*-1;

          ListPreisLab.Caption :='Kas.-Preis ('+Inttostr(KMainForm.PEbene)+'):';

          dm1.GridLoadLayout (tDBGrid(ArtSuchGrid), 'KAS_ARTIKEL_LISTE',101);
          dm1.GridLoadLayout (tDBGrid(SerNoGrid),   'KAS_ARTIKEL_SERNO');

          Res :=DM1.ReadBooleanU ('','KAS_ARTIKEL_HIR_DOK',False);
          if Res then
          begin
            SpeedButton1Click(nil);
            ArtHirDockBtnClick(nil);
          end;

          Limit :=DM1.ReadIntegerU ('','KAS_ARTIKEL_TREFFER',50);
          case Limit of
                   10 : N101.Checked :=True;
                   50 : N501.Checked :=True;
                  100 : N1001.Checked :=True;
                  200 : N2001.Checked :=True;
                  500 : N5001.Checked :=True;
            999999999 : alle1.Checked :=True;
          end;
          if Limit < 10 then Limit :=10;

          me :=DM1.ReadIntegerU ('','ARTIKEL_ME_EDI',-1);
          if me=-1 then
          begin
            DM1.WriteBooleanU ('','ARTIKEL_ME_EDI',False);
            me :=0;
          end;

          ME_EDIT :=me=1;


          me :=DM1.ReadIntegerU ('','ARTIKEL_MEBEST_EDI',-1);
          if me=-1 then
          begin
            DM1.WriteBooleanU ('','ARTIKEL_MEBEST_EDI',False);
            me :=0;
          end;

          ME_EKBEST_EDIT :=me=1;


          // letztes Tabsheet wiederherstellen
          me :=DM1.ReadIntegerU ('','KAS_ARTIKEL_TABSHEET',-1);

          if (me>=0)and(me<ART_PC.PageCount)
           then ART_PC.ActivePage :=ART_PC.Pages[me]
           else ART_PC.ActivePage :=Allg_TS;

          SortField :=DM1.ReadStringU  ('',
                                        'KAS_ARTIKEL_SORTFELD',
                                        'MATCHCODE');

          SortName  :=DM1.ReadStringU  ('',
                                        'KAS_ARTIKEL_SORTNAME',
                                        'Suchbegriff');

          SuchFeldCB.ItemIndex :=DM1.ReadIntegerU ('','KAS_ARTIKEL_SUCHBEGRIFF',0);



          for i:=0 to ArtSuchGrid.Columns.Count-1 do
          begin
           if Uppercase(ArtSuchGrid.Columns[i].FieldName)='MENGE_AKT'
            then ArtSuchGrid.Columns[i].ReadOnly :=not ME_EDIT;
           if Uppercase(ArtSuchGrid.Columns[i].FieldName)='ARTIKELTYP'
            then ArtSuchGrid.Columns[i].ReadOnly :=TRUE;

           if ArtSuchGrid.Columns[i].Field.DisplayLabel = SortName
            then ArtSuchGrid.Columns[i].Title.Font.Style :=[fsBold]
            else ArtSuchGrid.Columns[i].Title.Font.Style :=[];
          end;

          i :=DM1.ReadIntegerU ('','SEARCH_AUTO_EXPAND',-1);
          if i<0 then DM1.WriteIntegerU ('','SEARCH_AUTO_EXPAND',0);

          SearchAuto := I=1;
          Application.ProcessMessages;
          Art_PCChange (Self);
     end;

     ArtWGRTV.Color        :=DM1.C2Color;

     MwstCB.Comboprops.ComboItems.Clear;
     MwstCB.Comboprops.ComboItems.Add ('keine');
     MwstCB.Comboprops.ComboItems.Add ('1='+FormatFloat('0.0"%"',DM1.MwstTab[1]));
     MwstCB.Comboprops.ComboItems.Add ('2='+FormatFloat('0.0"%"',DM1.MwstTab[2]));
     MwstCB.Comboprops.ComboItems.Add ('3='+FormatFloat('0.0"%"',DM1.MwstTab[3]));

     MwstCB.Comboprops.ComboValues.Clear;
     MwstCB.Comboprops.ComboValues.Add ('0');
     MwstCB.Comboprops.ComboValues.Add ('1');
     MwstCB.Comboprops.ComboValues.Add ('2');
     MwstCB.Comboprops.ComboValues.Add ('3');

     MwstCB.CreateDropDownList; // wichtig, damit die neuen Werte �bernommen werden

     EKPreiseAnzeigen1.Checked :=LastF9;
     F9Change (LastF9);

     UpdateQuery;

     if ID>0 then ASQuery.Locate ('REC_ID',ID,[]);

     try
       if Art_PC.Enabled then
       begin
         case Art_PC.ActivePage.Tag of
              1: match.setfocus;
              2: if (ekpreis.visible)and(ekpreis.enabled) then ekpreis.setfocus
                                                          else vk1.setfocus;
              3: SerNoGrid.SetFocus;
              6: ArtSuchGrid.SetFocus;
         end;
       end;
     except end;

     if not DM1.LiefRabGrp.Active then DM1.LiefRabGrp.Open;

     UebernahmeBtn.Enabled     :=(ASQuery.RecordCount>0);
     uebernehmen1.Enabled      :=(ASQuery.RecordCount>0);
     uebernSchliessen1.Enabled :=(ASQuery.RecordCount>0);

     ArtikelPanelResize(Sender);
     SB1.Top :=Self.ClientHeight;
     ArtikelPanelResize(Sender);
end;
//------------------------------------------------------------------------------
procedure TKasseArtikelForm.FormDeactivate(Sender: TObject);
begin
     if (ASQuery.State in [dsEdit,dsInsert]) then
     begin
        if MessageDlg ('Der aktuelle Artikel wurde ver�ndert,'+#13#10+
                       'wollen Sie die �nderung speichern ?',
                       mtconfirmation,[mbYes, mbNo],0) = mryes then
        begin
           ASQuery.Post;
        end
           else
        begin
           try ASQuery.Cancel; except end;
        end;
     end;

     DM1.WriteIntegerU ('','KAS_ARTIKEL_TABSHEET',ART_PC.ActivePageIndex);
     DM1.WriteStringU  ('','KAS_ARTIKEL_SORTFELD',SortField);
     DM1.WriteStringU  ('','KAS_ARTIKEL_SORTNAME',SortName);
     DM1.WriteIntegerU ('','KAS_ARTIKEL_SUCHBEGRIFF',SuchFeldCB.ItemIndex);

     if (not (ArtikelPanel.Parent is tForm)) and
        (ArtikelPanel.Parent.Parent is tForm) and
        (assigned(tForm(ArtikelPanel.Parent.Parent).ActiveControl))
      then tDBEdit(tForm(ArtikelPanel.Parent.Parent).ActiveControl).Color :=clWindow;

     if ASQuery.Active        then ASQuery.Close;
     if SerNoTab.Active       then SerNoTab.Close;
     if DM1.LiefRabGrp.Active then DM1.LiefRabGrp.Close;
end;
//------------------------------------------------------------------------------
procedure TKasseArtikelForm.WMGetMinMaxInfo(var Msg: TWMGetMinMaxInfo);
begin
  inherited;
  with Msg.MinMaxInfo^ do
  begin
    ptMinTrackSize.x:= 705;
    ptMaxTrackSize.x:= Screen.Width;
    ptMinTrackSize.y:= 605;
    ptMaxTrackSize.y:= Screen.Height;
  end;
end;
//------------------------------------------------------------------------------
procedure TKasseArtikelForm.F9Change (Ein : Boolean);
var i : integer;
begin
     LastF9 :=Ein;

     EK_Preis.Visible :=LastF9;
     EKPreisLab.Visible :=LastF9;
     FaktorLab.Visible :=LastF9;
     GewinnFaktor.Visible :=LastF9;

     EKPreisGB.Visible :=False;
     VKPreisGB.Visible :=False;

     VKPreisGB.Visible :=True;
     EKPreisGB.Visible :=LastF9;
     EKPreisGB.Top :=CaoGroupBox3.Top+CaoGroupBox3.Height-5;

     for i:=0 to ArtSuchGrid.Columns.Count-1 do
      if (ArtSuchGrid.Columns[i].FieldName='EK_PREIS')or
         (ArtSuchGrid.Columns[i].FieldName='CALC_EK')
       then ArtSuchGrid.Columns[i].Visible :=LastF9;

     vk1_rgw.visible :=(LastF9) and (AnzVKPreis >=1);
     vk2_rgw.visible :=(LastF9) and (AnzVKPreis >=2);
     vk3_rgw.visible :=(LastF9) and (AnzVKPreis >=3);
     vk4_rgw.visible :=(LastF9) and (AnzVKPreis >=4);
     vk5_rgw.visible :=(LastF9) and (AnzVKPreis >=5);
     Label35.visible :=LastF9;

     EKPreiseanzeigen1.Checked :=LastF9;
end;
//------------------------------------------------------------------------------
procedure TKasseArtikelForm.SetSort (Sort : Integer);
var I : Integer;
begin
     if sort<0 then sort :=aktsort;
     if sort=aktsort then exit;
     case sort of
  {mach}     1:begin
                    sortname         :='Suchbegriff';
                    SortField        :='MATCHCODE'
               end;
  {name}     2:begin
                    sortname         :='Artikelnummer';
                    SortField        :='ARTNUM'
               end;
     end;
     AktSort :=Sort;

     if First then exit;

     UpdateQuery;
     UpdateStatus;

     for i:=0 to ArtSuchGrid.Columns.Count-1 do
     begin
        if ArtSuchGrid.Columns[i].Field.DisplayLabel = SortName
         then ArtSuchGrid.Columns[i].Title.Font.Style :=[fsBold]
         else ArtSuchGrid.Columns[i].Title.Font.Style :=[];
     end;
end;
//------------------------------------------------------------------------------
procedure TKasseArtikelForm.UpdateQuery;
var LastTime : DWord;
begin
     LastTime :=GetTickCount;
     ASQuery.SQL.Clear;
     ASQuery.Close;

     ASQuery.SQL.Add ('SELECT ARTIKEL.*');
     ASQuery.SQL.Add ('FROM ARTIKEL');

     if (SuchBegr <> '')and(SuchFeldCB.ItemIndex=3)
      then ASQuery.SQL.Add (', ARTIKEL_PREIS');

     if (Pos('INNER JOIN',uppercase(Artikel_SQL))>0)or
        (Pos('WHERE',uppercase(Artikel_SQL))>0)
      then ASQuery.SQL.Add (Artikel_SQL);
//      else

     if (SuchBegr <> '') or
        ((Artikel_SQL <> '')and
         (Pos('INNER JOIN',uppercase(Artikel_SQL))=0)
        ) then
     begin
        if Pos('WHERE',UpperCase(Artikel_SQL))=0
         then ASQuery.SQL.Add ('WHERE')
         else if SuchBegr <> '' then ASQuery.SQL.Add ('AND ');

     end;

     if SuchBegr <> '' then
     begin
        if SearchAuto then
        begin
          case SuchFeldCB.ItemIndex of
            1:   begin
                   ASQuery.SQL.Add (DM1.GetSearchSQL (['UPPER(MATCHCODE)'],Suchbegr));
                 end;
            2:   begin
                   ASQuery.SQL.Add (DM1.GetSearchSQL (['UPPER(ARTNUM)'],Suchbegr));
                 end;
            3:   begin
                   ASQuery.SQL.Add ('('+DM1.GetSearchSQL (['UPPER(ARTIKEL_PREIS.BESTNUM)'],Suchbegr)+
                                    ') AND ARTIKEL_PREIS.ARTIKEL_ID=ARTIKEL.REC_ID');
                 end;
            4:   begin
                   ASQuery.SQL.Add (DM1.GetSearchSQL (['UPPER(INFO)'],Suchbegr));
                 end;
            else begin
                   ASQuery.SQL.Add (DM1.GetSearchSQL (['UPPER(LANGNAME)',
                                                       'UPPER(KURZNAME)',
                                                       'UPPER(KAS_NAME)'],Suchbegr));
                 end;
          end;
        end
           else
        begin
          case SuchFeldCB.ItemIndex of
            1:   begin
                   ASQuery.SQL.Add ('UPPER(MATCHCODE) LIKE "'+SuchBegr+'%"');
                 end;
            2:   begin
                   ASQuery.SQL.Add ('UPPER(ARTNUM) LIKE "'+SuchBegr+'%"');
                 end;
            3:   begin
                   ASQuery.SQL.Add ('(UPPER(ARTIKEL_PREIS.BESTNUM) LIKE "'+SuchBegr+'%"'+
                                    ') AND ARTIKEL_PREIS.ARTIKEL_ID=ARTIKEL.REC_ID');
                 end;
            4:   begin
                   ASQuery.SQL.Add ('UPPER(INFO) LIKE "'+SuchBegr+'%"');
                 end;
            else begin
                   ASQuery.SQL.Add ('((UPPER(LANGNAME) LIKE "'+SuchBegr+'%")or');
                   ASQuery.SQL.Add ('(UPPER(KURZNAME) LIKE "'+SuchBegr+'%")or');
                   ASQuery.SQL.Add ('(UPPER(KAS_NAME) LIKE "'+SuchBegr+'%"))');
                 end;
          end;
        end;
        if (Artikel_SQL <> '') and
           (Pos('INNER JOIN',Uppercase(Artikel_SQL))=0) and
           (Pos('WHERE',Uppercase(Artikel_SQL))=0)
         then ASQuery.SQL.Add (' and ');
     end;
     if (Artikel_SQL <> '')  and
        (Pos('INNER JOIN',Uppercase(Artikel_SQL))=0) and
        (Pos('WHERE',Uppercase(Artikel_SQL))=0)
      then ASQuery.SQL.Add (Artikel_SQL);

     ASQuery.SQL.Add ('ORDER BY '+SortField);
     ASQuery.SQL.Add ('LIMIT 0,'+Inttostr(limit));
     Screen.Cursor :=crSQLWait;

     try
        ASQuery.Open;
     finally
        Screen.Cursor :=crDefault;
     end;

     QueryTime :=GetTickCount-LastTime;
     UpdateStatus;


     if (ASQuery.RecordCount=0) and
        (Art_PC.Enabled)
      then Art_PC.Enabled :=False
      else
     if (ASQuery.RecordCount>0) and
        (not Art_PC.Enabled)
      then Art_PC.Enabled:=True;
end;
//------------------------------------------------------------------------------
procedure TKasseArtikelForm.Art_PCChange(Sender: TObject);
begin
     if Art_PC.ActivePage=Preis_TS then
     begin
         vk1Change(vk1);
         vk1Change(vk2);
         vk1Change(vk3);
         vk1Change(vk4);
         vk1Change(vk5);
     end else
     begin
     end;

     if Art_PC.ActivePage = SerNoTS then
     begin
          SerNoTab.Close;
          SerNoTab.ParamByName('ID').AsInteger :=ASQueryREC_ID.Value;
          SerNoTab.Open;
     end else SerNoTab.Close;

     SichtbareSpalten1.Enabled :=Art_PC.ActivePage=Such_TS;

     ArtAllgemeinBtn.Font.Style  :=[];
     ArtMengeBtn.Font.Style      :=[];
     ArtSuchenBtn.Font.Style     :=[];
     ArtSernoBtn.Font.Style      :=[];


     case Art_PC.ActivePageIndex of
        0: begin
                ArtAllgemeinBtn.Font.Style :=[fsBold];
                Allgemein1.Checked :=True;
           end;
        1: begin
                ArtMengeBtn.Font.Style :=[fsBold];
                MengePreise1.Checked :=True;
           end;
        2: begin
                ArtSernoBtn.Font.Style :=[fsBold];
                Serno1.Checked :=True;
           end;
        3: begin
                ArtSuchenBtn.Font.Style :=[fsBold];
                Suchen1.Checked :=True;
           end;
     end;
end;
//------------------------------------------------------------------------------
procedure TKasseArtikelForm.SortierungClick(Sender: TObject);
begin
    if (tmenuitem (sender).tag>0)and(tmenuitem (sender).tag<6) then
    begin
      tmenuitem (sender).checked :=not tmenuitem (sender).checked;
      SetSort (tmenuitem (sender).tag);
    end;
end;
//------------------------------------------------------------------------------
procedure TKasseArtikelForm.ARTIKEL_GRDTitleClick(Column: TColumn);
begin
     case Column.Index of
{match}   1:SetSort (1);
{artnum}  5:SetSort (2);
     end;
end;
//------------------------------------------------------------------------------
procedure TKasseArtikelForm.ArtSuchGridApplyCellAttribute(Sender: TObject;
  Field: TField; Canvas: TCanvas; State: TGridDrawState);
begin
     if (not (gdSelected in State)) and
        (uppercase(Field.FieldName) = 'MENGE_AKT') and
        (Field.AsFloat<0)
      then Canvas.Font.Color :=clRed;
end;
//------------------------------------------------------------------------------
procedure TKasseArtikelForm.UebernahmeBtnClick(Sender: TObject);
begin
     UebernahmeBtn.Enabled :=False;
     if ASQuery.State in [dsEdit,dsInsert] then ASQuery.Post;
     if Assigned (OnAddArtikel)
      then OnAddArtikel (ASQueryRec_ID.Value,AddMengeEdi.Value);
     if OnlyOneArtikel then Close;
end;
//------------------------------------------------------------------------------
procedure TKasseArtikelForm.ArtSuchGridDblClick(Sender: TObject);
begin
     Art_PC.ActivePage :=Allg_TS;
     Art_PCChange(Sender);
end;
//------------------------------------------------------------------------------
procedure TKasseArtikelForm.ASQueryAfterScroll(DataSet: TDataSet);
begin
     if LastFocus>0 then
     begin
        Windows.SetFocus (LastFocus);
        LastFocus :=0;
     end;

     ArtMengeBtn.Enabled :=ASQueryARTIKELTYP.Value<>'T';
     ArtSernoBtn.Enabled :=(ASQueryARTIKELTYP.Value<>'T')and
                           (ASQueryARTIKELTYP.Value<>'L')and
                           (ASQuerySN_FLAG.Value=True);
     SerNo1.Enabled      :=ArtSernoBtn.Enabled;

     NO_EK.Enabled :=ASQueryARTIKELTYP.Value<>'S';

     if (ASQuery.RecordCount=0) and
        (Art_PC.Enabled) and
        (not (ASquery.State in [dsInsert]))
      then Art_PC.Enabled :=False
      else
     if (ASQuery.RecordCount>0) and
        (not Art_PC.Enabled)
      then Art_PC.Enabled:=True;


     if LastID = ASQueryRec_ID.Value then exit;

     if Art_PC.ActivePage=Preis_TS then
     begin
       // wenn es ein Artikel ein Text ist, dann Wechseln auf Allgemein !!!
       if (ASQueryArtikelTyp.Value='T') then
       begin
            Art_PC.ActivePage:=Allg_TS;
            Art_PCChange (Self);
            exit;
       end;
       vk1Change(vk1);
       vk1Change(vk2);
       vk1Change(vk3);
       vk1Change(vk4);
       vk1Change(vk5);
     end else
     if Art_PC.ActivePage=SerNoTS then
     begin
       // wenn es ein Artikel ohne Seriennummern ist, dann Wechseln auf Allgemein !!!
       if (ASQueryArtikelTyp.Value<>'T') then
       begin
            Art_PC.ActivePage:=Allg_TS;
            SerNoTab.Close;
            Art_PCChange (Self);
            exit;
       end;
       SerNoTab.Close;
       SerNoTab.ParamByName('ID').AsInteger :=ASQueryREC_ID.Value;
       SerNoTab.Open;
     end;

     UpdateStatus;

     LastID :=ASQueryRec_ID.Value;


     UpdateGewinnFaktor;

     MwStLabel.Caption :=Formatfloat ('"MwSt ("0.0"%)"',
                                      DM1.MwStTab[ASQuerySTEUER_CODE.Value]);

     UebernahmeBtn.Enabled     :=(ASQuery.RecordCount>0);
     uebernehmen1.Enabled      :=(ASQuery.RecordCount>0);
     uebernSchliessen1.Enabled :=(ASQuery.RecordCount>0);

     ProgressForm.Stop;
end;
//------------------------------------------------------------------------------
procedure TKasseArtikelForm.matchKeyPress(Sender: TObject; var Key: Char);
begin
     if key=#13 then
     begin
          key :=#0;

          try
            if (uppercase(tControl(Sender).Name)='SERNOCB')and(SernoCB.Enabled)
             then Match.SetFocus
             else
            if (uppercase(tControl(Sender).Name)='NO_BEZEICHNUNG')and(not SernoCB.Enabled)
             then Match.SetFocus
             else
            if (uppercase(tControl(Sender).Name)='EKPREIS')
             then VK1.SetFocus
             else

            if (uppercase(tControl(Sender).Name)='VK2_RGW')and(EKPreisGB.Visible)and(not VK3.Visible)
             then EKPreis.SetFocus
             else
            if (uppercase(tControl(Sender).Name)='VK2_RGW')and(not EKPreisGB.Visible)and(not VK3.Visible)
             then VK1.SetFocus
             else
            if (uppercase(tControl(Sender).Name)='VK3_RGW')and(EKPreisGB.Visible)and(not VK4.Visible)
             then EKPreis.SetFocus
             else
            if (uppercase(tControl(Sender).Name)='VK3_RGW')and(not EKPreisGB.Visible)and(not VK4.Visible)
             then VK1.SetFocus
             else
            if (uppercase(tControl(Sender).Name)='VK4_RGW')and(EKPreisGB.Visible)and(not VK5.Visible)
             then EKPreis.SetFocus
             else
            if (uppercase(tControl(Sender).Name)='VK4_RGW')and(not EKPreisGB.Visible)and(not VK5.Visible)
             then VK1.SetFocus
             else

            if (uppercase(tControl(Sender).Name)='VK5_RGW')and(EKPreisGB.Visible)
             then EKPreis.SetFocus
             else
            if (uppercase(tControl(Sender).Name)='VK5_RGW')and(not EKPreisGB.Visible)
             then VK1.SetFocus
            else
            begin
                 if ArtikelPanel.Parent is tForm
                   then SendMessage (ArtikelPanel.Parent.Handle,WM_NEXTDLGCTL,0,0)
                   else SendMessage (ArtikelPanel.Parent.Parent.Handle,WM_NEXTDLGCTL,0,0);
            end;
          except end;
     end;
end;
//------------------------------------------------------------------------------
procedure TKasseArtikelForm.matchKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
//var k : char;
begin
     if (Shift=[]) and (key=33) then // PGUP
     begin
          ASQuery.Prior;
          key :=0;
     end else
     if (Shift=[]) and (key=34) then // PGDOWN
     begin
          ASQuery.Next;
          key :=0;
     end;
end;
//------------------------------------------------------------------------------
procedure TKasseArtikelForm.vk1Change(Sender: TObject);
var B : Double;
begin
     // MWST-Summen berechnen              
     // Brutto-Preise berechnen
     Update :=True;
     try
       case tControl(Sender).Tag of
        -1:begin
                if (
                    (ASQueryVK1.Value * (1 + DM1.MwStTab[ASQuerySTEUER_CODE.Value]/100) >
                        asqueryvk1b.Value+BN_CALC_SCHRANKE) or
                    (ASQueryVK1.Value * (1 + DM1.MwStTab[ASQuerySTEUER_CODE.Value]/100) <
                        asqueryvk1b.Value-BN_CALC_SCHRANKE)
                   ) and (asquery.state in [dsedit, dsinsert]) then
                begin
                  B :=ASQueryVK1.Value * (1 + DM1.MwStTab[ASQuerySTEUER_CODE.Value]/100);

                  // Runden
                  B :=cao_round(B*100); // jetzt ganze Cent

                  B :=cao_Round (B / DM1.BR_RUND_WERT) * DM1.BR_RUND_WERT / 100;

                  ASQueryVK1B.Value :=B;
                end;

                vk1_mwst.Value   :=ASQueryCALC_VK1.Value * (DM1.MwStTab[ASQuerySTEUER_CODE.Value]/100);
                vk1_rgw.Value    :=ASQueryCALC_VK1.Value - ASQueryCalc_EK.Value;
                vk1_rgw.Font.Color :=clRed*ord (vk1_rgw.value<0);
           end;
        -2:begin
                //vk2_brutto.Value :=ASQueryVK2.Value * (1 + DM1.MwStTab[ASQuerySTEUER_CODE.Value]/100);

                if (
                    (ASQueryVK2.Value * (1 + DM1.MwStTab[ASQuerySTEUER_CODE.Value]/100) >
                        asqueryVK2b.Value+BN_CALC_SCHRANKE) or
                    (ASQueryVK2.Value * (1 + DM1.MwStTab[ASQuerySTEUER_CODE.Value]/100) <
                        asqueryVK2b.Value-BN_CALC_SCHRANKE)
                   ) and (asquery.state in [dsedit, dsinsert]) then
                begin
                  B :=ASQueryVK2.Value * (1 + DM1.MwStTab[ASQuerySTEUER_CODE.Value]/100);

                  // Runden
                  B :=cao_round(B*100); // jetzt ganze Cent

                  B :=cao_Round (B / DM1.BR_RUND_WERT) * DM1.BR_RUND_WERT / 100;

                  ASQueryVK2B.Value :=B;
                end;

                vk2_mwst.Value   :=ASQueryCALC_VK2.Value * (DM1.MwStTab[ASQuerySTEUER_CODE.Value]/100);
                vk2_rgw.Value    :=ASQueryCALC_VK2.Value - ASQueryCalc_EK.Value;
                vk2_rgw.Font.Color :=clRed*ord (vk2_rgw.value<0);
           end;
        -3:begin
                //vk3_brutto.Value :=ASQueryVK3.Value * (1 + DM1.MwStTab[ASQuerySTEUER_CODE.Value]/100);

                if (
                    (ASQueryVK3.Value * (1 + DM1.MwStTab[ASQuerySTEUER_CODE.Value]/100) >
                        asqueryVK3b.Value+BN_CALC_SCHRANKE) or
                    (ASQueryVK3.Value * (1 + DM1.MwStTab[ASQuerySTEUER_CODE.Value]/100) <
                        asqueryVK3b.Value-BN_CALC_SCHRANKE)
                   ) and (asquery.state in [dsedit, dsinsert]) then
                begin
                  B :=ASQueryVK3.Value * (1 + DM1.MwStTab[ASQuerySTEUER_CODE.Value]/100);

                  // Runden            
                  B :=cao_round(B*100); // jetzt ganze Cent

                  B :=cao_Round (B / DM1.BR_RUND_WERT) * DM1.BR_RUND_WERT / 100;

                  ASQueryVK3B.Value :=B;
                end;

                vk3_mwst.Value   :=ASQueryCALC_VK3.Value * (DM1.MwStTab[ASQuerySTEUER_CODE.Value]/100);
                vk3_rgw.Value    :=ASQueryCALC_VK3.Value - ASQueryCalc_EK.Value;
                vk3_rgw.Font.Color :=clRed*ord (vk3_rgw.value<0);
           end;
        -4:begin
                //vk4_brutto.Value :=ASQueryVK4.Value * (1 + DM1.MwStTab[ASQuerySTEUER_CODE.Value]/100);

                if (
                    (ASQueryVK4.Value * (1 + DM1.MwStTab[ASQuerySTEUER_CODE.Value]/100) >
                        asqueryVK4b.Value+BN_CALC_SCHRANKE) or
                    (ASQueryVK4.Value * (1 + DM1.MwStTab[ASQuerySTEUER_CODE.Value]/100) <
                        asqueryVK4b.Value-BN_CALC_SCHRANKE)
                   ) and (asquery.state in [dsedit, dsinsert]) then
                begin
                  B :=ASQueryVK4.Value * (1 + DM1.MwStTab[ASQuerySTEUER_CODE.Value]/100);

                  // Runden
                  B :=cao_round(B*100); // jetzt ganze Cent

                  B :=cao_Round (B / DM1.BR_RUND_WERT) * DM1.BR_RUND_WERT / 100;

                  ASQueryVK4B.Value :=B;
                end;

                vk4_mwst.Value   :=ASQueryCALC_VK4.Value * (DM1.MwStTab[ASQuerySTEUER_CODE.Value]/100);
                vk4_rgw.Value    :=ASQueryCALC_VK4.Value - ASQueryCalc_EK.Value;
                vk4_rgw.Font.Color :=clRed*ord (vk4_rgw.value<0);
           end;
        -5:begin
                //vk5_brutto.Value :=ASQueryVK5.Value * (1 + DM1.MwStTab[ASQuerySTEUER_CODE.Value]/100);

                if (
                    (ASQueryVK5.Value * (1 + DM1.MwStTab[ASQuerySTEUER_CODE.Value]/100) >
                        asqueryVK5b.Value+BN_CALC_SCHRANKE) or
                    (ASQueryVK5.Value * (1 + DM1.MwStTab[ASQuerySTEUER_CODE.Value]/100) <
                        asqueryVK5b.Value-BN_CALC_SCHRANKE)
                   ) and (asquery.state in [dsedit, dsinsert]) then
                begin
                  B :=ASQueryVK5.Value * (1 + DM1.MwStTab[ASQuerySTEUER_CODE.Value]/100);

                  // Runden
                  B :=cao_round(B*100); // jetzt ganze Cent

                  B :=cao_Round (B / DM1.BR_RUND_WERT) * DM1.BR_RUND_WERT / 100;

                  ASQueryVK5B.Value :=B;
                end;

                vk5_mwst.Value   :=ASQueryVK5.Value * (DM1.MwStTab[ASQuerySTEUER_CODE.Value]/100);
                vk5_rgw.Value    :=ASQueryVK5.Value - ASQueryCalc_EK.Value;
                vk5_rgw.Font.Color :=clRed*ord (vk5_rgw.value<0);
           end;
       end; // case
     finally
        Update :=False;
     end;
     UpdateGewinnFaktor;
     matchExit(Sender);
end;
//------------------------------------------------------------------------------
procedure TKasseArtikelForm.vk1_bruttoChange(Sender: TObject);
var B, N : Double;
begin
     // Nettopresi zur�ckrechnen, da der Brutto-Preis ge�ndert wurde !
     if Update then exit;
     if not (ASQuery.State in [dsEdit, dsInsert]) then ASQuery.Edit;
     case tControl(Sender).Tag of
          -1:begin
               // Runden
               B :=cao_round(ASQueryVK1B.Value*100); // jetzt ganze Cent
               B :=cao_Round (B / DM1.BR_RUND_WERT) * DM1.BR_RUND_WERT / 100;
               ASQueryVK1B.Value :=B;

               // jetzt ggf. Netto berechnen
               N :=ASQueryVK1B.Value / (1 + DM1.MwStTab[ASQuerySTEUER_CODE.Value]/100);
               N :=cao_round(N * 100)/100;

               if (asquery.state in [dsedit, dsinsert]) and
                  (
                   (N>ASQueryvk1.Value+BN_CALC_SCHRANKE) or
                   (N<ASQueryvk1.Value+BN_CALC_SCHRANKE)
                  )
                then ASQueryvk1.Value :=N;

               vk1_mwst.Value   :=ASQueryVK1.Value * (DM1.MwStTab[ASQuerySTEUER_CODE.Value]/100);
               vk1_rgw.Value    :=ASQueryVK1.Value - ASQueryCalc_EK.Value;
               vk1_rgw.Font.Color :=clRed*ord (vk1_rgw.value<0);

             end;

          -2:begin
               // Runden
               B :=cao_round(ASQueryVK2B.Value*100); // jetzt ganze Cent
               B :=cao_Round (B / DM1.BR_RUND_WERT) * DM1.BR_RUND_WERT / 100;
               ASQueryVK2B.Value :=B;

               // jetzt ggf. Netto berechnen
               N :=ASQueryVK2B.Value / (1 + DM1.MwStTab[ASQuerySTEUER_CODE.Value]/100);
               N :=cao_round(N * 100)/100;

               if (asquery.state in [dsedit, dsinsert]) and
                  (
                   (N>ASQueryVK2.Value+BN_CALC_SCHRANKE) or
                   (N<ASQueryVK2.Value+BN_CALC_SCHRANKE)
                  )
                then ASQueryVK2.Value :=N;

               vk2_mwst.Value   :=ASQueryVK2.Value * (DM1.MwStTab[ASQuerySTEUER_CODE.Value]/100);
               vk2_rgw.Value    :=ASQueryVK2.Value - ASQueryCalc_EK.Value;
               vk2_rgw.Font.Color :=clRed*ord (vk2_rgw.value<0);

             end;
          -3:begin
               // Runden
               B :=cao_round(ASQueryVK3B.Value*100); // jetzt ganze Cent
               B :=cao_Round (B / DM1.BR_RUND_WERT) * DM1.BR_RUND_WERT / 100;
               ASQueryVK3B.Value :=B;

               // jetzt ggf. Netto berechnen
               N :=ASQueryVK3B.Value / (1 + DM1.MwStTab[ASQuerySTEUER_CODE.Value]/100);
               N :=cao_round(N * 100)/100;

               if (asquery.state in [dsedit, dsinsert]) and
                  (
                   (N>ASQueryVK3.Value+BN_CALC_SCHRANKE) or
                   (N<ASQueryVK3.Value+BN_CALC_SCHRANKE)
                  )
                then ASQueryVK3.Value :=N;

               vk3_mwst.Value   :=ASQueryVK3.Value * (DM1.MwStTab[ASQuerySTEUER_CODE.Value]/100);
               vk3_rgw.Value    :=ASQueryVK3.Value - ASQueryCalc_EK.Value;
               vk3_rgw.Font.Color :=clRed*ord (vk3_rgw.value<0);

             end;
          -4:begin
               // Runden
               B :=cao_round(ASQueryVK4B.Value*100); // jetzt ganze Cent
               B :=cao_Round (B / DM1.BR_RUND_WERT) * DM1.BR_RUND_WERT / 100;
               ASQueryVK4B.Value :=B;

               // jetzt ggf. Netto berechnen
               N :=ASQueryVK4B.Value / (1 + DM1.MwStTab[ASQuerySTEUER_CODE.Value]/100);
               N :=cao_round(N * 100)/100;

               if (asquery.state in [dsedit, dsinsert]) and
                  (
                   (N>ASQueryVK4.Value+BN_CALC_SCHRANKE) or
                   (N<ASQueryVK4.Value+BN_CALC_SCHRANKE)
                  )
                then ASQueryVK4.Value :=N;

               vk4_mwst.Value   :=ASQueryVK4.Value * (DM1.MwStTab[ASQuerySTEUER_CODE.Value]/100);
               vk4_rgw.Value    :=ASQueryVK4.Value - ASQueryCalc_EK.Value;
               vk4_rgw.Font.Color :=clRed*ord (vk4_rgw.value<0);

             end;
          -5:begin
               // Runden
               B :=cao_round(ASQueryVK5B.Value*100); // jetzt ganze Cent
               B :=cao_Round (B / DM1.BR_RUND_WERT) * DM1.BR_RUND_WERT / 100;
               ASQueryVK5B.Value :=B;

               // jetzt ggf. Netto berechnen
               N :=ASQueryVK5B.Value / (1 + DM1.MwStTab[ASQuerySTEUER_CODE.Value]/100);
               N :=cao_round(N * 100)/100;

               if (asquery.state in [dsedit, dsinsert]) and
                  (
                   (N>ASQueryVK5.Value+BN_CALC_SCHRANKE) or
                   (N<ASQueryVK5.Value+BN_CALC_SCHRANKE)
                  )
                then ASQueryVK5.Value :=N;

               vk5_mwst.Value   :=ASQueryVK5.Value * (DM1.MwStTab[ASQuerySTEUER_CODE.Value]/100);
               vk5_rgw.Value    :=ASQueryVK5.Value - ASQueryCalc_EK.Value;
               vk5_rgw.Font.Color :=clRed*ord (vk5_rgw.value<0);

             end;
     end;
     UpdateGewinnFaktor;
     MatchExit (Sender);
end;
//------------------------------------------------------------------------------
procedure TKasseArtikelForm.vk1_rgwChange(Sender: TObject);
begin
     // VK-Preise neu berechnen, da der Gewinn ge�ndert wurde !
     if Update then exit;
     if not (ASQuery.State in [dsEdit, dsInsert]) then ASQuery.Edit;
     case tControl(Sender).Tag of
         -1:begin
                 ASQueryVK1.Value :=ASQueryCalc_EK.Value + vk1_rgw.value;
                 vk1_rgw.Font.Color :=clRed*ord (vk1_rgw.value<0);
                 VK1Change(VK1);
            end;
         -2:begin
                 ASQueryVK2.Value :=ASQueryCalc_EK.Value + vk2_rgw.value;
                 vk2_rgw.Font.Color :=clRed*ord (vk2_rgw.value<0);
                 VK1Change(VK2);
            end;
         -3:begin
                 ASQueryVK3.Value :=ASQueryCalc_EK.Value + vk3_rgw.value;
                 vk3_rgw.Font.Color :=clRed*ord (vk3_rgw.value<0);
                 VK1Change(VK3);
            end;
         -4:begin
                 ASQueryVK4.Value :=ASQueryCalc_EK.Value + vk4_rgw.value;
                 vk4_rgw.Font.Color :=clRed*ord (vk4_rgw.value<0);
                 VK1Change(VK4);
            end;
         -5:begin
                 ASQueryVK5.Value :=ASQueryCalc_EK.Value + vk5_rgw.value;
                 vk5_rgw.Font.Color :=clRed*ord (vk5_rgw.value<0);
                 VK1Change(VK5);
            end;
     end;
end;
//------------------------------------------------------------------------------
procedure TKasseArtikelForm.ekpreisChange(Sender: TObject);
begin
     // Gewinne neu kalkulieren !!!
     vk1Change(vk1);
     vk1Change(vk2);
     vk1Change(vk3);
     vk1Change(vk4);
     vk1Change(vk5);
end;
//------------------------------------------------------------------------------
procedure TKasseArtikelForm.MengeChange(Sender: TObject);
begin
     if ASQueryMenge_Akt.Value<0
      then Menge.Font.Color :=clRed
      else Menge.Font.Color :=clBlack;
end;
//------------------------------------------------------------------------------
procedure TKasseArtikelForm.ArtSuchGridTitleBtnClick(Sender: TObject;
  ACol: Integer; Field: TField);

var S : String; I : Integer;
begin
     S :=Field.FieldName;
     If Uppercase(S)='CALC_EK'
      then S :='EK_PREIS'
      else if (Pos('CALC_',Uppercase(S))=1) then delete (S,1,5);

     if SortField <>S then
     begin
       SortField :=S;
       SortName :=Field.DisplayLabel;
       UpdateQuery;
     end
        else
     begin
       SortField :=SortField + ' DESC';

       SortName :=Field.DisplayLabel;
       UpdateQuery;
     end;

     for i:=0 to ArtSuchGrid.Columns.Count-1 do
     begin
       if ArtSuchGrid.Columns[i].Field.DisplayLabel = SortName
        then ArtSuchGrid.Columns[i].Title.Font.Style :=[fsBold]
        else ArtSuchGrid.Columns[i].Title.Font.Style :=[];
     end;
     {
     if SortField <>Field.FieldName then
     begin
       SortField :=Field.FieldName;
       If Uppercase(SortField)='CALC_EK'
        then SortField :='EK_PREIS'
        else
          if (Pos('CALC_',Uppercase(SortField))=1)
            then delete (Sortfield,1,5);
       UpdateQuery;
     end;}
end;
//------------------------------------------------------------------------------
procedure TKasseArtikelForm.ASQueryBeforePost(DataSet: TDataSet);
var AKTO, EKTO : Integer;
begin
     // Wichtig :
     // Wenn eines dieser Felder den Focus hat, dann mu� der Focus zun�chst
     // auf ein anderes Felf (Suchbegriff) gelegt werden, damit die jeweilige
     // "OnExit" Routine ausgef�hrt wird bevor der DS gespeichert wird
     //
     // Im Ereignis AfterPost wird dann der Focus zur�ck auf das le. aktive
     // Feld gesetzt
     
     if (vk1.Focused)or(vk2.Focused)or(vk3.Focused)or(vk4.Focused)or
        (vk5.Focused)or(vk1_brutto.Focused)or(vk2_brutto.Focused)or
        (vk3_brutto.Focused)or(vk4_brutto.Focused)or(vk5_brutto.Focused)or
        (listpreis.Focused)or(BruttoPreis.Focused) then
     begin
        LastFocus :=getfocus; // Feld was den Focus hatte merken
        Suchbeg.SetFocus;     // Focus umlegen, f�hrt zu ausl�sung von OnExit
        Application.ProcessMessages;
     end;

     if LtextMemo.Focused then  LtextMemoExit(Self);

     ASQueryMATCHCODE.Value :=Uppercase(ASQueryMATCHCODE.Value);
     ASQueryARTNUM.Value :=Uppercase(ASQueryARTNUM.Value);
     ASQueryERSATZ_ARTNUM.Value :=Uppercase(ASQueryERSATZ_ARTNUM.Value);

     if ASQueryWarengruppe.Value=0 then
     begin
          If WGR=0 then
          begin
               MessageDlg ('Bitte erst Warengruppe zuweisen !',mterror,[mbok],0);
               Abort;
               Exit;
          end;
     end;

     if ASQuery.State=dsInsert then
     begin
       ASQueryERSTELLT.Value :=now;
       ASQueryERST_NAME.Value :=DM1.user;
     end;


     if ASQueryArtikelTyp.Value='T' then // Text
     begin
          ASQueryBarCode.AsString     :='';
          ASQueryME_Einheit.AsString  :='';
          ASQueryPR_Einheit.AsInteger :=0;
          ASQuerySteuer_Code.Value    :=0;
          ASQueryAufw_Kto.AsInteger   :=-1;
          ASQueryErloes_Kto.AsInteger :=-1;
          ASQueryInventur_Wert.Value  :=0;
          ASQueryGewicht.AsFloat      :=0;
          ASQueryMenge_Min.Value      :=0;
          ASQueryMenge_Akt.Value      :=0;
          ASQueryek_preis.Value       :=0;
          ASQueryvk1.Value            :=0;
          ASQueryvk2.Value            :=0;
          ASQueryvk3.Value            :=0;
          ASQueryvk4.Value            :=0;
          ASQueryvk5.Value            :=0;
          ASQueryDimension.Value      :='';
          ASQueryLaenge.Value         :='';
          ASQueryGroesse.Value        :='';

          ASQuerySN_FLAG.Value        :=False;

     end else
     if ASQueryArtikelTyp.Value='L' then // Lohn
     begin
          EKTO :=DM1.ReadInteger ('MAIN\ARTIKEL','DEF_ERLOESKTO',8400);
          AKTO :=DM1.ReadInteger ('MAIN\ARTIKEL','DEF_AUFWANDSKTO',3400);

          ASQueryBarCode.AsString :='';
          if (ASQueryAufw_Kto.Value   =0)or(ASQueryAufw_Kto.Value=AKTO)
           then ASQueryAufw_Kto.Value   :=DM1.ReadInteger ('MAIN\ARTIKEL',
                                                           'DEF_LOHN_AUFWANDSKTO',
                                                           3401);

          if (ASQueryErloes_Kto.Value =0)or(ASQueryErloes_Kto.Value=EKTO)
           then ASQueryErloes_Kto.Value :=DM1.ReadInteger ('MAIN\ARTIKEL',
                                                           'DEF_LOHN_ERLOESKTO',
                                                           8401);

          ASQueryInventur_Wert.Value :=0;
          ASQueryGewicht.AsFloat     :=0;
          ASQueryMenge_Min.Value     :=0;
          ASQueryDimension.Value     :='';
          ASQueryLaenge.Value        :='';
          ASQueryGroesse.Value       :='';

          ASQuerySN_FLAG.Value       :=False;
     end else
     if ASQueryARTIKELTYP.Value='S' then
     begin
          EKTO :=DM1.ReadInteger ('MAIN\ARTIKEL','DEF_ERLOESKTO',8400);
          AKTO :=DM1.ReadInteger ('MAIN\ARTIKEL','DEF_AUFWANDSKTO',3400);

          DM1.GetWGRDefaultKonten (WGR, EKTO, AKTO);

          ASQueryNo_Ek_Flag.Value :=True;
          if (ASQueryAufw_Kto.Value   <1) then ASQueryAufw_Kto.Value   :=AKTO;
          if (ASQueryErloes_Kto.Value <1) then ASQueryErloes_Kto.Value :=EKTO;
     end else
     begin
          EKTO :=DM1.ReadInteger ('MAIN\ARTIKEL','DEF_ERLOESKTO',8400);
          AKTO :=DM1.ReadInteger ('MAIN\ARTIKEL','DEF_AUFWANDSKTO',3400);

          DM1.GetWGRDefaultKonten (WGR, EKTO, AKTO);

          if (ASQueryAufw_Kto.Value   <1) then ASQueryAufw_Kto.Value   :=AKTO;
          if (ASQueryErloes_Kto.Value <1) then ASQueryErloes_Kto.Value :=EKTO;


          if (ArtnumAuto)and(length(ASQueryARTNUM.AsString)=0) then
          begin
             // Artikelnummer vergeben

             ASQueryARTNUM.AsString :=FormatFloat (DM1.GetNummerFormat(ARTNUM_KEY),
                                                   DM1.IncNummer(ARTNUM_KEY));
          end;

     end;

     // Kalkulation
     if ASQueryARTIKELTYP.Value[1] in ['N','S','L'] then
     begin
       if Vk1_faktor  > 0 then
       begin
         ASQueryVK1.AsFloat :=cao_round(ASQueryEK_PREIS.AsFloat *
                                        VK1_Faktor *100) / 100;

         ASQueryVK1B.AsFloat :=cao_round(ASQueryVK1.AsFloat *
                                   (100+DM1.MwStTab[ASQuerySTEUER_CODE.AsInteger])
                                  )/100;
       end;



       if Vk2_faktor  > 0 then
       begin
         ASQueryVK2.AsFloat :=cao_round(ASQueryEK_PREIS.AsFloat *
                                    VK2_Faktor *100) / 100;

         ASQueryVK2B.AsFloat :=cao_round(ASQueryVK2.AsFloat *
                                   (100+DM1.MwStTab[ASQuerySTEUER_CODE.AsInteger])
                                  )/100;
       end;



       if Vk3_faktor  > 0 then
       begin
         ASQueryVK3.AsFloat :=cao_round(ASQueryEK_PREIS.AsFloat *
                                    VK3_Faktor *100) / 100;

         ASQueryVK3B.AsFloat :=cao_round(ASQueryVK3.AsFloat *
                                   (100+DM1.MwStTab[ASQuerySTEUER_CODE.AsInteger])
                                  )/100;

       end;


       if Vk4_faktor  > 0 then
       begin
         ASQueryVK4.AsFloat :=cao_round(ASQueryEK_PREIS.AsFloat *
                                    VK4_Faktor *100) / 100;

         ASQueryVK4B.AsFloat :=cao_round(ASQueryVK4.AsFloat *
                                   (100+DM1.MwStTab[ASQuerySTEUER_CODE.AsInteger])
                                  )/100;
       end;



       if Vk5_faktor  > 0 then
       begin
         ASQueryVK5.AsFloat :=cao_round(ASQueryEK_PREIS.AsFloat *
                                    VK5_Faktor *100) / 100;

         ASQueryVK5B.AsFloat :=cao_round(ASQueryVK5.AsFloat *
                                   (100+DM1.MwStTab[ASQuerySTEUER_CODE.AsInteger])
                                  )/100;
       end;
     end;

     ASQueryGEAEND.Value :=now;
     ASQueryGEAEND_NAME.Value :=DM1.User;
end;
//------------------------------------------------------------------------------
procedure TKasseArtikelForm.AS_DSDataChange(Sender: TObject; Field: TField);
var C,C1:TColor;E,E1:Boolean;Dummy : Double;
begin
     if LastArtikelTyp <> ASQueryArtikelTyp.Value then
     begin
         LastArtikelTyp :=ASQueryArtikelTyp.Value;
         if LastArtikelTyp ='T' then C :=clBtnFace else C :=clWindow;
         c1 :=c;
         if LastArtikelTyp ='L' then C1 :=clBtnFace;
         E :=LastArtikelTyp<>'T';
         E1 :=E;
         if LastArtikelTyp ='L' then E1 :=False;
         A_KTO.Enabled           :=E;
         E_KTO.Enabled           :=E;
         MEinheit.Enabled        :=E;
         RabGrpLoCB.Enabled      :=E;


         //PEinheit.Enabled        :=E;
         MwstCB.Enabled          :=E;
         MinMenge.Enabled        :=E;
         Menge.Enabled           :=E and ME_EDIT;

         MEBestellt.Enabled      :=E and ME_EKBEST_EDIT;

         ME_Bestvorschlag.Enabled:=E1;
         Gewicht.Enabled         :=E1;
         Inventur_wert.Enabled   :=E1;
         Barcode.Enabled         :=E1;
         Lagerort.Enabled        :=E1;
         Land.Enabled            :=E1;
         Land_CB.Enabled         :=E1;
         NO_RABATT.Enabled       :=E;
         NO_PROV.Enabled         :=E;
         Laenge.Enabled          :=E1;
         Groesse.Enabled         :=E1;
         Dimension.Enabled       :=E1;
         SernoCB.Enabled         :=E1;
         //EK_Preis.Enabled        :=E;
         ListPreis.Enabled       :=E;
         BruttoPreis.Enabled     :=E;
         GewinnFaktor.Enabled    :=E;

         //-------------------------

         A_KTO.Color             :=C;
         E_KTO.Color             :=C;
         MEinheit.Color          :=C;
         RabGrpLoCB.Color        :=C;
         //PEinheit.Color          :=C;
         MwstCB.Color            :=C;
         MinMenge.Color          :=C;
         Menge.Color             :=C;
         //EK_Preis.Color          :=C;
         ListPreis.Color         :=C;
         BruttoPreis.Color       :=C;
         Gewicht.Color           :=C1;
         Inventur_wert.Color     :=C1;
         Barcode.Color           :=C1;
         Lagerort.Color          :=C1;
         Land.Color              :=C1;
         Land_CB.Color           :=C1;
         Laenge.Color            :=C1;
         Groesse.Color           :=C1;
         Dimension.Color         :=C1;
         ME_Bestvorschlag.Color  :=C1;
         MEBestellt.Color        :=C1;
         GewinnFaktor.Color      :=C;
     end;
     EK_Preis.Enabled :=(ASQueryRABGRP_ID.AsString='-')and
                        (ASQueryArtikelTyp.Value<>'T');

     if not EK_Preis.Enabled then EK_Preis.Color :=clBtnFace
                             else EK_Preis.Color :=clWindow;
     ekpreis.enabled :=EK_Preis.Enabled;
     ekpreis.color   :=EK_Preis.Color;

     Dummy :=1;
     vk1.Enabled :=(ASQueryArtikelTyp.Value<>'T')and
                   (
                    (ASQueryRABGRP_ID.AsString='-')or
                    (not DM1.CalcRabGrpPreis (ASQueryRABGRP_ID.AsString,1{VK1},Dummy))
                    )and(Vk1_faktor=0);
     Dummy :=1;
     vk2.Enabled :=(ASQueryArtikelTyp.Value<>'T')and
                   (
                    (ASQueryRABGRP_ID.AsString='-')or
                    (not DM1.CalcRabGrpPreis (ASQueryRABGRP_ID.AsString,2{VK2},Dummy))
                   )and(Vk2_faktor=0);
     Dummy :=1;
     vk3.Enabled :=(ASQueryArtikelTyp.Value<>'T')and
                   (
                    (ASQueryRABGRP_ID.AsString='-')or
                    (not DM1.CalcRabGrpPreis (ASQueryRABGRP_ID.AsString,3{VK3},Dummy))
                   )and(Vk3_faktor=0);
     Dummy :=1;
     vk4.Enabled :=(ASQueryArtikelTyp.Value<>'T')and
                   (
                    (ASQueryRABGRP_ID.AsString='-')or
                    (not DM1.CalcRabGrpPreis (ASQueryRABGRP_ID.AsString,4{VK4},Dummy))
                   )and(Vk4_faktor=0);

     vk5.Enabled :=(ASQueryArtikelTyp.Value<>'T')and
                   (Vk5_faktor=0);

     ListPreis.Enabled :=(ASQueryArtikelTyp.Value<>'T')and
                         (Vk5_faktor=0);

     BruttoPreis.Enabled :=(ASQueryArtikelTyp.Value<>'T')and
                           (Vk5_faktor=0);

     if VK1.Enabled then VK1.DataField :='VK1' else VK1.DataField :='CALC_VK1';
     vk1_brutto.enabled :=vk1.enabled; vk1_rgw.enabled :=vk1.enabled;

     if VK2.Enabled then VK2.DataField :='VK2' else VK2.DataField :='CALC_VK2';
     vk2_brutto.enabled :=vk2.enabled; vk2_rgw.enabled :=vk2.enabled;

     if VK3.Enabled then VK3.DataField :='VK3' else VK3.DataField :='CALC_VK3';
     vk3_brutto.enabled :=vk3.enabled; vk3_rgw.enabled :=vk3.enabled;

     if VK4.Enabled then VK4.DataField :='VK4' else VK4.DataField :='CALC_VK4';
     vk4_brutto.enabled :=vk4.enabled; vk4_rgw.enabled :=vk4.enabled;

     vk5_brutto.enabled :=vk5.enabled; vk5_rgw.enabled :=vk5.enabled;

     if VK1.Enabled then VK1_BRUTTO.DataField :='VK1B' else VK1_BRUTTO.DataField :='CALC_VK1B';
     if VK2.Enabled then VK2_BRUTTO.DataField :='VK2B' else VK2_BRUTTO.DataField :='CALC_VK2B';
     if VK3.Enabled then VK3_BRUTTO.DataField :='VK3B' else VK3_BRUTTO.DataField :='CALC_VK3B';
     if VK4.Enabled then VK4_BRUTTO.DataField :='VK4B' else VK4_BRUTTO.DataField :='CALC_VK4B';

     gewinnfaktor.enabled :=ekpreis.enabled;
     gewinnfaktor.color   :=ekpreis.color;

     if (ASQueryRABGRP_ID.AsString='-')
       then EK_Preis.DataField :='EK_PREIS'
       else EK_Preis.DataField :='CALC_EK';

     ekpreis.DataField :=ek_preis.DataField;

     if ASQueryRec_ID.Value <> LastID then ASQueryAfterScroll (nil);
end;
//------------------------------------------------------------------------------
procedure TKasseArtikelForm.ArtAllgemeinBtnClick(Sender: TObject);
begin
     if sender is tSpeedButton
      then  Art_PC.ActivePage :=Art_PC.Pages[tSpeedButton(sender).Tag-1]
      else if sender is tMenuItem
             then Art_PC.ActivePage :=Art_PC.Pages[tMenuItem(sender).Tag-1];

     Art_PCChange (Sender);
end;
//------------------------------------------------------------------------------
procedure TKasseArtikelForm.SpeedButton1Click(Sender: TObject);
begin
     if not ArtWGRPan.Visible then
     begin
          ArtWGrPan.Visible :=True;
          try if Assigned(Sender) then ArtWgrTV.SetFocus; except end;
     end else ArtWGRPan.Hide;
end;
//------------------------------------------------------------------------------
procedure TKasseArtikelForm.ArtWgrTVDblClick(Sender: TObject);
begin
     if not ArtWgrDok then ArtWgrPan.Hide;
end;
//------------------------------------------------------------------------------
procedure TKasseArtikelForm.ArtWgrTVExit(Sender: TObject);
begin
     if not ArtWgrDok then ArtWgrPan.Hide;
end;
//------------------------------------------------------------------------------
procedure TKasseArtikelForm.ArtHirDockBtnClick(Sender: TObject);
begin
     if not ArtWgrDok then
     begin
          ArtWgrDok :=True;
          Art_PC.Align :=Alclient;
     end
        else
     begin
          ArtWgrDok :=False;
          ArtWgrPan.Hide;
          Art_PC.Align :=alNone;
          ArtikelPanelResize(Sender);
     end;
     DM1.WriteBoolean ('USERSETTINGS\'+DM1.User,'KAS_ARTIKEL_HIR_DOK',ArtWgrDok);
end;
//------------------------------------------------------------------------------
procedure TKasseArtikelForm.ArtikelPanelResize(Sender: TObject);
begin
     if Art_PC.Align = alNone then
     begin
          Art_PC.Left :=0;
          Art_PC.Top :=ArtPan.Height;
          Art_PC.width :=ArtikelPanel.ClientWidth;

          if SB1.Visible
            then Art_PC.Height :=ArtikelPanel.ClientHeight -
                                 ArtPan.Height -
                                 ArtikelToolbar1.Height -
                                 sb1.height
            else Art_PC.Height :=ArtikelPanel.ClientHeight -
                                 ArtPan.Height -
                                 ArtikelToolbar1.Height;
     end;
     if ArtWgrPan.Visible then
     begin
        // ca. 22% der Gesamtbreite
        ArtWgrPan.Width :=cao_Round(ArtikelPanel.Width * 0.22);
        ArtHirDockBtn.Left :=Panel1.Width - ArtHirDockBtn.Width - 3;
     end;
end;
//------------------------------------------------------------------------------
procedure TKasseArtikelForm.ArtWgrTVChange(Sender: TObject; Node: TTreeNode);
var Last  : String;
    N    : tTreeNode;
begin
     if First then exit;

     Last :=Artikel_SQL;

     // obersten Node ermitteln
     n :=Node;
     repeat
       if assigned(N.Parent) then N :=N.Parent else break;
     until (not assigned(N))or(not assigned(N.Parent));

     if (assigned(N))and(N.Text='alle Warengruppen') then
     begin
       Wgr :=Integer(Node.Data);
       if not DM1.WgrTab.Active then DM1.WgrTab.Open;
       if DM1.WgrTab.Locate ('ID',variant (wgr),[])
        then Artikel_SQL :='WARENGRUPPE='+IntToStr(wgr)
        else Artikel_SQL :='';
     end
     else
     if (assigned(N))and(N.Text='Spezial') then
     begin
        // Spezialauswahl laden
          DM1.Uniquery.Close;
          DM1.UniQuery.Sql.Text :='select VAL_INT AS ID, NAME, VAL_BLOB '+
                                  'from REGISTERY where '+
                                  'MAINKEY="MAIN\\ART_HIR" '+
                                  'and VAL_INT='+IntToStr(Integer(Node.Data));
          DM1.UniQuery.Open;
          if DM1.UniQuery.RecordCount=1
           then Artikel_SQL :=DM1.UniQuery.FieldByName ('VAL_BLOB').AsString;
          DM1.UniQuery.Close;
     end
     else Artikel_SQL :='';
     
     if Last <> Artikel_SQL then UpdateQuery;
end;
//------------------------------------------------------------------------------
procedure TKasseArtikelForm.SuchbegChange(Sender: TObject);
begin
     searchtime :=15;
     searchtimer.enabled :=true;
end;
//------------------------------------------------------------------------------
procedure TKasseArtikelForm.SearchTimerTimer(Sender: TObject);
begin
     if SuchBeg.Text = '%' then exit;
     if searchtime > 0 then dec (searchtime) else
     begin
         searchtimer.enabled :=false;

         SuchBegr :=Suchbeg.Text;
         UpdateQuery;
     end;
end;
//------------------------------------------------------------------------------
procedure TKasseArtikelForm.Layoutspeichern1Click(Sender: TObject);
begin
     dm1.GridSaveLayout (tDBGrid(SerNoGrid),'KAS_ARTIKEL_SERNO');
     dm1.GridSaveLayout (tDBGrid(ArtSuchGrid),'KAS_ARTIKEL_LISTE',101);
end;
//------------------------------------------------------------------------------
procedure TKasseArtikelForm.SuchFeldCBChange(Sender: TObject);
begin
     if SuchBeg.Text<>'' then
     begin
       searchtime :=15;
       searchtimer.enabled :=true;
     end;
end;
//------------------------------------------------------------------------------
procedure TKasseArtikelForm.Aktualisieren1Click(Sender: TObject);
begin
     ASQuery.Refresh;
end;
//------------------------------------------------------------------------------
procedure TKasseArtikelForm.SichtbareSpalten1Click(Sender: TObject);
begin
     VisibleSpaltenForm.UpdateTable (tDBGrid(ArtSuchGrid));
end;
//------------------------------------------------------------------------------
procedure TKasseArtikelForm.Neu1Click(Sender: TObject);
begin
     ASQuery.Append;
end;
//------------------------------------------------------------------------------
procedure TKasseArtikelForm.Lschen1Click(Sender: TObject);
begin
     IF MessageDlg ('Wollen Sie diesen Artikel wirklich l�schen ?',
                    mtconfirmation,mbyesnocancel,0)=mryes then ASQuery.Delete;
end;
//------------------------------------------------------------------------------
procedure TKasseArtikelForm.aname1Exit(Sender: TObject);
begin
     MatchExit(Sender);

     if (length(ASQueryKURZNAME.Value)=0) or
        (ASQueryKas_Name.Value = ASQueryKURZNAME.Value) or
        (length(ASQueryKURZNAME.Value)=0) then exit;


     if not (ASQuery.State in [dsEdit, dsInsert]) then ASQuery.Edit;
     if ASQueryKas_Name.Value=''
        then ASQueryKas_Name.Value := ASQueryKURZNAME.Value;

     if K2L then
     begin
       if ASqueryLANGNAME.AsString=''
        then ASqueryLANGNAME.AsString :=ASQueryKURZNAME.Value;
     end;
end;
//------------------------------------------------------------------------------
procedure TKasseArtikelForm.ASQueryBeforeDelete(DataSet: TDataSet);
begin
     // 1. Artikel im Journal aktualisieren ( auf Freien Artikel setzten )
     with dm1.UniQuery do
     begin
       sql.clear;
       sql.add ('Update JOURNALPOS');
       sql.Add ('Set ARTIKELTYP="F", ARTIKEL_ID="-1"');
       sql.Add ('Where ARTIKEL_ID='+IntToStr(ASQueryRec_ID.Value));
       execsql;
     end;

     // 2. Langtexte l�schen
     with dm1.UniQuery do
     begin
       sql.clear;
       sql.add ('delete from ARTIKEL_LTEXT');
       sql.Add ('Where ARTIKEL_ID='+IntToStr(ASQueryRec_ID.Value));
       execsql;
     end;

     // 3. Seriennummern l�schen
     with dm1.UniQuery do
     begin
       sql.clear;
       sql.add ('delete from ARTIKEL_SERNUM');
       sql.Add ('Where ARTIKEL_ID='+IntToStr(ASQueryRec_ID.Value));
       execsql;
     end;

     // 4. St�cklisten l�schen
     with dm1.UniQuery do
     begin
       sql.clear;
       sql.add ('delete from ARTIKEL_STUECKLIST');
       sql.Add ('Where REC_ID='+IntToStr(ASQueryRec_ID.Value));
       sql.Add ('or ART_ID='+IntToStr(ASQueryRec_ID.Value));
       execsql;
     end;

     // 5. ArtikelToKatalog l�schen
     with dm1.UniQuery do
     begin
       sql.clear;
       sql.add ('delete from ARTIKEL_TO_KAT');
       sql.Add ('Where ARTIKEL_ID='+IntToStr(ASQueryRec_ID.Value));
       execsql;
     end;

     // 6. ArtikelPreise l�schen
     with dm1.UniQuery do
     begin
       sql.clear;
       sql.add ('delete from ARTIKEL_PREIS');
       sql.Add ('Where ARTIKEL_ID='+IntToStr(ASQueryRec_ID.Value));
       execsql;
     end;
end;
//------------------------------------------------------------------------------
procedure TKasseArtikelForm.LtextMemoExit(Sender: TObject);
begin
     if ((ASQueryKas_Name.Value='')or(ASQueryKurzName.Value=''))and
        (not (ASQuery.State in [dsEdit, dsInsert]))and
        (length(LtextMemo.Lines[0])>0) then ASQuery.Edit;

     if ASQueryKas_Name.Value=''
      then ASQueryKas_Name.Value :=LtextMemo.Lines[0];
     if ASQueryKurzName.Value=''
      then ASQueryKurzName.Value :=LtextMemo.Lines[0];

     MatchExit (Sender);
end;
//------------------------------------------------------------------------------
procedure TKasseArtikelForm.Suchen1Click(Sender: TObject);
begin
     try
        Suchbeg.SetFocus;
     except end;
end;
//------------------------------------------------------------------------------
procedure TKasseArtikelForm.alle1Click(Sender: TObject);
begin
     Limit :=tMenuItem(Sender).Tag;
     tMenuItem(Sender).Checked :=True;
     DM1.WriteInteger ('USERSETTINGS\'+DM1.User,'KAS_ARTIKEL_TREFFER',Limit);
     UpdateQuery;
end;
//------------------------------------------------------------------------------
procedure TKasseArtikelForm.UpdateStatus;
var SuchZeit, Datensatz, Sortierung, Erstellt, Geaendert : String;
begin
     if ASQuery.RecordCount=0 then Datensatz :='keine'
     else if ASQuery.RecordCount=Limit
            then Datensatz :=inttostr (ASQuery.RecNo)+
                             ' von >='+
                             inttostr (ASQuery.RecordCount)
     else Datensatz :=inttostr (ASQuery.RecNo)+
                      ' von '+
                      inttostr (ASQuery.RecordCount);

     Sortierung :='Sortierung : '+sortname;

     if (asquery.active)and(asquery.recordcount>0) then
     begin
          Erstellt :='Erstellt:'+
                     formatdatetime ('dd.mm.yyyy',ASQueryErstellt.Value);

          if ASQueryGeaend.Value>1
           then Geaendert :='Ge�ndert:'+
                            formatdatetime ('dd.mm.yyyy',ASQueryGeaend.Value)
           else Geaendert :='-';
     end
        else
     begin
          Erstellt :='';
          Geaendert :='';
     end;

     SuchZeit :=FormatFloat ('0.00',(querytime)/1000)+' Sek.';

     if assigned (OnUpdateStatusBar) then
     begin
         SB1.Visible :=False;
         OnUpdateStatusBar (SuchZeit, Datensatz, Sortierung, Erstellt, Geaendert);
     end
        else
     begin
         SB1.Visible :=True;

         sb1.panels[0].Text :=SuchZeit;
         sb1.panels[1].Text :=Datensatz;
         SB1.Panels[2].Text :=Sortierung;
         Sb1.Panels[3].Text :=Erstellt;
         Sb1.Panels[4].Text :=Geaendert;
     end;
end;
//------------------------------------------------------------------------------
procedure TKasseArtikelForm.SuchbegKeyPress(Sender: TObject; var Key: Char);
begin
     if Key=#13 then
     begin
        Key :=#0;
        try
          if Art_PC.ActivePage=Allg_TS then Match.SetFocus else
          if Art_PC.ActivePage=Such_TS then ArtSuchGrid.SetFocus;
        except end;
     end
     else matchKeyPress (Sender,Key);
end;
//------------------------------------------------------------------------------
procedure TKasseArtikelForm.Kopieren1Click(Sender: TObject);
var data : array of Variant;
    i    : Integer;
begin
     if ASquery.State in [dsEdit,dsInsert] then ASQuery.Post;

     // Artikel kopieren
     setlength (data, ASQuery.Fields.Count);
     for i:=0 to ASQuery.Fields.Count-1 do data[i] :=ASQuery.Fields[i].Value;
     ASQuery.Append;

     for i:=0 to ASQuery.Fields.Count-1 do
      if (Uppercase(ASQuery.Fields[i].FieldName)<>'REC_ID')and
         (Uppercase(ASQuery.Fields[i].FieldName)<>'SHOP_ID')and
         (Uppercase(ASQuery.Fields[i].FieldName)<>'SHOP_ARTIKEL_ID')and
         (Uppercase(ASQuery.Fields[i].FieldName)<>'SHOP_CHANGE_FLAG')and
         (Uppercase(ASQuery.Fields[i].FieldName)<>'SHOP_DEL_FLAG')and
         (Uppercase(ASQuery.Fields[i].FieldName)<>'SHOP_VISIBLE')
       then ASQuery.Fields[i].Value :=data[i];

     ASqueryMenge_Akt.Value :=0;

     SetLength(Data,0);
end;
//------------------------------------------------------------------------------
procedure TKasseArtikelForm.matchEnter(Sender: TObject);
begin
     tDBEdit(sender).Color :=$009FFF9F;
end;
//------------------------------------------------------------------------------
procedure TKasseArtikelForm.matchExit(Sender: TObject);
begin
     tDBEdit(sender).Color :=clWindow;
end;
//------------------------------------------------------------------------------
procedure TKasseArtikelForm.EKPreiseanzeigen1Click(Sender: TObject);
begin
     EKPreiseanzeigen1.Checked :=not EKPreiseanzeigen1.Checked;
     F9Change (EKPreiseanzeigen1.Checked);
end;
//------------------------------------------------------------------------------
procedure TKasseArtikelForm.ASQueryNewRecord(DataSet: TDataSet);
begin
     ASQueryMenge_Akt.Value      :=0;
     ASQueryMenge_Min.Value      :=0;
     ASQueryMenge_BVor.Value     :=0;
     ASQueryMENGE_BESTELLT.Value :=0;
     ASQueryMENGE_START.Value    :=0;
     ASQueryAUTODEL_Flag.Value   :=False;
     ASQueryEK_PREIS.Value       :=0;
     ASQueryVK1.Value            :=0;
     ASQueryVK2.Value            :=0;
     ASQueryVK3.Value            :=0;
     ASQueryVK4.Value            :=0;
     ASQueryVK5.Value            :=0;
     ASQuerySTEUER_CODE.Value    :=2;
     ASQueryWarengruppe.Value    :=WGR;
end;
//------------------------------------------------------------------------------
procedure TKasseArtikelForm.ASQueryBeforeInsert(DataSet: TDataSet);
begin
     try
        if not Art_PC.Enabled then Art_PC.Enabled :=True;

        ART_PC.ActivePage :=Allg_TS;
        Art_PCChange (Self);
        Match.SetFocus;
     except end;
end;
//------------------------------------------------------------------------------
procedure TKasseArtikelForm.Schlieen1Click(Sender: TObject);
begin
     Close;
end;
//------------------------------------------------------------------------------
procedure TKasseArtikelForm.Artikelliste1Click(Sender: TObject);
begin
end;
//------------------------------------------------------------------------------
procedure TKasseArtikelForm.UpdateGewinnFaktor;
var VK,EK : Double;
begin
    if InFaktorUpdate then exit;
    InFaktorUpdate :=True;
    try
      //EK :=ASQueryEK_PREIS.Value;
      EK :=ASQuery.FieldByName (EK_Preis.DataField).AsFloat;
      if (GewinnFaktor.Enabled)or(EK_Preis.DataField='CALC_EK') then
      begin                   {EK * VK / 100}
          case KMainForm.PEbene of
                 1: VK :=ASQueryVK1.Value;
                 2: VK :=ASQueryVK2.Value;
                 3: VK :=ASQueryVK3.Value;
                 4: VK :=ASQueryVK4.Value;
               else VK :=ASQueryVK5.Value;
          end;

          if EK <>0 then GewinnFaktor.Value :=((VK-EK) / EK)+1
                    else GewinnFaktor.Value :=0;
      end else GewinnFaktor.Value :=0;

      if (GewinnFaktor.Value<>0)and(GewinnFaktor.Value<1.03)
        then GewinnFaktor.Font.Color :=clRed
        else GewinnFaktor.Font.Color :=clBlack;
    finally
      InFaktorUpdate :=False;
    end;
end;
//------------------------------------------------------------------------------
procedure TKasseArtikelForm.EK_PreisChange(Sender: TObject);
begin
     UpdateGewinnFaktor; ASQueryCalcFields(nil);
end;
//------------------------------------------------------------------------------
procedure TKasseArtikelForm.GewinnFaktorChange(Sender: TObject);
begin
     if InFaktorUpdate then exit;
     InFaktorUpdate :=True;
     try
       if not (ASQuery.State in [dsEdit, dsInsert]) then ASQuery.Edit;
       case KMainForm.PEbene of
           1: begin ASQueryVK1.Value :=ASQueryEK_Preis.Value * GewinnFaktor.Value; vk1Change (vk1); end;
           2: begin ASQueryVK2.Value :=ASQueryEK_Preis.Value * GewinnFaktor.Value; vk1Change (vk2); end;
           3: begin ASQueryVK3.Value :=ASQueryEK_Preis.Value * GewinnFaktor.Value; vk1Change (vk3); end;
           4: begin ASQueryVK4.Value :=ASQueryEK_Preis.Value * GewinnFaktor.Value; vk1Change (vk4); end;
         else begin ASQueryVK5.Value :=ASQueryEK_Preis.Value * GewinnFaktor.Value; vk1Change (vk5); end;
       end;
     finally
       InFaktorUpdate :=False;
    end;
end;
//------------------------------------------------------------------------------
procedure TKasseArtikelForm.ArtAllgemeinBtnMouseEnter(Sender: TObject);
begin
     if (sender is tJvSpeedButton)and
        (tJvSpeedButton(Sender).Enabled) then
     begin
       tJvSpeedButton(Sender).Transparent :=False;
       tJvSpeedButton(Sender).Font.Color :=clBlack;
     end;
end;
//------------------------------------------------------------------------------
procedure TKasseArtikelForm.ArtAllgemeinBtnMouseLeave(Sender: TObject);
begin
     if (sender is tJvSpeedButton) then
     begin

       tJvSpeedButton(Sender).Transparent :=True;
       tJvSpeedButton(Sender).Font.Color :=clWhite;
     end;
end;
//------------------------------------------------------------------------------
procedure TKasseArtikelForm.ASQueryCalcFields(DataSet: TDataSet);
var P, PR, B, M : Double;
begin
     if (ASQuerySTEUER_CODE.AsInteger>=0)and(ASQuerySTEUER_CODE.AsInteger<4)
       then M :=DM1.MwStTab[ASQuerySTEUER_CODE.AsInteger]+100 else M :=100;

     if ASQueryRABGRP_ID.AsString<>'-' then
     begin
        case AnzVKPreis of
                 1: PR :=ASQueryVK1.Value;
                 2: PR :=ASQueryVK2.Value;
                 3: PR :=ASQueryVK3.Value;
                 4: PR :=ASQueryVK4.Value;
               else PR :=ASQueryVK5.Value;
        end;
        P :=PR;
        DM1.CalcRabGrpPreis (ASQueryRABGRP_ID.AsString,0{EK},P);
        ASQueryCalc_EK.Value := P;

        P :=PR;
        if DM1.CalcRabGrpPreis (ASQueryRABGRP_ID.AsString,1{VK1},P) then
        begin
           ASQueryCalc_VK1.Value :=P;
           B :=cao_round(P*M); // jetzt ganze Cent
           B :=cao_Round (B / DM1.BR_RUND_WERT) * DM1.BR_RUND_WERT / 100;
           ASQueryCALC_VK1B.Value :=B;
        end
         else
        begin
           ASQueryCalc_VK1.Value :=ASQueryVK1.Value;
           ASQueryCalc_VK1B.Value :=ASQueryVK1B.Value;
        end;
        P :=PR;
        if DM1.CalcRabGrpPreis (ASQueryRABGRP_ID.AsString,2{VK2},P) then
        begin
           ASQueryCalc_VK2.Value :=P;
           B :=cao_round(P*M); // jetzt ganze Cent
           B :=cao_Round (B / DM1.BR_RUND_WERT) * DM1.BR_RUND_WERT / 100;
           ASQueryCALC_VK2B.Value :=B;
        end
         else
        begin
           ASQueryCalc_VK2.Value :=ASQueryVK2.Value;
           ASQueryCalc_VK2B.Value :=ASQueryVK2B.Value;
        end;
        P :=PR;
        if DM1.CalcRabGrpPreis (ASQueryRABGRP_ID.AsString,3{VK3},P) then
        begin
           ASQueryCalc_VK3.Value :=P;
           B :=cao_round(P*M); // jetzt ganze Cent
           B :=cao_Round (B / DM1.BR_RUND_WERT) * DM1.BR_RUND_WERT / 100;
           ASQueryCALC_VK3B.Value :=B;
        end
         else
        begin
           ASQueryCalc_VK3.Value :=ASQueryVK3.Value;
           ASQueryCalc_VK3B.Value :=ASQueryVK3B.Value;
        end;
        P :=PR;
        if DM1.CalcRabGrpPreis (ASQueryRABGRP_ID.AsString,4{VK4},P) then
        begin
           ASQueryCalc_VK4.Value :=P;
           B :=cao_round(P*M); // jetzt ganze Cent
           B :=cao_Round (B / DM1.BR_RUND_WERT) * DM1.BR_RUND_WERT / 100;
           ASQueryCALC_VK4B.Value :=B;
        end
         else
        begin
           ASQueryCalc_VK4.Value :=ASQueryVK4.Value;
           ASQueryCalc_VK4B.Value :=ASQueryVK4B.Value;
        end;
     end
        else
     begin
       ASQueryCalc_EK.Value :=ASQueryEK_Preis.Value;

       ASQueryCalc_VK1.Value :=ASQueryVK1.Value;
       ASQueryCalc_VK2.Value :=ASQueryVK2.Value;
       ASQueryCalc_VK3.Value :=ASQueryVK3.Value;
       ASQueryCalc_VK4.Value :=ASQueryVK4.Value;

       ASQueryCalc_VK1B.Value :=ASQueryVK1B.Value;
       ASQueryCalc_VK2B.Value :=ASQueryVK2B.Value;
       ASQueryCalc_VK3B.Value :=ASQueryVK3B.Value;
       ASQueryCalc_VK4B.Value :=ASQueryVK4B.Value;
     end;
end;
//------------------------------------------------------------------------------
procedure TKasseArtikelForm.ArtikelnrChange(Sender: TObject);
begin
     if (length(Artikelnr.Text)=0) or
        (Art_PC.ActivePage<>Allg_TS) then exit;

     dm1.uniquery.close;
     dm1.uniquery.sql.text :='SELECT REC_ID FROM ARTIKEL WHERE UPPER(ARTNUM)="'+
                             Artikelnr.Text+'" and REC_ID!='+
                             IntToStr(ASQueryREC_ID.AsInteger);
     dm1.uniquery.open;

     if dm1.uniquery.recordcount>0
       then Artikelnr.Color :=clRed
       else
     if Artikelnr.Focused
       then Artikelnr.Color :=$009FFF9F
       else Artikelnr.Color :=clWindow;

     dm1.uniquery.close;
end;
//------------------------------------------------------------------------------
procedure TKasseArtikelForm.ASQueryUpdateRecord(DataSet: TDataSet;
  UpdateKind: TUpdateKind; var UpdateAction: TUpdateAction);

var i : integer; first : boolean; SQL : tStrings;
begin
     if UpdateKind in [ukModify,ukInsert] then
     begin
       // SQL best�cken

       case UpdateKind of
          ukModify : SQL := ArtikelUpdateSQL.ModifySql;
          ukInsert : SQL := ArtikelUpdateSQL.InsertSql;
       end;

       First :=True;

       SQL.Clear;

       if UpdateKind = ukModify
        then SQL.Add ('UPDATE ARTIKEL SET')
        else SQL.Add ('INSERT INTO ARTIKEL SET');

       for i:=0 to DataSet.FieldCount-1 do
       begin
          if (DataSet.Fields[i].CanModify) and
             (not DataSet.Fields[i].Calculated) and
             ((DataSet.Fields[i].CurValue<>DataSet.Fields[i].OldValue) or
             ((DataSet.Fields[i].IsBlob)and(tBlobField(DataSet.Fields[i]).Modified))or
             ((UpdateKind=ukInsert)and(DataSet.Fields[i].FieldName<>'REC_ID'))
             ) then
          begin
            if First then First :=False else SQL.Add (', ');

            SQL.Add (DataSet.Fields[i].FieldName+' =:'+
                     DataSet.Fields[i].FieldName);
          end;
       end;
       if UpdateKind = ukModify then SQL.Add ('where REC_ID=:REC_ID');
       if First then
       begin
          SQL.Clear;
          UpdateAction :=uaAbort;
       end;
     end;
end;
//------------------------------------------------------------------------------
procedure TKasseArtikelForm.SpeedButton2Click(Sender: TObject);
begin
end;
//------------------------------------------------------------------------------
procedure TKasseArtikelForm.ASQueryPostError(DataSet: TDataSet;
  E: EDatabaseError; var Action: TDataAction);
begin
     Action :=daAbort;
     ASQuery.Cancel;
end;
//------------------------------------------------------------------------------
procedure TKasseArtikelForm.MwstCBChange(Sender: TObject);
var i:integer;
begin
     // Bruttopreis neu berechnen
     for i:=1 to AnzVKPreis do
     begin
       case i of
          1: vk1change(vk1);
          2: vk1change(vk2);
          3: vk1change(vk3);
          4: vk1change(vk4);
          5: vk1change(vk5);
       end;
     end;
end;
//------------------------------------------------------------------------------
procedure TKasseArtikelForm.uebernSchliessen1Click(Sender: TObject);
begin
     UebernahmeBtnClick(Sender);
     Close;
end;
//------------------------------------------------------------------------------
end.
