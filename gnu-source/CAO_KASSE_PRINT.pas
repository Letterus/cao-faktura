{
CAO-Faktura f�r Windows Version 1.0
Copyright (C) 2003 Jan Pokrandt / Jan@JP-SOFT.de

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.


******* CAO-Faktura comes with ABSOLUTELY NO WARRANTY ***************


Programm     : CAO-Faktura
Modul        : CAO_KASSE_PRINT
Stand        : 05.07.2003
Version      : 1.1.1.3
Beschreibung : Druckausgabe Beleg

History :

06.05.2003 - Version 1.0.0.54 released Jan Pokrandt
}

unit CAO_KASSE_PRINT;

{$I CAO32.INC}

{$IFNDEF REPORTBUILDER}
DIESE U_N_I_T D�RFTE OHNE DIESES DEFINE NICHT AUFGERUFEN WERDEN,
BITTE ENTFERNEN SIE DIE U_N_I_T AUS ALLEN U_S_E_S KLAUSELN DES PROJEKTES
{$ENDIF}

interface

uses
  Db, ppDBJIT, ppCache, ppClass,  ppBands, ppProd, ppReport, ppEndUsr, ppDB,
  ppComm, ppRelatv, ppDBPipe,  ZQuery, ZMySqlQuery, StdCtrls, JvSpin, Buttons,
  Controls, JvLookup, Classes, Windows, Messages, SysUtils, Graphics, Forms,
  Dialogs, ppPrnabl, ppStrtch, ppSubRpt, ppDevice, ppPrnDev,  ppCtrls,
  JvComponent, JvMail, JvEdit, Mask, JvMaskEdit, ppViewr;

type
  TPrintBonForm = class(TForm)
    Label1: TLabel;
    Label2: TLabel;
    FormularCB: TComboBox;
    ZielCB: TComboBox;
    BinNamCB: TComboBox;
    AnzCopy: TJvSpinEdit;
    VorschauBtn: TBitBtn;
    PrintBtn: TBitBtn;
    DruEinrBtn: TBitBtn;
    CloseBtn: TBitBtn;
    ReportTab: TZMySqlQuery;
    ReportTabMAINKEY: TStringField;
    ReportTabNAME: TStringField;
    ReportTabVAL_BLOB: TMemoField;
    ReportTabVAL_CHAR: TStringField;
    ReportTabVAL_BIN: TBlobField;
    ReportDS: TDataSource;
    RepPipeline: TppDBPipeline;
    RechReport: TppReport;
    ppDetailBand1: TppDetailBand;
    Label7: TLabel;
    Label8: TLabel;
    ReportTabVAL_TYP: TIntegerField;
    FirmaTabPipeline: TppDBPipeline;
    BonTab: TZMySqlQuery;
    BonDS: TDataSource;
    BonPipeline: TppDBPipeline;
    BonPosPipeline: TppDBPipeline;
    BonPosDS: TDataSource;
    BonPosTab: TZMySqlQuery;
    BonTabBELEGNUMMER: TIntegerField;
    BonTabBELEGDATUM: TDateField;
    BonTabZAHLART: TIntegerField;
    BonTabMWST_0: TFloatField;
    BonTabMWST_1: TFloatField;
    BonTabMWST_2: TFloatField;
    BonTabMWST_3: TFloatField;
    BonTabNSUMME: TFloatField;
    BonTabMSUMME_0: TFloatField;
    BonTabMSUMME_1: TFloatField;
    BonTabMSUMME_2: TFloatField;
    BonTabMSUMME_3: TFloatField;
    BonTabBSUMME: TFloatField;
    BonTabWAEHRUNG: TStringField;
    BonTabBEDIENER: TStringField;
    BonTabZAHLART_KURZ: TStringField;
    BonPosTabMATCHCODE: TStringField;
    BonPosTabARTNUM: TStringField;
    BonPosTabBARCODE: TStringField;
    BonPosTabMENGE: TFloatField;
    BonPosTabLAENGE: TStringField;
    BonPosTabGROESSE: TStringField;
    BonPosTabDIMENSION: TStringField;
    BonPosTabGEWICHT: TFloatField;
    BonPosTabME_EINHEIT: TStringField;
    BonPosTabPR_EINHEIT: TFloatField;
    BonPosTabEPREIS: TFloatField;
    BonPosTabRABATT: TFloatField;
    BonPosTabRABATT2: TFloatField;
    BonPosTabRABATT3: TFloatField;
    BonPosTabSTEUER_CODE: TIntegerField;
    BonPosTabBEZEICHNUNG: TMemoField;
    BonPosTabGPREIS: TFloatField;
    BonPosTabERPREIS: TFloatField;
    BonPosTabEPREIS_B: TFloatField;
    BonPosTabERPREIS_B: TFloatField;
    BonPosTabGPREIS_B: TFloatField;
    BonPosTabMWST_BETRAG: TFloatField;
    BonPosTabMWSTPROZ: TFloatField;
    BonTabREC_ID: TIntegerField;


    procedure CloseBtnClick(Sender: TObject);
    procedure FormularCBChange(Sender: TObject);
    procedure ZielCBChange(Sender: TObject);
    procedure PrintBtnClick(Sender: TObject);
    procedure RechReportPreviewFormCreate(Sender: TObject);
    procedure VorschauBtnClick(Sender: TObject);
    procedure AnzCopyChange(Sender: TObject);
    procedure DruEinrBtnClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure BonTabCalcFields(DataSet: TDataSet);
    procedure BonPosTabCalcFields(DataSet: TDataSet);
  private
    { Private-Deklarationen }
    MainKey  : String;
  public
    { Public-Deklarationen }
    procedure ShowDlg (Beleg : Integer; Show : Boolean; Formular, Drucker : String);
  end;

var
  PrintBonForm: TPrintBonForm;

implementation

{$R *.DFM}

uses cao_dm, cao_var_const, cao_tool1, ppTypes, ppPrintr;
//------------------------------------------------------------------------------
procedure TPrintBonForm.FormCreate(Sender: TObject);
begin
     height :=162;
end;
//------------------------------------------------------------------------------
procedure TPrintBonForm.ShowDlg (Beleg : Integer; Show : Boolean; Formular, Drucker : String);
var Res : Boolean; i:integer;
begin
     MainKey :='MAIN\REPORT\KASSENBON';
     RechReport.DataPipeline :=BonPipeline;
     if not DM1.ZahlartTab.Active
       then DM1.ZahlartTab.Open
       else DM1.ZahlartTab.Refresh;
     BonTab.Close;
     BonPosTab.Close;
     BonTab.ParamByName ('VRENUM').Value :=Beleg;
     BonTab.ParamByName ('QUELLE').Value :=VK_RECH;
     BonTab.ParamByName ('QUELLE_SUB').Value :=2;
     BonTab.Open;
     BonPosTab.ParamByName ('REC_ID').Value :=BonTab.FieldByName ('REC_ID').Value;
     BonPosTab.Open;

     ReportTab.Close;
     ReportTab.ParamByName('KEY').Value :=MainKey;
     ReportTab.Open;

     FormularCB.Items.Clear;
     while not ReportTab.Eof do
     begin
        FormularCB.Items.Add (ReportTabName.AsString);
        ReportTab.Next;
     end;

     if ReportTab.Locate ('NAME',Formular,[])
      then FormularCB.ItemIndex :=FormularCB.Items.IndexOf (Formular)
      else
     if ReportTab.RecordCount>0 then
     begin
       ReportTab.First;
       FormularCB.ItemIndex :=0;//Text :=ReportTabName.AsString;
     end
     else FormularCB.ItemIndex :=-1;

     FormularCBChange(Self);

     // Eintrag "Screen" aus der Drop-Down-Box l�schen
     ZielCB.Items.Assign (RechReport.PrinterSetup.PrinterNames);
     I :=ZielCB.Items.IndexOf ('Screen');
     if I>=0 then ZielCB.Items.Delete (i);

     // Wenn kein Drucker �bergeben wurde, dann den Drucker, der im Formular
     // definiert wurde verwenden
     if length(Drucker)=0
      then Drucker :=RechReport.PrinterSetup.PrinterName;

     // Vorgabedrucker ausw�hlen
     if ZielCB.Items.Count >=0 then
      for i:=0 to ZielCB.Items.Count-1 do
       if ZielCB.Items[i]=Drucker
         then ZielCB.ItemIndex :=I;

     // Wenn Drucker nicht gefunden, dann Defaultdrucker ausw�hlen
     if ZielCB.ItemIndex=-1 then
      if ZielCB.Items.IndexOf ('Default')>-1
       then ZielCB.ItemIndex := ZielCB.Items.IndexOf ('Default');

     ZielCBChange(Self);

     DM1.FirmaTab.Open;

     if Show then ShowModal
             else PrintBtnClick (Self);

     if DM1.FirmaTab.Active then DM1.FirmaTab.Close;
     //if ReSNTab.Active then ReSNTab.Close;
     if BonTab.Active then BonTab.Close;
     if BonPosTab.Active then BonPosTab.Close;
end;
//------------------------------------------------------------------------------
procedure TPrintBonForm.FormShow(Sender: TObject);
begin
     PrintBtn.SetFocus;
end;
//------------------------------------------------------------------------------
procedure TPrintBonForm.CloseBtnClick(Sender: TObject);
begin
     Close;
end;
//------------------------------------------------------------------------------
procedure TPrintBonForm.FormularCBChange(Sender: TObject);
begin
     if FormularCB.ItemIndex>-1 then
     begin
       ReportTab.Locate ('NAME',FormularCB.Text,[]);
       RechReport.Template.DatabaseSettings.Name :=FormularCB.Text;
       try RechReport.Template.LoadFromDatabase; except end;
       Rechreport.PrinterSetup.Copies :=1;
       RechReport.Language :=lgGerman;
       RechReport.OnPreviewFormCreate :=RechReportPreviewFormCreate;
       RechReport.AllowPrintToFile :=True;
     end
        else
     begin
       RechReport.Template.DatabaseSettings.Name :='';
       try RechReport.Template.New; except end;
     end;
end;
//------------------------------------------------------------------------------
procedure TPrintBonForm.ZielCBChange(Sender: TObject);
begin
     {set the printer name for the device}
     RechReport.PrinterSetup.PrinterName  := ZielCB.Items[ZielCB.ItemIndex];

     Application.ProcessMessages;

     BinNamCB.Items.Assign (RechReport.PrinterSetup.BinNames);

     if BinNamCB.Items.IndexOf(RechReport.PrinterSetup.BinName)>-1
      then BinNamCB.ItemIndex :=BinNamCB.Items.IndexOf(RechReport.PrinterSetup.BinName);

     if BinNamCB.ItemIndex=-1 then BinNamCB.ItemIndex :=BinNamCB.Items.IndexOf('Default');

     AnzCopy.Value :=Rechreport.PrinterSetup.Copies;
end;
//------------------------------------------------------------------------------
procedure TPrintBonForm.PrintBtnClick(Sender: TObject);
begin
     RechReport.PrinterSetup.DocumentName :='CAO-Faktura Kassenbon Nr.:'+
                                            Inttostr(BonTabBELEGNUMMER.Value);
     RechReport.DeviceType :=dtPrinter;
     RechReport.ShowPrintDialog :=False;
     Rechreport.PrinterSetup.Copies :=round(AnzCopy.Value);
     RechReport.Print;
end;
//------------------------------------------------------------------------------
procedure TPrintBonForm.RechReportPreviewFormCreate(Sender: TObject);
begin
     RechReport.PreviewForm.WindowState := wsMaximized;
     TppViewer(RechReport.PreviewForm.Viewer).ZoomSetting := zs100Percent;
end;
//------------------------------------------------------------------------------
procedure TPrintBonForm.VorschauBtnClick(Sender: TObject);
begin
     RechReport.DeviceType :=dtScreen;
     RechReport.Print;
     RechReport.DeviceType :=dtPrinter;
end;
//------------------------------------------------------------------------------
procedure TPrintBonForm.AnzCopyChange(Sender: TObject);
begin
     Rechreport.PrinterSetup.Copies :=round(ANzCopy.Value);
end;
//------------------------------------------------------------------------------
procedure TPrintBonForm.DruEinrBtnClick(Sender: TObject);
var myPrinter : TppPrinter;
begin
     myPrinter := TppPrinter.Create;
     try
        myPrinter.PrinterSetup :=RechReport.PrinterSetup;

        if myPrinter.ShowSetupDialog
         then RechReport.PrinterSetup := myPrinter.PrinterSetup;

        if ZielCB.Items.IndexOf (RechReport.PrinterSetup.PrinterName)>0
         then ZielCB.ItemIndex :=ZielCB.Items.IndexOf (RechReport.Printer.PrinterName);

       ZielCBChange(Sender);
     finally
        myPrinter.Free;
     end;
end;
//------------------------------------------------------------------------------
procedure TPrintBonForm.BonTabCalcFields(DataSet: TDataSet);
begin
     if not DM1.ZahlartTab.Active then DM1.ZahlartTab.Open;

     if (DM1.ZahlartTab.Locate ('ZAHL_ID',BonTabZAHLART.AsInteger,[])) and
        (DM1.ZahlartTabZahl_ID.AsInteger=BonTabZAHLART.AsInteger) then
     begin
       BonTabZAHLART_KURZ.AsString :=DM1.ZahlartTabTEXT.AsString;
     end;
end;
//------------------------------------------------------------------------------
procedure TPrintBonForm.BonPosTabCalcFields(DataSet: TDataSet);
begin
     case BonPosTabSTEUER_CODE.Value of
          0: BonPosTabMwstProz.Value :=BonTabMwst_0.Value;
          1: BonPosTabMwstProz.Value :=BonTabMwst_1.Value;
          2: BonPosTabMwstProz.Value :=BonTabMwst_2.Value;
          3: BonPosTabMwstProz.Value :=BonTabMwst_3.Value;
     end;
     BonPosTabEPREIS_B.Value    :=Round (BonPosTabEPREIS.Value * (100 + BonPosTabMwstProz.Value)) / 100;
     BonPosTabERPREIS_B.Value   :=Round (BonPosTabERPREIS.Value * (100 + BonPosTabMwstProz.Value)) / 100;
     BonPosTabGPREIS_B.Value    :=Round (BonPosTabGPREIS.Value * (100 + BonPosTabMwstProz.Value)) / 100;
     BonPosTabMWST_BETRAG.Value :=Round (BonPosTabGPREIS.Value * BonPosTabMwstProz.Value) / 100;
end;

end.
