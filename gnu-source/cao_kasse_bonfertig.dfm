object BonFertigForm: TBonFertigForm
  Left = 204
  Top = 322
  BorderStyle = bsDialog
  Caption = 'Beleg fertigstellen'
  ClientHeight = 190
  ClientWidth = 653
  Color = 14680063
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -16
  Font.Name = 'MS Sans Serif'
  Font.Style = [fsBold]
  KeyPreview = True
  OldCreateOrder = False
  Position = poMainFormCenter
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 20
  object Label1: TLabel
    Left = 304
    Top = 2
    Width = 82
    Height = 16
    Caption = 'Zahlungsart'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object BarBtn: TJvSpeedButton
    Left = 300
    Top = 21
    Width = 200
    Height = 30
    Caption = 'BAR [F2]'
    Margin = 116
    Spacing = 1
    OnClick = zahlartClick
    HotTrackFont.Charset = DEFAULT_CHARSET
    HotTrackFont.Color = clWindowText
    HotTrackFont.Height = -11
    HotTrackFont.Name = 'MS Sans Serif'
    HotTrackFont.Style = []
  end
  object ScheckBtn: TJvSpeedButton
    Tag = 1
    Left = 300
    Top = 50
    Width = 200
    Height = 30
    Caption = 'SCHECK [F3]'
    Margin = 81
    OnClick = zahlartClick
    HotTrackFont.Charset = DEFAULT_CHARSET
    HotTrackFont.Color = clWindowText
    HotTrackFont.Height = -11
    HotTrackFont.Name = 'MS Sans Serif'
    HotTrackFont.Style = []
  end
  object ECKarteBtn: TJvSpeedButton
    Tag = 2
    Left = 300
    Top = 79
    Width = 200
    Height = 30
    Caption = 'EC-KARTE [F4]'
    Margin = 65
    Spacing = 0
    OnClick = zahlartClick
    HotTrackFont.Charset = DEFAULT_CHARSET
    HotTrackFont.Color = clWindowText
    HotTrackFont.Height = -11
    HotTrackFont.Name = 'MS Sans Serif'
    HotTrackFont.Style = []
  end
  object Label5: TLabel
    Left = 9
    Top = 2
    Width = 76
    Height = 16
    Caption = 'Zahlbetrag'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label3: TLabel
    Left = 9
    Top = 123
    Width = 67
    Height = 16
    Caption = 'R�ckgeld'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label2: TLabel
    Left = 9
    Top = 63
    Width = 53
    Height = 16
    Caption = 'Gezahlt'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object SpeedButton1: TSpeedButton
    Left = 512
    Top = 21
    Width = 44
    Height = 41
    Caption = '7'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -24
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    OnClick = NumPadBtnClick
  end
  object SpeedButton2: TSpeedButton
    Left = 556
    Top = 21
    Width = 44
    Height = 41
    Caption = '8'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -24
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    OnClick = NumPadBtnClick
  end
  object SpeedButton3: TSpeedButton
    Left = 600
    Top = 21
    Width = 44
    Height = 41
    Caption = '9'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -24
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    OnClick = NumPadBtnClick
  end
  object SpeedButton4: TSpeedButton
    Left = 512
    Top = 62
    Width = 44
    Height = 41
    Caption = '4'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -24
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    OnClick = NumPadBtnClick
  end
  object SpeedButton5: TSpeedButton
    Left = 556
    Top = 62
    Width = 44
    Height = 41
    Caption = '5'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -24
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    OnClick = NumPadBtnClick
  end
  object SpeedButton6: TSpeedButton
    Left = 600
    Top = 62
    Width = 44
    Height = 41
    Caption = '6'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -24
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    OnClick = NumPadBtnClick
  end
  object SpeedButton7: TSpeedButton
    Left = 512
    Top = 103
    Width = 44
    Height = 41
    Caption = '1'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -24
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    OnClick = NumPadBtnClick
  end
  object SpeedButton8: TSpeedButton
    Left = 556
    Top = 103
    Width = 44
    Height = 41
    Caption = '2'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -24
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    OnClick = NumPadBtnClick
  end
  object SpeedButton9: TSpeedButton
    Left = 600
    Top = 103
    Width = 44
    Height = 41
    Caption = '3'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -24
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    OnClick = NumPadBtnClick
  end
  object NumPadBtn: TSpeedButton
    Left = 512
    Top = 144
    Width = 44
    Height = 39
    Caption = '0'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -24
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    OnClick = NumPadBtnClick
  end
  object SpeedButton11: TSpeedButton
    Left = 556
    Top = 144
    Width = 44
    Height = 39
    Caption = ','
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -24
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    OnClick = NumPadBtnClick
  end
  object BackBtn: TSpeedButton
    Left = 600
    Top = 144
    Width = 44
    Height = 39
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -24
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    Glyph.Data = {
      F6000000424DF600000000000000760000002800000010000000100000000100
      0400000000008000000000000000000000001000000010000000000000000000
      BF0000BF000000BFBF00BF000000BF00BF00BFBF0000C0C0C000808080000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
      7777777777777777777777777777777777777777777777777777777777777777
      7777777777777778477777444447777748777744447777777477774447777777
      7477774474777777747777477744777748777777777744448777777777777777
      7777777777777777777777777777777777777777777777777777}
    ParentFont = False
    OnClick = BackBtnClick
  end
  object BuchenBtn: TBitBtn
    Left = 300
    Top = 119
    Width = 200
    Height = 30
    Caption = 'Buchen'
    TabOrder = 0
    OnClick = BuchenBtnClick
    Glyph.Data = {
      DE010000424DDE01000000000000760000002800000024000000120000000100
      0400000000006801000000000000000000001000000000000000000000000000
      80000080000000808000800000008000800080800000C0C0C000808080000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
      3333333333333333333333330000333333333333333333333333F33333333333
      00003333344333333333333333388F3333333333000033334224333333333333
      338338F3333333330000333422224333333333333833338F3333333300003342
      222224333333333383333338F3333333000034222A22224333333338F338F333
      8F33333300003222A3A2224333333338F3838F338F33333300003A2A333A2224
      33333338F83338F338F33333000033A33333A222433333338333338F338F3333
      0000333333333A222433333333333338F338F33300003333333333A222433333
      333333338F338F33000033333333333A222433333333333338F338F300003333
      33333333A222433333333333338F338F00003333333333333A22433333333333
      3338F38F000033333333333333A223333333333333338F830000333333333333
      333A333333333333333338330000333333333333333333333333333333333333
      0000}
    Margin = 5
    NumGlyphs = 2
    Spacing = 105
  end
  object Panel1: TPanel
    Left = 8
    Top = 142
    Width = 281
    Height = 40
    BevelOuter = bvNone
    BorderStyle = bsSingle
    Ctl3D = False
    ParentCtl3D = False
    TabOrder = 2
    object zurueck: TLabel
      Left = 56
      Top = 9
      Width = 217
      Height = 25
      Alignment = taRightJustify
      AutoSize = False
      Caption = '0,00 �'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -24
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Layout = tlBottom
    end
  end
  object AbortBtn: TBitBtn
    Tag = 1
    Left = 300
    Top = 153
    Width = 200
    Height = 30
    Caption = 'Schlie�en [ESC]'
    TabOrder = 1
    Kind = bkClose
    Margin = 5
    Spacing = 34
  end
  object Panel2: TPanel
    Left = 8
    Top = 81
    Width = 281
    Height = 40
    BevelOuter = bvNone
    BorderStyle = bsSingle
    Color = clWindow
    Ctl3D = False
    ParentCtl3D = False
    TabOrder = 3
    object ZahlartLab: TLabel
      Left = 3
      Top = 9
      Width = 91
      Height = 25
      AutoSize = False
      Caption = 'EC-Karte'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -19
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Layout = tlCenter
    end
    object Gegeben: TJvxCurrencyEdit
      Left = 96
      Top = 6
      Width = 177
      Height = 35
      AutoSize = False
      BorderStyle = bsNone
      CheckOnExit = True
      DisplayFormat = ',0.00 EUR;-,0.00 EUR'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -24
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      FormatOnEditing = True
      MaxValue = 999999
      ParentFont = False
      TabOrder = 0
      Value = 11287.99
      ZeroEmpty = False
      OnChange = GegebenChange
      OnExit = GegebenExit
      OnKeyPress = GegebenKeyPress
    end
  end
  object Panel3: TPanel
    Left = 8
    Top = 22
    Width = 281
    Height = 40
    BevelOuter = bvNone
    BorderStyle = bsSingle
    Ctl3D = False
    ParentCtl3D = False
    TabOrder = 4
    object ZahlBetragLab: TLabel
      Left = 56
      Top = 9
      Width = 217
      Height = 25
      Alignment = taRightJustify
      AutoSize = False
      Caption = '0,00 �'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -24
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Layout = tlBottom
    end
  end
end
