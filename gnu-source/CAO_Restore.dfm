object RestoreForm: TRestoreForm
  Left = 312
  Top = 199
  HelpContext = 100000
  BorderStyle = bsDialog
  Caption = 'Daten-Rücksicherung'
  ClientHeight = 193
  ClientWidth = 480
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 107
    Width = 122
    Height = 13
    Caption = 'Gesamtfortschritt Backup:'
  end
  object TabNameLab: TLabel
    Left = 96
    Top = 54
    Width = 3
    Height = 13
  end
  object Label3: TLabel
    Left = 8
    Top = 64
    Width = 52
    Height = 13
    Caption = 'Entpacken'
  end
  object ZipSizeLab: TLabel
    Left = 112
    Top = 128
    Width = 3
    Height = 13
  end
  object Label2: TLabel
    Left = 352
    Top = 16
    Width = 32
    Height = 13
    Caption = 'Label2'
  end
  object StartRestoreBtn: TBitBtn
    Left = 8
    Top = 152
    Width = 185
    Height = 33
    Anchors = [akLeft, akBottom]
    Caption = 'Rücksicherung starten'
    Default = True
    TabOrder = 0
    OnClick = StartRestoreBtnClick
    Glyph.Data = {
      66010000424D6601000000000000760000002800000014000000140000000100
      040000000000F000000000000000000000001000000010000000000000000000
      8000008000000080800080000000800080008080000080808000C0C0C0000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00877777777777
      77777777504000000000000000000007001108777777777777777707CCCC0888
      8888888888888707333308889988888888888807000008800000000000008807
      CCCC0888888888888888880733330880000000000000880700000F8888888888
      88888807CCCC0FFFFFFFFFFFFFFFFF0733330000000000000000000800008888
      8888888888888888CCCC888888888EE7888888883333888888888EE788888888
      0000888888888EE7888888880000888888888EE7888888882222888888EEEEEE
      EE788888AAAA8888888EEEEEE7888888888888888888EEEE7888888800008888
      88888EE7888888880000}
  end
  object pb1: TProgressBar
    Left = 8
    Top = 123
    Width = 465
    Height = 16
    Min = 0
    Max = 100
    TabOrder = 1
  end
  object PB3: TProgressBar
    Left = 8
    Top = 80
    Width = 465
    Height = 16
    Min = 0
    Max = 10001
    TabOrder = 2
  end
  object CloseBtn: TBitBtn
    Left = 304
    Top = 152
    Width = 169
    Height = 33
    Anchors = [akLeft, akBottom]
    Cancel = True
    Caption = 'S&chließen'
    TabOrder = 3
    OnClick = CloseBtnClick
    Glyph.Data = {
      DE010000424DDE01000000000000760000002800000024000000120000000100
      0400000000006801000000000000000000001000000000000000000000000000
      80000080000000808000800000008000800080800000C0C0C000808080000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00388888888877
      F7F787F8888888888333333F00004444400888FFF444448888888888F333FF8F
      000033334D5007FFF4333388888888883338888F0000333345D50FFFF4333333
      338F888F3338F33F000033334D5D0FFFF43333333388788F3338F33F00003333
      45D50FEFE4333333338F878F3338F33F000033334D5D0FFFF43333333388788F
      3338F33F0000333345D50FEFE4333333338F878F3338F33F000033334D5D0FFF
      F43333333388788F3338F33F0000333345D50FEFE4333333338F878F3338F33F
      000033334D5D0EFEF43333333388788F3338F33F0000333345D50FEFE4333333
      338F878F3338F33F000033334D5D0EFEF43333333388788F3338F33F00003333
      4444444444333333338F8F8FFFF8F33F00003333333333333333333333888888
      8888333F00003333330000003333333333333FFFFFF3333F00003333330AAAA0
      333333333333888888F3333F00003333330000003333333333338FFFF8F3333F
      0000}
    NumGlyphs = 2
  end
  object JvFilenameEdit1: TJvFilenameEdit
    Left = 8
    Top = 16
    Width = 321
    Height = 21
    OnAfterDialog = JvFilenameEdit1AfterDialog
    DefaultExt = 'CAO_backup_*.zip'
    Filter = 
      'gepackte CAO-Datensicherung|CAO_backup_*.zip|CAO-Datensicherung|' +
      'CAO_backup_*.sql|Alle Dateien|*.*'
    DialogTitle = 'Backup-Datei auswählen'
    ButtonFlat = False
    DirectInput = False
    NumGlyphs = 1
    TabOrder = 4
    Text = 'JvFilenameEdit1'
  end
  object Zip: TZipMaster
    Verbose = False
    Trace = False
    AddCompLevel = 9
    AddOptions = [AddHiddenFiles]
    ExtrOptions = []
    Unattended = False
    MinZipDllVers = 170
    MinUnzDllVers = 170
    VersionInfo = '1.70'
    AddStoreSuffixes = [assGIF, assPNG, assZ, assZIP, assZOO, assARC, assLZH, assARJ, assTAZ, assTGZ, assLHA, assRAR, assACE, assCAB, assGZ, assGZIP, assJAR]
    OnProgress = ZipProgress
    OnMessage = ZipMessage
    KeepFreeOnDisk1 = 0
    Left = 251
    Top = 58
  end
end
