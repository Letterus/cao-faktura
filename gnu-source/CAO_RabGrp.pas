{

CAO-Faktura f�r Windows Version 1.0
Copyright (C) 2003 Jan Pokrandt / Jan@JP-SOFT.de

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.


******* CAO-Faktura comes with ABSOLUTELY NO WARRANTY ***************

Programm     : CAO-Faktura
Modul        : CAO_RABGRP
Stand        : 26.06.2003
Version      : 1.0.0.54E
Beschreibung : allgemeines Grundmodul,
               welches alle anderen Module in die Oberfl�che einbindet

History :

- 18.05.2003 Unit erstellt
- 26.06.2003 Rabatte werden jetzt mit einer Nachkommastelle angezeigt

}

unit CAO_RabGrp;

{$I CAO32.INC}

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Grids, DBGrids, JvDBCtrl, Db, ExtCtrls, ComCtrls, DBCtrls, ZQuery,
  ZMySqlQuery, StdCtrls, JvComponent, CaoDBGrid;

type
  TRabGrpForm = class(TForm)
    StatusBar1: TStatusBar;
    Panel1: TPanel;
    LiefRGDS: TDataSource;
    LiefRGGrid: TCaoDBGrid;
    Panel2: TPanel;
    Panel3: TPanel;
    KunRGGrid: TCaoDBGrid;
    KunRGDS: TDataSource;
    LiefRabGrp: TZMySqlQuery;
    LiefRabGrpRABGRP_ID: TStringField;
    LiefRabGrpRABGRP_TYP: TIntegerField;
    LiefRabGrpMIN_MENGE: TIntegerField;
    LiefRabGrpLIEF_RABGRP: TIntegerField;
    LiefRabGrpRABATT1: TFloatField;
    LiefRabGrpRABATT2: TFloatField;
    LiefRabGrpRABATT3: TFloatField;
    LiefRabGrpADDR_ID: TIntegerField;
    LiefRabGrpBESCHREIBUNG: TStringField;
    KunRabGrp: TZMySqlQuery;
    DBNavigator1: TDBNavigator;
    DBNavigator2: TDBNavigator;
    KunRabGrpRABGRP_ID: TStringField;
    KunRabGrpRABGRP_TYP: TIntegerField;
    KunRabGrpMIN_MENGE: TIntegerField;
    KunRabGrpLIEF_RABGRP: TIntegerField;
    KunRabGrpRABATT1: TFloatField;
    KunRabGrpRABATT2: TFloatField;
    KunRabGrpRABATT3: TFloatField;
    KunRabGrpADDR_ID: TIntegerField;
    KunRabGrpBESCHREIBUNG: TStringField;
    Label1: TLabel;
    Label2: TLabel;
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure LiefRabGrpBeforePost(DataSet: TDataSet);
    procedure FormShow(Sender: TObject);
    procedure LiefRabGrpAfterScroll(DataSet: TDataSet);
    procedure KunRabGrpNewRecord(DataSet: TDataSet);
    procedure FormCreate(Sender: TObject);
  private
    { Private-Deklarationen }
  public
    { Public-Deklarationen }
  end;

//var RabGrpForm: TRabGrpForm;

implementation

{$R *.DFM}

uses cao_dm;

//------------------------------------------------------------------------------
procedure TRabGrpForm.FormCreate(Sender: TObject);
begin
     if Screen.PixelsPerInch <> 96 then
     begin
       Self.ScaleBy (96, Screen.PixelsPerInch);
       Refresh;
     end;
end;
//------------------------------------------------------------------------------
procedure TRabGrpForm.FormShow(Sender: TObject);
var i : integer;
begin
     LiefRabGrp.Open;
     KunRabGrp.Open;
     KunRGGrid.RowColor1  :=DM1.C2Color;
     LiefRGGrid.RowColor1 :=DM1.C2Color;
     KunRGGrid.EditColor  :=DM1.EditColor;
     LiefRGGrid.EditColor :=DM1.EditColor;

     KunRabGrpLIEF_RABGRP.MaxValue :=DM1.AnzPreis-1;

     KunRGGrid.Columns[2].PickList.Clear;
     for i:=1 to DM1.AnzPreis-1 do
     begin
       KunRGGrid.Columns[2].PickList.Add (IntToStr(I));
     end;
end;
//------------------------------------------------------------------------------
procedure TRabGrpForm.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
     LiefRabGrp.Close;
     KunRabGrp.Close;
end;
//------------------------------------------------------------------------------
procedure TRabGrpForm.LiefRabGrpBeforePost(DataSet: TDataSet);
begin
     LiefRabGrpRABGRP_TYP.AsInteger :=5;
end;
//------------------------------------------------------------------------------
procedure TRabGrpForm.LiefRabGrpAfterScroll(DataSet: TDataSet);
//------------------------------------------------------------------------------
begin
     KunRabGrp.Close;
     KunRabGrp.ParamByName('RABGRP_ID').AsString :=LiefRabGrpRABGRP_ID.AsString;
     KunRabGrp.Open;
     KunRabGrp.ReadOnly :=LiefRabGrp.RecordCount=0;
     //KunRabGrpLIEF_RABGRP.AsInteger :=1;
end;
//------------------------------------------------------------------------------
procedure TRabGrpForm.KunRabGrpNewRecord(DataSet: TDataSet);
begin
     KunRabGrpRABGRP_TYP.AsInteger :=3;
     KunRabGrpRABGRP_ID.AsString :=LiefRabGrpRABGRP_ID.AsString;
     KunRabGrpBESCHREIBUNG.AsString :=LiefRabGrpBESCHREIBUNG.AsString;
     KunRabGrpLIEF_RABGRP.AsInteger :=1;
end;
//------------------------------------------------------------------------------
end.
