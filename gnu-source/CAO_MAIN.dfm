object MainForm: TMainForm
  Left = 272
  Top = 163
  Width = 797
  Height = 576
  Caption = 'CAO-Faktura'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Menu = MainMenu1
  OldCreateOrder = False
  Position = poDesktopCenter
  Scaled = False
  OnActivate = FormActivate
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object SBar: TStatusBar
    Left = 0
    Top = 511
    Width = 789
    Height = 19
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    Panels = <
      item
        Alignment = taCenter
        Text = 'Benutzer'
        Width = 100
      end
      item
        Alignment = taCenter
        Text = 'Speicher'
        Width = 70
      end
      item
        Alignment = taCenter
        Text = 'Suchzeit'
        Width = 80
      end
      item
        Alignment = taCenter
        Text = 'Datensatz'
        Width = 110
      end
      item
        Alignment = taCenter
        Text = 'Suchfeld'
        Width = 150
      end
      item
        Text = 'erstellt'
        Width = 140
      end
      item
        Text = 'ge�ndert'
        Width = 140
      end
      item
        Width = 50
      end>
    SimplePanel = False
    UseSystemFont = False
  end
  object RxSplitter1: TJvxSplitter
    Left = 100
    Top = 0
    Width = 3
    Height = 511
    ControlFirst = LeftPan
    ControlSecond = MainPanel
    Align = alLeft
    Enabled = False
    Ctl3D = True
    TopLeftLimit = 30
    BottomRightLimit = 200
  end
  object MainPanel: TPanel
    Left = 103
    Top = 0
    Width = 686
    Height = 511
    Align = alClient
    BevelOuter = bvNone
    Ctl3D = True
    ParentCtl3D = False
    TabOrder = 2
  end
  object LeftPan: TPanel
    Left = 0
    Top = 0
    Width = 100
    Height = 511
    Align = alLeft
    BevelOuter = bvNone
    TabOrder = 3
    object Panel1: TPanel
      Left = 0
      Top = 0
      Width = 100
      Height = 18
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object ZurueckBtn: TSpeedButton
        Left = 0
        Top = 0
        Width = 101
        Height = 19
        Caption = 'Zur�ck'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        Glyph.Data = {
          F6000000424DF600000000000000760000002800000010000000100000000100
          0400000000008000000000000000000000001000000010000000000000000000
          BF0000BF000000BFBF00BF000000BF00BF00BFBF0000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
          7777777777777777777777777777777777777777777777777777777777777777
          7777777777777778477777444447777748777744447777777477774447777777
          7477774474777777747777477744777748777777777744448777777777777777
          7777777777777777777777777777777777777777777777777777}
        ParentFont = False
        Spacing = 10
        OnClick = ZurueckBtnClick
      end
    end
    object OLBar: TJvOutlookBar
      Left = 0
      Top = 18
      Height = 493
      Align = alClient
      Pages = <
        item
          Buttons = <
            item
              Caption = 'Home'
              ImageIndex = 0
              Tag = 0
            end
            item
              Caption = 'Adressen'
              ImageIndex = 1
              Tag = 1010
            end
            item
              Caption = 'Artikel'
              ImageIndex = 2
              Tag = 1020
            end
            item
              Caption = 'Fahrzeuge'
              ImageIndex = 3
              Tag = 1030
            end
            item
              Caption = 'Mitarbeiter'
              ImageIndex = 4
              Tag = 1040
            end
            item
              Caption = 'PIM'
              ImageIndex = 10
              Tag = 1050
            end
            item
              Caption = 'Notizen'
              ImageIndex = 5
              Tag = 1060
            end
            item
              Caption = 'Vertreter'
              ImageIndex = 24
              Tag = 1070
            end>
          Caption = 'Stammdaten'
          Color = clBtnShadow
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ButtonSize = olbsLarge
          ParentColor = True
          TopButtonIndex = 0
        end
        item
          Buttons = <
            item
              Caption = 'Angebot'
              ImageIndex = 6
              Tag = 2010
            end
            item
              Caption = 'Rechnung'
              ImageIndex = 7
              Tag = 2040
            end
            item
              Caption = 'Einkauf'
              ImageIndex = 8
              Tag = 2050
            end
            item
              Caption = 'EK-Bestellung'
              ImageIndex = 9
              Tag = 2060
            end
            item
              Caption = 'Bestellvorschl�ge'
              ImageIndex = 32
              Tag = 2100
            end
            item
              Caption = 'Vertr�ge'
              ImageIndex = 10
              Tag = 2070
            end
            item
              Caption = 'Mahnung'
              ImageIndex = 28
              Tag = 2080
            end
            item
              Caption = 'Textverarbeitung'
              ImageIndex = 11
              Tag = 2090
            end>
          Caption = 'Vorg�nge'
          Color = clBtnShadow
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ButtonSize = olbsLarge
          ParentColor = True
          TopButtonIndex = 0
        end
        item
          Buttons = <
            item
              Caption = 'Angebot'
              ImageIndex = 6
              Tag = 3010
            end
            item
              Caption = 'Lieferschein'
              ImageIndex = 12
              Tag = 3030
            end
            item
              Caption = 'Kasse'
              ImageIndex = 13
              Tag = 3050
            end
            item
              Caption = 'Rechnung'
              ImageIndex = 14
              Tag = 3040
            end
            item
              Caption = 'Einkauf'
              ImageIndex = 15
              Tag = 3100
            end
            item
              Caption = 'EK-Bestellung'
              ImageIndex = 16
              Tag = 3110
            end>
          Caption = 'Journale'
          Color = clBtnShadow
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ButtonSize = olbsLarge
          ParentColor = True
          TopButtonIndex = 0
        end
        item
          Buttons = <
            item
              Caption = 'Kassenbuch'
              ImageIndex = 17
              Tag = 4010
            end
            item
              Caption = 'Zahlungseingang'
              ImageIndex = 18
              Tag = 4020
            end
            item
              Caption = 'Zahlungsausgang'
              ImageIndex = 18
              Tag = 4030
            end
            item
              Caption = '�berweisungen'
              ImageIndex = 19
              Tag = 4040
            end
            item
              Caption = 'Lastschriften'
              ImageIndex = 19
              Tag = 4050
            end
            item
              Caption = 'Export FIBU'
              ImageIndex = 22
              Tag = 4060
            end>
          Caption = 'Finanzen'
          Color = clBtnShadow
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ButtonSize = olbsLarge
          ParentColor = True
          TopButtonIndex = 0
        end
        item
          Buttons = <
            item
              Caption = 'Statistik'
              ImageIndex = 31
              Tag = 5010
            end
            item
              Caption = 'PLZ'
              ImageIndex = 23
              Tag = 5001
            end
            item
              Caption = 'BLZ'
              ImageIndex = 24
              Tag = 5002
            end
            item
              Caption = 'Shop-Artikel'
              ImageIndex = 25
              Tag = 5021
            end
            item
              Caption = 'Shop-Transfer'
              ImageIndex = 27
              Tag = 5020
            end
            item
              Caption = 'Inventur'
              ImageIndex = 26
              Tag = 5030
            end
            item
              Caption = 'Export'
              ImageIndex = 22
              Tag = 5040
            end
            item
              Caption = 'Wartungsdaten'
              ImageIndex = 30
              Tag = 5050
            end>
          Caption = 'Tools'
          Color = clBtnShadow
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ButtonSize = olbsLarge
          ParentColor = True
          TopButtonIndex = 0
        end>
      LargeImages = OlBarImgList
      SmallImages = OlBarImgList
      PageButtonHeight = 17
      ActivePageIndex = 4
      OnButtonClick = OLBarButtonClick
      BorderStyle = bsNone
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TabOrder = 1
    end
  end
  object MainMenu1: TMainMenu
    AutoLineReduction = maManual
    Images = ImageList1
    Left = 448
    Top = 154
    object Mandant1: TMenuItem
      AutoHotkeys = maManual
      Caption = '&Datei'
      GroupIndex = 1
      object Mandant2: TMenuItem
        Caption = 'Mandant'
        ImageIndex = 28
        OnClick = Wechseln1Click
      end
      object N3: TMenuItem
        Caption = '-'
      end
      object Datenbankprfenoptimierenreparieren1: TMenuItem
        Caption = 'Datenbank pr�fen, optimieren, reparieren'
        ImageIndex = 18
        OnClick = Datenbankprfenoptimierenreparieren1Click
      end
      object Datensicherung1: TMenuItem
        Caption = 'Datensicherung'
        ImageIndex = 16
        OnClick = Datensicherung1Click
      end
      object Rcksicherung1: TMenuItem
        Caption = 'R�cksicherung'
        ImageIndex = 29
        OnClick = Rcksicherung1Click
      end
      object N1: TMenuItem
        Caption = '-'
      end
      object Einstellungen1: TMenuItem
        Caption = 'allgemeine Einstellungen'
        ImageIndex = 24
        OnClick = Einstellungen1Click
      end
      object ShopEinstellungen1: TMenuItem
        Caption = 'Shop-Einstellungen'
        ImageIndex = 24
        OnClick = ShopEinstellungen1Click
      end
      object N5: TMenuItem
        Caption = '-'
      end
      object Druckereinrichten1: TMenuItem
        Caption = 'Drucker einrichten'
        ImageIndex = 24
        OnClick = Druckereinrichten1Click
      end
      object N4: TMenuItem
        Caption = '-'
      end
      object EKPreiseanzeigen1: TMenuItem
        Caption = 'EK-Preise anzeigen'
        Checked = True
        GroupIndex = 99
        ImageIndex = 25
        ShortCut = 120
        OnClick = EKPreiseanzeigen1Click
      end
      object N2: TMenuItem
        Caption = '-'
        GroupIndex = 99
      end
      object OnlineUpdate1: TMenuItem
        Caption = 'Online-Update'
        GroupIndex = 99
        ImageIndex = 21
        Visible = False
        OnClick = OnlineUpdate1Click
      end
      object N6: TMenuItem
        Caption = '-'
        GroupIndex = 99
        Visible = False
      end
      object Beenden1: TMenuItem
        Caption = 'Beenden'
        GroupIndex = 99
        ImageIndex = 44
        ShortCut = 32883
        OnClick = Beenden1Click
      end
    end
    object Ansicht1: TMenuItem
      AutoHotkeys = maManual
      Caption = 'Mod&ul'
      GroupIndex = 98
      object Stammdaten1: TMenuItem
        Caption = 'Stammdaten'
        ImageIndex = 30
        object Adressen1: TMenuItem
          Tag = 1010
          Caption = 'Adressstamm'
          ImageIndex = 30
          ShortCut = 49220
          OnClick = ModulAuswahlClick
        end
        object Artikel1: TMenuItem
          Tag = 1020
          Caption = 'Artikelstamm'
          ImageIndex = 30
          ShortCut = 49217
          OnClick = ModulAuswahlClick
        end
        object Fahrzeugstamm1: TMenuItem
          Tag = 1030
          Caption = 'Fahrzeugstamm'
          ImageIndex = 30
          ShortCut = 49222
          OnClick = ModulAuswahlClick
        end
        object Notizen1: TMenuItem
          Tag = 1060
          Caption = 'Notizen'
          ImageIndex = 30
          ShortCut = 49230
          OnClick = ModulAuswahlClick
        end
        object Mitarbeiter: TMenuItem
          Tag = 1040
          Caption = 'Mitarbeiter'
          ImageIndex = 30
          ShortCut = 49229
          OnClick = ModulAuswahlClick
        end
        object PIMTermine1: TMenuItem
          Tag = 1050
          Caption = 'PIM / Termine'
          ImageIndex = 30
          ShortCut = 49232
          OnClick = ModulAuswahlClick
        end
        object Vertreter1: TMenuItem
          Tag = 1070
          Caption = 'Vertreter'
          ImageIndex = 30
          ShortCut = 49238
          OnClick = ModulAuswahlClick
        end
      end
      object N12: TMenuItem
        Caption = '-'
      end
      object Vorgnge1: TMenuItem
        Caption = 'Vorg�nge'
        SubMenuImages = ImageList1
        ImageIndex = 40
        object Auftrag1: TMenuItem
          Tag = 2020
          Caption = 'Auftrag bearbeiten'
          Enabled = False
          ImageIndex = 40
          ShortCut = 49237
          OnClick = ModulAuswahlClick
        end
        object Angebot1: TMenuItem
          Tag = 2010
          Caption = 'Angebot bearbeiten'
          ImageIndex = 40
          ShortCut = 49223
          OnClick = ModulAuswahlClick
        end
        object Lieferschein1: TMenuItem
          Tag = 2030
          Caption = 'Lieferschein bearbeiten'
          Enabled = False
          ImageIndex = 40
          ShortCut = 49228
          OnClick = ModulAuswahlClick
        end
        object Rechnung1: TMenuItem
          Tag = 2040
          Caption = 'Rechnung bearbeiten'
          ImageIndex = 40
          ShortCut = 49234
          OnClick = ModulAuswahlClick
        end
        object Einkauf1: TMenuItem
          Tag = 2050
          Caption = 'Einkauf bearbeiten'
          ImageIndex = 40
          ShortCut = 49227
          OnClick = ModulAuswahlClick
        end
        object EKBestellungbearbeiten1: TMenuItem
          Tag = 2060
          Caption = 'EK-Bestellung bearbeiten'
          ImageIndex = 40
          ShortCut = 49218
          OnClick = ModulAuswahlClick
        end
        object Vertrgebearbeiten1: TMenuItem
          Tag = 2070
          Caption = 'Vertr�ge bearbeiten'
          ImageIndex = 40
          ShortCut = 49239
          OnClick = ModulAuswahlClick
        end
        object Mahnungenerstellen1: TMenuItem
          Tag = 2080
          Caption = 'Mahnungen erstellen'
          ImageIndex = 40
          ShortCut = 49224
          OnClick = ModulAuswahlClick
        end
        object TextverarbeitungBriefe1: TMenuItem
          Tag = 2090
          Caption = 'Textverarbeitung / Briefe'
          ImageIndex = 40
          ShortCut = 49240
          OnClick = ModulAuswahlClick
        end
      end
      object N13: TMenuItem
        Caption = '-'
      end
      object Journale1: TMenuItem
        Caption = 'Journale'
        ImageIndex = 42
        object Auftrag2: TMenuItem
          Tag = 3020
          Caption = 'Auftragsjournal'
          Enabled = False
          ImageIndex = 42
          ShortCut = 24647
          OnClick = ModulAuswahlClick
        end
        object Angebot2: TMenuItem
          Tag = 3010
          Caption = 'Angebotsjournal'
          ImageIndex = 42
          ShortCut = 24642
          OnClick = ModulAuswahlClick
        end
        object Lieferschein2: TMenuItem
          Tag = 3030
          Caption = 'Lieferscheinjournal'
          ImageIndex = 42
          ShortCut = 24652
          OnClick = ModulAuswahlClick
        end
        object Kasse1: TMenuItem
          Tag = 3050
          Caption = 'Kassenjournal'
          ImageIndex = 42
          ShortCut = 24651
          OnClick = ModulAuswahlClick
        end
        object Rechnung2: TMenuItem
          Tag = 3040
          Caption = 'Rechnungsjournal'
          ImageIndex = 42
          ShortCut = 24658
          OnClick = ModulAuswahlClick
        end
        object Einkaufsjournal1: TMenuItem
          Tag = 3100
          Caption = 'Einkaufsjournal'
          ImageIndex = 42
          ShortCut = 24649
          OnClick = ModulAuswahlClick
        end
        object Bestelljournal1: TMenuItem
          Tag = 3110
          Caption = 'Bestelljournal'
          ImageIndex = 42
          ShortCut = 24659
          OnClick = ModulAuswahlClick
        end
      end
      object N14: TMenuItem
        Caption = '-'
      end
      object Fibu1: TMenuItem
        Tag = 39
        Caption = 'Fibu'
        ImageIndex = 43
        object Kassenbuch1: TMenuItem
          Tag = 4010
          Caption = 'Kassenbuch'
          ImageIndex = 43
          ShortCut = 24641
          OnClick = ModulAuswahlClick
        end
        object Zahlungseingang1: TMenuItem
          Tag = 4020
          Caption = 'Zahlungseingang'
          ImageIndex = 43
          ShortCut = 24643
          OnClick = ModulAuswahlClick
        end
        object Zahlungsausgang1: TMenuItem
          Tag = 4030
          Caption = 'Zahlungsausgang'
          ImageIndex = 43
          ShortCut = 24644
          OnClick = ModulAuswahlClick
        end
        object berweisungen1: TMenuItem
          Tag = 4040
          Caption = '�berweisungen'
          ImageIndex = 43
          ShortCut = 24661
          OnClick = ModulAuswahlClick
        end
        object Lastschriften1: TMenuItem
          Tag = 4050
          Caption = 'Lastschriften'
          ShortCut = 24645
          Visible = False
          OnClick = ModulAuswahlClick
        end
        object ExportFibu1: TMenuItem
          Tag = 4060
          Caption = 'Export Fibu'
          ImageIndex = 43
          ShortCut = 24664
          OnClick = ModulAuswahlClick
        end
      end
      object N10: TMenuItem
        Caption = '-'
      end
      object Tools1: TMenuItem
        Tag = 39
        Caption = 'Tools'
        ImageIndex = 24
        object Shoptransfer1: TMenuItem
          Tag = 5020
          Caption = 'Shoptransfer'
          ImageIndex = 45
          ShortCut = 49231
          OnClick = ModulAuswahlClick
        end
        object Statistik1: TMenuItem
          Tag = 5010
          Caption = 'Statistik'
          ImageIndex = 77
          OnClick = ModulAuswahlClick
        end
      end
      object N7: TMenuItem
        Caption = '-'
      end
      object Log1: TMenuItem
        Tag = 100
        Caption = 'SQL-Log'
        GroupIndex = 2
        ImageIndex = 15
        OnClick = Log1Click
      end
      object RegisteryEditor1: TMenuItem
        Tag = 101
        Caption = 'Registrierungs-Editor'
        GroupIndex = 2
        ImageIndex = 25
        OnClick = RegisteryEditor1Click
      end
      object N8: TMenuItem
        Caption = '-'
        GroupIndex = 2
      end
      object Zurck1: TMenuItem
        Caption = 'Zur�ck'
        GroupIndex = 2
        Hint = 'wechselt in das zuletzt aufgerufene Modul zur�ck'
        ImageIndex = 79
        ShortCut = 32776
        OnClick = ZurueckBtnClick
      end
    end
    object Hilfe1: TMenuItem
      AutoHotkeys = maManual
      Caption = 'Hilfe'
      GroupIndex = 99
      object CAOHandbuch1: TMenuItem
        Caption = 'Hilfe'
        ImageIndex = 49
        OnClick = CAOHandbuch1Click
      end
      object ber1: TMenuItem
        Caption = 'In&fo'
        GroupIndex = 99
        ImageIndex = 47
        OnClick = ber1Click
      end
      object Intro1: TMenuItem
        Caption = 'Intro'
        GroupIndex = 99
        ImageIndex = 48
        OnClick = Intro1Click
      end
      object N9: TMenuItem
        Caption = '-'
        GroupIndex = 99
      end
      object JPSOFTHomepage1: TMenuItem
        Caption = 'JP-SOFT Homepage'
        GroupIndex = 99
        ImageIndex = 45
        Visible = False
        OnClick = JPSOFTHomepage1Click
      end
      object Anwenderforum1: TMenuItem
        Caption = 'Anwenderforum'
        GroupIndex = 99
        ImageIndex = 45
        OnClick = Anwenderforum1Click
      end
      object eMailandenEntwickler1: TMenuItem
        Caption = 'eMail an die Entwickler'
        GroupIndex = 99
        ImageIndex = 46
        Visible = False
        OnClick = eMailandenEntwickler1Click
      end
    end
  end
  object Timer1: TTimer
    OnTimer = Timer1Timer
    Left = 223
    Top = 144
  end
  object ZMonitor1: TZMonitor
    Transaction = DM1.Transact1
    OnMonitorEvent = ZMonitor1MonitorEvent
    Left = 352
    Top = 192
  end
  object PrinterSetupDialog1: TPrinterSetupDialog
    Left = 222
    Top = 240
  end
  object ImageList1: TImageList
    Left = 285
    Top = 192
    Bitmap = {
      494C010150005400040010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      00000000000036000000280000004000000050010000010010000000000000A8
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000010420000
      0000000000000000104200000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000F702EF01000000000000
      F702F70200000000F702F702F70200000000000000000000000000000040007C
      007C007C007C007C004000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7F00000000000000000000F701F701EF1CF73DEF00F701F701
      EF1CF73DEF00F701EF1CF73DEF00F70100000000000000000000007C007C007C
      007C007C007C007C007C007C0000000000000000000000000000000000000000
      0000000000000000000000000000000000000000FF7FFF7FFF7FFF7FFF7FFF7F
      FF7F00000000000000001042000000000000F7010000EF3DFF7FEF0100000000
      EF3DFF7FEF010000EF3DFF7FEF0100000000000000000000007C007C007C007C
      007C007C007C007C007C007C007C000000000000000000000000000000000000
      0000000000000000000000000000000000000000FF7FFF7FFF7FFF7FFF7FFF7F
      000010421863186310420000104200000000F7010000EF3DFF7FEF0100000000
      EF3DFF7FEF010000EF3DFF7FEF0100000000000010420040007C007C007C007C
      007C007C007C007C007C007C007C004010420000000000000000000000000000
      0000000000000000000000000000000000000000FF7FFF7FFF7FFF7FFF7F0000
      104218631863FF0310421042000000000000F7010000EF3DFF7FEF0100000000
      EF3DFF7FEF010000EF3DFF7FEF010000000000000000007C007C007CFF7FFF7F
      007C007C007CFF7FFF7F007C007C007C00000000000000000000000000000000
      0000000000000000104217000000000000000000FF7FFF7FFF7FFF7FFF7F0000
      186318631863186310421863000000000000F7010000EF3DFF7FEF0100000000
      E73CEF7DE73CEF3DE75CEF7DE73C0000000000000000007C007C007C007CFF7F
      FF7F007CFF7FFF7F007C007C007C007C00000000000017001700170017001700
      0000000000000000000017001042000000000000FF7FFF7FFF7FFF7FFF7F0000
      1863FF031863186310421863000000000000F31D0000EF3DFF7FEF010000EF3D
      E75CFF7FEF010000EF3DFF7FEF010000000000000000007C007C007C007C007C
      FF7FFF7FFF7F007C007C007C007C007C00000000000017001700170017000000
      0000000000000000000000001700000000000000FF7FFF7FFF7FFF7FFF7F0000
      1042FF03FF03186310421042000000000000F701EF3DE75CFF7FE73CEF3D0000
      F702EF01F7020000EF3DFF7FEF010000000000000000007C007C007C007C007C
      FF7FFF7FFF7F007C007C007C007C007C00000000000017001700170000000000
      0000000000000000000000001700000000000000FF7FFF7FFF7FFF7FFF7FFF7F
      000010421863186310420000000000000000F7010000EF3DEF7DEF0100000000
      0000000000000000EF3DFF7FEF010000000000000000007C007C007C007CFF7F
      FF7F007CFF7FFF7F007C007C007C007C00000000000017001700000017000000
      0000000000000000000000001700000000000000FF7FFF7FFF7FFF7FFF7FFF7F
      FF7F00000000000000000000000000000000F7010000EF3DFF7FEF0100000000
      0000000000000000F702EF01F70200000000000010420040007C007CFF7FFF7F
      007C007C007CFF7FFF7F007C007C004010420000000017000000000000001700
      1700000000000000000017001042000000000000FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7F00000000000000000000F7010000F702EF01F70200000000
      000000000000000000000000000000000000000000000000007C007C007C007C
      007C007C007C007C007C007C007C000000000000000000000000000000000000
      0000170017001700170010420000000000000000FF7FFF7FFF7FFF7FFF7FFF7F
      FF7F00000000000000000000000000000000F701000000000000000000000000
      0000000000000000000000000000000000000000000000000000007C007C007C
      007C007C007C007C007C007C0000000000000000000000000000000000000000
      0000000000000000000000000000000000000000FF7FFF7FFF7FFF7FFF7FFF7F
      FF7F00001863000000000000000000000000FB02000000000000000000000000
      000000000000000000000000000000000000000000000000000000000040007C
      007C007C007C007C004000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000FF7FFF7FFF7FFF7FFF7FFF7F
      FF7F0000000000000000000000000000000000000000F7020000F702F702EF01
      F702F702EF010000F70200000000000000000000000000000000000010420000
      0000000000000000104200000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000F702F702F702F702F702
      F702F702EF010000F70200000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FF7F0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000FF7F0000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FF7FFF7FFF7FFF7FFF7F
      FF7FFF7F00001042104210421042104200000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FF7F
      0000000000000000000000000000FF7F00000000000000000000000000000000
      0000000000000000000000000000000000000000104200001863186318631863
      186318630000FF7F186318631863104200000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FF7F
      000000000000000000000000FF7F00000000000000000000000000000000EF3D
      000000000000000000000000000000000000000010420000FF7FFF7FFF7FFF7F
      FF7FFF7F0000FF7F186318631863104200000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      FF7F00000000000000000000FF7F0000000000000000000000000000EF3D0000
      00000000000000000000EF3D000000000000000010420000FF7FFF7FFF7FFF7F
      FF7FFF7FFF7F0000FF7F186310420000FF7F0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FF7F000000000000FF7F0000000000000000000000000000000000000000
      000000000000000000000000000000000000000010420000FF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7F000010420000FF7FFF7F0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000FF7F00000000000000000000000000000000000000000000
      EF3D00000000000000000000000000000000000010420000FF7FFF7FFF7FFF7F
      FF7FFF7F0000FF7FFF7F0000FF7FFF7F00000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FF7F00000000000000000000000000000000000000000000EF3D
      0000000000000000EF3D0000000000000000000010420000FF7FFF7FFF7F0000
      FF7FFF7F0000FF7FFF7FFF7FFF7FFF7F00000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000FF7F00000000000000000000000000000000000000000000
      000000000000000000000000000000000000000010420000FF7FFF7F00001042
      0000FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FF7F00000000FF7F00000000000000000000000000000000000000000000
      000000000000000000000000000000000000000010420000FF7F0000FF7F1863
      10420000FF7FFF7FFF7FFF7FFF7FFF7FFF7F0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      FF7F00000000000000000000FF7F000000000000000000000000000000000000
      EF3D00000000EF3D000000000000000000000000104200000000FF7F18631863
      186310420000FF7FFF7FFF7FFF7FFF7FFF7F0000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FF7F
      0000000000000000000000000000FF7F00000000000000000000000000000000
      0000000000000000000000000000000000000000104200000000FF7F18631863
      1863104200000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FF7F00000000
      00000000000000000000000000000000FF7F0000000000000000000000000000
      0000000000000000000000000000000000000000104210420000FF7FFF7FFF7F
      FF7F104200001042104210421042104210420000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000005C00000000
      0000000000001700000000000000000000000000000000000000000000000000
      0000000000001042104200000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000005C005C005C0000
      0000000017001700170000000000000000001042104210421042104210421042
      1042104200001042104210421042000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000005C0000005C00000000
      0000000000001700000017000000000000000000000000000000000000000000
      0000104200000000000010421042104200000000000000001700000000001700
      0000000017000000000017000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000005C00000000000000000000
      000000000000000000000000170000000000FF7FFF7FFF7FFF7FFF7FFF7FFF7F
      0000104200000000000000000000104210420000170000000000170000000000
      1700000000001700000000001700000000000000000000000000000000000000
      000000000000000000000000000000000000005C000000000000000000000000
      000000000000000000000000000017000000FF7F10001000FF7F10001000FF7F
      0000104200000000000000000000000010420000000017000000000000000000
      0000000000000000000000000000170000000000000000000000000000000000
      000000000000000000000000000000000000005C000000000000000010421042
      104210421042104200000000000017000000FF7FFF7FFF7FFF7FFF7FFF7FFF7F
      0000104200000000104200000000000000000000000000001700000000000000
      0000000000000000000017000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FF7F00000000
      000000000000104200000000000000001700FF7F10001000100010001000FF7F
      0000104200000000000000001042000000000000170000000000000000000000
      0000000000000000000000001700000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FF7F00000000
      000000000000104200000000000000001700FF7FFF7FFF7FFF7FFF7FFF7FFF7F
      0000104200000000000000000000104200000000000017000000000000000000
      0000000000000000000000000000170000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FF7F00000000
      000000000000104200000000000000001700FF7F10001000100010001000FF7F
      0000104200000000000010420000000000000000000000001700000000000000
      0000000000000000000017000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FF7F00000000
      000000000000104200000000000000001700FF7FFF7FFF7FFF7FFF7FFF7FFF7F
      0000104200000000000010420000000000000000170000000000000000000000
      0000000000000000000000001700000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FF7F00000000
      000000000000104200000000000000001700FF7F10001000FF7FFF7FFF7FFF7F
      0000104200000000000010421042000000000000000017000000000000000000
      0000000000000000000000000000170000000000000000000000000000000000
      000000000000000000000000000000000000005C000000000000FF7FFF7FFF7F
      FF7FFF7FFF7F000000000000000017000000FF7FFF7FFF7FFF7F000000000000
      0000000000000000000000000000000010420000000000001700000000000000
      0000000000000000000017000000000000000000000000000000000000000000
      000000000000000000000000000000000000005C000000000000000000000000
      000000000000000000000000000017000000FF7F10001000FF7F0000FF7F0000
      0000000000000000000000001042000000000000170000000000170000000000
      0000000000000000000000001700000000000000000000000000000000000000
      0000000000000000000000000000000000000000005C00000000000000000000
      000000000000000000000000170000000000FF7FFF7FFF7FFF7F000000000000
      0000000000000000000010420000000000000000000017000000000017000000
      0000170000000000170000000000170000000000000000000000000000000000
      00000000000000000000000000000000000000000000005C0000005C00000000
      0000000000001700000017000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000001700000000001700
      0000000017000000000017000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000005C005C005C0000
      0000000017001700170000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000001F000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7F0000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000010421042104210421F00
      1042104210420000000000000000000000000000000000000000000000000000
      0000000000000000EF3DEF3D00000000000000000000FF7FFF7FFF7FFF7F0000
      00000000FF7FFF7FFF7FFF7F0000000000000000000000000000000000000000
      0000F702F702F702F7020000000000000000000000001042E07FE07FE07F1F00
      E07FE07F1042000000000000FF7F0000000000001F0000000000000000000000
      000000000000EF3DEF3D000000000000000000000000FF7FFF7FFF7FFF7FFF7F
      0000FF7FFF7FFF7FFF7FFF7F0000000000000000000000000000000000000000
      F702F702F702F702F702F702000000000000000000001042E07F1F001F00E07F
      E07FE07F104200000000FF7FFF7FFF7F00001F0000001F001F00000000000000
      00000000EF3DEF3D0000000000000000000000000000FF7FFF7FFF7FFF7F0000
      0000FF7FFF7FFF7FFF7FFF7F0000000000000000000000000000000000000000
      F702F702F702F702F702F702000000000000000000001042E07FE07FE07FE07F
      E07FE07F104200000000FF7FFF7FFF7F00001F000000000000001F0000000000
      0000EF3DEF3DFF7FFF7FFF7FFF7FFF7F000000000000FF7FFF7FFF7FFF7FFF7F
      0000FF7FFF7FFF7FFF7FFF7F0000000000000000000017001700170017000000
      F702F702F702F702F702F702000000000000000000001042E07FE07FE07FE07F
      E07FE07F104200000000FF7FFF7F104210421F001F0000000000000000000000
      FF7FEF3DEF3DEF3DEF3DEF3DEF3DEF3D000000000000FF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7F0000000000000000000000001700170017000000
      F702F702F702F702F702F702000000000000000000001042E05EE05EE05EE07F
      E07FE07F1042000000000000FF7F1042E07F00001F001F001F001F001F000000
      0000000000000000000000000000000000000000000000000000FF7F0000FF7F
      0000FF7F0000FF7F0000FF7F0000000000000000000000000000170017001700
      0000F702F702F702F7020000E07F00000000000000001042000000000000E05E
      E07FE07F10420000000000001042E07F10420000000000000000000000000000
      00000000FF7FFF7FFF7FFF7FFF7FFF7F000000000000FF7F0000FF7F00000000
      0000FF7F0000EF3D0000FF7F0000000000000000000000000000000017000000
      00000000000000000000E07FFF7F0000000000000000007C007C007C007C0000
      E05EE07F104200000000000000001042E07F0000000000001F0000001F000000
      00000000FF7F00000000FF7F0000FF7F000000000000FF7F0000FF7F0000FF7F
      0000FF7F0000EF3D0000FF7F0000000000000000000000000000000000000000
      0000FF7FE07FFF7FE07FFF7FE07F000000000000005C007C007C007C007C007C
      0000104210420000000000000000E07F1042000000001F00000000001F000000
      00000000FF7FFF7FFF7FFF7FFF7FFF7F000000000000FF7F0000FF7FFF7F0000
      FF7FFF7F0000FF7F0000FF7F0000000000000000000000000000000000000000
      0000E07FFF7FE07FFF7FE07FFF7F000000000000005C007C007C007C007C007C
      00000000000000000000000000000000E07F000000001F00000000001F000000
      00000000FF7F0000FF7FFF7FFF7FFF7F000000000000FF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7F0000000000000000000000000000000000000000000000000000
      0000FF7FE07FFF7FE07FFF7FE07F000000000000005C007C007C007C007C007C
      000000000000000000000000000000000000000000001F000000000000000000
      00000000FF7FFF7FFF7F000000000000000000000000FF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7F0000FF7FFF7F0000000000000000000000000000000000000000
      0000E07FFF7FE07FFF7FE07FFF7F000000000000005C005C007C007C007C007C
      0000000000000000000000000000000000000000000000001F001F001F000000
      00000000FF7F0000FF7F0000FF7FFF7F000000000000007C007C007C007C007C
      007C007C007C0000FF7F00000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000005C005C005C005C0000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000FF7FFF7FFF7F0000FF7F0000000000000000FF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7F0000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000170017001700170017001700
      1700170017001700170017000000000000000000000000000000000000000000
      0000FF7F104210421042104210421042FF7F0000000010421000000000000000
      100000000000000000001000000000000000000000001863FF7F1863FF7F1863
      FF7F1863FF7F0000E07FFF7FE07FFF7FE07F0000000017000000000000000000
      0000000000000000170000000000000000000000000000000000000000000000
      0000E07FFF7FE07FFF7FE07FFF7FE07FFF7F0000000010421000000000000000
      10000000000000000000100000000000000000000000FF7F1863FF7F1863FF7F
      1863FF7F18630000FF7F18631863104210420000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FF7F104210421042104210421042FF7F0000000000001042100000000000
      100010420000000010421000000000001000000000001863FF7F1863FF7F1863
      FF7F18631000100010001000E07FFF7FE07F0000000010421042104210421042
      104210421042104210421042000000001700000000000000FF7FFF7FFF7FFF7F
      0000E07FFF7FE07FFF7FE07FFF7FE07FFF7F0000000000000000104210001000
      10001000104210421000100010001000104200000000FF7F1863FF7F1863FF7F
      1863FF7F18631F00100010001863104210420000FF7F00000000000000000000
      000000000000000000001042000017001700000000000000FF7FFF7FFF7FFF7F
      0000FF7F104210421042104210421042FF7F0000000000000000000000001000
      100010001000100010001000100000000000000000001863FF7F1863FF7F1863
      FF7F1863100010001F001000E07FFF7FE07F0000FF7F00000000FF7FFF7FFF7F
      FF7FFF7FFF7FFF7F00001042000000001700000000000000FF7F186318631863
      0000E07FFF7FE07FFF7FE07FFF7FE07FFF7F0000000000000000000000000000
      10001002FF7FFF7F1002100000000000000000000000FF7F1863FF7F1863FF7F
      1863100010001000FF7F1000FF7FFF7FFF7F0000FF7F00001042000000000000
      000000000000FF7F00001042000000001700000000000000FF7FFF7FFF7FFF7F
      0000FF7FFF7FFF7FFF7FFF7FFF7F000000000000000000000000000000000000
      0000FF7F00000000FF7F0000000000000000000000001863FF7F1863FF7F1863
      1000100010000000E07FFF7FE07FFF7FE07F0000FF7F00001042000000000000
      000000000000FF7F00001042000000001700000000000000FF7F186318631863
      0000E07FFF7FE07FFF7FE07FFF7F000000000000000000000000000000000000
      FF7FFF7FFF7FFF7F0000FF7F00000000000000000000FF7F1863FF7F18631000
      1000100018630000000000000000000000000000FF7F00001042000000000000
      000000000000FF7F00001042000000001700000000000000FF7FFF7FFF7FFF7F
      000000000000000000000000000000000000000000000000000000000000FF7F
      FF7FFF7F00000000FF7FFF7FFF7F00000000000000001863FF7F1863FF7F1863
      10001863FF7F1863FF7F00000000000000000000FF7F00001042000000000000
      000000000000FF7F00001042000000001700000000000000FF7F186318631863
      186318631863FF7F0000000000000000000000000000000000000000FF7FFF7F
      FF7F0000000000000000FF7FFF7FFF7F000000000000FF7F1863FF7F1863FF7F
      1863FF7F1863FF7F186300000000000000000000FF7F00001042000000000000
      000000000000FF7F00001042000000001700000000000000FF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7F000000000000000000000000000000000000FF7FFF7FFF7F
      000000000000000000000000FF7FFF7FFF7F000000001863FF7F1863FF7F1863
      FF7F1863FF7F1863FF7F00000000000000000000FF7F00001042000000000000
      000000000000FF7F00001042000000001700000000000000FF7FFF7FFF7FFF7F
      FF7FFF7F0000000000000000000000000000000000000000FF7FFF7FFF7F0000
      0000000000000000000000000000FF7FFF7F00000000FF7F1863100010001000
      100010001000FF7F186300000000000000000000FF7F00001042104210421042
      104210421042000000001042000000001700000000000000FF7FFF7FFF7FFF7F
      FF7FFF7F0000000000000000000000000000000000000000FF7FFF7F00000000
      00000000000000000000000000000000FF7F0000000000000000FF031F001F00
      1F001F0010000000000000000000000000000000FF7F00000000000000000000
      0000000000000000000010420000170017000000000000000000000000000000
      000000000000000000000000000000000000000000000000FF7F000000000000
      0000000000000000000000000000000000000000000000000000FF031F001F00
      1F001F0010000000000000000000000000000000FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7F00000000000017000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000FF03FF03
      FF03FF0300000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000170017001700000000000000
      000000000000000000000000000000000000000000000000000000000000FF7F
      1863FF7F1863FF7F1863FF7F1863FF7F00000000000000000000170017000000
      00000000000000000000000000000000000000000000FF7FFF7FFF7FFF7FFF7F
      FF7FFF7F00001042104210421042104200000000170017001700000000000000
      0000000000000000000000000000000000000000000000000000000000001863
      1F001F001F001F001F001F001F00186300000000000000001700000000001700
      0000000017001700000000000000000000000000104200001863186318631863
      186318630000FF7F186318631863104200000000170017001700000000000000
      00000000000000000000000000000000000000000000FF7F1863FF7F0000FF7F
      1863FF7F1863FF7F1863FF7F1863FF7F00000000000000001700000000001700
      000017000000000017000000000000000000000010420000FF7FFF7FFF7FFF7F
      FF7FFF7F0000FF7F186318631863104200000000000000000000000000000000
      0000000000000000000000000000000000000000000018631F001F0000001863
      FF7F1863005C005CFF7F1863FF7F186300000000000000001700000000001700
      000017000000000017000000000000000000000010420000FF7FFF7FFF7FFF7F
      FF7FFF7FFF7F0000FF7F186310420000FF7F0000000000000000000000000000
      00000000000000000000000000000000000000000000FF7F1863FF7F0000FF7F
      1863FF7F005C005C1863FF7F1863FF7F00000000000000000000170017001700
      000017000000000017000000000000000000000010420000FF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7F000010420000FF7FFF7F0000170017001700000000000000
      000000000000000000000000000000000000000000001863005C005C00001863
      FF7F1863FF7F005C005C1863FF7F186300000000000000000000000000001700
      000017001700170000000000000000000000000010420000FF7FFF7FFF7FFF7F
      FF7FFF7F0000FF7FFF7F0000FF7FFF7F00000000170017001700000000000000
      00000000000000000000000000000000000000000000FF7F005CFF7F0000FF7F
      1863FF7F1863FF7F005C005C1863FF7F00000000000000000000000000001700
      000017000000000000000000000000000000000010420000FF7FFF7FFF7F0000
      FF7FFF7F0000FF7FFF7FFF7FFF7FFF7F00000000170017001700000000000000
      000000000000000000000000000000000000000000001863005C005C00001863
      FF7F005C005C005C005C005CFF7F186300000000000000000000000000000000
      000000000000000000000000000000000000000010420000FF7FFF7F00001042
      0000FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F0000000000000000000000000000
      00000000000000000000000000000000000000000000FF7F005CFF7F0000FF7F
      1863FF7F1863FF7F1863FF7F1863FF7F00000000000000000000000000000000
      000000000000000000000000000000000000000010420000FF7F0000FF7F1863
      10420000FF7FFF7FFF7FFF7FFF7FFF7FFF7F0000000000000000000000000000
      000000000000000000000000000000000000000000001863005C005C00000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000104200000000FF7F18631863
      186310420000FF7FFF7FFF7FFF7FFF7FFF7F0000170017001700000000000000
      00000000000000000000000000000000000000000000FF7F1863FF7F00001F00
      1F001F001F001F001F001F001F001F0000000000000000000000000000000000
      0000000000000000000000000000000000000000104200000000FF7F18631863
      1863104200000000000000000000000000000000170017001700000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000104210420000FF7FFF7FFF7F
      FF7F104200001042104210421042104210420000170017001700000000000000
      000000000000000000000000000000000000000000001F001F001F001F001F00
      1F001F001F000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F00000000000000000000000000000000
      E07FEF3DFF7FFF7FFF7FFF7FFF7FE07F00000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      FF7F0000000000000000FF7F0000FF7F00000000000000000000000000000000
      E07FEF3DFF7FFF7FFF7FFF7FFF7FE07F00000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000100010001000
      1042000010001000100010420000000000000000000000000000000000000000
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F00000000000000000000000000000000
      E07FEF3DFF7FFF7FFF7FFF7FFF7FE07F00000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000100010001000
      000000001000100010000000000000000000EF3D000000000000000000000000
      00000000EF3DFF7F000000000000000000000000000000000000000000000000
      E07FEF3DFF7FFF7FFF7FFF7FFF7FE07F00000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000010001042
      0000000000001000104200000000000000000000E07FE07FE07FE07FE07FE07F
      E07FE07F0000FF7F0000FF7FFF7F000000000000000000000000000000000000
      000000000000EF3DEF3D00000000EF3D00000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000010001042
      0000000000001000104200000000000000000000E07FEF3DEF3DEF3DEF3DEF3D
      E07FE07F0000FF7F0000FF7F0000000000000000FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7F000000000000EF3DEF3DE07F00000000000000000000000000000000
      0000000000000000104217000000000000000000000000000000000010001042
      0000000000001000104200000000000000000000E07FEF3DFF7FFF7FFF7FFF7F
      FF7FE07F0000FF7F000000000000000000000000FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7F000000000000EF3DE07FE07F00000000000017001700170017001700
      0000000000000000000017001042000000000000000000001042104210001042
      0000000000001000104200000000000000000000E07FEF3DFF7FFF7FFF7FFF7F
      FF7FE07F000000000000007C0000000000000000FF7F00000000FF7F00000000
      0000FF7F000000000000007C00000000EF3D0000000017001700170017000000
      0000000000000000000000001700000000000000000010001000100010001042
      0000000000001000104200000000000000000000E07FEF3DFF7FFF7FFF7FFF7F
      FF7FE07F00000000007C007C007C000000000000FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7F00000000007C007C007C000000000000000017001700170000000000
      0000000000000000000000001700000000000000000010001042000010001042
      0000000000001000104200000000000000000000E07FEF3DFF7FFF7FFF7FFF7F
      FF7FE07F0000007C007C007C007C007C00000000FF7F0000000000000000FF7F
      0000FF7F0000007C007C007C007C007C00000000000017001700000017000000
      0000000000000000000000001700000000000000000010001042104210001042
      1042104210421000104210421042000000000000E07FEF3DFF7FFF7FFF7FFF7F
      FF7FE07F007C007C007C007C007C007C007C0000FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7F007C007C007C007C007C007C007C0000000017000000000000001700
      1700000000000000000017001042000000000000000010001000100010001000
      1000100010001000100010001042000000000000E07FEF3DFF7FFF7FFF7FFF7F
      FF7FE07F00000000007C007C007C000000000000FF7F00000000FF7F00000000
      0000000000000000007C007C007C000000000000000000000000000000000000
      0000170017001700170010420000000000000000000000001000100010001000
      1000100010001000100010000000000000000000EF3D00000000EF3DEF3D0000
      0000EF3D00000000007C007C007C000000000000FF7FFF7FFF7FFF7F0000FF7F
      FF7F000000000000007C007C007C000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000E07FEF3DEF3D00000000EF3D
      EF3DE07F00000000007C007C007C000000000000FF7F0000F75EFF7F0000FF7F
      000000000000EF3D007C007C007C000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000E07FE07FEF3D00000000EF3D
      E07FE07F007C007C007C007C0000000000000000FF7FFF7FFF7FFF7F00000000
      0000007C007C007C007C007C0000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000EF3D000000000000000000000000
      00000000EF3D0000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000001F001F00
      1F001F001F000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000001F001F001F001F001F001F00F75E
      0000F75E1F001F001F001F001F001F0000000000000000000000000000000000
      00000000000000000000000000000000000000000000FF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7F0000000000000000000000000000104210420000
      00000000000000000000000000000000000000000000F75EF75EF75EF75E0000
      EF3D0000F75EF75EF75EF75E0000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FF7FFF7F0000FF7FFF7F
      FF7F0000FF7FFF7FFF7FFF7F0000000000000000000000000000000010420000
      0000000000000000000000000000000000000000FF7F0000EF3DEF3D0000FF7F
      EF3DFF7F0000EF3DEF3D0000FF7F0000000000000000000000000000FF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7F00000000000000000000FF7FFF7F0000FF7FFF7F
      FF7F0000FF7FFF7FFF7FFF7F00000000000000000000FF7F0000000000001042
      000000000000000000000000000000000000EF3D0000FF7FFF7FFF7FFF7FFF7F
      EF3DFF7FFF7FFF7FFF7FFF7F0000EF3D000000000000000000000000FF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7F00000000000000000000FF7F0000000000000000
      0000000000000000FF7FFF7F000000000000000000000000FF7F000000001042
      104210421042104210420000000000000000EF3D0000FF7FFF7FFF7FE07F007C
      007CFF7FE07FFF7FFF7FFF7F0000EF3D000000000000005CFF7F0000FF7F1042
      10421042104210421042FF7F00000000000000000000FF7FFF7FFF7F0000FF7F
      FF7FFF7F0000FF7FFF7FFF7F00000000000000000000000000000000FF7F0000
      10421863186310420000104210420000000000000000FF7FFF7FE07FFF7F007C
      007CE07FFF7FE07FFF7FFF7F0000000000000000005C005CFF7F0000FF7F1042
      175CE07FFF7FE07F1042FF7F00000000000000000000FF7FFF7FFF7F0000FF7F
      FF7FFF7F0000FF7FFF7FFF7F0000000000000000000000000000000000001042
      18631863FF7F1863104200001042000000000000EF3D0000E07FFF7FE07FFF7F
      EF3DFF7FE07FFF7FE07F0000EF3D0000000000000000005C005CFF7F005C005C
      E07FFF7FE07FFF7F1042FF7F00000000000000000000FF7FFF7F000000000000
      00000000000000000000FF7F0000000000000000000000000000000000001863
      186318631863FF7F186310420000000000000000EF3D0000FF7FE07FFF7F007C
      F75EE07FFF7FE07FFF7F0000EF3D00000000000000000000005C175C005C0000
      10421042104210421042FF7F00000000000000000000FF7FFF7FFF7FFF7F0000
      FF7FFF7FFF7F0000FF7FFF7F0000000000000000000000000000000000001863
      1863186318631863FF7F18630000000000000000E07F0000E07FFF7FE07F007C
      007C0000E07FFF7FE07F0000000000000000000000000000175C005C005CFF7F
      FF7FFF7FFF7FFF7FFF7FFF7F00000000000000000000FF7FFF7FFF7FFF7F0000
      FF7FFF7FFF7F0000FF7FFF7F0000000000000000000000000000000000001863
      FF7FFF7F18631863186318630000000000000000FF7FE07F000000000000E07F
      007C007C000000000000E07F0000000000000000175C005C005C005CFF7F005C
      FF7FFF7FFF7F00000000000000000000000000000000FF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7F0000000000000000000000000000000010421042
      FF7FFF7F18631863186310421042000000000000E07FFF7FE07FFF7FE07FFF7F
      E07F007C007CFF7FE07FFF7F0000000000000000005C175C00000000FF7FFF7F
      005CFF7FFF7F0000FF7F000000000000000000000000FF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7F0000000000000000000000000000000000000000000000001042
      1042186318631863104200000000000000000000FF7FE07FFF7F007C007CE07F
      FF7F007C007CE07FFF7FE07F00000000000000000000000000000000FF7FFF7F
      FF7FFF7FFF7F00000000000000000000000000000000FF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7F0000FF7FFF7F0000000000000000000000000000000000000000
      10421042104210420000000000000000000000000000FF7FE07F007C007CFF7F
      E07F007C007CFF7FE07F00000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000007C007C007C007C007C
      007C007C007C0000FF7F00000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000FF7FE07F007C007C
      007C007CFF7FE07F000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7F0000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FF7FE07FFF7F
      E07FFF7FE07F0000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000010420000
      0000000000000000104200000000000000000000000000000040004000400040
      0040004000400040004000000000000000000000000000000000000010001000
      1F0010001F000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      FF7FFF7F18631863000000000000000000000000000000000040004000400040
      00400040004000400040000000000000000000000000000010001F0010001000
      10001F0010001000100000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000010420000
      0000000000000000000000000000000000000000000000000040004000400040
      0040004000400040004000000000000000000000000010001F00100000420042
      10001000100010001000100000000000000000000000FF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F00000000000000000000000000000000
      FF7FFF7F18631863000000000000000000000000000000000040004000400040
      004000400040004000400000000000000000000010001F0010001F0000420042
      10001F0010001F001000004200420000000000000000FF7FFF7FFF7FFF7F1700
      1700170017001700FF7FFF7FFF7FFF7F00000000000000000000000010420000
      0000000000000000000000000000000000000000000000000040004000400040
      004000400040004000400000000000000000000010001F001F001F0000420042
      00421000100010001000004200420000000000000000FF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F0000000000000000000000000000FF7F
      104218631042FF7F000000000000000000000000000000000040004000400040
      0040004000400040004000000000000000001F001F001F001F001F0000420042
      00421F001F001F0010001F0010001000000000000000FF7FFF7FFF7FFF7F1700
      17001700170017001700FF7FFF7FFF7F0000000000000000000000001042E07F
      104218631042E07FFF7F00000000000000000000000000000040004000400040
      004000400040E07F0040000000000000000010001F001F000042004200420042
      00421F001F001F001000100010001F00000000000000FF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F000000000000000000000000FF7FFF7F
      104218631042FF7FFF7F10420000000000000000000000000040004000400040
      0040004000400040004000000000000000001F00100000420042004200420042
      00421F0000421F0010001F001F001F00000000000000FF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7F005C005CFF7F00000000000000001042E07FFF7FE07F
      104218631042E07FFF7FE07F1042000000000000000000000040004000400040
      00400040004000400040000000000000000010001F001F000042004200420042
      00420042004200421F0010001F001000000000000000FF7F0000000000000000
      FF7FFF7FFF7FFF7FFF7F005C005CFF7F0000000000001042E07FFF7FFF7F1042
      1863186318631042FF7FFF7FE07F1042000000000000000000400040FF03FF03
      FF03FF03FF030040004000000000000000001F001F001F00004200421F001F00
      10000042004200421F001F0010001F00000000000000FF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F0000000000001042FF7FE07FFF7F1042
      1863186318631042FF7FE07FFF7F1042000000000000000000400040FF03FF03
      FF03FF03FF0300400040000000000000000000001F001F001F001F001F000042
      0042004200420042004210001F00000000000000000000000000000000000000
      000000000000000000000000000000000000000000001042E07FFF7F00001042
      00000000000010420000FF7FE07F1042000000000000000000400040FF03FF03
      FF03FF03FF03004000400000000000000000000010001F000042004200420042
      004200420042004200421F001F00000000000000000000000000000000000000
      000000000000000000000000000000000000000000001042FF7FE07FFF7F1042
      FF7FE07FFF7F1042FF7FE07FFF7F104200000000000000000040004000400040
      0040004000400040004000000000000000000000000000420042004200421F00
      1F0000421F00004200421F000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000001042FF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7F1042000000000000000000000040004000400040
      004000400040004000400000000000000000000000000000004200421F001F00
      10001F0010001F001F0000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000001042FF7FE07F
      FF7FE07FFF7FE07FFF7F10420000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000001F001000
      1F001F001F000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000010421042
      E07FFF7FE07F1042104200000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000FF7FFF7FFF7FFF7FFF7F0000000000000000000000000000
      00000000FF7FFF7FFF7FFF7FFF7FFF7FFF7F0000000000000000000000000000
      1042104200000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FF7FFF7F0000FF7FFF7FFF7F0000000000000000000000000000
      00000000FF7F10421042104210421042FF7F0000000000000000000000001042
      0000000010420000000000000000000000000000000000001042104210421042
      1042104210421042104210421042104200000000000000000000000000000000
      00000000FF7FFF7F0000FF7FFF7FFF7FFF7F0000000000000000000000000000
      00000000FF7F1042FF7FFF7FFF7F1042FF7F0000000000000000000010420000
      0000FF7F00000000104200000000000000000000000000000000000000000000
      0000000000000000000000000000104200000000000000000000000000000000
      0000000000000000FF7FFF7FFF7FFF7FFF7F0000000000000000000000000000
      00000000FF7F10421042104210421042FF7F000000000000000010420000005C
      0000FF7FFF7F000000001042000000000000000000000000FF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7F0000104200000000000000000000000000000000
      000000000000FF7FFF7F0000FF7FFF7FFF7F0000170017001700170017001700
      17000000FF7FFF7FFF7FFF7FFF7FFF7FFF7F00000000000010420000005C005C
      00000000FF7FFF7FFF7F0000000010420000000000000000FF7F170017001700
      17001700FF7FFF03FF03FF7F0000104200000000000000000000000000000000
      0000FF7FFF7FFF7F0000FF7F0000FF7FFF7F0000170010421042104210421042
      10420000FF7F10421042000000000000FF7F0000000010420000005C005C0000
      007C007C00000000FF7FFF7FFF7F00000000000000000000FF7F1700FF7FFF7F
      FF7F1700FF7FFF7FFF7FFF7F000010420000000000000000000000000000FF7F
      FF7FFF7F00000000FF7F0000FF7F0000FF7F0000170000000000000000000000
      00000000FF7FFF7FFF7FFF7F000000000000000000000000005C005C0000007C
      007C007C007C007C00000000FF7F10420000000000000000FF7F1700FF7FFF7F
      FF7F1700FF7FF702F702FF7F00001042000000000000E05EE05E0000FF7FFF7F
      00000000FF7FFF7F0000FF7F0000FF7F00000000170000000000000000000000
      00000000FF7F104210420000000000000000000000000000005C0000007C007C
      007C007C007C007C007C007C000000000000000000000000FF7F170017001700
      17001700FF7FFF03FF03FF7F00001042000000000000FF7FFF7F0000FF7F0000
      E05EE05E00000000FF7F0000FF7F000000000000170000000000000000000000
      00000000FF7FFF7FFF7FFF7F0000000000000000000000000000007C007C007C
      007C007C007C007C007C007C000000000000000000000000FF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7F00001042000000000000FF7F0000000000000000
      0000000000000000000000000000E05E00000000170000000000000000000000
      0000000000000000000000000000000000000000000000000000007C007C007C
      007C007C007C007C007C0000000000000000000000000000FF7FFF03FF03FF03
      FF03FF7FFF03FF7FFF7FFF7F00001042000000000000FF7FFF7FE05EFF7FE05E
      FF7FE05EFF7FE05EFF7FE05EFF7FE05E00000000170000000000000000000000
      000000000000000000001700000000000000000000000000000000000000007C
      007C007C007C007C000000000000000000000000000000001700170017001700
      17001700170017001700170000001042000000000000FF7F0000000000000000
      0000000000000000000000000000E05E00000000170017001700170017001700
      1700170017001700170017000000000000000000000000000000000000000000
      0000007C007C0000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FF7FFF7FE05EFF7FE05E
      FF7FE05EFF7FE05EFF7FE05EFF7F0000000000001700FF7F17001700FF7F1700
      1700FF7F17001700FF7F17000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000170017001700170017001700
      1700170017001700170017000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000F702
      0000000000000000000000000000000000000000000000000000000000000000
      0000F7020000000000000000000000000000000000000000000000000000F702
      F702F70200000000000000000000000000000000000000000000000000000000
      F7020000000000000000000000000000000000000000000000000000F702F702
      0000000000000000000000000000000000000000000000000000000000000000
      0000F702F702000000000000000000000000000000000000000000000000F702
      F702F7020000000000000000000000000000000000000000000000000000F702
      F702F70200000000000000000000000000000000000000000000F702F702F702
      F702F702F702F702000000000000000000000000000000000000F702F702F702
      F702F702F702F70200000000000000000000000000000000000000000000F702
      F702F702000000000000000000000000000000000000000000000000F702F702
      F702F702F702000000000000000000000000000000000000F702F702F702F702
      F702F702F702F702000000000000000000000000000000000000F702F702F702
      F702F702F702F702F7020000000000000000000000000000000000000000F702
      F702F70200000000000000000000000000000000000000000000F702F702F702
      F702F702F702F702000000000000000000000000000000000000F702F702F702
      F702F702F702F702000000000000000000000000000000000000F702F702F702
      F702F702F702F702000000000000000000000000000000000000F702F702F702
      F702F702F702F70200000000000000000000000000000000000000000000F702
      F702F702000000000000000000000000000000000000000000000000F702F702
      0000000000000000000000000000000000000000000000000000000000000000
      0000F702F70200000000000000000000000000000000000000000000F702F702
      F702F702F702000000000000000000000000000000000000000000000000F702
      F702F7020000000000000000000000000000000000000000000000000000F702
      0000000000000000000000000000000000000000000000000000000000000000
      0000F7020000000000000000000000000000000000000000000000000000F702
      F702F7020000000000000000000000000000000000000000000000000000F702
      F702F70200000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      F70200000000000000000000000000000000000000000000000000000000F702
      F702F70200000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000005C000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000005C005C000000000000000000000000000000000000000000000000
      000000000000FF7FFF7FFF7FFF7FFF7FFF7F0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000005C005C005C000000000000005C0000000000000000000000000000
      000000000000FF7F1042104210421042FF7F0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000005C005C00000000005C005C0000000000000000000000000000
      000000000000FF7FFF7FFF7FFF7FFF7FFF7F0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000005C005C175C00000000000000000000000000000000
      000000000000FF7F1042104210421042FF7F0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000175C005C005C000000000000170017001700170017001700
      170017000000FF7FFF7FFF7FFF7FFF7FFF7F0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000005C175C005C005C00000000170010421042104210421042
      104210420000FF7F1042104210421042FF7F0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000005C005C00000000005C005C0000170000000000000000000000
      000000000000FF7FFF7FFF7FFF7FFF7FFF7F0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000017001700170017001700
      1700175C005C17001700170017000000175C0000170000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000001700FF7FFF7FFF7FFF7F
      FF7F005C175CFF7FFF7FFF7F1700000000000000170000000000000000000000
      0000000000000000000017000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000001700FF7F00000000FF7F
      00000000FF7F00000000FF7F1700000000000000170000000000000000000000
      0000000000000000000017000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000001700FF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7F1700000000000000170000000000000000000000
      0000000000000000000017000000000017000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000017001700170017001700
      1700170017001700170017001700000000000000170017001700170017001700
      1700170017001700170017000000170017000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000017000000170017000000
      17001700000017001700000017000000000000001700FF7F17001700FF7F1700
      1700FF7F17001700FF7F17000000000017000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000017001700170017001700
      1700170017001700170017001700000000000000170017001700170017001700
      1700170017001700170017000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000170017001700170017001700
      1700170017001700170017000000000000001042104210421042104210421042
      10421042000000000000000000000000000000000000FF7FFF7FFF7F00000000
      00000000000000000000FF7FFF7FFF7F000000000000E05EE05E000000000000
      0000000000000000E05EE05E0000000000000000170010421042104210421042
      1042104210421042104217000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FF7FFF7FFF7F00000000
      00000000000000000000FF7FFF7FFF7F000000000000E05EE05E000000000000
      0000000000000000E05EE05E0000000000000000170000000000000000000000
      0000000000000000000017000000000000000000000000000000000000000000
      00000000000000000000000000000000E07F0000000000000000000000000000
      00000000000000000000000000000000000000000000E05EE05E000000000000
      0000000000000000E05EE05E0000000000000000170000000000000000000000
      0000000000000000000017000000000000001042104210421042104200000000
      00000000000000000000000000000000E07F0000000000000000000000000000
      0000FF7F000000000000000000000000000000000000E05EE05E000000000000
      0000000000000000E05EE05E0000000000000000170000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000E07F00000000000000000000000000000000000000000000
      FF7FFF7FFF7F00000000000000000000000000000000E05EE05EE05EE05EE05E
      E05EE05EE05EE05EE05EE05E0000000000000000170000000000000000000000
      0000000000000000E07F10420000000000000000000000000000000000000000
      00001F00000000000000E07F000000000000000000000000000000000000FF7F
      FF7FFF7FFF7FFF7F0000000000000000000000000000E05EE05E000000000000
      0000000000000000E05EE05E0000000000000000170000000000000000000000
      0000000000000000E07F00000000000000000000000000000000000000000000
      1F001F0000000000000000000000000000000000000000000000000000000000
      FF7FFF7FFF7F00000000000000000000000000000000E05E0000FF7FFF7FFF7F
      FF7FFF7FFF7FFF7F0000E05E0000000000000000170017001700170017001700
      17001700170017000000E07F1042000000000000000000000000000000001F00
      1F001F0000000000000000000000000000000000000000000000000000000000
      0000FF7F000000000000000000000000000000000000E05E0000FF7FFF7FFF7F
      1F00FF7FFF7FFF7F0000E05E00000000000000001700FF7F17001700FF7F1700
      1700FF7F170017000000E07F000000000000000000000000000000001F001F00
      1F001F0000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000E05E0000FF7FFF7F1F00
      1F001F00FF7FFF7F0000E05E0000000000000000170017001700170017001700
      170017001700170017000000E07F104200000000000000000000000000001F00
      1F001F0000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000E05E0000FF7FFF7FFF7F
      1F001F001F00FF7F000000000000000000000000000000000000000000000000
      000000000000000000000000E07F104200000000000000000000000000000000
      1F001F0000000000000000000000000000000000000000000000000000000000
      FF7FFF7FFF7F00000000000000000000000000000000E05E0000FF7FFF7FFF7F
      FF7F1F001F001F0000001F000000000000000000000000000000000000000000
      00000000000000000000000000000000175C0000000000000000000000000000
      00001F0000000000000000000000000000000000000000000000000000000000
      FF7FFF7FFF7F0000000000000000000000000000000000000000000000000000
      000000001F001F001F001F000000000000000000000000000000000000000000
      0000000000000000000000000000175C175C1042104210421042104200000000
      000000000000000000000000E07F000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000001F001F001F000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000E07F00000000E07F0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000001F001F001F001F000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000E07F000000000000000000000000FF7F
      FF7F000000000000007C0040007C000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000001042104217001700
      1700170017001042104200000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000EF3D000000000000FF7FFF7F
      0000000000000000007C0040007C000000420000000000000000000000000000
      00000000000000000000E07FFF7FE07F00000000000010421700170017001700
      170017001700170017001042F7020000000000000000FF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7F0000000000000000EF3D0000FF7FFF7FFF7FFF7F0000
      0000000000000000007C0040007C004200420000000000000000000000000000
      00000000000000000000FF7FE07FFF7F0000000017001700170017001700E05E
      E07FE05E17001700170017001700F702000000000000FF7FFF7FFF7F0000FF7F
      FF7F0000FF7FFF7FFF7F00000000000000000000FF7F00000000FF7FFF7F0000
      0000000000000000007C0040007C004200420000000000000000000000000000
      0000E07FFF7FE07F0000E07FFF7FE07F0000F70217001700170017001700E07F
      E07FE07F170017001700170017001042000000000000FF7FFF7FFF7F00000000
      00000000FF7FFF7FFF7F000000000000000000000000000000000000FF7F0000
      0000000000000000007C0040007C004200000000000000000000000000000000
      0000FF7FE07FFF7F00000000000000000000170017001700170017001700E05E
      E07FE05E170017001700170017001700104200000000FF7FFF7FFF7F0000FF7F
      FF7F0000FF7FFF7FFF7F00001863186300000000EF3D000000000000FF7F0000
      000000000000007C007C007C007C007C0000000000000000FF7FE07FFF7FE07F
      0000E07FFF7FE07FFF7FE07F0000000000001700170017001700170017001700
      17001700170017001700170017001700104200000000FF7FFF7FFF7FFF7F0000
      0000FF7FFF7F00000000000018631863000000000000EF3D0000FF7F0000EF3D
      000000000000004000400040004000400000000000000000E07FFF7FE07FFF7F
      0000FF7FE07FFF7FE07FFF7F000000000000170017001700170017001700E05E
      E07FE05E170017001700170017001700170000000000FF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7F00000000186318631863000000000000000000000000EF3D0000
      000000000042004200420000000000000000000000000000FF7FE07FFF7FE07F
      0000E07FFF7FE07FFF7FE07F000000000000170017001700170017001700E07F
      E07FE05E17001700170017001700170017000000000000000000000000000000
      0000000000000000186300001863186300000000000000000000000000000000
      000000420042004200000000000000000000000000000000E07FFF7FE07FFF7F
      000000000000000000000000000000000000170017001700170017001700E05E
      E07FE07FE05E1700170017001700170017000000000000000000000000000000
      1863186300000000000000001863186300000000000000000000000000000000
      004200420042000000000000000000000000000000000000FF7FE07FFF7FE07F
      FF7FE07FFF7FE07F000000000000000000001700170017001700170017001700
      E05EE07FE07FE05E170017001700170017000000000000001700000000000000
      1863186300001863186300001863186300000000000000000000000000000042
      004200420000000000000000000000000000000000000000E07FFF7FE07FFF7F
      E07FFF7FE07FFF7F000000000000000000001700170017001700170017001700
      1700E05EE07FE07FE05E17001700170017000000000017001700170000000000
      1863186318630000000018631863186300000000000000000000000000420042
      004200000000000000000000000000000000000000000000FF7FE07FFF7FE07F
      FF7FE07FFF7FE07F00000000000000000000170017001700E05EE07FE05E1700
      17001700E07FE07FE05E17001700170010420000000000001700000000000000
      1863186318631863186318630000000000000000000000000000000000000042
      0000000000000000EF3D0000EF3D00000000000000000000E07FFF7FE07FFF7F
      E07FFF7FE07FFF7F00000000000000000000170017001700E05EE07FE07FE05E
      E05EE05EE07FE07FE05E17001700170010420000000000001700000000000000
      1863186318631863186318630000000000000000000000000000000000000000
      0000000000000000EF3D0000EF3D000000000000000000000000000000000000
      000000000000000000000000000000000000FF7F170017001700E05EE07FE07F
      E07FE07FE07FE05E170017001700104200000000000000001700170000000000
      0000000000000000000000000000000000000000000000420000000000000000
      EF3D000000000000EF3D0000EF3D000000000000000000000000000000000000
      000000000000000000000000000000000000E07F0000170017001700E05EE07F
      E07FE07FE05E1700170017001700000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000420042000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000E07FFF7F1700170017001700
      17001700170017001700F7020000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000010421042104210421042
      1042104210420000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000001042000000000000000000000000000000000000000010001000
      1F0010001F000000000000000000000000000000000000000000000017000000
      000000000000000000000000000000000000000000000000000000000000FF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7F000000000000E07FFF7FFF7FFF7FE07FFF7F
      FF7FFF7F000010421042104210421042104200000000000010001F0010001000
      10001F0010001000100000000000000000000000000000000000000017001700
      00000000005C005C0000005C005C0000005C000000000000000000000000FF7F
      000000000000000000000000FF7F000000000000FF7F10001000100010001000
      1000FF7F00000000000000000000000000000000000010001F00100000420042
      1000100010001000100010000000000000000000170017001700170017001700
      17000000005C005C0000005C005C0000005C000000000000000000000000FF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7F000000000000FF7FFF7FE07FFF7FFF7FFF7F
      E07FFF7F0000FF7FE07FFF7FFF7FFF7F0000000010001F0010001F0000420042
      10001F0010001F00100000420042000000000000000000000000000017001700
      000000000000000000000000000000000000000000000000000000000000FF7F
      000000000000000000000000FF7F000000000000FF7F10001000100010001000
      1000FF7F00001000100010001000FF7F0000000010001F001F001F0000420042
      0042100010001000100000420042000000000000000000000000000017000000
      000000000000000000000000000000000000000000000000000000000000FF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7F000000000000E07FFF7FFF7FFF7FE07FFF7F
      FF7FFF7F0000FF7FFF7FFF7FE07FFF7F00001F001F001F001F001F0000420042
      00421F001F001F0010001F001000100000000000000000000000000000000000
      0000000000000000000000000000000000000000FF7F000000001042FF7FFF7F
      000000000000000000000000FF7F000000000000FF7F10001000100010001000
      1000FF7F00001000100010001000FF7F000010001F001F000042004200420042
      00421F001F001F001000100010001F0000000000000000000000000000000000
      00000000000000000000000000000000000000001042000000001042E07FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7F000000000000FF7FFF7FE07FFF7FFF7FFF7F
      E07FFF7F0000FF7FE07FFF7FFF7FFF7F00001F00100000420042004200420042
      00421F0000421F0010001F001F001F0000000000000000000000FF7FFF7FFF7F
      000000000000000000000000000000000000000000000000FF7F1042FF7F1042
      000000000000FF7F000000000000000000000000FF7F10001000FF7F00000000
      0000000000001000100010421042FF7F000010001F001F000042004200420042
      00420042004200421F0010001F0010000000000000000000000000000000FF7F
      0000000000000000000000000000000000000000104210421042FF7FE07F0000
      FF7FFF7FFF7FFF7F0000FF7F0000000000000000E07FFF7FFF7FFF7F0000FF7F
      FF7FFF7F0000FF7FFF7F00000000000010421F001F001F00004200421F001F00
      10000042004200421F001F0010001F00000000000000FF7FFF7FFF7F0000FF7F
      0000000000000000000000000000000000000000FF7FE07F1042E07FFF7FE07F
      FF7FE07FFF7FE07F000000000000000000000000FF7F10001000FF7F0000FF7F
      E07F00000000FF7F10420000E0030000104200001F001F001F001F001F000042
      0042004200420042004210001F000000000000000000FF7F1042FF7F00000000
      000000000000000000000000000000000000000000001042E07F1042E07F1042
      E07F000000000000000000000000000000000000FF7FFF7FE07FFF7F0000FF7F
      0000FF7FFF7F000000000000E00300000000000010001F000042004200420042
      004200420042004200421F001F000000000000000000FF7F1042FF7F00000000
      00000000000000000000000000000000000000001042E07F00001042FF7F0000
      1042E07F00000000000000000000000000000000000000000000000000000000
      FF7F100010420000E003E003E003E003E0030000000000420042004200421F00
      1F0000421F00004200421F0000000000000000000000FF7FFF7F000000000000
      0000000000000000000000000000000000000000E07F000000001042E07F0000
      00001042FF7F0000000000000000000000000000000000000000000000000000
      FF7FFF7FE07F000000000000E00300000000000000000000004200421F001F00
      10001F0010001F001F0000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000001042FF7F0000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000104210420000E00300000000000000000000000000001F001000
      1F001F001F000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000E07F000000000000000000000000E07F0000000000000000000000000000
      000000000000000000000000000000000000000000000000007C004000401042
      00000000000000000000007C1042000000000000000000000000104200000000
      0000000000000000000010420000000000000000E07FE07F0000EF3DEF3DEF3D
      E07FE07FEF3DEF3DEF3DEF3DE07FE07F00000000000000000000000000000000
      000000000000000000000000000000000000000000000000007C004000400040
      1042000000000000007C004000401042000000000000E05EE05E000010421042
      104210421042000000000000E05E0000000000000000E07F0000000000000000
      000000000000000000000000E07F000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000007C004000400040
      004010420000007C0040004000400040104200000000E05EE05E000010421042
      104210421042000000000000E05E000000000000000000000000FF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7F0000EF3D000000000000000000000000104218630000
      0000000000000000000000000000000000000000000000000000007C00400040
      00400040104200400040004000400040104200000000E05EE05E000010421042
      104210421042000000000000E05E000000000000000000000000FF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7F0000EF3D00000000000000000000E07FE05E00001042
      18630000000000000000000000000000000000000000000000000000007C0040
      00400040004000400040004000401042000000000000E05EE05E000000000000
      000000000000000000000000E05E000000000000000000000000FF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7F0000EF3D000000000000000000000000E07FE07F0000
      000010421863000000000000000000000000000000000000000000000000007C
      00400040004000400040004010420000000000000000E05EE05EE05EE05EE05E
      E05EE05EE05EE05EE05EE05EE05E000000000000000000000000FF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7F0000EF3D0000000000000000000000000000E07FE07F
      E05E000000001042000000000000000000000000000000000000000000000000
      00400040004000400040104200000000000000000000E05E0000000000000000
      00000000000000000000E05EE05E00000000E07FE07FE07F0000FF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7F0000E07FE07F0000000000000000000000000000E07F
      E07FE07F00000000104200000000000000000000000000000000000000000000
      007C0040004000400040104200000000000000000000E05E0000000000000000
      000000000000000000000000E05E000000000000E07FE07F0000FF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7F0000E07FE07FE07F0000000000000000000000000000
      E07FE07FE07F000000001863000000000000000000000000000000000000007C
      00400040004000400040104200000000000000000000E05E0000000000000000
      000000000000000000000000E05E000000000000000000000000FF7FFF7FFF7F
      FF7F00000000000000000000000000000000000000000000000000000000E07F
      E07FE07F000000001863000000000000000000000000000000000000007C0040
      00400040104200400040004010420000000000000000E05E0000000000000000
      000000000000000000000000E05E000000000000000000000000FF7FFF7FFF7F
      FF7F0000FF7FFF7F000000000000000000000000000000000000000000000000
      E07FE07FE07F0000000018630000000000000000000000000000007C00400040
      004010420000007C0040004000401042000000000000E05E0000000000000000
      000000000000000000000000E05E000000000000000000000000FF7FFF7FFF7F
      FF7F0000FF7F0000E07F00000000000000000000000000000000000000000000
      00000000E07FE07F000000001863000000000000000000000000007C00400040
      1042000000000000007C004000400040104200000000E05E0000000000000000
      0000000000000000000000000000000000000000000000000000FF7FFF7FFF7F
      FF7F000000000000E07FE07F0000000000000000000000000000000000000000
      0000000000000000E07F000000001863000000000000000000000000007C0040
      00000000000000000000007C00400040004000000000E05E0000000000000000
      00000000000000000000000000000000000000000000E07F0000000000000000
      00000000000000000000E07FE07F000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000007C0040007C0000000000000000000000000000
      0000000000000000000000000000000000000000E07FE07F0000000000000000
      E07FE07F0000000000000000E07FE07F00000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000E07F000000000000000000000000
      E07F0000000000000000000000000000E07F0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000001700FF7F0000000000000000000000000000000000000000007C007C
      007C007C007C0000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000001863
      0000000000000000000000000000186300000000000000000000000000000000
      0000170017001700FF7F0000000000000000000000000000007C007C007C007C
      007C007C007C007C007C00000000000000000000000000000000000000000000
      0000FF7FFF7FFF7FFF7FFF7FFF7FFF7F00000000000000000000000000000000
      0000104200000000000010420000000000000000000000000000000000000000
      1700170017001700FF7F000000000000000000000000007C007C007C0000EF3D
      0000EF3D0000007C007C007C0000000000000000000000000000000000000000
      0000FF7F1F001F001F001F001F00FF7F00000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000001700
      17001700FF7F17001700FF7F0000000000000000007C007C007C000000000000
      0000000000000000007C007C007C000000000000000000000000000000000000
      0000FF7FFF7FFF7FFF7FFF7FFF7FFF7F00000000170017001700170000001863
      00000000000000001863000000000000000000000000FF7FFF7FFF7F17001700
      1700FF7F0000000017001700FF7F000000000000007C007C000000000000EF3D
      0000EF3D000000000000007C007C0000000000000000FF7FFF7FFF7FFF7FFF7F
      0000FF7F1F001F001F001F001F00FF7F000000001700FF7FFF7FFF7F00001863
      00000000000000001863000000000000000000000000FF7F1042104210421700
      FF7FFF7F0000000000001700FF7F00000000007C007C00000000000000000000
      000000000000000000000000007C007C000000000000FF7F1F001F001F001F00
      0000FF7FFF7FFF7FFF7FFF7FFF7FFF7F000000001700FF7F1042104200000000
      00000000000000000000000000000000000000000000FF7FFF7FFF7FFF7FFF7F
      FF7FFF7F00000000000000001700FF7F0000007C007C00000000000000000000
      000000000000000000000000007C007C000000000000FF7FFF7FFF7FFF7FFF7F
      0000FF7F1F001F00FF7F000000000000000000001700FF7FFF7FFF7FFF7F0000
      00000000000018630000000000000000000000000000FF7F1042104210421042
      1042FF7F000000000000000000001700FF7F007C007C0000000000000000EF3D
      0000EF3D000000000000000000000000000000000000FF7F1F001F001F001F00
      0000FF7FFF7FFF7FFF7F0000FF7F0000000000001700FF7F10421042FF7F1042
      0000000010421042FF7F000000000000000000000000FF7FFF7FFF7FFF7FFF7F
      FF7FFF7F0000000000000000000000001700007C007C00000000000000000040
      00000040000000000000000000000000000000000000FF7FFF7FFF7FFF7FFF7F
      0000FF7FFF7FFF7FFF7F000000000000000000001700FF7FFF7FFF7FFF7FFF7F
      00000000FF7FFF7FFF7F000000000000000000000000FF7F1042104200000000
      0000FF7F0000000000000000000000000000007C007C00000000000000000000
      000000000000007C007C007C007C007C000000000000FF7F1F001F00FF7F0000
      00000000000000000000000000000000000000001700FF7F10421042FF7F1042
      0000000010421042FF7F000000000000000000000000FF7FFF7FFF7FFF7F0000
      0000000000000000000000000000000000000000007C007C0000000000000000
      0000000000000000007C007C007C007C000000000000FF7FFF7FFF7FFF7F0000
      FF7F0000000000000000000000000000000000001700FF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7F170000000000000000000000FF7F1042104200000000
      0000000000000000000000000000000000000000007C007C007C000000000000
      00000000000000000000007C007C007C000000000000FF7FFF7FFF7FFF7F0000
      0000000000000000000000000000000000000000170017001700170017001700
      17001700170017001700170000000000000000000000FF7FFF7FFF7FFF7F0000
      00000000000000000000000000000000000000000000007C007C007C0000EF3D
      0000EF3D0000007C007C007C007C007C00000000000000000000000000000000
      0000000000000000000000000000000000000000170018631700170018631700
      1700186317001700186317000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000007C007C007C007C
      007C007C007C007C007C00000000007C00000000000000000000000000000000
      0000000000000000000000000000000000000000170017001700170017001700
      1700170017001700170017000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000007C007C
      007C007C007C0000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000F75EF75EF75EF75EF75EF75E
      F75EF75EF75EF75EF75EF75EF75EF75E00000000000000000000000000000000
      0000000000000000000000000000104200000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000F75EF75EF75EF75EF75EF75E
      F75EF75EF75EF75EF75EF75EF75EF75E00000000000000000000E07FE07FE07F
      E07FE07FE07FE07FE07FE07FE07F0000000000000000E07F0000000000000000
      00000000000000000000FF7F0000000000000000000000001700000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000E07FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FE07F0000000000000000FF7F0000000000000000
      00000000000000000000E07F0000000000000000000000001700170000000000
      0000E05EE05EE05EE05EE05E0000000000000000FF7FF75EFF7FF75EFF7FF75E
      FF7FF75EFF7FF75EFF7FF75EFF7FF75E0000000000000000FF7FE07FFF7F1863
      18631863186318631863FF7FE07F000000000000000000000000000000000000
      0000000000000000000000000000000000001700170017001700170017000000
      0000000000000000000000000000E05E00000000F75EFF7FF75EFF7FF75EFF7F
      F75EFF7FF75EFF7FF75EFF7F007CFF7F0000000000000000170017001863FF7F
      FF7FFF7FFF7FFF7FFF7FFF7FE07F00000000000000000000E07F000000000000
      000000000000E07F000000000000000000000000000000001700170000000000
      0000FF7F0000FF7F0000FF7F0000000000000000FF7FF75EFF7FF75EFF7FF75E
      FF7FF75EFF7FF75EFF7FF75EFF7FF75E000000001700170017001700FF7F1863
      FF7F1863186318631863FF7FE07F00000000000000000000FF7F000000000000
      000000000000FF7F000000000000000000000000000000001700000000000000
      0000000000000000000000000000104200000000000000000000000000000000
      0000000000000000000000000000000000000000170017001700170017001700
      1863FF7FFF7FFF7FFF7FFF7FE07F00000000000000000000E07F000000000000
      000000000000E07F000000000000000000000000000000000000000000000000
      00000000FF7FFF7FFF7FFF7F0000000000000000000000000000FF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7F0000000000000000000017001700170017001700FF7F
      FF7FFF7F186318631863FF7FE07F000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000FF7F10421042FF7F0000000000000000000000000000FF7F00000000
      00000000FF7F0000FF7F0000000000000000000000000000170017001863FF7F
      FF7FFF7FFF7FFF7FFF7FFF7FE07F000000000000000000000000E07F00000000
      000000000000E07F00000000000000000000000000000000FF7FFF7FFF7F0000
      000000000000FF7FFF7FFF7FFF7F000000000000000000000000FF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7F00000000000000000000000000001700FF7FFF7FFF7F
      18631863186318631863FF7FE07F000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000FF7F0000
      0000000000000000000000000000000000000000000000000000FF7F00000000
      FF7F000000000000000000000000000000000000000000000000E07FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FE07F000000000000000000000000000000000000
      0000000000000000000000000000000000000000FF7FFF7FFF7F0000FF7F0000
      0000000000000000000000000000000000000000000000000000FF7FFF7FFF7F
      FF7F0000FF7FFF7F000000000000000000000000000000000000E07FFF7FFF7F
      0000000000000000FF7FFF7FE07F0000000000000000000000000000E07F0000
      0000000000000000E07F00000000000000000000FF7F1042FF7F000000000000
      0000000000000000000000000000000000000000000000000000FF7F0000F75E
      FF7F0000FF7F0000000000000000000000000000000000000000E07FE07FE07F
      0000186318630000E07FE07FE07F000000000000000000000000000000000000
      0000000000000000000000000000000000000000FF7F1042FF7F000000000000
      0000000000000000000000000000000000000000000000000000FF7FFF7FFF7F
      FF7F000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000FF7FFF7F0000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FF7FEF3DEF3D
      EF3DEF3DEF3DEF3D000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F00000000000000000000FF7F0000F75E
      0000F75E0000EF3D000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000FF7F00000000FF7F00000000
      FF7F00000000FF7F000000000000FF7F00000000000000000000FF7F0000F75E
      0000EF3D0000EF3D000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000001F7C1F7C
      1F7C1F7C1F7C0000000000000000000000000000FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F00000000000000000000FF7F0000F75E
      0000F75E0000EF3D000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000001F7C1F7C1F7C
      1F7C1F7C1F7C1F7C000000000000000000000000FF7F00000000FF7F00000000
      FF7F00000000FF7F000000000000FF7F00000000000000000000FF7F0000F75E
      0000EF3D0000EF3D000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000001F7C1F7C1F7C1F7C
      1F7C1F7C1F7C1F7C1F7C00000000000000000000FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F00000000000000000000FF7F0000F75E
      0000F75E0000EF3D000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000001F7C1F7C1F7C1F7C
      1F7C1F7C1F7C1F7C1F7C00000000000000000000FF7F00000000FF7F00000000
      FF7F00000000FF7F000000000000FF7F00000000000000000000FF7F0000F75E
      0000EF3D0000EF3D000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000001F7C1F7C1F7C1F7C
      1F7C1F7C1F7C1F7C1F7C00000000000000000000FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F00000000000000000000FF7F0000F75E
      0000F75E0000EF3D000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000FF7FFF7F1F7C1F7C
      1F7C1F7C1F7C1F7C1F7C00000000000000000000FF7F00000000FF7F00000000
      FF7F00000000FF7F000000000000FF7F00000000000000000000FF7F0000F75E
      0000EF3D0000EF3D000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FF7FFF7F1F7C
      1F7C1F7C1F7C1F7C000000000000000000000000FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F00000000000000000000EF3D0000EF3D
      0000EF3D0000EF3D000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000FF7F1F7C
      1F7C1F7C1F7C00000000000000000000000000001F001F001F001F001F001F00
      1F001F001F001F001F001F001F001F0000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000F75EF75E1F001F001F001F00
      1F001F001F001F001F001F00F75EF75E0000000000000000FF7FF75EF75EF75E
      EF3DEF3DEF3DEF3DEF3D00000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000EF3D
      EF3DEF3D00000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000FF03FF03
      FF03FF03FF0300000000000000000000000000000000000000000000007C007C
      007C007C007C00000000000000000000000000000000000000000000E003E003
      E003E003E00300000000000000000000000000000000000000000000E07FE07F
      E07FE07FE07F0000000000000000000000000000000000000000FF03FF03FF03
      FF03FF03FF03FF03000000000000000000000000000000000000007C007C007C
      007C007C007C007C000000000000000000000000000000000000E003E003E003
      E003E003E003E003000000000000000000000000000000000000E07FE07FE07F
      E07FE07FE07FE07F00000000000000000000000000000000FF03FF03FF03FF03
      FF03FF03FF03FF03FF030000000000000000000000000000007C007C007C007C
      007C007C007C007C007C0000000000000000000000000000E003E003E003E003
      E003E003E003E003E0030000000000000000000000000000E07FE07FE07FE07F
      E07FE07FE07FE07FE07F0000000000000000000000000000FF03FF03FF03FF03
      FF03FF03FF03FF03FF030000000000000000000000000000007C007C007C007C
      007C007C007C007C007C0000000000000000000000000000E003E003E003E003
      E003E003E003E003E0030000000000000000000000000000E07FE07FE07FE07F
      E07FE07FE07FE07FE07F0000000000000000000000000000FF03FF03FF03FF03
      FF03FF03FF03FF03FF030000000000000000000000000000007C007C007C007C
      007C007C007C007C007C0000000000000000000000000000E003E003E003E003
      E003E003E003E003E0030000000000000000000000000000E07FE07FE07FE07F
      E07FE07FE07FE07FE07F0000000000000000000000000000FF7FFF7FFF03FF03
      FF03FF03FF03FF03FF030000000000000000000000000000FF7FFF7F007C007C
      007C007C007C007C007C0000000000000000000000000000FF7FFF7FE003E003
      E003E003E003E003E0030000000000000000000000000000FF7FFF7FE07FE07F
      E07FE07FE07FE07FE07F00000000000000000000000000000000FF7FFF7FFF03
      FF03FF03FF03FF03000000000000000000000000000000000000FF7FFF7F007C
      007C007C007C007C000000000000000000000000000000000000FF7FFF7FE003
      E003E003E003E003000000000000000000000000000000000000FF7FFF7FE07F
      E07FE07FE07FE07F0000000000000000000000000000000000000000FF7FFF03
      FF03FF03FF0300000000000000000000000000000000000000000000FF7F007C
      007C007C007C00000000000000000000000000000000000000000000FF7FE003
      E003E003E00300000000000000000000000000000000000000000000FF7FE07F
      E07FE07FE07F0000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000500100000100010000000000800A00000000000000000000
      000000000000000000000000FFFFFF0000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FFFFFFFFF80FFFFF000CCE63F007FFFF
      00080001E003FFFF00014623C001FFFF000346238000FFFF000346238000FFE7
      000346038000C1F3000344238000C3FB000302238000C7FB000747E38000CBFB
      000F47E38000DCF3000F47FFC001FF07000F7FFFE003FFFF001F7FFFF007FFFF
      003FD02FF80FFFFF807FC02FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCFFFFFF80
      FFFFF3FFF183C000E223E1F9FBC78000FFFFE1F3F9C78000F9CFF0E3F8078000
      F88FF847FD8F8000F087FC0FFC8F8000F227FE1FFC8F8000F227FC0FFE1F8000
      E773F84FFE1F8000E7F3E0E3FE1F8000E7F3C1F1FF3F8000FFFFC7F8FF7F8000
      FFFFFFFFFFFFE03FFFFFFFFFFFFFFFFF8000F7DFFFCFFFFFBDB6E38F0003FFFF
      9B6CD7D70001EDB7AFDABFFB0000B6DBB6B6701D0000DFFDBB4E680D0060EC17
      BCFEE7CE0078B99BBFFEE7CE007CD99DBFE6E7CE0076EC97BFDAE7CE0066BE1B
      BFDAE7CE0040DB9DBFE6602D0080EC37BFFE701D0183B7FB8000BFFB03C7DB6D
      FFFFD7D707EFEDB7FFFFE38FFFFFFFFFFFFF8003FFFFFBE7FFE78003FF0FC023
      FFC78003FE07C0218F8F8003FC03C020070080030003C020320080038003C020
      00008003C001C03080008003E001C030F9008003F0018038E1008003FA010038
      C9008003FE0100FCC9008003FE0100FCC3008003FE0100FEE3008007FE0181FE
      FF01800FFFFFC3FFFF03801FFFFFFFFF8007FE00CEF78000DFEFFE00CEF78000
      FFFFC000E6668000C006C000F0008000BFF4C000FC038000B016C000FE078000
      AFD6C000FE078000AFD6C000FC038000AFD6C001F8018007AFD6C00FF0608007
      AFD6C00FE0F08007AFD6C00FC1F88007A036C01FC3FCC00FBFF4C03FC7FEF03F
      800EFFFFCFFFF87FFFFFFFFFFFFFFFFFFFFFFFFFF800FFFFFF808FFFF800F3FF
      C0008C018000ED9F80008FFF8000ED6F8000FFFF8000ED6F8000FFFF8000F16F
      80008FFF8000FD1F80008C018000FC7F80008FFF8000FEFF8000FFFF8000FC7F
      8000FFFF8000FD7F80008FFF8000F93F80008C018000FBBF80008FFF801FFBBF
      E03FFFFF801FFBBFFFFFFFFFFFFFFFFFFFFFFC00FC00FFFFFFFFFC00FC00FFFF
      F087FC00FC00FFFFF18F0000FC00FFFFF9CF00010000FFFFF9CF00030000FFE7
      F9CF00070000C1F3E1CF00070000C3FBC1CF00230023C7FBC9CF00010001CBFB
      C00300000000DCF3C00300230023FF07E00700230063FFFFFFFF002300C3FFFF
      FFFF00070107FFFFFFFF003F03FFFFFFFFFFF83FFFFF8003FFFF0001FFFF8003
      E3FF0001F0038003C3FF0001F0038003C1FF0001F0038003C00F0001C0038003
      F003800380038003F8038003C0038003F8038003E2038003F8038007E0038003
      F803800780038003F803800790078003FC078007F00F8003FC0FC00FF01F8007
      FFFFE01FFFFF800FFFFFF03FFFFF801FC007FFFFFFFFF80FC007F83FFFFFF80F
      C007E00F8000F80FC007C0078000F80FC00780038000F80FC00780038000F80F
      C00700018000F807C00700018000F007C00700018000E003C00700018000C001
      C00700018000C001C00780038000C001C0078003FFFFC001C007C007FFFFE003
      C007E00FFFFFF007C007F83FFFFFF80FFFFFFF00FFFFFFFFFFC0FF00FE7FFFFF
      FF80FF00FC3FE001FF00FF00F80FC001FF00FF00F007C001FE408000E001C001
      FD00800EC001C001C000BF00C001C0018000A40AC001C0018000BF01C003C001
      8000A403C007C0018000BFF7F00FC00180008007FC1FC00380028007FF3FFFFF
      C0018007FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFEFFFEFFFFFFFFFFFCFFFE7FF83FFEFFF8FFFE3FF83FFC7F
      F00FE01FF83FF83FE00FE00FF83FF01FC00FE007C007E00FE00FE00FE00FC007
      F00FE01FF01FF83FF8FFFE3FF83FF83FFCFFFE7FFC7FF83FFEFFFEFFFEFFF83F
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFFF80FFFFFFFFFF9FFF80FFFFFFFF
      FF8EFF80FFFFFFFFFFCCFF80FFFFFFFF8001FF80FFFFFE3FBFE38000FF7FFE3F
      A4A18000FE3FFE3FBFCCBF80FC1FF80F8002A480F80FFC1FC003BFF7FE3FFE3F
      C003A497FE3FFF7FC003BFF6FE3FFFFFC0038004FFFFFFFFD24B8006FFFFFFFF
      C0038007FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF83E0C0038007007F
      83E086038007007F83E08603BFD7FFFE83608603A48707FEEE3B8003BFC707EF
      EC1B8003A483FF77E0038003BFCBFE7EFC1F80038001FC7EFE3F80038005F878
      FF7F80038000FC7EFC1F801FFFF0FE7EFC1F8017FFF8FF7FFC1F8187FFF807FB
      FC1FFFC7FFFC07F6FFFFFF87FFFFFFFEF862FFE0E00F800780E0FFE0C0038007
      01E0FE008001800701E0FE000001800031E1C0000000800031C1C00300008000
      C181C00300008000C307C00300008000FE17C0030000FC00CC37C00F0000EC00
      A877C00F0000C40040F7C00F0000EC0001E3C00F0000EC01C1E3C00F0001E403
      C0E3FFFF4003FFFFC83FFFFF8007FFFFC03FFFFFFFFFF801001FF83FFBFFF801
      0000E00FF992F8010000C0078092F80100008003F9FFF80100008003FBFFF801
      00000001FFFFB00100000001E0FFB00100000001E0FFE0010000000180FF8203
      0000000180FF80070000800380FFC00F0000800381FF927F0000C00783FFB33F
      FC00E00F87FFF3FFFC01F83FFFFFFFFFFFFFFF7EFFFFE1F3C0019001FFFFE0E1
      8031C003FFFFE0408031E003C3FFF0008031E003C0FFF8018001E003E03FFC03
      8001E003F01FFE0780010001F80FFE078FF18000F807FC078FF1E007F00FF803
      8FF1E00FF807F0418FF1E00FFE03F0E08FF1E027FF81F9F08FF5C073FFE3FFF8
      80019E79FFFFFFFFFFFF7EFEFFFFFFFFFFFFFFFFFFFFF8F8FF9FF83FFE00F8F8
      FF0FE00FFE00F870FE0FC447FE00F80080078C638000800080239C7380008000
      80333FF98000800080393EF980008001803C3C7F80018003803E3C7F80038003
      873F3C4180078003803F9C61807F8007857F8C7180FF800780FFC44181FF8007
      81FFE00DFFFF8007FFFFF83FFFFFFFFF8001FFFFFFFFFFFF0000F00183E0FF03
      0000E00183E0EE010000E00183E0E6000000E001808002000000E0018000E654
      000080018100EE04000080018100FF01E0078001C001C103E007E001E083C181
      E007E001E08301C1E007E001F1C701FFE00FE001F1C701FFE01FE001F1C703FF
      E03FF003FFFF07FFE07FFE1FFFFF0FFFFFFFFFFFE00FFFFFFFFF0000E00FFFFF
      F83F0000E00FC007E00F0000E00FE7E7C0070000E00FF3F7C0070000E00FF9F7
      80030000E00FFCFF80030000A00BFE7F80030000C007FF3F80030000E00FFE7F
      C0070000E00FFCFFC0070000C007F9F7E00F0000C007F3F7F83F0000C007E7E7
      FFFFFFFFF83FC007FFFFFFFFF83FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      F83FF83FF83FF83FE00FE00FE00FE00FC007C007C007C007C007C007C007C007
      8003800380038003800380038003800380038003800380038003800380038003
      C007C007C007C007C007C007C007C007E00FE00FE00FE00FF83FF83FF83FF83F
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00000000000000000000000000000000
      000000000000}
  end
  object OpenDialog1: TOpenDialog
    DefaultExt = 'zip'
    Filter = 'gepackte Datensicherung (*.zip)|*.zip'
    Left = 350
    Top = 240
  end
  object OlBarImgList: TImageList
    Height = 32
    Width = 32
    Left = 224
    Top = 192
    Bitmap = {
      494C010121002200040020002000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000800000002001000001001000000000000020
      0100000000000000000000000000000000000000000000000000010000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000008214A294B2D9035A710
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000103E0D4ACB55345A1442
      A610000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000002D5289720672AA591256
      1546A71000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000887A0A7FC97EE76DAA59
      13561542A7100000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000877EA87E0A7FA97E076E
      AA59335614428610000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000877EA97E0B7FA97E
      0672AA593356F541A71000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000006776E97E0B7F
      C97E0672AA593356F44186100000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000A120A4592666
      097FC87E0672A95912562C290000000000000104020423042308010000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000023042304230003006310
      E8550A7FC87E2776CC4D6B2D8410650C4E25D335153E774A984AD235C9182204
      0000220023042304230423040100000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000120DF208F208F208D200
      F208ED49AA72B062524A3146144279425E57DF63FF6FFF6FFF6FDF6B3C5FF335
      A9108A04F00812091209120D4504000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000793AFA56FA56FA56FA56
      D952D75A35733863D75A3646DD4ABF5FFF67FF67FF6BFF6FFF6FFF73FF7BBF73
      153E0B1DD031D8521B5B182E4500000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000003C12FB7FDB7FDB7FDB7F
      DB7FDB7BDC7B7973B756DC4ABF5BFF67FF63FF67FF6FFF73FF77FF7BFF7FFF7F
      9E6FB1358C35366BFC7F984A2500000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000003C12DB7FBB779A779A77
      9A77BB77BA771763BA4E1B537B5B3B575B577B5F7B637B677B6B7B6B7B6F7A6F
      DE73D9524C29714EBB7B984A2500000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000003C12DB7F796FF662F55E
      F55EF662F55E7452D94E1857F852D84E1857385B395F3963396359675A6B5A67
      BC677C5FB035EE3D9977B94A2500000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000003C12DB7FBB779A779A77
      9A77BA777973DA5A5E5BFF639F579F5BDF63FF6BFF6FFF77FF7BFF7BFF7BFF77
      FF73FF6B353E8D357873B94A2500000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000003C12DB7FBB779A779A77
      9A779A77386BFB567D5B3957D94AF94E1953395B595B595F596359637A637A63
      7B63FE6B97468D357873B9462500000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000003C12DC7F7973F65EF55E
      F65EF662D55AB75219571957F94E1953195339575A5B5A5F5A5F5A5F7B639B63
      9B5FBD5F5642CF3D9977B9462500000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000003C12DB7FBB77BB7BBB7B
      BB7BDC7BBB7BFA5E7D5BFF67BF639F5F9F5BDF5FFF63FF67FF6BFF6BFF6BFF67
      FF679F5F143A314ADB7FBA462600000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000003C12DC7F9B777973796F
      796F79737973D85A1B537A5F395F1957F952F94E19533957395739573A575B5B
      DD631C53B135D55AFC7FBA462600000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000003C12DC7F79731663F65E
      F662F65E1663F65E964E19577A6F7A6F5A671B531B533B531B4F1B535C579C5F
      7D5B363E1042796FFD7FBA424600000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000003C12DB7FDC7BFD7FFD7F
      FD7FFD7FFD7FFD7F3B67FC5AFF7BFF7FFF7FFF6BBF5F9F579F57DF5FFF67DF5F
      DC4A1342386BDC7FFC7FBA424600000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000003C12DC7F9A7337673767
      37673767376737671767754E974E5B679C6BBC6BBD639D5B9D5FBD5F7D5B9B46
      F33DB45A796F9B77FD7FBA424600000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000003C12DC7F9A73386B3867
      38673867386B3867386B386BF75E764A974AFA4E3B533C573C57DA4E784A754E
      17679A737A6F9B77FD7FBB424600000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000003C12DC7FBC7BDC7BDC7B
      DC7BDC7BDC7BDC7BDC7BDC7BDC7B9B73F95E554A3546974E9852D85A396BBB77
      DC7BDC7BBC7BBC7BFC7FDB424600000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000003C12DC7F9A7337671767
      37673767376737673767376737673867386B386B586B586B586B586B796F7A6F
      796F796F796F9B77FD7FDB424600000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000003C12FC7F9B77586F586B
      586F586F586F586F586F586F586F586F596F596F596F796F796F796F7A739A73
      9A739A739A73BB7BFD7FDB424700000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000003C12DC7FDC7BFD7FFD7F
      FD7FFD7FFD7FFD7FFD7FFD7FFD7BDD7BDD7BDD7BDD7BDD7BDD7BDD7BDC7BDC7B
      DC7BDC7BDC7BDC77FD7FDB424700000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000181A3726372217221722
      1722172217221722172217221722172217221722172217221722172217221722
      17221722172237223826191A6700000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000097017701770177017701
      770177017701770177017701770177017701770177017701770197011A16B805
      9801FA1176056D3D902DD9098700000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000D9053B163C163C123C16
      3C123C123C163C123C123C163C123C123C121C0E1C0E1C0E1C0E3C129E265D16
      5D16BE263B1E124A153A1A0E6700000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000DA053B163C163C0E3C0E
      1C0E1C0E1C0E1C0E1C0A1C0E1C0E1C0E3C0E1C0E1C0A1C0A1C0A1B0AFB051C0A
      1C0A1B0A1C0A1B0A1C0AB8054500000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000010420000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000001042FF7FFE7FFE7F7863186318631863945294521042104290310000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000010420000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000AE396F316F316F31
      6F318F356F316F31000000000000000000000000000000000000000000000000
      000000001042FF7FFE7FFE7F7863186318631863945294521042104290310000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000CE39CE39CE39CE39CE39CE39CE39CE39CE390000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000001042000000000000920100001042
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000AE398F358F318F35F63D574A574A
      574A574A5546F63D8F356F310000000000000000000000000000000000000000
      000000009031FF7FFE7FFE7F7863186318631863945294521042104290310000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000CE39CE39AF35AF351121111DF11CF11C111D0E1D2D21AF35CE39
      CE39000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000104200000000000018630000920100000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000008F318F351642B956DA5AFA5E3A633A63
      3A633A675B6B5B6B3A67B9566F31000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000CE39CE39913157299931F93D7A4A9C525E4A3D461A42B9357931F620F11C
      0C1DCE39CE390000000000000000000000000000000000000000000000000000
      0000000000000000000010420000000000000D7F000000005F470000F7010000
      1042000000000000000000000000000000000000000000000000000000000000
      00000000000000000000531153118F31F5419952B956794E16421642794E3A63
      5B6B5B6B5B6B7B6F7B6F7B6F98526F3100001042186310421042104210421042
      1042104210421042104210421042104210421042104210421042104210421042
      1042104210421042104210421042104200000000000000000000000000000000
      CE39B135B9357A4A1E5FFF5EDF5ABF56BF529F529F4E7F4E5F4A5E4A1D429931
      5629F11C0C1DCE39000000000000000000000000000000000000000000000000
      00000000000010420000000000000D7F0D7F0D7F00005F475F470000F7010000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000091215311DA098F35F54199529852F541F5415546F95EDA5AF95E
      5B6B5B6B7B6F9B739C779C779B7398566F311042FF7F18639452945294529452
      9452945294529452945294529452945294529452945294529452945208219452
      945208210821082108219452945210420000000000000000000000000000CE39
      D4397A4A3E631F5FFF5EFF5ADF5ABF56DF56BF569F527F4E5F4A3E463D461C42
      FC3D9931772DEC18CE3900000000000000000000000000000000000000000000
      0000104200000000000010420D7F0D7F0D7F0D7F0D7F00005F475F470000F701
      0000104200000000000000000000000000000000000000000000000000000000
      0000000053115311B91D7315AE39584A9952794E354635463546D95A3A639856
      5B6B7B6F9B73BD77BD77BE779C777B6F6F311042FF7F18639452945294529452
      94529452945294529452945294529452945294529452945294529452E0039452
      94529452945294529452945294521042000000000000000000000000CE39764E
      DD5A3E633F631F5F1F5F1F633E635E675E677E6B7E6B7E6B5E67DE5ABD521C42
      FB3DDB399831772DEC18CE390000000000000000000000000000000000001042
      00000000104210420D7F0D7F0D7F0D7F0D7F0D7F0D7F00005F475F470000F701
      0000000000000000000000000000000000000000000000000000000000000000
      53115311B91DB91D7315F300D739784A98529952164216423542574A5B6B574A
      5B6B9B739C77BE77DE77DE77BD779B736F311042FF7F18639452104210421042
      1042104210421042104210421042104210421042104210421042104210421042
      1042104210421042104210429452104200000000000000000000CE3915461D5F
      5E673F631F633F633F633F633E633E635E675E675E675E675E677E6B7E6B7E6B
      1E5F1A42B935782D1725CE39CE39000000000000000000000000104200000000
      00000000F701F701104210420D7F0D7F0D7F0D7F0D7F0D7F00005F4700005B02
      5B020000104200000000000000000000000000000000AD3D8F356F3191215311
      7701360115011305F300F400F541784A99569956F63D1642164216423A63584A
      5B6B9B73BD77DE7BDE77DE77BE777B6F6F311042FF7F18639452FF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7F9452104200000000000000000000F441DD5A5E67
      1F5FFF5E1F5F1F5F1F5F1F5F1F633F633F633F633E633E635E675E675E675E67
      5E677E6BBC5298315729F31CCE39000000000000000010420000000000000D7F
      0D7F0D7FF701FF26F701104210420D7F0D7F0D7F0D7F0D7F00000000606F0000
      5B02000000000000000000000000000000000000445A445A43624362DA113601
      F300F400F400F400F40015018F3516429952B956D739D739F63DF63DB9569956
      FA5E9B73BD77DE7BDE777B6FBD776F3100001042FF7F18639452000018639031
      9031903190311863903190319031903118639031903190319031186390319031
      90319031903190319031FF7F945210420000000000000000CE3935467E6BFF5E
      DF5ADF5ADF5AFF5AFF5AFF5E1F5F1F5F1F5F1F5F1F633F633F633F633E633E63
      5E675E675E67BC52772D36250C1DCE39000000000000000000000D7F0D7F0D7F
      0D7F0D7FF701DF6BFF26F701104210420D7F0D7F0D7F0D7F0D7F0000606FE07F
      0000BF0200001042000000000000000000006D6B45524362436255467701F400
      F400F400F400F4001501810181019952584A9956B739B739D739D739584A9956
      99527B6F3A639952D739584A3A63000000001042FF7F18639452000018631863
      1863186390311863186318631863903118631863186318639031186318631863
      18639031903190319031FF7F94521042000000000000000035461D5FFF5ABF56
      BF56BF56DF56DF56DF5ADF5ADF5AFF5AFF5AFF5EFF5E1F5F1F5F1F5F1F633F63
      3F633F633E633E639C523625F11CCE390000104200000D7F0D7F0D7F0D7F0D7F
      0D7F0D7F0D7FF701DF6BFF26F701104210420D7F0D7F0D7F0D7F0000E07FE07F
      0000BF0200000000000000000000000000008C7B43624362E8451305F400F400
      F40015013601360136018101A101A10198529852B831B831B735B735D739584A
      584AF63D794E3A637E47784ADA5A000000001042FF7F18639452000090219021
      90219021FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7F9021FF7F94521042000000000000000035465E679F529F52
      9F52BF52BF52BF56BF56BF56DF56DF56DF5ADF5ADF5AFF5AFF5AFF5EFF5E1F5F
      1F5F1F5F1F633F633F635A4A15213025CE390000000000000D7F0D7F0D7F0D7F
      0D7F0D7F0D7F0D7FF701DF6BFF26F701104210420D7F0D7F0D7F0D7F0000FA7F
      FA7F00001F370000104200000000000000008C7B8C7B8F317315F400F4001501
      360156017701770197018101A101A101A1019852B831B831B831B831B735F63D
      B956BE5B5F3BBF1ABF1A584ADA5A000000001042FF7F1863945200009021FF7F
      FF7F9021FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7F9021FF7F9452104200000000000000003546DF567F4E7F4E
      9F4E9F529F529F529F52BF52BF52BF56BF56BF56DF56DF56DF5ADF5ADF5AFF5A
      FF5AFF5EFF5E1F5F1F5FBF5256292D21CE390000104200000D7F0D7F0D7F0D7F
      0D7F0D7F0D7F0D7F0D7FF701DF6BFF26F7011042000010420D7F0D7F0000FA7F
      000010421F370000000000000000000000008C7B8C7B6B7BCB41330936015601
      77019701B805B9059801C101C101C101220A220AB735B82DB831584AB9567E47
      FF26BF1ABF1ABF1ABF1A7A3AB956000000001042FF7F1863945200009021FF7F
      E07F9021FF7FFF7FFF7F10731073107310631063106310638862886288628052
      80518051FF7FFF7F9021FF7F945210420000000000000000764E7F4E7F4E7F4E
      7F4E7F4E7F4E7F4E9F4E9F529F529F529F52BF52BF52BF56BF56BF56DF56DF56
      DF5ADF5ADF5AFF5AFF5ADF5A56293025CE3900000000000000000D7F0D7F0D7F
      A07EA07EA07EA07EA07E0D7FF701DF6BFF26F701406E000010420D7F0D7F0000
      1F7C00001F371F37000010420000000000008C7B6B7B6B7B497B467F90259701
      B905B905B905B905E105E105E105C101E105584AD739784AB94A1E2FDF1EDF1A
      DF1ADF1ADF1ABF1ABF1A7A3A9952000000001042FF7F18639452000090219021
      90219021FF7FFF7FFF7F10731073107310631063106310638862886288628052
      80518051FF7FFF7F9021FF7F9452104200000000CE39CE3916465F4A5F4A5F4A
      5F4A5F4A7F4E7F4E7F4E7F4E7F4E7F4E9F4E9F529F529F529F52BF52BF52BF56
      BF56BF56DF56DF56DF5ADF5ADA3D2D21CE3900000000104200000D7F0D7F984E
      D239702D902DB135A07EA07E0D7FF701DF6BFF26F701406E000010420D7F0000
      3F7D3F7D00001F37000000000000000000008C7B6B7B497B467F257F257F436A
      AD3D9801B905DA0998014552E105584AF63D9A429A3EFF26DF1EFF22FF22FF22
      FF22DF1EDF1EDF1EDF1A9C2ADA5A000000001042FF7F1863945200009021FF7F
      FF7F9021FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7F9021FF7F945210420000913114095519BA353E463E463E46
      5E4A5F4A5F4A5F4A5F4A5F4A7F4E7F4E7F4E7F4E7F4E7F4E9F4E9F529F529F52
      9F52BF52BF52BF56BF56BF56FA413025CE390000000000000000984EEA1CAA14
      5125B435B73156297A29A07EA07E0D7FF701DF6BFF26F701406E000010420D7F
      00003F7D00001F371F3700001042000000009C778C7B467F257F257F037FE27E
      E27EC17ECB4133093309827AB8317A3A9C2EDF1EFF22FF22FF26FF26FF26FF26
      FF26FF26FF22FF22DF1EBD22B956FA5E00001042FF7F1863945200009021FF7F
      E07F9021FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F90219021902190219021
      90219021FF7FFF7F9021FF7F945210420000560D990198019931993199319931
      99319931993199319931993199319931993199313E467F4E7F4E7F4E7F4E7F4E
      9F4E9F529F529F529F52BF52FA41CE390000000000000000974A2800974EFB5E
      1C631D637F6F7F6F56297A29A07EA07E0D7FF701DF6BFF26F701406E00000D7F
      00003F7E3F7E00001F37000000000000000000009B736C73257F037FE27EE27E
      E27EE27EC17EA27E264E827AB831DF1EFF22FF26FF261F2B1F2B1F2B1F2B1F2B
      1F2B1F2B1F2BFF26FF26FF269956DA5A00001042FF7F18639452000090219021
      90219021FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F90219021902190219021
      90219021FF7FFF7F9021FF7F945210420000760D990199019901990199019901
      990199019901990199019901BA012925CE39D539F63D5F4A5F4A5F4A7F4E7F4E
      7F4E7F4E7F4E7F4E9F4E9F52DA3DCE390000000000003B5F00001B671B673C6B
      5D6B3E6B9F6F9F6F9F6F56297A29A07E0D7F0D7FF701DF6BFF26406E00420000
      0D7F00003F7E00001F371F370000104200000000BD779C329025E27EE27EE27E
      E27EC17EA27EA27E827A4276F63DFF26FF261F2B1F2B1F2F3F333F333F333F33
      3F333F331F2F1F2B1F2BFF26794EB95600001042FF7F1863945200009021FF7F
      E07F9021FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F9021FF7F9021FF7FFF7F
      FF7F9021FF7FFF7F9021FF7F94521042000036251F4B1F4B1F4B1F4B1F4B1F4B
      1F4B1F4B1F4B1F4B1F4B1F4B9D2EEF10CE390000D5393E463E465E4A5F4A5F4A
      5F4A5F4A7F4E7F4E7F4E7F4ECE390000000000000000B1351B671B673B6B3B6B
      5E6FFB5E01043E639F6F3F677A29A07E0D7F0D7F0D7FF701DF6BFF26F7010000
      0D7F00005F7F00001F371F370000000000000000BD777E43B905B905445AE27E
      E27EC17EA27E827A427A427616429C2E1F2B3F333F333F373F375F3B5F3B5F3B
      5F3B3F373F373F333F331F2B9A42FA5E00001042FF7F1863945200009021E07F
      18639021FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F90219021902190219021
      90219021FF7FFF7F9021FF7F94521042000055191F431F431F431F431F431F43
      1F431F431F431F431F431F43DE3AF00CCE390000B335FB3D3E463E463E463E46
      3E465E4A5F4A5F4A5F4A5E4ACE390000000000003B5FB1353B6B5B6F7C777C77
      4D29C61C5E6B9F739F777F6F51256E08517F0D7F0D7F0D7FF7011002F7010000
      0D7F00005F7F00001F371F371F370000104200000000BF53B905DA099805A41E
      4552A27E827A427A42764276584A9B323F333F375F3B5F3B5F3F5F3F5F3F5F3F
      5F3F5F3F5F3B5F3B3F373F339A3E3A673A671042FF7F18639452000090219021
      90219021FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F9021FF7F9021FF7FFF7F
      FF7F9021FF7FFF7F9021FF7F9452104200007331B819DE3ADE3ADE3ADE3ADE3A
      DE3ADE3ADE3ADE3ADE3ADE3ADE3AB90DCE390000134213421D421D421D423D46
      3E463E463E463E463E46CE3900000000000000007C4A37737C777C777C770000
      51259F6F5E6B7F6F9F6F5F6BB135B110517F517F0D7F0D7F0D7FF70100000D7F
      0D7F000000001F371F37104200000000000000000000BE5BDA09DA09DA09A41E
      E726092F4552427642764276784A9B363F375F3B5F3F5F3F7F437F437E477E47
      7E477F437F435F3F5F3B3F379A3E3A67FA5E1042FF7F1863945200009021FF7F
      FF7F9021FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F90219021902190219021
      90219021FF7FFF7F9021FF7F94521042000000005519BD32BD32BD32BD32BD32
      BD32BD32BD32BD32BD32BD32BD32FA152D2100000000D239FC3D1C421C421D42
      1D421D421D423D46CE3900000000000000000000FC39BB7FBC7FBD7F9D7FD466
      A514DF565E6B5F6B5F6B3D67B2356D0C967F517F517F0D7F0D7F0D7F0D7F0000
      00001F371F3710420000000010420000000000000000BE771E2FDA09841AC622
      092F092F092F42764276436A99527A3A5F3B5F3F7F437E479E4B9F4B9F4B9F4B
      9F4B9E4B7E477F435F3F5F3B5F3BFA5EF95E1042FF7F1863945200009021FF7F
      E07F9021FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F9021FF7F9021FF7FFF7F
      FF7F9021FF7FFF7F9021FF7F94521042000000007331760D7D2A7D2A7D2A7D2A
      7D2A7D2A7D2A7D2A7D2A7D2A7D2A3B1EEF10CE390000D239BA35FC3DFC3DFC3D
      FC3D1C421C42CE390000000000000000000000007C4A396FDC7FDD7FDD7FBE7F
      ED417431DF5E7D735D6BFB5E3325AD35967F967F517F517F517F000000001F37
      1F3710420000000010420000000000000000000000000000B94A6316C622E726
      092F092F4F3F42764E5F4F4BB9567A3A5F3F7F439E4B9F4B9F4FBF53BF53BF53
      BF539F4F9F4B9E4B7F435F3F5F3BD95AF95E1042FF7F18639452000090219021
      90219021FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F90219021902190219021
      90219021FF7FFF7F9021FF7F94521042000000000000B335B8155C225C225C22
      5C225C225C225C225C225C225C225C22560DCE390000F43D1342DB39DB39DB39
      FB3DFB3DCE3900000000000000000000000000003E63143EFD7FFE7FFE7FDF7F
      D466C61CFF5ABE563B67B652F518967F967F967F967F000000001F371F371042
      000000001042000000000000000000000000000000000000BE770B33C622092F
      092F4F3F4F47BE5BDE5FDE73DA5AD7397F437E479F4BBF53BF53BF57BF57BF57
      BF57BF53BF539F4B9E4B7F435F3FD94E5B6B1042FF7F1863945200009021FF7F
      FF039021FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7F9021FF7F9452104200000000000000007331760D1B1A3B1E
      1B1A1B1A1B1A3B1E1B1A3B1E1B1A1B1A1405CE39000000001342BA35DA3DDB39
      CE39CE39000000000000000000000000000000000000D418FF7FFE7FFE7FFE7F
      7A77B25E2B25BB5AB9563121AD35967F967F000000001F371F37104200000000
      10420000000000000000000000000000000000000000000000007B6F092F092F
      0B334F3F4F4BBE5BDF63DF63DA5AD7397E479F4B9F4FBF57BF57DF63DF63DF63
      DF63BF57BF57BF539F4B7E477F43574ABE771042FF7F1863945200009021FF03
      E07F90213E533E533E533E533E533E533E533E533E533E533E533E533E533E53
      3E533E533E533E539021FF7F94521042000000000000000000007331760DB90D
      1B12FA151B12DA111B12DA111B12DA119709CE3900000000D239CE39CE39CE39
      00000000000000000000000000000000000000000000FF5EB114FE7FFC7FFC7F
      BC7F925A176BF6662E21AD35967F000000001F371F3710420000000010420000
      00000000000000000000000000000000000000000000000000000000F95E1E2F
      0D332E3B4F477E43BE5BBE5BDA5AF63D7E479F4FBF53BF57DF63DF63DF67DF67
      DF63DF63BF57BF539F4F9E4B7E47BA46BD771042FF7F18639452000090219021
      902190213E533E533E533E533E533E533E533E533E533E533E533E533E533E53
      3E533E533E533E539021FF7F9452104200000000000000000000000000005519
      35099801DA09DA09DA09DA09DA09DA09B909CE39000000000000000000000000
      000000000000000000000000000000000000000000000000BE568F0CB6529977
      59775056F03DB114AD35000000005F475F471042000000001042000000000000
      0000000000000000000000000000000000000000000000000000000000003A67
      4F3FDF1E2E3B7E437E477E43DA5A584A7E479F4FBF57BF57DF63DF67DF67DF67
      DF67BE5BD952B94E564A554655465546BD771042FF7F18639452000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000FF7F9452104200000000000000000000000000000000
      00005519760D560D560D35015701780178017801000000000000000000000000
      0000000000000000000000000000000000000000000000000000FB5E7C4A5629
      1421F2180E1D000000009F5B9F5B9F5B00000000104200000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      BD77BF531E2F2E3B4F3F4F47DA5A584ABA46BF53BF57DF63BE5B3A63D952574A
      3546354635469856F95E3A637B6FBE777B6F1042FF7F18639452945294529452
      9452945294529452945294529452945294529452945294529452945294529452
      9452945294529452945294529452104200000000000000000000000000000000
      00000000000000000000F43DF43DF43DF43D0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000010420000DF6BDF6B10420000000010420000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000007B6F9E4B4F471E2FDA5A1642584ABA46574A16423542564A9952DA5A
      3A637B6F5B6B3A63F95EF95EF95E3A6700001042FF7F18631863186318631863
      1863186318631863186318631863186318631863186318631863186318631863
      1863186318631863186318631863104200000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000001042000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000BD77DF6B7E47F54116429952B956FA5E3A63FA5ED95AF95E
      F95E3A637B6FBD77000000000000000000001042FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7F186300000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000104200000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000574AB956DA5A3A637B6FBD7700000000
      0000000000000000000000000000000000001042104210421042104210421042
      1042104210421042104210421042104210421042104210421042104210421042
      1042104210421042104210421042104200000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000001042104210420000104210421042
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000001F0010001F001042000000000000
      1042104200000000104200000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000007C00400000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000010001F0010000000104210421042
      0000000010421042000010421042104210421042104210421042104210421042
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000007C007C007C00400040004000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000001F0000001F001042104210421042
      1042104200000000000000000000000000000000000000000000000000001042
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FF03
      FF03FF03FF03FF03FF03FF030000000000000000000000000000000000000000
      000000000000007C007C007C007C007C00400040004000001040000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000010001F0010001042186318631863
      104210420000004000400040007C007C007C007C007C007C007C007C00001042
      0000000010421042104200001042104210420000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FF030000FF03
      FF03FF03FF03FF03FF03FF030000000000000000000000000000000000000000
      0000007C007C007C007C007C007C100210020040004000001F7C104010400000
      0000000000000000000000000000000000001042004200420042004200420042
      0042004200420042004200420042004200420042004200420042004200420042
      0042004200420042004200420042004200001F0010001F001863186318631863
      186318631042000000400040007C007C007C007C007C007C007C007C00001042
      0000104200000000000010421F0010001F000000000000000000000000000000
      000000000000000000000000000000000000000000000000FF03FF030000FF03
      FF03FF03FF03FF03FF03FF03000000000000000000000000000000000000007C
      007C007C007C007C007C10021002FF03FF031002100200001F7C104010400000
      0000000000000000000000000000000000001042FF7FE07F1863E07F1863E07F
      1863E07F1863E07F1863E07F1863E07F1863E07F1863E07F1863E07F1863E07F
      1863E07F1863E07F1863E07F18630042000010001F001000FF7F186318631863
      18631863186310420000000000400040007C007C007C007C007C007C00001042
      00000000104210421042000010001F0010000000000000000000000000000000
      00000000000000000000000000000000000000000000FF03FF03FF030000FF03
      FF03FF03FF03FF03FF03FF030000000000000000000000000000007C007C007C
      007C007C007C100010001002FF03FF03FF031002100200001F7C104010400000
      0000000000000000000000000000000000001042FF7F1863E07F1863E07F1863
      E07F1863E07F1863E07F1863E07F1863E07F1863E07F1863E07F1863E07F1863
      E07F1863E07F1863E07F1863E07F004200001F0010001F001042FF7F18631863
      186318631863186318631863000000000040007C007C007C007C007C00000000
      1042104210421042104210421F0000001F000000000000000000000000000000
      00000000000000001F001F001F001F0000020000FF03FF03FF03FF0300000000
      00000000000000000000000000000000000000000000007C007C007C007C007C
      007C100010001F001F001002FF03FF03FF031002100200001F7C104010400000
      0000000000000000000000000000000000001042FF7FE07F1863E07F1863E07F
      1863E07F0000000000000000000000000000E07F1F00E07F1F00E07F1F00E07F
      1863E07F1863E07F1863E07F18630042000010001F00100000421042FF7F1863
      186318630000000000001863186318630000007C007C007C007C004000001042
      10421042186318631863104210001F0010000000000000000000000000000000
      00001F001F001F001F001F001F001F000000FF03FF03FF03FF03FF0300000000
      0000000000000000000000000000000000000040007C007C007C007C007C1000
      10001F001F001F001F001002FF03FF03FF031002100200001F7C104010400000
      0000000000000000000000000000000000001042FF7F1863E07F1863E07F1863
      E07F104210421042104210421042104200001F00E07F1F00E07F1F00E07F1F00
      E07F1863E07F1863E07F1863E07F00420000000000001042FF7FE07F1042FF7F
      FF7F1863104200400040000000000000007C007C007C00400040000010421042
      1863186318631863186318631F0010001F000000000000000000000000001F00
      1F001F001F001F001F001F001F001F000000FF03FF03FF03FF03FF0300000000
      0000000000000000000000000000000000000040007C007C007C007C007C1000
      1F001F001F001F001F001002FF03FF0310021002100200001F7C104010400000
      0000000000000000000000000000000000001042FF7FE07F1863E07F1863E07F
      186310421863186318631863186310420000E07F1863E07F1863E07F18631F00
      1863E07F1863E07F1863E07F186300420000000000001042FF7F1863E07F1042
      FF7FFF7F1863004000400040007C007C007C0040004000000000104218631863
      18631863186318631863FF7F10001F001000000000000000000000001F001F00
      1F001F00FF7F1F001F001F001F001F000000FF03FF03FF03FF03FF0300000000
      0000000000000000000000000000000000000040007C007C007C007C007C1000
      1F001F001F001F001F00100210021002FF03FF03FF0300001F7C104010400000
      0000000010020000000000000000000000001042FF7F1863E07F1863E07F1863
      E07F1863104210421042104210420000E07F1863E07F1863E07F1863E07F1F00
      E07F1863E07F1863E07F1863E07F00420000000000001042FF7FE07F1863E07F
      1042FF7FFF7F004000400040007C007C00400000000018631863186318631863
      1863186318631863FF7F10421F0010001F0000000000000000001F001F001F00
      1F00FF7FFF7F1F001F001F001F001F000000FF03FF03FF03FF03FF0300001F00
      0000000000000000000000000000000000000040007C007C007C007C007C1000
      1F001F001F001F001F0010001002FF03FF03100210021F7C1F7C104010400000
      000000001002FF03FF03FF03FF03FF0300001042FF7FE07F1863E07F1863E07F
      1863E07F1042FF03FF03FF03104200001863E07F186300000000000018631F00
      1863E07F1863E07F1863E07F186300420000000000001042FF7F1863E07F1863
      E07F10421042004000400040007C007C00001863186318630000000000001863
      186318631863FF7F1042186310001F00100000000000000000001F001F001F00
      FF7F1F001F001F001F000000000000000000FF03FF03FF03FF03FF0300000000
      0000000000000000000000000000000000000040007C007C007C007C007C1000
      1F001F001F001F00E00300020002000010021F7C1F7C1F7C1F7C104010400000
      0000000010021002100210021002100210421042FF7F1863E07F1863E07F1863
      E07F18631042FF03FF03FF0310420000E07F0000000018631863104200000000
      E07F1863E07F1863E07F1863E07F00420000000000001042FF7FE07F1863E07F
      1863E07F1863004000400040007C007C007C0000000000000040007C00001042
      1863FF7FFF7F10421863E07F1863004200000000000000001F001F001F00FF7F
      1F00FF7F1F001F001F0000020000FF03FF03FF03FF03FF03FF03FF03FF03FF03
      0000000000000000000000000000000000000040007C007C007C007C007C1000
      1F001F00E003E003E0030002000200001F7C1F7C1F7C1F7C1040104010400000
      0000000000001002100210021002100210021042FF7FE07F1863E07F1863E07F
      1863E07F10421042104210421042E07F18631042186318630000000010421042
      0000E07F1863E07F1863E07F186300420000000000001042FF7F1863E07F1863
      E07F1863E07F004000400040007C007C007C007C007C007C007C007C00001042
      FF7FFF7F10421863E07F1863E07F004200000000000000001F001F001F001F00
      1F001F001F001F001F00000200020000FF03FF03FF03FF03FF03FF03FF030000
      1F00000000000000000000000000000000000040007C007C007C007C007C1000
      E003E003E003E003E0030002000200001F7C1F7C104010401F7C1F7C1F7C0000
      0000000000000000000000000000000000001042FF7F1863E07F1863E07F1863
      E07F1863E07F1863E07F1863E07F1863E07F104200420042E07F004200000000
      00001863E07F1863E07F1863E07F00420000000000001042FF7FE07F1863E07F
      1863E07F1863004000400040007C007C007C007C007C007C007C007C00001863
      FF7F10421863E07F1863E07F186300420000000000001F001F001F001F001F00
      1F001F001F000002000200021F0000020000FF03FF03FF03FF03FF0300001F00
      1F001F0000000000000000000000000000000040007C007C007C007C007C0002
      E003E003E003E003E003000200020000104010401F7C1F7C1F7C104010400000
      0000186300000000000000000000000000001042FF7FE07F1863E07F1863E07F
      1863E07F1863E07F1863E07F1863E07F1863E07F1042E07F1863E07F00000042
      0000E07F1863E07F1863E07F186300420000000000001042FF7F1863E07F1863
      E07F1863E07F004000400040007C007C007C007C007C007C007C007C00001042
      10421863E07F1863E07F1863E07F00420000000000001F001F001F001F001F00
      1F001F00000200021F0000021F001F001F000000FF03FF03FF0300001F001F00
      1F00000200000000000000000000000000000040007C007C007C007C007C0002
      E003E003E003E003E0030002000200001F7C1F7C1F7C10401040000000000000
      1863000018630000000000000000000000001042FF7F1863E07F1863E07F1863
      E07F1863000000000000000000000000000018631042104200420000E07FE07F
      00001863E07F1863E07F1863E07F00420000000000001042FF7FE07F1863E07F
      1863E07F1863004000400040007C007C007C007C007C007C007C007C0000E07F
      1863E07F1863E07F1863E07F186300420000000000001F0000021F001F001F00
      1F000002000200021F001F001F001F001F0000020000FF0300001F001F001F00
      1F00000200000000000000000000000000000040007C007C007C007C007C0002
      E003E003E003E00300020002000200000042104010400000000000000000E003
      0002186300021863000000000000000000001042FF7FE07F1863E07F1863E07F
      1863104210421042104210421042104200001F001F001042E07FE07F10421042
      1863E07F1863E07F1863E07F186300420000000000001042FF7F1863E07F1863
      E07F1863E07F1863E07F1863E07F1863E07F1863E07F1863E07F1863E07F1863
      E07F1863E07F1863E07F1863E07F00420000000000001F001F001F001F001F00
      1F0000020002000200021F001F0000021F001F001F0000001F001F001F001F00
      1F00000200000000000000000000000000000040007C007C007C007C007C0002
      E003E00300020002FF7FE003E0030000004200000000000000000000E0030002
      FF7F000218630000000000000000000000001042FF7F1863E07F1863E07F1863
      E07F104218631863186318631863104200001863E07F186310421042E07F1863
      E07F1863E07F1863E07F1863E07F00420000000000001042FF7FE07F1863E07F
      1863E07F1863E07F1863E07F1863E07F1863E07F1863E07F1863E07F1863E07F
      1863E07F1863E07F1863E07F186300420000000000001F001F001F001F001F00
      000200020002000200020002000200021F001F001F00FF7F1F001F001F001F00
      1F00000200000000000000000000000000000040007C007C007C007C007C0002
      00020002FF7FE003E00300020002004200420000000000000000E0030002FF7F
      0002186300000000000000000000000000001042FF7FE07F1863E07F1863E07F
      1863E07F1042104210421042104200001863E07F1863E07F1863E07F1863E07F
      1863E07F1863E07F1863E07F186300420000000000001042FF7F1863E07F1863
      E07F1863E07F1863E07F1863E07F1863E07F1863E07F1863E07F1863E07F1863
      E07F1863E07F1863E07F1863E07F00420000000000001F001F001F001F001F00
      0002000200020002000200020002000200021F001F00FF7FFF7FFF7F1F001F00
      1F001F0000000000000000000000000000000040007C007C007C007C007C0002
      FF7FE003E00300020002E07FE07F00420042000000000000E0030002FF7F0002
      1863000000000000000000000000000000001042FF7F1863E07F1863E07F1863
      E07F18631042FF03FF03FF0310420000E07F1863E07F1863E07F1863E07F1863
      E07F1863E07F1863E07F1863E07F00420000000000001042FF7FE07F1863E07F
      1863E07F1863E07F1863E07F1863E07F1863E07F1863E07F1863E07F1863E07F
      1863E07F1863E07F1863E07F186300420000000000001F001F001F001F001F00
      00020002000200020002000200020002000200021F001F001F001F001F001F00
      1F001F0000000000000000000000000000000040007C007C007C007C007C007C
      0002000200020000E07FE07FE07F004200420000000000020002FF7F00021863
      0000000000000000000000000000000000001042FF7FE07F1863E07F1863E07F
      1863E07F1042FF03FF03FF03104200001863E07F1863E07F1863E07F1863E07F
      1863E07F1863E07F1863E07F186300420000000000001042FF7F1863E07F1863
      E07F1863E07F1863E07F1863E07F1863E07F1863E07F1863E07F1863E07F1863
      E07F1863E07F1863E07F1863E07F004200000000000000001F001F001F001F00
      00020002FF7F0002FF7FFF7F00021F000002000200021F001F001F001F001F00
      1F00000000000000000000000000000000000040007C007C007C007C007C007C
      007C004000400000E07FE07FE07F004200420000000000000002000218630000
      0000000000000000000018630000000000001042FF7F1863E07F1863E07F1863
      E07F1863104210421042104210421863E07F1863E07F1863E07F1863E07F1863
      E07F1863E07F1863E07F1863E07F00420000000000001042FF7FE07F1863E07F
      1863E07F1863E07F1863E07F1863E07F1863E07F1863E07F1863E07F1863E07F
      1863E07F1863E07F1863E07F1863004200000000000000001F001F001F001F00
      0002FF7FFF7FFF7F0002000200020002000200021F001F001F001F001F001F00
      0000000000000000000000000000000000000040007C007C007C007C007C007C
      0040004000400000E07FE07FE07F004200420000000000000000000000000000
      0000000000000000186300001863000000001042FF7FE07F1863E07F1863E07F
      1863E07F1863E07F1863E07F1863E07F1863E07F1863E07F1863E07F1863E07F
      1863E07F1863E07F1863E07F186300420000000000001042FF7F1863E07F1863
      E07F1863E07F1863E07F1863E07F1863E07F1863E07F1863E07F1863E07F1863
      E07F1863E07F1863E07F1863E07F00420000000000000000000000021F000002
      000200020002000200020002000200021F00000200021F001F00FF7F1F000000
      0000000000000000000000000000000000000040007C007C007C007C00400040
      1F7C007C00400000E07FE07FE07F004200420000000000000000000000000000
      0000000000001863000018630000186300001042FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7F00000000000000001042FF7FE07F1863E07F
      1863E07F1863E07F1863E07F1863E07F1863E07F1863E07F1863E07F1863E07F
      1863E07F1863E07F1863E07F18630042000000000000000000001F0000020002
      00020002000200020002000200021F001F0000021F001F0000021F001F001F00
      0000000000000000000000000000000000000040007C007C004000401F7C007C
      007C00400040E07FE07FE07F0042004200420000000000000000000000000000
      00000000186300001F7C10401863104000001042186318631863186318631863
      1863186318631863186318631863186318631042104210421042104210421042
      104210421042104210421042104200000000000000001042FF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7F00000000000000000000000000001F000002
      0002000200020002000200021F00000200021F001F000002000200021F000000
      0000000000000000000000000000000000000040004000401F7C007C007C0040
      0040E07FE07FE07F00420042FF7FE07F00420000000000000000000000000000
      0000186300001F7C1040FF7F10400000000000001042FF7FE07F1863E07F1863
      E07F1863E07F1863E07F1863E07F186310420000000000000000000000000000
      0000000000000000000000000000000000000000000010421863186318631863
      1863186318631863186318631863186318631863186310421042104210421042
      1042104210421042104210421042000000000000000000000000000000000000
      0002000200021F00000200020002000200021F0000020002FF7F1F0000000000
      00000000000000000000000000000000000000401F7C007C007C004000401042
      E07FE07F00420042FF7FE07FE07F104210420000000000000000000000000000
      186300001F7C1040FF7F1040000000000000000000001042FF7FE07F1863E07F
      1863E07F1863E07F1863E07F1863104200000000000000000000000000000000
      0000000000000000000000000000000000000000000000001042FF7FE07F1863
      E07F1863E07F1863E07F1863E07F1863E07F1863104200000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00001F001F001F001F001F00000200021F001F00000200020000000000000000
      0000000000000000000000000000000000000000004000400040000000001042
      00420042FF7FE07FE07F10421042000000000000000000000000000000001863
      00001F7C1040FF7F104000000000000000000000000000001042FF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7F1042000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000001042FF7FE07F
      1863E07F1863E07F1863E07F1863E07F18631042000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000001F001F00FF7FFF7FFF7F1F000002000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000001042
      FF7FE07FE07F1042104200000000000000000000000000000000000000000000
      1F7C1040FF7F1040000000000000000000000000000000000000104210421042
      1042104210421042104210420000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000001042FF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F10420000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      1042104210420000000000000000000000000000000000000000000000001040
      1040FF7F10400000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000001042
      1042104210421042104210421042104200000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      1040104000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      1863104200000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000104210421042
      1042104210421042104210421042104210421042104210421042104210421042
      1042104210421042104210420000000000000000000000000000000000000000
      0000186310420000000000000000104218630000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000001042
      1042000000000000000000000000104200000000104210420000000000000000
      0000000000000000000000000000000000000000000000000000000000001863
      0040104210420000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000001042FF7F1863
      1863186318631863186318631863186318630000186318631863186318631863
      1863186318631863186310420000000000000000000000000000000000000000
      1863104218630000000000000000000010421863000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000104200000000000000000000004210001042000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000001863
      0040004000401042104210420000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000001042FF7F1863
      1863186318631863186318631863186318630000186318631863186318631863
      1863186318631863186310420000000000000000000000000000000000001863
      1042104200000000000000000000000000001042186300000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000018630000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000018630040
      FF7F000000000040004000401042104210420000000000000000000000000000
      00000000000000000000000000000000000000000000000000001042FF7F1863
      1863000010421863186310420000FF0318630000186318631863186318631863
      1863186300001863186310420000000000000000000000000000000018631042
      1042104210420000000000000000000000000000104218630000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000001042000000000000000000000000104200000000000000000000
      0000000000000000000000000000000000000000000000000000000018630040
      1863FF7F18630000000000000040004000401042004000000000000000000000
      00000000000000000000000000000000000000000000000000001042FF7F1863
      1863186318631863186318631863186318630000186318631863186318631863
      1863186318631863186310420000000000000000000000000000186310421042
      1042104210421042000000000000000000000000000010421863000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000104218631863104210421042104210421042000000000000000000000000
      0000000000000000000000000000000000000000000000000000186300401863
      FF7F1863FF7F1863104218630000000000000040004000401042104210421042
      10421042000000000000000000000000000000000000000000001042FF7F1863
      1863000010421863104200001042186318630000186318631863FF0310420000
      1042104200001863186310420000000000000000000000001863104210421042
      1042104210421042104200000000000000000000000000001042186300420042
      0042004200420042004200420000000000000000000000000000000000001042
      1042186300000000000000000000000010420000000000000000000000000000
      000000000000000000000000000000000000000000000000000018630040FF7F
      1863FF7F1863FF7F186310421863104218630000000000000040004000400040
      00400040004000401042000000000000000000000000000000001042FF7F1863
      1863186318631863186318631863186318630000186318631863186318631863
      1863186318631863186310420000000000000000000018631042104200001042
      1042104210421042104210420000000000000000000000000000104218630000
      0000000000000000000000000042000000000000000000000000000010421863
      18630000000000001F0000001F00000000000000104200000000000000000000
      00000000000000000000000000000000000000000000000018630040FF7F1863
      FF7F1863FF7F1863FF7FE07F1863186318630000186318630000000000000000
      00000000000000400040000000000000000000000000000000001042FF7F1863
      1863104200001042186318631863186318630000186318631863186318631863
      1042000018631863186310420000000000000000186310421042000000000000
      1042104210421042104200000000000000001863000000000000000010421863
      1042104210421042104200000042000000000000000000000000104218630000
      000000001F00000000001F0000001F0000000000000000001042000000000000
      0000000000000000000000000000000000000000000018630040FF7F1863FF7F
      1863FF7F1863FF7FE07FFF7FE07FFF7F000018631863FF7F1863104210421042
      18631863186300000040004200420042000000000000000000001042FF7F1863
      1863FF0310421042186310421042186318630000186318631863186318631863
      1863186318631863186310420000000000001863104210421042000000000000
      0000104210421042000000000000000018631863186300000000000000001042
      1863FF7FFF7FFF7F104200000042000000000000000000001042186300000000
      00001F0000001F001F0000001F00000000400000000000000000004200420042
      00420042004200420042004200420042000000000000186300401863FF7F1863
      FF7F1863FF7F1863FF7FE07FFF7FE07F1042186318631863FF7F18631863E07F
      FF7FE07F186318630000E07F18630042000000000000000000001042FF7F1863
      1863186318631863186318631863186318630000186318631863186310421863
      1863104200001863186310420000000000001042186300001042104200000000
      0000104210420000000000000000186318631863186318630000000000000000
      10421863E07FFF7F104200000042000000000000000000001042186300000000
      1F0000001F00000000001F00000000400042004000000000000010421863E07F
      1863E07F1863E07F1863E07F1863004200000000186300401863FF7F1863FF7F
      1863FF7F1863FF7FE07FFF7FE07F0000186318631863FF7F1863FF7FE07FFF7F
      E07FFF7FE07F0000E07F1863E07F0042000000000000000000001042FF7F1863
      1863186318631863186318631863186318630000186318631863186318631863
      1863186318631863186310420000000000000000000000000000104210420000
      10421042000000000000000018631863186318631863FF7FFF7F000000000000
      00001042FF7FFF7F104200000042000000000000000010421863000000001F00
      00001F0000001F001F00000000420042000200420040000000000000E07F1863
      E07F1863E07F1863E07F1863E07F00420000000018630040FF7F1863FF7F1863
      FF7F1863FF7FE07FFF7FE07FFF7F104218631863FF7F1863FF7F1863FF7FE07F
      FF7FE07F104200001863E07F18630042000000000000000000001042FF7F1863
      1863104200001042FF0318631863186318630000186318631863104200001042
      1863FF0300001863186310420000000000000000000000000000000010421042
      1042000000000000000018631863186318631863FF7FFF7FFF7FFF7F0000FF7F
      00000000E07FFF7F10420000004200000000000000001042186300001F000000
      1F0000001F00000000000042004000020040000200001F00000000001042E07F
      1863E07F1863E07F1863E07F18630042000000000040FF7F1863FF7F1863FF7F
      1863FF7FE07FFF7FE07FFF7F000018631863FF7F1863FF7F1863FF7FE07FFF7F
      E07F186300001863E07F1863E07F0042000000000000000000001042FF7F1863
      1863186318631863186318631863186318630000186318631863186318631863
      1863186318631863186310420000000000000000000000000000000000001042
      000000000000000018631863186318631863FF7FFF7FFF7FFF7FFF7FFF7F0000
      00000000FF7FFF7F1042000000420000000000000000104200001F0000001F00
      00001F0000001F001F0000400002004000020040000200000000000000001863
      E07F1863E07F1863E07F1863E07F00420000000000401863FF7F1863FF7F1863
      FF7F1863FF7FE07FFF7FE07F1042186318631863FF7F1863FF7FE07FFF7FE07F
      FF7F00001863E07F1863E07F18630042000000000000000000001042FF7F1863
      18630000104210421042FF031863186318630000186318630000104218631042
      1042FF0300001863186310420000000000000000000000000000000000000000
      00000000000018631863186318631863FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      00000000E07FFF7F1042000000420000000010421042186300001F0000001002
      0042004200421F000042004000420040004200401F000000000000000000E07F
      1863E07F1863E07F1863E07F18630042000000401863FF7F1863FF7F1863FF7F
      1863FF7FE07FFF7FE07F0000186318631863FF7F1863FF7FE07FFF7FE07FFF7F
      18630000E07F1863E07F1863E07F0042000000000000000000001042FF7F1863
      1863186318631863186318631863186318630000186318631863186318631863
      1863186318631863186310420000000000000000000000000000000000000000
      0000000018631863186318631863FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F1863
      0000FF7FFF7FFF7F104200000042000000000000104218630000000000420002
      100200420042100200400002004000420002004200001F000000000000001863
      E07F1863E07F1863E07F1863E07F0042000018631042186310421863FF7F1863
      FF7F1863FF7F1863FF7F104218631863FF7F1863FF7F1863FF7FE07FFF7FE07F
      0000E07F1863E07F1863E07F18630042000000000000000000001042FF7F1863
      1863186318631863186318631863007C18630000186310421042186318631863
      1863186318631863186310420000000000000000000000000000000000000000
      000018631863186318631863FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F18631042
      FF7FFF7FE07FFF7F1042000000420000000010421042186300001F0000020042
      00420042000200420042004000020040004200001F000000000000000000E07F
      1863E07F1863E07F1863E07F186300420000000000001042FF7F10421863FF7F
      1863FF7F1863FF7F104218631863FF7F1863FF7F1863FF7FE07FFF7FE07F1042
      00001863E07F1863E07F1863E07F0042000000000000000000001042FF7F1863
      00000000000000000000000000000000007C007C004010421042104210420000
      0000000000000000186310420000000000000000000000000000000000000000
      18631863186318631863FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F18631042FF7F
      E07FFF7FFF7FFF7F104200000042000000000000000010421F00000000421002
      004200420042000200400042004010020040100200001F000000000000001863
      E07F1863E07F1863E07F1863E07F00420000000000001042FF7FE07F18631042
      10421042104210420000186318631863FF7F1863FF7FFF7FFF7FE07F18630000
      1863E07F1863E07F1863E07F18630042000000000000000000001042FF7F1863
      18631863186318631863186318631863007C007C007C00001042104210421042
      1042186318631863186310420000000000000000000000000000000000000000
      0000186318631863FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F18631042E07FFF7F
      FF7FFF7FE07FFF7F104200000042000000000000000010421863000000000042
      004200021002004210020040100200400002004000420000000000001042E07F
      1863E07F1863E07F1863E07F186300420000000000001042FF7F1863E07F1863
      E07F1863E07F1863104210421042186318631863186318631863186310420000
      E07F1863E07F1863E07F1863E07F0042000000000000000000001042FF7F1863
      186318631863000018631863186318631863007C000010420000104210421042
      1042104210421863186310420000000000000000000000000000000000000000
      000000001863FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F18631042FF7FFF7FFF7F
      E07FFF7FFF7F0000000000000000000000000000000010421863000000001002
      00420042004200420042004200400002004000420000000000000000E07F1863
      E07F1863E07F1863E07F1863E07F00420000000000001042FF7FE07F1863E07F
      1863E07F1863E07F1863E07F186310421042104210421042104210420000E07F
      1863E07F1863E07F1863E07F18630042000000000000000000001042FF7F1863
      1863186310421863000018631863186318630000FF7F18631042000010421042
      1042104210421042104210420000000000000000000000000000000000420000
      000000000000FF7FFF7FFF7FFF7FFF7FFF7FFF7F18631042FF7FFF7FE07FFF7F
      FF7FFF7F00001042104210421042000000000000000000001042186300020042
      000200421042004200420002004200420042004000020000000010421863E07F
      1863E07F1863E07F1863E07F186300420000000000001042FF7F1863E07F1863
      E07F1863E07F1863E07F1863E07F1863E07F1863E07F1863E07F1863E07F1863
      E07F1863E07F1863E07F1863E07F0042000000000000000000001042FF7F1863
      18631863186300001863186318631863186318630000FF7F1863104200001042
      104210421042104210421042104210420000000000000000000000000042E07F
      0000000000000000FF7FFF7FFF7FFF7FFF7F18631042FF7FE07FFF7FFF7FFF7F
      E07F104200001863FF7FFF7F1863104200000000000000001042186310420002
      00421863000200420002004200420042186300420042000200001863E07F1863
      E07F1863E07F1863E07F1863E07F00420000000000001042FF7FE07F1863E07F
      1863E07F1863E07F1863E07F1863E07F1863E07F1863E07F1863E07F1863E07F
      1863E07F1863E07F1863E07F18630042000000000000000000001042FF7F1863
      186318630000186310421863186318631863186318630000FF7F186310420000
      1863104210421042104210421042104200000000000000000000000000421863
      00000000000000000000FF7FFF7FFF7F18631042E07FFF7FFF7FFF7FE07FFF7F
      FF7F10420000FF7F104200001042104200000000000010420000104218631042
      1863000218631863004218631042186318631863004200001863E07F1863E07F
      1863E07F1863E07F1863E07F186300420000000000001042FF7F1863E07F1863
      E07F1863E07F1863E07F1863E07F1863E07F1863E07F1863E07F1863E07F1863
      E07F1863E07F1863E07F1863E07F0042000000000000000000001042FF7F1863
      1863186318630000186318631863186318631863186318630000FF7F18631042
      000018631863104210421042104210420000000000000000000000000042E07F
      000018630000000010420000FF7F18631042FF7FFF7FFF7F0000FF7FFF7FFF7F
      E07F10420000FF7F00001863000010420000000000001042FF7F000010421863
      0040104218631863186318631863186310420042E07F1863E07F1863E07F1863
      E07F1863E07F1863E07F1863E07F00420000000000001042FF7FE07F1863E07F
      1863E07F1863E07F1863E07F1863E07F1863E07F1863E07F1863E07F1863E07F
      1863E07F1863E07F1863E07F18630042000000000000000000001042FF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F0000FF7F1863
      1042000018631863186310421042104200000000000000000000000000421863
      00001863E07F00000000000000000000FF7FFF7FE07FFF7FFF7F0000E07FFF7F
      FF7F10420000FF7F0000FF7F000010420000000000001042FF7FE07F00000000
      0000E07F1863104210421863104218631863E07F1863E07F1863E07F1863E07F
      1863E07F1863E07F1863E07F186300420000000000001042FF7F1863E07F1863
      E07F1863E07F1863E07F1863E07F1863E07F1863E07F1863E07F1863E07F1863
      E07F1863E07F1863E07F1863E07F004200000000000000000000104210421042
      104210421042104210421042104210421042104210421042104210420000FF7F
      186310420000104210421042000000000000000000000000000000000042E07F
      00001863FF7FFF7F000000000000FF7FE07FFF7FFF7FFF7FE07FFF7F00000000
      000010420000FF7F0000FF7F000010420000000000001042FF7F1863E07F1863
      E07F1863E07F1863E07F1863E07F1863E07F1863E07F1863E07F1863E07F1863
      E07F1863E07F1863E07F1863E07F00420000000000001042FF7FE07F1863E07F
      1863E07F1863E07F1863E07F1863E07F1863E07F1863E07F1863E07F1863E07F
      1863E07F1863E07F1863E07F1863004200000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      FF7F186310420000000000000000000000000000000000000000000000421863
      00001863E07FFF7FFF7FFF7FE07FFF7FFF7FFF7FFF7FFF7FFF7F104200000040
      004010420000FF7F0000FF7F000010420000000000001042FF7FE07F1863E07F
      1863E07F1863E07F1863E07F1863E07F1863E07F1863E07F1863E07F1863E07F
      1863E07F1863E07F1863E07F186300420000000000001042FF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7F000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FF7F1863104200000000000000000000000000000000000000000042E07F
      00001863FF7FFF7FE07FFF7FFF7FFF7FE07FFF7FFF7FE07FFF7F00000040007C
      004010420000FF7F0000FF7F000010420000000000001042FF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7F000000000000000010421863186318631863
      1863186318631863186318631863186318631863186310421042104210421042
      1042104210421042104210421042000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000FF7F1863104200000000000000000000000000000000000000421863
      0000186318631863186318631863186318631863186318631863104200400040
      186310420000FF7F0000FF7F1863104200000000000010421863186318631863
      1863186318631863186318631863186318631863186310421042104210421042
      1042104210421042104210421042000000000000000000001042FF7FE07F1863
      E07F1863E07F1863E07F1863E07F1863E07F1863104200000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FF7F18631042000000000000000000000000000000000042E07F
      0000000000000000000000000000000000000000000000000000000000000000
      000010420000FF7F000010421863104200000000000000001042FF7FE07F1863
      E07F1863E07F1863E07F1863E07F1863E07F1863104200000000000000000000
      00000000000000000000000000000000000000000000000000001042FF7FE07F
      1863E07F1863E07F1863E07F1863E07F18631042000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000FF7F00000000000000000000000000000000000000421863
      E07F1863E07F1863E07F1863E07F1863E07F1863E07F1863E07F1863E07F1863
      E07F10420000FF7F1042000000000000000000000000000000001042FF7FE07F
      1863E07F1863E07F1863E07F1863E07F18631042000000000000000000000000
      000000000000000000000000000000000000000000000000000000001042FF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F10420000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000042
      0042004200420042004200420042004200420042004200420042004200420042
      004200420000104218631863104200000000000000000000000000001042FF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F10420000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000001042
      1042104210421042104210421042104200000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000001042
      1042104210421042104210421042104200000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000040004000400040
      0040004000400040004000400040004000400040004000400040004000400040
      0040004000400000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000010421042104210421042
      1042104210421042104210421042000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000001042
      1042104210421042104210421042104210421042104210421042104210421042
      1042104210421042104210421042104200000000004000401863104210421042
      1042104210421042104210421042104210421042104210421042104210421042
      1042104210420000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000001042FF7FE07F1863E07F
      1863E07F1863E07F1863E07F1042000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000001042
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7F104200000000004000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000001863000000000000000000000000000000000000000000000000
      0000000000000000000000000000FF7F18631863000000000000000000000000
      00000000000000000000000000000000000000001042FF7FE07F1863E07F1863
      E07F1863E07F1863E07F18630000104200000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000001042
      1863FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7F1863104200000000000010400000FF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F10421042104210421042
      10421042FF7F0000000000000000000000000000000000000000000000000000
      000000000000000000000000FF7FFF7F18631863186318630000000000000000
      00000000000000000000000000000000000000001042FF7F1863E07F1863E07F
      1863E07F1863E07F186310420000104200000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000001042
      FF7F1863FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7F1863FF7F1042000000001042000010400000FF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F104210421042104210421042FF7F
      FF7FFF7FFF7F0000000000000000000000000000000000000000000000000000
      00000000000000000000FF7FFF7FFF7F18631863186318631863186300000000
      0000000000000000000000000000000000001042FF7F1863E07F1863E07F1863
      E07F1863E07F1863E07F00001042104200000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000001042
      FF7FFF7F1863FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7F1863FF7FFF7F10420000000000000000007C104000001863
      1863186318631863186318631863186318631863186318631863186318631863
      FF7FFF7FFF7F0000000000000000000000000000000000000000000000000000
      0000000000000000FF7FFF7FFF7FFF7F18631863186318631863186318631863
      0000000000000000000000000000000000001042FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7F10420000FF7F000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000001042
      FF7FFF7FFF7F1863FF7FFF7FFF7FFF7FFF7F186318631863186318631863FF7F
      FF7FFF7FFF7F1863FF7FFF7FFF7F104200000000000000401040007C10400000
      FF7FFF7FFF7FFF7FFF7F1042104210421042104210421042FF7FFF7FFF7FFF7F
      FF7FFF7FFF7F0000000000000000000000000000000000000000000000000000
      000000000000FF7FFF7FFF7FFF7FFF7F18631863186318631863186318631863
      1863186300000000000000000000000000001042104210421042104210421042
      104210421042104210421042FF7F00000000000000000000007C007C007C0000
      0000000000000000000000000000000000000000000000000000000000001042
      FF7FFF7FFF7FFF7F1863FF7FFF7F186310421042104210421042104210421863
      FF7FFF7F1863FF7FFF7FFF7FFF7F10420000000010420000007C1040007C1040
      0000186318631863186318631863186318631863186318631863186318631863
      FF7FFF7FFF7F0000000000000000000000000000000000000000000000000000
      00000000FF7FFF7FFF7FFF7FFF7FFF7F18631863186318631863186318631000
      10421863186318630000000000000000000000001042FF7F1863E07F18631042
      FF7FFF7F00000000FF7F0000FF7FFF7F0000000000000000000000000000007C
      007C000000000000000000000000000000000000000000000000000000001042
      FF7FFF7FFF7FFF7FFF7F186318631042FF7FFF7FFF7FFF7FFF7FFF7FFF7F1042
      18631863FF7FFF7FFF7FFF7FFF7F104200000000000000001040007C1040007C
      007C0000104210421042104210421042104210421042FF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7F0000000000000000000000000000000000000000000000000000
      0000FF7FFF7FFF7FFF7FFF7FFF7FFF7F18631863186318631863186310001000
      10001863186318631863186300000000000000001042FF7FE07F1863E07F1863
      00000000FF7FFF7FFF7F0000FF7FFF7F00000000000000000000000000000000
      0000007C00000000000000000000000000000000000000000000000000001042
      FF7FFF7FFF7FFF7FFF7F18631042FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      10421863FF7FFF7FFF7FFF7FFF7F10420000000000000040007C1040007C1040
      007C000018631863186318631863186318631863186318631863186318631863
      FF7FFF7FFF7F0000000000000000000000000000000000000000000000000000
      FF7FFF7FFF7FFF7FFF7FFF7FFF7F186318631863186318631863100010001000
      10001042186318631863186318630000000000001042FF7F1863E07F00000000
      FF7FFF7FFF7FFF7FFF7FFF7F0000FF7FFF7F0000000000000000000000000000
      0000007C00000000000000000000000000000000000000000000000000001042
      FF7FFF7FFF7FFF7F18631042FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7F10421863FF7FFF7FFF7FFF7F104200000000104200001040007C1040007C
      007C00001042104210421042104210421042FF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7F000000000000000000000000000000000000000000000000FF7F
      FF7FFF7FFF7FFF7FFF7FFF7F1863FF7FFF7FFF7F186318631042100010001000
      100010001863186318631863186300000000000000001042FF7FFF7F1042FF7F
      FF7FFF7FFF7FFF7FFF7FFF7F0000FF7FFF7FFF7F000000000000000000000000
      00000000007C0000000000000000000000000000000000000000000000001042
      FF7FFF7FFF7F18631042FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7F10421863FF7FFF7FFF7F10420000000000000000007C1040007C1040
      007C000018631863186318631863186318631863186318631863186318631863
      FF7FFF7FFF7F00000000000000000000000000000000000000000000FF7FFF7F
      FF7FFF7FFF7FFF7FFF7F1863FF7F186310421042FF7FFF7F1863186310421000
      100010001042186318631863186300000000000000000000104210421042FF7F
      FF7FFF7FFF7F00000000FF7FFF7F0000FF7FFF7FFF7F00000000000000000000
      00000000007C0000000000000000000000000000000000000000000000001042
      FF7FFF7F18631042FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7F10421863FF7FFF7F104200000000000000401040007C1040007C
      007C000010421042104210421042FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7F0000000000000000000000000000000000000000FF7FFF7FFF7F
      FF7FFF7FFF7FFF7F1863FF7F18631863E07F186318631042FF7FFF7F18631863
      1042100010001863186318631863000000000000000000000000000000001042
      FF7F00000000FF7F0000FF7FFF7F0000FF7F1042104200000000000000000000
      007C007C007C007C007C00000000000000000000000000000000000000001042
      FF7F18631042FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7F10421863FF7F10420000000010420000007C1040007C1040
      007C000018630000186318631863186318631863186318631863186318631863
      FF7FFF7F00001F7C007C1F7C007C00000000000000000000FF7FFF7FFF7FFF7F
      FF7FFF7FFF7F1863FF7F18631863E07F1863E07F1863E07F18631042FF7FFF7F
      1863186318631863186318631863000000000000000000000000000000000000
      0000FF7FFF7FFF7F0000FF7FFF7FFF7F00000000000000000000000000000000
      0000007C007C007C000000000000000000000000000000000000000000001042
      18631042FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7F10421863104200000000000000001040007C1040007C
      007C00001042FF7F00000000FF7FFF7FFF7FFF7F000000000000000000000000
      FF7FFF7F000000001F7C007C1F7C0000000000000000FF7FFF7FFF7FFF7FFF7F
      FF7FFF7F1863FF7F18631863E07F1863E07F1863E07F1863E07F186318631042
      FF7FFF7F1863186318631863186300000000000000000000000000000000FF7F
      FF7FFF7FFF7FFF7FFF7F0000FF7FFF7FFF7F0000000000000000000000000000
      00000000007C0000000000000000000000000000000000000000000000001042
      1042FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7F104210420000000000000040007C1040007C1040
      007C000018631863000010400000000000000000FF7FFF7FFF7FFF7FFF7F1863
      000000001863186300001F7C007C0000000000000000FF7FFF7FFF7FFF7FFF7F
      FF7F1863FF7F1042FF7FE07F1863E07F1863E07F1863E07F1863E07F1863E07F
      18631042FF7FFF7F1863186318630000000000000000000000001042FF7FFF7F
      FF7FFF7FFF7FFF7FFF7F0000FF7FFF7FFF7FFF7F000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000001042
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7F104200000000104200001040007C1040007C
      007C0000FF7FFF7FFF7F00000000FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7F00001F7C0000000000000000FF7FFF7FFF7FFF7FFF7F
      1863FF7F00421042FF7F1863E07F1863E07F1863E07F1863E07F1863E07F1863
      E07F186318631042FF7FFF7F10420000000000000000000000001042FF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7F0000FF7F10421042000000000000000000000000
      0000000000000000000000000000000000000000000000000000186318631042
      1042104210421042104210421042104210421042104210421042104210421042
      104210421042104210421042104210420000000000000000007C1040007C1040
      007C000018631863186318630000000000000000000000001863FF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7F0000007C0000000000000000FF7FFF7FFF7FFF7F1863
      FF7F186318631042FF7FFF7FFF7FE07F1863E07F1863E07F1863E07F1863E07F
      1863E07F1863104210421863000000000000000000000000000000001042FF7F
      FF7FFF7FFF7FFF7FFF7FFF7F0000104200000000000000000000000000000000
      000000000000000000000000000000000000104200001863186318631863FF7F
      FF7F186310421042104200000000000000000000000000000000000000000000
      1000000000000000000000000000000000000000000000401040007C1040007C
      007C0000FF7FFF7FFF7FFF7F000000001F7C0000FF7F00001863FF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7F00001F7C0000000000000000FF7FFF7FFF7F1863FF7F
      1042FF7FE07F1042FF7F18631863FF7FFF7FE07FE07F1863E07F1863E07F1863
      E07F1863E07F104218630000000000000000000000000000000000001042FF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7F000000000000000000000000000000000000
      0000000000000000000000000000000000001042186318631863FF7FFF7FFF7F
      FF7FFF7F18631042104210421042000000000000000000000000000000001000
      100010000000000000000000000000000000000010420000007C1040007C1040
      007C000018631863186318630000186300001F7C000000001863FF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7F0000007C0000000000000000FF7FFF7F1863FF7F0042
      1042FF7F18631042FF7FFF7FE07F18631863FF7FFF7FE07F1863E07F1863E07F
      1863E07F18631042000000000000000000000000000000000000000000001042
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F00000000000010421042104210421042
      10421042104210421042104210420000000010421863FF7FFF7FFF7F10421042
      1042FF7FFF7F1863104210421042104210420000000000000000000010001000
      100010001000000000000000000000000000000000000000007C007C1040007C
      007C0000FF7FFF7FFF7FFF7F0000FF7F186300001F7C00001863FF7FFF7FFF7F
      FF7FFF7FFF7FFF7F0000000000000000000000000000FF7F1863FF7F18631863
      1042FF7FFF7FFF7F10421042FF7FFF7F186310421863FF7FFF7FE07FE07F1863
      E07F1863E07F0000000000000000000000000000000000000000000000001042
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F000000001042FF7FE07F1863E07F
      1863E07F1863E07F1863E07F1042000000001042FF7FFF7FFF7F1042FF7F1863
      18631042FF7FFF7F186300000000000000000000000000000000100010001000
      1000100010001000000000000000000000000000000000000000007C007C1040
      007C0000000000000000000000000000FF7F186300001F7C0000FF7FFF7FFF7F
      FF7FFF7F0000000000000000000000000000000000001863FF7F1042FF7FE07F
      1042FF7F18631863FF7FFF7F104210421863E07F186318631863FF7FFF7FE07F
      1863E07F18630000000000000000000000000000000000000000000000000000
      1042FF7FFF7FFF7FFF7FFF7FFF7F1042104200001042FF7FE07F1863E07F1863
      E07F1863E07F1863E07F186300001042000000001042FF7FFF7F1042FF7FFF7F
      18631042FF7FFF7F000018631863104210420000000000000000000000000000
      10000000000000000000000000000000000000000000000000000000007C007C
      007C00000000000000000000000000000000FF7F186300001F7C0000FF7FFF7F
      FF7F0000000000000000000000000000000000000000000000001042FF7F1863
      1042FF7FFF7FE07F18631863FF7FFF7FE07F1863E07F1863E07F10421863FF7F
      FF7FE07FE07F0000000000000000000000000000000000000000000000000000
      00001042FF7FFF7FFF7F104210420000000000001042FF7F1863E07F1863E07F
      1863E07F1863E07F18631042000010420000000000001042FF7FFF7F10421042
      1042FF7FFF7F1863000018631863104210420000000000000000000000000000
      100000000000000000000000000000000000000000000000000000000000007C
      007C000000000000000000000000000000000000FF7FFF7F00001F7C00000000
      00000000000000000000000000000000000000000000000000000000FF7FFF7F
      FF7F10421042FF7FE07F186310421863FF7FFF7F1863E07F1863E07F18631042
      1863FF7FFF7F0000000000000000000000000000000000000000000000000000
      0000000010421042104200000000000000001042FF7F1863E07F1863E07F1863
      E07F1863E07F1863E07F00001042104200000000000000001042FF7FFF7FFF7F
      FF7FFF7F10420000186318631863186300000000000000000000000000000000
      1000000000000000000000000000000000000000000000000000000000000000
      007C00000000000000000000000000000000000000000000000000001F7C0000
      00000000000000000000000000000000000000000000000000000000FF7F1863
      1863FF7FFF7F104210421863E07F186318631863FF7FFF7FE07F1863E07F1863
      E07F000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000001042FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7F10420000186310420000000000000000104200001863FF7F
      1042000010421863FF7FFF7FFF7F104210000000000000000000000000001000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000001F7C
      00000000000000000000000000000000000000000000000000000000FF7FFF7F
      E07F18631863FF7FFF7FE07F1863E07F1863E07F10421863FF7FFF7F1863E07F
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000001042104210421042104210421042
      104210421042104210421042E07F104200000000000000001042186310420000
      10421863FF7FFF7FFF7F10421042000000001000100000000000000000001000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      1F7C000000000000000000000000000000000000000000000000000000000000
      FF7FE07F186300000000FF7FFF7F1863E07F1863E07F186310421863FF7FFF7F
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000001042FF7F1863E07F1863E07F
      1863E07F1863E07F1863E07F1863104200000000000000001042FF7F18631863
      FF7FFF7FFF7F1042104200000000000000000000000010001000100010000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00001F7C00000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FF7FFF7FE07F1863E07F1863E07F00000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000001042FF7FE07F1863E07F1863
      E07F1863FF7FFF7FFF7FFF7FFF7F1042000000000000000000001042FF7FFF7F
      FF7F104210420000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000001F7C0000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF7FFF7F1863E07F000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000001042FF7F1863E07F1863E07F
      1863FF7F10421042104210421042104200000000000000000000000010421042
      1042000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000001F7C000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FF7FFF7F000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000001042FF7FFF7FFF7FFF7F
      FF7F104200000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000001042104210421042
      1042000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000001042
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000186300420000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000010420000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000FF7FFF7F186300000000000000000000000000000000FF7FFF7F1863
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000421863004200420042
      00000000000000000000000000000000000000000000000000000000FF7F1863
      0000004000000000000010420000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      E07F0042E07F0042E07F0042E07F00420042004200420000000000000000E07F
      E07F0042E07F0042000000000000000000000000000000000000000000000000
      00001863FF7FFF7FFF7F18630000000000000000000000001863FF7FFF7FFF7F
      1863000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000001863004218630042186300420042
      0042000000000000000000000000000000000000000000000000000018630042
      FF7F104200001042004000000000000010420000000000000000000000000000
      0000000000000000000000000000000000000000000000000000E07F0042E07F
      0042E07F0042E07F0042004200420042004200420042000000420000E07FE07F
      0042004200000042004200000000000000000000000000000000000000000000
      00001863FF7F0000FF7FFF7F0000000000000000000000001863FF7F0000FF7F
      FF7F000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000004218630042186300421863004200420042
      0042004200000000000000000000000000000000000000000000FF7F00421042
      0042FF7F10420042104200001042004000000000104200000000000000000000
      0000000000000000000000000000000000000000000000000042004200420042
      0000000000000000000000000000000000000000000000420000E07FE07F0000
      E07F0042E07F0042004200420000000000000000000000000000000000000000
      000018631863FF7FFF7FFF7F00000000000000000000000018631863FF7FFF7F
      FF7F000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000042186300421863004218630042186300420042
      0042004200420000000000000000000000000000000000000000FF7F18630040
      0000000000421042FF7F10420042104200001042000000000000104200000000
      0000000000000000000000000000000000000000000000000042004200420042
      E07F1863E07F1863004218630042004200420042000000420042E07FE07FE07F
      E07F004200420042004200420000000000000000000000000000000000000000
      0000000018631863186300000000000000000000000000000000186318631863
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000004218630000186300421863004218630042FF7FFF7FE07F0042
      0042004200420000000000000000000000000000000000000000186300420000
      000000000000000000000042FF7F186310420042000010420040000000000000
      1042000000000000000000000000000000000000000000000000004200420042
      0042004200420042004200420042004200420000E07F0042E07F0042E07FE07F
      00420042E07F0042E07F0042E07F000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000186300421863004200000042186300421863FF7FFF7FFF7FE07FFF7FE07F
      004200420042000000420000000000000000000000000000FF7F004210420000
      0000FF7FFF7F000000000000000000000042FF7F186310420042000018630040
      00000000000010420000000000000000000000000000000000000000E07F0042
      E07F0042E07F0042004200420042004200420000E07F0042E07F00420042E07F
      0042E07FE07FE07F004200420042000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000042
      18630042186300421863000018630042FF7FFF7FE07FFF7FE07FFF7FE07FFF7F
      E07F00420042000000420042000000000000000000000000FF7F186300400000
      0000FF7FFF7F000010420000000000000000000000000042FF7F186310421863
      00001863004000000000104200420042000000000000E07F0042E07F0042E07F
      0042E07F0042E07F004200420042004200420000E07F0042E07F004200420042
      E07FE07FE07F004200420042E07F000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000004200421863
      004218630042186300420000FF7FFF7FFF7FE07FFF7FE07FFF7FE07FFF7FE07F
      FF7FE07F00420000004200420042000000000000000010421863004200000000
      FF7FFF7F00001042000000000000000010420000000000000000004210420042
      1863004200001042000010421863004200000000004200420042004200000000
      0000000000000000000000000000000000000000E07F0042E07F00420042E07F
      E07FE07FE07FE07F0042E07F0042000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000004218630042
      1863004218630042FF7FE07F00421863E07FFF7FE07FFF7FE07FFF7FE07FFF7F
      E07F1863186300000042004200420042000000000000FF7F0042104200000000
      0000FF7F000000000000FF7F0000104200000000000010420000000000000000
      000010420042186300401042E07F004200000000004200000000000000000000
      E07F0042E07F0042E07F00420042004200420000E07F0042E07F004200420042
      E07FE07F0000E07F00420042E07F000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000004200421863
      00421863FF7FFF7FFF7FE07FFF7F004218630042FF7FE07FFF7FE07FFF7F1863
      186300420042E07F0042004200420042000000000000FF7F1863004000000000
      0000FF7FFF7FFF7FFF7F000010420000FF7F0000000010420000000000000000
      00000042186300401042E07F1863004200000000000000420000E07F0042E07F
      0042E07F0042E07F0042E07F0042004200420000E07F0042E07F004200420042
      E07FE07FE07FE07F0042E07F0042000000000000000000000000000000000000
      0000000000001F7C1F7C00001F7C1F7C00001F001F0000001F001F000000E07F
      E07F0000E07FE07F000000000000000000000000000000000000004218630042
      FF7FFF7FE07FFF7FE07FFF7F18630042000000000042FF7FE07F186318630042
      0042FF7FE07FFF7FE07F00420042004200000000FF7F18630042000000000000
      00000000FF7FFF7FFF7F000010420000FF7F0000104200000000FF7F00000000
      004218631042000010421863E07F004200000000004200000042004200420042
      0042004200420042004200420042004200420000E07F00420042004200420042
      E07FE07FE07F0042E07F0042E07F000000000000000000000000000000000000
      0000000000001F7C1F7C00001F7C1F7C00001F001F0000001F001F000000E07F
      E07F0000E07FE07F0000000000000000000000000000000000000042FF7FFF7F
      FF7FE07FFF7FE07F18630042000000001863186300000042186300420042E07F
      FF7FE07FFF7FE07FFF7FE07F0042004200000000FF7F00420000000000000000
      000000000000FF7FFF7F00000000FF7FFF7F00000000FF7FFF7FFF7F00000000
      18631042004010421863E07F1863004200000000000000000042000000000000
      E07F186318631863004218630042186300420000E07FE07F0042004200420042
      0042004200420042E07FE07F0000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000042FF7F
      E07FFF7F004218630000000018631863FF7FFF7F186300000042FF7FE07FFF7F
      E07FFF7FE07FFF7FE07FFF7FE07F004200000000104200000042FF7F00420000
      0000000000000000FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F000000001042
      0042FF7F00401042E07F1863E07F004200000000000000000000004200420042
      00420000000000000000000000000000000000000000E07F00420042E07F0042
      004200420042E07FE07F00420000000000000000000000000000000000000000
      0000000000001F7C1F7C00001F7C1F7C00001F001F0000001F001F000000E07F
      E07F0000E07FE07F000000000000000000000000000000000000000000000042
      186300420000000018631863FF7FFF7FFF7FFF7FFF7F186300000042FF7FE07F
      FF7FE07FFF7F186300400040FF7FE07F000000000040FF7F1863104200000042
      FF7F00420000000000000000FF7FFF7FFF7FFF7FFF7F00000000000000000042
      18631042000010421863E07F1863004200000000000000000042004200000000
      0000E07F0042E07F0042E07F0042E07F0042E07F0000E07FE07F004200420042
      00420000E07FE07FE07F00000000000000000000000000000000000000000000
      0000000000001F7C1F7C00001F7C1F7C0000E07FE07F0000004000400000E07F
      E07F0000E07FE07F000000000000000000000000000000000000000000000000
      0000000018631863FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F186300000042FF7F
      E07FFF7F004000401863FF7FE07F0042000000000000000000400040FF7F1863
      104200000042FF7F004200000000000000000000000000000000000000001863
      FF7F004010421863E07F1863E07F0042000000000000000000000000E07F0042
      E07F0042E07F0042E07F0042E07F0042E07F0042E07F0000E07FE07F00420042
      E07F0042E07FE07F000000000000000000000000000000000000000000000000
      000000000000000000000000E07FE07F0000E07FE07F0000004000400000E07F
      E07F0000E07FE07F000000000000000000000000000000000000000000000000
      18631863FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F186300000042
      FF7FE07FFF7FE07FFF7F0042004200000000000000001042FF7FE07F00000040
      0040104218631042000000421863004200000000000000000000000010420042
      FF7F00001042E07F1863E07F1863004200000000000000000000004200420042
      00420042004200420042004200420042004200420042004200000000E07F0042
      0042004200420000000000000000000000000000000000000000000000000000
      000000000000000000000000E07FE07F0000E07FE07F0000004000400000E07F
      E07F0000E07FE07F000000000000000000000000000000000000000018631863
      FF7FFF7FFF7FFF7F1F00FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F18630000
      0042FF7FE07F004200420000000000000000000000001042FF7F1863E07F1863
      E07F000000400040104218631042000000421863004200000000000000421863
      00401042E07F1863E07F1863E07F004200000000000000000000004200000000
      0000E07FE07FE07F186300421863004218630042186300420000000000000042
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000E07FE07F0000E07FE07F00000040004000000040
      004000001F001F000000000000000000000000000000000018631863FF7FFF7F
      FF7FFF7F1F001F00FF7FFF7F1F00FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F1863
      000000420042000000000000000000000000000000001042FF7FE07F1863E07F
      1863E07F1863E07F00000040004010421863004200000042186300421863FF7F
      004010421863E07F1863E07F1863004200000000000000000000000000420042
      0042004200421863000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000E07FE07F0000E07FE07F00000040004000000040
      004000001F001F0000000000000000000000000018631863FF7FFF7FFF7FFF7F
      1F001F00FF7FFF7F1F001F00FF7FFF7F1F00FF7FFF7FFF7FFF7F00400040FF7F
      186300000000000000000000000000000000000000001042FF7F1863E07F1863
      E07F1863E07F1863E07F1863E07F000000400040104218630042104210421863
      00001042E07F1863E07F1863E07F004200000000000000000000004200420042
      1863000000000000E07F0042E07F0042E07F0042E07F0042E07F0042E07F0000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000040004000000040
      004000001F001F000000000000000000000000001042FF7FFF7FFF7FFF7FFF7F
      FF7FFF7F1F001F00FF7FFF7F1F001F00FF7FFF7FFF7FFF7F0040007C007C0040
      FF7F18630000000000000000000000000000000000001042FF7FE07F1863E07F
      1863E07F1863E07F1863E07F1863E07F1863E07F000000400040104218630040
      1042E07F1863E07F1863E07F1863004200000000000000000000000000420042
      0000E07F0042E07F0042E07F0042E07F0042E07F0042E07F0042E07F0042E07F
      0042004200000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000040004000000040
      004000000000000000000000000000000000000000001042FF7FFF7FFF7FFF7F
      FF7F1F00FF7FFF7F1F001F00FF7FFF7FFF7FFF7FFF7FFF7FFF7F00400040007C
      1863FF7F1042000000000000000000000000000000001042FF7F1863E07F1863
      E07F1863E07F1863E07F1863E07F1863E07F1863E07F1863E07F000000400000
      10421863E07F1863E07F1863E07F004200000000000000000000000000000000
      0042004200420042FF7FFF7FFF7FFF7FFF7FFF7F004200420042004200420042
      0042004200420000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000040004000000040
      0040000000000000000000000000000000000000000000001042FF7FFF7FFF7F
      FF7FFF7F1F001F00FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F1863FF7F
      104210420000000000000000000000000000000000001042FF7FE07F1863E07F
      1863E07F1863E07F1863E07F1863E07F1863E07F1863E07F1863E07F1863E07F
      1863E07F1863E07F1863E07F1863004200000000000000000000000000000000
      0042000018631863E07FE07FE07FE07F0042E07F0042E07F0042E07F00420000
      0000000000420000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000001042FF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F10421042
      000000000000000000000000000000000000000000001042FF7F1863E07F1863
      E07F1863E07F1863E07F1863E07F1863E07F1863E07F1863E07F1863E07F1863
      E07F1863E07F1863E07F1863E07F004200000000000000000000000000000000
      000000420042004200420042E07FE07FE07FE07FE07F0042E07F0042E07F0042
      0042004200000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000001042FF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F1042104200000000
      000000000000000000000000000000000000000000001042FF7FE07F1863E07F
      1863E07F1863E07F1863E07F1863E07F1863E07F1863E07F1863E07F1863E07F
      1863E07F1863E07F1863E07F1863004200000000000000000000000000000000
      004200420042E07F0042E07F0042E07FE07FE07FE07FE07F0042E07F0042E07F
      0042004200420000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000001042
      FF7FFF7FFF7FFF7FFF7FFF7F1F001F00FF7FFF7F104210420000000000000000
      000000000000000000000000000000000000000000001042FF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7F000000000000000000000000000000000000
      0000004200420042E07F0042E07FE07FE07FE07FE07FE07FE07F004200420042
      0042004200000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      1042FF7FFF7FFF7F1F001F00FF7FFF7F10421042000000000000000000000000
      0000000000000000000000000000000000000000000010421863186318631863
      1863186318631863186318631863186318631863186310421042104210421042
      1042104210421042104210421042000000000000000000000000000000000000
      00000000000000000042E07F0042E07F0042E07FFF7FFF7FFF7FFF7FE07F0000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00001042FF7F1F00FF7FFF7F1042104200000000000000000000000000000000
      0000000000000000000000000000000000000000000000001042FF7FE07F1863
      E07F1863E07F1863E07F1863E07F1863E07F1863104200000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000001042FF7F104210420000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000001042FF7FE07F
      1863E07F1863E07F1863E07F1863E07F18631042000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FF7F00000000000000000000000000000000
      0000000000001042000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000001042FF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F10420000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000001042
      1042104210421042104210421042104200000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000010421042104210421042
      1042104210421042104210421042104210421042104210421042104210421042
      1042104210421042104210421042104210420000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000001042FF03FF03FF03FF03FF03FF03
      FF03FF03FF03FF03FF03FF03FF03FF03FF03FF03FF03FF03FF03FF03FF03FF03
      FF03100200000000000000000000000000000000000010421042104210421042
      1042104210421042104210421042104210421042104210421042104210421042
      1042104210421042104210421042104210420000000000000000000000000000
      0000000000000000000010021002100210021002100210021002100210021002
      1002100210021002100210021002000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000001042FF03FF03FF03FF03FF03FF03
      FF03FF03FF03FF03FF03FF03FF03FF03FF03FF03FF03FF03FF03FF03FF03FF03
      FF03100210020000000000000000000000001F001F001F001F001F001F001F00
      1F001F001F001F001F001F001F001F001F001F001F001F001F001F001F001F00
      1F001F001F001F001F001F001F00104210420000000000000000000000000000
      0000000000000000000000001042104210421042104210421042104210421042
      1042104210421042104210421042000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000001042FF03FF03FF03FF03FF03FF03
      FF03FF03FF03FF03FF03FF03FF03FF03FF03FF03FF03FF03FF03FF03FF03FF03
      FF03100210021002000000000000000000001F00FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7F1F00104210421042000000000000000000000000
      00000000000010420000104200000000FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7F10420000000000000000FF7F1042104200000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000FF7F1042104200000000000000001042FF03FF03FF03FF03FF03FF03
      FF03FF03FF03FF7FFF7FFF7FFF7FFF7FFF03FF03FF03FF03FF03FF03FF03FF03
      FF03100210021002100200000000000000001F00FF7F00000000FF7FFF7FFF7F
      00000000FF7FFF7FFF7FFF7FFF7FFF7F00000000000000000000FF7FFF7FFF7F
      00000000FF7FFF7FFF7F00001F00104210421042FF7FFF7F0000000000000000
      0000FF7F1863104200001863FF7F1042FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7F1042000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000001042FF03FF03FF03FF03FF03FF03
      FF03FF03FF7F00000000000000001042FF7FFF03FF03FF03FF03FF03FF03FF03
      FF03100210021002100210020000000000001F00FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7F1F00104210421042FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7F18631042000010421042FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7F1042000000000000004200420042004200420042
      0042004200420042004200420042004200420042004200420042004200420042
      0042004200420042004200420042004200001042FF03FF03FF03FF03FF03FF03
      FF03FF7F0000FF031863FF031863FF031042FF7FFF03FF03FF03FF03FF03FF03
      FF03100210021002100210021002000000001F00FF7FFF7FFF7F00000000FF7F
      00000000000000000000FF7F000000000000FF7FFF7FFF7F00000000FF7FFF7F
      000000000000FF7FFF7FFF7F1F00104210421042FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7F18631042000010421042FF7FFF7FFF7F100010001000100010001000
      1000100010001000FF7FFF7F1042000000000000FF7FE07FFF7FE07FFF7FE07F
      FF7FE07FFF7FE07FFF7FE07FFF7FE07FFF7FE07FFF7FE07FFF7FE07FFF7FE07F
      FF7FE07FFF7FE07FFF7FE07FFF7F004200001042FF03FF03FF03FF03FF03FF03
      FF7F0000FF031863FF031863FF031863FF031042FF7FFF03FF03FF03FF03FF03
      FF03100210021002100210021002100200001F00FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7F1F00104210421042FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7F186310420000104200000000FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7F1042000000000000E07FFF7FE07FFF7FE07FFF7F
      E07FFF7FE07FFF7FE07FFF7FE07FFF7FE07FFF7FE07FFF7FE07FFF7FE07FFF7F
      E07FFF7FE07FFF7FE07FFF7FE07F004200001042FF03FF03FF03FF03FF03FF7F
      0000FF031863FF031863FF031863FF031863FF031042FF7FFF03FF03FF03FF03
      FF03100210021002100210021002100200001F00FF7FFF7F0000FF7FFF7FFF7F
      0000000000000000FF7F000000000000000000000000FF7FFF7FFF7F00000000
      FF7FFF7FFF7F00000000FF7F1F00104210421042FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7F1863104200001863FF7F1042FF7FFF7F100010001000100010001000
      1000100010001000FF7FFF7F1042000000000000FF7FE07F0000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000004200001042FF03FF7FFF7FFF7FFF7F0000
      10021002FF031863FF031863FF031863FF031863FF031042FF7FFF7FFF7FFF7F
      FF03100210021002100210021002100200001F00FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7F1F00104210421042FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7F18631042000010421042FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7F1042000000000000E07FFF7FFF7FFF7F0000FF7F
      FF7F0000FF7FFF7F0000FF7FFF7F0000FF7FFF7F0000FF7FFF7F0000FF7FFF7F
      0000FF7FFF7F0000FF7FFF7F0000004200001042FF7F00000000000000001002
      1002100210021002100210021002100210021002100210021042104210421042
      FF7F100210021002100210021002100200001F00FF7F0000000000000000FF7F
      000000000000FF7F000000000000000000000000FF7F00000000000000000000
      0000FF7FFF7FFF7FFF7FFF7F1F00104210421042FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7F18631042000010421042FF7FFF7FFF7F100010001000100010001000
      1000100010001000FF7FFF7F10420000000000000000E07FFF7F000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000E07F0042000000000000104210021002100210021002
      100210021863FF031863FF031863FF031863FF031863FF031863FF031863FF03
      1042FF7F10021002100210021002100200001F00FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7F00000000000000001F00104210421042FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7F186310420000104200000000FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7F104200000000000000000000E07FFF7FFF7F0000
      FF7FFF7F0000FF7FFF7F0000FF7FFF7F0000FF7FFF7F0000FF7FFF7F0000FF7F
      FF7F0000FF7FFF7F000000420000000000000000000010421002100210021002
      10021002FF031863FF031863FF031863FF031863FF031863FF031863FF031863
      FF031042FF7F1002100210021002100200001F00FF7FFF7F0000FF7F00000000
      000000000000000000000000000000000000FF7FFF7F00000000104210421042
      FF7FFF7FFF7FFF7FFF7FFF7F1F00104210421042FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7F1863104200001863FF7F1042FF7FFF7F100010001000100010001000
      1000100010001000FF7FFF7F1042000000000000000000000000E07F00000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000E07F004200000000000000000000000000001042100210021002
      100210021863FF031863FF031863FF031863FF031863FF031863FF031863FF03
      1863FF031042FF7F100210021002100200001F00FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7F00000000000000420042004200420000104210421042
      1042FF7FFF7FFF7FFF7FFF7F1F00104210421042FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7F18631042000010421042FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7F10420000000000000000000000000000E07FFF7F
      0000FF7FFF7F0000FF7FFF7F0000FF7FFF7F0000FF7FFF7F0000FF7FFF7F0000
      FF7FFF7F00000042000000000000000000000000000000000000104210021002
      10021002FF031863FF031863FF031863FF031863FF031863FF031863FF031863
      FF031863FF031042FF7F10021002100200001F00FF7FFF7FFF7F0000FF7F0000
      FF7F000000000000000000000000004200420042004200420042000010420000
      00001042000000000000FF7F1F00104210421042FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7F18631042000010421042FF7FFF7FFF7F100010001000100010001000
      1000100010001000FF7FFF7F10420000000000000000000000000000FF7FE07F
      FF7FE07FFF7FE07FFF7FE07FFF7FE07FFF7FE07FFF7FE07FFF7FE07FFF7FE07F
      FF7FE07FFF7F0042000000000000000000000000000000000000000010421002
      100210021863FF031863FF031863FF031863FF031863FF031863FF031863FF03
      1863FF031863FF031042FF7F1002100200001F00FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7F0000004200420042E07FE07F00420042004200001042
      1042104210421042104210420000104210421042FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7F186310420000104200000000FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7F10420000000000000000000000000000E07FFF7F
      E07FFF7FE07FFF7FE07FFF7FE07FFF7FE07FFF7FE07FFF7FE07FFF7FE07FFF7F
      E07FFF7FE07F0042000000000000000000000000000000000000000000001042
      10021002FF031863FF031863FF031863FF031863FF031863FF031863FF031863
      FF031863FF031863FF031042FF7F100200001F00FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7F000000420042E07FE07FE07FE07F0042004200420000
      1042104210421042104210420000104210421042FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7F1863104200001863FF7F1042FF7FFF7F100010001000100010001000
      1000100010001000FF7FFF7F10420000000000000000000000000000FF7FE07F
      FF7FE07FFF7FE07FFF7FE07FFF7FE07FFF7FE07FFF7FE07FFF7FE07FFF7FE07F
      FF7FE07FFF7F0042000000000000000000000000000000000000000000000000
      104210021863FF031863FF031863FF031863FF031863FF031863FF031863FF03
      1863FF031863FF031863FF031042FF7F000010421042104210421F001F001F00
      1F001F001F001F001F0000000042FF7FE07FE07FE07FE07FE07F004200420042
      0000000000000000000000000000104210421042FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7F18631042000010421042FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7F10420000000000000000000000000000E07FFF7F
      E07FFF7FE07FFF7FE07FFF7F000000000000000000000000E07FFF7FE07FFF7F
      E07FFF7FE07F0042000000000000000000000000000000000000000000000000
      0000104210421042104210421042104210421042104210421042104210421042
      1042104210421042104210421042104200001042FF7FFF7F10421F001F001F00
      1F001F001F001F001F000000FF7FFF7FFF7FE07FE07FE07FE07FE07F00420042
      0042000000000000000000000000104210421042FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7F18631042000010421042FF7FFF7FFF7F100010001000100010001000
      1000100010001000FF7FFF7F10420000000000000000000000000000FF7FE07F
      FF7FE07FFF7FE07FFF7F00001863186318631863186318630000E07FFF7FE07F
      FF7FE07FFF7F0042000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000010421042104210421F001F001F00
      1F001F001F001F001F001F000000FF7FFF7FFF7FE07FE07FE07FE07FE07F0042
      0042004200000000000000000000104210421042FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7F186310420000104200000000FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7F104200000000000010420000000010420000FF7F
      E07FFF7FE07FFF7F0000FF7F1042FF7FFF7FFF7FFF7F1042FF7F0000E07FFF7F
      E07FFF7F00420000104200000000104200000000000000000000000000000000
      0000000000000000000000000000000010000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FF7FFF7FFF7FE07FE07FE07FE07FE07F
      0042004200420000104210421042104210421042FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7F1863104200001863FF7F1042FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7F0000000000000000FF7F1042000000000000FF7F10420000104210420000
      0000000000000000FF7FFF7FFF7F1042104210421042FF7FFF7FFF7F00000000
      0000000000001042104200001042FF7F00000000000000000000000000000000
      0000000000000000000000000000100010001000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF7FFF7FFF7FE07FE07FE07FE07F
      E07F004200420042000010421042104210421042FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7F18631042000010421042FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F0000
      0000186318631863186300000000000000000000FF7F00000000FF7FFF7FFF7F
      FF7F0000FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      00001863FF7FFF7FFF7F00000000FF7F00000000000000000000000000000000
      0000000000000000000000001000100010001000100000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000FF7FFF7FFF7FE07FE07FE07F
      E07FE07F00420042004200000000104210421042FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7F1863104200001042FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F10421863
      1863186318631863186318631863000000000000000000000000186318631863
      18630000FF7FFF7F1F001F001F001F001F001F001F001F001F001F00FF7FFF7F
      0000104218631863186300000000000000000000000000000000000000000000
      0000000000000000000010001000100010001000100010000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FF7FFF7FFF7FE07FE07F
      E07FE07FE07F0042004200420000000000001042FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7F18631042000010421042104210421042104210421042104210421863
      FF7FFF7FFF7FFF7FFF7F18631863000000000000000000000000000000000000
      00000000FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000100010001000100010001000100010001000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000FF7FFF7FFF7FE07F
      E07FE07FE07FE07F004200420042000000001042FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7F186300000000000000000000000000000000000000001042FF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7F1863186300000000000000000000000000000000
      00000000FF7FFF7F1F001F001F001F001F001F001F001F001F00FF7FFF7FFF7F
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000001000100010001000100010001000100010001000100000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FF7FFF7FFF7F
      E07FE07FE07FE07FE07F0000000000000000000010421042FF7FFF7FFF7FFF7F
      FF7F0000000000000000000000000000000000000000000000001042FF7FFF7F
      FF7FFF7F00000000000000001863186300000000000000000000000000000000
      00000000FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000100010001000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000FF7FFF7F
      FF7FE07FE07FE07F000000420042004200000000000000001042104210421042
      10420000000000000000000000000000000000000000000000001042FF7FFF7F
      FF7FFF7F0000FF7FFF7FFF7F1863186300000000000000000000000000000000
      00000000FF7FFF7F1F001F001F00FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000100010001000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FF7F
      FF7FFF7FE07F0000004200420042004200000000000000000000000000000000
      00000000000000000000000000000000000000000000000000001042FF7FFF7F
      FF7FFF7F0000FF7FFF7FFF7FFF7F186300000000000000000000000000000000
      000010421042FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      1042104200000000000000000000000000000000000000000000000000000000
      0000000000000000000000001000100010000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      FF7FFF7F00000042004200000042004200000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000001042FF7F
      FF7FFF7F0000FF7FFF7FFF7FFF7F104200000000000000000000000000000000
      000000000000FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7F000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000001000100010000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FF7F00000042004200420042000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000001042FF7F
      FF7FFF7FFF7FFF7FFF7FFF7F1863104200000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      1000000000000000000010001000100000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000042004200420000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000001042
      1042FF7FFF7FFF7FFF7F10421042000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000100010001000100010000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000104210421042104200000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000010421863
      0000186310420000000000000000000000000000000018631042104200001042
      1042186300000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000001042186318631863
      1863186318631863186318631863186318631863186318631863186318631863
      1863186318631863186300000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000001863E07F
      1042E07F1863000000000000000000000000000000001863E07FE07F1042E07F
      E07F186300000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000001042FF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7F186300000000000000000000000000000000000000000000
      0000000000000000000000000000000010420000000000000000000000000000
      000000000000000000000000000000000000000000000000000000001863E07F
      1042E07F1863000000000000000000000000000000001863E07FE07F1042E07F
      E07F186300000000000000000000000000000000000000000000000000000000
      0000000000000000004218631863186310420042186300420042104210420000
      0000000000000000000000000000000000000000000000001042FF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7F186300000000000000000000000000000000000000000000
      0000000000000000000000000000104218631863000000000000000000000000
      000000000000000000000000000000000000000000000000000000001863E07F
      1042E07F1863000000000000000000000000000000001863E07FE07F1042E07F
      E07F186300000000000000000000000000000000000000000000000000000000
      000000421863FF7FE07FFF7FFF7FFF7FE07F104218631863E07F186318630042
      1042104200000000000000000000000000000000000000001042FF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F186318631863FF7FFF7FFF7FFF7F
      186318631863FF7F186300000000000000000000000000000000000000000000
      000000000000000000000000FF7F104218631863186318630000000000000000
      000000000000000000000000000000000000000000000000000000001863E07F
      1042E07F1863000010420042104200421042004200001863E07FE07F1042E07F
      E07F186300000042104200421042004200000000000000000000000010421863
      1863FF7FE07FFF7FFF7FFF7FE07FFF7FFF7FFF7F1042FF7FFF7F1863E07F1863
      1863186300421042000000000000000000000000000000001042FF7FFF7F1F00
      1F001F001F001F001F00FF7FFF7FFF7F007C007C007CFF7FFF7FFF7FFF7F007C
      007C007CFF7FFF7F186300000000000000000000000000000000000000000000
      00000000000000000000FF7FFF7F104218631863186318631863186300000000
      000000000000000000000000000000000000000000000000000000001863E07F
      1042E07F18630000E07F1863E07F1863E07F186300001863E07FE07F1042E07F
      E07F186300001863E07F1863E07F10420000000000000000000000001042FF7F
      E07FFF7FFF7FFF7FE07FFF7FFF7FFF7FE07FFF7FFF7F1042E07FFF7FFF7FFF7F
      E07F186318631863104200000000000000000000000000001042FF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F007C1863FF7FFF7FFF7FFF7FFF7F
      007C1863FF7FFF7F186300000000000000000000000000000000000000000000
      0000000000000000FF7FFF7FFF7F104218631863186318631863186318631863
      000000000000000000000000000000000000000000000000000000001863E07F
      1042E07F186300001863E07F1863E07F1863E07F00001863E07FE07F1042E07F
      E07F18630000E07F1863E07F1863004200000000000000000000000000001042
      FF7FFF7FE07FFF7FFF7FFF7FE07FFF7FFF7FFF7FE07F1042FF7FFF7FE07FFF7F
      FF7FFF7FE07FFF7F004210420000000000000000000000001042FF7FFF7F1F00
      1F001F001F001F001F001F001F00FF7FFF7FFF7F007C18631863186318631863
      007C1863FF7FFF7F186300000000000000000000000000000000000000000000
      000000000000FF7FFF7FFF7FFF7F104218631863186318631863186318631863
      186318630000000000000000000000000000000000000000000000001863E07F
      1863E07F1863000000000000E07F1863E07F186300001863E07FE07F1042E07F
      E07F186300001863E07F1863E07F104200000000000000000000000000000000
      1042FF7FFF7FFF7FE07FFF7FFF7FFF7FE07FFF7FFF7FFF7F1042FF7FFF7FFF7F
      E07FFF7FFF7F1042104210421042000000000000000000001042FF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F007C007C007C007C007C
      007C1863FF7FFF7F186300000000000000000000000000000000000000000000
      00000000FF7FFF7FFF7FFF7FFF7F104218631863186318631863186318631863
      186318631863186300000000000000000000000000001042E07FE07FE07FE07F
      E07FE07FE07FE07FE07F18630000E07F1863000000001863E07FE07F1863E07F
      E07F1863000000001863E07F1863004200000000000000000000000000000000
      1042FF7FE07FFF7FFF7FFF7FE07FFF7FFF7FFF7FE07FFF7F1042FF7FE07F1042
      104210421042FF7FFF7FFF7F1863000000000000000000001042FF7FFF7F1F00
      1F001F001F001F001F001F001F001F001F00FF7FFF7FFF7F007C1863FF7FFF7F
      007C1863FF7FFF7F186300000000000000000000000000000000000000000000
      0000FF7FFF7FFF7FFF7FFF7FFF7F104210421863186318631863186310421042
      186318631863186318631863000000000000000000000000E07FE07FE07FE07F
      E07FE07FE07FE07FE07F0000000000000000104218631042E07FE07F1863E07F
      E07F10421863000000001863E07F104200000000000000000000000000000000
      00001042FF7FFF7FE07FFF7FFF7FFF7FE07FFF7FFF7FFF7F104210421042FF7F
      E07FFF7FFF7FFF7FE07FFF7F1863000000000000000000001042FF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F007C1863FF7F
      007C1863FF7FFF7F186300000000000000000000000000000000000000000000
      FF7FFF7FFF7FFF7FFF7FFF7F10421863FF7F10421042186318631042FF7F1863
      1042186318631863186318631863186300000000E07F10421042E07FE07FE07F
      E07FE07FE07FE07F10421863E07F000000001042E07F1042E07FE07FE07FE07F
      E07F1042E07F10420000E07F1863004200000000000000000000000000000000
      00001042E07FFF7FFF7FFF7FE07FFF7FFF7FFF7FE07FFF7FFF7FFF7FE07FFF7F
      FF7FFF7FE07FFF7FFF7FFF7F1863000000000000000000001042FF7FFF7F1F00
      1F001F001F001F001F001F001F001F001F001F001F00FF7FFF7FFF7F007C1863
      007C1863FF7FFF7F18630000000000000000000000000000000000000000FF7F
      FF7FFF7FFF7FFF7FFF7F10421863FF7F1863FF7F186318631042FF7F1863FF7F
      18631042186318631863186318631863000000001863E07F1042E07FE07FE07F
      E07FE07FE07FE07F1042E07F1863000000001042E07F1042E07FE07FE07FE07F
      E07F1042E07F104200001863E07F104200000000000000000000000000000000
      000000001042FF7FE07FFF7FFF7FFF7FE07FFF7FFF7FFF7FE07FFF7FFF7FFF7F
      E07FFF7FFF7FFF7FE07FFF7F1863000000000000000000001042FF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F007C
      007C1863FF7FFF7F1863000000000000000000000000000000000000FF7FFF7F
      FF7FFF7FFF7FFF7F10421863FF7F1863FF7F1863FF7F1863FF7F1863FF7F1863
      FF7F1042186318631863186318631863000000001863E07F1042E07FE07FE07F
      E07FE07FE07FE07F1042E07F1863000000001042E07F1042E07FE07FE07FE07F
      E07F1042E07F10420000E07F1863004200000000000000000000000000000000
      000000001042FF7FFF7FFF7FE07FFF7FFF7FFF7FE07FFF7FFF7FFF7FE07FFF7F
      FF7FFF7FE07FFF7FFF7FFF7F1863000000000000000000001042FF7FFF7F1F00
      1F001F001F001F001F001F001F001F001F001F001F001F001F00FF7FFF7FFF7F
      007CFF7FFF7FFF7F186300000000000000000000000000000000FF7FFF7FFF7F
      FF7FFF7FFF7F10421863FF7F1863FF7F1863FF7F1863FF7F186318631863FF7F
      18631863104218631863186318631863000000001042E07F10421863E07FE07F
      E07FE07FE07F18631042E07F1042000000001042E07F1042E07FE07FE07FE07F
      E07F1042E07F104200001863E07F104200000000000000000000000000000000
      000000000000000000000000FF7FFF7FE07FFF7FFF7FFF7FE07FFF7FFF7FFF7F
      E07FFF7FFF7FFF7FE07FFF7F1863000000000000000000001042FF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7F18630000000000000000000000000000FF7FFF7FFF7FFF7F
      FF7FFF7F10421863FF7F1863FF7F1863FF7F1863FF7F100218631863FF7F1863
      FF7F1863FF7F10421042186318631863000000000000E07FE07F1042E07FE07F
      E07FE07FE07F1042E07FE07F0000E07F00001042E07F1042E07FE07FE07FE07F
      E07F1042E07F10420000E07F1863004200000000000000000000000000000000
      0000000010021002100210020000FF7FFF7FFF7FE07FFF7FFF7FFF7FE07FFF7F
      FF7FFF7FE07FFF7FFF7FFF7F1863000000000000000000001042FF7FFF7F1F00
      1F001F001F001F001F001F001F001F001F001F001F001F001F001F001F001F00
      1F00FF7FFF7FFF7F1863000000000000000000000000FF7FFF7FFF7FFF7FFF7F
      FF7F10421863FF7F1863FF7F1863FF7F1863FF7F10021002104218631863FF7F
      1863FF7F1863FF7FFF7F1042104218630000000000001042E07F1042E07FE07F
      E07FE07FE07F1042E07F18630000186300001042E07F1863E07FE07FE07FE07F
      E07F1863E07F104200001863E07F104200000000000010021002100200000000
      0000000010021002100210020000FF7FE07FFF7FFF7FFF7FE07FFF7FFF7FFF7F
      E07FFF7FFF7FFF7FE07FFF7F1863000000000000000000001042FF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7F186300000000000000000000FF7FFF7FFF7FFF7FFF7FFF7F
      10421863FF7F1863FF7F1863FF7F1863FF7F10021002100210421863FF7F1863
      FF7F1863FF7FFF7FFF7FFF7FFF7F00000000000000001042E07F1863E07FE07F
      E07FE07FE07F1863E07F10420000E07F00001042E07F1863E07FE07FE07FE07F
      E07F1863E07F10420000E07F1863004200000000100210021002100210020000
      00000000000000001002100210020000FF7FFF7FE07FFF7FFF7FFF7FE07FFF7F
      FF7FFF7FE07FFF7FFF7FFF7F1863000000000000000000001042FF7FFF7F1F00
      1F001F001F001F001F001F001F001F001F001F001F001F001F001F001F001F00
      1F00FF7FFF7FFF7F186300000000000000000000FF7FFF7FFF7FFF7FFF7F1042
      1863FF7F1863FF7F1863FF7F1863FF7F1002100210021002104218631863FF7F
      1863FF7FFF7FFF7FFF7FFF7F000000000000000000000000E07FE07FE07FE07F
      E07FE07FE07FE07FE07F0000E07F1863E07F0000E07FE07FE07FE07FE07FE07F
      E07FE07FE07F0000E07F1863E07F104200000000FF0310021002100210020000
      10021002100210020000100210020000FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      E07FFF7FFF7FFF7FE07FFF7F1863000000000000000000001042FF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7F186300000000000000000000FF7FFF7FFF7FFF7F10421863
      FF7F1863FF7F1863FF7F1863FF7F1002100210021002100210421863FF7F1863
      FF7FFF7FFF7FFF7FFF7F00000000000000000000000000000000104218631863
      E07F1863186310420000E07F1863E07F1863E07F0000104218631863E07F1863
      186310420000E07F1863E07F1863004200000000FF0310021002100210020000
      10021002100210020000100210020000000000000000000000000000FF7FFF7F
      FF7FFF7FE07FFF7FFF7FFF7F1863000000000000000000001042FF7FFF7F0000
      0000000000000000000000000000FF7FFF7F1F001F001F001F001F001F001F00
      1F00FF7FFF7FFF7F186300000000000000000000FF7FFF7FFF7F10421863FF7F
      1863FF7F1863FF7F1863FF7F100210021002100210021002100210421863FF7F
      FF7FFF7FFF7FFF7F000000000000000000000000000000000000000000001042
      1863104200000000E07F1863E07F1863E07F1863E07F00000000104218631042
      00000000E07F1863E07F1863E07F104200000000FF03FF031002100210020000
      100210021002100200001002100200001042104210421042104210421042FF7F
      E07FFF7FFF7FFF7FE07FFF7F1863000000000000000000001042FF7FFF7F1042
      007C007C0000FF7FFF7FFF7F0000FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7F186300000000000000000000FF7FFF7F1042186318631863
      18631863FF7F1863FF7F18631002100210021002100210021002104218631863
      FF7FFF7FFF7F0000000000000000000000000000000000000000000000001863
      E07F18630000E07F1863E07F1863E07F1863E07F1863E07F00001863E07F1863
      0000E07F1863E07F1863E07F1863004200000000FF03FF031002100210020000
      FF03FF03FF03FF031002100210020000FF7FFF7FE07FFF7FFF7FFF7FE07FFF7F
      FF7FFF7FE07FFF7FFF7FFF7F1863000000000000000000001042FF7FFF7F1042
      0000000000000000000000000000FF7FFF7F1F001F001F001F001F001F001F00
      1F00FF7FFF7FFF7F186300000000000000000000FF7F10421863186318631863
      186318631863FF7F1863FF7F1002100210021002100210021002100210421863
      1863FF7F0000000010020000000000000000000000000000000000001042E07F
      E07FE07F10420000E07F1863E07F1863E07F1863E07F00001042E07FE07FE07F
      10420000E07F1863E07F1863E07F1042000000000000FF03FF03FF0300001042
      10420000FF031002100210020000FF7FE07FFF7FFF7FFF7FE07FFF7FFF7FFF7F
      E07FFF7FFF7FFF7FE07FFF7F1863000000000000000000001042FF7FFF7F1042
      FF7FFF7F0000E07FE07FE07F0000FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7F186300000000000000000000104218631863186318631863
      186318631863186318631863FF7F100210021002100210021002100210021042
      186300000000000010021002000000000000000000000000000000001042E07F
      E07FE07F104200001863E07F1863E07F1863E07F186300001042E07FE07FE07F
      104200001863E07F1863E07F1863004200000000000000000000000000000000
      00000000FF03FF03FF0310020000FF7FFF7FFF7FE07FFF7FFF7FFF7FE07FFF7F
      FF7FFF7FE07FFF7FFF7FFF7F1863000000000000000000001042FF7FFF7F1042
      000000000000E07FE07FE07F0000FF7FFF7F1F001F001F001F001F001F001F00
      1F00FF7FFF7FFF7F186300000000000000000000000000001863186318631863
      1863186318631863186318631863100210021002100210021002100210021002
      1002000000000000100210021002000000000000000000000000000000001863
      E07F186300001863E07F1863E07F1863E07F1863E07F186300001863E07F1863
      00001863E07F1863E07F1863E07F104200000000000000000000000000000000
      000000000000000000000000FF7FFF7FE07FFF7FFF7FFF7FE07FFF7FFF7FFF7F
      E07FFF7FFF7FFF7FE07FFF7F1863000000000000000000001042FF7FFF7F1042
      FF7FFF7F0000E07FE07FE07F0000FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7F186300000000000000000000000000000000000018631863
      1863186318631863186318631863FF7F10021002100210021002100210021002
      1002100210021002100210021002100200000000000000000000000018630000
      000000001863E07F1863E07F1863E07F1863E07F1863E07F1863000000000000
      1863E07F1863E07F1863E07F1863004200000000000000000000000000000000
      000000001042FF7FFF7FFF7FE07FFF7FFF7FFF7FE07FFF7FFF7FFF7FE07FFF7F
      FF7FFF7FE07FFF7FFF7FFF7F1863000000000000000000001042FF7FFF7F1042
      FF7FFF7F0000E07FE07FE07F0000FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F1042
      0000000000000000000000000000000000000000000000000000000000000000
      18631863186318631863186318631863FF7F1002100210021002100210021002
      10021002100210021002100210021002100200000000000000000000E07F1863
      E07F1863E07F1863E07F1863E07F1863E07F1863E07F1863E07F1863E07F1863
      E07F1863E07F1863E07F1863E07F104200000000000000000000000000000000
      000000001042FF7FE07FFF7FFF7FFF7FE07FFF7FFF7FFF7FE07FFF7FFF7FFF7F
      E07FFF7FFF7FFF7FE07FFF7F1863000000000000000000001042FF7FFF7F1042
      FF7FFF7F00000000000000000000FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F1042
      FF7FFF7F18631042000000000000000000000000000000000000000000000000
      000000001863186318631863186318631863FF7F100210021002100210021002
      100210021002100210021002100210020000000000000000000000001863E07F
      1863E07F1863E07F1863E07F1863E07F1863E07F1863E07F1863E07F1863E07F
      1863E07F1863E07F1863E07F1863004200000000000000000000000000000000
      000000001042FF7FFF7FFF7FE07FFF7FFF7FFF7FE07FFF7FFF7FFF7FE07FFF7F
      FF7FFF7FE07FFF7FFF7FFF7F1863000000000000000000001042FF7FFF7F1042
      FF7FFF7F00001F001F001F000000FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F1042
      FF7F186310420000000000000000000000000000000000000000000000000000
      000000000000000018631863186318631863FF7FFF7F18630000186310021002
      10021002100210021002100210020000000000000000000000000000E07F1863
      E07F1863E07F1863E07F1863E07F1863E07F1863E07F1863E07F1863E07F1863
      E07F1863E07F1863E07F1863E07F104200000000000000000000000000000000
      000000001042FF7FE07FFF7FFF7FFF7FE07FFF7FFF7FFF7FE07FFF7FFF7FFF7F
      E07FFF7FFF7FFF7FE07FFF7F1863000000000000000000001042FF7FFF7F1042
      1042104210421042104210420000FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F1042
      1863104200000000000000000000000000000000000000000000000000000000
      000000000000000000000000186318631863FF7FFF7F00000000000000000000
      0000000000000000100210020000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000001863E07F
      1863E07F18630042104200421042004200000000000000000000000000000000
      0000000010421042104210421042104210421042104210421042104210421042
      1042104210421042104210421042000000000000000000001042FF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F1042
      1042000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000001863FF7F000000000000000000000000
      0000000000000000100200000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000001863
      E07F1863E07F1863E07F1863E07F000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000001042FF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F1042
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000001042104210421042
      1042104210421042104210421042104210421042104210421042104210421042
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000186318631863
      1863186318631863000000420042004200420042004200001863186318631863
      1863186318630000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      FF7F000000000000000000000000000000000000000000000000000000000000
      0000FF7F00000000000000000000000000000000000000000000186318631863
      1863186318631863000000420042004200420042004200001863186318631863
      1863186318630000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000104210421042104210421042
      1042104210421042104210421042104210421042104210421042104210421042
      104210421042104210421042104210421042000000000000000000000000FF7F
      0000FF7F00000000000000000000000000000000000000000000000000000000
      FF7F0000FF7F0000000000000000000000000000000000000000186318631863
      1863186318631863000000420042004200420042004200001863186318631863
      1863186318630000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000010420040004000400040000000000000
      FF7F000000000000004000400040004000400040004000400040004000000000
      0000FF7F00000000000000400040004000400000000000000000186318631863
      1863186318631863000000420042004200420042004200001863186318631863
      1863186318630000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000FF7F10421042104210421042
      1042104210421042104210421042104210421042104210421042104210421042
      1042104210421042104210421042000010420040007C007C007C004000000000
      0000000000000040007C007C007C007C007C007C007C007C007C007C00400000
      00000000000000000040007C007C007C00400000000000000000186318631863
      1863186318631863000000420042004200420042004200001863186318631863
      1863186318630000000000000000000000001042104210421042104210421042
      1042104210421042104210421042104210421042104210421042104210421042
      1042104210421042104210421042104200000000FF7F1863FF7F1863FF7F1863
      FF7F1863FF7F1863FF7F1863FF7F1863FF7F1863FF7F1863FF7F1863FF7F1863
      FF7F1863FF7F1863FF7F18631042000010420040007C007C007C007C00400000
      000000000040007C007C007C007C007C007C007C007C007C007C007C007C0040
      0000000000000040007C007C007C004000000000000000000000186318631863
      1863186318631863000000420042004200420042004200001863186318631863
      1863186318630000000000000000000000001042186318631863186318631863
      186318631863186318631863FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7F104200000000FF7FFF7F0000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000FF7F1042000010420040E07F007C007C007C007C0040
      00400040007C007C007C007C007C007C007C007C007C007C007C007C007C007C
      004000400040007C007C00400040000000000000000000000000186318631863
      1863186318631863000000420042004200420042004200001863186318631863
      1863186318630000000000000000000000001042186318631863186318631863
      186318631863186318631863FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7F104200000000FF7F18630000FF7F10421042
      1042104210421042104210421042104210421042104210421042104210421042
      10421042104210420000186310420000104200000040007C007C007C007C007C
      007C007C007C007C007C007C007C007CFF7FFF7F007C007C007C007C007C007C
      007C007C007C0040004000000000000000000000000000000000186318631863
      1863186318631863000000420042004200420042004200001863186318631863
      1863186318630000000000000000000000001042186318631863186318631863
      186318631863186318631863FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7F104200000000FF7FFF7F0000FF7F1863FF7F
      1863FF7F1863FF7F1863FF7F1863FF7F1863FF7F1863FF7F1863FF7F1863FF7F
      1863FF7F186310420000FF7F10420000104200000040007C007C007C007C007C
      007C007C007C007C007C007C007C007C007C007C007C007C007C007C007C007C
      0040004000400000000000000000000000000000000000000000186318631863
      1863186318631863000000000000000000000000000000001863186318631863
      1863186318630000000000000000000000001042186318631863186318631863
      186318631863186318631863FF7F104210421042104210421042104210421042
      104210421042104210421042FF7F104200000000FF7F18630000FF7FFF7F1863
      FF7F1863FF7F1863FF7F1863FF7F1863FF7F1863FF7F18631863000000000000
      00000000FF7F1042000018631042000010420000000000400040004000401863
      FF7FFF7FFF7F007C0040007C0040FF7FFF7FFF7FFF7FFF7FFF7F186310421042
      0000000000000000000000000000000000000000000000000000186318631863
      1863186318631863186318631863186318631863186318631863186318631863
      1863186318630000000000000000000000001042186318631863186318631863
      186318631863186318631863FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7F104200000000FF7FFF7F0000FF7F1863FF7F
      1863FF7F1863FF7F1863FF7F1863FF7F1863FF7F1863FF7F1863FF7FFF7FFF7F
      FF7F0000186310420000FF7F1042000010420000000000000000000000000040
      00401863FF7F007C0040007C0040FF7FFF7FFF7FFF7F18631042104200000000
      0000000000000000000000000000000000000000000000000000186318631863
      1863186318631863186318631863186318631863186318631863186318631863
      1863186318630000000000000000000000001042186318631863186318631863
      186318631863186318631863FF7F104210421042104210421042104210421042
      104210421042104210421042FF7F104200000000FF7F18630000FF7FFF7F1863
      FF7F1863FF7F1863FF7F1863FF7F1863FF7F1863FF7F18631863186318631863
      18631863FF7F104200001863104200001042000000000000000000000000FF7F
      FF7F00400040007C0040007C0040FF7FFF7F1863104210420000000000000000
      0000000000000000000000000000000000000000000000000000186318631863
      1863186318631863186318631863186318631863186318631863186318631863
      1863186318630000000000000000000000001042186318631863186318631863
      186318631863186318631863FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7F104200000000FF7FFF7F0000FF7F1863FF7F
      1863FF7F1863FF7F1863FF7F1863FF7F1863FF7F1863FF7F1863000000000000
      00000000186310420000FF7F104200001042000000000000000000000000FF7F
      FF7F0000FF7F0040004000400040004000400040000000000000000000000000
      1042000000000000000000000000000000000000000000000000186318631863
      1863186318631863186318631863186318631863186318631863186318631863
      1863186318630000000000000000000000001042186318631863186318631863
      186318631863186318631863FF7F104210421042104210421042104210421042
      104210421042104210421042FF7F104200000000FF7F18630000FF7FFF7F1863
      FF7F1863FF7F1863FF7F1863FF7F1863FF7F1863FF7F18631863FF7FFF7FFF7F
      FF7F0000FF7F104200001863104200001042000000001042000000000000FF7F
      0000000018631042000000000000000000000000000000000000000000001042
      E07F000000000000000000000000000000000000000000000000000000001863
      1863186318631863186318631863186318631863186318631863186318631863
      1863000000000000000000000000000000001042186310421042104210421042
      104210421042104218631863FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7F104200000000FF7FFF7F0000FF7F1863FF7F
      1863FF7F1863FF7F1863FF7F1863FF7F1863FF7F1863FF7F1863186318631863
      18631863186310420000FF7F10420000104200001042FF7F0000000000000000
      000000001042000000000000000000000000000000000000000000001042FF7F
      00000000000000000000000000000000000000000000E07FE07FE07FE07F0000
      1863186318631863186318631863186318631863186318631863186318631863
      0000E07FE07FE07FE07F00000000000000001042186310420000100210020000
      000010020000104218631863FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7F104200000000FF7F18630000FF7FFF7F1863
      FF7F1863FF7F1863FF7F1863FF7F1863FF7F1863FF7F18631863000000000000
      00000000FF7F1042000018631042000010421042FF7FFF7F00000000FF7F0000
      00000000000000000000000000000000000000000000000000001042E07F0000
      00000000000000000000000000000000000000000000E07FE07FE07FE07FE07F
      0000186318631863186318631863186318631863186318631863186318630000
      E07FE07FE07FE07FE07F0000000000000000104218631042104200000000FF7F
      FF7F00001042104218631863FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7F104200000000FF7FFF7F0000FF7F1863FF7F
      1863FF7F1863FF7F1863FF7F1863FF7F1863FF7F1863FF7F1863FF7FFF7FFF7F
      FF7F0000186310420000FF7F1042000010421042FF7FFF7F0000FF7F18630000
      0000186300000000000000001042000000000000000000001042FF7F00000000
      000000000000000000000000000000000000000000000000E07FE07FE07FE07F
      E07F00001863186318631863186318631863186318631863186318630000E07F
      E07FE07FE07FE07F0000000000000000000010421863104210420000FF7FFF7F
      000000001042104218631863FF7F100010001000100010001000100010001000
      100010001000100010001000FF7F104200000000FF7F18630000FF7FFF7F1863
      FF7F1863FF7F1863FF7F1863FF7F1863FF7F1863FF7F18631863186318631863
      18631863FF7F1042000018631042000010421042FF7FFF7F0000FF7F18630000
      18631863FF7F00000000FF7FFF7F10420000000000001042E07F000000000000
      0000000000000000000000000000000000000000000000000000E07FE07FE07F
      E07FE07F000018631863186318631863186318631863186318630000E07FE07F
      E07FE07FE07F000000000000000000000000104218631042000000000000FF7F
      FF7FFF7F0000104218631863FF7F100010001000100010001000100010001000
      100010001000100010001000FF7F104200000000FF7FFF7F0000FF7F1863FF7F
      1863FF7F1863FF7F1863FF7F1863FF7F1863FF7F1863FF7F1863FF7F1863FF7F
      1863FF7F186310420000FF7F1042000010421042FF7F00000000186310420000
      1863FF7FFF7F0000FF7FFF7FFF7FFF7F104200001042FF7F0000000000000000
      00000000000000000000000000000000000000000000000000000000E07FE07F
      E07FE07FE07F0000186318631863186318631863186318630000E07FE07FE07F
      E07FE07F000000000000000000000000000010421863000018631863FF7FFF7F
      FF7FFF7F0000104218631863FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7F104200000000FF7F18630000FF7FFF7F1863
      FF7F1863FF7F1863FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7F000018631042000010420000000000000000104200000000
      FF7F1863FF7F0000FF7FFF7FFF7FFF7FFF7F1042E07F00000000000000000000
      000000000000000000000000000000000000000000000000000000000000E07F
      E07FE07FE07FE07F00001863186318631863186318630000E07FE07FE07FE07F
      E07F00000040000000000000000000000000104218630000186318630000FF7F
      FF7FFF7FFF7F000018631863FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7F104200000000FF7FFF7F0000FF7F1863FF7F
      1863FF7F1863FF7F000000000000000000000000000000000000000000000000
      00000000000000000000FF7F1042000010420000000000000000000000000000
      0000FF7F000000000000FF7FFF7FFF7F1042FF7F000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      E07FE07FE07FE07FE07F000018631863186318630000E07FE07FE07FE07FE07F
      000000400040000000000000000000000000104218630000186318630000FF7F
      FF7F00000000104218631863FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7F104200000000FF7F18630000FF7FFF7F1863
      FF7F1863FF7FFF7F0000FF7F1863FF7F1863FF7F186318630000FF7F1863FF7F
      1863FF7F18631863000018631042000010420000000000001863000000000000
      000000000000000000000000FF7F1042E07F0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000E07FE07FE07FE07FE07F0000186318630000E07FE07FE07FE07FE07F0000
      0000004000400000000000000000000000001042186300001863186318630000
      FF7FFF7F0000104218631863FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7F104200000000FF7FFF7F18630000FF7FFF7F
      FF7FFF7FFF7F000018630000FF7F1863FF7FFF7F1863000018630000FF7F1863
      FF7F1863186300001863FF7F1042000010420000000018631863FF7F00000000
      FF7FFF7F00000000000000001042FF7F00000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000E07FE07FE07FE07FE07F00000000E07FE07FE07FE07FE07F00000000
      0000004000400000000000000000000000001042186310420000186318631863
      186318630000104218631863FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7F104200000000FF7F1863FF7F000000000000
      0000000000000000FF7F0000000000000000000000000000FF7F000000000000
      0000000000000000FF7F1863104200001042000010421863FF7FFF7F0000FF7F
      FF7FFF7FFF7F000000001042E07F000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000E07FE07FE07FE07FE07FE07FE07FE07FE07FE07F000000000000
      0000004000400000000000000000000000001042186310421042000000000000
      000000001042104218631863FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7F104200000000FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7F0000104200001042FF7F1863FF7F0000FF7F
      FF7FFF7FFF7FFF7F1042FF7F0000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000E07FE07FE07FE07FE07FE07FE07FE07F0000000000000000
      0000004000400000000000000000000000001042186318631863186318631863
      186318631863186318631863FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7F104200000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000001042000000001042FF7F000000000000
      FF7FFF7FFF7F1042E07F00000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000E07FE07FE07FE07FE07FE07F00000000000000000000
      0000004000400000000000000000000000001042104210421042104210421042
      1042104210421042104210421042104210421042104210421042104210421042
      10421042104210421042104210421042000000001F001F001F001F001F001F00
      1F001F001F001F001F001F001F001F001F001F001F001F001F001F001F001F00
      1F00000018630000186300001863000010420000000000001042000000000000
      0000FF7F1042FF7F000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000E07FE07FE07FE07F000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00001042E07F0000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000E07FE07F0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      1042FF7F00000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000001042
      E07F000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000001042
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000080000000200100000100010000000000001200000000000000000000
      000000000000000000000000FFFFFF00C0FFFFFF000000000000000000000000
      C07FFFFF000000000000000000000000C03FFFFF000000000000000000000000
      C01FFFFF000000000000000000000000C00FFFFF000000000000000000000000
      C007FFFF000000000000000000000000E003FFFF000000000000000000000000
      F00303FF000000000000000000000000E0000003000000000000000000000000
      C0000003000000000000000000000000C0000003000000000000000000000000
      C0000003000000000000000000000000C0000003000000000000000000000000
      C0000003000000000000000000000000C0000003000000000000000000000000
      C0000003000000000000000000000000C0000003000000000000000000000000
      C0000003000000000000000000000000C0000003000000000000000000000000
      C0000003000000000000000000000000C0000003000000000000000000000000
      C0000003000000000000000000000000C0000003000000000000000000000000
      C0000003000000000000000000000000C0000003000000000000000000000000
      C0000003000000000000000000000000C0000003000000000000000000000000
      C0000003000000000000000000000000C0000003000000000000000000000000
      C0000003000000000000000000000000C0000003000000000000000000000000
      C0000003000000000000000000000000FFFFF3FFFFFF8007FF8001FFFFFFFFFF
      FFFFC3FFFFFE0003FF8001FFFFF803FFFFFF01FFFFF00001FF8001FFFFC000FF
      FFFC01FFFFC0000100000000FF00003FFFF000FFFF80000000000000FE00001F
      FFC000FFFE00000000000000FC00000FFF00007FC000000000000000F8000007
      FC00007F8000000000000000F0000003F000003F0000000000000000F0000003
      C000003F0000000000000000E00000010000001F0000000100000000E0000001
      0000001F0000000300000000E00000008000000F0000000300000000E0000000
      8000000F0000000300000000E0000000C0000007000000030000000080000000
      C0000007000000030000000000000000E0000003000000010000000000000001
      E0000003800000010000000000000001C0000001800000010000000000008003
      C000000180000001000000000000800380000000C00000000000000000008007
      80000000C0000000000000008000C00F80000003C0000000000000008000401F
      8000000FE000000000000000C000403F8000003FE000000000000000E000607F
      C00000FFF000000000000000F00061FFC00003FFF800000000000000FC007FFF
      E0000FFFFC00000000000000FF007FFFF0003FFFFE00000000000000FFF0FFFF
      FF00FFFFFF80000100000000FFFFFFFFFF83FFFFFFE0001F00000000FFFFFFFF
      FFCFFFFFFFFE07FF00000000FFFFFFFF11FFFFFFFFFFFFFFFFFCFFFFFFFFFFFF
      006000FFFFFFFFFFFFF03FFFFFFFFFFF000000FFFFFFF803FFC00FFFFFFFFFFF
      400000FFFFFFF003FF0003FF8000000100000088FFFFE003FC0001FF00000000
      00000000FFFFC003F00001FF0000000000000000FFC00003C00001FF00000000
      00000002FF000003000001FF0000000000000000FC0003FF000001FF00000000
      C0000000F80003FF000001FF00000000C0000000F00001FF0000018100000000
      C0000000E00000FF0000018000000000C0000000E000007F0000018000000000
      C0000000C000007F000001C000000000C0000000C000007F000001FF00000000
      C00000008000003F0000027F00000000C00000008000003F00000C3F00000000
      C00000008000003F0000381F00000000C00000008000003F0000703F00000000
      C00000008000003F0000607F00000000C00000008000003F000040FF00000000
      C00000008000003F000041FF00000000C0000000C000007F000063F700000000
      C0000000C000007F00007FE300000000C0000000E00000FF00007FC100000001
      C0000000E00000FF00007F8100000003C0000000F00001FF00007F038000FFFF
      C0000001F80003FF0000FE07C001FFFFE0003FFFFC0007FF8C03FC0FE003FFFF
      F0007FFFFF001FFFFC0FFC1FF007FFFFF800FFFFFFC07FFFFE3FFC3FFFFFFFFF
      FC01FFFFFFFFFFFFFFFFFE7FFFFFFFFFFE7FFFFFF0000007FF00FFFFFC001FFF
      FC3FFFFFF0000007FE007FFFFF007FFFFC07FFFFF0000007FC003FFFFF81FFFF
      F800FFFFF0000007F8001FFFFF003FFFF8003FFFF0000007F0000003FC003FFF
      F000007FF0000007E0000001F8003FFFF000000FF0000007C0000001F0003FFF
      E0000001F000000780000001E0000001C0000000F000000700000001C0000000
      C0000000F000000700000001C000000080000000F00000070000000180000000
      80000000F0000007000000018000000080000000F00000070000000180000000
      80000000F0000007000000010000000000000000F00000078000000100000000
      00000000F0000007C000000100000000C0000000F0000007E000000180000000
      C0000000F0000007F000000180000000C0000000F0000007F800000180000000
      C0000000F0000007F8000001C0000000C0000000F0000001F8000000C0000000
      C0000000F0000001F8000000C0000000C0000000F0000001F8000000C0000000
      C0000000F0000001F8000000C0000000C0000000F0000007F8000000C0000000
      C0000000FFFFFC1FF8000000C0000000C0000000FFFFFE0FF8000000C0000000
      C0000001FFFFFF07F8000000C0000001E0003FFFFFFFFF87F8000000E0003FFF
      F0007FFFFFFFFFCFF8000000F0007FFFF800FFFFFFFFFFEFFC000001F800FFFF
      FC01FFFFFFFFFFFFFFFFFFC3FC01FFFFFFFFFFFFFFFFFFFFFFFFFFFFE000FFFF
      FC000000E000003FFFFCFFFFC000FFFFFC0000008000001FFFF83FFFC000FFFF
      FC0000008000000FFFF00FFF8000FFFFFC0000008000000FFFE003FF8000FFFF
      FC0000008000000FFFC000FF0000FFFFFC0000008000000FFF80003F0000FFFF
      FC0000008000000FFF00000F0000E3FFFC0000008000000FFE0000038000FCFF
      FC0000008000000FFC0000018000FF7FFC0000008000000FF800000180007F7F
      FC0000008000000FF0000001C0003FBFFC0000008000000FE0000001E0001FBF
      FC00000080000003C0000001FC003E0FFC0000008000000180000001FC00FF1F
      FC0000008000000180000001F0007FBFFC0000008000000180000001F0003FFF
      F00000008000000180000001F0007FFFC00000008000000180000003F801FFFF
      0003FEFF8000000180000007F801F0000000FC7F800000018000000FFC00C000
      0000783F800000018000001FFC0040000000701FE000001F8000001FFE008000
      80007EFFF07E007FC000001FFF038000C0007EFFF87F00FFF000001FFF8F0000
      E000FEFFFC7F81FFF000003FFFFF0000E000FDFFFE7FF8FFF00000FFFFFF0000
      E0033DFFFFFFFC7FF80000FFFFFF8000E00FC3FFFFFFFE3FFE3001FFFFFF8000
      F03FFFFFFFFFFF1FFFFC07FFFFFF8001F8FFFFFFFFFFFF8FFFFF07FFFFFFC07F
      FFFFFFFFFFFFFFFFFFFFCFFFFFFFE0FFFFFFF3FFF9FFFFFFFFFFFFFFFF8FF1FF
      FFFFC1FFF83FFFFFFE003C1FFF07E0FFFFFF00FFF007FFFFF000000FFE03C07F
      FFFC007FF000FFFFE0000007FC00001FFFF0003FE0003FFFC0000003FC00000F
      FFC0001FE00007FFC0000003FF07E0C7FF00000FE00000FFC0000001FF8FF1E7
      FC000007C0000001C0000001FFFFFFE7F0000003C000000080000001FFFFFFE7
      F00000018000000000000001E0000007F00000008000000000000001C0000007
      F00000008000000000000001CD800007F00000000000000000000001CD800007
      F00000000000000080000003C0000007F800000000000000C00000038D800007
      FC00000000000000C00000078D800007FE000001C0000000E000000F8DB00007
      F8000003C0000000E000001F8DB00007E000000FC0000000E00000FF8DB00007
      8000003FC0000000E00000FF8DB000070000007FC0000000E000007F0DB6C007
      8000003FC0000000F000003F2DB6C067C000001FC0000000F800001F2DB6C067
      E000007FC0000000FC00001F2DB6DB67F00001FFC0000000FC00001F0036DB67
      F80007FFC0000000FC00001FFF801B67FC001FFFC0000000FE00003FFFFFC007
      FE007FFFC0000001FF00007FFFFFFFCFFF01FFFFE0003FFFFFE003FFFFFFFFE1
      FF87FFFFF0007FFFFFFFFFFFFFFFFFF0FFDFFFFFF800FFFFFFFFFFFFFFFFFFF9
      FFFFFFFFFC01FFFFFFFFFFFFFFFFFFFF0000007FC0000000FFF00001FFFFFFFF
      0000003FC0000000FFE00001FFFFFFFF0000001F000000007FC00001C7FFFF8F
      0000000F000000001F00000183FFFF0700000007000000000000000180000001
      0000000300000000000000010000000000000001000000000000000100000000
      0000000000000000000000010000000000000000000000000000000100000000
      0000000000000000000000010000000000000000000000000000000180000001
      800000000000000000000001C0000003C00000000000000000000001E0000007
      E00000000000000000000001F000000FF00000000000000000000001F000000F
      F80000000000000000000001F000000FFC0000000000000000000001F000000F
      FE0000000000000000000001E0000007FF000000000000000000000120000004
      FFFFFFFF000000000000000100000000FFFEFFFFFFFC00000000000100000000
      FFFC7FFFFFFE00000000000100000000FFF83FFFFFFF00040000000120000004
      FFF01FFFFFFF800300000001F000000FFFE00FFFFFFFC001001FF000FF0000FF
      FFC007FFFFFFE000803FF000FF0000FFFFFC7FFFFFFFF000E0FFF000FF0000FF
      FFFC7FFFFFFFF800FFFFF000FF00007FFFF8FFFFFFFFFC00FFFFF801FF80007F
      FFF8FFFFFFFFFE01FFFFF801FF80007FFEF1FFFFFFFFFF03FFFFFC03FFFFFFFF
      FF07FFFFFFFFFF87FFFFFF0FFFFFFFFFF83FC07FFFFFFFFFE0000007FFFFFFFF
      F01F803FFFFFFFFFE0000007FFFCFFFFF01F803FFFE003FFE0000007FFF83FFF
      F01F803FFF00007FE0000007FFF00FFFF0000000FC00001FE0000007FFE003FF
      F0000000F800000FE0000007FFC000FFF0000000F8000007E0000007FF80003F
      F0000000FC000003E0000007FF00000FC0000000FE000001E0000007FE000003
      80000000FE000001E0000007FC00000000000000FF000001E0000007F8000000
      00000000FF000001E0000007F000000000000000FF800001E0000007E0000000
      00000000FF800001E0000007C000000000000000FF800001E000000780000000
      80000000C7000001E0000007000000008000000083000001E000000700000001
      8000000000000001E000000700000003C000000000000001E000000700000007
      E000000000000001E00000070000000FF000000000000001E00000070000000F
      F000000000000001E000000700000027F000000080000001E000000700000063
      F0000000C7000001E000000780000001F0000000FF800001E0000007E0000000
      F0000000FF800001E0000007F8000000F0000000FF800001E000000FFE000001
      F0000000FF800001E000001FFF800003F0000000FF800001E000003FFFE01FE7
      F8000000FF800001E000007FFFF83FEFFFFFF801FFFFFFFFE00000FFFFFE7FFF
      FFFFFC03FFFFFFFFE00001FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC7FFE3F
      E000001FFFFFFFFFFFFFFFFFF83FFC1FE000001FFFFFFFFFFFFFFFFFF01FF80F
      E000001FFFFFFFFF8000000080000000E000001FFFFFFFFF0000000000000000
      E000001F000000000000000000000000E000001F000000000000000000000001
      E000001F000000000000000000000003E000001F00000000000000008000000F
      E000001F00000000000000008000003FE000001F0000000000000000C00001FF
      E000001F0000000000000000F80007FFE000001F0000000000000000F0001FFF
      E000001F0000000000000000E0007E7FE000001F0000000000000000C00FFC7F
      800000070000000000000000800FF8FF8000000700000000000000000007F1FF
      8000000700000000000000000003E3FFC000000F00000000000000000001C7FF
      E000001F000000000000000000008FFFF000001F000000000000000080001FFF
      F800001F000000000000000080003FFFFC00001F000000000000000080007FFF
      FE00001F00000000000000008000FFFFFF00021F00000000000000008001FFFF
      FF80061F00000000000000008003FFFFFFC00E1F0000000000000000C007FFFF
      FFE01E1F0000000000000000E00FFFFFFFF03E1FFFFFFFFF00000001F01FFFFF
      FFF87FFFFFFFFFFFFFFFFFFFF83FFFFFFFFCFFFFFFFFFFFFFFFFFFFFFC7FFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFCFFFFFF00000000000000000000000000000000
      000000000000}
  end
  object VorgangImgList: TImageList
    Height = 20
    Width = 20
    Left = 220
    Top = 300
    Bitmap = {
      494C010111001300040014001400FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      000000000000360000002800000050000000640000000100100000000000803E
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000104210421042
      1042104210421042104200000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000010420000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000E07FFF7FFF7FFF7F
      E07FFF7FFF7FFF7F000010421042104210421042104200000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000FF7F100010001000
      100010001000FF7F000000000000000000000000000010420000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000FF7FFF7FE07FFF7F
      FF7FFF7FE07FFF7F0000FF7FE07FFF7FFF7FFF7F000010420000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000FF7F100010001000
      100010001000FF7F00001000100010001000FF7F000010420000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000E07FFF7FFF7FFF7F
      E07FFF7FFF7FFF7F0000FF7FFF7FFF7FE07FFF7F000010420000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000FF7F100010001000
      100010001000FF7F00001000100010001000FF7F000010420000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000FF7FFF7FE07FFF7F
      FF7FFF7FE07FFF7F0000FF7FE07FFF7FFF7FFF7F000010420000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000FF7F10001000FF7F
      000000000000000000001000100010421042FF7F000010420000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000E07FFF7FFF7FFF7F
      0000FF7FFF7FFF7F0000FF7FFF7F000000000000104210420000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000FF7F10001000FF7F
      0000FF7FE07F00000000FF7F10420000E0030000104210420000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000FF7FFF7FE07FFF7F
      0000FF7F0000FF7FFF7F000000000000E0030000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000FF7F100010420000E003E003E003E003E00300000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000FF7FFF7FE07F000000000000E0030000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000104210420000E0030000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000001042000000000000
      000000001042000000000000000000000000000000000000000000000000FF7F
      FF7F000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000040007C007C007C
      007C007C004000000000000000000000000000000000000000000000FF7F0000
      0000FF7FFF7F0000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F00000000
      00000000000000000000000000000000000000000000007C007C007C007C007C
      007C007C007C007C000000000000000000000000000000000000FF7F0000FF7F
      FF7F00000000FF7FFF7F00000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000FF7FFF7FFF7FFF7FFF7FFF7FFF7F00000000000000001042
      0000000000000000000000000000000000000000007C007C007C007C007C007C
      007C007C007C007C007C0000000000000000000000000000FF7F0000FF7F1042
      0000FF7FFF7F00000000FF7FFF7F000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000FF7FFF7FFF7FFF7FFF7FFF7F000010421863186310420000
      1042000000000000000000000000000010420040007C007C007C007C007C007C
      007C007C007C007C007C00401042000000000000000000000000FF7F10420000
      000010420000FF7FFF7F00000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000FF7FFF7FFF7FFF7FFF7F0000104218631863FF0310421042
      000000000000000000000000000000000000007C007C007CFF7FFF7F007C007C
      007CFF7FFF7F007C007C007C000000000000000000000000FF7F10420000FF7F
      E07F0000000010420000FF7FFF7F000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000FF7FFF7FFF7FFF7FFF7F0000186318631863186310421863
      000000000000000000000000000000000000007C007C007C007CFF7FFF7F007C
      FF7FFF7F007C007C007C007C00000000000000000000000000000000FF7FE07F
      FF7FE07FFF7F0000000010420000000000000000000000000000000000000000
      1700000000000000000000000000000000000000000000000000000000000000
      0000000000000000FF7FFF7FFF7FFF7FFF7F00001863FF031863186310421863
      000000000000000000000000000000000000007C007C007C007C007CFF7FFF7F
      FF7F007C007C007C007C007C0000000000000000000000000000FF7FE07FFF7F
      E07FFF7FE07FFF7FE07F00000000000000000000000000000000000000000000
      1700170000000000000000000000000000000000000000000000000000000000
      0000000000000000FF7FFF7FFF7FFF7FFF7F00001042FF03FF03186310421042
      000000000000000000000000000000000000007C007C007C007C007CFF7FFF7F
      FF7F007C007C007C007C007C000000000000000000000000FF7FE07FFF7FE07F
      FF7FE07FFF7FE07FFF7FE07FFF7F000000000000000000000000000017001700
      1700170017000000000000000000000000000000000000000000000000000000
      0000000000000000FF7FFF7FFF7FFF7FFF7FFF7F000010421863186310420000
      000000000000000000000000000000000000007C007C007C007CFF7FFF7F007C
      FF7FFF7F007C007C007C007C00000000000000000000000000000000E07FFF7F
      E07FFF7FE07FFF7FE07FFF7F0000000000000000000000000000000000000000
      1700170000000000000000000000000000000000000000000000000000000000
      0000000000000000FF7FFF7FFF7FFF7FFF7FFF7FFF7F00000000000000000000
      0000000000000000000000000000000010420040007C007CFF7FFF7F007C007C
      007CFF7FFF7F007C007C00401042000000000000000000000000000000000000
      FF7FE07FFF7FE07FFF7F00000000000000000000000000000000000000000000
      1700000000000000000000000000000000000000000000000000000000000000
      0000000000000000FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F00000000
      0000000000000000000000000000000000000000007C007C007C007C007C007C
      007C007C007C007C007C00000000000000000000000000000000000000000000
      00000000E07F0000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000FF7FFF7FFF7FFF7FFF7FFF7FFF7F00000000000000000000
      00000000000000000000000000000000000000000000007C007C007C007C007C
      007C007C007C007C000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000FF7FFF7FFF7FFF7FFF7FFF7FFF7F00001863000000000000
      0000000000000000000000000000000000000000000000000040007C007C007C
      007C007C00400000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000FF7FFF7FFF7FFF7FFF7FFF7FFF7F00000000000000000000
      0000000000000000000000000000000000000000000000001042000000000000
      0000000010420000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000010421042104210421042104210421042
      1042104210421042104210421042104200000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000104210421042104210421042104210421042000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000001042104200000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000104210421042104210421042104210421042104210420000
      0000000000000000000000000000E07FFF7FE07F0000FF7FFF7FFF7FFF7FFF7F
      FF7FFF7F0000E07FFF7FE07F0000104200000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000001700170017001700170017001700170017001700104210420000
      0000000000000000000000000000FF7FE07FFF7F0000FF7F00000000FF7FFF7F
      FF7FFF7F0000FF7FE07FFF7F0000104200000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000001700FF7FE07FE07FFF7F0000000000000000FF7F104210420000
      0000000000000000000000000000E07FFF7FE07F0000FF7F10421042FF7FFF7F
      FF7FFF7F0000E07FFF7FE07F0000104200000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000001700E07FFF7FFF7FE07F0000E05EE05EE05E0000104210420000
      0000000000000000000000000000FF7FE07FFF7F0000FF7FFF7FFF7FFF7FFF7F
      FF7FFF7F0000FF7FE07FFF7F0000104200000000000000000000000000000000
      0000F70200000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000F70200000000000000000000000000000000
      0000000000001700FF7FE07FE07FFF7F0000E05EE07FE05EE05E104210420000
      0000000000000000000000000000E07FFF7FE07FFF7F00000000000000000000
      00000000FF7FE07FFF7FE07F0000104200000000000000000000000000000000
      F702F70200000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000F702F7020000000000000000000000000000
      0000000000001700FF7FE07FE07FFF7F0000FF7FE07FE07FE07FE05E00001042
      0000000000000000000000000000FF7FE07FFF7FE07FFF7FE07FFF7FE07FFF7F
      E07FFF7FE07FFF7FE07FFF7F000010420000000000000000000000000000F702
      F702F702F702F702F702F7020000000000000000000000000000000000000000
      000000000000F702F702F702F702F702F702F702000000000000000000000000
      0000000000001700E07FFF7FFF7FE07FFF7FFF7FFF7FE07FE07FE05EE05E0000
      1042000000000000000000000000E07FFF7FE07FFF7FE07FFF7FE07FFF7FE07F
      FF7FE07FFF7FE07FFF7FE07F00001042000000000000000000000000F702F702
      F702F702F702F702F702F7020000000000000000000000000000000000000000
      000000000000F702F702F702F702F702F702F702F70200000000000000000000
      0000000000001700FF7FE07FE07FFF7FE07F0000FF7FFF7FE07FE07FE05EE05E
      1042104200000000000000000000FF7FE07FFF7FE07FFF7FE07FFF7FE07FFF7F
      E07FFF7FE07FFF7FE07FFF7F000010420000000000000000000000000000F702
      F702F702F702F702F702F7020000000000000000000000000000000000000000
      000000000000F702F702F702F702F702F702F702000000000000000000000000
      0000000000001700E07FFF7FFF7FE07FFF7FFF7F0000FF7FFF7FE07FE07FE05E
      0000104210420000000000000000E07FFF7FE07FFF7FE07FFF7FE07FFF7FE07F
      FF7FE07FFF7FE07FFF7FE07F0000104200000000000000000000000000000000
      F702F70200000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000F702F7020000000000000000000000000000
      0000000000001700E07FFF7FFF7FE07FFF7F1700170017000000FF7FE07FE07F
      0000000000000000000000000000FF7FE07FFF7F000000000000000000000000
      000000000000FF7FE07FFF7F0000104200000000000000000000000000000000
      0000F70200000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000F70200000000000000000000000000000000
      0000000000001700FF7FE07FE07FFF7FE07F1700FF7FE07F1700FF7FFF7FE07F
      E05EE05E00000000000000000000E07FFF7F0000FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7F0000E07FFF7FE07F0000104200000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000001700E07FFF7FFF7FE07FFF7F1700E07F170000000000FF7F0000
      0000E05E00000000000000000000FF7FE07F0000FF7F1F001F001F001F001F00
      1F00FF7F0000FF7FE07FFF7F0000104200000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000001700170017001700170017001700000000000000000000000000
      0000000000000000000000000000E07FFF7F0000FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7F0000E07FFF7FE07F0000104200000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FF7FE07F0000FF7F1F001F001F001F001F00
      1F00FF7F0000FF7F0000FF7F0000104200000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000E07FFF7F0000FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7F0000E07FFF7FE07F0000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000104210421042104210421042
      1042104210421042000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000104210421042104210421042
      1042104210421042000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000001700
      1700170017001700170017001700170017001700170000000000000000000000
      0000000000000000000000000000FF7F0000FF7F0000FF7F0000FF7F00000000
      0000000000000000000000000000000000001700170017001700170017001700
      1700170017001042000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000001700
      1042104210421042104210421042104210421042170000000000000000000000
      0000000000000000000000000000FF7F0000FF7F0000FF7F0000FF7F00000000
      0000000000000000000000000000000000001700E07FFF7FE07FE07FFF7FE07F
      FF7FFF7F17001042000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000001700
      0000000000000000000000000000000000000000170000000000000000000000
      0000000000000000000000000000FF7F0000FF7F0000FF7F0000FF7F00000000
      0000000000000000000000000000000000001700FF7FE07FFF7FFF7FE07FFF7F
      E07FE07F17001042000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000001700
      0000000000000000000000000000000000000000170000000000000000000000
      0000000000000000000000000000FF7F0000FF7F0000FF7F0000FF7F00000000
      0000000000000000000000000000000000001700E07FFF7FE07FE07FFF7FE07F
      FF7FFF7F17001042000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000001700
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FF7F0000FF7F0000FF7F0000FF7F00000000
      0000000000000000000000000000000000001700E07FFF7FE07FE07FFF7FE07F
      FF7FFF7F17001042000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000001700
      000000000000000000000000000000000000E07F104200000000000000000000
      0000000000000000000000000000FF7F0000FF7F0000FF7F0000FF7F00000000
      0000000000000000000000000000000000001700FF7FE07FFF7FFF7FE07FFF7F
      E07FE07F17001042000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000001700
      000000000000000000000000000000000000E07F000000000000000000000000
      0000000000000000000000000000FF7F0000FF7F0000FF7F0000FF7F00000000
      0000000000000000000000000000000000001700E07FFF7FE07FE07FFF7FE07F
      FF7FFF7F17001042000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000001700
      1700170017001700170017001700170017000000E07F10420000000000000000
      0000000000000000000000000000FF7F0000FF7F0000FF7F0000FF7F00000000
      0000000000000000000000000000000000001700FF7FE07FFF7FFF7FE07FFF7F
      E07FE07F17001042000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000001700
      FF7F17001700FF7F17001700FF7F170017000000E07F00000000000000000000
      0000000000000000000000000000FF7F0000FF7F0000FF7F0000FF7F00000000
      0000000000000000000000000000000000001700FF7FE07FFF7FFF7FE07F1700
      1700170017000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000001700
      17001700170017001700170017001700170017000000E07F1042000000000000
      0000000000000000000000000000FF7F0000FF7F0000FF7F0000FF7F00000000
      000000000000000000000000000000000000FF7FE07FFF7FE07FE07FFF7F1700
      FF7F170000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000E07F1042000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FF7F0000FF7FFF7FE07FFF7FFF7FE07F1700
      E07F000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000175C00000000
      000000000000000000000000FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F0000
      0000000000000000000000000000FF7FFF7FFF7FFF7FFF7F1700170017001700
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000175C175C00000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FF7FFF7F000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FF7F0000FF7FFF7F00000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000104210421042104210421042104210421042104210421042104200000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000010421042000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000040007C007C104200000000000000000000004010420000000000000000
      0000104210421042104210421042104210421042104210420000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000FF7FFF7FFF7FFF7FFF7FFF7F000000000000000000000000
      00000040007C007C007C10420000000000000040007C007C1042000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000FF7F1042104210421042FF7F000000000000000000000000
      00000040007C007C007C007C104200000040007C007C007C007C104200000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000E07F0000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000FF7FFF7FFF7FFF7FFF7FFF7F000000000000000000000000
      000000000040007C007C007C007C1042007C007C007C007C007C104200000000
      0000104210421042104210421042104200000000000000000000000000000000
      00000000E07F0000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000FF7F1042104210421042FF7F000000000000000000000000
      0000000000000040007C007C007C007C007C007C007C007C1042000000000000
      00000000000000000000000000000000000000000000000000000000E07F0000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000017001700170017001700
      1700170017000000FF7FFF7FFF7FFF7FFF7FFF7F000000000000000000000000
      00000000000000000040007C007C007C007C007C007C10420000000000000000
      000000000000000000000000000000000000000000001F00000000000000E07F
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000017001042104210421042
      1042104210420000FF7F1042104210421042FF7F000000000000000000000000
      00000000000000000000007C007C007C007C007C104200000000000000000000
      00000000000000000000000000000000000000001F001F000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000017000000000000000000
      0000000000000000FF7FFF7FFF7FFF7FFF7FFF7F000000000000000000000000
      000000000000000000000040007C007C007C007C104200000000000000000000
      0000000000000000000000000000000000001F001F001F000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000017000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000040007C007C007C007C007C104200000000000000000000
      000000000000000000000000000000001F001F001F001F000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000017000000000000000000
      0000000000000000000000001700000000000000000000000000000000000000
      0000000000000040007C007C007C1042007C007C007C10420000000000000000
      0000000000000000000000000000000000001F001F001F000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000017000000000000000000
      0000000000000000000000001700000000000000000000000000000000000000
      000000000040007C007C007C104200000040007C007C007C1042000000000000
      00000000000000000000000000000000000000001F001F000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000017000000000000000000
      0000000000000000000000001700000000001700000000000000000000000000
      000000000040007C007C10420000000000000040007C007C007C104200000000
      000000000000000000000000000000000000000000001F000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000017001700170017001700
      1700170017001700170017001700000017001700170000000000000000000000
      0000000000000040007C000000000000000000000040007C007C007C00000000
      0000104210421042104210421042104200000000000000000000000000000000
      E07F000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000001700FF7F17001700FF7F
      17001700FF7F17001700FF7F1700000000001700000000000000000000000000
      000000000000000000000000000000000000000000000040007C004000000000
      000000000000000000000000000000000000000000000000000000000000E07F
      00000000E07F0000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000017001700170017001700
      1700170017001700170017001700000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000E07F0000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000104210421042104210421042104210421042104210420000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000050000000640000000100010000000000B00400000000000000000000
      000000000000000000000000FFFFFF00FFFFF0000000000000000000FFFFF000
      0000000000000000F00FF0000000000000000000C007F0000000000000000000
      C00030000000000000000000C00010000000000000000000C000100000000000
      00000000C00010000000000000000000C00010000000000000000000C0001000
      0000000000000000C00010000000000000000000C00010000000000000000000
      C00010000000000000000000C00010000000000000000000C000100000000000
      00000000C00010000000000000000000FF0010000000000000000000FF007000
      0000000000000000FFFC70000000000000000000FFFFF0000000000000000000
      FFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFF0000FCFFFFFFFFFFFFFF
      E03F0000F83FFFF7FFC0033FC01F0000F00FFFFFFFC0023F800F0000E003FE13
      FFC0007F00070000C101FFFFFFC000FE00030000E043FE1003C000FE00030000
      C011FFFFFFC000FE00030000E003FEF03FC000FE00030000E003FE703FC000FE
      00030000C001F83FFFC001FE00030000E003FE7007C003FE00030000F806FEF0
      07C003FF00070000FE2C7FFFFFC003FF800F0000FF983E1007C007FFC01F0000
      FFFEFFFFFFC00FFFE03F0000FFFEFFF7FFE01FFFFFFF0000FFFFFFFFFFFFFFFF
      FFFF0000FFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFF0000FFFFFFFF
      FFFFFFFE00010000FFFFFFFFFFE00FFC00010000FFFFFFFFFFE003F800010000
      FFFFFFFFFFC003F800010000FFBFFFFBFFC003F800010000FF3FFFF9FFC003F8
      00010000FE3FFFF8FFC003F800010000FC03FF807FC001F800010000F803FF80
      3FC000F800010000F003FF801FC0007800010000F803FF803FC0003800010000
      FC03FF807FC0003800010000FE3FFFF8FFC0003800010000FF3FFFF9FFC01038
      00010000FFBFFFFBFFC07CF800010000FFFFFFFFFFFFFFF800010000FFFFFFFF
      FFFFFFF800030000FFFFFFFFFFFFFFFC00070000FFFFFFFFFFFFFFFFFFFF0000
      FFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFF
      801F0000FFFFFFFFFFFC07FF801F0000FFFFFC003FF803FF001F0000FFFFFC00
      3FF803FF001F0000FFFFFDFEBFF803FF001F0000FFDFFD243FF803FF001F0000
      FF8FFDFE3FF803FF001F0000FF07FD241FF803FF001F0000FE03FDFE5FF803FF
      001F0000FF8FFC000FF803FF001F0000FF8FFC002FF803FF003F0000FF8FFC00
      07F803FF007F0000FFFFFFFF87F001FD00FF0000FFFFFFFFC3F001FC01FF0000
      FFFFFFFFC3F803FE7FFF0000FFFFFFFFE7FF1FFD3FFF0000FFFFFFFFFFFFFFFF
      FFFF0000FFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFF0007FFFFFF0000FFFFFFFF
      FF0007FFFFFF0000FFFFFF9FFFFFFFFFFFFF0000FFC03F0F9F001FFFFFFF0000
      FFC03F070F001FFFFFFF0000FFC03F0207FFFFBFFFFF0000FFC03F800701FFBF
      F8FF0000FFC03FC00F01FBFFF8FF0000C0003FE01FFFDDFFF8FF0000C0003FF0
      3FFF9FBFE03F0000DFC03FF03FFF1FBFF07F0000D2403FE03FFE1E0FF8FF0000
      DFFBFFC01FFF1FBFFDFF0000D24BFF820FFF9FBFFFFF0000DFFB7F8707FFDFFF
      FFFF0000C0023FCF8701FEFFFFFF0000C0037FFFC701FDBFFFFF0000C003FFFF
      FFFFFFBFFFFF0000FFFFFFFFFF001FFFFFFF0000FFFFFFFFFF001FFFFFFF0000
      00000000000000000000000000000000000000000000}
  end
  object FindDialog1: TFindDialog
    Options = [frDown, frHideWholeWord, frWholeWord]
    Left = 353
    Top = 296
  end
  object JvFormPlacement1: TJvFormPlacement
    IniFileName = '\SOFTWARE\SBP\CAO-Faktura'
    IniSection = 'MAIN'
    MinMaxInfo.MinTrackHeight = 660
    MinMaxInfo.MinTrackWidth = 850
    UseRegistry = True
    Version = 1004
    Left = 287
    Top = 144
  end
  object XPMenu1: TXPMenu
    DimLevel = 30
    GrayLevel = 10
    Font.Charset = ANSI_CHARSET
    Font.Color = clMenuText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    Color = clBtnFace
    DrawMenuBar = False
    IconBackColor = clBtnFace
    MenuBarColor = clBtnFace
    SelectColor = clHighlight
    SelectBorderColor = clHighlight
    SelectFontColor = clMenuText
    DisabledColor = clInactiveCaption
    SeparatorColor = clBtnFace
    CheckedColor = clHighlight
    IconWidth = 24
    DrawSelect = True
    UseSystemColors = True
    UseDimColor = False
    OverrideOwnerDraw = False
    Gradient = True
    FlatMenu = True
    AutoDetect = False
    XPControls = [xcMainMenu, xcPopupMenu, xcCheckBox, xcRadioButton]
    Active = False
    Left = 287
    Top = 240
  end
end
