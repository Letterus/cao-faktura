object TextEditForm: TTextEditForm
  Left = 274
  Top = 181
  Width = 841
  Height = 629
  Caption = 'CAO Texteditor'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Menu = MainMenu1
  OldCreateOrder = True
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnDeactivate = FormDeactivate
  PixelsPerInch = 96
  TextHeight = 13
  object MainPanel: TPanel
    Left = 0
    Top = 0
    Width = 833
    Height = 583
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    OnResize = MainPanelResize
    object ArtPan: TPanel
      Left = 0
      Top = 0
      Width = 833
      Height = 27
      Align = alTop
      BevelOuter = bvNone
      BorderStyle = bsSingle
      Color = clBtnShadow
      TabOrder = 0
      object Label35: TLabel
        Left = 0
        Top = 0
        Width = 130
        Height = 23
        Align = alLeft
        Caption = '  Textverarbeitung '
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        Layout = tlCenter
      end
      object EditBtn: TJvSpeedButton
        Tag = 6
        Left = 668
        Top = 0
        Width = 83
        Height = 22
        Caption = 'Bearbeite&n'
        Flat = True
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        OnClick = EditBtnClick
        HotTrackFont.Charset = DEFAULT_CHARSET
        HotTrackFont.Color = clWindowText
        HotTrackFont.Height = -11
        HotTrackFont.Name = 'MS Sans Serif'
        HotTrackFont.Style = []
        OnMouseEnter = AuswahlBtnMouseEnter
        OnMouseLeave = AuswahlBtnMouseLeave
      end
      object AuswahlBtn: TJvSpeedButton
        Tag = 5
        Left = 558
        Top = 0
        Width = 105
        Height = 22
        Caption = 'Aus&wahl / Liste'
        Flat = True
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        OnClick = AuswahlBtnClick
        HotTrackFont.Charset = DEFAULT_CHARSET
        HotTrackFont.Color = clWindowText
        HotTrackFont.Height = -11
        HotTrackFont.Name = 'MS Sans Serif'
        HotTrackFont.Style = []
        OnMouseEnter = AuswahlBtnMouseEnter
        OnMouseLeave = AuswahlBtnMouseLeave
      end
    end
    object ToolBar1: TToolBar
      Left = 0
      Top = 553
      Width = 833
      Height = 30
      Align = alBottom
      AutoSize = True
      BorderWidth = 1
      ButtonHeight = 26
      ButtonWidth = 94
      DockSite = True
      EdgeBorders = []
      EdgeInner = esNone
      EdgeOuter = esNone
      Flat = True
      Images = MainForm.VorgangImgList
      Indent = 4
      List = True
      ParentShowHint = False
      ShowCaptions = True
      ShowHint = False
      TabOrder = 1
      Wrapable = False
      object DBNavigator1: TDBNavigator
        Left = 4
        Top = 0
        Width = 165
        Height = 26
        DataSource = TextDS
        VisibleButtons = [nbFirst, nbPrior, nbNext, nbLast, nbRefresh]
        Flat = True
        TabOrder = 0
      end
      object ToolButton1: TToolButton
        Left = 169
        Top = 0
        Width = 8
        Caption = 'ToolButton1'
        Style = tbsSeparator
      end
      object NeuBtn: TToolButton
        Left = 177
        Top = 0
        AutoSize = True
        Caption = 'Neu'
        ImageIndex = 7
        OnClick = NeuBtnClick
      end
      object ToolButton8: TToolButton
        Left = 232
        Top = 0
        Width = 8
        Caption = 'ToolButton8'
        ImageIndex = 19
        Style = tbsSeparator
      end
      object SetAddrBtn: TToolButton
        Left = 240
        Top = 0
        AutoSize = True
        Caption = 'Adresse zuw.'
        ImageIndex = 2
        OnClick = SetAddrBtnClick
      end
      object ToolButton3: TToolButton
        Left = 338
        Top = 0
        Width = 8
        Caption = 'ToolButton3'
        ImageIndex = 16
        Style = tbsSeparator
      end
      object SaveBtn: TToolButton
        Left = 346
        Top = 0
        AutoSize = True
        Caption = 'Speichern'
        ImageIndex = 11
        OnClick = SaveBtnClick
      end
      object AbortBtn: TToolButton
        Left = 429
        Top = 0
        AutoSize = True
        Caption = 'Verwerfen'
        ImageIndex = 6
        OnClick = AbortBtnClick
      end
      object ToolButton7: TToolButton
        Left = 512
        Top = 0
        Width = 8
        Caption = 'ToolButton7'
        ImageIndex = 18
        Style = tbsSeparator
      end
      object DelBtn: TToolButton
        Left = 520
        Top = 0
        AutoSize = True
        Caption = 'L�schen'
        ImageIndex = 1
        OnClick = DelBtnClick
      end
      object ToolButton2: TToolButton
        Left = 596
        Top = 0
        Width = 8
        Caption = 'ToolButton2'
        ImageIndex = 7
        Style = tbsSeparator
      end
      object Panel37: TPanel
        Left = 604
        Top = 0
        Width = 112
        Height = 26
        BevelOuter = bvNone
        TabOrder = 1
        object DruckenBtn: TJvArrowButton
          Left = 0
          Top = 0
          Width = 112
          Height = 26
          ArrowWidth = 17
          DropDown = PrintPopUp
          Caption = 'Drucken'
          Flat = True
          FillFont.Charset = DEFAULT_CHARSET
          FillFont.Color = clWindowText
          FillFont.Height = -11
          FillFont.Name = 'MS Sans Serif'
          FillFont.Style = []
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            04000000000000010000130B0000130B00001000000000000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00300000000000
            00033FFFFFFFFFFFFFFF0888888888888880777777777777777F088888888888
            8880777777777777777F0000000000000000FFFFFFFFFFFFFFFF0F8F8F8F8F8F
            8F80777777777777777F08F8F8F8F8F8F9F0777777777777777F0F8F8F8F8F8F
            8F807777777777777F7F0000000000000000777777777777777F3330FFFFFFFF
            03333337F3FFFF3F7F333330F0000F0F03333337F77773737F333330FFFFFFFF
            03333337F3FF3FFF7F333330F00F000003333337F773777773333330FFFF0FF0
            33333337F3FF7F3733333330F08F0F0333333337F7737F7333333330FFFF0033
            33333337FFFF7733333333300000033333333337777773333333}
          NumGlyphs = 2
          Spacing = -1
          OnClick = DruckenBtnClick
        end
      end
    end
    object PC1: TJvPageControl
      Left = 0
      Top = 27
      Width = 833
      Height = 526
      ActivePage = TabSheet2
      Align = alClient
      Style = tsFlatButtons
      TabOrder = 2
      OnChange = PC1Change
      ClientBorderWidth = 0
      object TabSheet1: TTabSheet
        Caption = 'TabSheet1'
        object TextGrid: TCaoDBGrid
          Left = 0
          Top = 0
          Width = 833
          Height = 503
          Align = alClient
          Ctl3D = True
          DataSource = TextDS
          DefaultDrawing = False
          ParentCtl3D = False
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          OnDblClick = EditBtnClick
          TitleButtons = True
          RowColor1 = 12255087
          RowColor2 = clWindow
          ShowTitleEllipsis = True
          DefaultRowHeight = 17
          EditColor = clBlack
          Columns = <
            item
              Expanded = False
              FieldName = 'ID'
              ReadOnly = True
              Title.Caption = 'lfd. Nr.'
              Width = 46
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CalcAdresse'
              ReadOnly = True
              Width = 350
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'BESCHREIBUNG'
              Width = 300
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CHANGE_LAST'
              Width = 70
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CHANGE_USER'
              Width = 100
              Visible = True
            end>
        end
      end
      object TabSheet2: TTabSheet
        Caption = 'TabSheet2'
        ImageIndex = 1
        object WPToolBar2: TWPToolBar
          Left = 0
          Top = 22
          Width = 833
          Height = 20
          ParentColor = False
          AutoSize = True
          UseDockManager = False
          DragKind = dkDock
          KeepGroupsTogether = True
          Align = alTop
          ParentShowHint = False
          ShowHint = True
          BevelLines = []
          AutoEnabling = True
          BevelOuter = bvNone
          WidthBetweenGroups = 10
          FontChoice = fsPrinterFonts
          sel_ListBoxes = [SelFontName, SelFontSize, SelFontColor, SelBackgroundColor]
          sel_StatusIcons = [SelBold, SelItalic, SelUnder, SelStrikeOut, SelSuper, SelSub, SelHidden, SelProtected, SelLeft, SelRight, SelBlock, SelCenter]
          sel_ActionIcons = []
          sel_DatabaseIcons = []
          sel_EditIcons = []
          sel_TableIcons = []
          sel_OutlineIcons = [SelBullets, SelNumbers, SelNextLevel, SelPriorLevel]
          FontSizeFrom = 8
          Hints.Strings = (
            'WPI_FontSelection=Schriftart'
            'WPI_FontSizeSelection=Schriftgrad'
            'WPI_FontColorSelection=Schriftfarbe'
            'WPI_FontBKColorSelection=Hintergrundfarbe'
            'WPI_ParColorSelection=ParColorSelection'
            'WPI_ParStyleSelection=ParStyleSelection'
            'WPI_Normal=Normal'
            'WPI_Bold=Fett'
            'WPI_Italic=Kursiv'
            'WPI_Under=Unterstrichen'
            'WPI_Hyperlink=Hyperlink'
            'WPI_StrikeOut=Durchgestrichen'
            'WPI_Sub=Tiefgestellt'
            'WPI_Super=Hochgestellt'
            'WPI_Hidden=Versteckt'
            'WPI_RTFCode=RTFCode'
            'WPI_Left=Linksb�ndig'
            'WPI_Right=Rechtsb�ndig'
            'WPI_Justified=Blocksatz'
            'WPI_Center=Zentriert'
            'WPI_Copy=Copy'
            'WPI_Cut=Cut'
            'WPI_Paste=Paste'
            'WPI_SelAll=SelAll'
            'WPI_HideSel=HideSel'
            'WPI_Find=Find'
            'WPI_Replace=Replace'
            'WPI_Spell=Spell'
            'WPI_Exit=Exit'
            'WPI_New=New'
            'WPI_Open=Open'
            'WPI_Save=Save'
            'WPI_Close=Close'
            'WPI_Print=Print'
            'WPI_PrintSetup=PrintSetup'
            'WPI_Next=Next'
            'WPI_Prev=Prev'
            'WPI_Add=Add'
            'WPI_Del=Del'
            'WPI_Edit=Edit'
            'WPI_Cancel=Cancel'
            'WPI_ToStart=ToStart'
            'WPI_ToEnd=ToEnd'
            'WPI_Post=Post'
            'WPI_PriorPage=PriorPage'
            'WPI_NextPage=NextPage'
            'WPI_ZoomIn=ZoomIn'
            'WPI_ZoomOut=ZoomOut'
            'WPI_FitWidth=FitWidth'
            'WPI_FitHeight=FitHeight'
            'WPI_Bullets=Aufz�hlungszeichen'
            'WPI_Numbers=Nummerierung'
            'WPI_NextLevel=NextLevel'
            'WPI_PriorLevel=PriorLevel'
            'WPI_CreateTable=CreateTable'
            'WPI_SelRow=SelRow'
            'WPI_SelCol=SelCol'
            'WPI_DelRow=DelRow'
            'WPI_InsRow=InsRow')
          FlatButtons = True
          TrueTypeOnly = False
          NextToolBar = WPToolBar3
        end
        object WPToolBar1: TWPToolBar
          Left = 0
          Top = 0
          Width = 833
          Height = 22
          ParentColor = False
          AutoSize = True
          UseDockManager = False
          DragKind = dkDock
          KeepGroupsTogether = True
          Align = alTop
          ParentShowHint = False
          ShowHint = True
          BevelLines = []
          AutoEnabling = True
          BevelOuter = bvNone
          WidthBetweenGroups = 10
          FontChoice = fsPrinterFonts
          sel_ListBoxes = []
          sel_StatusIcons = []
          sel_ActionIcons = [SelZoomIn, SelZoomOut, SelNextPage, SelPriorPage]
          sel_DatabaseIcons = []
          sel_EditIcons = [SelUndo, SelCopy, SelCut, SelPaste, SelSelAll, SelFind, SelReplace]
          sel_TableIcons = []
          sel_OutlineIcons = []
          FontSizeFrom = 8
          Hints.Strings = (
            'WPI_FontSelection=FontSelection'
            'WPI_FontSizeSelection=FontSizeSelection'
            'WPI_FontColorSelection=FontColorSelection'
            'WPI_FontBKColorSelection=FontBKColorSelection'
            'WPI_ParColorSelection=ParColorSelection'
            'WPI_ParStyleSelection=ParStyleSelection'
            'WPI_Normal=Normal'
            'WPI_Bold=Bold'
            'WPI_Italic=Italic'
            'WPI_Under=Under'
            'WPI_Hyperlink=Hyperlink'
            'WPI_StrikeOut=StrikeOut'
            'WPI_Sub=Sub'
            'WPI_Super=Super'
            'WPI_Hidden=Hidden'
            'WPI_RTFCode=RTFCode'
            'WPI_Left=Left'
            'WPI_Right=Right'
            'WPI_Justified=Justified'
            'WPI_Center=Center'
            'WPI_Copy=Kopieren'
            'WPI_Cut=Ausschneiden'
            'WPI_Paste=Einf�gen'
            'WPI_SelAll=Alles markieren'
            'WPI_HideSel=Auswahl verstecken'
            'WPI_Find=Suchen'
            'WPI_Replace=Ersetzen'
            'WPI_Spell=Spell'
            'WPI_Exit=Exit'
            'WPI_New=Neu'
            'WPI_Open=�ffnen'
            'WPI_Save=Speichern'
            'WPI_Close=Schlie�en'
            'WPI_Print=Drucken'
            'WPI_PrintSetup=Drucker einrichten'
            'WPI_Next=Next'
            'WPI_Prev=Prev'
            'WPI_Add=Add'
            'WPI_Del=Del'
            'WPI_Edit=Edit'
            'WPI_Cancel=Cancel'
            'WPI_ToStart=ToStart'
            'WPI_ToEnd=ToEnd'
            'WPI_Post=Post'
            'WPI_PriorPage=vorherige Seite'
            'WPI_NextPage=n�chste Seite'
            'WPI_ZoomIn=ZoomIn'
            'WPI_ZoomOut=ZoomOut'
            'WPI_FitWidth=Zoom an Fenstergr��e anpassen'
            'WPI_FitHeight=ganze Seite'
            'WPI_Bullets=Bullets'
            'WPI_Numbers=Numbers'
            'WPI_NextLevel=NextLevel'
            'WPI_PriorLevel=PriorLevel'
            'WPI_CreateTable=CreateTable'
            'WPI_SelRow=SelRow'
            'WPI_SelCol=SelCol'
            'WPI_DelRow=DelRow'
            'WPI_InsRow=InsRow')
          FlatButtons = True
          TrueTypeOnly = False
          NextToolBar = WPToolBar2
          object WPValueEdit1: TWPValueEdit
            Left = 255
            Top = 0
            Width = 58
            Height = 22
            Hint = 'Zoom'
            AvailableUnits = []
            UnitType = euPercent
            TabOrder = 0
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            OnChange = WPValueEdit1Change
            AllowNegative = False
            Value = 100
          end
        end
        object WPToolBar3: TWPToolBar
          Left = 0
          Top = 42
          Width = 833
          Height = 20
          ParentColor = False
          AutoSize = True
          UseDockManager = False
          DragKind = dkDock
          KeepGroupsTogether = True
          Align = alTop
          ParentShowHint = False
          ShowHint = True
          Visible = False
          BevelLines = []
          AutoEnabling = True
          BevelOuter = bvNone
          WidthBetweenGroups = 10
          FontChoice = fsPrinterFonts
          sel_ListBoxes = []
          sel_StatusIcons = []
          sel_ActionIcons = []
          sel_DatabaseIcons = []
          sel_EditIcons = []
          sel_TableIcons = [SelCreateTable, SelSelRow, SelInsRow, SelDelRow, SelSelCol, SelSplitCell, SelCombineCell, SelBAllOff, SelBAllOn, SelBInner, SelBOuter, SelBLeft, SelBRight, SelBTop, SelBBottom]
          sel_OutlineIcons = []
          FontSizeFrom = 8
          Hints.Strings = (
            'WPI_FontSelection=FontSelection'
            'WPI_FontSizeSelection=FontSizeSelection'
            'WPI_FontColorSelection=FontColorSelection'
            'WPI_FontBKColorSelection=FontBKColorSelection'
            'WPI_ParColorSelection=ParColorSelection'
            'WPI_ParStyleSelection=ParStyleSelection'
            'WPI_Normal=Normal'
            'WPI_Bold=Bold'
            'WPI_Italic=Italic'
            'WPI_Under=Under'
            'WPI_Hyperlink=Hyperlink'
            'WPI_StrikeOut=StrikeOut'
            'WPI_Sub=Sub'
            'WPI_Super=Super'
            'WPI_Hidden=Hidden'
            'WPI_RTFCode=RTFCode'
            'WPI_Left=Linksb�ndig'
            'WPI_Right=Rechstb�ndig'
            'WPI_Justified=Blocksatz'
            'WPI_Center=Zentriert'
            'WPI_Copy=Kopieren'
            'WPI_Cut=Ausschneiden'
            'WPI_Paste=Einf�gen'
            'WPI_SelAll=Alles markieren'
            'WPI_HideSel=markierung verstecken'
            'WPI_Find=Suchen'
            'WPI_Replace=Ersetzen'
            'WPI_Spell=Spell'
            'WPI_Exit=Exit'
            'WPI_New=New'
            'WPI_Open=Open'
            'WPI_Save=Save'
            'WPI_Close=Close'
            'WPI_Print=Print'
            'WPI_PrintSetup=PrintSetup'
            'WPI_Next=Next'
            'WPI_Prev=Prev'
            'WPI_Add=Add'
            'WPI_Del=Del'
            'WPI_Edit=Edit'
            'WPI_Cancel=Cancel'
            'WPI_ToStart=ToStart'
            'WPI_ToEnd=ToEnd'
            'WPI_Post=Post'
            'WPI_PriorPage=vorherige Seite'
            'WPI_NextPage=n�chste Seite'
            'WPI_ZoomIn=Zoom (+)'
            'WPI_ZoomOut=Zoom (-)'
            'WPI_FitWidth=FitWidth'
            'WPI_FitHeight=FitHeight'
            'WPI_Bullets=Bullets'
            'WPI_Numbers=Numbers'
            'WPI_NextLevel=NextLevel'
            'WPI_PriorLevel=PriorLevel'
            'WPI_CreateTable=Tabelle erstellen'
            'WPI_SelRow=Zeile ausw�hlen'
            'WPI_SelCol=Spalte ausw�hlen'
            'WPI_DelRow=Zeile l�schen'
            'WPI_InsRow=Zeile einf�gen')
          FlatButtons = True
          TrueTypeOnly = False
        end
        object WPRuler1: TWPRuler
          Left = 0
          Top = 62
          Width = 833
          Height = 25
          ColorRuler = clBtnShadow
          ColorTextArea = clWindow
          ColorLabel = clBlack
          ColorTableMarker = clAqua
          DrawOptions = [wrShowNumbers, wrShowLines]
          ColorMarker = clBtnFace
          ColorTabStop = clBlack
          MarkerWidth = 9
          InnerBorder = 7
          Options = [wrShowTabSelector, wrShowTabStops, wpUseIntervalls, wpUpdateAtOnce, wpDrawIndentLine, wpKeepMarksVisible]
          Units = wrCentimeter
          XOffset = 36
          Align = alTop
          BevelOuter = bvNone
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = False
        end
        object WPVertRuler1: TWPVertRuler
          Left = 0
          Top = 87
          Width = 20
          Height = 416
          ColorRuler = clBtnShadow
          ColorTextArea = clWindow
          ColorLabel = clBlack
          InnerBorder = 6
          DrawOptions = [wrShowNumbers, wrShowLines]
          Units = wrCentimeter
          Align = alLeft
          BevelOuter = bvNone
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
        end
        object WPRichText1: TDBWPRichText
          Left = 20
          Top = 87
          Width = 813
          Height = 416
          Cursor = crIBeam
          AutoDisplay = True
          DataField = 'LANG_TEXT'
          NoUpdateOnExit = False
          NoReloadOnEdit = True
          AcceptFiles = True
          EditBoxModes = [wpemLimitTextWidth]
          PrintParameter.NumberOfCopies = 1
          PrintParameter.PrintTitle = 'Wache 3.0 Wachbericht'
          PrintParameter.PageMirror = False
          PrintParameter.PageRange = wprAllPages
          PrintParameter.PageSides = wpsAll
          PrintParameter.PrintHeaderFooter = wprNever
          PrintParameter.PrintHeaderFooterOnLastPage = [wprHeader, wprFooter]
          PrintParameter.PrintOptions = [wpDoNotChangePrinterDefaults]
          WYSIWYG = True
          EditStateEvent = WPRichText1EditStateEvent
          OnChangeModified = WPRichText1ChangeModified
          OnGetPageGapText = WPRichText1GetPageGapText
          Header.StoreOptions = []
          Header.HTMLStoreOptions = []
          Header.LoadOptions = [soIgnoreWordwrapInformation]
          Header.UsedUnit = UnitIn
          Header.RoundTabs = True
          Header.RoundTabsDivForInch = 90
          Header.RoundTabsDivForCm = 144
          Header.DecimalTabChar = #0
          Header.PrintHeaderFooterInPageMargins = True
          Header.DefaultTabstop = 2440
          Header.DefaultPageWidth = 11906
          Header.DefaultPageHeight = 16781
          Header.DefaultTopMargin = 760
          Header.DefaultBottomMargin = 760
          Header.DefaultLeftMargin = 760
          Header.DefaultRightMargin = 1880
          Header.DefaultMarginHeader = 0
          Header.DefaultMarginFooter = 0
          Header.DefaultLandscape = False
          Header.PageWidth = 11906
          Header.PageHeight = 16838
          Header.LeftMargin = 760
          Header.RightMargin = 1880
          Header.TopMargin = 760
          Header.BottomMargin = 760
          Header.MarginHeader = 254
          Header.MarginFooter = 254
          Header.Landscape = False
          Header.DefaultPageSize = wp_Custom
          Header.PageSize = wp_DinA4
          WPToolBar = WPToolBar1
          WPRuler = WPRuler1
          WPVertRuler = WPVertRuler1
          DefaultFont.Color = clBlack
          DefaultFont.BKColor = clNone
          DefaultFont.Name = 'Arial'
          DefaultFont.Size = 11
          DefaultFont.Style = []
          DefaultFont.UseBKColor = False
          DragAndDropSupport = True
          OnSetGaugeValue = WPRichText1SetGaugeValue
          AfterCompleteWordEvent = WPRichText1AfterCompleteWordEvent
          TextObjectCursor = crArrow
          HyperLinkCursor = crHandPoint
          NoBlockMarking = False
          Inserting = True
          OneClickHyperlink = False
          ScreenResMode = rmNormal
          WordWrap = True
          TextSaveFormat = 'AUTO'
          TextLoadFormat = 'AUTO'
          HighLightColor = clHighlight
          HighLightTextColor = clHighlightText
          AutoZoom = wpAutoZoomOff
          Zooming = 100
          Resizing = 100
          ProtectedProp = [ppProtected, ppParProtected, ppNoEditAfterProtection, ppInsertBetweenProtectedPar]
          LayoutMode = wplayNormal
          WriteBitmapMode = wbmDIBBitmap
          WriteObjectMode = wobDependingOnObject
          OnUNDOKindChanged = DestTextUNDOKindChanged
          OnMailMergeGetText = WPRichText1MailMergeGetText
          OnXPosChanged = WPRichText1XPosChanged
          OnYPosChanged = WPRichText1XPosChanged
          OnTextObjectMouseDown = DestTextTextObjectMouseDown
          OnTextObjectCheckProperties = DestTextTextObjectCheckProperties
          OnTextObjectDestroying = DestTextTextObjectDestroying
          HTMLStoreComment = shcOFF
          HTMLCreateImages = wpciCreateImages
          ViewOptions = [wpUseDoubleBuffer, wpDrawFineUnderlines]
          ClipboardOptions = []
          EditOptions = [wpTableResizing, wpTableOneCellResizing, wpTableColumnResizing, wpObjectMoving, wpObjectResizingWidth, wpObjectResizingHeight, wpObjectResizingKeepRatio, wpObjectSelecting, wpObjectDeletion, wpSpreadsheetCursorMovement, wpActivateUndo, wpActivateUndoHotkey, wpActivateRedo, wpMoveCPOnPageUpDown]
          BackgroundScrolling = True
          Transparent = False
          BackgroundLoadFromFile = False
          InsertPointAttr.Bold = tsIgnore
          InsertPointAttr.Italic = tsIgnore
          InsertPointAttr.DoubleUnderline = False
          InsertPointAttr.Underline = tsIgnore
          InsertPointAttr.StrikeOut = tsIgnore
          InsertPointAttr.SuperScript = tsIgnore
          InsertPointAttr.SubScript = tsIgnore
          InsertPointAttr.Hidden = False
          InsertPointAttr.UnderlineColor = clBlack
          InsertPointAttr.TextColor = clRed
          InsertPointAttr.BackgroundColor = clBlack
          InsertPointAttr.UseUnderlineColor = False
          InsertPointAttr.UseTextColor = True
          InsertPointAttr.UseBackgroundColor = False
          AutomaticTextAttr.Bold = tsIgnore
          AutomaticTextAttr.Italic = tsIgnore
          AutomaticTextAttr.DoubleUnderline = False
          AutomaticTextAttr.Underline = tsTRUE
          AutomaticTextAttr.StrikeOut = tsIgnore
          AutomaticTextAttr.SuperScript = tsIgnore
          AutomaticTextAttr.SubScript = tsIgnore
          AutomaticTextAttr.Hidden = False
          AutomaticTextAttr.UnderlineColor = 16744576
          AutomaticTextAttr.TextColor = clBlack
          AutomaticTextAttr.BackgroundColor = clGray
          AutomaticTextAttr.UseUnderlineColor = True
          AutomaticTextAttr.UseTextColor = False
          AutomaticTextAttr.UseBackgroundColor = False
          HyperlinkTextAttr.Bold = tsIgnore
          HyperlinkTextAttr.Italic = tsIgnore
          HyperlinkTextAttr.DoubleUnderline = False
          HyperlinkTextAttr.Underline = tsTRUE
          HyperlinkTextAttr.StrikeOut = tsIgnore
          HyperlinkTextAttr.SuperScript = tsIgnore
          HyperlinkTextAttr.SubScript = tsIgnore
          HyperlinkTextAttr.Hidden = False
          HyperlinkTextAttr.UnderlineColor = clBlue
          HyperlinkTextAttr.TextColor = clGreen
          HyperlinkTextAttr.BackgroundColor = clBlack
          HyperlinkTextAttr.UseUnderlineColor = True
          HyperlinkTextAttr.UseTextColor = True
          HyperlinkTextAttr.UseBackgroundColor = False
          ProtectedTextAttr.Bold = tsIgnore
          ProtectedTextAttr.Italic = tsIgnore
          ProtectedTextAttr.DoubleUnderline = False
          ProtectedTextAttr.Underline = tsIgnore
          ProtectedTextAttr.StrikeOut = tsIgnore
          ProtectedTextAttr.SuperScript = tsIgnore
          ProtectedTextAttr.SubScript = tsIgnore
          ProtectedTextAttr.Hidden = False
          ProtectedTextAttr.UnderlineColor = clBlack
          ProtectedTextAttr.TextColor = clBlack
          ProtectedTextAttr.BackgroundColor = clSilver
          ProtectedTextAttr.UseUnderlineColor = False
          ProtectedTextAttr.UseTextColor = False
          ProtectedTextAttr.UseBackgroundColor = True
          BookmarkTextAttr.Bold = tsIgnore
          BookmarkTextAttr.Italic = tsFALSE
          BookmarkTextAttr.DoubleUnderline = False
          BookmarkTextAttr.Underline = tsIgnore
          BookmarkTextAttr.StrikeOut = tsIgnore
          BookmarkTextAttr.SuperScript = tsIgnore
          BookmarkTextAttr.SubScript = tsIgnore
          BookmarkTextAttr.Hidden = False
          BookmarkTextAttr.UnderlineColor = clGreen
          BookmarkTextAttr.TextColor = clBlack
          BookmarkTextAttr.BackgroundColor = clPurple
          BookmarkTextAttr.UseUnderlineColor = False
          BookmarkTextAttr.UseTextColor = False
          BookmarkTextAttr.UseBackgroundColor = True
          FieldObjectTextAttr.Bold = tsIgnore
          FieldObjectTextAttr.Italic = tsIgnore
          FieldObjectTextAttr.DoubleUnderline = False
          FieldObjectTextAttr.Underline = tsIgnore
          FieldObjectTextAttr.StrikeOut = tsIgnore
          FieldObjectTextAttr.SuperScript = tsIgnore
          FieldObjectTextAttr.SubScript = tsIgnore
          FieldObjectTextAttr.Hidden = False
          FieldObjectTextAttr.UnderlineColor = clBlack
          FieldObjectTextAttr.TextColor = clBlack
          FieldObjectTextAttr.BackgroundColor = clBlack
          FieldObjectTextAttr.UseUnderlineColor = False
          FieldObjectTextAttr.UseTextColor = False
          FieldObjectTextAttr.UseBackgroundColor = False
          HiddenTextAttr.Bold = tsIgnore
          HiddenTextAttr.Italic = tsIgnore
          HiddenTextAttr.DoubleUnderline = False
          HiddenTextAttr.Underline = tsIgnore
          HiddenTextAttr.StrikeOut = tsIgnore
          HiddenTextAttr.SuperScript = tsIgnore
          HiddenTextAttr.SubScript = tsIgnore
          HiddenTextAttr.Hidden = True
          HiddenTextAttr.UnderlineColor = clBlack
          HiddenTextAttr.TextColor = clBlack
          HiddenTextAttr.BackgroundColor = clGreen
          HiddenTextAttr.UseUnderlineColor = False
          HiddenTextAttr.UseTextColor = False
          HiddenTextAttr.UseBackgroundColor = True
          XOffset = 16
          YOffset = 141
          Align = alClient
          BorderStyle = bsNone
          Color = clWhite
          ColorDesktop = clBtnShadow
          Ctl3D = True
          ParentColor = False
          ParentCtl3D = False
          ScrollBars = ssBoth
          TabOrder = 5
          TabStop = True
          WantTabs = True
          OnChangeZooming = WPRichText1ChangeZooming
          OnKeyPress = DestTextKeyPress
        end
      end
    end
  end
  object WPParagraphPropDlg1: TWPParagraphPropDlg
    EditBox = WPRichText1
    AutoUpdate = False
    Left = 69
    Top = 209
  end
  object OpenDialog1: TOpenDialog
    Filter = 'Graphics|*.BMP;*.JPG;*.WMF'
    Left = 301
    Top = 160
  end
  object WPParagraphBorderDlg1: TWPParagraphBorderDlg
    AutoUpdate = False
    Left = 183
    Top = 257
  end
  object SaveDialog1: TSaveDialog
    Left = 300
    Top = 256
  end
  object WPTabDlg1: TWPTabDlg
    Left = 64
    Top = 257
  end
  object WPSymbolDlg1: TWPSymbolDlg
    UseOKButton = False
    Left = 62
    Top = 305
  end
  object WPTableDlg1: TWPTableDlg
    EditBox = WPRichText1
    Left = 69
    Top = 161
  end
  object WPBulletDlg1: TWPBulletDlg
    Left = 183
    Top = 161
  end
  object FontDialog1: TFontDialog
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    MinFontSize = 0
    MaxFontSize = 0
    Left = 299
    Top = 304
  end
  object GraphicPopup: TPopupMenu
    Left = 405
    Top = 256
    object FixedPopup: TMenuItem
      Caption = 'Position'
      object asCharacter1: TMenuItem
        Caption = 'mit Text in der Zeile'
        OnClick = asCharacter1Click
      end
      object Auto1: TMenuItem
        Caption = 'verschiebbare Grafik - Automatischer Umbruch'
        OnClick = Auto1Click
      end
      object MovableGraphic1: TMenuItem
        Caption = 'linksb�ndig'
        OnClick = MovableGraphic1Click
      end
      object MovableGraphicWrapLeft1: TMenuItem
        Caption = 'rechtsb�ndig'
        OnClick = MovableGraphicWrapLeft1Click
      end
    end
    object Convert1: TMenuItem
      Caption = 'Konvertieren'
      object ConvertintoBitmap1: TMenuItem
        Caption = 'in Bitmap'
        OnClick = ConvertintoBitmap1Click
      end
      object ConvertToMetafile1: TMenuItem
        Caption = 'in Metafile'
        OnClick = ConvertToMetafile1Click
      end
    end
    object Transparent1: TMenuItem
      Caption = 'Transparent-Modus umschalten'
      Visible = False
      OnClick = Transparent1Click
    end
  end
  object OpenPictureDialog1: TOpenPictureDialog
    Filter = 
      'Alle Dateien (*.jpg;*.jpeg;*.bmp;*.ico;*.emf;*.wmf)|*.jpg;*.jpeg' +
      ';*.bmp;*.ico;*.emf;*.wmf|JPEG Bilddatei (*.jpg)|*.jpg|JPEG Bildd' +
      'atei (*.jpeg)|*.jpeg|Bitmaps (*.bmp)|*.bmp|Symbole (*.ico)|*.ico' +
      '|Erweiterte Metadateien (*.emf)|*.emf|Metadateien (*.wmf)|*.wmf'
    Left = 299
    Top = 208
  end
  object WPToolsEnviroment1: TWPToolsEnviroment
    IgnoreObjStreamNames = False
    Left = 184
    Top = 209
  end
  object TextDS: TDataSource
    DataSet = TextTab
    OnStateChange = TextDSStateChange
    Left = 132
    Top = 378
  end
  object TextTab: TZMySqlTable
    Database = DM1.db1
    Transaction = DM1.Transact1
    CachedUpdates = False
    ShowRecordTypes = [ztModified, ztInserted, ztUnmodified]
    Options = [doHourGlass, doQueryAllRecords, doAutoFillDefs, doUseRowId]
    LinkOptions = [loAlwaysResync]
    Constraints = <>
    AfterOpen = TextTabAfterOpen
    AfterInsert = TextTabAfterOpen
    BeforePost = TextTabBeforePost
    AfterPost = TextTabAfterOpen
    AfterDelete = TextTabAfterOpen
    AfterScroll = TextTabAfterOpen
    OnCalcFields = TextTabCalcFields
    ExtraOptions = [moStoreResult]
    TableName = 'SCHRIFTVERKEHR'
    Left = 60
    Top = 378
    object TextTabID: TIntegerField
      DisplayWidth = 12
      FieldName = 'ID'
    end
    object TextTabADDR_ID: TIntegerField
      DisplayWidth = 12
      FieldName = 'ADDR_ID'
    end
    object TextTabBESCHREIBUNG: TStringField
      DisplayLabel = 'Beschreibung / Betreff'
      DisplayWidth = 122
      FieldName = 'BESCHREIBUNG'
      Size = 250
    end
    object TextTabLANG_TEXT: TMemoField
      DisplayWidth = 13
      FieldName = 'LANG_TEXT'
      BlobType = ftMemo
    end
    object TextTabCalcAdresse: TStringField
      DisplayLabel = 'Adresse'
      FieldKind = fkCalculated
      FieldName = 'CalcAdresse'
      Size = 255
      Calculated = True
    end
    object TextTabCHANGE_USER: TStringField
      DisplayLabel = 'ge�ndert von'
      FieldName = 'CHANGE_USER'
      Size = 100
    end
    object TextTabCHANGE_LAST: TDateTimeField
      DisplayLabel = 'le. �nderung'
      FieldName = 'CHANGE_LAST'
      DisplayFormat = 'dd.mm.yyyy'
    end
  end
  object MainMenu1: TMainMenu
    Images = MainForm.ImageList1
    OwnerDraw = True
    Left = 402
    Top = 160
    object Edit1: TMenuItem
      Caption = '&Bearbeiten'
      GroupIndex = 2
      object Undo1: TMenuItem
        Caption = '&R�ckg�ngig'
        ImageIndex = 55
        ShortCut = 16474
        OnClick = Undo1Click
      end
      object N2: TMenuItem
        Caption = '-'
      end
      object Cut1: TMenuItem
        Caption = 'A&usschneiden'
        ImageIndex = 59
        ShortCut = 16472
        OnClick = Cut1Click
      end
      object Copy1: TMenuItem
        Caption = '&Kopieren'
        ImageIndex = 14
        ShortCut = 16451
        OnClick = Copy1Click
      end
      object Paste1: TMenuItem
        Caption = 'E&inf�gen'
        ImageIndex = 53
        ShortCut = 16470
        OnClick = Paste1Click
      end
      object Delete1: TMenuItem
        Caption = 'markierten Text &l�schen'
        ImageIndex = 73
        OnClick = Delete1Click
      end
      object SelectAll1: TMenuItem
        Caption = 'Alles markieren'
        ImageIndex = 71
        ShortCut = 16449
        OnClick = SelectAll1Click
      end
      object HideSelection1: TMenuItem
        Caption = 'Markierung l�s&chen'
        OnClick = HideSelection1Click
      end
      object N9: TMenuItem
        Caption = '-'
      end
      object Suchen1: TMenuItem
        Caption = '&Suchen'
        ImageIndex = 10
        ShortCut = 16454
        OnClick = Suchen1Click
      end
      object Ersetzen1: TMenuItem
        Caption = '&Ersetzen'
        ImageIndex = 70
        ShortCut = 16456
        OnClick = Ersetzen1Click
      end
      object N1: TMenuItem
        Caption = '-'
      end
      object SeitenAnsicht1: TMenuItem
        Caption = 'Seitenansicht'
        ImageIndex = 76
        ShortCut = 16467
        OnClick = SeitenAnsicht1Click
      end
      object Drucken1: TMenuItem
        Caption = 'Drucken'
        ImageIndex = 8
        ShortCut = 16464
        OnClick = DruckenBtnClick
      end
    end
    object View1: TMenuItem
      Caption = '&Ansicht'
      GroupIndex = 3
      object WordWrap1: TMenuItem
        Caption = 'automatischer &Zeilenumbruch'
        GroupIndex = 33
        ImageIndex = 72
        OnClick = WordWrap1Click
      end
      object N7: TMenuItem
        Caption = '-'
        GroupIndex = 33
      end
      object Controlzeichenzeigen1: TMenuItem
        Caption = 'Controlzeichen &zeigen'
        GroupIndex = 34
        ImageIndex = 52
        OnClick = Controlzeichenzeigen1Click
      end
      object Special1: TMenuItem
        Caption = 'Spezial'
        GroupIndex = 35
        Visible = False
        object WindowObjectsasObjects1: TMenuItem
          Caption = #39'Live'#39' window objects'
          Checked = True
          OnClick = WindowObjectsasObjects1Click
        end
        object ParagraphSymbols1: TMenuItem
          Caption = 'Paragraph Symbols'
        end
        object AbsoluteFontheight1: TMenuItem
          Caption = 'Absolute Fontheight'
          OnClick = AbsoluteFontheight1Click
        end
        object Gridlines1: TMenuItem
          Caption = 'Gridlines'
          OnClick = Gridlines1Click
        end
        object DoubleBuffer1: TMenuItem
          Caption = 'Double Buffer'
          OnClick = DoubleBuffer1Click
        end
      end
    end
    object Insert1: TMenuItem
      Caption = '&Einf�gen'
      GroupIndex = 4
      object Symbol1: TMenuItem
        Caption = 'Symb&ol...'
        ImageIndex = 18
        OnClick = Symbol1Click
      end
      object InsertFile1: TMenuItem
        Caption = '&Datei'
        ImageIndex = 29
        OnClick = InsertFile1Click
      end
      object Object1: TMenuItem
        Caption = 'O&bjekt aus Datei'
        ImageIndex = 66
        OnClick = Object1Click
      end
      object Graphicfile1: TMenuItem
        Caption = 'Grafik durch &Link'
        ImageIndex = 67
        OnClick = Graphicfile1Click
      end
      object Graphicfile2: TMenuItem
        Caption = '&Grafik aus Datei (eingebettet)'
        ImageIndex = 68
        OnClick = Graphicfile2Click
      end
      object Table2: TMenuItem
        Caption = '&Tabelle'
        ImageIndex = 5
        Visible = False
        OnClick = Table2Click
      end
      object Number1: TMenuItem
        Caption = '&Seitenzahlen'
        ImageIndex = 51
        object Page1: TMenuItem
          Caption = 'Seite'
          OnClick = PageCount1Click
        end
        object NextPage1: TMenuItem
          Tag = 1
          Caption = 'n�chste Seite'
          OnClick = PageCount1Click
        end
        object PriorPage1: TMenuItem
          Tag = 2
          Caption = 'vorherige Seite'
          OnClick = PageCount1Click
        end
        object PageCount1: TMenuItem
          Tag = 3
          Caption = '&Anzahl Seiten'
          OnClick = PageCount1Click
        end
      end
    end
    object Format1: TMenuItem
      Caption = '&Format'
      GroupIndex = 5
      object Font1: TMenuItem
        Caption = '&Zeichen'
        ImageIndex = 74
        OnClick = Font1Click
      end
      object Paragraph1: TMenuItem
        Caption = '&Absatz'
        ImageIndex = 75
        OnClick = Paragraph1Click
      end
      object Bullets1: TMenuItem
        Caption = '&Nummerierung und Aufz�hlungszeichen'
        ImageIndex = 57
        OnClick = Bullets1Click
      end
      object Borders1: TMenuItem
        Caption = '&Rahmen und Schattierungen'
        OnClick = Borders1Click
      end
      object N10: TMenuItem
        Caption = '-'
      end
      object Tabstops1: TMenuItem
        Caption = 'Tabstops...'
        ImageIndex = 56
        OnClick = Tabstops1Click
      end
      object N3: TMenuItem
        Caption = '-'
      end
      object StandardSchriftart1: TMenuItem
        Caption = 'Standard-Schriftart'
        ImageIndex = 74
        OnClick = StandardSchriftart1Click
      end
    end
  end
  object PrintPopUp: TPopupMenu
    Images = MainForm.ImageList1
    Left = 404
    Top = 206
    object Seitenansicht2: TMenuItem
      Caption = 'Seitenansicht'
      ImageIndex = 76
      ShortCut = 16467
      OnClick = SeitenAnsicht1Click
    end
  end
end
