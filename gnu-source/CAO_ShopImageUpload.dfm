object FileUpload: TFileUpload
  Left = 459
  Top = 288
  BorderStyle = bsDialog
  Caption = 'FileUpload'
  ClientHeight = 98
  ClientWidth = 331
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnCloseQuery = FormCloseQuery
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 11
    Top = 40
    Width = 305
    Height = 13
    Alignment = taCenter
    AutoSize = False
  end
  object JvFilenameEdit1: TJvFilenameEdit
    Left = 8
    Top = 8
    Width = 313
    Height = 21
    DialogKind = dkOpenPicture
    Filter = 'Bilder|*.jpg;*.gif|All files (*.*)|*.*'
    DialogTitle = 'Bild ausw�hlen'
    ButtonFlat = False
    NumGlyphs = 1
    TabOrder = 0
    OnChange = JvFilenameEdit1Change
  end
  object UploadBtn: TBitBtn
    Left = 80
    Top = 64
    Width = 169
    Height = 25
    Caption = 'Upload'
    TabOrder = 1
    OnClick = UploadBtnClick
  end
  object IdHTTP1: TIdHTTP
    MaxLineAction = maException
    AllowCookies = True
    ProxyParams.BasicAuthentication = False
    ProxyParams.ProxyPort = 0
    Request.ContentLength = 0
    Request.ContentRangeEnd = 0
    Request.ContentRangeStart = 0
    Request.ContentType = 'text/html'
    Request.Accept = 'text/html, */*'
    Request.BasicAuthentication = False
    Request.Password = 'test'
    Request.Referer = 'CAO-FAKTURA'
    Request.UserAgent = 'Mozilla/3.0 (compatible; Indy Library)'
    Request.Username = 'jan'
    HTTPOptions = [hoForceEncodeParams]
    Left = 24
    Top = 48
  end
end
