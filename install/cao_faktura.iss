; Script generated by the Inno Setup Script Wizard.
; SEE THE DOCUMENTATION FOR DETAILS ON CREATING INNO SETUP SCRIPT FILES!

[Setup]
AppName=freie Warenwirtschaftssoftware CAO-Faktura
AppVerName=CAO-Faktura 1.2 RC3
AppPublisher=JP-Soft
AppPublisherURL=http://www.jp-soft.de/cao/default.htm
AppSupportURL=http://www.jp-soft.de/board/index.php
AppUpdatesURL=http://www.jp-soft.de/cao/cao_download.htm
DefaultDirName={pf}\CAO-Faktura
DefaultGroupName=CAO-FAKTURA
LicenseFile=P:\CAO.MYSQL\bin\fw_license.txt
InfoAfterFile=P:\CAO.MYSQL\readme.txt
OutputDir=P:\CAO.MYSQL\cao_cvs\cao\install\setup
Compression=zip/9
MinVersion=0,4.0.1381sp5
OnlyBelowVersion=0,5.1.2600sp1
AppCopyright=2003 JP-Soft JP
AppVersion=1.2 RC2
AppID={2121BEF3-F102-4B7F-B5CF-A5299DAADA25}
OutputBaseFilename=cao_1.2_RC3_setup
SolidCompression=true
AlwaysShowDirOnReadyPage=true
AlwaysShowGroupOnReadyPage=true
ShowLanguageDialog=yes

[Messages]
SelectLanguageLabel=Sprache
SelectLanguageTitle=Sparache w�hlen
UninstallOpenError=Uninstaller Fehler

[_ISTool]
EnableISX=false
Use7zip=false

[LangOptions]
LanguageName=German
LanguageID=$0407

[Files]
Source: P:\CAO.MYSQL\bin\CAO32_DB.exe; DestDir: {app}; Flags: promptifolder
Source: P:\CAO.MYSQL\bin\cao_kasse.exe; DestDir: {app}; Flags: promptifolder
Source: P:\CAO.MYSQL\bin\changes.txt; DestDir: {app}; Flags: promptifolder
Source: P:\CAO.MYSQL\bin\formulare.cao; DestDir: {app}; Flags: overwritereadonly promptifolder
Source: P:\CAO.MYSQL\bin\libmySQL.dll; DestDir: {app}; Flags: promptifolder
Source: P:\CAO.MYSQL\bin\unzdll.dll; DestDir: {app}; Flags: promptifolder
Source: P:\CAO.MYSQL\bin\zipdll.dll; DestDir: {app}; Flags: promptifolder
Source: P:\CAO.MYSQL\cao_cvs\cao\bin\CAO32_DB.cfg; DestDir: {app}; Flags: onlyifdoesntexist
Source: P:\CAO.MYSQL\cao_cvs\cao\bin\cao_update.exe; DestDir: {app}; Flags: promptifolder
Source: P:\CAO.MYSQL\cao_cvs\cao\bin\readme.txt; DestDir: {app}
Source: P:\CAO.MYSQL\cao_cvs\cao\bin\fw_license.txt; DestDir: {app}
Source: P:\CAO.MYSQL\bin\winsys\rbDADE.eng; DestDir: {sys}; Flags: comparetimestamp promptifolder
Source: P:\CAO.MYSQL\bin\winsys\rbDADE.deu; DestDir: {sys}; Flags: comparetimestamp promptifolder
Source: P:\CAO.MYSQL\bin\winsys\rbide.eng; DestDir: {sys}; Flags: comparetimestamp promptifolder
Source: P:\CAO.MYSQL\bin\winsys\rbide.deu; DestDir: {sys}; Flags: comparetimestamp promptifolder
Source: P:\CAO.MYSQL\bin\winsys\rbide.cst; DestDir: {sys}; Flags: comparetimestamp promptifolder
Source: P:\CAO.MYSQL\bin\winsys\rbprint.cst; DestDir: {sys}; Flags: comparetimestamp promptifolder
Source: P:\CAO.MYSQL\bin\winsys\rbprint.deu; DestDir: {sys}; Flags: comparetimestamp promptifolder
Source: P:\CAO.MYSQL\bin\winsys\rbprint.eng; DestDir: {sys}; Flags: comparetimestamp promptifolder
Source: P:\CAO.MYSQL\bin\winsys\rbRAP.cst; DestDir: {sys}; Flags: comparetimestamp promptifolder
Source: P:\CAO.MYSQL\bin\winsys\rbRAP.deu; DestDir: {sys}; Flags: comparetimestamp promptifolder
Source: P:\CAO.MYSQL\bin\winsys\rbRAP.eng; DestDir: {sys}; Flags: comparetimestamp promptifolder
Source: P:\CAO.MYSQL\bin\land.cao; DestDir: {app}; Flags: comparetimestamp
Source: P:\CAO.MYSQL\bin\blz.cao; DestDir: {app}; Flags: comparetimestamp
Source: P:\CAO.MYSQL\bin\plz.cao; DestDir: {app}; Flags: comparetimestamp
Source: P:\CAO.MYSQL\cao_cvs\cao\php\cao_update.php; DestDir: {app}\php\; Flags: comparetimestamp
Source: P:\CAO.MYSQL\cao_cvs\cao\php\xml_export.php; DestDir: {app}\php\; Flags: comparetimestamp
Source: P:\CAO.MYSQL\cao_cvs\cao\php\OSC-CAO_install.txt; DestDir: {app}\php\; Flags: comparetimestamp
Source: P:\CAO.MYSQL\bin\caofaq.chm; DestDir: {app}; Flags: promptifolder
; NOTE: Don't use "Flags: ignoreversion" on any shared system files

[Icons]
Name: {group}\CAO-Faktura; Filename: {app}\CAO32_DB.exe; IconIndex: 0
Name: {group}\CAO-Kasse; Filename: {app}\cao_kasse.exe; WorkingDir: {app}; IconIndex: 0
Name: {group}\CAO-FAQ; Filename: {app}\caofaq.chm; WorkingDir: {app}; IconIndex: 0

[Tasks]


[INI]
Filename: {app}\CAO32_DB.cfg; Section: VERSION; Key: cao32_db.exe; String: 1.2.3.5
Filename: {app}\CAO32_DB.cfg; Section: VERSION; Key: cao_kasse.exe; String: 1.2.3.2
Filename: {app}\CAO32_DB.cfg; Section: VERSION; Key: zipdll.dll; String: 1.7.0.8
Filename: {app}\CAO32_DB.cfg; Section: VERSION; Key: cao_update.exe; String: 1.3.0.2
Filename: {app}\CAO32_DB.cfg; Section: VERSION; Key: rbide.cst; String: 1.0.0.0
Filename: {app}\CAO32_DB.cfg; Section: VERSION; Key: rbide.eng; String: 1.0.0.0
Filename: {app}\CAO32_DB.cfg; Section: VERSION; Key: rbide.deu; String: 1.0.0.0
Filename: {app}\CAO32_DB.cfg; Section: VERSION; Key: rbprint.cst; String: 1.0.0.0
Filename: {app}\CAO32_DB.cfg; Section: VERSION; Key: rbprint.eng; String: 1.0.0.0
Filename: {app}\CAO32_DB.cfg; Section: VERSION; Key: rbprint.deu; String: 1.0.0.0
Filename: {app}\CAO32_DB.cfg; Section: VERSION; Key: psrbd50.bpl; String: 0.99
Filename: {app}\CAO32_DB.cfg; Section: VERSION; Key: unzdll.dll; String: 1.7.0.8
Filename: {app}\CAO32_DB.cfg; Section: VERSION; Key: changes.txt; String: 23.01.2003
Filename: {app}\CAO32_DB.cfg; Section: VERSION; Key: license.txt; String: 20.01.2003
Filename: {app}\CAO32_DB.cfg; Section: VERSION; Key: license-GER.txt; String: 20.01.2003
Filename: {app}\CAO32_DB.cfg; Section: VERSION; Key: land.cao; String: 1.06
Filename: {app}\CAO32_DB.cfg; Section: VERSION; Key: plz; String: 1.08
Filename: {app}\CAO32_DB.cfg; Section: VERSION; Key: blz; String: 1.08
Filename: {app}\CAO32_DB.cfg; Section: VERSION; Key: formulare.cao; String: 13.12.2003

[Dirs]
Name: {app}\php

[Languages]
Name: German; MessagesFile: E:\Inno Setup 3\German.isl
