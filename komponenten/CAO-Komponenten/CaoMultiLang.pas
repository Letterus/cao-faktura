//
// based on sources that can be found on
// http://www.del-net.com/frmDelphiMultiLan.html
//
//

unit CaoMultiLang;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, FileCtrl, IniFiles, Buttons, ComCtrls, Menus, ExtCtrls;

type
  TLngWriteMode = (lwmAppend, lwmCreate);

  TCAOMultiLang = class(TComponent)
  private
    { Private-Deklarationen }
    FWriteLog      : boolean;
    FWriteMode     : TLngWriteMode;
    FMessageHeader : string;

    FLanguageFilesDirectory: string;
    FLanguageFile: string; // wenn '' dann default
    procedure SetLanguageFilesDirectory(value: string);
    //procedure ReplaceLF(var value: string{;ReplaceWith: string});
  protected
    { Protected-Deklarationen }
  public
    { Public-Deklarationen }
    constructor Create(AOwner: TComponent); override;
    // Speichern
    procedure WriteComponentProperties;
    procedure WriteInternalStrings(var InternalStrings: array of string;
                                   Offset: Integer);
    // Laden
    procedure GetLanguages(values: TStrings);
    procedure FillMsgArray(IniFileName: string; var s: array of string; StartID: Integer);
    //function  GetActiveLanguage(RootKey: dword; Key: string; value: string): string;
    //function  SetActiveLanguage(RootKey: dword; Key: string; value: string; Lan: string): boolean;
    procedure InitForm(IniFileName: string; Frm: TForm);
  published
    { Published-Deklarationen }
    property WriteLog: boolean read FWriteLog write FWriteLog default false;
    property MessageHeader: string read FMessageHeader write FMessageHeader;
    property WriteMode: TLngWriteMode read FWriteMode write FWriteMode;

    property LanguageFilesDirectory: string read FLanguageFilesDirectory write SetLanguageFilesDirectory;
    property LanguageFile: string read FLanguageFile write FLanguageFile;
  end;

procedure Register;

implementation

uses CaoGroupBox, JvSpeedButton;

//const RStr : String ='\r\n';

//------------------------------------------------------------------------------
procedure Register;
begin
  RegisterComponents('CAO-Faktura', [TCAOMultiLang]);
end;
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
constructor TCAOMultiLang.Create(AOwner: TComponent);
begin
  inherited;
  FWriteLog      := false;
  FWriteMode     := lwmAppend;
  FMessageHeader := 'Messages';
  FLanguageFilesDirectory :=ExtractFilePath(ParamStr(0));
end;
//------------------------------------------------------------------------------



//------------------------------------------------------------------------------
// Routinen zum erzeugen der Sparachdateien
//------------------------------------------------------------------------------
procedure TCAOMultiLang.WriteComponentProperties;
var
  ini        : TIniFile;
  FormsIdx,
  j          : Integer;
  FormName   : string;
  C, C2      : TComponent;
  ItemCount  : Integer;
  ItemString : string;
  FileName   : String;
begin
  FileName :=LanguageFilesDirectory+LanguageFile;

  if WriteMode = lwmCreate then
  begin
    if FileExists(FileName) then DeleteFile(FileName);
  end;
  ini := TIniFile.Create(FileName);

  try
    for FormsIdx := 0 to Application.ComponentCount - 1 do
    begin
      if Application.Components[FormsIdx] is TForm then
      begin
        C2 := Application.Components[FormsIdx];
        FormName := C2.Name;
        ini.WriteString(FormName, FormName, (C2 as TForm).Caption);

        for j := 0 to Application.Components[FormsIdx].ComponentCount - 1 do
        begin
          c := Application.Components[FormsIdx].Components[j];
          // be carefully with ClassNames - if you compare with "=" they are case sensitive!

          if c.ClassName = 'TPanel' then
          begin
            ini.WriteString(FormName, c.Name, (c as TPanel).Caption);
            if (c as TPanel).Hint <> '' then
              ini.WriteString(FormName, c.Name + 'Hint', (c as TPanel).Hint);
            continue;
          end;

          if c.ClassName = 'TGroupBox' then
          begin
            ini.WriteString(FormName, c.Name, (c as TGroupBox).Caption);
            if (c as TGroupBox).Hint <> '' then
              ini.WriteString(FormName, c.Name + 'Hint', (c as TGroupBox).Hint);
            continue;
          end;

          if c.ClassName = 'TCaoGroupBox' then
          begin
            ini.WriteString(FormName, c.Name, (c as TCaoGroupBox).Caption);
            if (c as TCaoGroupBox).Hint <> '' then
              ini.WriteString(FormName, c.Name + 'Hint', (c as TCaoGroupBox).Hint);
            continue;
          end;
          if c.ClassName = 'TButton' then
          begin
            ini.WriteString(FormName, c.Name, (c as TButton).Caption);
            if (c as TButton).Hint <> '' then
              ini.WriteString(FormName, c.Name + 'Hint', (c as TButton).Hint);
            continue;
          end;
          if c.ClassName = 'TLabel' then
          begin
            ini.WriteString(FormName, c.Name, (c as TLabel).Caption);
            if (c as TLabel).Hint <> '' then
              ini.WriteString(FormName, c.Name + 'Hint', (c as TLabel).Hint);
            continue;
          end;
          if c.ClassName = 'TComboBox' then
          begin
            //ItemCount := (c as TComboBox).Items.Count;
            //ItemString := (c as TComboBox).Items.Text;
            //ItemString :=StringReplace(ItemString, #13#10, RStr, [rfReplaceAll]);

            ini.WriteString(FormName, c.Name + 'Items', (c as TComboBox).Items.CommaText);
            //ini.WriteString(FormName, c.Name + 'Items', ItemString);
            if (c as TComboBox).Hint <> '' then
              ini.WriteString(FormName, c.Name + 'Hint', (c as TComboBox).Hint);
            continue;
          end;
          if c.ClassName = 'TOpenDialog' then
          begin
            ini.WriteString(FormName, c.Name + 'Title', (c as TOpenDialog).Title);
            ini.WriteString(FormName, c.Name + 'Filter', (c as TOpenDialog).Filter);
            continue;
          end;
          if c.ClassName = 'TSaveDialog' then
          begin
            ini.WriteString(FormName, c.Name + 'Title', (c as TSaveDialog).Title);
            ini.WriteString(FormName, c.Name + 'Filter', (c as TSaveDialog).Filter);
            continue;
          end;
          if c.ClassName = 'TSpeedButton' then
          begin
            ini.WriteString(FormName, c.Name, (c as TSpeedButton).Caption);
            if (c as TSpeedButton).Hint <> '' then
              ini.WriteString(FormName, c.Name + 'Hint', (c as TSpeedButton).Hint);
            continue;
          end;
          if c.ClassName = 'TJvSpeedButton' then
          begin
            ini.WriteString(FormName, c.Name, (c as TJvSpeedButton).Caption);
            if (c as TJvSpeedButton).Hint <> '' then
              ini.WriteString(FormName, c.Name + 'Hint', (c as TJvSpeedButton).Hint);
            continue;
          end;
          if c.ClassName = 'TToolButton' then
          begin
            ini.WriteString(FormName, c.Name, (c as TToolButton).Caption);
            if (c as TToolButton).Hint <> '' then
              ini.WriteString(FormName, c.Name + 'Hint', (c as TToolButton).Hint);
            continue;
          end;
          if c.ClassName = 'TCheckBox' then
          begin
            ini.WriteString(FormName, c.Name, (c as TCheckBox).Caption);
            if (c as TCheckBox).Hint <> '' then
              ini.WriteString(FormName, c.Name + 'Hint', (c as TCheckBox).Hint);
            continue;
          end;
          if c.ClassName = 'TRadioButton' then
          begin
            ini.WriteString(FormName, c.Name, (c as TRadiobutton).Caption);
            if (c as TRadioButton).Hint <> '' then
              ini.WriteString(FormName, c.Name + 'Hint', (c as TRadiobutton).Hint);
            continue;
          end;
          if c.ClassName = 'TBitBtn' then
          begin
            ini.WriteString(FormName, c.Name, (c as TBitBtn).Caption);
            if (c as TBitBtn).Hint <> '' then
              ini.WriteString(FormName, c.Name + 'Hint', (c as TBitBtn).Hint);
            continue;
          end;
          if c.ClassName = 'TMenuItem' then
          begin
            ini.WriteString(FormName, c.Name, (c as TMenuItem).Caption);
            if (c as TMenuItem).Hint <> '' then
              ini.WriteString(FormName, c.Name + 'Hint', (c as TMenuItem).Hint);
            continue;
          end;
          if c.ClassName = 'TListBox' then
          begin
            //ItemCount := (c as TListBox).Items.Count;
            ItemString := (c as TListBox).Items.CommaText;

            //ItemString :=StringReplace(ItemString, #13#10, RStr, [rfReplaceAll]);

            ini.WriteString(FormName, c.Name + 'Items', ItemString);
            if (c as TListBox).Hint <> '' then
              ini.WriteString(FormName, c.Name + 'Hint', (c as TListBox).Hint);
            continue;
          end;
          if WriteLog then
            ini.WriteString(FormName, c.ClassName, 'LOG: Component not parsed yet');
        end;
      end;
    end;
  finally
    ini.Free;
  end;
end;
//------------------------------------------------------------------------------
procedure TCAOMultiLang.WriteInternalStrings(var InternalStrings: array of string;
                                             Offset: Integer);
var
  InternalStringIdx : Integer;
  ini               : TIniFile;
  FileName          : String;
begin
  FileName :=LanguageFilesDirectory+LanguageFile;

  if WriteMode = lwmCreate then
  begin
    if FileExists(FileName) then DeleteFile(FileName);
  end;

  ini := TIniFile.Create(FileName);
  try
    ini.EraseSection(FMessageHeader);
    for InternalStringIdx := 0 to Length(InternalStrings) - 1 do
      ini.WriteString(FMessageHeader, IntToStr(Offset + InternalStringIdx),
                      InternalStrings[InternalStringIdx]);
  finally
    ini.Free;
  end;
end;
//------------------------------------------------------------------------------

















//------------------------------------------------------------------------------
// Sprache laden etc.
//------------------------------------------------------------------------------


procedure TCAOMultiLang.FillMsgArray( IniFileName: string;
                                  var s: array of string;
                                  StartID: Integer);
var
  ini: TIniFile;
  Idx: Integer;
begin
  if FileExists(IniFileName) then
  begin
    ini := TIniFile.Create(IniFileName);
    try
      for Idx := 0 to Length(s) - 1 do
      begin
        s[Idx] := ini.ReadString('OtherStrings', IntToStr(Idx + StartID), s[Idx]);
      end;
    finally
      ini.Free;
    end;
  end;
end;
//------------------------------------------------------------------------------
{
function TCAOMultiLang.GetActiveLanguage(RootKey: dword; Key,
  value: string): string;
var
  vReg: TRegistry;
begin
  Result := '';
  vReg := TRegistry.Create;
  try
    vReg.RootKey := RootKey;

    if vReg.KeyExists(Key) then
      if vReg.OpenKey(Key, false) then
      begin
        if vReg.ValueExists(value) then
          Result := vReg.ReadString(value);
        vReg.CloseKey();
      end;
  finally
    vReg.Free;
  end;
end;
}
//------------------------------------------------------------------------------
procedure TCAOMultiLang.GetLanguages(values: TStrings);
var
  SrcRec: TSearchRec;
  found: boolean;
begin
  found := false;
  values.Clear;
  if DirectoryExists(FLanguageFilesDirectory) then
  begin
    if FindFirst(FLanguageFilesDirectory + '\*.lng', faAnyFile, SrcRec) = 0 then
    repeat
      found := true;
      values.Add(Copy(SrcRec.Name, 1, Length(SrcRec.Name) - 4));
    until FindNext(SrcRec) <> 0;

    if found then
      FindClose(SrcRec);
  end;
end;
//------------------------------------------------------------------------------
procedure TCAOMultiLang.InitForm(IniFileName: string; Frm: TForm);
var
  i: Integer;
  ini: TIniFile;
  FormName: string;
  C: TComponent;
  //ItemCount: Integer;
  //ItemString: string;
  s: string;
begin
  if FileExists(IniFileName) then
  begin
    ini := TIniFile.Create(IniFileName);

    try
      FormName := Frm.Name;
      frm.Caption := ini.ReadString(FormName, FormName, frm.Caption);
      FormName := Frm.Name;
      for i := 0 to frm.ComponentCount - 1 do
      begin
        c := frm.Components[i];

        if c.ClassName = 'TGroupBox' then
        begin
          (C as TGroupBox).Caption := ini.ReadString(FormName, (C as TGroupBox).Name, (C as TGroupBox).Caption);
          (C as TGroupBox).Hint := ini.ReadString(FormName, (C as TGroupBox).Name + 'Hint', (C as TGroupBox).Hint);
          continue;
        end;

        if c.ClassName = 'TCaoGroupBox' then
        begin
          (C as TCaoGroupBox).Caption := ini.ReadString(FormName, (C as TCaoGroupBox).Name, (C as TCaoGroupBox).Caption);
          (C as TCaoGroupBox).Hint := ini.ReadString(FormName, (C as TCaoGroupBox).Name + 'Hint', (C as TCaoGroupBox).Hint);
          continue;
        end;

        if c.ClassName = 'TPanel' then
        begin
          (C as TPanel).Caption := ini.ReadString(FormName, (C as TPanel).Name, (C as TPanel).Caption);
          (C as TPanel).Hint := ini.ReadString(FormName, (C as TPanel).Name + 'Hint', (C as TPanel).Hint);
          continue;
        end;

        if c.ClassName = 'TButton' then
        begin
          (C as TButton).Caption := ini.ReadString(FormName, (C as TButton).Name, (C as TButton).Caption);
          (C as TButton).Hint := ini.ReadString(FormName, (C as TButton).Name + 'Hint', (C as TButton).Hint);
          continue;
        end;
        if c.ClassName = 'TLabel' then
        begin
          (C as TLabel).Caption := ini.ReadString(FormName, (C as TLabel).Name, (C as TLabel).Caption);
          (C as TLabel).Hint := ini.ReadString(FormName, (C as TLabel).Name + 'Hint', (C as TLabel).Hint);
          continue;
        end;
        if c.ClassName = 'TCheckBox' then
        begin
          (C as TCheckBox).Caption := ini.ReadString(FormName, (C as TCheckBox).Name, (C as TCheckBox).Caption);
          (C as TCheckBox).Hint := ini.ReadString(FormName, (C as TCheckBox).Name + 'Hint', (C as TCheckBox).Hint);
          continue;
        end;
        if c.ClassName = 'TBitBtn' then
        begin
          (C as TBitBtn).Caption := ini.ReadString(FormName, (C as TBitBtn).Name, (C as TBitBtn).Caption);
          (C as TBitBtn).Hint := ini.ReadString(FormName, (C as TBitBtn).Name + 'Hint', (C as TBitBtn).Hint);
          continue;
        end;
        if c.ClassName = 'TOpenDialog' then
        begin
          (C as TOpenDialog).Title := ini.ReadString(FormName, (C as TOpenDialog).Name + 'Title', (C as TOpenDialog).Title);
          (C as TOpenDialog).Filter := ini.ReadString(FormName, (C as TOpenDialog).Name + 'Filter', (C as TOpenDialog).Filter);
          continue;
        end;
        if c.ClassName = 'TSaveDialog' then
        begin
          (C as TSaveDialog).Title := ini.ReadString(FormName, (C as TSaveDialog).Name + 'Title', (C as TSaveDialog).Title);
          (C as TSaveDialog).Filter := ini.ReadString(FormName, (C as TSaveDialog).Name + 'Filter', (C as TSaveDialog).Filter);
          continue;
        end;
        if c.ClassName = 'TToolButton' then
        begin
          (C as TToolButton).Caption := ini.ReadString(FormName, (C as TToolButton).Name, (C as TToolButton).Caption);
          (C as TToolButton).Hint := ini.ReadString(FormName, (C as TToolButton).Name + 'Hint', (C as TToolButton).Hint);
          continue;
        end;
        if c.ClassName = 'TSpeedButton' then
        begin
          (C as TSpeedButton).Caption := ini.ReadString(FormName, (C as TSpeedButton).Name, (C as TSpeedButton).Caption);
          (C as TSpeedButton).Hint := ini.ReadString(FormName, (C as TSpeedButton).Name + 'Hint', (C as TSpeedButton).Hint);
          continue;
        end;
        if c.ClassName = 'TJvSpeedButton' then
        begin
          (C as TJvSpeedButton).Caption := ini.ReadString(FormName, (C as TJvSpeedButton).Name, (C as TJvSpeedButton).Caption);
          (C as TJvSpeedButton).Hint := ini.ReadString(FormName, (C as TJvSpeedButton).Name + 'Hint', (C as TJvSpeedButton).Hint);
          continue;
        end;
        if c.ClassName = 'TRadioButton' then
        begin
          (C as TRadioButton).Caption := ini.ReadString(FormName, (C as TRadioButton).Name, (C as TRadioButton).Caption);
          (C as TRadioButton).Hint := ini.ReadString(FormName, (C as TRadioButton).Name + 'Hint', (C as TRadioButton).Hint);
          continue;
        end;
        if c.ClassName = 'TMenuItem' then
        begin
          s := (C as TMenuItem).Caption;
          (C as TMenuItem).Caption := ini.ReadString(FormName, (C as TMenuItem).Name, (C as TMenuItem).Caption);
          (C as TMenuItem).Hint := ini.ReadString(FormName, (C as TMenuItem).Name + 'Hint', (C as TMenuItem).Hint);
          continue;
        end;
        if c.ClassName = 'TComboBox' then
        begin
          //ItemCount := (c as TComboBox).Items.Count;
          //ItemString := ini.ReadString(FormName, (C as TComboBox).Name + 'Items', (c as TComboBox).Items.Text);
          //(c as TComboBox).Items.Text := StringReplace(ItemString, RStr, #13#10, [rfReplaceAll]);

          (c as TComboBox).Items.CommaText :=ini.ReadString(FormName, (C as TComboBox).Name + 'Items', (c as TComboBox).Items.CommaText);

          (C as TComboBox).Hint := ini.ReadString(FormName, (C as TComboBox).Name + 'Hint', (C as TComboBox).Hint);
          continue;
        end;
          if c.ClassName = 'TListBox' then
          begin
          //ItemCount := (c as TListBox).Items.Count;
          //ItemString := ini.ReadString(FormName, (C as TListBox).Name + 'Items', (c as TListBox).ITems.Text);
          //(c as TListBox).Items.Text := StringReplace(ItemString, RStr, #13#10, [rfReplaceAll]);

          (c as TListBox).Items.CommaText :=ini.ReadString(FormName, (C as TListBox).Name + 'Items', (c as TListBox).Items.CommaText);

          (C as TListBox).Hint := ini.ReadString(FormName, (C as TListBox).Name + 'Hint', (C as TListBox).Hint);
          continue;
        end;
      end;
    finally
      ini.Free;
    end;
  end;
end;
//------------------------------------------------------------------------------
{
function TCAOMultiLang.SetActiveLanguage(RootKey: dword; Key, value,
  Lan: string): boolean;
var
  vReg: TRegistry;
begin
  Result := false;
  vReg := TRegistry.Create;
  try
    vReg.RootKey := RootKey;

    if vReg.OpenKey(Key, true) then
    begin
      vReg.WriteString(value, Lan);
      vReg.CloseKey();
    end;
  finally
    vReg.Free;
  end;
end;
}
//------------------------------------------------------------------------------
procedure TCAOMultiLang.SetLanguageFilesDirectory(value: string);
begin
  if FLanguageFilesDirectory <> value then
  begin
    FLanguageFilesDirectory := value;
    if (length(FLanguageFilesDirectory)>0)and
       (FLanguageFilesDirectory[Length(FLanguageFilesDirectory)] <> '\')
     then FLanguageFilesDirectory :=FLanguageFilesDirectory+'\';
  end;
end;
//------------------------------------------------------------------------------
end.
