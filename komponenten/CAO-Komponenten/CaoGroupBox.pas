{

DELPHI-CLASS  TCaoGroupBox Version 1.0
Copyright (C) 2003 Jan Pokrandt / Jan@JP-SOFT.de

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.


******* TCaoGroupBox comes with ABSOLUTELY NO WARRANTY ***************
}

unit CaoGroupBox;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls;

type
  TCaoGroupBox = class(TCustomGroupBox)
  private
    { Private-Deklarationen }
    FFRameColor : tColor;
  protected
    { Protected-Deklarationen }
    procedure Paint; override;
    procedure AdjustClientRect(var Rect: TRect); override;
    procedure ResizeNew(Sender: TObject);
    procedure SetFrameColor (NewCol : tColor);
  public
    { Public-Deklarationen }
    constructor Create(AOwner: TComponent); override;
  published
    { Published-Deklarationen }
    property Align;
    property Anchors;
    property BiDiMode;
    property Caption;
    property Color;
    property Constraints;
    property Ctl3D;
    property DockSite;
    property DragCursor;
    property DragKind;
    property DragMode;
    property Enabled;
    property Font;
    property ParentBiDiMode;
    property ParentColor;
    property ParentCtl3D;
    property ParentFont;
    property ParentShowHint;
    property PopupMenu;
    property ShowHint;
    property TabOrder;
    property TabStop;
    property Visible;
    property FRameColor : tColor Read FFRameColor Write SetFrameColor Default clSilver;

    property OnClick;
    property OnContextPopup;
    property OnDblClick;
    property OnDragDrop;
    property OnDockDrop;
    property OnDockOver;
    property OnDragOver;
    property OnEndDock;
    property OnEndDrag;
    property OnEnter;
    property OnExit;
    property OnGetSiteInfo;
    property OnMouseDown;
    property OnMouseMove;
    property OnMouseUp;
    property OnStartDock;
    property OnStartDrag;
    property OnUnDock;
    //property OnResize;
  end;

procedure Register;

implementation

constructor TCaoGroupBox.Create(AOwner: TComponent);
begin
     inherited Create (AOwner);
     OnResize :=ResizeNew;
     FFrameColor :=clSilver;
end;

procedure TCaoGroupBox.SetFrameColor (NewCol : tColor);
begin
     if NewCol <> FFRameColor then
     begin
        FFrameColor :=NewCol;
        Invalidate;
     end;
end;

procedure TCaoGroupBox.AdjustClientRect(var Rect: TRect);
begin
  inherited AdjustClientRect(Rect);
  //Canvas.Font := Font;
  //Inc(Rect.Top, Canvas.TextHeight('0g')+4);
  inc(Rect.Left,5);

  inc (Rect.Top,5);

  dec (Rect.Right,5);
  dec (Rect.Bottom,5);

  InflateRect(Rect, -1, -1);
  if not Ctl3d then InflateRect(Rect, -1, -1);
end;

procedure TCaoGroupBox.Paint;
var
  H: Integer;
  R: TRect;
  W: Integer;
  Flags: Longint;
begin
  with Canvas do
  begin
    Pen.Color :=FrameColor;
    Brush.Color :=FrameColor;
    FillRect(ClientRect);

    Pen.Color :=Color;
    Brush.Color :=Color;

    Font := Self.Font;
    Font.Style :=[fsBold];

    H := TextHeight('0g');

    if Text <> '' then
    begin
      W :=TextWidth (Text);
      Canvas.Polygon ([Point(3,0), Point(3,h+1), Point (5+w+5,h+1), Point (5+w+5+h+1,0)]);
      Canvas.Polygon ([Point (w+15,h+1), Point (w+15+h+1,0), Point (width-4,0), Point(width-4,h+1)]);


      R := Rect(8, 0, 8+W, H);
      Flags := DrawTextBiDiModeFlags(DT_SINGLELINE);
      DrawText(Handle, PChar(Text), Length(Text), R, Flags or DT_CALCRECT);
      DrawText(Handle, PChar(Text), Length(Text), R, Flags);
    end
       else
    begin
         W :=20;
         Canvas.Polygon ([Point(3,0), Point(3,h+1), Point (5+w+5,h+1), Point (5+w+5+h+1,0)]);
         Canvas.Polygon ([Point (w+15,h+1), Point (w+15+h+1,0), Point (width-4,0), Point(width-4,h+1)]);
    end;

    Pen.Color :=Color;
    r :=clientrect;
    inc (r.right,5);
    inc (r.bottom,5);
    dec (r.top,3);
    dec (r.left,5);
    AdjustClientRect (r);
    FillRect (r);

  end;
end;

procedure TCaoGroupBox.ResizeNew(Sender: TObject);
var i : integer;
    Temp : TControl;
begin
     if ControlCount > 0 then
     for I := ControlCount - 1 downto 0 do
     begin
       Temp := Controls[I];
       if (Temp.Tag>0) then
        tEdit(Temp).Width :=ClientWidth - tEdit(Temp).Left - 8;
     end;
end;


procedure Register;
begin
  RegisterComponents('JP-SOFT', [TCaoGroupBox]);
end;

end.
