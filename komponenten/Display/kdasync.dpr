{

CAO-Faktura (Kundendisplay)
Copyright (C) 2003 Daniel Pust / daniel@pust.de

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

******* CAO-Faktura comes with ABSOLUTELY NO WARRANTY ***************

Programm     : CAO-Faktura / Kassen
Modul        : ba63.dll
Stand        : 21.08.2003
Version      : 0.6
Beschreibung : Anbindung der Kasse an Kundendisplay DLL f�r ba63 (Siemens)
               Kundendisplay 2x20 Zeichen mit serieller Ansteuerung.

History :
21.08.2003 - DP: Erste Version
01.04.2004 - JP: DOS-Zeichenumwandlung eingebaut

TODO:
- Laden der Bilder in eine Stringliste
- Konfiguration ob Display � Zeichen unterst�tzt und wenn ja wie dieses
  programmiert wird.

}

library kdasync;

uses
  SysUtils,
  Dialogs,
  Classes,
  Serial,
  IniFiles,
  JclStrings;

{$R *.res}

var
  KDPORT: TSerial;         //
  InitCode: string;        // Steuercode f�r Display Initialisierung
  ClearCode: string;       // Steuercode f�r Display l�schen
  Display_aktiv: boolean;  // Wird beim Initialisieren gesetzt und bei Fehlern
                           // oder beim deaktivieren zur�ckgesetzt
  ininame: string;         // Dateiname der INI Datei incl. Pfad wird durch Init
                           // belegt
  prefix: string;
  OEM_Convert : Boolean;   // True, wenn Umwandlung von WIN->DOS durchgef�hrt 
                           // werden soll

//******************************************************************************
// Senden eines Strings (Text o. Befehl) an das Display
procedure KD_PRINT(Text: string);
begin
  if Display_aktiv then
  begin
    if OEM_Convert then Text :=StrAnsiToOem(Text);
    KDPORT.TransmittText(Text);
  end;
end;

//******************************************************************************
// Funktion zu "abmelden" des Display's
function KD_EXIT: boolean;
begin
  {
  if Display_aktiv then
    KD_PRINT(ClearCode); }
  Display_aktiv := false;
  KDPORT.Free;
  result := true;
end;

//******************************************************************************
// Display l�schen und Cursor an Position 1/1 setzen
procedure KD_CLEAR;
begin
  if Display_aktiv then
    KD_PRINT(ClearCode);
end;

//******************************************************************************
// Initialisieren des Display's, die Parameter werden aus der CAO32_DB.cfg
// gelesen
function KD_INIT: boolean;
var
  INI: TIniFile;
  dummy: string;
  error: boolean;

begin
  error := false;
  ini := nil;
  ininame := extractfilepath(paramstr(0)) + 'CAO32_DB.CFG';

  if FileExists(ininame) then
  begin
    INI := TIniFile.Create(ininame);
    if INI.SectionExists('DISPLAY') then
    begin
      // Objekt f�r Schnittstelle erstellen
      KDPORT := TSerial.Create(nil);
      // Schnittstellenkonfiguration laden

      dummy := INI.ReadString('DISPLAY', 'PORT', '');
      if dummy <> '' then
      begin
        KDPORT.COMPort := StrToInt(dummy);
      end else error := true;

      dummy := INI.ReadString('DISPLAY', 'BAUD', '');
      if dummy <> '' then
      begin
        if dummy = '50' then KDPORT.Baudrate := br_000050 else
          if dummy = '110' then KDPORT.Baudrate := br_000110 else
            if dummy = '150' then KDPORT.Baudrate := br_000150 else
              if dummy = '300' then KDPORT.Baudrate := br_000300 else
                if dummy = '600' then KDPORT.Baudrate := br_000600 else
                  if dummy = '1200' then KDPORT.Baudrate := br_001200 else
                    if dummy = '2400' then KDPORT.Baudrate := br_002400 else
                      if dummy = '4800' then KDPORT.Baudrate := br_004800 else
                        if dummy = '9600' then KDPORT.Baudrate := br_009600 else
                          if dummy = '14400' then
                            KDPORT.Baudrate := br_014400 else
                            if dummy = '19200' then
                              KDPORT.Baudrate := br_019200 else
                              if dummy = '38400' then
                                KDPORT.Baudrate := br_038400 else
                                if dummy = '56000' then
                                  KDPORT.Baudrate := br_056000 else
                                  if dummy = '57600' then
                                    KDPORT.Baudrate := br_057600 else
                                    if dummy = '115200' then
                                      KDPORT.Baudrate := br_115200;
      end else error := true;

      dummy := INI.ReadString('DISPLAY', 'DATA', '');
      if dummy <> '' then
      begin
        if dummy = '7' then KDPORT.DataBits := db_7 else
          if dummy = '8' then KDPORT.DataBits := db_8;
      end else error := true;

      dummy := INI.ReadString('DISPLAY', 'PARITY', '');
      if dummy <> '' then
      begin
        if dummy = 'none' then KDPORT.ParityBit := none else
          if dummy = 'odd' then KDPORT.ParityBit := odd else
            if dummy = 'even' then KDPORT.ParityBit := even else
              if dummy = 'mark' then KDPORT.ParityBit := mark else
                if dummy = 'space' then KDPORT.ParityBit := space;
      end else error := true;

      dummy := INI.ReadString('DISPLAY', 'STOP', '');
      if dummy <> '' then
      begin
        if dummy = '1' then KDPORT.StopBits := sb_1 else
          if dummy = '15' then KDPORT.StopBits := sb_15 else
            if dummy = '2' then KDPORT.StopBits := sb_2;
      end else error := true;

      dummy := INI.ReadString('DISPLAY', 'NAME', '');
      if dummy <> '' then
      begin
        prefix := dummy;
      end else error := true;

      dummy := INI.ReadString('DISPLAY', prefix + '_CODE00', '');
      if dummy <> '' then
      begin
        InitCode := StrEscapedToString(dummy);
      end else error := true;

      dummy := INI.ReadString('DISPLAY', prefix + '_CODE01', '');
      if dummy <> '' then
      begin
        ClearCode := StrEscapedToString(dummy);
      end else error := true;

      OEM_Convert :=INI.ReadBool ('DISPLAY','OEM_ZEICHENSATZ',True);

    end else error := true;
  end else error := true;

  INI.Free;

  if not error then
  begin
    Display_Aktiv := true;
    KDPORT.OpenComm;
    KD_PRINT(FORMAT(InitCode, []));
    result := true;
  end
  else
  begin
    Display_Aktiv := false;
    KDPORT.Free;
    result := false;
  end;
end;

//******************************************************************************
// Bild ausgeben
function KD_BILD(BildName: string; Values: array of const): boolean;
var
  INI: TIniFile;
  dummy: string;
  error: boolean;

begin
  error := false;
  ini := nil;

  if Display_aktiv then
  begin
    KD_CLEAR;
    if FileExists(ininame) then
    begin
      try
        INI := TIniFile.Create(ininame);
        if INI.SectionExists('DISPLAY') then
        begin
          dummy := INI.ReadString('DISPLAY', prefix + '_' + BildName, '');
        end else error := true;
      finally
        INI.Free;
      end;
    end else error := true;

    if not error then
    begin
      try
        KD_PRINT(FORMAT(StrEscapedToString(dummy), Values));
      except
        error := true;
      end;
    end;

  end;
  result := not error;
end;
//******************************************************************************

exports
  KD_CLEAR,
  KD_INIT,
  KD_PRINT,
  KD_BILD,
  KD_EXIT;

begin
end.

