<?php

/*******************************************************************************************
*                                                                                          *
*  CAO-Faktura f�r Windows Version 1.0                                                     *
*  Copyright (C) 2003 Jan Pokrandt / Jan@JP-SOFT.de                                        *
*                                                                                          *
*  This program is free software; you can redistribute it and/or                           *
*  modify it under the terms of the GNU General Public License                             *
*  as published by the Free Software Foundation; either version 2                          *
*  of the License, or any later version.                                                   *
*                                                                                          *
*  This program is distributed in the hope that it will be useful,                         *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of                          *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                           *
*  GNU General Public License for more details.                                            *
*                                                                                          *
*  You should have received a copy of the GNU General Public License                       *
*  along with this program; if not, write to the Free Software                             *
*  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
*                                                                                          *
*  ******* CAO-Faktura comes with ABSOLUTELY NO WARRANTY ***************                   *
*                                                                                          *
*  Programm     : CAO-Faktura                                                              *
*  Modul        : cao_update.php                                                           *
*  Stand        : 31.01.2004                                                               *
*  Version      : 0.30                                                                     *
*  Beschreibung : Script zum Datenaustausch CAO-Faktura <--> osCommerce-Shop               *
*                                                                                          *
*  based on:                                                                               *
* (c) 2000 - 2001 The Exchange Project                                                     *
* (c) 2001 - 2003 osCommerce, Open Source E-Commerce Solutions                             *
* (c) 2001 - 2003 TheMedia, Dipl.-Ing Thomas Pl�nkers                                      *
* (c) 2003 JP-Soft, Jan Pokrandt                                                           *
* (c) 2003 IN-Solution, Henri Schmidhuber                                                  *
* (c) 2003 www.websl.de, Karl Langmann                                                     *
* (c) 2003 RV-Design Raphael Vullriede
                                                   *
* Released under the GNU General Public License                                            *
*                                                                                          *
*  History :                                                                               *
*                                                                                          *
*  - 25.06.2003 JP Version 0.1 released                                                    *
*  - 26.06.2003 HS beim Orderexport orderstatus und comment hinzugefuegt                   *
*  - 29.06.2003 JP order_update entfernt und in die Datei cao_update.php verschoben        *
*  - 20.07.2003 HS Shipping und Paymentklassen aufgenommen                                 *
*  - 02.08.2003 KL MANUFACTURERS_DESCRIPTION  language_id ge�ndert in languages_id         *
*  - 09.08.2003 JP fuer das Modul Banktransfer werden jetzt die daten bei der Bestll-      *
*                  uebermittlung mit ausgegeben                                            *
*  - 10.08.2003 JP Geburtsdatum wird jetzt in den Bestellungen mit uebergeben              *
*  - 18.08.2003 JP Bug bei Products/URL beseitigt                                          *
*  - 18.08.2003 HS Bankdaten werden nur bei Banktransfer ausgelesen                        *
*  - 25.10.2003 Kunden-Export hinzugef�gt                                                  *
*  - 24.11.2003 HS Fix Kunden-Export - Newsletterexport hinzugef�gt                        *
*  - 31.01.2004 JP Resourcenverbrauch minimiert                                            *
*                  tep_set_time_limit ist jetzt per DEFINE zu- und abschaltbar             *
*******************************************************************************************/

require('includes/application_top.php');


$order_total_class['ot_cod_fee']['prefix'] = '+';
$order_total_class['ot_cod_fee']['tax'] = '16';

$order_total_class['ot_customer_discount']['prefix'] = '-';
$order_total_class['ot_customer_discount']['tax'] = '16';

$order_total_class['ot_gv']['prefix'] = '-';
$order_total_class['ot_gv']['tax'] = '0';

$order_total_class['ot_loworderfee']['prefix'] = '+';
$order_total_class['ot_loworderfee']['tax'] = '16';

$order_total_class['ot_shipping']['prefix'] = '+';
$order_total_class['ot_shipping']['tax'] = '16';


/******************************************************************************************/
$version_nr    = '0.30';
$version_datum = '2004.01.31';
/******************************************************************************************/


  //define('SET_TIME_LIMIT', 1); // aktivieren um SetTimeLimit einzuschalten


  header ("Last-Modified: ". gmdate ("D, d M Y H:i:s"). " GMT");  // immer ge�ndert
  header ("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
  header ("Pragma: no-cache"); // HTTP/1.0
  header ("Content-type: text/xml");


  if ($HTTP_GET_VARS['action']) 
  {
    switch ($HTTP_GET_VARS['action']) 
    {
      
      case 'categories_export':
      
        if (defined('SET_TIME_LIMIT')) { tep_set_time_limit(0); echo "aaa"; }
        
        $schema = '<?xml version="1.0" encoding="' . CHARSET . '"?>' . "\n" .
                  '<CATEGORIES>' . "\n";
                  
        echo $schema;
                  
        $cat_query = tep_db_query("select categories_id, categories_image, parent_id, sort_order, date_added, last_modified ".
        	                         " from " . TABLE_CATEGORIES . " order by parent_id, categories_id");
        while ($cat = tep_db_fetch_array($cat_query)) 
        {
          $schema  = '<CATEGORIES_DATA>' . "\n" .
                     '<ID>' . $cat['categories_id'] . '</ID>' . "\n" .
                     '<PARENT_ID>' . $cat['parent_id'] . '</PARENT_ID>' . "\n" .
                     '<IMAGE_URL>' . htmlspecialchars($cat['categories_image']) . '</IMAGE_URL>' . "\n" .
                     '<SORT_ORDER>' . $cat['sort_order'] . '</SORT_ORDER>' . "\n" .
                     '<DATE_ADDED>' . $cat['date_added'] . '</DATE_ADDED>' . "\n" .
                     '<LAST_MODIFIED>' . $cat['last_modified'] . '</LAST_MODIFIED>' . "\n";
                     
          
          $detail_query = tep_db_query("select categories_id, language_id, categories_name, " . TABLE_LANGUAGES . ".code as lang_code, " . TABLE_LANGUAGES . ".name as lang_name from " . TABLE_CATEGORIES_DESCRIPTION . "," . TABLE_LANGUAGES .
												   " where " . TABLE_CATEGORIES_DESCRIPTION . ".categories_id=" . $cat['categories_id'] . " and " . TABLE_LANGUAGES . ".languages_id=" . TABLE_CATEGORIES_DESCRIPTION . ".language_id");
			
	       while ($details = tep_db_fetch_array($detail_query)) 
          {
               $schema .= "<CATEGORIES_DESCRIPTION ID='" . $details["language_id"] ."' CODE='" . $details["lang_code"] . "' NAME='" . $details["lang_name"] . "'>\n";
         		$schema .= "<NAME>" . htmlspecialchars($details["categories_name"]) . "</NAME>" . "\n";
         		$schema .= "</CATEGORIES_DESCRIPTION>\n";
          }
          
          // Produkte in dieser Categorie auflisten
          
          
          $prod2cat_query = tep_db_query("select categories_id, products_id from " . TABLE_PRODUCTS_TO_CATEGORIES . 
                                         " where categories_id='" . $cat['categories_id'] . "'");
                                       
          while ($prod2cat = tep_db_fetch_array($prod2cat_query)) 
          {
            $schema .="<PRODUCTS ID='" . $prod2cat["products_id"] ."'></PRODUCTS>" . "\n";
          }
          
          $schema .= '</CATEGORIES_DATA>' . "\n";
          
          echo $schema;
        }
        $schema = '</CATEGORIES>' . "\n";
        
        echo $schema;

        exit;
        
      case 'manufacturers_export':
      
        if (defined('SET_TIME_LIMIT')) { tep_set_time_limit(0); }
        $schema = '<?xml version="1.0" encoding="' . CHARSET . '"?>' . "\n" .
                  '<MANUFACTURERS>' . "\n";
                  
        echo $schema;
                  
        $cat_query = tep_db_query("select manufacturers_id, manufacturers_name, manufacturers_image, date_added, last_modified ".
        	                         " from " . TABLE_MANUFACTURERS . " order by manufacturers_id");
        while ($cat = tep_db_fetch_array($cat_query)) 
        {
          $schema  = '<MANUFACTURERS_DATA>' . "\n" .
                     '<ID>' . $cat['manufacturers_id'] . '</ID>' . "\n" .
                     '<NAME>' . htmlspecialchars($cat['manufacturers_name']) . '</NAME>' . "\n" .
                     '<IMAGE>' . htmlspecialchars($cat['manufacturers_image']) . '</IMAGE>' . "\n" .
                     '<DATE_ADDED>' . $cat['date_added'] . '</DATE_ADDED>' . "\n" .
                     '<LAST_MODIFIED>' . $cat['last_modified'] . '</LAST_MODIFIED>' . "\n";
                     
          
          $detail_query = tep_db_query("select manufacturers_id, " . TABLE_MANUFACTURERS_INFO . ".languages_id, manufacturers_url, url_clicked, date_last_click, " . TABLE_LANGUAGES . ".code as lang_code, " . TABLE_LANGUAGES . ".name as lang_name from " . TABLE_MANUFACTURERS_INFO . "," . TABLE_LANGUAGES .
												   " where " . TABLE_MANUFACTURERS_INFO . ".manufacturers_id=" . $cat['manufacturers_id'] . " and " . TABLE_LANGUAGES . ".languages_id=" . TABLE_MANUFACTURERS_INFO . ".languages_id");
			
	       while ($details = tep_db_fetch_array($detail_query)) 
          {
               $schema .= "<MANUFACTURERS_DESCRIPTION ID='" . $details["languages_id"] ."' CODE='" . $details["lang_code"] . "' NAME='" . $details["lang_name"] . "'>\n";
         		$schema .= "<URL>" . htmlspecialchars($details["manufacturers_url"]) . "</URL>" . "\n" ;
         		$schema .= "<URL_CLICK>" . $details["url_clicked"] . "</URL_CLICK>" . "\n" ;
         		$schema .= "<DATE_LAST_CLICK>" . $details["date_last_click"] . "</DATE_LAST_CLICK>" . "\n" ;
         		$schema .= "</MANUFACTURERS_DESCRIPTION>\n";
          }
          
          $schema .= '</MANUFACTURERS_DATA>' . "\n";
          echo $schema;
        }
        $schema = '</MANUFACTURERS>' . "\n";
        
        echo $schema;
        exit;
      
      case 'orders_export':
        $order_from = tep_db_prepare_input($HTTP_GET_VARS['order_from']);
        $order_to = tep_db_prepare_input($HTTP_GET_VARS['order_to']);
        $order_status = tep_db_prepare_input($HTTP_GET_VARS['order_status']);
        
        if (defined('SET_TIME_LIMIT')) { tep_set_time_limit(0); }
        $schema = '<?xml version="1.0" encoding="' . CHARSET . '"?>' . "\n" .
                  '<ORDER>' . "\n";
        
        echo $schema;
        
        $sql ="select * from " . TABLE_ORDERS . " where orders_id >= '" . tep_db_input($order_from) . "'";
        if (!isset($order_status) && !isset($order_from)) 
        {
        	 $order_status = 1;
        	 $sql .= "and orders_status = " . $order_status;
        }
        
        $orders_query = tep_db_query($sql);
        
        while ($orders = tep_db_fetch_array($orders_query)) 
        {
          
          // Geburtsdatum laden
          $cust_sql = "select * from " . TABLE_CUSTOMERS . " where customers_id=" . $orders['customers_id'];
          $cust_query = tep_db_query ($cust_sql);
          if (($cust_query) && ($cust_data = tep_db_fetch_array($cust_query)))
          {
            $cust_dob = $cust_data['customers_dob'];
          }
          
          $schema  = '<ORDER_INFO>' . "\n" .
                     '<ORDER_HEADER>' . "\n" .
                     '<ORDER_ID>' . $orders['orders_id'] . '</ORDER_ID>' . "\n" .
                     '<CUSTOMER_ID>' . $orders['customers_id'] . '</CUSTOMER_ID>' . "\n" .
                     '<ORDER_DATE>' . $orders['date_purchased'] . '</ORDER_DATE>' . "\n" .
                     '<ORDER_STATUS>' . $orders['orders_status'] . '</ORDER_STATUS>' . "\n" .
                     '<ORDER_CURRENCY>' . htmlspecialchars($orders['currency']) . '</ORDER_CURRENCY>' . "\n" .
                     '<ORDER_CURRENCY_VALUE>' . $orders['currency_value'] . '</ORDER_CURRENCY_VALUE>' . "\n" .
                     '</ORDER_HEADER>' . "\n" .
                     '<BILLING_ADDRESS>' . "\n" .
                     '<COMPANY>' . htmlspecialchars($orders['billing_company']) . '</COMPANY>' . "\n" .
                     '<NAME>' . htmlspecialchars($orders['billing_name']) . '</NAME>' . "\n" .
                     '<STREET>' . htmlspecialchars($orders['billing_street_address']) . '</STREET>' . "\n" .
                     '<ZIP>' . htmlspecialchars($orders['billing_postcode']) . '</ZIP>' . "\n" .
                     '<CITY>' . htmlspecialchars($orders['billing_city']) . '</CITY>' . "\n" .
                     '<SUBURB>' . htmlspecialchars($orders['billing_suburb']) . '</SUBURB>' . "\n" .
                     '<STATE>' . htmlspecialchars($orders['billing_state']) . '</STATE>' . "\n" .
                     '<COUNTRY>' . htmlspecialchars($orders['billing_country']) . '</COUNTRY>' . "\n" .
                     '<TELEPHONE>' . htmlspecialchars($orders['customers_telephone']) . '</TELEPHONE>' . "\n" . // JAN
                     '<EMAIL>' . htmlspecialchars($orders['customers_email_address']) . '</EMAIL>' . "\n" . // JAN
                     '<BIRTHDAY>' . htmlspecialchars($cust_dob) . '</BIRTHDAY>' . "\n" .
                     '</BILLING_ADDRESS>' . "\n" .
                     '<DELIVERY_ADDRESS>' . "\n" .
                     '<COMPANY>' . htmlspecialchars($orders['delivery_company']) . '</COMPANY>' . "\n" .
                     '<NAME>' . htmlspecialchars($orders['delivery_name']) . '</NAME>' . "\n" .
                     '<STREET>' . htmlspecialchars($orders['delivery_street_address']) . '</STREET>' . "\n" .
                     '<ZIP>' . htmlspecialchars($orders['delivery_postcode']) . '</ZIP>' . "\n" .
                     '<CITY>' . htmlspecialchars($orders['delivery_city']) . '</CITY>' . "\n" .
                     '<SUBURB>' . htmlspecialchars($orders['delivery_suburb']) . '</SUBURB>' . "\n" .
                     '<STATE>' . htmlspecialchars($orders['delivery_state']) . '</STATE>' . "\n" .
                     '<COUNTRY>' . htmlspecialchars($orders['delivery_country']) . '</COUNTRY>' . "\n" .
                     '</DELIVERY_ADDRESS>' . "\n" .
                     '<PAYMENT>' . "\n" . 
                     '<PAYMENT_METHOD>' . htmlspecialchars($orders['payment_method']) . '</PAYMENT_METHOD>'  . "\n" .
                     '<PAYMENT_CLASS>' . htmlspecialchars($orders['payment_class']) . '</PAYMENT_CLASS>'  . "\n";
          switch ($orders['payment_class']) {
            case 'banktransfer':
              // Bankverbindung laden, wenn aktiv
              $bank_name = '';
              $bank_blz  = '';
              $bank_kto  = '';
              $bank_inh  = '';
              $bank_stat = -1;
  	          $bank_sql = "select * from banktransfer where orders_id = " . $orders['orders_id'];
              $bank_query = tep_db_query($bank_sql);
	            if (($bank_query) && ($bankdata = tep_db_fetch_array($bank_query))) {
	              $bank_name = $bankdata['banktransfer_bankname'];
	              $bank_blz  = $bankdata['banktransfer_blz'];
	              $bank_kto  = $bankdata['banktransfer_number'];
	              $bank_inh  = $bankdata['banktransfer_owner'];
	              $bank_stat = $bankdata['banktransfer_status'];
	            }
              $schema .= '<PAYMENT_BANKTRANS_BNAME>' . htmlspecialchars($bank_name) . '</PAYMENT_BANKTRANS_BNAME>' . "\n" .
                         '<PAYMENT_BANKTRANS_BLZ>' . htmlspecialchars($bank_blz) . '</PAYMENT_BANKTRANS_BLZ>' . "\n" .
                         '<PAYMENT_BANKTRANS_NUMBER>' . htmlspecialchars($bank_kto) . '</PAYMENT_BANKTRANS_NUMBER>' . "\n" .
                         '<PAYMENT_BANKTRANS_OWNER>' . htmlspecialchars($bank_inh) . '</PAYMENT_BANKTRANS_OWNER>' . "\n" .
                         '<PAYMENT_BANKTRANS_STATUS>' . htmlspecialchars($bank_stat) . '</PAYMENT_BANKTRANS_STATUS>' . "\n";
              break;
          }           
          $schema .= '</PAYMENT>' . "\n" . 
                     '<SHIPPING>' . "\n" . 
                     '<SHIPPING_METHOD>' . htmlspecialchars($orders['shipping_method']) . '</SHIPPING_METHOD>'  . "\n" .
                     '<SHIPPING_CLASS>' . htmlspecialchars($orders['shipping_class']) . '</SHIPPING_CLASS>'  . "\n" .
                     '</SHIPPING>' . "\n" .                      
                     '<ORDER_PRODUCTS>' . "\n";
          $products_query = tep_db_query("select orders_products_id, products_id, products_model, products_name, final_price, products_tax, products_quantity from " . TABLE_ORDERS_PRODUCTS . " where orders_id = '" . $orders['orders_id'] . "'");
          while ($products = tep_db_fetch_array($products_query)) 
          {
            $schema .= '<PRODUCT>' . "\n" .
                       '<PRODUCTS_ID>' . $products['products_id'] . '</PRODUCTS_ID>' . "\n" .
                       '<PRODUCTS_QUANTITY>' . $products['products_quantity'] . '</PRODUCTS_QUANTITY>' . "\n" .
                       '<PRODUCTS_MODEL>' . htmlspecialchars($products['products_model']) . '</PRODUCTS_MODEL>' . "\n" .
                       '<PRODUCTS_NAME>' . htmlspecialchars($products['products_name']) . '</PRODUCTS_NAME>' . "\n" .
                       '<PRODUCTS_PRICE>' . $products['final_price'] . '</PRODUCTS_PRICE>' . "\n" .
                       '<PRODUCTS_TAX>' . $products['products_tax'] . '</PRODUCTS_TAX>' . "\n";
                       
            
            $attributes_query = tep_db_query("select products_options, products_options_values, options_values_price, price_prefix�from " . TABLE_ORDERS_PRODUCTS_ATTRIBUTES . " where orders_id = '" .$orders['orders_id'] . "' and orders_products_id = '" . $products['orders_products_id'] . "'");
            if (tep_db_num_rows($attributes_query)) 
            {
              while ($attributes = tep_db_fetch_array($attributes_query)) 
              {
                $schema .= '<OPTION>' . "\n" .
                           '<PRODUCTS_OPTIONS>' .  htmlspecialchars($attributes['products_options']) . '</PRODUCTS_OPTIONS>' . "\n" . 
                           '<PRODUCTS_OPTIONS_VALUES>' .  htmlspecialchars($attributes['products_options_values']) . '</PRODUCTS_OPTIONS_VALUES>' . "\n" .
                           '<PRODUCTS_OPTIONS_PRICE>' .  $attributes['price_prefix'] . ' ' . $attributes['options_values_price'] . '</PRODUCTS_OPTIONS_PRICE>' . "\n" .
                           '</OPTION>' . "\n";
              }
            }            
            $schema .=  '</PRODUCT>' . "\n";

          }
		    $schema .= '</ORDER_PRODUCTS>' . "\n";                     
          $schema .= '<ORDER_TOTAL>' . "\n";
          
          $totals_query = tep_db_query("select title, value, class, sort_order from " . TABLE_ORDERS_TOTAL . " where orders_id = '" . $orders['orders_id'] . "' order by sort_order");
          while ($totals = tep_db_fetch_array($totals_query)) 
          {
            $total_prefix = "";
            $total_tax  = "";
            $total_prefix = $order_total_class[$totals['class']]['prefix'];
            $total_tax = $order_total_class[$totals['class']]['tax'];
            $schema .= '<TOTAL>' . "\n" .
                       '<TOTAL_TITLE>' . htmlspecialchars($totals['title']) . '</TOTAL_TITLE>' . "\n" .
                       '<TOTAL_VALUE>' . htmlspecialchars($totals['value']) . '</TOTAL_VALUE>' . "\n" .
                       '<TOTAL_CLASS>' . htmlspecialchars($totals['class']) . '</TOTAL_CLASS>' . "\n" .
                       '<TOTAL_SORT_ORDER>' . htmlspecialchars($totals['sort_order']) . '</TOTAL_SORT_ORDER>' . "\n" .
                       '<TOTAL_PREFIX>' . htmlspecialchars($total_prefix) . '</TOTAL_PREFIX>' . "\n" .
                       '<TOTAL_TAX>' . htmlspecialchars($total_tax) . '</TOTAL_TAX>' . "\n" . 
                       '</TOTAL>' . "\n";
          }
          $schema .= '</ORDER_TOTAL>' . "\n";
          $comments_query = tep_db_query("select comments from " . TABLE_ORDERS_STATUS_HISTORY . " where orders_id = '" . $orders['orders_id'] . "' and orders_status_id = '" . $orders['orders_status'] . "' ");
          if ($comments =  tep_db_fetch_array($comments_query)) {
            $schema .=  '<ORDER_COMMENTS>' . htmlspecialchars($comments['comments']) . '</ORDER_COMMENTS>' . "\n";
          }
          $schema .= '</ORDER_INFO>' . "\n\n";
          echo $schema;
        }
        $schema = '</ORDER>' . "\n\n";
        
        echo $schema;
        exit;
      
      case 'products_export':
        if (defined('SET_TIME_LIMIT')) { tep_set_time_limit(0); }
        $schema = '<?xml version="1.0" encoding="' . CHARSET . '"?>' . "\n" .
                  '<PRODUCTS>' . "\n";
        echo $schema;
                  
        $sql = "select products_id, products_quantity, products_model, products_image, products_price, " .
               "products_date_added, products_last_modified, products_date_available, products_weight, " .
               "products_status, products_tax_class_id, manufacturers_id, products_ordered from " . TABLE_PRODUCTS;
               
        $from = tep_db_prepare_input($HTTP_GET_VARS['products_from']);
        $anz  = tep_db_prepare_input($HTTP_GET_VARS['products_count']);
        if (isset($from))
        {
          if (!isset($anz)) $anz=1000;
          
          $sql .= " limit " . $from . "," . $anz;
        }
                  
        $orders_query = tep_db_query($sql);
        while ($products = tep_db_fetch_array($orders_query)) 
        {
          $schema  = '<PRODUCT_INFO>' . "\n" .
                     '<PRODUCT_DATA>' . "\n" .
                     '<PRODUCT_ID>' . $products['products_id'] . '</PRODUCT_ID>' . "\n" .
                     '<PRODUCT_QUANTITY>' . $products['products_quantity'] . '</PRODUCT_QUANTITY>' . "\n" .
                     '<PRODUCT_MODEL>' . htmlspecialchars($products['products_model']) . '</PRODUCT_MODEL>' . "\n" .
                     '<PRODUCT_IMAGE>' . htmlspecialchars($products['products_image']) . '</PRODUCT_IMAGE>' . "\n" .
                     '<PRODUCT_PRICE>' . $products['products_price'] . '</PRODUCT_PRICE>' . "\n" .
                     '<PRODUCT_WEIGHT>' . $products['products_weight'] . '</PRODUCT_WEIGHT>' . "\n" .
                     '<PRODUCT_STATUS>' . $products['products_status'] . '</PRODUCT_STATUS>' . "\n" .
                     '<PRODUCT_TAX_CLASS_ID>' . $products['products_tax_class_id'] . '</PRODUCT_TAX_CLASS_ID>' . "\n"  .
                     '<MANUFACTURERS_ID>' . $products['manufacturers_id'] . '</MANUFACTURERS_ID>' . "\n" .
                     
                     '<PRODUCT_DATE_ADDED>' . $products['products_date_added'] . '</PRODUCT_DATE_ADDED>' . "\n" .
                     '<PRODUCT_LAST_MODIFIED>' . $products['products_last_modified'] . '</PRODUCT_LAST_MODIFIED>' . "\n" .
                     '<PRODUCT_DATE_AVAILABLE>' . $products['products_date_available'] . '</PRODUCT_DATE_AVAILABLE>' . "\n" .
                     
                     '<PRODUCTS_ORDERED>' . $products['products_ordered'] . '</PRODUCTS_ORDERED>' . "\n" ;
                     
          $detail_query = tep_db_query("select products_id, language_id, products_name, " . TABLE_PRODUCTS_DESCRIPTION . 
          										".products_description, products_url, name as language_name, code as language_code " .
 												   "from " . TABLE_PRODUCTS_DESCRIPTION . ", " . TABLE_LANGUAGES .
												   " where " . TABLE_PRODUCTS_DESCRIPTION . ".language_id=" . TABLE_LANGUAGES . ".languages_id " .
												   "and " . TABLE_PRODUCTS_DESCRIPTION . ".products_id=" . $products['products_id']);
												  
			 
												  
	       while ($details = tep_db_fetch_array($detail_query)) 
          {
         		$schema .= "<PRODUCT_DESCRIPTION ID='" . $details["language_id"] ."' CODE='" . $details["language_code"] . "' NAME='" . $details["language_name"] . "'>\n";
         		           
         		if ($details["products_name"] !='Array')
         		{
         			$schema .= "<NAME>" . htmlspecialchars($details["products_name"]) . "</NAME>" . "\n" ;
         		}
         		$schema .=  "<URL>" . htmlspecialchars($details["products_url"]) . "</URL>" . "\n" ;
         		
         		$prod_details = $details["products_description"];
         		if ($prod_details != 'Array')
         		{
         			$schema .=  "<DESCRIPTION>" . htmlspecialchars($prod_details) . "</DESCRIPTION>" . "\n";
         		}
         		$schema .= "</PRODUCT_DESCRIPTION>\n";
          }
          $schema .= '</PRODUCT_DATA>' . "\n" .
                     '</PRODUCT_INFO>' . "\n";
          echo $schema;
        }
        $schema = '</PRODUCTS>' . "\n\n";
        echo $schema;
        exit;
        
	//-- Raphael Vullriede
	//-- added action for customers
  //-- fixed by Henri Schmidhuber
  case 'customers_export':
		if (defined('SET_TIME_LIMIT')) { tep_set_time_limit(0); }
		$schema = '<?xml version="1.0" encoding="' . CHARSET . '"?>' . "\n" .
			'<CUSTOMERS>' . "\n";
			
		echo $schema;
			
		$from = tep_db_prepare_input($HTTP_GET_VARS['customers_from']);
		$anz  = tep_db_prepare_input($HTTP_GET_VARS['customers_count']);
		
		$address_query = "select c.customers_gender, c.customers_id, c.customers_firstname, c.customers_lastname,c.customers_dob, c.customers_email_address, c.customers_telephone, c.customers_fax, 
                             ci.customers_info_date_account_created  as customers_date_account_created, 
                             a.entry_firstname as firstname, a.entry_lastname as lastname, a.entry_company as company, a.entry_street_address as street_address, a.entry_city as city, a.entry_postcode as postcode, 
                             co.countries_iso_code_2 as country 
                      from " . TABLE_CUSTOMERS. " c, ". TABLE_CUSTOMERS_INFO. " ci, ". TABLE_ADDRESS_BOOK . " a , ".TABLE_COUNTRIES." co 
                      where c.customers_id = ci.customers_info_id
                        AND c.customers_id = a.customers_id 
                        AND c.customers_default_address_id = a.address_book_id 
                        AND a.entry_country_id  = co.countries_id";
		if (isset($from)) 
		{
			if (!isset($anz)) $anz = 1000;
			$address_query.= " limit " . $from . "," . $anz;
		}
		$address_result = tep_db_query($address_query);
		
		while ($address = tep_db_fetch_array($address_result)) 
		{
			$schema = '<CUSTOMERS_DATA>' . "\n";		
				foreach($address as $key => $value) 
				{
					$schema.= '<'.strtoupper($key).'>'.htmlspecialchars($value).'</'.strtoupper($key).'>'."\n";
				}
			$schema .= '</CUSTOMERS_DATA>' . "\n";		
			echo $schema;
		}
		
		$schema = '</CUSTOMERS>' . "\n\n";
		echo $schema;
		exit;
	
	//-- end action for customers

  // Newsletter export
  case 'customers_newsletter_export':
		if (defined('SET_TIME_LIMIT')) { tep_set_time_limit(0); }
		$schema = '<?xml version="1.0" encoding="' . CHARSET . '"?>' . "\n" .
			'<CUSTOMERS>' . "\n".
			
		$from = tep_db_prepare_input($HTTP_GET_VARS['customers_from']);
		$anz  = tep_db_prepare_input($HTTP_GET_VARS['customers_count']);
		
		$address_query = "select *
                      from " . TABLE_CUSTOMERS. " 
                      where customers_newsletter = 1
                     ";
		if (isset($from)) {
			if (!isset($anz)) $anz = 1000;
			$address_query.= " limit " . $from . "," . $anz;
		}
		$address_result = tep_db_query($address_query);
		
		while ($address = tep_db_fetch_array($address_result)) 
		{
			$schema .= '<CUSTOMERS_DATA>' . "\n";		
      	$schema .= '<CUSTOMERS_ID>' . $address['customers_id'] . '</CUSTOMERS_ID>' . "\n";	
      	$schema .= '<CUSTOMERS_GENDER>' . $address['customers_gender'] . '</CUSTOMERS_GENDER>' . "\n";	
      	$schema .= '<CUSTOMERS_FIRSTNAME>' . $address['customers_firstname'] . '</CUSTOMERS_FIRSTNAME>' . "\n";	
      	$schema .= '<CUSTOMERS_LASTNAME>' . $address['customers_lastname'] . '</CUSTOMERS_LASTNAME>' . "\n";	
      	$schema .= '<CUSTOMERS_EMAIL_ADDRESS>' . $address['customers_email_address'] . '</CUSTOMERS_EMAIL_ADDRESS>' . "\n";	
			$schema .= '</CUSTOMERS_DATA>' . "\n";		
		}
		
		$schema .= '</CUSTOMERS>' . "\n\n";
		echo $schema;
		exit;
	
	//-- end action for customers
  
	
   case 'version':
        // Ausgabe Scriptversion
        $schema = '<?xml version="1.0" encoding="' . CHARSET . '"?>' . "\n" .
                  '<STATUS>' . "\n" .
                  '<STATUS_DATA>' . "\n" .
                  '<ACTION>' . $HTTP_GET_VARS['action'] . '</ACTION>' . "\n" .
                  '<CODE>' . '111' . '</CODE>' . "\n" .              
                  '<SCRIPT_VER>' . $version_nr . '</SCRIPT_VER>' . "\n" . 
                  '<SCRIPT_DATE>' . $version_datum . '</SCRIPT_DATE>' . "\n" . 
                  '</STATUS_DATA>' . "\n" .
                  '</STATUS>' . "\n\n";
       echo $schema; 
       exit;
     }
  }
?>